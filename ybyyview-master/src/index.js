import 'url-polyfill';
import dva from 'dva';
import registerModels from './models/index';
import createLoading from 'dva-loading';
import 'moment/locale/zh-cn';
import { run } from 'snk-sso-um';
import './rollbar';
import createHistory from 'history/createHashHistory';
import React from 'react';
import 'antd/dist/antd.less';
import { routerRedux, Route, Switch } from 'dva/router';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import dynamic from 'dva/dynamic';
import Authorized from './utils/Authorized';
import './index.less';
import { LocaleProvider, Spin, message } from 'snk-web';

const menuData = [
  {
    name: '消息管理',
    icon: 'message',
    path: 'message-manage',
    children: [
      {
        name: '消息读取页面',
        path: 'message-form',
      },
      {
        name: '任务处理界面',
        path: 'task-form',
      },
    ],
  },
  {
    name: '投保管理',
    icon: 'folder-open',
    path: 'apply-manage',
    children: [
      {
        name: '投保单查询',
        path: 'apply-list',
      },
      {
        name: '自建渠道投保',
        path: 'selfchannel-form',
      },
      {
        name: '自建渠道质检审核',
        path: 'selfchannel-audit',
      },
    ],
  },
  {
    name: '保单批改',
    icon: 'edit',
    path: 'correction',
    children: [
      {
        name: '保单注退批改',
        path: 'cancel-correction',
      },
      {
        name: '批改审核-注退',
        path: 'cancel-audit',
      },
      {
        name: '客户信息批改',
        path: 'basic-correction',
      },
      {
        name: '批改审核-客户信息',
        path: 'basic-audit',
      },
      {
        name: '取消续保批改',
        path: 'review-correction',
      },
      {
        name: '转快钱批改',
        path: 'fastmoney-correction',
      },
      {
        name: '续保挽回继续请款',
        path: 'renewal-payment',
      }
    ],
  },
  {
    name: '续保管理',
    icon: 'folder-add',
    path: 'renewal-manage',
    children: [
      {
        name: '续保查询',
        path: 'renewal-list',
      },
      {
        name: '续保情况汇总展示',
        path: 'renewal-display',
      },
    ],
  },
  {
    name: '请扣款管理',
    icon: 'pay-circle-o',
    path: 'finance-manage',
    children: [
      {
        name: '请扣款清单查询',
        path: 'finance-list',
      },
      {
        name: '请扣款文件汇总查询',
        path: 'finance-sum',
      },
    ],
  },
  {
    name: '查询功能',
    icon: 'table',
    path: 'query-manage',
    children: [
      {
        name: '保单信息查询',
        path: 'policy-query',
      },
      {
        name: '保单详情页',
        path: 'policy-list',
      },
      {
        name: '理赔信息查询',
        path: 'claim-form',
      },
    ],
  },
  {
    name: '报表功能',
    icon: 'line-chart',
    path: 'report-manage',
    children: [
      {
        name: '整体情况汇总',
        path: 'basic-report',
      },
      {
        name: '个性报表取数',
        path: 'free-report',
      },
      {
        name: '固定报表取数',
        path: 'absolute-report',
      },
      {
        name: '财务对账报表',
        path: 'finance-report',
      },
    ],
  },
  {
    name: '快递管理',
    icon: 'gift',
    path: 'express-manage',
    children: [
      {
        name: '快递、回执单查询',
        path: 'express-route',
      }, 
      {
        name: '保单补发登记',
        path: 'apply-resend',
      },
      {
        name: '快递费用管理查询',
        path: 'express-charge',
      },
      {
        name: '回执单扫码入库',
        path: 'receipt-scan',
      },
    ],
  },
  {
    name: '保单打印',
    icon: 'printer',
    path: 'policy-print',
    children: [
      {
        name: '新单打印管理',
        path: 'print-manage',
      },
      {
        name: '保单补打管理',
        path: 'reprint-manage',
      },
       {
        name: '打印详情页',
        path: 'list-details',
      },
      {
        name: '电子邮件发送详情',
        path: 'mail-detail',
      },
    ],
  },
  {
    name: '销售方案管理',
    icon: 'schedule',
    path: 'sales-plan',
    children: [
      {
        name: '销售方案查询',
        path: 'plan-query',
      },
      {
        name: '续保通知函发送配置',
        path: 'renewal-config',
      },
    ],
  },
  {
    name: '自动任务管理',
    icon: 'like-o',
    path: 'quartz-manage',
    children: [
      {
        name: 'job管理',
        path: 'job-manage',
      },
      {
        name: '自动任务列表管理',
        path: 'quartz-list',
      },
    ],
  },
  {
    name: '回执管理',
    icon: 'like-o',
    path: 'receipt-manage',
    children: [
      {
        name: '回执管理',
        path: 'receipt-manage',
      },
    ],
  },
  {
    name: '增值服务',
    icon: 'money-collect',
    path: 'addValue',
    children: [
      {
        name: '数据提取',
        path: 'data-extraction',
      },
    ]
  },
  {
    name: '积分专题',
    icon: 'pay-circle-o',
    path: 'integral',
    children: [
      {
        name: '积分查询',
        path: 'search-integral'
      },
      {
        name: '赠送积分',
        path: 'gift-integral',
      },
      {
        name: '积分管理',
        path: 'manage-integral'
      },
      {
        name: '积分整体',
        path: 'all-integral',
      },
      {
        name: '客户留存率',
        path: 'customer-integral',
      },      
    ],
  },

];

const router = {
  // 信息读取界面
  '/message-manage/message-form': {
    component: () => import('./routes/MessageManage/MessageForm'),
    model:['messageManage'],
    permission: 'all',
  },
  // 自建渠道
  '/apply-manage/selfchannel-form': {
    component: () => import('./routes/SelfchannelForm'),
    model:[],
  },
   // 投保查询
  '/apply-manage/apply-list': {
    component:() => import('./routes/ApplyManage/ApplyList'),
    model:['searchlist'],
  }, 
  '/apply-manage/selfchannel-audit': {
    component:  () => import('./routes/SelfChannelAudit'),
    model:['selfchannelForm'],
  },
  '/apply-manage/apply-editable-list': {
    component:  () => import('./routes/ApplyManage/ApplyEditableList'),
    model:[],
  },
  // 快递管理
  '/express-manage/express-route': {
    component: () => import('./routes/ExpressRoute'),
    model:[],
  },
  '/policy-print/apply-resend': {
    component: () => import('./routes/ApplyResend'),
    model:[],
  },
  '/policy-print/rep-register-manage': {
    component: () => import('./routes/PolicyPrint/RepRegisterManage'),
    model:[],
  },
  '/express-manage/express-charge': {
    component: () => import('./routes/ExpressCharge'),
    model:[],
  },
  '/renewal-manage/renewal-list': {
    component:  () => import('./routes/RenewalList'),
    model:["searchlist"],
  },
  // 续保通知函寄送日志
  '/renewal-manage/renewal-notification': {
    component:  () => import('./routes/RenewalNotification/RenewalNotification'),
  },
  '/renewal-manage/renewal-display': {
    component: () => import('./routes/RenewalDisplay'),
    model:[],
  },
  '/report-manage/absolute-report': {
    component: () => import('./routes/AbsoluteReport'),
    model:["searchlist"],
  },
  '/report-manage/online-report': {
    component: () => import('./routes/ReportFunction/OnlineReport'),
    model:["reportModels"],
  },
  '/report-manage/finance-report': {
    component:() => import('./routes/FinanceReport/FinanceReport'),
    model:["error"],
  },
  '/report-manage/finance-query': {
    component:() => import('./routes/FinanceQuery'),
    model:["financeQuery"],
  },
  '/report-manage/import-report': {
    component:() => import('./routes/ImportReport'),
    model:[],
  },
  '/report-manage/basic-report': {
    component: () => import('./routes/BasicReport'),
    model:["report"],
  },
  '/report-manage/free-report': {
    component:  () => import('./routes/FreeReport'),
    model:["searchlist"],
  },
  // 固定取数
  '/report-manage/fixed-fetch': {
    component: () =>
      import('./routes/FixedFetch/FixedFetch'),
      model:["form"],
      // permission:'all',
  },
   // 转块钱批改
   '/correction/fastmoney-correction': {
    component: () =>
      import('./routes/Correction/FastmoneyCorrection')
    ,
    model:["form"],
  },
   // 客户信息批改
   '/correction/basic-correction': {
    component:  () => import('./routes/Correction/BasicCorrection'),
    model:["profile", "selfchannelForm"],
  },
   // 保单注退批改
   '/correction/cancel-correction': {
    component: () =>
      import('./routes/Correction/CancelCorrection')
    ,
    model:["form"],
  },
  // 取消续保批改
  '/correction/review-correction': {
    component: () =>
      import('./routes/Correction/ReviewCorrection')
    ,
    model:["form"],
  },
  // 批量注销
  '/correction/batch-cancel': {
    component: () => import('./routes/Correction/BatchCancel'),
    model:["form"],
  },
  // 非犹豫期内的退保退费审核
  '/correction/cancel-audit': {
    component: () =>
      import('./routes/Correction/CancelAudit')
    ,
    model:["form"],
    // permission:'all',
  },
  // 客户信息批改审核
  '/correction/basic-audit': {
    component: () =>
      import('./routes/Correction/BasicAudit'),
      model:["form"],
      // permission:'all',
  },
  // 客户信息批改审核界面 --- 不支持搜索
  '/message-manage/correction': {
    component:  () =>
      import('./routes/Correction/Correction'),
    model:["profile"],
  },
  // 保单信息查询
  '/query-manage/policy-query': {
    component:() => import('./routes/QueryManage/PolicyQuery'),
    model:["rule", 'searchlist'],
  },
  // 理赔信息查询
  '/query-manage/claim-form': {
    component:() => import('./routes/QueryManage/ClaimForm'),
    model:[],
  },
  // 保单详情页
  '/query-manage/policy-list': {
    component: () => import('./routes/QueryManage/PolicyList'),
    model:[],
    permission:'all',
  },
   // 自动运行任务
   '/quartz-manage/job-manage': {
    component:  () => import('./routes/QuartzManage/JobManage'),
    model:['rule'],
  },
  '/quartz-manage/quartz-list': {
    component:  () => import('./routes/QuartzManage/QuartzList'),
    model:['rule'],
  },
  // 请扣款管理
  '/finance-manage/finance-list': {
    component: () => import('./routes/FinanceManage/FinanceList'),
    model:['rule'],
  },
  '/finance-manage/finance-sum': {
    component: () => import('./routes/FinanceManage/FinanceSum'),
    model:['rule'],
  },
  // 销售方案管理
  '/sales-plan/plan-query': {
    component: () => import('./routes/SalesPlan/PlanQuery'),
    model:['rule'],
  },
  '/sales-plan/renewal-config': {
    component:  () => import('./routes/SalesPlan/RenewalConfig'),
    model:['rule'],
    permission:'all',
  },
  '/finance-manage/finance-list': {
    component: () => import('./routes/FinanceManage/FinanceList'),
    model:['rule'],
  },
  // 打印管理
  '/policy-print/print-manage': {
    component:() => import('./routes/PolicyPrint/printManage/PrintManage'),
    model:['rule'],
  },
  '/policy-print/reprint-manage': {
    component: () => import('./routes/PolicyPrint/ReprintManage'),
    model:['rule'],
    permission:'all',
  },
   '/policy-print/mail-detail': {
    component:() => import('./routes/PolicyPrint/MailDetail'),
    model:['rule'],
  },
  '/policy-print/list-details': {
    component:() => import('./routes/PolicyPrint/ListDetails'),
    model:['rule'],
  },
  '/profile/basic': {
    component: () => import('./routes/Profile/BasicProfile'),
    model:['profile'],
  },
  '/profile/advanced': {
    component: () =>
      import('./routes/Profile/AdvancedProfile')
    ,
    model:['profile'],
  },
  // 任务处理界面
  '/message-manage/task-form': {
    component:  () => import('./routes/MessageManage/TaskForm'),
    model:['profile'],
    permission:'all',
  },
 // 读取邮件
  '/message-manage/read-message': {
    component:  () => import('./routes/MessageManage/ReadMessage'),
    model:['profile'],
    permission:'all',
  },
  // 回执管理
  '/express-manage/receipt-manage': {
    component:  () => import('./routes/ReceiptManage/ReceiptManage'),
    model:['receiptManage'],
  },
   // 人员信息管理 - 人员列表
   '/person/searchPerson': {
    component: () => import('./routes/Person/SearchPerson'), model: ['personModels']
  },

  // 人员信息管理 - 新增员工
  '/person/addPerson': {
    component: () => import('./routes/Person/BasePersonMessage'), model: ['personModels'],
  },
   // 人员信息管理 - 人员列表(客服)
   '/person/searchCustomerPerson': {
    component: () => import('./routes/Person/searchCustomerPerson'), model: ['personCustomerModels']
  },

  // 团队和组别管理
  '/person/searchGroup': {
    component: () => import('./routes/Person/SearchGroup'), model:['error','groupModel','shareModel'],
  },
   // 职级审核
   '/person/rankRecordAudit': {
    component: () => import('./routes/Person/RankRecordAudit'), model:['error','personModels'],
  },
 
  '/salary/importFactor': {
    component: () => import('./routes/Salary/ImportFactor'), model: ['error','shareModel'],
  },
  // 待审核任务-待审核任务
  '/salary/approvedList': {
    component: () => import('./routes/Salary/ApprovedList'), model: ['approvedList'],
  },
 
  // 团队和组别详情管理
  '/person/searchGroupDetails/:id': {
    component: () => import('./routes/Person/SearchGroupDetails'), model:['error','groupModel','shareModel'], permission: 'all',
  },

  // 模版管理
  '/person/searchModel': {
    component: () => import('./routes/Salary/personTempConfig'), model:['error','groupModel','shareModel',], 
  },

  // 人员信息管理 - 编辑员工
  '/person/editPerson': {
    component: () => import('./routes/Person/EditBasePersonMessage'), model: ['personModels'], permission: 'all'
  },
  // 人员信息管理 - 编辑员工
  '/person/editCustomerPerson': {
    component: () => import('./routes/Person/EditBaseCustomerPersonMessage'), model: ['personCustomerModels'], permission: 'all'
  },
  // 人管消息通知 - 消息列表
  '/message/messageList': {
    component: () => import('./routes/MessageManage/MessageFormHR'), model: ['messageManageHR'],
  },
  // 消息通知 - 消息列表2
  '/message/messageList2': {
    component: () => import('./routes/List/List'), model: ['messageManage'],
  },
  
  // 待审核任务-工资邮件
  '/salary/emailSalary': {
    component: () => import('./routes/Salary/EmailSalary'), model: ['approvedList'],
  },
   // 待审核任务-职级审核
   '/salary/rankApproved': {
    component: () => import('./routes/Salary/RankApproved'), model: [],
  },
  // 读取邮件
  '/message-manage/read-message': {
    component:  () => import('./routes/MessageManage/ReadMessage'),
    model:['profiles'],
    permission:'all',
  },
  //职级管理
  '/rank/editPosition': {
    component: () => import('./routes/Position/EditPositon'),
    model: ['editPosition'],
    permission:'all'
  },
  //劳务派遣公司
  '/outsource/searchDispatch': {
    component: () => import('./routes/OutSource/OutSource'), model: ['error'],
  },
 
  //薪酬查询
  '/salary/querySarlary': {
    component: () => import('./routes/Salary/querySarlary'), model: ['error'],
  },
  //薪酬修改
  '/salary/editSarlary':{
    component: () => import('./routes/Salary/editSarlary'), model: ['error'], permission: 'all',
  },
  //薪酬审核
  '/salary/auditingSarlary': {
    component: () => import('./routes/Salary/auditingSarlary'), model: ['error'], permission: 'all',
  },
  //退回审核
  '/salary/returnBackSarlary': {
    component: () => import('./routes/Salary/returnBackSarlary'), model: ['error'], permission: 'all',
  },
  //审核完成
  '/salary/finishAuditSarlary': {
    component: () => import('./routes/Salary/finishAuditSarlary'), model: ['error'], permission: 'all',
  },
  '/': {
    component: () => import('./layouts/BasicLayout'),
    model:['umssouserinfo'],
  },
  '/exception/403': {
    component:  () => import('./routes/Exception/403'),
    model:[],
  },
  '/result/success': {
    component:  () => import('./routes/Result/Success'),
    model:[],
  },
  '/result/fail': {
    component:  () => import('./routes/Result/Error'),
    model:[],
  },
  '/message-manage/blank': {
    component:  () => import('./routes/MessageManage/Blank'),
    model:[],
    permission:'all',
  },
  '/exception/404': {
    component:  () => import('./routes/Exception/404'),
    model:[],
  },
  '/exception/500': {
    component:  () => import('./routes/Exception/500'),
    model:[],
  },
  '/exception/trigger': {
    component:  () =>
      import('./routes/Exception/triggerException')
    ,
    model:['error'],
  },
  //续保挽回继续请款
  '/correction/renewal-payment': {
    component: () => import('./routes/Correction/RenewalPayment'), model: ['error'],
  },
  //积分查询
  '/query-manage/search-integral':{
    component: () => import('./routes/Integral/IntegralSearch/IntegralSearch'), model: ['error'],
  },
  // 赠送积分
  '/integral/gift-integral':{
    component: () => import('./routes/Integral/IntegralGift/IntegralGift'), model: ['error'],
  },
  //积分管理
  '/integral/manage-integral':{
    component: () => import('./routes/Integral/IntegralManage/IntegralManage'), model: ['error'],
  },
  //积分报表-积分整体
  '/integral/report-integral/all-report-integral':{
    component: () => import('./routes/Integral/IntegralReport/IntegralAll'), model: ['error'],
  },
   //积分报表-客户留存率
   '/integral/report-integral/customer-report-integral':{
    component: () => import('./routes/Integral/IntegralReport/IntegralCustomer'), model: ['error'],
  },
   //积分报表-客户整体查询
   '/integral/report-integral/customer-all':{
    component: () => import('./routes/Integral/IntegralReport/customerAll/CustomerAllSearch'), model: ['error'],
  },
    //积分红包--查询
    '/integral/redPacket-integral/redPacketList':{
    component: () => import('./routes/Integral/redPacket/RedPacketSearch'), model: ['error'],
  },
  //积分红包--客户红包详情
  '/integral/redPacket-integral/redPacketCustomerList':{
    component: () => import('./routes/Integral/redPacket/RedPacketList'), model: ['error'],
    permission:'all',
  },
  // 新单增值服务取数
  '/valueadded/new-order-data':{
    component: () => import('./routes/ValueAdded/newOrderData'), model: ['error'],
  },
  // 保全增值服务取数
  '/valueadded/security-policy-data':{
    component: () => import('./routes/ValueAdded/securityPolicyData'), model: ['error'],
  },
  // 结算维度取数
  '/valueadded/account-data':{
    component: () => import('./routes/ValueAdded/accountData'), model: ['error'],
  },
  // 增值服务物流管理系统
  '/valueadded/logistics-manage':{
    component: () => import('./routes/ValueAdded/LogisticsManage'), model: ['error'],
  },
  // // 增值服务管理配置，模块暂时取消
  // '/valueadded/config-manage':{
  //   component: () => import('./routes/ValueAdded/ConfigManage'), model: ['error'],
  // },
  // 增值服务库存管理
  '/valueadded/inventory-control':{
    component: () => import('./routes/ValueAdded/InventoryControl'), model: ['error'],
  },
  // 增值服务库存详情
  '/valueadded/inventory-detail':{
    component: () => import('./routes/ValueAdded/InventoryDetail'), model: ['error'],
    permission:'all',
  },
  // 增值服务快递单打印
  '/valueadded/value-added-print':{
    component: () => import('./routes/ValueAdded/ValueAddedPrint'), model: ['error'],
  },
};

function RouterConfig({ history, app }) {
  return run({
    app,
    um: true,// 屏蔽单点登录及menu服务
    useButtonPermission:true,
    menuData,
    router,
    renderContent: (params) => {
      return (
        <LocaleProvider locale={zhCN}>
          <ConnectedRouter history={history}>
            <Switch>
              <Route
                path="/"
                render={
                  props => <params.BasicLayout {...props} {...params} />}
              />
            </Switch>
          </ConnectedRouter>
        </LocaleProvider>);
    },
  });
}

dynamic.setDefaultLoadingComponent(() => {
  return <Spin size="large" className="globalSpin" />;
});


const { ConnectedRouter } = routerRedux;
const { AuthorizedRoute } = Authorized;
const history = createHistory();
const app = dva({
  history,
  onError(e){
    if (e.code) {
      message.error(`index >>>>>> ${e.message}`);
    }
  },
});

// 2. Plugins
app.use(createLoading());

// 3. Register global model
registerModels(app);
// 4. Router
app.router(RouterConfig);
// 5. Start
app.start('#root');
export default app._store;





