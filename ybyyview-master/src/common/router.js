import { createElement } from 'react';
import dynamic from 'dva/dynamic';
import pathToRegexp from 'path-to-regexp';
import { getMenuData } from './menu';

let routerDataCache;

const modelNotExisted = (app, model) =>
  // eslint-disable-next-line
  !app._models.some(({ namespace }) => {
    return namespace === model.substring(model.lastIndexOf('/') + 1);
  });

// wrapper of dynamic
const dynamicWrapper = (app, models, component) => {
  // () => require('module')
  // transformed by babel-plugin-dynamic-import-node-sync
  if (component.toString().indexOf('.then(') < 0) {
    models.forEach(model => {
      if (modelNotExisted(app, model)) {
        // eslint-disable-next-line
        app.model(require(`../models/${model}`).default);
      }
    });
    return props => {
      if (!routerDataCache) {
        routerDataCache = getRouterData(app);
      }
      return createElement(component().default, {
        ...props,
        routerData: routerDataCache,
      });
    };
  }
  // () => import('module')
  return dynamic({
    app,
    models: () =>
      models.filter(model => modelNotExisted(app, model)).map(m => import(`../models/${m}.js`)),
    // add routerData prop
    component: () => {
      if (!routerDataCache) {
        routerDataCache = getRouterData(app);
      }
      return component().then(raw => {
        const Component = raw.default || raw;
        return props =>
          createElement(Component, {
            ...props,
            routerData: routerDataCache,
          });
      });
    },
  });
};

// 只有有权限的菜单才注册

let _serverMenuData = null;
export const getRouterData = (app,serverMenuData) => {
  if(serverMenuData){
    _serverMenuData = serverMenuData;
  }
  if(!serverMenuData&&_serverMenuData){
    serverMenuData = _serverMenuData;
  }
  
  const noPermissonMenu = {
    '/': {
      component: dynamicWrapper(app, ['user'], () => import('../layouts/BasicLayout')),
    },
    '/exception/403': {
      component: dynamicWrapper(app, [], () => import('../routes/Exception/403')),
    },
    '/result/success': {
      component: dynamicWrapper(app, [], () => import('../routes/Result/Success')),
    },
    '/result/fail': {
      component: dynamicWrapper(app, [], () => import('../routes/Result/Error')),
    },
    '/exception/404': {
      component: dynamicWrapper(app, [], () => import('../routes/Exception/404')),
    },
    '/exception/500': {
      component: dynamicWrapper(app, [], () => import('../routes/Exception/500')),
    },
    '/exception/trigger': {
      component: dynamicWrapper(app, ['error'], () =>
        import('../routes/Exception/triggerException')
      ),
    },
  };
  const routerConfig = {
    //自建渠道
    '/apply-manage/selfchannel-form': {
      component: dynamicWrapper(app, [], () => import('../routes/SelfchannelForm')),
    },
     //投保查询
    '/apply-manage/apply-list': {
      component: dynamicWrapper(app, ['searchlist'], () => import('../routes/ApplyManage/ApplyList')),
    }, 
    '/message-manage/selfchannel-audit': {
      component: dynamicWrapper(app, [], () => import('../routes/SelfChannelAudit')),
    },
    //快递管理
    '/express-manage/express-route': {
      component: dynamicWrapper(app, [], () => import('../routes/ExpressRoute')),
    },
    '/express-manage/apply-resend': {
      component: dynamicWrapper(app, [], () => import('../routes/ApplyResend')),
    },
    '/express-manage/express-charge': {
      component: dynamicWrapper(app, [], () => import('../routes/ExpressCharge')),
    },
    '/renewal-manage/renewal-list': {
      component: dynamicWrapper(app, ["searchlist"], () => import('../routes/RenewalList')),
    },
    '/renewal-manage/renewal-display': {
      component: dynamicWrapper(app, [], () => import('../routes/RenewalDisplay')),
    },
    '/report-manage/absolute-report': {
      component: dynamicWrapper(app, ["searchlist"], () => import('../routes/AbsoluteReport')),
    },
    '/report-manage/finance-report': {
      component: dynamicWrapper(app, ["financeReport"], () => import('../routes/FinanceReport')),
    },
   
    '/report-manage/basic-report': {
      component: dynamicWrapper(app, ["report"], () => import('../routes/BasicReport')),
    },
    '/report-manage/free-report': {
      component: dynamicWrapper(app, ["searchlist"], () => import('../routes/FreeReport')),
    },
     //转块钱批改
     '/correction/fastmoney-correction': {
      component: dynamicWrapper(app, ['form'], () =>
        import('../routes/Correction/FastmoneyCorrection')
      ),
    },
     //客户信息批改
     '/correction/basic-correction': {
      component: dynamicWrapper(app, ['profile'], () =>
        import('../routes/Correction/BasicCorrection')
      ),
    },
     //保单注退批改
     '/correction/cancel-correction': {
      component: dynamicWrapper(app, ['form'], () =>
        import('../routes/Correction/CancelCorrection')
      ),
    },
    //取消续保批改
    '/correction/review-correction': {
      component: dynamicWrapper(app, ['form'], () =>
        import('../routes/Correction/ReviewCorrection')
      ),
    },
    //非犹豫期内的退保退费审核
    '/message-manage/cancel-audit': {
      component: dynamicWrapper(app, ['form'], () =>
        import('../routes/Correction/CancelAudit')
      ),
    },
    //客户信息批改审核
    '/message-manage/basic-audit': {
      component: dynamicWrapper(app, ['form'], () =>
        import('../routes/Correction/BasicAudit')
      ),
    },
    //客户信息批改审核界面 --- 不支持搜索
    '/correction/correction': {
      component: dynamicWrapper(app, ['profile'], () =>
        import('../routes/Correction/Correction')
      ),
    },
    //保单信息查询
    '/query-manage/policy-query': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/QueryManage/PolicyQuery')),
    },
    //理赔信息查询
    '/query-manage/claim-form': {
      component: dynamicWrapper(app, [], () => import('../routes/QueryManage/ClaimForm')),
    },
    //保单详情页
    '/query-manage/policy-list': {
      component: dynamicWrapper(app, [], () => import('../routes/QueryManage/PolicyList')),
    },
     //自动运行任务
     '/quartz-manage/job-manage': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/QuartzManage/JobManage')),
    },
    '/quartz-manage/quartz-list': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/QuartzManage/QuartzList')),
    },
    //请扣款管理
    '/finance-manage/finance-list': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/FinanceManage/FinanceList')),
    },
    '/finance-manage/finance-sum': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/FinanceManage/FinanceSum')),
    },
    //销售方案管理
    '/sales-plan/plan-query': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/SalesPlan/PlanQuery')),
    },
    '/sales-plan/renewal-config': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/SalesPlan/RenewalConfig')),
    },
    '/finance-manage/finance-list': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/FinanceManage/FinanceList')),
    },
    //打印管理
    '/policy-print/print-manage': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/PolicyPrint/PrintManage')),
    },
    '/policy-print/reprint-manage': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/PolicyPrint/ReprintManage')),
    },
     '/policy-print/mail-detail': {
      component: dynamicWrapper(app, ['rule'], () => import('../routes/PolicyPrint/MailDetail')),
    },
    '/profile/basic': {
      component: dynamicWrapper(app, ['profile'], () => import('../routes/Profile/BasicProfile')),
    },
    '/profile/advanced': {
      component: dynamicWrapper(app, ['profile'], () =>
        import('../routes/Profile/AdvancedProfile')
      ),
    },
    //信息读取界面
    '/message-manage/message-form': {
      component: dynamicWrapper(app, ['messageManage'], () => import('../routes/MessageManage/MessageForm')),
    },
    // 任务处理界面
    '/message-manage/task-form': {
      component: dynamicWrapper(app, ['profile'], () => import('../routes/MessageManage/TaskForm')),
    },
   // 读取邮件
    '/message-manage/read-message': {
      component: dynamicWrapper(app, ['profile'], () => import('../routes/MessageManage/ReadMessage')),
    },
  };

  const re = {};
  for(var key in routerConfig){
    if(hasPermission(key,serverMenuData)){
      re[key] = routerConfig[key];
    }
  }
  for(var key in noPermissonMenu){
    re[key] = noPermissonMenu[key];
  }
  return re;
};

function isExistInServerMenuData(p,serverMenuData){
  let re = false;
  for(let i = 0,j=serverMenuData.length;i<j;i+=1){
    const url = `/${serverMenuData[i].url}/`;
    if(url.indexOf(`/${p}/`)>=0){
      re = true;
      break;
    }
  }
  return re;
}
function hasPermission(path,serverMenuData){
  const arr = path.split("/");
  let re = true;
  for(let i = 0,j=arr.length;i<j;i+=1){
    const sPath = arr[i];
    if(sPath!==''){
      if(!isExistInServerMenuData(sPath,serverMenuData)){
        re = false;
        break;
      }
    }
  }
  return re;
}

