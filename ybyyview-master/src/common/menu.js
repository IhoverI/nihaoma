import { isUrl } from '../utils/utils';

function formatter(data, parentPath = '/', parentAuthority) {
  return data.map(item => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}



let result = [];
function convertServerDataToMenuData (serverMenuData){
  if(result.length>0){
    return result;
  }
  if(!serverMenuData){
    return [];
  }
  const Re = [];
  for(let i = 0,j=serverMenuData.length;i<j;i+=1){
    const item = serverMenuData[i];
    if(!item.parentId){
      Re.push({
        name: item.resourceName,
        icon: item.resourceIcon,
        path: item.url,
        children:getChildren(item.resourceId,serverMenuData)
      });
    }
  }
  result = formatter(Re)
  return result;
}

function getChildren(id,serverdata){
  const Re = [];
  for(let i = 0,j=serverdata.length;i<j;i+=1){
    const item = serverdata[i];
    if(item.parentId === id){
      Re.push({
        name: item.resourceName,
        icon: item.resourceIcon,
        path: item.url,
        children:getChildren(item.resourceId,serverdata)
      });
    }
  }
  return Re;
}

export const getMenuData = (serverMenuData) => convertServerDataToMenuData(serverMenuData);
