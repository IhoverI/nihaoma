import shareService from '../services/shareService';

export default {

  namespace: 'shareModel',

  state: {
    groupsData: [],
    data: {},
  },

  effects: {
    *searchGroups({data}, { call, put }) {
      const response = yield call(shareService.searchGroups, { data });
      console.log(response)
      yield put({
        type: 'initGroups',
        payload: response,
      });
    },

    *updateBatchPolicy({data}, { call, put }) {
       yield call(shareService.updateBatchPolicy, { data });
    },

  },
  

  reducers: {

    initGroups(state, { payload }) {
      console.log('initGroups..');
      return {
        ...state,
        groupsData: payload.data,
      };
    },
  },
}