import sarlaryService from '../services/sarlaryService';
import { approvedSalaryResult } from '../services/approvedList';
import { expressSalary,downloadSalary } from '../services/api';

export default {
  namespace: 'sarlaryMdl',
  state: {
    sarlaryData: [],
    sarlaryRTdata:[],
    collectSalary:{},
    allSalary:[],       //无须权限查看的薪酬统计
    positionData:[]
  },

  effects: {
    //查询所有劳务派遣公司
    *queryPrivSalaryResult({data}, { call, put }){
        const result = yield call(sarlaryService.queryPrivSalaryResult, {
        data:data,
      });
      yield put({
        type: 'initSarlaryResult',
        payload: result,
      });
    },

    //薪酬调整
    *updateSalaryResult({data}, { call, put }){
        const result = yield call(sarlaryService.updateSalaryResult, {
        data:data,
      });
    },
    *updateStatus({ payload }, { call }) {
      const para= {"data":{...payload}}
      console.log(para)
       const response = yield call(approvedSalaryResult, para);
       return response
     },
    //  下载
     *exportSalaryResult({data}, { call, put }){
      const impData = {data: data};
      console.log('exportSalaryResult called...');
      console.log(impData);
      const response = yield call(downloadSalary, impData);
      const fileName = "人员薪酬表.xls"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    //审核
    *approvedSalaryResult({data}, { call, put }){
        const result = yield call(sarlaryService.approvedSalaryResult, {
        data:data,
      });
    
    },

    //查询打回/审核通过的数据
    *querySalaryRTData({data}, { call, put }){
        console.log('querySalaryResult...');
        console.log(data);
        const result = yield call(sarlaryService.querySalaryResult, {
        data:data,
      });
      
      console.log(result);
      yield put({
        type: 'initSarlaryData',
        payload: result,
      });
    },

    //查看汇总金额
    *collectSalaryResult({data}, { call, put }){
        const result = yield call(sarlaryService.collectSalaryResult, {
        data:data,
      });
      yield put({
        type: 'initCollectSalary',
        payload: result,
      });
    },

    //导出汇总金额
    *downloadSalary({data},{call,put}) {
      const impData = {data: data};
      console.log(impData);
      const response = yield call(expressSalary, impData);

      const fileName = "薪酬汇总金额.xls"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
    *querySalaryResult({data}, { call, put }){
        const result = yield call(sarlaryService.querySalaryResult, {
        data:data,
      });
      yield put({
        type: 'allSalaryData',
        payload: result,
      });
    },
     //查看岗位
    *searchPositions({data}, { call, put }){
        console.log('-------岗位是：--------'+ data)
        const result = yield call(sarlaryService.searchPositions, {
        data:data,
      });
      yield put({
        type: 'initPosition',
        payload: result,
      });
    },
   
  },

  
  reducers: {
    
    initSarlaryResult(state, action) {
      return {
        ...state,
        sarlaryData: action.payload.data,
      };
    },
    
    initSarlaryData(state, action) {
      console.log('initSarlaryData....');
      return {
        ...state,
        sarlaryRTdata: action.payload.data,
      };
    },

    initCollectSalary(state, action) {
      return {
        ...state,
        collectSalary: action.payload.data,
      };
    },

    allSalaryData(state, action) {
      return {
        ...state,
        allSalary: action.payload.data,
      };
    },

    initPosition(state, action) {
      return {
        ...state,
        positionData: action.payload.data,
      };
    },
    
  }
}
