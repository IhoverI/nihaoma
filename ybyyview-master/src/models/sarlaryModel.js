import sarlaryService from '../services/sarlaryService';
import shareService from '../services/shareService';
import { filterEmp, downloadSalary,exportPolicyExcel } from '../services/api';
import { isMoment } from 'moment';
import moment from 'moment';

export default {
  namespace: 'salaryModel',
  state: {
    titleList: [],
    data: {},
    factoryHistoryList: [],
  },

  effects: {

    //查询所有劳务派遣公司
    *selectSalaryTitle({data}, { call, put }){
      const result = yield call(sarlaryService.querySalaryTitle, {
        data:data,
      });
      yield put({
        type: 'initSalaryTitle',
        payload: result,
      });
    },
    *updateBatchPolicy({data}, { call, put }) {
      yield call(shareService.updateBatchPolicy, { data });
   },

    // 清理
    *clearSalaryResult({data}, { call, put }){
      const result = yield call(sarlaryService.clearSalaryResult, {
        data:data,
      });
      return result
    },

    *searchFactoryHistory({data}, { call, put }){
      console.log(data);
      const result = yield call(sarlaryService.querySalaryInfo, {
        data: data,
      });
      yield put({
        type: 'initFactoryHistory',
        payload: result,
      });
    },

   *calculateSalary({data}, { call, put }){
      console.log('calculateSalary called..');
      console.log(data);
      const result = yield call(sarlaryService.calculateSalary, {
        data: data,
      });
      console.log(result);
      // yield put({
      //   type: 'initFactoryHistory',
      //   payload: result,
      // });
    },

    *exportSalaryResult({data}, { call, put }){
      const impData = {data: data};
      console.log('exportSalaryResult called...');
      console.log(impData);
      const response = yield call(downloadSalary, impData);
      const fileName = "人员薪酬表.xls"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
    
    
    // 筛选功能--导出
    *downloadFactor({data},{call,put}) {
      const impData = {data: data};
      console.log(impData);
      const response = yield call(filterEmp, impData);
      // console.log(response);
      // const objectURL = window.URL.createObjectURL(response);
      // const a = document.createElement('a'); 
      // const url = window.URL.createObjectURL(response);  
      // a.href = url; 
      // a.download = "考核人员筛选清单.xls"; 
      // a.click(); 
      // window.URL.revokeObjectURL(url);

      const fileName = "考核人员筛选清单.xls"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
   
    *exportPolicyExcel({data},{call,put}){
      const response = yield call(exportPolicyExcel, {data});
      const fileName = "保费数据.xls"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    }
  },

  reducers: {
    // 薪酬筛选
    initSalaryTitle(state, {payload}) {
      console.log('initSalaryTitle called..');
      const importFactorTitleList = [];
      const  titleList = payload.data;
      for(let i = 0; i < titleList.length; i ++){
        let title = titleList[i];
        let obj = {};
        obj = { ...title};
        obj.key = title.field;
        obj.width = '300px';
        obj.dataIndex = title.field;
        importFactorTitleList.push(obj);
       // obj.
      }
      return {
        ...state,
        data: payload.data,
        titleList: importFactorTitleList,
      };
    },

    // 查询导入历史记录
    initFactoryHistory(state, {payload}) {
      console.log('initFactoryHistory called..');
     
      console.log(payload.data);
      let resultList = [];
      for(let i = 0; i < payload.data.length; i ++){
        let result = payload.data[i];
        result.key = i + '';
        resultList.push(result);
      }
      return {
        ...state,
        factoryHistoryList: resultList,
      };
    },

    // 查询导入历史记录
    initClearSalaryResult(state, {payload}) {
      console.log('initClearSalaryResult called..');

      return {
        ...state,
      };
    },
  }
}
