import { receiptQuery, updateReceipt } from '../services/api';
import { message } from 'snk-web';

export default {
  namespace: 'receiptManage',

  state: {
    ReceiptQueryData:{},
    UpdateResp: {}
  },

  effects: {
    // 回执查询
    *receiptQuery({ payload }, { call, put }) {
        const response = yield call(receiptQuery, payload);
        0 == response.code ? message.success('查询成功！') : message.error(response.message);
        yield put({
          type: 'save',
          payload: {
            ReceiptQueryData: response
          },
        });
    },
      
    // 更新回执
    *updateReceipt({ payload }, { call, put }) {
      const response = yield call(updateReceipt, payload);
      yield put({
          type: 'save',
          payload: {
            UpdateResp: response
          },
        });
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload
      };
    },
  },
};
