import {SearchSysCode,SearchJobList,AddJob,SearchJobBufferList,AddJobBuffer,DeleteJob,UpdateJob,UpdateRow,UpdateJobBuffer} from '../services/quartzManage';
import { checkResponse } from '../utils/utils';

export default {
  namespace: 'quartzmanage',

  state: {
    JobListData:{},
    JobBufferListData:{},
    SysCodeData: []
  },

  effects: {
    *reset({ payload }, { call, put }) {
      yield put({
        type: 'save',
        payload: {
          JobListData:{},
          JobBufferListData:{},
        },
      });
    },
    *SearchJobBufferList({ payload }, { call, put }) {
      const response = yield call(SearchJobBufferList, payload);
      yield put({
        type: 'save',
        payload: {
          JobBufferListData:response.data||{}
        },
      });
    },
    *UpdateJobBuffer({ payload }, { call, put,select }) {
      const response = yield call(UpdateJobBuffer, payload);
      checkResponse(response);
      const bufferId = payload.whereMap.bufferId;
      const updateMap = payload.updateMap;
      //bufferId
      const JobBufferListData = yield select(state => state.quartzmanage.JobBufferListData);
      if(JobBufferListData&&JobBufferListData.list){
        const JobBufferList = JobBufferListData.list;
        for(let i=0,j=JobBufferList.length;i<j;i++){
          const item = JobBufferList[i];
          if(item.bufferId === bufferId){
            for(var key in updateMap){
              item[key] = updateMap[key];
            }
            break;
          }
        }
        yield put({
          type: 'save',
          payload: {
            JobBufferListData,
          },
        });
      }
    },
    *UpdateRow({ payload }, { call, put }) {
      const response = yield call(UpdateRow, payload);
      const JobListData = yield select(state => state.quartzmanage.JobListData);
      if(JobListData&&JobListData.list){
        const JobList = JobListData.list;
        for(let i=0,j=JobList.length;i<j;i++){
          const item = JobList[i];
          if(item.jobId === response.data.jobId){
            JobList[i] =  response.data;
            break;
          }
        }
        yield put({
          type: 'save',
          payload: {
            JobListData:JobListData
          },
        });
      }
    },
    *AddJob({ payload }, { call, put }) {
      const response = yield call(AddJob, payload);
      checkResponse(response);
    },
    *UpdateJob({ payload }, { call, put }) {
      const response = yield call(UpdateJob, payload);
      checkResponse(response);
    },
    *DeleteJob({ payload }, { call, put, select }) {
      const response = yield call(DeleteJob, payload);
      checkResponse(response);
      const JobListData = yield select(state => state.quartzmanage.JobListData);
      if(JobListData&&JobListData.list){
        const JobList = JobListData.list;
        for(let i=0,j=JobList.length;i<j;i++){
          const item = JobList[i];
          if(item.jobId === payload.jobId){
            item.isValid = "0";
            break;
          }
        }
        yield put({
          type: 'save',
          payload: {
            JobListData:JobListData
          },
        });
      }
    },
    *AddJobBuffer({ payload }, { call, put }) {
      const response = yield call(AddJobBuffer, payload);
      checkResponse(response);
    },
    *SearchJobList({ payload }, { call, put }) {
      const response = yield call(SearchJobList, payload);
      yield put({
        type: 'save',
        payload: {
          JobListData:response.data||{}
        },
      });
    },
    *SearchSysCode({ payload }, { call, put }) {
      const response = yield call(SearchSysCode, payload);
      yield put({
        type: 'save',
        payload: {
          SysCodeData:response.data||{}
        },
      });
      return
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload
      };
    },
  },
};
