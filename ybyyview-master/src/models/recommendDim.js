import recommendsService from '../services/recommends';
import { isMoment } from 'moment';
import moment from 'moment';

export default {
  namespace: 'recommendDim',

  state: {
    list: [],
    data: [],
    pagination: {},
    selectedRowData: {},
    outerList: [],
    chaList: [],
    carModelsData: [],
  },

  effects: {
    *setupSelectedRowData({data}, { call, put }) {
      yield put({
        type: 'setupSelectedRow',
        payload: data,
      });
    },
    
    *searchChaCodeByClm({data}, { call, put }) {
      let result = {};
      console.log("searchChaCodeByClm called...");
      console.log(data);
      result = yield call(recommendsService.searchChaCodeByClm, {
        data
      });
     
      console.log(result);
      yield put({
        type: 'initSearchChaCodeByClm',
        payload: result,
      });
    },

    *searchOuterCodeByChaCode({data}, { call, put }) {
      let result = {};
      console.log("searchChaCodeByClm called...");
      console.log(data);
      result = yield call(recommendsService.searchOuterCodeByChaCode, {
        data
      });
     
      console.log(result);
      yield put({
        type: 'initSearchOuterCodeByChaCode',
        payload: result,
      });
    },

    *searchOuterByClm({data}, { call, put }) {
      let result = {};
      console.log("searchOuterByClm called...");
      console.log(data);
      result = yield call(recommendsService.searchOuterByClm, {
        data
      });
     
      console.log(result);
      yield put({
        type: 'initSearchOuterByClm',
        payload: result,
      });
    },

    *searchCarModels({data}, { call, put }) {
      console.log("searchCarModels called...");
      console.log(data);
      const result = yield call(recommendsService.searchCarModel, {
        data
      });
     
      console.log(result);
      yield put({
        type: 'initCarModels',
        payload: result,
      });
    },


    *searchChaByOuter({data}, { call, put }) {
      let result = {};
    
      result = yield call(recommendsService.searchChaByOuter, {
        data
      });
     
      console.log(result);
      yield put({
        type: 'initSearchChaByOuter',
        payload: result,
      });
    },

    *searchClm({data}, { call, put }) {
      let result = {};
      if(data){
        result = yield call(recommendsService.searchClm, {
          data
        });
      }else{
        result = yield call(recommendsService.searchClm, {
        
        });
      }

      const data3 = {
        "code": 0,
        "message": "Success",
        "data": {
            "pageNum": 1,
            "pageSize": 10,
            "size": 1,
            "startRow": 1,
            "endRow": 1,
            "total": 1,
            "pages": 1,
            "list": [
                {
                    "key": "f37f8a0bca2548d692e91b8d749bbbce",
                    "operDeptCode": "0102150502",
                    "thirdPartName": "广东分公司佛山市桂城营销服务部",
                    "provinceCode": "150700",
                    "thirdPartCode": "110000",
                    "clmCode": "110000",
                    "occurCityName": "北京市",
                    "occurDistrictName": "呼伦贝尔市",
                    "updatedUser": "李思思",
                    "updatedTime": "2018-05-25 10:31:02",
                    "approveUser": "旺才",
                    "approveTime": "2018-05-25 16:55:33",
                    "status": "0",
                    "ruleName": "你好",
                    "approveStatus": "1",
                    "bgnTime": "2018-12-23 16:55:33",
                    "endTime": "2018-12-23 16:55:33",
                    "approveContent": null,
                    "approveResult": null,
                    "contactName": null,
                    "contactPhone": null,
                    "pageNum": 1,
                    "pageSize": 10
                },
                {
                  "key": "f37f8a0bca2548d692e91b8d749bbbce2",
                  "operDeptCode": "0102150502",
                  "thirdPartName": "广东分公司佛山市桂城营销服务部",
                  "provinceCode": "150700",
                  "thirdPartCode": "110000",
                  "clmCode": "110000",
                  "occurCityName": "北京市",
                  "occurDistrictName": "呼伦贝尔市",
                  "updatedUser": "李思思",
                  "updatedTime": "2018-05-25 10:31:02",
                  "approveUser": "旺才",
                  "approveTime": "2018-05-25 16:55:33",
                  "status": "0",
                  "ruleName": "你好",
                  "approveStatus": "1",
                  "bgnTime": "2018-12-23 16:55:33",
                  "endTime": "2018-12-23 16:55:33",
                  "approveContent": null,
                  "approveResult": null,
                  "contactName": null,
                  "contactPhone": null,
                  "pageNum": 1,
                  "pageSize": 10
              }
            ],
            "prePage": 0,
            "nextPage": 0,
            "isFirstPage": true,
            "isLastPage": true,
            "hasPreviousPage": false,
            "hasNextPage": false,
            "navigatePages": 8,
            "navigatepageNums": [
                1
            ],
            "navigateFirstPage": 1,
            "navigateLastPage": 1,
            "firstPage": 1,
            "lastPage": 1
        },
        "timestamp": 1527299595041
    };
      console.log("searchClm called...");
      console.log(result);
      //data = data1.data;
      // const response = yield call(queryActivities);
      yield put({
        type: 'initSearchClm',
        payload: result,
      });
    },

    *clearClmDim({data}, { call, put }) {
      yield put({
        type: 'clearClmData',
        selectedRowData: {},
        list: [],
      });
    },


    *searchProvinceData(_, { call, put }) {
      console.log("model searchProvinceData called...");
      const result = yield call(recommendsService.searchProvinceData, {
      
      });
      
      console.log(result);
      yield put({
        type: 'initProvinceData',
        payload: result,
      });
    },

    *searchCityByPrveData({data}, { call, put }) {
      console.log("model searchCityByPrveData called...");
      const result = yield call(recommendsService.searchCityByPrveData, {
        data
      });
      
      console.log(">>>>>>>");
      console.log(result);
      yield put({
        type: 'initCityByPrveData',
        payload: result,
      });
    },

    *addRecommendMain({payload}, { call, put }) {
      console.log("model addRecommendMain called...");
      for(var i in payload.addMainRule){
        console.log(i); // 属性
        var val = payload.addMainRule[i];
        if(isMoment(val)){
          console.log(val); // 属性
          console.log(moment(val).format("YYYY-MM-DD HH:mm:ss"));
          payload.addMainRule[i] = Date.parse(moment(val).format("YYYY-MM-DD HH:mm:ss")) + "";
          console.log("时间戳为：" + payload.addMainRule[i]);
        }
      }
      const data = payload.addMainRule;
      const result = yield call(recommendsService.addRecommendMain, {
        data
      });
    },

    *editPosition({data}, { call, put }) {
      console.log("model editRank called...");
      console.log(data);
      const data1 = yield call(positionsService.editPosition, {
        data
      });
    },
    
    *editRank({data}, { call, put }) {
      console.log("model editRank called...");
      console.log(data);
      const data1 = yield call(positionsService.editRank, {
        data
      });
    },

    *addRank({data}, { call, put }) {
      console.log("model addRank called...");
      console.log(data);
      const data1 = yield call(positionsService.addRank, {
        data
      });
    },
  },

  reducers: {
    initSearchClm(state, {payload}) {
      console.log("model initSearchClm called...");
      console.log(payload);
      
      return {
        ...state,
        data: payload.data,
        list: payload.data.list,
      };
    },
    initProvinceData(state, action) {
      return {
        ...state,
        provinceData: action.payload.data,
      };
    },
    initCityByPrveData(state, action) {
      return {
        ...state,
        cityData: action.payload.data,
      };
    },
    clearClmData(state, action){
      return{
        list: [],
        data: [],
        pagination: {},
        selectedRowData: {},
      };
    },
    setupSelectedRow(state, {payload}){
      console.log("setupSelectedRow called...");
      console.log(payload);
      return{
        ...state,
        selectedRowData: payload,
      };
    },
    initSearchOuterByClm(state, {payload}){
      console.log("initSearchOuterByClm called...");
      console.log(payload);
      return{
        ...state,
        outerList: payload.data,
      };
    },

     // 新：初始化返回渠道信息
    initSearchChaCodeByClm(state, {payload}){
      console.log("initSearchChaCodeByClm called...");
      console.log(payload);
      return{
        ...state,
        chaList: payload.data,
      };
    },

    // 新：初始化返回外部网点信息
    initSearchOuterCodeByChaCode(state, {payload}){
      console.log("initSearchOuterCodeByChaCode called...");
      console.log(payload);
      return{
        ...state,
        outerList: payload.data,
      };
    },

    initSearchChaByOuter(state, {payload}){
      console.log("initSearchChaByOuter called...");
      return{
        ...state,
        chaList: payload.data,
      };
    },
    initCarModels(state, { payload }) {
      console.log("initCarModels called...");
      console.log(payload);
      return {
        ...state,
        carModelsData: payload.data,
      };
    },
  },
};
