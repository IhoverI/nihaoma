import { onlineReport, exportOnlineReport } from '../services/api';
import { message } from 'snk-web';

export default {
  namespace: 'reportModels',

  state: {
    onlineReportResp:{},
  },

  effects: {
    // 线上数据-查询
    *onlineReport({ payload }, { call, put }) {
        const response = yield call(onlineReport, payload);
        0 == response.code ? message.success('查询成功！') : message.error(response.message);
        yield put({
          type: 'save',
          payload: {
            onlineReportResp: response
          },
        });
    },
 
    // 线上数据-导出excel
    *exportOnlineReport({ payload }, { call, put }) {
      const response = yield call(exportOnlineReport, payload);
      const fileName = `线上数据.xlsx`;
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a');
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload
      };
    },
  },
};
