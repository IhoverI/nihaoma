import {
	message,
} from 'snk-web';
import {
    downTemplate,
    dataImport,
    exportWorkData,
    queryImportData,
} from '../services/customerService';
export default {
  namespace: 'importDataModel',
  state: {
        isSecPlyDate: false,
        newValueAddData: {}, // 新单数据提取
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
  effects: {
    // 数据导入模板下载
    *downTemplate({ payload }, { call, put }) {
      console.log('xiazai',payload);
      
      let response = yield call(downTemplate,payload);
      let fileName = "数据导入模板下载.xlsx"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
          const a = document.createElement('a'); 
          const url = window.URL.createObjectURL(response);  
          a.href = url; 
          a.download = fileName; 
          a.click(); 
          window.URL.revokeObjectURL(url);
      }
    },
    //数据导入
    *dataImport( { payload }, { call }) {
      console.log('payload',payload);
      try {
        const result= yield call(dataImport, payload);
        return result;
      } catch (data) {
        return data;
      }  
    },
    //数据导出
    *exportData({ payload }, { call, put }) {
      console.log('导出',payload);
      let response = yield call(exportWorkData,payload);
      let fileName = "回访数据.xlsx"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
          const a = document.createElement('a'); 
          const url = window.URL.createObjectURL(response);  
          a.href = url; 
          a.download = fileName; 
          a.click(); 
          window.URL.revokeObjectURL(url);
      }
    },
    //查询
    *queryImportData( { payload }, { call, put }) {
      return yield call(queryImportData, payload);
    },
  }
}
