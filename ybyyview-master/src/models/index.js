
const context = require.context('./', false, /\.js$/);
export default function registerModels(app) {
  context
  .keys()
  .filter(item => item !== './index.js')
  .map(key => {app.model(context(key))});
}
