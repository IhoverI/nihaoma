import personTempCfgService from '../services/personTempCfgService';

export default {
  namespace: 'personTempCfgMdl',
  state: {
    personData: [],
    addData:[]
  },

  effects: {
    //查询所有劳务派遣公司
    *selectLmporFactor({data}, { call, put }){
      const result = yield call(personTempCfgService.selectLmporFactor, {
        data:data,
      });
      yield put({
        type: 'initPersonTemp',
        payload: result,
      });
    },
    *insertImportFactor({data}, { call, put }){
      const result = yield call(personTempCfgService.insertImportFactor, {
        data:data,
      });
      yield put({
        type: 'initAddFactorData',
        payload: result,
      });
    },
    *deleteImprotFactor({data}, { call, put }){
      const result = yield call(personTempCfgService.deleteImprotFactor, {
        data:data,
      });
      
    },
    *updateImportFactor({data}, { call, put }){
      const result = yield call(personTempCfgService.updateImportFactor, {
        data:data,
      });
      
    },

  },

  reducers: {
    //初始化劳务派遣公司数据
    initPersonTemp(state, action) {
      return {
        ...state,
        personData: action.payload.data
      };
    },
    initAddFactorData(state, action) {
      return {
        ...state,
        addData: action.payload.data
      };
    },
    

  }
}
