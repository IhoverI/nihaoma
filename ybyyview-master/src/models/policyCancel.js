import {
  queryRulePolicyCancel,
  submitRulePolicyCancel,
  queryRulePolocyList,
  queryBackPrm,
  cancelSubmit
} from '../services/api';
import { message } from 'snk-web';

export default {
  namespace: 'policyCancel',

  state: {
    data: {
      baseDto: [],
      payMap: [],
      appDto: [],
      recDto: [],
      thirdDto: [],
      bnfDto: [],
      plyImageDto: [],      
      list: [],
      pagination: {},
    },
    search: {},
    queryBackPrm: {},
    executeResp: {},
    cancelSubmitResp: {},
  },

  effects: {
    // 注退搜索
    *fetchPolicyCancel({ payload }, { call, put }) {
      const response = yield call(queryRulePolicyCancel, payload);
      if (0 !== response.code) {message.error(response.message);}
      yield put({
        type: 'save',
        payload: response,
      });
    },
    // 注退
    *submitPolicyCancel({ payload }, { call, put }) {
      const response = yield call(submitRulePolicyCancel, payload);
      yield put({
        type: 'execute',
        payload: response,
      });
    },
    // 查询实退保费
    *queryBackPrm({ payload }, { call, put }) {
      const response = yield call(queryBackPrm, payload);
      if (0 !== response.code) {message.error(response.message);}
      yield put({
        type: 'initBackPrm',
        payload: response,
      });
    },
    // 撤消申请
    *cancelSubmit({ payload }, { call, put }) {
      const response = yield call(cancelSubmit, payload);
      yield put({
        type: 'cancelSubmitPut',
        payload: response,
      });
    },
    // 查询保单详情
    *fetchPolicyList({ payload }, { call, put }) {
      const response = yield call(queryRulePolocyList, payload);
      yield put({
        type: 'save',
        payload: response,
      });
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        search: payload
      };
    },
    initBackPrm(state, { payload }) {
      return {
        ...state,
        queryBackPrm: payload
      };
    },
    execute(state, { payload }) {
      return {
        ...state,
        executeResp: payload
      };
    },
    cancelSubmitPut(state, { payload }) {
      return {
        ...state,
        cancelSubmitResp: payload
      };
    },
  },
};
