import {
  SubmitSelfchannelForm,
  SubmitSelfUpdateChannelForm,
  getUserInfo,
  getItemAccOccupationCode,
  getCountryCode,
  getRelationBetweenCode,
} from '../services/selfchannelForm';
import { checkResponse } from '../utils/utils';
import { Modal, message } from 'snk-web';
export default {
  namespace: 'selfchannelForm',

  state: {
    data: {},
    getUserInfo: {},
    beneficiaryList: {},
    CountryCode: [],
    ItemAccOccupationCode: [],
    relationBetweenCode: [],
  },

  effects: {
    *submit({ payload = {} }, { call, put, select }) {
      const { policyMapInfo } = yield select(state => state.searchlist);
      const { benfType, beneficiaryList } = payload;
      let bList = beneficiaryList;
      if (benfType === '1') {
        let totalNBenfProp = 0;
        bList = [];
        const { beneficiaryList } = yield select(state => state.selfchannelForm);
        for (let key in beneficiaryList) {
          const { nBenfProp } = beneficiaryList[key];
          bList = [...bList, beneficiaryList[key]];
          totalNBenfProp += nBenfProp;
        }
        if (totalNBenfProp > 100) {
          message.warning('受益比例之和不能超过100%', 5);
          return false;
        }
      }
      let totalNBenfProp = 0;
      let handleSubmitFormData = payload;
      let response;
      if(policyMapInfo.cPlyAppNoEditable) {
        handleSubmitFormData = {...handleSubmitFormData, cPlyAppNo: policyMapInfo.cPlyAppNoEditable, beneficiaryList: bList};
        response = yield call(SubmitSelfUpdateChannelForm, handleSubmitFormData);
        checkResponse(response || {});
        Modal.success({
          title: '',
          content: `修改成功！投保单号：${response.data}`,
        });
        yield put({
          type: 'searchlist/save',
          payload: {
            policyMapInfo: {...payload},
          }
        })
        return response;
      } else {
        response = yield call(SubmitSelfchannelForm, {...payload, beneficiaryList: bList});
        checkResponse(response || {});
        Modal.success({
          title: '',
          content: `提交成功！投保单号：${response.data}`,
        });
        yield put({
          type: 'searchlist/save',
          payload: {
            policyMapInfo: {...payload},
          }
        })
        return response;
      }
    },

    *getUserInfo({ payload }, { call, put }) {
      const response = yield call (getUserInfo, payload);
      yield put ({
        type: 'save',
        payload: {
          getUserInfo : response,
        },
      })
    },
  
    *checkCodeType(action, { put, call }) {
      const { data: ItemAccOccupationCode } = yield call(getItemAccOccupationCode);
        const { data: CountryCode } = yield call(getCountryCode);
        const { data: relationBetweenCode } = yield call(getRelationBetweenCode);
        yield put({
          type: 'save',
          payload: {
            CountryCode,
            ItemAccOccupationCode,
            relationBetweenCode,
          }
        });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 添加受益人信息
    savebeneficiaryList({beneficiaryList, ...state}, { payload }) {
      return {
        ...state,
        beneficiaryList: {...beneficiaryList, ...payload},
      }
    },
    // 删除受益人信息
    deletebeneficiaryList({beneficiaryList, ...state}, { payload }) {
      const { index } = payload;
      delete beneficiaryList[index];
      return {
        ...state,
        beneficiaryList,
      }
    },
  },
};
