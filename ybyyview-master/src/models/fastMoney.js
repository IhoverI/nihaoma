import {
  queryRuleFastMoney,
  saveRuleFastMoney,
  submitRuleFastMoney,
  queryRulePolocyList,
  fetchBindata,
} from '../services/api';
import { message } from 'snk-web';

export default {
  namespace: 'fastMoney',

  state: {
    fastData: {},
    policyData: {},
    blobURL:{}
  },

  effects: {
    *fetchFastMoney({ payload }, { call, put }) {
      const response = yield call(queryRuleFastMoney, payload);
      let alertMessage = response.message;
      0 === response.code ? message.success('查询成功！') : message.error(alertMessage);
      yield put({
        type: 'save',
        payload: {fastData:response},
      });
    },
    *saveFastMoney({ payload }, { call, put }) {
      const response = yield call(saveRuleFastMoney, payload);
      let alertMessage = response.message;
      0 === response.code ? message.success('暂存成功！') : message.error(alertMessage);
    },
    *submitFastMoney({ payload }, { call, put }) {
      const response = yield call(submitRuleFastMoney, payload);
      let alertMessage = response.message;
      '0' == response.code ? message.success('提交成功！') : message.error(alertMessage);
    },
    *submitDownload({ payload }, { call, put }) {
      const response = yield call(fetchBindata, payload);
      const objectURL = URL.createObjectURL(response);
      const blobURL = {
        blobURL: objectURL
      }
      yield put({
        type: 'save',
        payload: blobURL,
      });
    },
    *fetchPolicyList({ payload }, { call, put }) {
      const response = yield call(queryRulePolocyList, payload);
      0 === response.code ? message.success('查询成功！') : message.error(response.message);
      yield put({
        type: 'save',
        payload: {policyData:response},
      });
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
