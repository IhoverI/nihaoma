import recommendsService from '../services/recommends';
import { isMoment } from 'moment';
import moment from 'moment';

export default {
  namespace: 'recommend',
  state: {
    list: [],
    data: {},
    pagination: {},
    provinceData: [],
    cityData: [],
    districtData: [],
    users: [],
    mainId: '',
    ruleId: '',
  },

  effects: {
    *searchRecommend({data}, { call, put }) {
      let result = {};

      // 转换成服务器接收的时间戳
      for(var i in data){
        var val = data[i];
        if(isMoment(val)){
          console.log(val); // 属性
          console.log(moment(val).format("YYYY-MM-DD HH:mm:ss"));
          data[i] = Date.parse(moment(val).format("YYYY-MM-DD HH:mm:ss")) + "";
          console.log("时间戳为：" + data[i]);
        }
      }

      if(data){
        result = yield call(recommendsService.searchRecommend, {
          data
        });
      }else{
        result = yield call(recommendsService.searchRecommend, {
        
        });
      }
      
      console.log(result);
      //data = data1.data;
      // const response = yield call(queryActivities);
      yield put({
        type: 'initRecommendMain',
        payload: result,
      });
    },

    *approveMain({data}, { call, put }){
      console.log("model approveMain called...");
      console.log(data);

      const result = yield call(recommendsService.approveMain, {
        data:data,
      });
    },

    *addRule({data}, { call, put }){
      console.log("model addRule called...");
      console.log(data);
      let saveData = {
        ...data,
      }
      console.log(">>>>>>>>>>>>>>&&&&&&&&&&&&&&>>>>");
      if(saveData.pushRepairRuleDto){
        saveData.pushRepairRuleDto.clmId = data.pushRepairRuleDto.thirdPartCode;
      }
      
      const result = yield call(recommendsService.addRule, {
        data:saveData,
      });
      yield put({
        type: 'initAddRuleResult',
        payload: result,
      });
    },


    *editRule({data}, { call, put }){
      console.log("model EditRule called...");
      let saveData = {
        ...data,
      }
      if(saveData.pushRepairRuleDto){
        saveData.pushRepairRuleDto.clmId = data.pushRepairRuleDto.thirdPartCode;
      }
      const result = yield call(recommendsService.editRule, {
        data:saveData,
      });
    },

    *searchProvinceData(_, { call, put }) {
      console.log("model searchProvinceData called...");
      const result = yield call(recommendsService.searchProvinceData, {
      
      });

      console.log(result);
      yield put({
        type: 'initProvinceData',
        payload: result,
      });
    },


    *findUsers({data}, { call, put }){
      console.log("model findUsers called...");
      const result = yield call(recommendsService.searchUsers, {
        data
      });

      yield put({
        type: 'initFindUsers',
        payload: result,
      });
    },

    *searchCityByPrveData({data}, { call, put }) {
      console.log("model searchCityByPrveData called...");
      const result = yield call(recommendsService.searchCityByPrveData, {
        data
      });
      console.log(result);
      yield put({
        type: 'initCityByPrveData',
        payload: result,
      });
    },

    *searchDistrictByCityData({data}, { call, put }) {
      console.log("model searchDistrictByCityData called...");
      const result = yield call(recommendsService.searchDistricData, {
        data
      })

      console.log(">>>>>>>根据市查区");
      console.log(result);
      yield put({
        type: 'initDistrictData',
        payload: result,
      });
    },

    *addRecommendMain({payload}, { call, put }) {
      console.log("model addRecommendMain called...");
      console.log(payload.addMainRule);
      for(var i in payload.addMainRule){
        console.log(i); // 属性
        var val = payload.addMainRule[i];
        if(isMoment(val)){
          console.log(val); // 属性
          console.log(moment(val).format("YYYY-MM-DD HH:mm:ss"));
          payload.addMainRule[i] = Date.parse(moment(val).format("YYYY-MM-DD HH:mm:ss")) + "";
          console.log("时间戳为：" + payload.addMainRule[i]);
        }
        if(i == "updatedTime"){
          payload.addMainRule[i] = Date.parse(val) + "";
          console.log("时间戳为：" + payload.addMainRule[i]);
        }
      }
      const data = payload.addMainRule;
      const result = yield call(recommendsService.addRecommendMain, {
        data
      });
      console.log(result);
      yield put({
        type: 'initAddRecommendMainResult',
        payload: result,
      });
    },

    *deleteRule({data}, { call, put }) {
      console.log("model deleteRule called...");
      const result = yield call(recommendsService.deleteRule, {
        data
      });
    },

    *editRecommendMain({data}, {call}) {
      console.log("model editRecommendMain called...");
      console.log(data);
      // 转换成服务器接收的时间戳
      for(var i in data){
        var val = data[i];
        if(isMoment(val)){
          console.log(val); // 属性
          console.log(moment(val).format("YYYY-MM-DD HH:mm:ss"));
          data[i] = Date.parse(moment(val).format("YYYY-MM-DD HH:mm:ss")) + "";
          console.log("时间戳为：" + data[i]);
        }
      }
      const result = yield call(recommendsService.updatePushRepairMain, {
        data
      });
    },

   
  },

  reducers: {
    initRecommendMain(state, action) {
      return {
        ...state,
        data: action.payload.data,
        list: action.payload.data.list,
      };
    },
    initProvinceData(state, action) {
      return {
        ...state,
        provinceData: action.payload.data,
      };
    },
    initCityByPrveData(state, action) {
      return {
        ...state,
        cityData: action.payload.data,
      };
    },
    initDistrictData(state, {payload}) {
      return {
        ...state,
        districtData: payload.data,
      };
    },
    initFindUsers(state, { payload }) {
      return {
        ...state,
        users: payload.data,
      };
    },
    initAddRecommendMainResult(state, { payload }) {
      console.log("initAddRecommendMainResult called..");
      console.log(payload.data);
      return {
        ...state,
        mainId: payload.data,
      };
    },
    initAddRuleResult(state, { payload }){
      return {
        ...state,
        ruleId: payload.data,
      }
    },
  },
};
