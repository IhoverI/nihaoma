import {messageCount, queryMessage, deletes, queryTopMessage, update, failPolicy, updateFailPolicy,  getMessage} from '../services/api';
import { message } from 'snk-web';

export default {
  namespace: 'messageManage',

  state: {
    count: {},
    queryData:[],
    queryTopData: [],
    updateData: [],
		readData: {},
		messageFormData:{},
  },

  effects: {
    *messageCount(payload, { call, put }) {
        const response = yield call(messageCount);
        if (response) {
            yield put({
                type: 'save',
                payload: {
                    count: response.data,
                },
            });
        }
    },

    *queryMessage(payload,{call,put}) {
				const { payload: rePayload } = payload;
				const response = yield call(queryMessage,payload);
        yield put({
					type: 'save',
					payload: {
						messageFormData: {...rePayload},
					},
        });
        if (response) {
            yield put({
                type: 'save',
                payload: {
                    queryData: response.data,
                },
            });
        }
    },

    *deletes(payload,{call,put}) {
        const response = yield call(deletes,payload);
        if(response){
            if (response.code === 0) {
                message.success('删除成功');
            } else {
                message.warning('删除失败');
            }
        }
    },

    *queryTopMessage(payload,{call,put}) {
        const response = yield call(queryTopMessage,payload);
        if (response) {
            yield put({
                type: 'save',
                payload: {
                    queryTopData: response.data,
                },
            });
        }
    },

    // 更新数据置顶
    *update(payload,{call,put}) { 
        const response = yield call(update,payload);
        if (response) {
            yield put({
                type: 'save',
                payload: {
                    queryTopData: response.data,
                },
            });
            if (response.code === 1) {
                message.error(response.message,1);
            } else {
                message.success(response.message,1)
            }
        }
    },

    // 查看问题数据处理数据
    *failPolicy(payload,{call,put}) {
        const response = yield call (failPolicy,payload);
        if (response) {
            if (response.data) {
                if (response.data.pageInfo) {
                    if (response.data.pageInfo.list.length === 0) {
                        message.warning('查无数据');
                    }
                }
            }
            yield put({
                type: 'save',
                payload: {
                    failPolicyData: response.data,
                    pageInfo: response.data.pageInfo,
                },
            });
        }
    },

    // 修改处理数据
    *updateFailPolicy(payload,{call,put}) {
        const response = yield call(updateFailPolicy,payload);
        if (response) {
            if (response.code === 0){
                message.success(response.message);
            } else {
                message.error(response.message);
            }
        }
    },

    // 根据id查询信息
    *getMessage(payload,{call,put}) {
        const response = yield call (getMessage,payload);
        if (response) {
            yield put ({
                type: 'save',
                payload: {
                    readData: response.data,
                },
            });
        }
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
 