import recommendsService from '../services/recommends';
import { isMoment } from 'moment';
import moment from 'moment';

export default {
  namespace: 'recommendRule',
  state: {
    list: [],
    data: [],
    addRuleList: [],
    pagination: {},
  },

  effects: {
    *addNewRule({data}, { call, put }) {
      
      let ruleContent = {
        key: '',
        mainId: data.mainId,
        orderCode: '',
        thirdPartCode: '',
        thirdPartName: '',
        chaOuterNodeId: '',
        outerNodeName: '',
        businessCode: '',
        contactName: '',
        contactMobile: '',
        defaultClumFlag: '',
        benefit:'',
        dim:[],
        chaCode: '',
      };
      yield put({
        type: 'addNewRule',
        payload: ruleContent,
      });
    },

    *searchRecommend({data}, { call, put }) {
      let result = {};
      if(data){
        result = yield call(recommendsService.searchRecommend, {
          data
        });
      }else{
        result = yield call(recommendsService.searchRecommend, {
        
        });
      }
      
      const data3 = {
        "code": 0,
        "message": "Success",
        "data": {
            "pageNum": 1,
            "pageSize": 10,
            "size": 1,
            "startRow": 1,
            "endRow": 1,
            "total": 1,
            "pages": 1,
            "list": [
                {
                    "ruleCode": "f37f8a0bca2548d692e91b8d749bbbce",
                    "operDeptCode": "0102150502",
                    "operDeptName": "广东分公司佛山市桂城营销服务部",
                    "occurCityCode": "150700",
                    "occurDistrictCode": "110000",
                    "occurCityName": "北京市",
                    "occurDistrictName": "呼伦贝尔市",
                    "updatedUser": "李思思",
                    "updatedTime": "2018-05-25 10:31:02",
                    "approveUser": "旺才",
                    "approveTime": "2018-05-25 16:55:33",
                    "status": "0",
                    "ruleName": "你好",
                    "approveStatus": "1",
                    "bgnTime": "2018-12-23 16:55:33",
                    "endTime": "2018-12-23 16:55:33",
                    "approveContent": null,
                    "approveResult": null,
                    "contactName": null,
                    "contactPhone": null,
                    "pageNum": 1,
                    "pageSize": 10
                }
            ],
            "prePage": 0,
            "nextPage": 0,
            "isFirstPage": true,
            "isLastPage": true,
            "hasPreviousPage": false,
            "hasNextPage": false,
            "navigatePages": 8,
            "navigatepageNums": [
                1
            ],
            "navigateFirstPage": 1,
            "navigateLastPage": 1,
            "firstPage": 1,
            "lastPage": 1
        },
        "timestamp": 1527299595041
    };
       
      console.log(result);
      //data = data1.data;
      // const response = yield call(queryActivities);
      yield put({
        type: 'initRecommendMain',
        payload: result,
      });
    },

   
  },

  reducers: {
    initRecommendMain(state, action) {
      return {
        ...state,
        data: action.payload,
        list: action.payload.data.list,
      };
    },
    
  },
};
