import {
	message,
} from 'snk-web';
import {
	repServiceLink,
	valueAddedData,
	serviceDetailsQuery,
	exportNewValueAddData,
	exportSecurityPolicy,
	policyServiceQuery,
	queryVipRank,
	giveGifts,
	retryNote,
	confQuery,
	updateConf,
	addConf,
	queryPendingExpress,
	deleteExpressApply,
	queryStockInfo,
	editStockInfo,
	addStockInfo,
	getOrderMsg,
	exportOrderMsg,
	exportExpressOrder,
	exportStockInfo,
	placeExpressOrder,
	queryExpressReport,
	exportExpressReport,
	downLoadLineTemplate,
	downLoadPackageTemplate,
	downLoadGeneBoxTemplate,
	newValueAddData,
	securityPolicy,
	expressList,
	sendEmail,
} from '../services/valueAdd';

export default {
  namespace: 'valueAdd',
  state: {
		isSecPlyDate: false,
		newValueAddData: {}, // 新单数据提取
		securityPolicyData: {}, // 保全数据提取
		OrderMsgDataList: [], // 结算取数提取
		serviceDetailsQueryList: {}, // 增值服务详情的增值服务查询
		serviceDetailsQueryVipRank: {},	// 客户可赠送增值服务
		valueAddConfigList: [], // 增值服务总配置列表
		expressPrintData: {}, // 快递打印数据
		detailExpressInfo: [], // 增值服务详情已赠送服务物流
		expressReportData: {}, // 物流服务管理列表
		stockList: [], //采购信息列表
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
  effects: {
		// 新单增值服务取数
		*newValueAddData({ payload }, { call, put }) {
			try {
				const { data = {} } = yield call(newValueAddData, payload);
				yield put({
					type: 'save',
					payload: {
						newValueAddData: data,
					},
				});
				if(!data.total) {
					message.info('暂无查询数据');
					return data;
				}
				message.success('查询成功！');
				return data;
			} catch (e) {
				message.error(e.message);
				// console.log(e);
			}
		},
		// 导出新单服务数据
		*exportNewValueAddData({ payload }, { call, put }) {
			const response = yield call(exportNewValueAddData, payload);
			const fileName = `新单增值服务数据提取报表.xlsx`;
			if('msSaveOrOpenBlob' in navigator){
				window.navigator.msSaveOrOpenBlob(response, fileName);
			} else{
				const a = document.createElement('a');
				const url = window.URL.createObjectURL(response);  
				a.href = url; 
				a.download = fileName; 
				a.click(); 
				window.URL.revokeObjectURL(url);
			}
		},
		// 保全增值服务取数
		*securityPolicy({ payload }, { call, put }) {
			try {
				const { data = {} } = yield call(securityPolicy, payload);
				yield put({
					type: 'save',
					payload: {
						securityPolicyData: data,
					},
				});
				if(!data.total) {
					message.info('暂无查询数据');
					return data;
				}
				message.success('查询成功！');
				return data;
			} catch (e) {
				message.error(e.message);
				// console.log(e);
			}
		},
		// 导出保全服务数据
		*exportSecurityPolicy({ payload }, { call, put }) {
			const response = yield call(exportSecurityPolicy, payload);
			const fileName = `保全增值服务数据提取报表.xlsx`;
			if('msSaveOrOpenBlob' in navigator){
				window.navigator.msSaveOrOpenBlob(response, fileName);
			} else{
				const a = document.createElement('a');
				const url = window.URL.createObjectURL(response);  
				a.href = url; 
				a.download = fileName; 
				a.click(); 
				window.URL.revokeObjectURL(url);
			}
		},
		// 保单详情页增值服务查询
		*policyServiceQuery({ payload }, { call, put }) {
			const { transactionCode } = payload;
			try {
				const response1 = yield call(policyServiceQuery, payload);
				let params = {
					serviceDetailsQueryList: response1.data,
				}
				yield put({
					type: 'save',
					payload: {
						...params,
					}
				});
			} catch (e) {
				// console.log(e);
			}
		},
		// 赠送增值服务
		*giveGifts({ payload }, { call, put }) {
			const { transactionCode, cPlyNo } = payload;
			try {
				const response = yield call(giveGifts, payload);
				yield put({
					type: 'policyServiceQuery',
					payload: {
						transactionCode,
						cPlyNo,
					},
				});
				return response;
			} catch (e) {
				// console.log(e);
			}
		},
		// 重发增值服务
		*retryNote({ payload }, { call, put }) {
			return yield call(retryNote, payload);
		},
		// 查看物流
		*expressList({ payload }, { call, put }) {
			try {
				const response = yield call(expressList, payload);
				yield put({
					type: 'save',
					payload: {
						detailExpressInfo: [...response.data],
					},
				});
			} catch (e) {
				// console.log(e);
			}
		},
		// 结算维度取数
		*getOrderMsg({ payload }, { call, put }) {
			try {
				const response = yield call(getOrderMsg, payload);
				yield put({
					type: 'save',
					payload: {
						OrderMsgDataList: response.data,
					},
				});
				return response.data;
			} catch (e) {
				// console.log(e);
			}
		},
		// 导出结算维度取数
		*exportOrderMsg({ payload }, { call }) {
			const response = yield call(exportOrderMsg, payload);
			const fileName = `结算维度数据提取.xlsx`;
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a');
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
		},
		// 库存查询
		*queryStockInfo({ payload }, { call, put }) {
			try {
				const response = yield call(queryStockInfo, payload);
				const { data: { list } } = response;
				yield put({
					type: 'save',
					payload: {
						stockList: list,
					},
				});
				return response;
			} catch (e) {
				// 
			}
		},
		// 库存查询修改
		*editStockInfo({ payload }, { call, put }) {
			return yield call(	editStockInfo, payload);
		},
		// 新增库存
		*addStockInfo({ payload }, { call, put }) {
			return yield call(	addStockInfo, payload);
		},
		// 增值服务总列表查询
		*confQuery(action, { call, put }) {
			const response = yield call(confQuery);
			if (response.code === 0) {
				yield put({
					type: 'save',
					payload: {
						valueAddConfigList: response.data,
					}
				})
			}
			return response;
		},
		// 修改增值服务的配置
		*updateConf({ payload }, { call, put }) {
			return yield call(updateConf, payload);
		},
		// 新增增值服务的配置
		*addConf({ payload }, { call, put }) {
			return yield call(addConf, payload);
		},
		// 增值服务快递单打印查询
		*queryPendingExpress({ payload }, { call, put }) {
			const response = yield call(queryPendingExpress, payload);
			if(response.code === 0) {
				yield put({
					type: 'save',
					payload: {
						expressPrintData: response.data,
					},
				});
			}
			return response;
		},
		// 增值服务物流详情查询
		*queryExpressReport({payload},{call,put}){
			const response = yield call(queryExpressReport, payload);
			if(response.code === 0) {
				yield put({
					type: 'save',
					payload: {
						expressReportData: response.data,
					},
				});
			}
			return response;
		},
		// 增值服务未打印快递单删除
		*deleteExpressApply({ payload }, { call, put }) {
			return yield call(deleteExpressApply, payload);
		},
		//快递单导出
		*exportExpressOrder({ payload }, { call, put }) {
			 let response = yield call(exportExpressOrder, payload);
			 let fileName = "快递单.xls"; 
			 if('msSaveOrOpenBlob' in navigator){
		 	 window.navigator.msSaveOrOpenBlob(response, fileName);
			 } else{
				const a = document.createElement('a'); 
				const url = window.URL.createObjectURL(response);  
				a.href = url; 
				a.download = fileName; 
				a.click(); 
				window.URL.revokeObjectURL(url);
			}
		},
	//物流管理导出
	*exportExpressReport({ payload }, { call, put }) {
		let response = yield call(exportExpressReport, payload);
		let fileName = "物流管理.xls"; 
		if('msSaveOrOpenBlob' in navigator){
		 window.navigator.msSaveOrOpenBlob(response, fileName);
		} else{
		   const a = document.createElement('a'); 
		   const url = window.URL.createObjectURL(response);  
		   a.href = url; 
		   a.download = fileName; 
		   a.click(); 
		   window.URL.revokeObjectURL(url);
	   }
   },
   	//打包寄送模板导出
	*downLoadPackageTemplate({ payload }, { call, put }) {
		let response = yield call(downLoadPackageTemplate, payload);
		let fileName = "打包寄送下载模板.xlsx"; 
		if('msSaveOrOpenBlob' in navigator){
		 window.navigator.msSaveOrOpenBlob(response, fileName);
		} else{
		   const a = document.createElement('a'); 
		   const url = window.URL.createObjectURL(response);  
		   a.href = url; 
		   a.download = fileName; 
		   a.click(); 
		   window.URL.revokeObjectURL(url);
	   }
   },

  //线下单模板导出
	*downLoadLineTemplate({ payload }, { call, put }) {
		let response = yield call(downLoadLineTemplate, payload);
		let fileName = "线下单导入模板下载.xlsx"; 
		if('msSaveOrOpenBlob' in navigator){
		 window.navigator.msSaveOrOpenBlob(response, fileName);
		} else{
		   const a = document.createElement('a'); 
		   const url = window.URL.createObjectURL(response);  
		   a.href = url; 
		   a.download = fileName; 
		   a.click(); 
		   window.URL.revokeObjectURL(url);
	   }
	},
	   //检测盒编码模板导出
	*downLoadGeneBoxTemplate({ payload }, { call, put }) {
		let response = yield call(downLoadGeneBoxTemplate, payload);
		let fileName = "检测盒编码模板下载.xlsx"; 
		if('msSaveOrOpenBlob' in navigator){
		 window.navigator.msSaveOrOpenBlob(response, fileName);
		} else{
		   const a = document.createElement('a'); 
		   const url = window.URL.createObjectURL(response);  
		   a.href = url; 
		   a.download = fileName; 
		   a.click(); 
		   window.URL.revokeObjectURL(url);
		 }
  },
		//库存导出
		*exportStockInfo({ payload }, { call, put }) {
			let response = yield call(exportStockInfo, payload);
			let fileName = "库存信息.xls"; 
			if('msSaveOrOpenBlob' in navigator){
			 window.navigator.msSaveOrOpenBlob(response, fileName);
			} else{
			   const a = document.createElement('a'); 
			   const url = window.URL.createObjectURL(response);  
			   a.href = url; 
			   a.download = fileName; 
			   a.click(); 
			   window.URL.revokeObjectURL(url);
		   }
		},
		// 快递单批量打印
		*placeExpressOrder({ payload }, { call, put }) {
			let response = yield call(placeExpressOrder, payload);
			console.log(response)
			return  response;
		},
		// 发送邮件
		*sendEmail({ payload }, { call }) {
			const res = yield call(sendEmail, payload);
			console.log('res', res);
			return res;
		}
  }
}