import {
  cancelReviewQuery, executeRenwal, queryCancelData, importPendingCancelExcl, batchCancel, exportPendingCancelExcl,
} from '../services/api';
import { message } from 'snk-web';

export default {
  namespace: 'correctionManage',

  state: {
    ReviewQueryData: {},
    CancelOrRenewalResp: {},
  },

  effects: {
    // 取消续保查询
    *cancelReviewQuery({ payload }, { call, put }) {
      yield put({
        type: 'save',
        payload: {ReviewQueryData:{}}
      });
      const response = yield call(cancelReviewQuery, payload);
      0 === response.code ? message.success("查询成功") : message.error(response.message);
      yield put({
        type: 'save',
        payload: response
      });
    },
    // 取消续保/恢复续保
    *executeRenwal({ payload }, { call, put }) {
      const response = yield call(executeRenwal, payload);
      yield put({
        type: 'execute',
        payload: response
      });
    },
    // 批量注销查询
    *queryCancelData( { payload }, { call, put }) {
      return yield call(queryCancelData, payload);
    },
    // 上传批量注销数据
    *importPendingCancelExcl( { payload }, { call }) {
      console.log('payload',payload);
      try {
        const result= yield call(importPendingCancelExcl, payload);
        console.log('result',result);
        return result;
      } catch (data) {
        return data;
      }
      
      
    },
    // 批量注销
    *batchCancel({ payload }, { call }) {
      return yield call(batchCancel, payload);
    },

    // 批量导出清单
    *exportPendingCancelExcl({ payload }, { call, put }) {
      console.log(payload);
      let response = yield call(exportPendingCancelExcl, payload);
      console.log('response 2', response)
			let fileName = "批量注销清单.xls"; 
			if('msSaveOrOpenBlob' in navigator){
			 window.navigator.msSaveOrOpenBlob(response, fileName);
			} else{
			   const a = document.createElement('a'); 
			   const url = window.URL.createObjectURL(response);  
			   a.href = url; 
			   a.download = fileName; 
			   a.click(); 
			   window.URL.revokeObjectURL(url);
		   }
		},
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ReviewQueryData: payload
      };
    },
    execute(state, { payload }) {
      return {
        ...state,
        CancelOrRenewalResp: payload
      };
    },
  },
};
