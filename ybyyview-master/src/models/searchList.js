import { message } from 'snk-web';
import { routerRedux } from 'dva/router';
import {
  SearchRenewalList,
  SearchApplyList,
  DownLoadApplyList,
  SearchPolicyQuery,
  queryClaimInfo,
  SearchFinanceSum,
  FixedReport,
  FixedReportDownload,
  queryChannelPolicy,
  approvedChannel,
  QueryDownload,
  forRenewalTotal,
  alreadyRenewalTotal,
  sucessRenewalTotal,
  lostRenewalTotal,
  orderRenewalTotal,
  // qureyAlreadyRe,
  // qureyForRe,
  // qureyLostRe,
  // qureyOrderRe,
  // qureySuccessRe,
  totalCount,
  dayMonthTotalCount,
  orderCount,
  newSlipInfor,
  newSlipIndex,
  newOrderTwo,
  caliberChangeRate,
  SearchExactApplyList,
  policyExcelExport,
  noticeSendQuery,
} from '../services/searchList';

export default {
  namespace: 'searchlist',

  state: {
    applylist:{},
    ClaimInfo:[],
    renewalList:{},
    PolicyQueryData:[],
    PolicyQueryTotal: 0,
    FinanceSumData:{},
    FixedReportData:[],
    blobURL:{},
    xubaoURL: {},
    queryChannelData: {},
    forRenewalTotal: {},
    alreadyRenewal: {},
    sucessRenewalTotalData: {},
    lostRenewalTotal: {},
    orderRenewalTotalData: {},
    qureySuccessReData: {},
    qureyAlreadyReData:{},
    totalCountData: {},
    dayMonthtotalCountData: {},
    orderCountData: {},
    newSlipInforData: {},
    newSlipIndexData: {},
    newOrderTwoData: {},
    caliberChangeRateData:{},
    policyExcelRespose: {},
    newSlipIndexPieData: {},
    allPolicyQueryData:{
      pageNum:1,
      pageSize:10,
      searchValues:{},
      policyQueryActiveKey:'1',
    },
    policyQuerySearchFormData:{},
    policyQueryDefaultTab: '1', //默认policyList默认打开的面板
    policyMapInfo:{},
    selfChannelData: {}, // 质检审核记录当前查询关键字
  },

  effects: {
    *allPolicyQueryData({ payload }, { call, put, select }) {
      const { allPolicyQueryData } = yield select(state => state.searchlist);
      yield put ({
        type: 'save',
        payload: {
          allPolicyQueryData: {...allPolicyQueryData,...payload},
        },
      });
    },
    // 续保查询
    *SearchRenewalList({ payload }, { call, put }) {
      const response = yield call(SearchRenewalList, payload);
      yield put ({
        type: 'save',
        payload: {
          renewalList: response,
        },
      });
    },

    *queryClaimInfo({ payload }, { call, put }) {
      const response = yield call(queryClaimInfo, payload);
      yield put ({
        type: 'save',
        payload: {
          ClaimInfo: response.data||[],
        },
      });
    },

    // 续保下载
    *QueryDownload({payload},{call,put}) {
      const response = yield call(QueryDownload, payload);
      const fileName = "续保查询.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.style.display='none';
        document.body.appendChild(a);
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    *SearchFinanceSum({ payload }, { call, put }) {
      const response = yield call(SearchFinanceSum, payload);
      yield put({
        type: 'save',
        payload: {
          FinanceSumData:response,
        },
      });
    },
    *DownLoadApplyList({ payload }, { call, put }) {
      const response = yield call(DownLoadApplyList, payload);
      const fileName = "投保单查询.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.style.display='none';
        document.body.appendChild(a);
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
    *SearchApplyList({ payload }, { call, put }) {
      yield put({
        type: 'save',
        payload: {
          applylist:[],
        },
      });
      const response = yield call(SearchApplyList, payload);
      if(response){
        yield put({
          type: 'save',
          payload: {
            applylist:response.data,
          },
        });
      }
    },
    *SearchExactApplyList({ payload }, { call, put }) {
      yield put({
        type: 'save',
        payload: {
          applylist:[],
        },
      });
      const response = yield call(SearchExactApplyList, payload);
      if(response){
        yield put({
          type: 'save',
          payload: {
            applylist:response.data,
          },
        });
      }
    },
    *SearchPolicyQuery({ payload }, { call, put, select }) {
        console.log('payload',payload);
        const { isPageChange = false } = payload;
        if (isPageChange) {
          for ( let key in payload) {
            if (key === 'cPlyNo' || key === 'cPlyAppNo') {
              delete payload[key];
            }
          }
        }
        const response = yield call(SearchPolicyQuery, payload);
        const { policyQuerySearchFormData } = yield select( state => state.searchlist);
        yield put({
          type: 'save',
          payload: {
            policyQuerySearchFormData: { ...policyQuerySearchFormData, ...payload},
          },
        })
        if (response.code === 0) {
          const {data = {}} = response || {};
            yield put({
              type: 'save',
              payload: {
                PolicyQueryData: data.list,   
                PolicyQueryTotal: data.total,
              },
            });
          message.success(response.message);
        } else {
            yield put({
              type: 'save',
              payload: {
                PolicyQueryData: [],
                PolicyQueryTotal: 0,
              },
            });
          message.error(response.message);
        }
    },

    // 导出excel-保单列表
    *policyExcel({ payload }, { call, put }) {
      const response = yield call(policyExcelExport, payload);
      const fileName = "保单列表.xlsx";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a');
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    // 固定报表读取查询
    *FixedReport({payload},{call,put}){
      const response = yield call(FixedReport,payload);
      if (response) {
        if (response.data.list === [] || response.data.list === null) {
          message.success('查无数据');
        }
          yield put({
            type: 'save',
            payload: {
              FixedReportData: response,
            },
        });
      }
    },

    // 固定报表读取下载
    *FixedReportDownload({payload},{call,put}) {
      const response = yield call(FixedReportDownload,payload);
      const fileName = "固定报表.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.style.display='none';
        document.body.appendChild(a);
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    // 自建渠道质检审核查询 //投保未通过批改查询
    *queryChannelPolicy({ payload }, { call, put }) {
      const response = yield call(queryChannelPolicy, payload);
      if (response.data.list === []) {
        message.warning('查无数据');
      }
      yield put({
        type: 'save',
        payload: {
          queryChannelData:response.data,
          selfChannelData:{...payload},
        },
      });
    },
    //投保查询可编辑操作
    *goToEdit({ payload }, { call, put, select }) {
      const { selfChannelData } = yield select( state => state.searchlist);
      const { isSelfchannelAudit = false, beneficiaryList } = payload;
      yield put({
        type: 'save',
        payload: {
          policyMapInfo: {...payload},
          selfChannelData: {
            ...selfChannelData,
            isSelfchannelAudit,
          }
        }
      });
      for(let i = 0; i< beneficiaryList.length; i += 1) {
        yield put({
          type: 'selfchannelForm/savebeneficiaryList',
          payload: {
            [i + 1]: beneficiaryList[i],
          }
        });
      }
      yield put(routerRedux.push('/apply-manage/selfchannel-form'));
    },
    // 自建渠道质检审核查询 --- 打回通过
     *approvedChannel({ payload }, { call }) {
      const response = yield call(approvedChannel, payload);
      if (response) {
        if (response.code === 0) {
          message.success('操作成功');
        } else {
          message.error(response.message);
        }
      }
    },

    // 续保整体情况汇总—待续保
    *forRenewalTotal({payload},{call,put}) {
      const response = yield call(forRenewalTotal, payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            forRenewalTotal: response.data,
          },
        });
      }
    },

    // 续保整体情况汇总—已续保
    *alreadyRenewalTotal({payload},{call,put}) {
      const response = yield call(alreadyRenewalTotal, payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            alreadyRenewal: response.data,
          },
        });
      }
    },

    // 续保整体情况汇总—续保流失
    *lostRenewalTotal({payload},{call,put}) {
      const response = yield call (lostRenewalTotal, payload);
      if (response) {
        yield put ({
          type: 'save',
          payload : {
            lostRenewalTotal : response.data,
          },
        });
      }
    },

    // 续保整体情况汇总—本年续保成功率
    *sucessRenewalTotal({payload},{call,put}) {
      const response = yield call (sucessRenewalTotal, payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            sucessRenewalTotalData: response.data,
          },
        });
      }
    },

    // 续保整体情况汇总—排名前3
    *orderRenewalTotal({payload},{call,put}) {
      const response = yield call (orderRenewalTotal,payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            orderRenewalTotalData: response.data,
          },
        });
      }
    },

    // 服务数据大盘--年月日
    *totalCount({payload},{call,put}) {
      const response = yield call (totalCount,payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            totalCountData: response.data,
          },
        });
      }
    },

    // 服务数据大盘--月日业绩
    *dayMonthTotalCount({payload},{call,put}) {
      const response = yield call (dayMonthTotalCount,payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            dayMonthtotalCountData: response.data,
          },
        });
      }
    },

    // 服务数据大盘--项目组业绩排行榜
    *orderCount({payload},{call,put}) {
      const response = yield call (orderCount,payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            orderCountData: response.data,
          },
        });
      }
    },

    // 服务数据大盘--新单情况
    *newSlipInfor({payload},{call,put}) {
      const response = yield call (newSlipInfor,payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            newSlipInforData: response.data,
          },
        });
      }
    },

    // 服务数据大盘--新单运营指标
    *newSlipIndex({payload},{call,put}) {
      const response = yield call (newSlipIndex,payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            newSlipIndexData: response.data,
          },
        });
      }
    },

    // 服务数据大盘--项目组业绩排行榜2
    *newOrderTwo({payload},{call,put}) {
      const response = yield call (newOrderTwo,payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            newSlipIndexPieData: response.data,
          },
        });
      }
    },

     // 服务数据大盘--业绩转换
    *caliberChangeRate({payload},{call,put}) {
      const response = yield call (caliberChangeRate,payload);
      if (response) {
        yield put ({
          type: 'save',
          payload: {
            caliberChangeRateData: response.data,
          },
        });
      }
    },

    // 续保通知函查询
    *noticeSendQuery({ payload }, { call }) {
      return yield call(noticeSendQuery, payload);
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
