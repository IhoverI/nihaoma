import { routerRedux } from 'dva/router';
import { message } from 'snk-web';
import { fakeSubmitForm } from '../services/api';
import recommendsService from '../services/recommends';

export default {
  namespace: 'recommendForm',

  state: {
    addMainRule: {
      occurProvinceCode: '',
      occurCityCode: '',
      ruleCode: '',
      ruleName: '',
      updatedTime: '',
      updatedUser: '',
      status: '',
      aprroveStatus: '',
      bgnTime: '',
      endTime: '',
      mainId: '',
    },
    pushRepairMainDto:{},
    pushRepairRule:[],
    data:{},
  },

  effects: {
    *initDefault({ payload }, { put }){
      console.log("initDefault called....");
      // yield put({
      //   type: 'initAddDefault',
      //   data: {},
      // });
      yield put({
        type: 'initAddDefault',
        payload: '',
      });
    },
    *submitRegularForm({ payload }, { call }) {
      yield call(fakeSubmitForm, payload);
      message.success('提交成功');
    },
    *submitAddForm({ data }, { call, put }) {
      //yield call(fakeSubmitForm, payload);
      console.log("submitAddForm called...");
      console.log(data);
      yield put({
        type: 'saveStepFormData',
        data: data,
      });
      //yield put(routerRedux.push('/recommend/add'));
    },
    *searchRule({data}, { call, put }){
      console.log("model searchRule called...");
      const result = yield call(recommendsService.searchRule, {
        data
      });
      console.log(result);
      yield put({
        type: 'initSearchRule',
        payload: result,
      });
    },

    *submitAdvancedForm({ payload }, { call }) {
      yield call(fakeSubmitForm, payload);
      message.success('提交成功');
    },
  },

  reducers: {
    saveStepFormData(state, { data }) {
      return {
        ...state,
        addMainRule: {
          ...data.addMainRule,
        },
      };
    },
    initSubmitSearchForm(state, { payload }){
      return {
        ...state,
      }
    },
    initAddDefault(state, { payload }){
      console.log("initAddDefault called...");
      return {
        ...state,
        addMainRule: {
          occurProvinceCode: '130000',
          occurCityCode: '130200',
          ruleCode: '2d664e98e82f4dda97ae7dddb744b1b8',
          ruleName: '测测新增',
          updatedTime: '1538182720000',
          updatedUser: '1538182720000',
          status: '1',
          approveStatus: '0',
          bgnTime: '1538182720000',
          endTime: '1538282720000',
          mainId: '2d664e98e82f4dda97ae7dddb744b1b8',
        }
      };
    },
    initSearchRule(state, { payload }) {
      console.log("initSearchRule called...");
      console.log(payload);
      return {
        ...state,
        data: payload.data,
        pushRepairMainDto: payload.data.pushRepairMainDto,
        addMainRule: payload.data.pushRepairMainDto,
        pushRepairRule: payload.data.pushRepairRule,
      };
    },
  },
};
