import groupService from '../services/groupService';
import { getDataSource } from '../utils/utils'

export default {
  namespace: 'groupModel',

  state: {
    list: [],
    data: [],
    pagination: {},
    groupId: '',
    teamId: '',
    teams: [],
    teamMembers: [],
    searchMemebers:[],
   
  },

  effects: {

    // 团队与组别管理_查询团队与组别
    *searchGroup({ payload }, { call, put }){
      console.log('searchGroup called...');
      console.log(payload);
      const result = yield call(groupService.searchGroup, {
        data: payload,
      });
      
      console.log(result);
      yield put({
        type: 'initSearchGroup',
        payload: result,
      });
    },

    // 团队与组别管理_新增团队(弹出框)
    *addGroup({ data }, { call, put }){
      console.log('addGroup called...');
      console.log(data);
      const result = yield call(groupService.addGroup, {
        data: data.payload,
      });
      
      console.log(result);
      // 新增团队后返回团队id
      yield put({
        type: 'initAddGroup',
        payload: result,
      });
    },

    // 团队与组别管理_新增组别(弹出框)
    *addTeam({ data }, { call, put }){
      console.log('addTeam called...');
      console.log(data.payload);
      const result = yield call(groupService.addTeam, {
        data: data.payload,
      });
      
      console.log(result);
      // 新增团队后返回组别id
      yield put({
        type: 'initAddTeam',
        payload: result,
      });
    },
    
    // 编辑团队与组别信息_编辑团队
    *editGroup({ payload }, { call, put }){
      console.log('editGroup called...');
      console.log(payload);
      const result = yield call(groupService.editGroup, {
        data: payload,
      });
      console.log(result);
    },

    // 编辑团队与组别信息_查询组别信息
    *queryTeamList({ payload }, { call, put }){
      console.log('editGroup called...');
      console.log(payload);
      const result = yield call(groupService.editGroup, {
        data: {},
      });
      console.log(result);
      
      yield put({
        type: 'initTeamList',
        payload: result,
      });
    },

     // 编辑团队与组别信息_更新组别成员
    *updateTeam({ payload }, { call, put }){
      console.log('updateTeam called...');
      console.log(payload);
      const result = yield call(groupService.updateTeam, {
        data: payload,
      });
      console.log(result);
    },

    // 编辑团队与组别信息_查询组别成员
    *queryTeamMember({ payload }, { call, put,select }){
      let result;
      const chuangxinGroupIds = getDataSource('hrEmp').chuangXinYeWu
      if(chuangxinGroupIds.indexOf(payload.groupId&&payload.groupId)>-1){
         result = yield call(groupService.queryTeamCustomerMember, {
          data: payload,
        });
      }else{
         result = yield call(groupService.queryTeamMember, {
          data: payload,
        });
      }
   
      console.log(result);
      
      yield put({
        type: 'initTeamMembers',
        payload: result,
      });
    },
    *isChuangXinGroup({ payload }, { call, put,select }){
      return payload.groupId&&getDataSource('hrEmp').chuangXinYeWu.indexOf(payload.groupId)>-1
    },

    // 编辑团队与组别信息_新增组别成员
    *addTeamMember({ payload }, { call, put }){
      console.log('addTeamMember called...');
      console.log(payload);
      const result = yield call(groupService.addTeamMember, {
        data: {},
      });
      console.log(result);
    },

    // 搜索成员
    *searchMemebers({ data }, { call, put,select }){
      console.log('searchMemebers called...');
      console.log(data);
      let result;
      const chuangxinGroupId = getDataSource('hrEmp').chuangXinYeWu
      if(data.groupId&&chuangxinGroupId.indexOf(data.groupId)>-1){
        result = yield call(groupService.searchCustomerMemebers, {
          data,
        });
      }else{
         result = yield call(groupService.searchMemebers, {
          data,
        });
      }
     
      
      console.log(result);
      yield put({
        type: 'initSearchMemebers',
        payload: result,
      });
    },
    
  },

  reducers: {
    initSearchGroup(state, {payload}) {
      console.log('initSearchGroup called...');
      console.log(payload)
      return {
        ...state,
        data: payload.data,
        list: payload.data.list,
        count:payload.data.count,
      };
    },
    initAddGroup(state, action) {
       console.log('initAddGroup called...');
       console.log(action.payload.groupId);
       console.log(action.payload.data);
       return {
        ...state,
        groupId: action.payload.groupId,
      };
    },
    initAddTeam(state, action) {
       console.log('initAddTeam called...');
       console.log(action);
       return {
        ...state,
        teamId: action.payload.data.teamId,
      };
    },
    initTeamList(state, action) {
       console.log('initTeamList called...');
       console.log(action);
       return {
        ...state,
        teamId: action.payload.data.teams,
      };
    },
    initTeamMembers(state, action) {
       console.log('initTeamMembers called...');
       console.log(action);
       return {
        ...state,
        teamMembers: action.payload.data,
      };
    },
    initSearchMemebers(state, action) {
      console.log(action)
      console.log('initSearchMemebers called...');
      return {
       ...state,
       searchMemebers: action.payload.data.list,
     };
   },
    
  },
};
