import {
  SearchFinanceList,
  financeListDownload,
  uploadFTPFile
} from '../services/financeList';
export default {
  namespace: 'financeList',

  state: {
    financeList:{},
    financeListURL: {},
  },

  effects: {
    // 请求扣款文件汇总查询
    *SearchFinanceList({ payload }, { call, put }) {
      const response = yield call(SearchFinanceList, payload);
      yield put ({
        type: 'save',
        payload: {
          financeList: response,
        }
      });
    },
    *uploadFTPFile({payload},{call,put}){
      const response = yield call(uploadFTPFile, payload);
    },
    *financeListDownload({payload},{call,put}) {
      const response = yield call(financeListDownload, payload);
      const fileName = payload.fileNameFordownload;
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.style.display='none';
        document.body.appendChild(a);
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
  },
 
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload
      };
    },
  },
};
