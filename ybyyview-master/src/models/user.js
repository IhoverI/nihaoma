import { query as queryUsers, queryCurrent,queryMenuData } from '../services/user';

export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
    serverMenuData:[],
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *logout(_, { call, put }) {
      const response = yield call(queryCurrent);
    },
    *fetchCurrent(_, { call, put }) {
      const response = yield call(queryCurrent);
      const menuData = yield call(queryMenuData);
      console.log(menuData);

      yield put({
        type: 'commonsave',
        payload: {
          currentUser:response,
          serverMenuData:menuData,
        },
      });
    },
  },

  reducers: {
    commonsave(state, action) {
      return {
        ...state,
        ...action.payload
      };
    },
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload,
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload,
        },
      };
    },
  },
};
