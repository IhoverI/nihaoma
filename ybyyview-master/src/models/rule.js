import {
  queryGuaranteeReprint,
  updateGuaranteeReprint,
  deleteGuaranteeReprint,
  guaranteeReissueQuery, 
  removeRule, 
  guaranteeReissueRegister,
  guaranteeReprintQuery,
  guaranteeReprintPrint,
  expressReceiptQuery,
  expressSettlementDeal,
  expressFeeCollect,
  exportFeeCollectExcel,
  makeGuaranteePDF,
  exportSettlementDealExcel,
  planQuery,
  renwalPlanQuery,
  renwalInfo,
  guaranteeRepairSendPrint,
  queryEmailSendDetail,
  guaranteeReprintExport,
  exportExpressReceiptExcel,
  exportExpressQuestionInfo,
  queryPrintInfos,
  uploadExpressBackFile,
  uploadSukeyBackFile,
  downloadSukeyBackFile,
  deleteGuaranteeReprintInfo,
  exportPrintInfos,
} from '../services/api';

import { message } from 'snk-web';

export default {
  namespace: 'rule',

  state: {
    list: [],
    pagination: {},
    queryRuleData: {},
    guaranteeReprintQueryData: [],
    receiptData: {},
    expressData: [],
    expressFeeCollectData: [],
    blobURL:{},
    blobUrl:{},
    planQueryData: [],
    renwalPlanQueryData: [],
    sendEmailDetailResp: {},
    queryPrintInfosData: {},
    sukeyData:{},
    repRegisterData:[],
  },

  effects: {
    *queryGuaranteeReprint({ payload }, { call, put }) {
      const response = yield call(queryGuaranteeReprint, payload);
      if (response.code === 0) {
        message.success(response.message || '查询成功');
        yield put ({
          type: 'save',
          payload: {
            repRegisterData : response.data,
          },
        });
      } else {
        message.error(response.message || '查询失败，请稍后再试');
      }
    },
    *updateGuaranteeReprint({ payload }, { call, put }) {
      const response = yield call(updateGuaranteeReprint, payload);
      if (response.code === 0) {
        message.success(response.message || '修改成功');
      } else {
        message.error(response.message || '修改失败，请稍后再试');
      }
    },
    *deleteGuaranteeReprint({ payload }, { call, put }) {
      const response = yield call(deleteGuaranteeReprint, payload);
      if (response.code === 0) {
        message.success(response.message || '删除成功');
      } else {
        message.error(response.message || '删除失败，请稍后再试');
      }
    },
    *guaranteeReissueQuery({ payload }, { call, put }) {
      const response = yield call(guaranteeReissueQuery, payload);
      yield put({
        type: 'save',
        payload: {
          queryRuleData: response.data,
        },
      });
    },
    *guaranteeReissueRegister ({ payload }, { call, put }) {
      const response = yield call(guaranteeReissueRegister, payload);
      if (response) {
        if (response.code === 0) {
          message.success(response.message || '提交成功');
        } else {
          message.warning(response.message);
        }
      }
    },

    *makeGuaranteePDF ({ payload }, { call, put }) {
      const response = yield call(makeGuaranteePDF, payload);
      if (response) {
        if (response.code === 0) {
          message.success('发送成功');
        } else {
          message.warning('发送失败');
        }
      }
    },

    *remove({ payload, callback }, { call, put }) {
      const response = yield call(removeRule, payload);
      yield put({
        type: 'save',
        payload: response,
      });
      if (callback) callback();
    },

    *guaranteeReprintQuery({payload},{call,put}) {
      const response = yield call (guaranteeReprintQuery, payload);
      if (response) {
        if (response.data === []) {
          message.warning('查无数据');
        }
      }
      yield put ({
        type: 'save',
        payload: {
          guaranteeReprintQueryData : response.data,
        },
      })
    },

    *guaranteeReprintPrint({payload},{call,put}) {
      const response = yield call (guaranteeReprintPrint, payload);
      if (response) {
        if (response.code === 0) {
          message.success('打印成功');
        } else {
          message.warning('打印失败');
        }
      }
    },

    *guaranteeRepairSendPrint({payload},{call,put}) {
      const response = yield call (guaranteeRepairSendPrint, payload);
      if (response) {
        if (response.code === 0) {
          message.success(response.message);
        } else {
          message.warning(response.message);
        }
      }
    },

    *deleteGuaranteeReprintInfo({ payload }, { call, put }) {
      const response = yield call(deleteGuaranteeReprintInfo, payload);
      if (response) {
          if (response.code === 0) {
            message.success('删除成功');
          } else {
            message.success('删除失败');
          }
        }
  },


    *queryPrintInfos({payload},{call,put}) {
      const response = yield call (queryPrintInfos, payload);
       yield put ({
        type: 'save',
        payload: {
          queryPrintInfosData : response,
        },
      })
      if (response) {
        if (response.code === 0) {
          message.success(response.message);
        } else {
          message.warning(response.message);
        }
      }
    },

    *uploadExpressBackFile({payload}, {call,put}) {
      const response = yield call (uploadExpressBackFile, payload);
      const fileName = "快递回执清单.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    *uploadSukeyBackFile({payload}, {call,put}) {
      const response = yield call (uploadSukeyBackFile, payload);
      const fileName = "sukey.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
      yield put ({
        type: 'save',
        payload: {
          sukeyData : response,
        },
      });
    },

    *downloadSukeyBackFile({payload},{call,put}) {
      const response = yield call (downloadSukeyBackFile,payload);
      const fileName = "templateSukey.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response); 
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    *guaranteeReprintExport({payload},{call,put}) {
      const response = yield call (guaranteeReprintExport,payload);
      const fileName = "保单补打清单.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    *expressReceiptQuery({payload},{call,put}) {
      const response = yield call (expressReceiptQuery, payload);
      yield put ({
        type: 'save',
        payload: {
          receiptData : response,
        },
      });
    },

    *exportExpressReceiptExcel({payload},{call,put}) {
      const response = yield call (exportExpressReceiptExcel, payload);
      const fileName = "快递回执清单.xlsx";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const url = window.URL.createObjectURL(response);  
        const a = document.createElement('a'); 
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
    *exportExpressQuestionInfo({payload},{call,put}) {
      const response = yield call (exportExpressQuestionInfo, payload);
      const fileName = "导出快递问题件清单.xlsx";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const url = window.URL.createObjectURL(response);  
        const a = document.createElement('a'); 
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },


    *exportFeeCollectExcel({payload},{call,put}) {
      const response = yield call (exportFeeCollectExcel, payload);
      const fileName = "费用管理汇总清单.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    *exportSettlementDealExcel({payload},{call,put}) {
      const response = yield call (exportSettlementDealExcel, payload);
      const fileName = "费用管理清单.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    *expressSettlementDeal({payload},{call,put}) {
      const response = yield call (expressSettlementDeal, payload);
      yield put ({
        type: 'save',
        payload: {
         expressData : response.data,
        },
      });
    },

    *expressFeeCollect({payload},{call,put}) {
      const response = yield call (expressFeeCollect, payload);
      yield put ({
        type: 'save',
        payload: {
         expressFeeCollectData : response.data,
        },
      });
    },
    *planQuery({payload},{call,put}) {
      const response = yield call (planQuery, payload);
      yield put ({
        type: 'save',
        payload: {
         planQueryData : response.data,
        },
      });
    },

    *renwalPlanQuery({payload},{call,put}) {
      const response = yield call (renwalPlanQuery, payload);
      yield put ({
        type: 'save',
        payload: {
         renwalPlanQueryData : response.data,
        },
      });
    },

    *renwalInfo({payload},{call,put}) {
      const response = yield call (renwalInfo, payload);
      if (response) {
        if (response.code === 0) {
          message.success('修改成功');
        } else {
          message.error('修改失败');
        }
      }
    },

    *exportSettlementDealExcel({payload},{call,put}) {
      const response = yield call (exportSettlementDealExcel, payload);
      const fileName = "快递费用管理.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
    
    *queryEmailSendDetail({payload},{call,put}) {
      const response = yield call (queryEmailSendDetail, payload);
      yield put ({
        type: 'save',
        payload: {
          sendEmailDetailResp : response,
        },
      });
    },

    //打印详情页导出
    *exportPrintInfos({payload},{call,put}) {
      const response = yield call (exportPrintInfos,payload);
      const fileName = "打印详情清单.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
  },

  reducers: {
    save(state, action) {
       return {
        ...state,
        ...action.payload,
      };
    },
  },
};
