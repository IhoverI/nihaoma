import { findSalaryProcess,updateSalaryResult,approvedSalaryResult } from '../services/approvedList';
import { message } from 'snk-web';

export default {
  namespace: 'approvedList',

  state: {
    dataList: [],
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      console.log('进入fetch函数')
      const response = yield call(findSalaryProcess, payload);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *update({ payload }, { call, put,select,take }) {
      let record=payload.record
     let para= {"data":{"calcMonth":record.calcMonth,"calcYear":record.calcYear,
     "groupId":record.groupId,"groupName":record.groupName}}
      const response = yield call(updateSalaryResult, para);
      if (response.code === 0) {
        message.success(response.message);
      } else {
        message.error(response.message);
      }
      return response
    },
    *updateStatus({ payload }, { call }) {
     const para= {"data":{...payload}}
      const response = yield call(approvedSalaryResult, para);
      if (response.code === 0) {
        message.success(response.message);
      } else {
        message.error(response.message);
      }
      return response
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        dataList: action.payload.data,
      };
    },
  },
};
