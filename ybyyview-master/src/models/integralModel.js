import { 
  message,
} from 'snk-web';
import moment from 'moment';
import integralService from '../services/integralService';
import { integralGiftDownload, integralGiftUploadFailDownload,
  integralAllDownList, integralCustomerDownList, exportRedPacketExcel, downloadCustomerAllList,
 } from '../services/integralService';

export default {
  namespace: 'integralModel',
  state: {
    integralSearchUserInfo:{}, //积分查询客户信息
    integralSearchListInfo:[], //积分查询列表详情
    integralManageListInfo:[], // 积分管理列表
    integralGiftListInfo: [], // 积分赠送列表
    intergralCustomerListInfo: [], // 客户存留率
    intergralAllListInfo: [], // 整体积分列表
    manageTotal: 0, // 积分管理列表
    giftTotal: 0,  // 赠送积分
    integralValueAddedListInfo: [], // 增值服务
    repacketSearchList: [], // 红包查询
    redPacketSearchListTotal: 0, // 红包查询总数
    repacketList: [], // 红包详情
    redPacketListTotal: 0, // 红包详情总数
    customerAllList: [], // 客户整体
    customerAllListTotal: 0,  //  客户整体总数
  },
  effects: {
    *queryIntegralSearchInfo({payload}, { call, put }){ // 积分查询
      try {
        const respontResults = yield call(integralService.queryIntegralSearchInfo, payload);
        // 查询增值服务
        const response = yield call(integralService.queryValueAddedSearchInfo, payload);
        const { cusMsg = {}, detail=[] } = respontResults || {};
        yield put({
          type: 'saveState',
          payload: {
            integralSearchUserInfo: cusMsg,
            integralSearchListInfo: detail,
            integralValueAddedListInfo: response.data,
          },
        });
      } catch (error) {
        yield put({
          type: 'saveState',
          payload: {
            integralSearchUserInfo: {},
            integralSearchListInfo: [],
          },
        });
        message.error(error.message || '没有查询到数据信息');
      }
    },
    *queryIntegralGiftList({payload}, { call, put }){ // 积分赠送列表
        try {
          const result = yield call(integralService.queryIntegralGiftList, payload);
          const {list = [], total = 0 } = result || {};
          yield put({
            type: 'saveState',
            payload: {
              integralGiftListInfo: list,
              giftTotal:total,
            },
          });
        } catch (error) {
          yield put({
            type: 'saveState',
            payload: {
              integralGiftListInfo: [],
              giftTotal: 0,
            },
          });
          message.error(error.message || '没有查询到数据信息');
        }
    },
    *integralGiftDownloadTemplate({payload}, { call, put }){ // 赠送积分模板下载
      try {
          const response = yield call(integralGiftDownload, payload);
          const timer = moment().format("YYYYMMDDHHmmss");
          const fileName = `赠送积分模板${timer}.xlsx`
            if('msSaveOrOpenBlob' in navigator){
                window.navigator.msSaveOrOpenBlob(response, fileName);
              } else{
                const a = document.createElement('a'); 
                const url = window.URL.createObjectURL(response);  
                a.href = url; 
                a.download = fileName; 
                a.click(); 
                window.URL.revokeObjectURL(url);
              }
      } catch (error) {
        message.error('文件下载失败');
      }
    },
    *downIntegralGiftUpFailFile({payload}, { call, put }){ // 赠送积分上传失败下载上传文件
      try {
        const response = yield call(integralGiftUploadFailDownload, payload);
        const {fileName} = payload || {};
          if('msSaveOrOpenBlob' in navigator){
              window.navigator.msSaveOrOpenBlob(response, fileName);
            } else{
              const a = document.createElement('a'); 
              const url = window.URL.createObjectURL(response);  
              a.href = url; 
              a.download = fileName; 
              a.click(); 
              window.URL.revokeObjectURL(url);
            }
      } catch (error) {
        message.error('文件下载失败');
      }
    },
    *queryIntegralManageList({payload}, { call, put }){ //积分管理列表
        try {
          const result = yield call(integralService.queryIntegralManageList, payload);
          const {list = [], total = 0 } = result || {};
          yield put({
            type: 'saveState',
            payload: {
              integralManageListInfo: list,
              manageTotal: total,
            },
          });
        } catch (error) {
          yield put({
            type: 'saveState',
            payload: {
              integralManageListInfo: [],
              manageTotal: 0,
            },
          });
          message.error(error.message || '没有查询到数据信息');
        } 
    },
    *updateIntegralManageRow({payload}, { call, put, select }){ //积分管理列表行修改
        try {
          const {integralManageListInfo} = yield select(state=> state.integralModel);
          const {updateRowdata,id} = payload;
          const index = integralManageListInfo.findIndex(item => id === item.id);
          const item = integralManageListInfo[index];
          const newRow = { ...item, ...updateRowdata};
          yield call(integralService.updateIntegralManageRow, newRow);
          //获取操作人
          const { currentUser } = yield select(state=> state.umssouserinfo);
          const { principal } = currentUser || {};
          const {name} = principal || {};
          // 获取当前时间
          const cTime = moment().format('YYYY-MM-DD:HH:MM:SS');
          const showRow = {...newRow, cTime, creator: name }
          const newdatas = [...integralManageListInfo];
          newdatas.splice(index, 1, { ...showRow });
          yield put({
            type: 'saveState',
            payload: {
              integralManageListInfo: newdatas,
            },
          });
          message.success('修改成功');
        } catch (error) {
          message.success('修改保存失败');
        }
   },
   *updateIntegralGiftRow({payload}, { call, put, select }){ //赠送积分列表行修改
      try {
        const {integralGiftListInfo} = yield select(state=> state.integralModel);
        const {updateRowdata,id} = payload;
        const index = integralGiftListInfo.findIndex(item => id === item.id);
        const item = integralGiftListInfo[index];
        const newRow = { ...item, ...updateRowdata};
        yield call(integralService.updateIntegralGiftRow, newRow);
        const newdatas = [...integralGiftListInfo];
        newdatas.splice(index, 1, { ...newRow });
        yield put({
          type: 'saveState',
          payload: {
            integralGiftListInfo: newdatas,
          },
        });
        message.success('修改成功');
      } catch (error) {
        message.error('修改保存失败');
      }
    },
    *delIntegralGiftRow({payload}, { call, put, select }){ //赠送积分行删除
        try {
          yield call(integralService.delIntegralGiftRow, payload);
          const {id} = payload;
          const {integralGiftListInfo} = yield select(state=> state.integralModel);
          const index = integralGiftListInfo.findIndex(item => id === item.id);
          const newdatas = [...integralGiftListInfo];
          newdatas.splice(index, 1);
          yield put({
            type: 'saveState',
            payload: {
              integralGiftListInfo: newdatas,
            },
          });
          message.success('删除成功');
        } catch (error) {
          message.error('删除失败！');
        }
    },
    *queryIntergralCustomerList({payload}, { call, put }){ // 客户存留率查询
      try {
        const result = yield call(integralService.queryIntergralCustomerList, payload);
        yield put({
          type: 'saveState',
          payload: {
            intergralCustomerListInfo: result || [],
          },
        });
        if(!result){
          message.info('没有查询到结果');
        }
      } catch (error) {
        yield put({
          type: 'saveState',
          payload: {
            intergralCustomerListInfo: [],
          },
        });
        message.error('查询失败,请稍后重试');
      }
    },
    *queryIntergralAllList({payload}, { call, put }){ // 整体积分查询
      try {
        const result = yield call(integralService.queryIntergralAllList, payload);
        yield put({
          type: 'saveState',
          payload: {
            intergralAllListInfo: result || [],
          },
        });
        if(!result){
          message.info('没有查询到结果');
        }
      } catch (error) {
        yield put({
          type: 'saveState',
          payload: {
            intergralAllListInfo: [],
          },
        });
        message.error('查询失败,请稍后重试');
      }
      
    },
    *integralAllDownList({payload}, { call, put }){ // 导出整体积分
      try {
        const response = yield call(integralAllDownList, payload);
        const timer = moment().format("YYYYMMDDHHmmss");
        const fileName = `整体积分${timer}.xlsx`
          if('msSaveOrOpenBlob' in navigator){
              window.navigator.msSaveOrOpenBlob(response, fileName);
            } else{
              const a = document.createElement('a'); 
              const url = window.URL.createObjectURL(response);  
              a.href = url; 
              a.download = fileName; 
              a.click(); 
              window.URL.revokeObjectURL(url);
            }
      } catch (error) {
        message.error('文件下载失败');
      }
    },
    *integralCustomerDownList({payload}, { call, put }){ // 导出客户存留率
      try {
        const response = yield call(integralCustomerDownList, payload);
        const timer = moment().format("YYYYMMDDHHmmss");
        const fileName = `客户存留率${timer}.xlsx`
          if('msSaveOrOpenBlob' in navigator){
              window.navigator.msSaveOrOpenBlob(response, fileName);
            } else{
              const a = document.createElement('a'); 
              const url = window.URL.createObjectURL(response);  
              a.href = url; 
              a.download = fileName; 
              a.click(); 
              window.URL.revokeObjectURL(url);
            }
      } catch (error) {
        message.error('文件下载失败');
      }
    },
    *queryIntergralRedPacketSearch({payload}, { call, put }){ // 红包查询
      try {
        const result = yield call(integralService.queryIntergralRedPacketSearch, payload);
        const {list = [], total = 0 } = result || {};
        yield put({
          type: 'saveState',
          payload: {
            repacketSearchList: list,
            redPacketSearchListTotal: total,
          },
        });
      } catch (error) {
        yield put({
          type: 'saveState',
          payload: {
            repacketSearchList: [],
            redPacketSearchListTotal: 0,
          },
        });
        message.error(error.message || '没有查询到数据信息');
      } 
    },
    *queryIntergralRedPacketList({payload}, { call, put }){ // 红包详情
      try {
        const result = yield call(integralService.queryIntergralRedPacketList, payload);
        const {list = [], total = 0 } = result || {};
        yield put({
          type: 'saveState',
          payload: {
            repacketList: list,
            redPacketListTotal: total,
          },
        });
      } catch (error) {
        yield put({
          type: 'saveState',
          payload: {
            repacketList: [],
            redPacketListTotal: 0,
          },
        });
        message.error(error.message || '没有查询到数据信息');
      } 
    },
    *extendBatch({ payload }, { call }) {
      let response = yield call(exportRedPacketExcel, payload);
			let fileName = "红包清单.xls"; 
			if('msSaveOrOpenBlob' in navigator){
			 window.navigator.msSaveOrOpenBlob(response, fileName);
			} else{
			   const a = document.createElement('a'); 
			   const url = window.URL.createObjectURL(response);  
			   a.href = url; 
			   a.download = fileName; 
			   a.click(); 
			   window.URL.revokeObjectURL(url);
		  }
    },
    
    *querycustomerAllSearchList({payload}, { call, put }){ // 红包详情
      try {
        const data = yield call(integralService.querycustomerAllSearchList, payload) || [];
       
        yield put({
          type: 'saveState',
          payload: {
            customerAllList: data,
            customerAllListTotal: data.length,
          },
        });
      } catch (error) {
        yield put({
          type: 'saveState',
          payload: {
            customerAllList: [],
            customerAllListTotal: 0,
          },
        });
        message.error(error.message || '没有查询到数据信息');
      } 
    },
    *downloadCustomerAllList({payload}, { call, put }){ // 导出客户存留率
      try {
        const {isUser} = payload;
        const response = yield call(downloadCustomerAllList, payload);
        const timer = moment().format("YYYYMMDDHHmmss");
        let fileName = `客户整体${timer}.xlsx`;
        if(isUser) fileName = `客户清单${timer}.xlsx`;
          if('msSaveOrOpenBlob' in navigator){
              window.navigator.msSaveOrOpenBlob(response, fileName);
            } else{
              const a = document.createElement('a'); 
              const url = window.URL.createObjectURL(response);  
              a.href = url; 
              a.download = fileName; 
              a.click(); 
              window.URL.revokeObjectURL(url);
            }
      } catch (error) {
        message.error('文件下载失败');
      }
    },
  },
  reducers: {
    saveState(state,{ payload }){
      return {...state, ...payload}
    },
  }
}
