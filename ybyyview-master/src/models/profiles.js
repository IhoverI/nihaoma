import {
    queryBasicProfile,
    queryAdvancedProfile,
    query,
    apply,
    passOrNot,
    userCorrect,
    userInforDetail,
    endorListData,
    endorList,
    pass,
    back,
    updateCardId,
    download,
  } from '../services/api';
  import { message } from 'snk-web';
  
  export default {
    namespace: 'profiles',
  
    state: {
      queryData: [],
      userCorrectData:[],
      queryInfoData: {},
      backResp: {},
      passResp: {},
    },
  
    effects: {
      *fetchBasic(_, { call, put }) {
        const response = yield call(queryBasicProfile);
        yield put({
          type: 'save',
          payload: response,
        });
      },
      *fetchAdvanced(_, { call, put }) {
        const response = yield call(queryAdvancedProfile);
        yield put({
          type: 'save',
          payload: response,
        });
      },
  
      // 保单批改 --- 客户信息批改查询
      *query({payload}, {call,put}) {
          const response = yield call (query, payload);
          if (response.code === 1) {
            message.warning(response.message);
          } 
          yield put ({
            type: 'save',
            payload: {
              queryData: response.data,
            },
          });
      },
  
      // 客户信息批改下载
      *download({payload},{call,put}) {
        const response = yield call (download, payload);
        // const a = document.createElement('a'); 
        // const url = window.URL.createObjectURL(response);  
        // a.href = url; 
        // a.download = "客户信息批改.xls"; 
        // a.click(); 
        // window.URL.revokeObjectURL(url);
      },
      
       // 保单批改 --- 客户信息批改提交
      *apply({payload},{call,put}) {
        const response = yield call (apply, payload);
        message.success(response)
      },
  
      // 保单批改 --- 客户信息批改卡号提交
      *updateCardId({payload},{call,put}) {
        const response = yield call (updateCardId, payload);
        message.success(response) 
      },
  
    // 保单批改 --- 客户信息批改审核通过或拒绝
      *passOrNot({payload},{call,put}) {
        const response = yield call (passOrNot, payload);
        yield put ({
          type: 'passOrNotPassResp',
          payload: response,
        });
      },
  
      // 客户信息批核审核
      *userCorrect({payload},{call,put}) {
        const response = yield call (userCorrect,payload);
        yield put ({
          type: 'save',
          payload : {
            userCorrectData: response.data,
          },
        });
      },
  
       // 客户信息批改审核详情查询
      *userInforDetail({payload},{call,put}) {
        const response = yield call (userInforDetail, payload);
        yield put ({
          type: 'save',
          payload : {
            queryInfoData: response.data,
          },
        });
      },
  
      // 注退待审核列表
      *endorList({payload},{call,put}) {
        const response = yield call (endorList, payload);
        response.code === 0 ? message.success('搜索成功') : message.error(response.message);
        yield put ({
          type: 'save',
          payload : {
            endorListData: response.data,
          },
        });
      },
  
      // 注退待审核通过
      *pass({payload},{call,put}) {
        const response = yield call (pass, payload);
        yield put ({
          type: 'initPassResp',
          payload: response,
        });
      },
  
      // 注退待审核打回
      *back({payload},{call,put}) {
        const response = yield call (back, payload);
        yield put ({
          type: 'initBackResp',
          payload: response,
        });
      },
  
    },
  
    reducers: {
      save(state, { payload }) {
        return {
          ...state,
          ...payload,
        };
      },
      initBackResp(state, { payload }) {
        return {
          ...state,
          backResp: payload,
        };
      },
      initPassResp(state, { payload }) {
        return {
          ...state,
          passResp: payload,
        };
      },
      passOrNotPassResp(state, { payload }) {
        return {
          ...state,
          passOrNotResp: payload,
        };
      },
    },
  };
  