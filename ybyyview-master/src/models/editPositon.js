import {getAllLaws} from '../services/editPositonService';

export default {
  namespace: 'editPosition',

  state: {
    lawOptions: [],
  },


  effects: {
    *getLawsInfos({ payload }, { call, put }) {
      const response = yield call(getAllLaws, {
      });
      console.log("getLawsInfos called...");
      yield put({
        type: 'initLawOptions',
        payload: response,
      });
    },
  },

  reducers: {
    initLawOptions(state, action) {
      console.log("initLawOptions called...");
      console.log(action.payload.data)
      return {
        ...state,
        lawOptions: action.payload.data,
      };
    },
  },
};
