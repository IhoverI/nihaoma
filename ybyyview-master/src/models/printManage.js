import { 
  searchPrintQuery,
  policyPrint,
  toMakeGuarantee,
  expressAddress,
  updateEXpressCompany,
  turnExpressCode,
  exportPrintInfoList,
 } from '../services/api';
import { message } from 'snk-web';

export default {
  namespace: 'printManage',

  state: {
    renewalList:[],
    PolicyQueryData:{}, // 保单列表
    PrintQueryData:[],
    printReturnData:{},
    blobURL: {},
  },

  effects: {
    *searchPrintQuery({ payload }, { call, put }) {  // 新单打印查询
        const response = yield call(searchPrintQuery, payload);
        const {data = []} =response || {};
        yield put({
          type: 'save',
          payload: {
            PrintQueryData: data || [],
          },
        });
        if(data && !data.length){
          message.info('没有查询到数据');
        }
    },
    *reset({ payload }, { call, put }) {
      yield put({
        type: 'save',
        payload: {
          PrintQueryData:[],
        },
      });
    },
    // 新单打印--批量打印
    *toMakeGuarantee({ payload }, { call, put }) {
        const response = yield call(toMakeGuarantee, payload);
        if (response) {
          if (response.code === 0) {
            message.success('打印成功');
            yield put({
              type: 'save',
              payload: {
                PrintQueryData:[],
              },
            });
          } else {
            message.warning(response.message || '打印失败');
          }
        }
    },

    // 新单打印 --- 操作
    *updateEXpressCompany({ payload }, { call, put }) {
        const response = yield call(updateEXpressCompany, payload);
        if (response) {
          if (response.code === 0) {
            message.success('操作成功');
          } else {
            message.success('操作失败');
          }
        }
    },

    // 新单打印--导出地址不全
    *expressAddress({payload},{call,put}) {
      const response = yield call(expressAddress, payload);
      const fileName = "新单打印清单(地址不全).xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
     // 打印管理清单导出
     *exportPrintExpressInfo({payload},{call,put}) {
      const response = yield call(exportPrintInfoList, payload);
      const fileName = "打印管理清单.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },
    // 新单打印--全部转德邦/顺丰
    *turnExpressCode({payload},{call,put}) {
      const response = yield call (turnExpressCode,payload);
      if (response) {
          if (response.code === 0) {
            message.success(response.message);
            yield put({
              type: 'save',
              payload: {
                PrintQueryData:[],
              },
            });
          } else {
            message.warn(response.message);
          }
        }
    },

    // 保单打印
    *policyPrint({ payload }, { call, put }) {
      const response = yield call(policyPrint, payload);
      const alertMessage = response.message;
      response.code == '0' ? message.success('提交成功') : message.error(alertMessage);      
      yield put({
        type: 'save',
        payload: {
          printReturnData:response,
        },
      });
  },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
