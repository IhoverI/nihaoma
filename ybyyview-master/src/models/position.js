import positionsService from '../services/positions';

export default {
  namespace: 'positions',

  state: {
    list: [],
    data: [],
    pagination: {},
  },

  effects: {
    *searchPosition({ payload }, { call, put }){
      console.log('searchPosition called...');
      console.log(payload);
      const result = yield call(positionsService.searchPosition, {
        data: {},
      });
      
      console.log('data1');
      console.log(result);
      yield put({
        type: 'initPosition',
        payload: result,
      });
    },

    *searchPosition2({ payload }, { call, put }) {
      console.log('searchPosition called...');
      console.log(payload.values);
      const data1 = yield call(positionsService.searchPosition, {
        data: payload.values,
      });
      console.log('data1');
      console.log(data1);
      
      let data = [{
        positionName: 'TSR',
        firstLineFlag: '1',
        status: '0',
        key:'27',
        ranks:[
          {
          rankName: 'TSR一级c',
          order: '1',
          firstLineFlag: '0',
          key:'positionName12',
          status: '1',},
          {
            rankName: 'TSR二级',
            order: '1',
            firstLineFlag: '1',
            key:'positionName13',
            status: '1',},
        ],
      }, {
        positionName: 'TSR主管',
        ranks: [],
        key:'21',
        firstLineFlag: '1',
        width: '25%',
        status: '0',
      }];
      //data = data1.positionList;
      // const response = yield call(queryActivities);
      yield put({
        type: 'initPosition',
        payload: data1,
      });
    },

    *addPosition({data}, { call, put }) {
      console.log("model addPosition called...");
      console.log(data);
      const result = yield call(positionsService.addPosition, {
        data
      });
    },

    *editPosition({data}, { call, put }) {
      console.log("model editRank called...");
      console.log(data);
      const data1 = yield call(positionsService.editPosition, {
        data
      });
    },
    
    *editRank({data}, { call, put }) {
      console.log("model editRank called...");
      console.log(data);
      const data1 = yield call(positionsService.editRank, {
        data
      });
    },

    *addRank({data}, { call, put }) {
      console.log("model addRank called...");
      console.log(data);
      const data1 = yield call(positionsService.addRank, {
        data
      });
    },
  },

  reducers: {
    initPosition(state, action) {
      console.log('initPosition called...');
      console.log(action);
      return {
        ...state,
        data: action.payload.data,
        list: action.payload.data.list
      };
    },
  },
};
