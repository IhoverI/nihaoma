import {
	message,
} from 'snk-web';
import {
  dataTotal,
  dataQueryAreaList,
  statisticsApply
} from '../services/taskOverviewServices';
export default {
  namespace: 'taskOverviewModels',
  state: {
        date: '',           //进线时间
        cPlyNo: '',         //保单号
        cMobileNo: '',      //联系电话
        scene:'',   
        list:[],
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
  effects: {
    // 进入任务概览页面获取显示数据
    *dataTotal({ payload }, { call, put }) {
      console.log('数据',payload);
      let response = yield call(dataTotal, payload);
      console.log('suju',response);
      const {untreatedTotal,trackedTotal, applyTotal, list, total } = response
      yield put({
        type: 'save',
        payload: {
          untreatedTotal,
          trackedTotal,	
          applyTotal,
          total, 
          list,        
        },
      });
    },
    // 查询数据
    *dataQueryAreaList({ payload }, { call, put }) {
      console.log('查询',payload);
      return yield call(dataQueryAreaList, payload);
    },
    // 申请
    *statisticsApply({ payload }, { call, put }) {
      console.log('申请',payload);
      debugger
     const data = yield call(statisticsApply, payload);
     console.log(data);
     
    },
    // 新建特殊工单前获取数据
    *beforeCreat({ payload }, { put }) {
      console.log('新建前',payload);
      const { date, scene } = payload;
      yield put({
        type: 'save',
        payload: {
            date, //进线时间
            scene //          
        },
      });
    },
    //新建工单
    *workOrder( { payload }, { call }) {
      console.log('xinjingongdan',payload);
      // try {
        const result= yield call(workOrder, payload);
        console.log('result',result);
        if(result=='0'){
          message.success('新建成功！');
        }
        // const {massage}=result;
        message.info('新增工单登记成功！')
        return result;
      // } catch (data) {
      //   console.log('data',data);
        
      //   message.error(data.message)
      // }  
    },
  },
}