import {message, notification } from 'snk-web';
import {
	ueryFinanceInfo, exportFinanceFile, checkConsistency, uploadFinanceFile, recountFileList, importOfflineInfo, queryFinanceInfo,
} from '../services/financeReport';

 export default {
    namespace: 'financeReport',
    state: {
        financeInfoList: [],
        financeInfoCount: 0,
        blobURL:{},
        queryFinanceInfo: {},
    },

    effects: {
        *queryFinanceList({payload}, {call,put}) { // 查询财务对账报表
            const response = yield call(queryFinanceInfo, payload);
            if(response.code == '0'){
                const {financeInfoCount = 0, financeInfoList = [] ,} = response.data || {};
                yield put ({
                    type: 'save',
                    payload: {
                        financeInfoCount,
                        financeInfoList
                    },
                });
                return '0';
            }else{
                yield put ({
                    type: 'save',
                    payload: {
                        financeInfoCount: 0,
                        financeInfoList: [],
                    },
                });
                message.error(response.message);
                return '-1';
            }
        },

        *exportFinanceFile({payload}, {call,put}) { // 导出文件
            const response = yield call (exportFinanceFile, payload);
            // console.log('res', response);
            const fileName = "财务报表.xls"
            if('msSaveOrOpenBlob' in navigator){
                window.navigator.msSaveOrOpenBlob(response, fileName);
              } else{
                const a = document.createElement('a'); 
                const url = window.URL.createObjectURL(response);  
                a.href = url; 
                a.download = fileName; 
                a.style.display='none';
                document.body.appendChild(a);
                a.click(); 
                window.URL.revokeObjectURL(url);
              }
        },
        *uploadFinanceFile({payload}, {call,put}) { // 导入文件
            try {
                yield call (uploadFinanceFile, payload);
                message.success('文件导入成功');
            } catch (err) {
								notification.error({
									message: '清单导入失败',
									description: err.message || '文件导入失败',
									duration: null,
								})
            }
        },
        *recountFileList({payload}, {call,put}) { // 重新计算
            try {
                const response = yield call (recountFileList, payload);
                notification.info({
                  message: '重新计算',
                  description: response.message || '上传成功，正在等待后台审核',
                  duration: null,
                });
            } catch (err) {
              notification.error({
                message: '重新计算',
                description: err.message || '重新计算失败',
                duration: null,
              })
            }
				},
				*importOfflineInfo({payload}, { call, put}) {	// 线下退费清单导入
					try {
						const response = yield call (importOfflineInfo, payload);
						message.success('文件导入成功');
					} catch (err) {
						notification.error({
							message: '清单导入失败',
							description: err.message || '文件导入失败',
							duration: null,
						})
					}
				},
        *checkConsistency({payload}, {call,put}) { // 审核完成
          try {
            const response = yield call (checkConsistency, payload);
            notification.info({
              message: '核对',
              description: response.message || '审核完成',
              duration: null,
            });
          } catch (err) {
            notification.error({
							message: '核对',
							description: err.message || '审核失败',
							duration: null,
						})
          }
        },
    },

    reducers: {
        save(state, action) {
        return {
            ...state,
            ...action.payload,
        };
        },
    },
 }

 