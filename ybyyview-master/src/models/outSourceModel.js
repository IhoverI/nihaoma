import outSourceService from '../services/outSourceService';
import { isMoment } from 'moment';
import moment from 'moment';

export default {
  namespace: 'outSource',
  state: {
    list: [],
    data: {},
    groupData:[],
    pagination: {},
    outSourceDto:{}
  },

  effects: {
    //查询所有劳务派遣公司
    *selectOutSourceInfo({data}, { call, put }){
      const result = yield call(outSourceService.selectOutSourceInfo, {
        data:data,
      });
      yield put({
        type: 'initOutSourceInfo',
        payload: result,
      });
    },
    //查询团队名称
    *searchGroup({data}, { call, put }){
      console.log('searchGroup called...');
      const result = yield call(outSourceService.searchGroup, {
        data:data,
      });
      console.log('result...');
      console.log(result);
      yield put({
        type: 'initSearchGroup',
        payload: result,
      });
    },
    //修改派遣公司信息
    *upadateOutSource({data}, { call, put }){
      console.log("model upadateOutSource called...");
      let saveData = {
        ...data,
      }
      const result = yield call(outSourceService.upadateOutSource, {
        data:saveData,
      });
    },
    //新增派遣公司信息
    *insertOutSource({data}, { call, put }){
      console.log("model insertOutSource called...");
      console.log(data);
      let saveData = {
        ...data,
      }
     
      const result = yield call(outSourceService.insertOutSource, {
        data:saveData,
      });
      yield put({
        type: 'initAddOutSource',
        payload: result,
      });
    },
    
    //删除派遣公司
    *deleteOutSource({data}, { call, put }) {
      console.log("model deleteOutSource called...");
      const result = yield call(outSourceService.deleteOutSource, {
        data
      });
    },

  },

  reducers: {
    //初始化劳务派遣公司数据
    initOutSourceInfo(state, { payload }) {
      return {
        ...state,
        data: payload.data,
        list: payload.data.list,
      };
    },
    //初始化团队名称数据
    initSearchGroup(state, { payload }) {
      console.log('initSearchGroup called...');
      return {
        ...state,
        groupData: payload.data,
      };
    },
    initAddOutSource(state, { payload }){
      return {
        ...state,
        outSourceDto: payload.data
      };
    }
  }
}
