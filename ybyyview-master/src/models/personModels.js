import { message } from 'snk-web';
import { searchPerson,downloadPerson, selectGroupName, selectTeamName, findPersonnelHisInfo, selectOutSourceName,
  selectPositionName, findPersonnel, selectRankName, addPersonnel, checkIdentityCode, insertTeamRecord,
  deleteTeamRecord, updateTeamRecord, insertRankRecord, updateRankRecord, updatePersonnel,deletePersonnel,checkIfShowGroupForm,downloadModal,checkIfHasAuditRole,auditRankRecord } from '../services/personService';
import { download } from '../services/api';

export default {

  namespace: 'personModels',

  state: {
    searchPersonResp: {},
    groupNames: {},
    teamNames: {},
    personChange: {},
    outsourceId: {},
    positionNames: {},
    personDetail: {},
    rankNames: {},
    addResp: {},
    checkExistResp: {},
    insertTeamRecordResp: {},
    updateTeamDataSourceResp: {},
    updateRankDataSourceResp:{},
    deleteTeamRecordResp: {},
    updateTeamRecordResp: {},
    insertRankRecordResp: {},
    updateRankRecordResp: {},
    updatePersonnelResp: {},
    deletePersonnelResp: {},
    groupFormVisibleData:{},
    ifHasAuditRole:false,
    auditRankRecord:false,
  },

  effects: {

    // 查询员工列表
    *searchPerson({ payload }, { call, put }) {
      const response = yield call(searchPerson, payload);
      if (response.code === 0) {
        message.success(response.message);
      } else {
        message.error(response.message);
        return;
      }
      yield put({
        type: 'save',
        payload: {
          searchPersonResp: response,
        }
      });
      console.log('searchPerson called..');
      console.log(response);
    },

    *downloadModal({payload},{call}){
      const response = yield call(downloadModal, payload);
      const fileName = "人员信息模板.xls"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }

    },

    *downloadPerson({payload},{call}){
      const response = yield call(downloadPerson, payload);
      const fileName = "人员信息表.xls"; 
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.click(); 
        window.URL.revokeObjectURL(url);
      }

    },

    // 团队下拉列表
    *selectGroupName({ payload }, { call, put }) {
      const response = yield call(selectGroupName, payload);
      yield put({
        type: 'save',
        payload: {
          groupNames: response,
        }
      });
    },
      // 是否显示团队下拉
      *checkIfShowGroupForm({ payload }, { call, put }) {
        const response = yield call(checkIfShowGroupForm, payload);
        yield put({
          type: 'save',
          payload: {
            groupFormVisibleData: response.data,
          }
        });
      },

    // 根据团队查询组别列表
    *selectTeamName({ payload }, { call, put }) {
      const response = yield call(selectTeamName,  payload);
      yield put({
        type: 'save',
        payload: {
          teamNames: response,
        }
      });
      console.log('model selectTeamName called...');
      console.log(payload);
      console.log(response);
    },

    // 员工历史记录
    *findPersonnelHisInfo({ payload }, { call, put }) {
      const response = yield call(findPersonnelHisInfo, payload);
      yield put({
        type: 'save',
        payload: {
          personChange: response,
        }
      });
    },

    // 查询所有的外包公司
    *selectOutSourceName({ payload }, { call, put }) {
      const response = yield call(selectOutSourceName, payload);
      yield put({
        type: 'save',
        payload: {
          outsourceId: response,
        }
      });
    },

    // 查询所有的培训岗位
    *selectPositionName({ payload }, { call, put }) {
      const response = yield call(selectPositionName, {data: payload});
      yield put({
        type: 'save',
        payload: {
          positionNames: response,
        }
      });
    },
    
    // 查询员工详细信息
    *findPersonnel({ payload }, { call, put }) {
      const response = yield call(findPersonnel, payload);
      yield put({
        type: 'save',
        payload: {
          personDetail: response,
        }
      });
      console.log(response);
    },

    // 根据岗位查询职级
    *selectRankName({ payload }, { call, put }) {
      const response = yield call(selectRankName, payload);
      yield put({
        type: 'save',
        payload: {
          rankNames: response,
        }
      });
      console.log('selectRankName called...');
      console.log(payload);
      console.log(response);
    },
    
    // 根据岗位查询职级
    *addPersonnel({ payload }, { call, put }) {
      const response = yield call(addPersonnel, payload);
      yield put({
        type: 'save',
        payload: {
          addResp: response,
        }
      });
    },

    // 根据岗位查询职级
    *checkIdentityCode({ payload }, { call, put }) {
      const response = yield call(checkIdentityCode, payload);
      yield put({
        type: 'save',
        payload: {
          checkExistResp: response,
        }
      });
    },

    // 新增挂靠组别
    *insertTeamRecord({ payload }, { call, put }) {
      const response = yield call(insertTeamRecord, payload);
      yield put({
        type: 'save',
        payload: {
          insertTeamRecordResp: response,
        }
      });
    },

    // 刷新员工组别信息
    *updateTeamDataSource({ payload }, { call, put }) {
      const response = yield call(findPersonnel, payload);
      yield put({
        type: 'save',
        payload: {
          updateTeamDataSourceResp: response,
        }
      });
    },

    // 删除挂靠组别
    *deleteTeamRecord({ payload }, { call, put }) {
      const response = yield call(deleteTeamRecord, payload);
      yield put({
        type: 'save',
        payload: {
          deleteTeamRecordResp: response,
        }
      });
    },

    // 更新挂靠组别
    *updateTeamRecord({ payload }, { call, put }) {
      const response = yield call(updateTeamRecord, payload);
      yield put({
        type: 'save',
        payload: {
          updateTeamRecordResp: response,
        }
      });
    },
    
    // 添加挂靠职级
    *insertRankRecord({ payload }, { call, put }) {
      const response = yield call(insertRankRecord, payload);
      yield put({
        type: 'save',
        payload: {
          insertRankRecordResp: response,
        }
      });
    },
    
    // 添加挂靠职级
    *updateRankRecord({ payload }, { call, put }) {
      const response = yield call(updateRankRecord, payload);
      yield put({
        type: 'save',
        payload: {
          updateRankRecordResp: response,
        }
      });
    },
    
    // 添加挂靠职级
    *updatePersonnel({ payload }, { call, put }) {
      const response = yield call(updatePersonnel, payload);
      yield put({
        type: 'save',
        payload: {
          updatePersonnelResp: response,
        }
      });
    },
     // 删除职级
     *deleteRankRecord({ payload }, { call, put }) {
      const response = yield call(deletePersonnel, payload);
      yield put({
        type: 'save',
        payload: {
          deletePersonnelResp: response,
        }
      });
    },
     // 刷新员工组别信息
     *updateRankDataSource({ payload }, { call, put }) {
      const response = yield call(findPersonnel, payload);
      console.log(response)
      yield put({
        type: 'save',
        payload: {
          updateRankDataSourceResp: response,
        }
      });
    },
    //查询是否有审核权限
    *checkIfHasAuditRole({payload},{call,put}){
      let response = yield call(checkIfHasAuditRole, payload);
      yield put({
        type: 'save',
        payload: {
          ifHasAuditRole: response.data,
        }
      });
    },
    //查询是否有审核权限
    *auditRankRecord({payload},{call,put}){
      let response = yield call(auditRankRecord, payload);
      console.log(response)
      yield put({
        type: 'save',
        payload: {
          auditRankRecord: response.data,
        }
      });
    }
    
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
}