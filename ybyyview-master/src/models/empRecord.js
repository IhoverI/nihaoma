import teamRecordService from '../services/teamRecord';

export default {
  namespace: 'empRecord',

  state: {
    selectList: [],
    data: {},
    pagination: {},
  },

  effects: {
    *search_team_record(_, { call, put }) {
      const data1 = yield call(teamRecordService.searchRecord, {
        
      });
      console.log("search_team_record called...");
      //console.log(data1);
      
      let data = {
        recordList:[{
        teamName: '高铁组',
        bgnTime: '1514736000',
        endTime: '1546272000',
        key:'27',
      }, {
        teamName: '飞机组',
        bgnTime: '1514736000',
        endTime: '1546272000',
        key:'28',
      },],
      selectList:[{
        teamName: '高铁组',
        key: '27',
      },{
        teamName: '飞机组',
        key: '28',
      },],
    };

    console.log(data);
     // data = data1.positionList;

      // const response = yield call(queryActivities);
      yield put({
        type: 'initRecord',
        payload: data,
      });
    },

    *addPosition({data}, { call, put }) {
      console.log("model addPosition called...");
      console.log(data);
      const result = yield call(positionsService.addPosition, {
        data
      });
    },

    *editPosition({data}, { call, put }) {
      console.log("model editRank called...");
      console.log(data);
      const data1 = yield call(positionsService.editPosition, {
        data
      });
    },
    
    *editRank({data}, { call, put }) {
      console.log("model editRank called...");
      console.log(data);
      const data1 = yield call(positionsService.editRank, {
        data
      });
    },

    *addRank({data}, { call, put }) {
      console.log("model addRank called...");
      console.log(data);
      const data1 = yield call(positionsService.addRank, {
        data
      });
    },
  },

  reducers: {
    initRecord(state, action) {
      console.log("initRecord called...");
      console.log(action);
      return {
        ...state,
        data: action.payload,
        selectList: action.payload.selectList,
      };
    },
  },
};
