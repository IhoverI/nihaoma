import {
	message,
} from 'snk-web';
import {
  registerSelect,
  workOrder,
  workOrderSelect,
} from '../services/reportPolicyServices';
import {
  SearchPolicyQuery,
} from '../services/searchList'
export default {
  namespace: 'ReportPolicyModel',
  state: {
        date: '',           //进线时间
        // cPlyNo: '',         //保单号
        cMobileNo: '',      //联系电话
        scene:'',
        data:[],  
        pageNum:'',
        pageSize:'',
        data:[]     
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
  effects: {
    // 登记工单前获取数据
    *registerSelect({ payload }, { call, put }) {
      const {scene} = payload
      console.log('登记前',payload);
      let response = yield call(registerSelect, payload);
      console.log('suju',response);
      const { date, cPlyNo, cMobileNo } = response.data;
      yield put({
        type: 'save',
        payload: {
            date,           //进线时间
            cPlyNo,         //保单号
            cMobileNo,      //联系电话
            scene      
        },
      });
    },
    // 新建特殊工单前获取数据
    *beforeCreat({ payload }, { put }) {
      console.log('新建前',payload);
      const { date, scene } = payload;
      yield put({
        type: 'save',
        payload: {
            date, //进线时间
            scene //          
        },
      });
    },
    //新建工单
    *workOrder( { payload }, { call }) {
      console.log('xinjingongdan',payload);
      // try {
        const result= yield call(workOrder, payload);
        console.log('result',result);
        if(result=='0'){
          message.success('新建成功！');
        }
        // const {massage}=result;
        message.info('新增工单登记成功！')
        return result;
      // } catch (data) {
      //   console.log('data',data);
        
      //   message.error(data.message)
      // }  
    },
    //客户信息查询
    *workOrderSelect( { payload }, { call, put }) {
      console.log('信息查询',payload);
      // const data = yield call(workOrderSelect, payload);
      // console.log(data);
      // yield put({
      //   type: 'save',
      //   payload: {
      //       data, //进线时间         
      //   },
      // });
      return yield call(workOrderSelect, payload);
      
      // try {
      //   const result= yield call(workOrderSelect, payload);
      //   return result;
      // } catch (data) {
      //   return data;
      //   message.error(error.message || '没有查询到数据信息');
      // }  
    },
  }
}
