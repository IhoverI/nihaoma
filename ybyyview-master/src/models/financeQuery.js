import { message } from 'snk-web';
import { queryPayChargeInfo, exportFinanceFile, exportFinanceQueryFile, checkConsistency,uploadFinanceFile } from '../services/financeQuery';

 export default {
    namespace: 'financeQuery',
    state: {
        dataSource: [],
        blobURL:{},
        queryFinanceInfo: {},
    },

    effects: {
        *queryPayChargeInfo({payload}, {call,put}) {
            const response = yield call (queryPayChargeInfo, payload);
            const alertMessage = response.message;
            response.code == '0' ? message.success('查询成功！') : message.warning(alertMessage,2);
            yield put ({
                type: 'save',
                payload: {
                    dataSource: response.data,
                },
            });
            
        },

        *exportFinanceQueyrFile({payload}, {call,put}) {
            const response = yield call (exportFinanceQueryFile, payload);
            const fileName = "请扣款清单.xls"
            if('msSaveOrOpenBlob' in navigator){
                window.navigator.msSaveOrOpenBlob(response, fileName);
              } else{
                const a = document.createElement('a'); 
                const url = window.URL.createObjectURL(response);  
                a.href = url; 
                a.download = fileName; 
                a.click(); 
                window.URL.revokeObjectURL(url);
              }
        },

        *uploadFinanceFile({payload}, {call,put}) {
            const response = yield call (uploadFinanceFile, payload);
            if (response) {
                message.success(response.message);
            }
        },

        *checkConsistency({payload}, {call,put}) {
            const response = yield call (checkConsistency, payload);

            if(response){
                if (response.code === "0") {
                    message.success(response.message);
                } else {
                    message.warning(response.message);
                }
            }
        },
    },

    reducers: {
        save(state, action) {
        return {
            ...state,
            ...action.payload,
        };
        },
    },
 }

 