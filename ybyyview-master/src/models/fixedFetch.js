import { postFixedFetch } from "../services/fixedFetch";
import {
	message
} from 'snk-web';

export default {
	namespace: 'fixesFetch',
	state: {

	},
  effects: {
		*getFixedFetch({ payload }, { call, put}) {
			const fetchData = yield call(postFixedFetch, payload);
			const { code, message } = fetchData;
			if(code === 0) {
				message.success(message, 2);
			}
			if(code === 1) {
        message.warning(message, 2);
			}
		},
	},
	reducers: {
		
	}
}