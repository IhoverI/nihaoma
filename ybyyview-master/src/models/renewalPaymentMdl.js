import paymentService from '../services/renewalPaymentService';

export default {
  namespace: 'renewalPaymentMdl',
  state: {
    renewalPayData:[],
    total:0,
    upStatus:""
  },

  effects: {
    //查询所有劳务派遣公司
    *queryRenewalPayment({payload}, { call, put }){
      console.log(payload)
      const result = yield call(paymentService.queryRenewalPayment, payload);
      yield put({
        type: 'initRenewalList',
        payload: result,
      });
    },

    //渠道继续请款
    *updateRenewalThirdFlag({payload}, { call, put }){
      console.log("model updateRenewalThirdFlag called..."+ payload);
      const result = yield call(paymentService.updateRenewalThirdFlag, payload);
      yield put({
        type: 'initUpdateState',
        payload: result,
      });
    },
   
  },

  reducers: {
    //初始化首期无请款续保单数据
    initRenewalList(state, action) {
      console.log(action.payload.data.list)
      return {
        ...state,
        renewalPayData: action.payload.data.list,   
        total:action.payload.data.total
      };
    },

    initUpdateState(state, action) {
      console.log(action.payload.data)
      return {
        ...state,
        upStatus: action.payload.data,   
        
      };
    },
    
  }
}
