import { 
  queryDynamicReport,
  findTemplate,
  findAllDynamicLabel,
  addTemplate,
  fetchDynamicReport,
  importQuery,
  downLoadTemplate } from '../services/report';

import {message} from 'snk-web';
import { checkResponse } from '../utils/utils';
export default {
  namespace: 'report',

  state: {
    DynamicReportData: {},
    TemplateData:{},
    DynamicLabelData:[],
    selectedTemplateIndex:-1,
    tableFields:{},
    blobURL:{}
  },

  effects: {
    *queryDynamicReport({ payload }, { call, put }) {
      const response = yield call(queryDynamicReport, payload);
      checkResponse(response);
      yield put({
        type: 'save',
        payload: {
          DynamicReportData:response.data
        },
      });
    },

    // 个性报表
    *fetchDynamicReport({ payload }, { call, put }) {
      const response = yield call(fetchDynamicReport, payload);
      message.success(response.message);
      // const objectURL = URL.createObjectURL(response);
      // const a = document.createElement('a'); 
      // const url = window.URL.createObjectURL(response);  
      // a.href = url; 
      // a.download = "个性报表取数.xls"; 
      // a.style.display='none';
      // document.body.appendChild(a);
      // a.click(); 
      // window.URL.revokeObjectURL(url);
      // document.body.removeChild(a);
    },
    
    *updateTableField({ payload }, { call, put }){
      yield put({
        type: 'save',
        payload: {
          tableFields:payload.fileds
        },
      });
    },
    *templateSelected({ payload }, { call, put }){
      yield put({
        type: 'save',
        payload: {
          selectedTemplateIndex:payload.index,
          tableFields:payload.fields||{}
        },
      });
    },
    *addTemplate({ payload }, { call, put,select }) {
      const response = yield call(addTemplate, payload);
      const TemplateData = yield select(state => state.report.TemplateData);
      checkResponse(response);
      const fields = JSON.parse(payload.dataJs);
      const field = [];
      const label = [];
      for(var key in fields){
        field.push(key);
        label.push(fields[key]);
      }

      TemplateData.push({
        field,
        label,
        templateName:payload.templateName
      });
      yield put({
        type: 'save',
        payload: {
          TemplateData:TemplateData,
        },
      });
    },
    *findAllDynamicLabel({ payload }, { call, put }) {
      const response = yield call(findAllDynamicLabel, payload);
      checkResponse(response);
      yield put({
        type: 'save',
        payload: {
          DynamicLabelData:response.data
        },
      });
    },
    *findTemplate(_action, { call, put }) {
      const response = yield call(findTemplate);
      checkResponse(response);
      yield put({
        type: 'save',
        payload: {
          TemplateData:response.data
        },
      });
    },

    *importQuery({ payload },{call,put}) {
      const response = yield call (importQuery,{file:payload.file});
      const fileName = "个性报表导入查询.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const url = window.URL.createObjectURL(response)
        const a = document.createElement('a'); 
        a.href = url; 
        a.download = fileName; 
        a.style.display='none';
        document.body.appendChild(a);
        a.click(); 
        window.URL.revokeObjectURL(url);
      }
    },

    *downLoadTemplate(payload,{call,put}) {
      const response = yield call (downLoadTemplate);
      const fileName = "个性报表导入模板.xls";
      if('msSaveOrOpenBlob' in navigator){
        window.navigator.msSaveOrOpenBlob(response, fileName);
      } else{
        const a = document.createElement('a'); 
        const url = window.URL.createObjectURL(response);  
        a.href = url; 
        a.download = fileName; 
        a.style.display='none';
        document.body.appendChild(a);
        a.click(); 
        window.URL.revokeObjectURL(url);
        document.body.removeChild(a);
      }
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
