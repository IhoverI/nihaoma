import {
	message,
} from 'snk-web';
import {
    workAllotStatistics,
    workDataDetail
} from '../services/dataDistributionServices';
export default {
  namespace: 'dataDistribution',
  state: {
        date: '',           //进线时间
        cPlyNo: '',         //保单号
        cMobileNo: '',      //联系电话
        scene:'',   
        list:[],
  },
  reducers: {
    save(state, {payload}) {
      return {
        ...state,
        ...payload,
      };
    },
  },
  effects: {
    // 工单分配统计分配统计
    *workAllotStatistics({ payload }, { call, put }) {
      console.log('分配统计',payload);
      return yield call(workAllotStatistics, payload);
      // let response = yield call(workAllotStatistics, payload);
      // console.log('分配统计显示',response);
      // const {message, list, total } = response
      // yield put({
      //   type: 'save',
      //   payload: {
      //     total, 
      //     list,        
      //   },
      // });
      // message.info(message)
    },
    // 数据明细
    *workDataDetail({ payload }, { call, put }) {
      console.log('数据明细',payload);
      return yield call(workDataDetail, payload);
    },
    // 申请
    *statisticsApply({ payload }, { call, put }) {
      console.log('申请',payload);
      debugger
     const data = yield call(statisticsApply, payload);
     console.log(data);
     
    },
    // 新建特殊工单前获取数据
    *beforeCreat({ payload }, { put }) {
      console.log('新建前',payload);
      const { date, scene } = payload;
      yield put({
        type: 'save',
        payload: {
            date, //进线时间
            scene //          
        },
      });
    },
    //新建工单
    *workOrder( { payload }, { call }) {
      console.log('xinjingongdan',payload);
      // try {
        const result= yield call(workOrder, payload);
        console.log('result',result);
        if(result=='0'){
          message.success('新建成功！');
        }
        // const {massage}=result;
        message.info('新增工单登记成功！')
        return result;
      // } catch (data) {
      //   console.log('data',data);
        
      //   message.error(data.message)
      // }  
    },
  },
}