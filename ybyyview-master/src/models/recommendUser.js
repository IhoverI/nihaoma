import recommendsService from '../services/recommends';
import { isMoment } from 'moment';
import moment from 'moment';

export default {
  namespace: 'recommendUser',
  state: {
    list: [],
    data: {},
    deptCode: '',
  },

  effects: {
    *searchUserDeptCode({data}, { call, put }) {
      const result = yield call(recommendsService.searchUserDeptCode, {
        data
      });
      console.log(result);
      yield put({
        type: 'initDeptCode',
        payload: result,
      });
    },
  },

  reducers: {
    initDeptCode(state, { payload }){
      return {
        ...state,
        deptCode: payload.data.DEPTCODE,
        data: payload.data,
      }
    },
  },
};
