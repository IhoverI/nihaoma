import fetch from 'dva/fetch';
import { notification } from 'snk-web';
import { routerRedux } from 'dva/router';
import store from '../index';

const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};
const upSet= new Set();
[
  `${SERVERLOCAL}ybyy-policy/policy/dynamicReport/importQuery`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/uploadExpressBackFile`,
  `${SERVERLOCAL}ybyy-policy/policy/QueryAskForPayDeduct/uploadFTPFile`,
  `${SERVERLOCAL}ybyy-policy/policy/DataFetch/uploadSukeyBackFile`
].forEach( s =>{upSet.add(s)} );

const downSet= new Set();
[
  `${SERVERLOCAL}ybyy-policy/policy/CreateCheckingFile/exportFinanceFile`,
  `${SERVERLOCAL}ybyy-policy/policy/FixedReport/fixedReportDownload`,
  `${SERVERLOCAL}ybyy-policy/policy/DownloadFile/downloadFTPFile`,
  `${SERVERLOCAL}ybyy-policy/policy/RenewalQueryDownload/renewalQueryDownload`,
  `${SERVERLOCAL}ybyy-policy/policy/DownloadPreGuaranteeSlip/downloadPreGuaranteeSlip`,
  `${SERVERLOCAL}ybyy-policy/policy/fastMoney/exportExcel`,
  `${SERVERLOCAL}ybyy-policy/policy/dynamicReport/downLoadTemplate`,
  `${SERVERLOCAL}ybyy-policy/policy/dynamicReport/importQuery`,
  `${SERVERLOCAL}ybyy-policy/policy/DownloadFile/downloadFile`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/exportExpressReceiptExcel`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/exportExpressQuestionInfo`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/uploadExpressBackFile`,
  `${SERVERLOCAL}ybyy-policy/policy/DataFetch/uploadSukeyBackFile`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/exportFeeCollectExcel`,
  `${SERVERLOCAL}ybyy-policy/policy/PayCounteroffer/exportPayCounterofferInfoExcel`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/exportSettlementDealExcel`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/expressAddress`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/exportPrintExpressInfo`,
  `${SERVERLOCAL}ybyy-policy/policy/DataFetch/exportSukeyExcel`,
  `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/guaranteeReprintExport`,
  `${SERVERLOCAL}ybyy-policy/policy/search/exportExcel`,
  `${SERVERLOCAL}ybyy-policy/policy/report/exportOnlineReport`,
   `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/exportPrintInfos`,
   `${SERVERLOCAL}ybyy-integral/integral/largess/download`,
   `${SERVERLOCAL}ybyy-integral/integral/largess/errorFile`,
   `${SERVERLOCAL}ybyy-integral/integral/report/whole/download`,
   `${SERVERLOCAL}ybyy-integral/integral/report/cusRetentionRate/download`,
   `${SERVERLOCAL}ybyy-integral/integral/report/customer/download`, // 客户整体
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/search/exportOrderMsg`,
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/express/exportExpressOrder`,
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/exportStockInfo`,
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/exportExpressReport`,
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/downLoadLineTemplate`,
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/downLoadPackageTemplate`,
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/downLoadGeneBoxTemplate`,
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/exportValueAddedService`,
   `${SERVERLOCAL}ybyy-policy/policy/BatchCancel/exportPendingCancelExcl`, // 批量注销清单导出
   `${SERVERLOCAL}ybyy-valueadded/valueAdded/extendBatch/exportExcel`, //红包清单导出

   `${SERVERHR}emp/filterEmp`,
   `${SERVERHR}emp/collectExport`,
   `${SERVERHR}emp/downloadRank`,
   `${SERVERHR}emp/downloadSalary`,
   `${SERVERHR}emp/exportSalaryResult`,
   `${SERVERHR}emp/collectExport`,
   `${SERVERHR}emp/downloadEmpTemplate`,
   `${SERVERHR}emp/exportPolicyExcel`,
   `${SERVERHR}emp/exportPersonInfo`
].forEach( s =>{downSet.add(s)} );


function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const errortext = codeMessage[response.status] || response.statusText;
  notification.error({
    message: `请求错误 ${response.status}: ${response.url}`,
    description: errortext,
  });
  const error = new Error(errortext);
  error.name = response.status;
  error.response = response;
  throw error;
}


export default function request(url, options) {
  const defaultOptions = {
    credentials: 'include',
  };
  const newOptions = { ...defaultOptions, ...options };
  if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
      if (!(newOptions.body instanceof FormData)) {
        newOptions.headers = {
        Accept: '*/*;type=ajax',
        'Content-Type': 'application/json; charset=utf-8',
        ...newOptions.headers,
      };
      newOptions.body = JSON.stringify(newOptions.body);
    } else if(upSet.has(url)){
      newOptions.headers = {
      Accept: '*/*;type=ajax',
      ...newOptions.headers,
    };
    } else{
      newOptions.headers = {
      Accept: '*/*;type=ajax',
      'Content-Type': 'multipart/form-data',
      ...newOptions.headers,
    };
    }
  }
  return fetch(url, newOptions)
    .then(checkStatus)
    .then(response => {
      const { dispatch } = store;
      if(response.status==202){
        const img = document.createElement('img');
        img.src = SSOLOGOUTSERVER;
        try {
          setTimeout(() => {
            dispatch({
              type: 'umssouserinfo/getUserInfo',
            }).then(() => {
            });
          }, 300);
        } catch (e) {
          console.log(e);
        }
        return;
      }
      if (newOptions.method === 'DELETE' || response.status === 204) {
        return response.text();
      }
      if (downSet.has(url)) {
        console.log(response)
        return response.blob();
      }
        return response.json();
   }).catch(e => {
      const { dispatch } = store;
      const status = e.name;
     
      if (status === 401) {
        dispatch({
          type: 'login/logout',
        });
        return;
      }
      if (status === 403) {
        dispatch(routerRedux.push('/exception/403'));
        return;
      }
      if (status <= 504 && status >= 500) {
        dispatch(routerRedux.push('/exception/500'));
        return;
      }
      // if (status >= 404 && status < 422) {
      //   dispatch(routerRedux.push('/exception/404'));
      //   return;
      // }
      throw e;
    });
}
