import { HttpClient } from 'snk-libs';

const httpClient = new HttpClient();

/**
 * Created on 2017/4/28.
 * @author JarkimZhu
 * @class
 */
export default class HttpUtils {
  static _seq = 0;

  static _createUrl(params,service) {
    if(service=='hr'){
      const server = params.server || SERVERHR;
      return server + params.url;
    }
    const server = params.server || SERVERLOCAL;
    // console.log(server + params.url)
    return server + params.url;
  }

  static async post(params) {
    // url, body
    const service=params.url.indexOf('emp')>-1?'hr':'yb'
    const uri = HttpUtils._createUrl(params,service);
    const request = HttpUtils._createRequest(params.body);
    request.headers = params.headers || {};
    const response = await httpClient.post(uri, request);
  
    return HttpUtils._processResponse(response,service);
  }

  static async get(params) {
    const service=params.url.indexOf('emp')>-1?'hr':'yb'
    const uri = HttpUtils._createUrl(params,service);
    const request = {
      headers: {
        Seq: HttpUtils._seq += 1,
      },
    };
    request.headers = params.headers || {};
    const response = await httpClient.get(uri, request);
    return HttpUtils._processResponse(response);
  }

  static async put(params) {
    const uri = HttpUtils._createUrl(params);
    const request = HttpUtils._createRequest(params.body);
    const response = await httpClient.put(uri, request);
    return HttpUtils._processResponse(response);
  }

  static async delete(params) {
    const uri = HttpUtils._createUrl(params);
    const request = HttpUtils._createRequest(params.body);
    const response = await httpClient.delete(uri, request);
    return HttpUtils._processResponse(response);
  }

  static async patch(params) {
    const uri = HttpUtils._createUrl(params);
    const request = HttpUtils._createRequest(params.body);
    const response = await httpClient.patch(uri, request);
    return HttpUtils._processResponse(response);
  }

  static async upload(params) {
    // url body pcb
    const uri = HttpUtils._createUrl(params);
    const request = HttpUtils._createRequest(params.body);
    request.headers = params.headers || {};

    const response = await httpClient.upload(uri, request, params.pcb);

    return HttpUtils._processResponse(response);
  }

  static _createRequest(body) {
    const request = {
      headers: {
        Seq: HttpUtils._seq += 1,
      },
      body,
    };
    return request;
  }

  static async _processResponse(response,service) {
    /**
     * 没有流数据请求到这里
     */
    // if(response._xhr.responseURL.indexOf("dynamicReport/importQuery")>0
    // ||response._xhr.responseURL.indexOf("ExpressManage/uploadExpressBackFile")>0){
    //   const blob = await response.blob();
    //   return blob;
    // }
    const json = await response.json();
    const { code, message, data } = json;
    if(service=='yb'){
      if (code === 0) {
        return data;
      } else {
        const err = { code, message };
        throw err;
      }
    }else{
      if (code === 0) {
        return json;
      } else {
        const err = json;
  
        throw err;
      }
    }
    
  }
}
