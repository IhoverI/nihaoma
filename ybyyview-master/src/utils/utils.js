import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';


export function fixedZero(val) {
  return val * 1 < 10 ? `0${val}` : val;
}

export function getTimeDistance(type) {
  const now = new Date();
  const oneDay = 1000 * 60 * 60 * 24;

  if (type === 'today') {
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    return [moment(now), moment(now.getTime() + (oneDay - 1000))];
  }

  if (type === 'week') {
    let day = now.getDay();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);

    if (day === 0) {
      day = 6;
    } else {
      day -= 1;
    }

    const beginTime = now.getTime() - day * oneDay;

    return [moment(beginTime), moment(beginTime + (7 * oneDay - 1000))];
  }

  if (type === 'month') {
    const year = now.getFullYear();
    const month = now.getMonth();
    const nextDate = moment(now).add(1, 'months');
    const nextYear = nextDate.year();
    const nextMonth = nextDate.month();

    return [
      moment(`${year}-${fixedZero(month + 1)}-01 00:00:00`),
      moment(moment(`${nextYear}-${fixedZero(nextMonth + 1)}-01 00:00:00`).valueOf() - 1000),
    ];
  }

  if (type === 'year') {
    const year = now.getFullYear();

    return [moment(`${year}-01-01 00:00:00`), moment(`${year}-12-31 23:59:59`)];
  }
}

export function getPlainNode(nodeList, parentPath = '') {
  const arr = [];
  nodeList.forEach(node => {
    const item = node;
    item.path = `${parentPath}/${item.path || ''}`.replace(/\/+/g, '/');
    item.exact = true;
    if (item.children && !item.component) {
      arr.push(...getPlainNode(item.children, item.path));
    } else {
      if (item.children && item.component) {
        item.exact = false;
      }
      arr.push(item);
    }
  });
  return arr;
}

export function digitUppercase(n) {
  const fraction = ['角', '分'];
  const digit = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
  const unit = [['元', '万', '亿'], ['', '拾', '佰', '仟']];
  let num = Math.abs(n);
  let s = '';
  fraction.forEach((item, index) => {
    s += (digit[Math.floor(num * 10 * 10 ** index) % 10] + item).replace(/零./, '');
  });
  s = s || '整';
  num = Math.floor(num);
  for (let i = 0; i < unit[0].length && num > 0; i += 1) {
    let p = '';
    for (let j = 0; j < unit[1].length && num > 0; j += 1) {
      p = digit[num % 10] + unit[1][j] + p;
      num = Math.floor(num / 10);
    }
    s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
  }

  return s
    .replace(/(零.)*零元/, '元')
    .replace(/(零.)+/g, '零')
    .replace(/^整$/, '零元整');
}

function getRelation(str1, str2) {
  if (str1 === str2) {
    console.warn('Two path are equal!'); // eslint-disable-line
  }
  const arr1 = str1.split('/');
  const arr2 = str2.split('/');
  if (arr2.every((item, index) => item === arr1[index])) {
    return 1;
  } else if (arr1.every((item, index) => item === arr2[index])) {
    return 2;
  }
  return 3;
}

function getRenderArr(routes) {
  let renderArr = [];
  renderArr.push(routes[0]);
  for (let i = 1; i < routes.length; i += 1) {
    let isAdd = false;
    // 是否包含
    isAdd = renderArr.every(item => getRelation(item, routes[i]) === 3);
    // 去重
    renderArr = renderArr.filter(item => getRelation(item, routes[i]) !== 1);
    if (isAdd) {
      renderArr.push(routes[i]);
    }
  }
  return renderArr;
}

/**
 * Get router routing configuration
 * { path:{name,...param}}=>Array<{name,path ...param}>
 * @param {string} path
 * @param {routerData} routerData
 */
export function getRoutes(path, routerData) {
  let routes = Object.keys(routerData).filter(
    routePath => routePath.indexOf(path) === 0 && routePath !== path
  );
  // Replace path to '' eg. path='user' /user/name => name
  routes = routes.map(item => item.replace(path, ''));
  // Get the route to be rendered to remove the deep rendering
  const renderArr = getRenderArr(routes);
  // Conversion and stitching parameters
  const renderRoutes = renderArr.map(item => {
    const exact = !routes.some(route => route !== item && getRelation(route, item) === 1);
    return {
      exact,
      ...routerData[`${path}${item}`],
      key: `${path}${item}`,
      path: `${path}${item}`,
    };
  });
  return renderRoutes;
}

export function timestampToString (time){
  var datetime = new Date();
   datetime.setTime(time);
   var year = datetime.getFullYear();
   var month = datetime.getMonth() + 1;
   var date = datetime.getDate();
   var hour = datetime.getHours();
   var minute = datetime.getMinutes();
   var second = datetime.getSeconds();
   var mseconds = datetime.getMilliseconds();
   return year + "-" + month + "-" + date;
   //+" "+hour+":"+minute+":"+second+"."+mseconds;
};

export function datetimeFormat (time){
  var datetime = new Date();
   datetime.setTime(time);
   var year = datetime.getFullYear();
   var month = datetime.getMonth() + 1;
   var date = datetime.getDate();
   var hour = datetime.getHours();
   var minute = datetime.getMinutes();
   var second = datetime.getSeconds();
   return year + "-" + month + "-" + date + " "+hour+":"+minute+":"+second;
};


/* eslint no-useless-escape:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/g;

export function isUrl(path) {
  return reg.test(path);
}

export function getRegularCheck(param) {
  const ruleCheck = {
    certNo: {
      rules: [
        { required: true, message: '请输入证件号码' },
        { pattern: /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, message: '请输入合法证件号码' },
      ],
    },
    cardId: {
      rules: [
        { required: true, message: '请输入扣款账号' },
        { pattern: /^(\d+)((?:\.\d+)?)$/, message: '请输入合法卡号' },
      ],
    },
    //允许数字横杠斜杠
    validity: {
      rules: [
        { required: true, message: '请输入' },
        { pattern: /^[\d/-]*$/, message: '请确认输入合法' },
      ],
    },
    //日期范围不能超过一个月
  };

  return ruleCheck[param];
}

export function MoneyFormat(val){
  if(!val){
    return 0;
  }
  if(isNaN(val)){
    return 0;
  }
  val = parseFloat(val);
  //金额转换保留2位小数 并每隔3位用逗号分开 1,234.56
  var str = (val).toFixed(2) + '';
  var intSum = str.substring(0,str.indexOf(".")).replace( /\B(?=(?:\d{3})+$)/g, ',' );//取到整数部分
  var dot = str.substring(str.length,str.indexOf("."))//取到小数部分搜索
  var ret = intSum + dot;
  return ret;
}

function Trim(x) {
  x = x||'';
  return x.replace(/^\s+|\s+$/gm,'');
}

export function CheckIdCard(id) {
  id = Trim(id);
    if(id.length===15){
      if(! /^[1-9]\d{7}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}$/.test(id)){
        return "身份证号码不合规";
      }
      // 15  位转 18位
      var arrInt = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);   
      var arrCh = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');   
      var cardTemp = 0, i;     
      var card = id.substr(0, 6) + '19' + id.substr(6, id.length - 6);  
      for(i = 0; i < 17; i ++)   
      {   
          cardTemp += card.substr(i, 1) * arrInt[i];   
      }   
      card += arrCh[cardTemp % 11];   
      id = card;
    }
  // 1 "验证通过!", 0 //校验不通过
     var format = /^(([1][1-5])|([2][1-3])|([3][1-7])|([4][1-6])|([5][0-4])|([6][1-5])|([7][1])|([8][1-2]))\d{4}(([1][9]\d{2})|([2]\d{3}))(([0][1-9])|([1][0-2]))(([0][1-9])|([1-2][0-9])|([3][0-1]))\d{3}[0-9xX]$/;
     //号码规则校验
     if(!format.test(id)){
         return "身份证号码不合规";
     }
     //区位码校验
     //出生年月日校验   前正则限制起始年份为1900;
     var year = id.substr(6,4),//身份证年
         month = id.substr(10,2),//身份证月
         date = id.substr(12,2),//身份证日
         time = Date.parse(month+'-'+date+'-'+year),//身份证日期时间戳date
         now_time = Date.parse(new Date()),//当前时间戳
         dates = (new Date(year,month,0)).getDate();//身份证当月天数
     if(time>now_time||date>dates){
         return "出生日期不合规";
     }
     //校验码判断
     var c = new Array(7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2);   //系数
     var b = new Array('1','0','X','9','8','7','6','5','4','3','2');  //校验码对照表
     var id_array = id.split("");
     var sum = 0;
     for(var k=0;k<17;k++){
         sum+=parseInt(id_array[k])*parseInt(c[k]);
     }
     if(id_array[17].toUpperCase() != b[sum%11].toUpperCase()){
         return "身份证校验码不合规"
     }
     return "success"
}

const payChannel1 = [
  {name: '招商银行',code: '95555'},
  {name: '民生银行', code: '95568'},
  {name: '建设银行', code: '95533'},
  {name: '盛大', code: '80001'},
  {name: '江苏邮政', code: '95580'},
  {name: '深圳发展银行', code: '95501'},
  {name: '深圳陌call', code: '10010'},
  {name: '华安保险分公司推广', code: '95556'},
  {name: '微信出单', code: '11002'},
  {name: '汇金保联', code: '11001'},
  {name: '中信银行', code: '95558'}, 
  {name: '转银联', code: '95516'},
  {name: '转快钱', code: '90001'},
  {name: '南昌电话中心',code:'11004'},
  {name: '成都自建', code: '11003'},
];

const extEnterpDesc = [
  {name: '招商银行', code: '95555'},
  {name: '民生银行', code: '95568'},
  {name: '建设银行', code: '95533'},
  {name: '盛大', code: '80001'},
  {name: '江苏邮政', code: '95580'},
  {name: '深发展', code: '95501'},
  {name: '深圳陌call', code: '10010'},
  {name: '南昌电话中心',code:'11004'},
  {name: '成都自建', code: '11003'},
  {name: '分公司推广', code: '95556'},
  {name: '微信出单', code: '11002'},
  {name: '汇金保联', code: '11001'},
  {name: '中信银行', code: '95558'},
];

const newOrderDataDesc = [
  {name: '招商银行', code: '95555'},
  {name: '民生银行', code: '95568'},
  {name: '南昌电话中心',code:'11004'},
  {name: '成都自建', code: '11003'},
  {name: '中信银行', code: '95558'},
];

const prodDesc = [
  {name: '绚丽人生', code: '0631',group:'95555',state:'已停售'},
  {name: '至尊保', code: '0632',group:'95555',state:'已停售'},
  {name: '尊享保', code: '0634',group:'95555',state:'已停售'},
  {name: '全意保', code: '0656',group:'95555',state:'在售'},
  {name: '福满堂', code: '0827',group:'95555',state:'已停售'},
  {name: '安康无忧', code: '1212',group:'95555',state:'已停售'},
  {name: '珍爱健康', code: '1203',group:'95555',state:'在售'},
  {name: '安健无忧', code: '1213',group:'95555',state:'在售'},
  {name: '康益无忧', code: '1231',group:'95555',state:'在售'},
  {name: '珍享健康', code: '1232',group:'95555',state:'在售'},
  {name: '如意保', code: '0637',group:'95555',state:'在售'},
  {name: '非凡人生', code: '0635',group:'95568',state:'在售'},
  {name: '乐享人生', code: '0639',group:'95568',state:'在售'},
  {name: '畅意人生', code: '0655',group:'95568',state:'在售'},
  {name: '福禄安康', code: '1212',group:'95568',state:'在售'},
  {name: '福佑安康', code: '1218',group:'95568',state:'在售'},
  {name: '尊享健康', code: '1225',group:'95568',state:'暂未开卖'},
  {name: '给力保', code: '0643',group:'95533',state:'已停售'},
  {name: '吉祥安泰', code: '0634',group:'80001',state:'已停售'},
  {name: '苏邮安康', code: '1212',group:'95580',state:'已停售'},
  {name: '苏邮尊享', code: '0633',group:'95580',state:'已停售'},
  {name: '守护天使', code: '0631',group:'95501',state:'已停售'},
  {name: '福瑞安康', code: '1212',group:'95501',state:'已停售'},
  {name: '创意人生', code: '0642',group:'10010',state:'项目暂停'},
  {name: '尊尚人生(深圳陌call)', code: '0636',group:'10010',state:'项目暂停'},
  {name: '祥泰安康(深圳陌call)', code: '1212',group:'10010',state:'项目暂停'},
  {name: '禽流感无忧', code: '1201',group:'10010',state:'项目暂停'},
  {name: '祥泰安康(南昌电话中心)', code: '1212',group:'11004',state:'在售'},
  {name: '尊尚人生(成都自建)', code: '0636',group:'11003',state:'项目暂停'},
  {name: '尊尚人生(南昌电话中心)', code: '0636',group:'11004',state:'在售'},
  {name: '祥泰安康(成都自建)', code: '1212',group:'11003',state:'项目暂停'},
  {name: '尊享人生(分公司推广)', code: '0638',group:'95556',state:'项目暂停'},
  {name: '尊享人生(微信出单)', code: '0638',group:'11002',state:'项目暂停'},
  {name: '尊享人生(汇金保联)', code: '0638',group:'11001',state:'在售'},
  {name: '畅享人生', code: '0648',group:'95558',state:'在售'},
  {name: '如意安康', code: '1213',group:'95558',state:'在售'},
];

export function getChannelNameByValue(value){
  let re = "";
  for(var i=0,j=extEnterpDesc.length;i<j;i++){
    const item = extEnterpDesc[i];
    if(item.code ===value){
      re = item.name;
    }
  }
  return re;
}

export function getpayChannelNameByValue(value){
  let re = "";
  for(var i=0,j=payChannel1.length;i<j;i++){
    const item = payChannel1[i];
    if(item.code ===value){
      re = item.name;
    }
  }
  return re;
}

export function getProductNameByValue(value){
  let re = null;
  for(var i=0,j=prodDesc.length;i<j;i++){
    const item = prodDesc[i];
    if(item.code ===value){
      re = item.name;
    }
  }
  return re;
}

// 通过业务渠道和产品代码，查询产品名称
export function getProductNameByExtEnterpCodeAndProdNo(oldEnterpCode, prondNo){
  let re = null;
  for(var i=0,j=prodDesc.length;i<j;i++){
    const item = prodDesc[i];
    if(item.code === prondNo && item.group === oldEnterpCode){
      re = item.name;
    }
  }
  return re;
}

export function checkResponse(response){
  if(response.code!==0){
    throw response;
    return false;
  }
  return true;
}
export function getDataSource(param) {
  
  const dataSource  = {
    extEnterpDesc:extEnterpDesc,
    newOrderDataDesc: newOrderDataDesc,
    payChannel1:payChannel1,
    prodDesc,
    labelSource: {
      valueAdd: '增值服务',
      prodNo: '产品代码',
      extEnterpCode: '所属渠道',
      teleLeadCode: '电话销售代表编号',
      teleLeadName: '电话销售代表姓名',
      tms: '分期付款次数(判断记录数)',
      payWay: '缴费方式',
      postAddress: '客户要求投递地址',
      prm: '所交保费',
      appBirth: '投保人出生日期',
      signDatetime: '签单日期',
      occupationCodeA: '投保人职业',
      applyCertfCde: '投保人证件号码',
      applyCertfCls: '投保人证件类型',
      applyClntNmeA: '投保人姓名',
      applySex: '投保人性别',
      applyZipCde: '投保人邮编',
      applyClntAddr: '投保人家庭地址',
      applyMobile: '投保人手机号码',
      applyClntMrk: '投保人客户类型',
      applyJobType: '投保人职业',
      applyTel: '投保人家庭电话',
      applyMail: '投保人Email',
      applyContact: '投保人联系人',
      applyLegal: '投保人法定代表人',
      creditCardsDate: '信用卡发卡日期',
      creditCardsType: '信用卡种类',
      cPlyAppNo: '投保单号',
      productPlan: '产品计划',
      applyNumber: '投保份数',
      transationCode: '原订单号',
      tInsrncBgnTm: '保险期间保险起期',
      tInsrncTm: '保险期间',
      tInsrncEndTm: '保险期间保险止期',
      payKind: '缴别',
      nGetPrm: '每期保费',
      cardId: '扣款卡号',
      tsrCode: 'TSR编号',
      tsrName: 'TSR姓名',
      tsrTeam: 'TSR所属小组',
      benfType: '受益方式',
      otherInsrncRelation: '其他受益人与被保人关系',
      nBenfPropB: '受益比例',
      benfClntNmeB: '受益人姓名',
      insrncRelation: '受益人与被保人的关系',
      benfCardId: '受益人证件号码',
      beneTsrInfo: '受益人与TSR基本信息',
      appDate: '投保日期',
      dealDate: '被保人出生年月',
      BirthDate: ['出生日期','出生年月','生日'],
      insrncBirth: '被保人出生日期',
      insrncAddress: '被保人家庭地址',
      insrncMobile: '被保人手机号码',
      insrncName: '被保人姓名',
      insrncSex: '被保人性别',
      insrncPostCode: '被保人邮编',
      identifyLongValid: '证件有效期',
      country: '国籍',
      occupationCodeI: '被保人职业',
      insrncCardId: '被保人证件号码',
      insrncCardType: '被保人证件类型',
      insrncJobType: '被保人职业',
      insrncComAddr: '被保人工作单位',
      insrncDayTel: '被保人日间电话',
      cRelationI: '被保人与投保人关系',
      cAppprofType: '被保人职业',
      cMobileNoI: '被保人手机号码',
      cTelI: '被保人家庭电话',
      cMailI: '被保人Email',
      cPlyNo: '保单号',
      cPlyNoOld: '原始保单号',
      renewalTime: '续保次数',
      oldEnterpCode: '订单渠道',
      prodCName: '产品名称',
      ifonline: '线上线下标志',
      cElcFlag: '电子纸质标志',
      thirdPayCity: '第三方扣款所属城市',
      thirdbank: '第三方扣款银行编码',
      thirdBank: '第三方扣款银行名称',
      thirdBranchBank: '第三方扣款开户行名称',
      thirdcardid: '第三方扣款账号',
      thirdCardDate: '第三方扣款账号有效期',
      thirdAppName: '第三方扣款账户姓名',
      thirdCardType: '第三方扣款账户类型',
      thirdcardtypeDesc: '第三方扣款账户类型描述',
      thirdcerttype: '第三方扣款客户证件类型',
      thirdcerttypeDesc: '第三方扣款客户证件类型描述',
      thirdcertno: '第三方扣款客户证件号码',
      thirdflag: '第三方扣款标志',
      thirdcallresult: '第三方支付回访结果',
      actId: '活动号',
      status: '状态',
      policyInfo: '保单基本信息',
      applyInfo: '投保人基本信息',
      insrncInfo: '被保人基本信息',
      payInfo: '请扣款信息',
      expressInfo: '快递信息',
      imageReceiptInfo: '回执影像信息',
      imagePolicyInfo: '保单影像信息',
      revisitInfo: '回访信息',
      presentInfo: '赠险信息',
      integralInfo: '赠送积分',
      printInfo: '打印信息',
      shortMessage: '短信信息',
      policyLog: '保单操作日志',
      correctionInfo: '保单批改信息',
      cardInfoInfo: '第三方卡号信息',
      cElcFlag: '电子标致',
      benfClntNmeB: '受益人姓名',
      beneficiarySex: '受益人性别',
      beneficiaryCertfCls: '受益人证件类型',
      beneficiaryCertfCde: '受益人证件号',
      identifyExpiry: '证件止期',
      beneficiaryAddr: '受益人地址 ',
      beneficiaryMobile: '受益人电话',
      occupationCodeB: '受益人职业职业',
    },
    selectSource: {
      fastMoneyBusinessType: [
        {name: '转快钱渠道', code: '01'},
        {name: '转银联', code: '03'},
      ],
      printStatusDesc: [
        {name: '待打印', code: '0'},
        {name: '已打印', code: '1'},
        {name: '打印失败', code: '2'},
      ],
      cancelCorrectionType: [
        {name: '注销', code: '01'},
        {name: '犹豫期内退保', code: '02'},
        {name: '无退费退保', code: '03'},
        {name: '全额退费退保', code: '04'},
        {name: '部分退费退保', code: '05'},
      ],
      period: [
        {name: '1期', code: 1},
        {name: '2期', code: 2},
        {name: '3期', code: 3},
        {name: '4期', code: 4},
        {name: '5期', code: 5},
        {name: '6期', code: 6},
        {name: '7期', code: 7},
        {name: '8期', code: 8},
        {name: '9期', code: 9},
        {name: '10期', code: 10},
        {name: '11期', code: 11},
        {name: '12期', code: 12},
      ],
      pyear: [
      
      ],
      reprintStatusDesc: [
        {name: '待打印', code: '1'},
        {name: '已打印', code: '2'},
        {name: '打印失败', code: '3'},
      ],
      printTypeDesc: [
        {name: '合规', code: '1'},
        {name: '不合规转合规', code: '0'},
        {name: '新单', code: ''},
      ],
      reprintTypeDesc: [
        {name: '补打', code: '1'},
        {name: '转寄', code: '2'},
        // {name: '电子邮件', code: '3'},
      ],
      financeFileDesc: [
        {name: '正常请款文件', code: '1'},
        {name: '退保（代付）文件', code: '2'},
        {name: '强扣文件', code: '3'},
        {name: '网银退费', code: '4'},
        {name: '请款回盘的源文件', code: '5'},
        {name: '退费退保的源文件', code: '6'},
      ],
      financeSumDesc: [
        {name: '请款文件汇总清单', code: '1'},
        {name: '请扣款汇总清单', code: '2'},
      ],
      checkType:[
        {name: '待续保清单', code: '1'},
        {name: '续保通知函', code: '2'},
        {name: '取消续保清单', code: '3'},
        {name: '首期无请款续保单', code: '4'},
      ],
      fileType:[
        {name: '新单首期无请款', code: '1'},
        {name: '新单首期请款失败', code: '2'},
        {name: '新单续期请款失败', code: '3'},
        {name: '续单首期无成功请款', code: '4'},
        {name: '续单续期请款失败', code: '5'},
      ],
      extEnterpDesc: extEnterpDesc,
      newOrderDataDesc: newOrderDataDesc,
      payChannel1:payChannel1,
      thirdBank: [
        {code:'ABC', name:'中国农业银行'},
        {code:'BCOM', name:'中国交通银行'},
        {code:'CIB', name:'中国兴业银行'},
        {code:'CITIC', name:'中信银行'},
        {code:'POST', name:'中国邮政储蓄'},
        {code:'SHB', name:'上海银行'},
      ],
      bank:[
        {code:'手动录入', name:'手动录入'},
        {code:'ABC', name:'中国农业银行'},// 2
        // {code:'AHRCB', name:'安徽省农村信用社联合社'},
        {code:'BCOM', name:'中国交通银行'},// 4
        {code:'BHB', name:'渤海银行'},// 14
        // {code:'BJB', name:'北京银行'},
        // {code:'BJRCB', name:'北京农村商业银行'},
        {code:'BOC', name:'中国银行'},// 5
        {code:'CCB', name:'中国建设银行'},// 3
        // {code:'CDB', name:'成都银行'},
        // {code:'CDRCB', name:'成都农村商业银行'},
        {code:'CEB', name:'中国光大银行'},// 8
        {code:'CGB', name:'广东发展银行'},// 9
        {code:'CIB', name:'中国兴业银行'},// 11
        {code:'CITIC', name:'中信银行'},// 12
        {code:'CMB', name:'中国招商银行'},// 6
        // {code:'CMBC', name:'中国民生银行'},
        // {code:'CQRCB', name:'重庆农村商业银行'},
        // {code:'DLB', name:'大连银行'},
        // {code:'FJRCB', name:'福建农村信用合作社'},
        // {code:'GDRCB', name:'广东农村信用合作社'},
        // {code:'GuiZhouRCB', name:'贵州农村信用合作社'},
        // {code:'GXRCB', name:'广西农村信用合作社'},
        {code:'GZRCB', name:'广州农村商业银行'},// 15
        // {code:'HBB', name:'河北银行'},
        // {code:'HENRCB', name:'河南农村信用合作社'},
        // {code:'HNRCB', name:'湖南农村信用合作社'},
        // {code:'HSB', name:'徽商银行'},
        {code:'HXB', name:'华夏银行'},// 13
        // {code:'HZB', name:'杭州银行'},
        {code:'ICBC', name:'中国工商银行'},// 1
        {code:'JSB', name:'江苏银行'},// 16
        // {code:'JXRCB', name:'江西农村信用合作社'},
        // {code:'KSRCB', name:'昆山农村商业银行'},
        // {code:'NBB', name:'宁波银行'},
        // {code:'NCB', name:'南昌银行'},
        {code:'PAB', name:'平安银行'},// 10
        // {code:'POST', name:'中国邮政储蓄'},
        // {code:'QDB', name:'青岛银行'},
        // {code:'SDB', name:'深圳发展银行'},
        {code:'SHB', name:'上海银行'},// 17
        // {code:'SHRCB', name:'上海农村商业银行'},
        {code:'SPDB', name:'上海浦东发展银行'},// 7
        // {code:'SRRCB', name:'上饶银行'},
        // {code:'SZRCB', name:'深圳农村商业银行'},
        // {code:'TJB', name:'天津银行'},
        // {code:'TJRCB', name:'天津农村商业银行'},
        // {code:'WZB', name:'温州银行'},
        // {code:'XMB', name:'厦门银行'},
        // {code:'YNRCB', name:'云南省农村信用社联合社'},
        // {code:'ZJRCB', name:'浙江农村信用合作社'},
        // {code:'ZSB', name:'浙商银行'}
      ],
      certiDesc: [
        {name:'身份证',code:'01'},
        {name:'护照',code:'02'},
        {name:'军官证',code:'03'},
        {name:'台胞证',code:'05'},
        {name:'港澳返乡证',code:'06'},
        {name:'出生证',code:'07'},
        {name:'其他',code:'99'},
      ],
      bankCardDesc: [
        {name: '借记卡', code: '0201'},
        {name: '贷记卡', code: '0203'},
        {name: '存折', code: '0204'},
      ],   
      payChannel: [
        {name: '快钱', code: '0'},
        {name: '银联', code: '1'},
      ],   
      prodDesc: prodDesc,
      relation:[
        {name: '父母', code: '601003'},
        {name: '父', code: '601010'},
        {name: '母', code: '601011'},
        {name: '本人', code: '601005'},
        {name: '配偶', code: '601004'},
        {name: '子女', code: '601002'},
        {name: '赡养', code: '601009'},
        {name: '抚养', code: '601006'},
        {name: '兄、姐', code: '601012'},
        {name: '叔伯、姨', code: '601013'},
        {name: '姑、舅', code: '601014'},
        {name: '其他亲属', code: '601015'},
        {name: '雇佣', code: '601001'},
        {name: '其它', code: '601008'},
        {name: '其他人员', code: '601016'},
      ],
      relation1:[
        {name: '配偶', code: '601004'},
        {name: '子女', code: '601002'},
        {name: '父母', code: '601003'},
        {name: '兄弟姐妹', code: '601012'},
        {name: '本人', code: '120006'},
        {name: '其他', code: '000000'},
      ],
      benfType:[
        {name: '法定', code: '0'},
        {name: '指定', code: '1'},
      ],
      'peeType0636':[
        {name: 'A', code: 'A'},
        {name: 'B', code: 'B'},
        {name: 'C', code: 'C'},
        {name: 'D', code: 'D'},
        {name: 'E', code: 'E'},
      ],
      'peeType1212':[
        {name: 'A', code: 'A'},
        {name: 'B', code: 'B'},
        {name: 'C', code: 'C'},
      ],
      sex:[
        {name: '男', code: '01'},
        {name: '女', code: '02'},
      ],
      area:[
        {name: '深圳陌call', code: '0'},
        {name: '成都陌call', code: '1'},
        {name: '南昌电话中心', code: '2'},
      ],
      applyTypeList:[
        {name: '所有清单', code: '1'},
        {name: '成功清单', code: '2'},
        {name: '失败清单', code: '3'},
        {name: '待确认问题件', code: '4'},
        {name: '质检不通过(自建渠道)', code: '5'},
        {name: '待质检(自建渠道)', code: '6'},
      ],
      payType:[
        {name: '首期请款', code: '00'},
        {name: '全额退费', code: '02'},
        {name: '复效保单', code: '03'},
        {name: '续期请款', code: '04'},
        {name: '非全额退费', code: '06'},
        {name: '无理赔返还', code: '08'},
        {name: '退保后恢复请款', code: '09'},
        {name: '特殊退费', code: '10'},
      ],
      paperReceiptFlag:[
        {name: '未入库', code: '2'},
        {name: '已入库', code: '1'},
        {name: '已遗失', code: '3'},
      ],
      expressCode:[
        {name: '顺丰', code: 'E1003'},
        {name: '德邦', code: 'E1010'},
        {name: 'EMS', code: 'E1008'},
      ],
      plyFlag:[
        {name: '有效', code: '1'},
        {name: '注销', code: '2'},
        {name: '退保', code: '3'},
        {name: '过期', code: '4'},
      ],
      fixedFetchAccessType:[ 
        {name: '中信取数', code: '1'}, 
        {name: '民生build-win取数', code: '2'}, 
        {name: '招行留存率', code: '3'}, 
        {name: '机构留存率', code: '4'}, 
        {name: '每月回盘取数', code: '5'}, 
        {name: '续单净保费', code: '6'}, 
        {name: '续保保费核算取数', code: '7'}, 
        {name: '每月净保费', code: '8'}, 
        {name: '增值服务数据', code: '9'},
        {name: '中信回盘状态汇总', code: '10'},
        {name: '扣款结果统计表', code: '11'},
        {name: '待续保活跃客户清单', code: '12'},
        {name: '增值服务电话变更短信', code: '13'},
        {name: '续保回访结果追踪清单', code: '14'},
        {name: '预留清单类型15', code: '15'},
        {name: '预留清单类型16', code: '16'},
      ] 
    },
    valueAdd: {
      valueServiceItem: [
        { code: '1', item: '30元话费' },
        { code: '2', item: '500M流量' },
        { code: '3', item: '视频会员' },
        { code: '4', item: '智能手环' },
        { code: '5', item: '基因检测' },
        { code: '6', item: '在线医生' },
      ],
      newPolicy: [
        { code: '5', item: '基因检测' },
        { code: '6', item: '在线医生' },
        { code: '7', item: '二选一礼包' },
        { code: '73', item: '礼包（视频）' },
        { code: '76', item: '礼包（在线医生）' },
      ],
      securityPolicy: [
        { code: '1', item: '话费' },
        { code: '2', item: '流量' },
        { code: '3', item: '视频会员' },
        { code: '4', item: '运动手环' },
        { code: '5', item: '基因检测' },
      ],
      securityPlyState: [
        // { code: '1', item: '普通' },
        { code: '2', item: '黄金VIP' },
        { code: '3', item: '铂金VIP' },
        { code: '4', item: '钻石VIP' },
      ],
      expressType: [
        { code: '1', item: '基因检测' },
        { code: '2', item: '运动手环' },
      ],
      ServiceStatus: [
        { code: '0', item: '未领用' },
        { code: '1', item: '已领用' },
        { code: '2', item: '已过期' },
        { code: '3', item: '未满足可领取条件' },
      ],
      serviceType: [
        { code: '0', item: '新单增值服务' },
        { code: '1', item: '保全增值服务' },
      ],
      printStatus: [
        { code: '0', item: '未打印' },
        { code: '1', item: '已打印' },
        { code: '2', item: '打印失败' },
      ],
      expressCode: [
        { code: '95338', item: '顺丰' },
        { code: 'NULL', item: '线下下单' },
      ],
      printType: [
        { code: '0', item: '全部' },
        { code: '1', item: '新单打印' },
        { code: '2', item: '补寄打印' },
        { code: '3', item: '保全打印' },
      ],
      status: [
        { code: '0', item: '已上架' },
        { code: '1', item: '已下架' },
      ],
      attribute: [
        { code: 0, item: '实物' },
        { code: 1, item: '虚拟' },
      ],
      informType: [
        { code: '0', item: '短信' },
        { code: '1', item: '微信推送' },
      ],
    },
    hrEmp:{
      chuangXinYeWu:['199']
    }
  }

  if(dataSource.selectSource.pyear.length===0){
    var curYear = new Date().getFullYear();
    for(var i=curYear;i>=2005;i--){
      dataSource.selectSource.pyear.push( {name: i+'年', code: i},);
    }
  }

  return dataSource[param];

}

//配置
export function getHost(param) {
  
    const hostDesc  = {
        serverlocal: 'http://10.11.32.144:8300/',
        serversit1: 'http://10.1.109.201:8300/',
        serversit2: 'http://10.1.109.202:8300/',
        serveruat1: '',
        serveruat2: '',
        serverrehearse: '',
        serverprod: '',

        viewlocal: 'http://localhost:8000/#/',
        viewsit1: 'http://10.1.109.201:8000/#/',
        viewsit2: 'http://10.1.109.201:8001/#/',
        viewuat1: '',
        viewuat2: '',
        viewrehearse: '',
        viewprod: '',
    }
    return hostDesc[param];
}

export  function getUrlParam(param) { 
    let theRequest = new Object();
    if (param.indexOf("?") != -1) {
    let str = param.substr(1);
    let strs= str.split("&");
    for(let i = 0; i < strs.length; i ++) {
    theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
  }

}
    return theRequest;
  }



export function getSelectChild(attr,rule,filter,codeInLabel,group) {
    const selectChild = [];
    const { Option } = Select;
    const selectChildArr = getDataSource('selectSource')[attr];
    const havaRule = !(undefined==getDataSource('selectSource')[attr][0].rule);

    for (let index in selectChildArr) {
      if(filter && filter(index,selectChildArr)===false){
        continue;
      }
      const { name, code, group } = selectChildArr[index];

      if(havaRule){
        const { ruleName,ruleCode } = getDataSource('selectSource')[attr][index].rule;

        selectChild.push(
          <Option 
          key={index}
          value={code+'('+ruleCode+')'}
          ref={group}
          >
            {codeInLabel&&name!=='所有'?(code+'-'+name):name}
          </Option>
          );
      }else{
        selectChild.push(
          <Option 
            value={code}
            key= {`${index}${group}`}
            ref={group}
          >
            {codeInLabel&&name!=='所有'?(code+'-'+name):name}
          </Option>
          );
      }
    }

  return selectChild;
}

export function bubbleSort(arr,item) {
  let len = arr.length - 1;
  for (let j = 0; j < len; j++) {
    for (let i = 0; i < len - j; i++) {
      if (arr[i][item] < arr[i + 1][item]) {
        [arr[i], arr[i + 1]] = [arr[i + 1], arr[i]]
      }
    }
  }
  return arr;
}

export function identityCodeValid(code) { 
  var city={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江 ",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北 ",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏 ",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外 "};
  var resp = "";

  if(!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)){
    resp = "身份证号格式错误";
  }

  else if(!city[code.substr(0,2)]){
    resp = "地址编码错误";
  }
  else{
      //18位身份证需要验证最后一位校验位
      if(code.length == 18){
          code = code.split('');
          //∑(ai×Wi)(mod 11)
          //加权因子
          var factor = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ];
          //校验位
          var parity = [ 1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2 ];
          var sum = 0;
          var ai = 0;
          var wi = 0;
          for (var i = 0; i < 17; i++)
          {
              ai = code[i];
              wi = factor[i];
              sum += ai * wi;
          }
          var last = parity[sum % 11];
          if(parity[sum % 11] != code[17]){
            resp = "校验位错误";
          }
      }
  }
  return resp;
}


export function getBirthdayFromIdentityCode(code) {
  return code.substring(6, 10) + "-" + code.substring(10, 12) + "-" + code.substring(12, 14);
}

export function getSexFromIdentityCode(code) {
  return code.slice(14, 17) % 2 ? "1" : "2";
}

export function getAgeFromIdentityCode(code) {
  var myDate = new Date();
  var month = myDate.getMonth() + 1;
  var day = myDate.getDate();
  var age = myDate.getFullYear() - code.substring(6, 10) - 1;
  if(code.substring(10, 12) < month || code.substring(10, 12) == month && code.substring(12, 14) <= day) {
      age++;
  }
  return age;
}

export function getGivenCount(rankName) {
  console.log(rankName);
  if(rankName) {
    let rank = 0;
    switch (ranKName) {
      case '黄金VIP': rank = 1; break;
      case '钻石VIP': rank = 1; break;
      case '铂金VIP': rank = 2; break;
      default: rank = 0; break;
    }
    return rank;
  }
}