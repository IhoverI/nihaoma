// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
module.exports = {
  'card-actions-background': '#f5f8fa',
  'font-size-base': '12px',
  'form-item-margin-bottom' : '5px',
  'menu-item-height': '30px',
  'form-vertical-label-padding' : '0 0 4px',
  'input-height-base':'29px',
  'collapse-header-padding': '7px 0 7px 40px',
  'card-padding-base':'13px',
  'menu-dark-item-active-bg':'#f75b5b',
  'disabled-color':'#333'
};
