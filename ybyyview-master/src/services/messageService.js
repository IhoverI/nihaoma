import HttpUtils from '../utils/HttpUtils'
import request from '../utils/request'

// // 数据处理界面
// export async function failPolicy(params) {
//     const payload = params.payload;
//     const extEnterpCode = params.payload.extEnterpCode;
//     const date = params.payload.date;
//     return request ( + `ybyy-channel/channel/emp/failPolicy/${extEnterpCode}/${date}`,{
//       method: 'POST',
//       body: {
//         ...payload,
//       },
//     })
//   }
  
//   // 问题数据修改
//   export async function updateFailPolicy(params) {
//     const payload = params.payload;
//     return request('/channel/emp/failPolicy',{
//       method: 'POST',
//       body: {
//         ...payload,
//       },
//     })
//   }
  
  
  // 消息读取界面
  export async function getMessage(params) {
    const id = params.payload.id;
    return request (`/emp/message/id/${id}`)
  }
  
  // 消息管理消息总数汇总
  export async function messageCount(params) {
    return HttpUtils.get({url:'/emp/message/messageCount'});
  }
  
  // 消息管理查询消息
  export async function queryMessage(params) {
    const body = params.payload;
    console.log(params)
    console.log(body)
    return HttpUtils.post({url:'/emp/message/queryMessage',body})
  }
  
  // 消息管理置顶消息查询
  export async function queryTopMessage(params) {
      const body = params.payload
    return HttpUtils.post({ url: '/emp/message/queryTopMessage', body });
  }
  
  // 消息管理更新消息
  export async function update(params) {
    const body=params.payload
    return HttpUtils.post({ url: '/emp/message/update', body });
  }
  
  // 消息管理删除消息
  export async function deletes(params) {
    const id = params.payload.id;
    console.log(id)
    return HttpUtils.get ({url:`/emp/message/delete/${id}`})
  }