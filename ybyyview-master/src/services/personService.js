import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

// export default class positionsService {

//   // 查询员工列表
//   static searchPerson(body) {
//     return HttpUtils.post({ url: 'emp/queryPersonnelMain', body });
//   }

//   // 团队下拉列表
//   static selectGroupName(body) {
//     return HttpUtils.post({ url: 'emp/selectGroupName', body });
//   }

//   // 根据团队查询组别列表
//   static selectTeamName(body) {
//     return HttpUtils.post({ url: 'emp/selectTeamName', body });
//   }
  
//   // 历史记录
//   static findPersonnelHisInfo(body) {
//     return HttpUtils.post({ url: 'emp/findPersonnelHisInfo', body });
//   }
// }

export async function searchPerson(params) {
  return request(SERVERHR + 'emp/queryPersonnelMain', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
export async function searchCusotomerPerson(params) {
  return request(SERVERHR + 'emp/CusSer/queryInfo', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function selectGroupName(params) {
  return request(SERVERHR + 'emp/selectGroupName', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function checkIfShowGroupForm(params) {
  return request(SERVERHR + 'emp/checkIfShowGroupForm', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function selectTeamName(params) {
  console.log(params);
  return request(SERVERHR + 'emp/selectTeamName', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function findPersonnelHisInfo(params) {
  return request(SERVERHR + 'emp/findPersonnelHisInfo', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function selectOutSourceName(params) {
  return request(SERVERHR + 'emp/selectOutSourceName', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function selectPositionName(params) {
  return request(SERVERHR + 'emp/selectPositionName', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function findPersonnel(params) {
  return request(SERVERHR + 'emp/findPersonnel', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
export async function findCustomerPersonnel(params) {
  return request(SERVERHR + 'emp/CusSer/editInfo', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
export async function checkIfHasAuditRole(params) {
  return request(SERVERHR + 'emp/checkIfHasAuditRole', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
export async function auditRankRecord(params) {
  return request(SERVERHR + 'emp/auditRankRecord', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}



export async function selectRankName(params) {
  return request(SERVERHR + 'emp/selectRankName', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function addPersonnel(params) {
  return request(SERVERHR + 'emp/addPersonnel', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function checkIdentityCode(params) {
  return request(SERVERHR + 'emp/checkIdentityCode', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function insertTeamRecord(params) {
  return request(SERVERHR + 'emp/insertTeamRecord', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}


export async function deleteTeamRecord(params) {
  return request(SERVERHR + 'emp/deleteTeamRecord', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function updateTeamRecord(params) {
  return request(SERVERHR + 'emp/updateTeamRecord', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function insertRankRecord(params) {
  return request(SERVERHR + 'emp/insertRankRecord', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function insertRankCustomerRecord(params) {
  return request(SERVERHR + 'emp/CusSer/insertRankRecord', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function updateRankRecord(params) {
  return request(SERVERHR + 'emp/updateRankRecord', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function updatePersonnel(params) {
  return request(SERVERHR + 'emp/updatePersonnel', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
export async function updateCustomerPersonnel(params) {
  return request(SERVERHR + 'emp/CusSer/saveInfo', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function deletePersonnel(params) {
  return request(SERVERHR + 'emp/deleteRankRecord', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

//模板下载
export async function downloadModal() {
  return request(SERVERHR + 'emp/downloadEmpTemplate', {
    method: 'POST',
  })
}

//模板下载
export async function downloadPerson(params) {
  return request(SERVERHR + 'emp/exportPersonInfo', {
    method: 'POST',
    body: {
      ...params,
    },
  })
}