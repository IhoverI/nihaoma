import request from '../utils/request';

// 续保查询—清单列表
export async function SearchRenewalList(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/RenewalQuery/renewalQuery`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 续保查询—清单下载
export async function QueryDownload(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/RenewalQueryDownload/renewalQueryDownload`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 续保整体情况汇总—待续保
export async function forRenewalTotal(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/RenewalQueryTotal/forRenewalTotal`,{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 续保整体情况汇总—已续保
export async function alreadyRenewalTotal(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/RenewalQueryTotal/alreadyRenewalTotal`,{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 续保整体情况汇总—续保流失
export async function lostRenewalTotal(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/RenewalQueryTotal/lostRenewalTotal`,{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 续保整体情况汇总—本年续保成功率
export async function sucessRenewalTotal(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/RenewalQueryTotal/sucessRenewalTotal`,{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 续保整体情况汇总—排名前3
export async function orderRenewalTotal(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/RenewalQueryTotal/orderRenewalTotal`,{
    // return request('http://10.11.33.157:8300/policy/RenewalQueryTotal/orderRenewalTotal',{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 服务数据大盘--年月日
export async function totalCount(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/EntireReport/totalCountForAll`,{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 服务数据大盘--月日业绩
export async function dayMonthTotalCount(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/EntireReport/dayMonthEachTotalCount`,{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 服务数据大盘--项目组业绩排行榜
export async function orderCount(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/EntireReport/orderCountAndCode`,{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 服务数据大盘-- 新单情况
export async function newSlipInfor(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/EntireReport/newSlipInformation`,{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 服务数据大盘-- 新单运营指标
export async function newSlipIndex(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/EntireReport/newSlipIndex`,{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 服务数据大盘-- 项目组业绩排行榜2
export async function newOrderTwo(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/EntireReport/newOrderTwo`,{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 服务数据大盘-- 业绩转换
export async function caliberChangeRate(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/EntireReport/caliberChangeRate`,{
    method: 'POST',
    body: {
      ...params,
    },
  })
}


export async function SearchFinanceSum(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/QueryAskForPayFile/queryAskForPayFile`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单列表查询
export async function SearchPolicyQuery(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/search/policyList`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}


export async function queryClaimInfo(params) {
  return request(`${SERVERLOCAL  }/ybyy-tocore/toClaim/queryClaimInfo`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 导出保单列表
export async function policyExcelExport(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/search/exportExcel`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 
export async function noticeSendQuery(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/renewalQuery/noticeSendQuery`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function DownLoadApplyList(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/DownloadPreGuaranteeSlip/downloadPreGuaranteeSlip`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
export async function SearchApplyList(params) {
    return request(`${SERVERLOCAL  }ybyy-policy/policy/QueryPreGuaranteeSlip/getPreGuaranteeSlip`, {
      method: 'POST',
      body: {
        ...params,
      },
    });
  }
  export async function SearchExactApplyList(params) {
    return request(`${SERVERLOCAL  }ybyy-policy/policy/QueryPreGuaranteeSlip/getPrecise`, {
      method: 'POST',
      body: {
        ...params,
      },
    });
  }
  

// 固定报表读取查询
export async function FixedReport(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/FixedReport/fixedReport`,{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 固定报表读取下载
export async function FixedReportDownload(params) {
  return request(`${SERVERLOCAL  }ybyy-policy/policy/FixedReport/fixedReportDownload`,{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 自建渠道质检审核查询
export async function queryChannelPolicy(params) {
  const pageNum = params.pageNum;
  const pageSize= params.pageSize;
  return request(`${SERVERLOCAL  }ybyy-channel/channel/queryChannelPolicy/${pageNum}/${pageSize}`,{
    method: 'POST',
    body: {
      ...params.postValue,
      queryType: params.queryType,
    },
  })
}

// 自建渠道质检审核查询 --- 打回通过
export async function approvedChannel(params) {
  return request(`${SERVERLOCAL  }ybyy-channel/channel/approvedChannelPolicy`,{
    method: 'POST',
    body: params,
  })
}
