import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export default class shareService {

  static searchGroups(body) {
    console.log('searchGroups called...');
    console.log(body);
    return HttpUtils.post({ url: 'emp/selectGroupName', body });
  }

  static updateBatchPolicy(body) {
    console.log(body);
    return HttpUtils.post({ url: 'emp/updatePolicyTeamId', body });
  }
}

