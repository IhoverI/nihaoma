import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export default class recordService {
  static searchRecord(body) {
    console.log("service searchRecord called...");
    // return HttpUtils.post({ url: 'emp/search_team_record', body });
    return request('http://localhost:8080/emp/search_position');
  }

  static addRecord(body) {
    console.log("service addRecord called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/add_team_record', body });
  }

  static editRecord(body) {
    console.log("service editRecord...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/edit_team_record', body });
  }

  static deleteRecord(body) {
    console.log("service deleteRecord...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/delete_team_record', body });
  }
}