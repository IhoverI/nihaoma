import request from '../utils/request';
import HttpUtils from '../utils/HttpUtils';

//请扣款查询—文件列表
export async function SearchFinanceList(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/QueryAskForPayDeduct/queryFTPFile', {
    method: 'POST',
    body: {
      ...params,
    },
  })
}

//请扣款清单查询—清单下载
export async function financeListDownload(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/DownloadFile/downloadFTPFile', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export function uploadFTPFile(params) {
  return HttpUtils.upload({
    url:'ybyy-policy/policy/QueryAskForPayDeduct/uploadFTPFile',
    body:params
  });
}

// export async function uploadFTPFile(params) {
//   let formData = new FormData();
//   formData.append("file", params.uploadFile);
//   return request(`${SERVERLOCAL}ybyy-policy/policy/QueryAskForPayDeduct/uploadFTPFile`, {
//     method: 'POST',
//     body: formData,
//   });
// }