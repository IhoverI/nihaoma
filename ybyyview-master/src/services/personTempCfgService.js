import HttpUtils from '../utils/HttpUtils';

export default class personTempCfgService {
  
  //导入因素模板查询
  static selectLmporFactor(body) {
    console.log("service selectLmporFactor called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/selectLmporFactor', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //新增导入因素
  static insertImportFactor(body) {
    console.log("service insertImportFactor called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/insertImportFactor', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //删除因素
  static deleteImprotFactor(body) {
    console.log("service deleteImprotFactor called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/deleteImprotFactor', body });
    //  return request('http://localhost:8080/emp/search_position');
  }
 
  //保存修改配置
  static updateImportFactor(body) {
    console.log("service updateImportFactor called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/updateImportFactor', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

}