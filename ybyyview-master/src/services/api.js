import { stringify } from 'qs';
import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

const serverHost = SERVERLOCAL;


export async function queryProjectNotice() {
  return request('/api/project/notice');
}

export async function queryActivities() {
  return request('/api/activities');
}


// 薪酬计算——下载手工导入因素模版
export async function filterEmp(params) {
  console.log('service filterEmp called..');
  // 10.11.32.88:8305
  return request(SERVERHR + 'emp/filterEmp', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单补发登记 -- 查询
export async function guaranteeReissueQuery(params) {
  return request(serverHost + 'ybyy-policy/policy/ExpressManage/guaranteeReissueQuery', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单补发登记 -- 提交
export async function guaranteeReissueRegister(params) {
  return request(serverHost + 'ybyy-policy/policy/ExpressManage/guaranteeReissueRegister', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单补发登记 -- 发送电子邮件
export async function makeGuaranteePDF(params) {
  return request(serverHost + 'ybyy-policy/policy/ExpressManage/makeGuaranteePDF', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 补发登记信息管理 -- 查询
export async function queryGuaranteeReprint(params) {
  return request(serverHost + 'ybyy-policy/policy/ExpressManage/queryGuaranteeReprint', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}

// 补发登记信息管理 -- 修改
export async function updateGuaranteeReprint(params) {
  return request(serverHost + 'ybyy-policy/policy/ExpressManage/updateGuaranteeReprint', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}

//补发登记信息管理 -- 删除
export async function deleteGuaranteeReprint(params) {
  return request(serverHost + 'ybyy-policy/policy/ExpressManage/deleteGuaranteeReprint', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}

//转快钱查询
export async function queryRuleFastMoney(params) {
  // return request('http://10.11.32.194:8300/policy/fastMoney/queryInFillPage', {
  return request(serverHost + 'ybyy-policy/policy/fastMoney/queryInFillPage', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}
//转快钱暂存
export async function saveRuleFastMoney(params) {
  return request(serverHost + 'ybyy-policy/policy/fastMoney/saveToTemp', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}
//转快钱提交
export async function submitRuleFastMoney(params) {
  return request(serverHost + 'ybyy-policy/policy/fastMoney/confirmByOne', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function fetchBindata(params) {
    return request(serverHost + 'ybyy-policy/policy/fastMoney/exportExcel', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

//保单注销查询
export async function queryRulePolicyCancel(params) {
  return request(serverHost + 'ybyy-policy/policy/surrender/search', {
    method: 'POST',
    body: {
      ...params
    },
  });
}

// 撤消注退申请
export async function cancelSubmit(params) {
  return request(serverHost + 'ybyy-policy/policy/surrender/cancelSubmit', {
    method: 'POST',
    body: {
      ...params
    },
  });
}

// 实退保单查询
export async function queryBackPrm(params) {
  return request(serverHost + 'ybyy-policy/policy/surrender/queryBackPrm', {
    method: 'POST',
    body: {
      ...params
    },
  });
}

//保单注销提交
export async function submitRulePolicyCancel(params) {
  return request(serverHost + 'ybyy-policy/policy/surrender/execute', {
    method: 'POST',
    body: {
      ...params
    },
  });
}
//查询保单详情
export async function queryRulePolocyList(params) {
  // return request('http://10.11.32.194:8300/policy/search/policyDetail', {
  return request(serverHost + 'ybyy-policy/policy/search/policyDetail', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

//新单打印管理--查询
export async function searchPrintQuery(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/queryExpressDetail', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

//新单打印管理--批量打印
export async function toMakeGuarantee(params) {
  // 10.11.32.88:8305
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/toMakeGuarantee', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

//新单打印管理-全部转德邦/顺丰
export async function turnExpressCode(params) {
  // 10.11.32.88:8305
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/turnExpressCode', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}


//新单打印管理--操作
export async function updateEXpressCompany(params) {
  // 10.11.32.88:8305
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/updateEXpressCompany', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

//新单打印管理--导出excel
export async function expressAddress(params) {
  // 10.11.32.88:8305
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/expressAddress', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
//打印管理清单导出--导出excel
export async function exportPrintInfoList(params) {
  // 10.11.32.88:8305
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/exportPrintExpressInfo', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
// 回执管理-查询
export async function receiptQuery(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/receiptSearch', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 回执管理-更新
export async function updateReceipt(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/editExpress', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}


// 报表功能-线上数据-查询
export async function onlineReport(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/report/onlineReport', {
  // return request('http://10.11.32.194:8300/policy/report/onlineReport', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
// 报表功能-线上数据-查询
export async function exportOnlineReport(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/report/exportOnlineReport', {
  // return request('http://10.11.32.194:8300/policy/report/exportOnlineReport', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}


// 保单补打管理--查询
export async function guaranteeReprintQuery(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/guaranteeReprintQuery', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单补打管理--补充打印
export async function guaranteeReprintPrint(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/guaranteeReprintPrint', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单补打管理--转寄打印
export async function guaranteeRepairSendPrint(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/guaranteeRepairSendPrint', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单补打管理 -- 导出
export async function guaranteeReprintExport(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/guaranteeReprintExport', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 打印详情页 -- 导出
export async function exportPrintInfos(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/exportPrintInfos', {
  // return request('http://10.11.32.99:8305/policy/ExpressManage/exportPrintInfos', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 快递、回执单查询
export async function expressReceiptQuery(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/expressReceiptQuery', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}

// 快递、回执单 -- 导出
export async function exportExpressReceiptExcel(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/exportExpressReceiptExcel', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}

// 快递、回执单 -- 问题清单导出
export async function exportExpressQuestionInfo(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/exportExpressQuestionInfo', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}
// 费用管理 -- 汇总导出
export async function exportFeeCollectExcel(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/exportFeeCollectExcel', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}


// 费用管理 -- 清单导出
export async function exportSettlementDealExcel(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/exportSettlementDealExcel', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}

// 快递费用管理查询
export async function expressSettlementDeal(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/expressSettlementDeal', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}

// 快递费用管理查询 -- 汇总结算
export async function expressFeeCollect(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/expressFeeCollect', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}

//
export async function policyPrint(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/toMakeGuarantee', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function deleteGuaranteeReprintInfo(params) {
  //  return request('http://10.11.32.99:8305/policy/ExpressManage/deleteGuaranteeReprintInfo', {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/deleteGuaranteeReprintInfo', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单批改 --- 客户信息批改查询
export async function query(params) {
  const cPlyNo = params.cPlyNo;
  return request(SERVERLOCAL + `ybyy-policy/policy/correct/${cPlyNo}`);
  // return request(`http://10.11.32.195:8300/policy/correct/${cPlyNo}`);
}

// 保单批改 --- 客户信息批改提交
export async function apply1(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/correct/apply', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 保单批改 --- 客户信息批改提交
export function apply(params) {
  return HttpUtils.upload({
    url:'ybyy-policy/policy/correct/apply/upload',
    body:params
  });
}

//客户信息批改下载
export function download(params) {
  return request(SERVERLOCAL + `ybyy-policy/policy/correct/download`, {
  // return request('http://10.11.32.195:8300/policy/correct/download', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function updateCardId(params) {
  return HttpUtils.upload({
    url:'ybyy-policy/policy/correct/updateCardId',
    body:params
  });
}

// 客户信息批改审核通过或拒绝
export async function passOrNot(params) {
  const id = params.cAppNo;
  if (params.remark !=='') {
    return request(SERVERLOCAL + `ybyy-policy/policy/correct/${id}`,{
      method: 'PUT',
      body: {
        statu: params.statu,
        remark: params.remark,
      },
    });
  } else {
    return request(SERVERLOCAL + `ybyy-policy/policy/correct/${id}`,{
      method: 'PUT',
      body: {
        statu: params.statu,
      },
    });
  }
  
}

// 客户信息批核审核
export async function userCorrect(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/correct',{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 客户信息批改审核详情查询
 export async function userInforDetail(params) {
  const id = params.id;
  return request(SERVERLOCAL + `ybyy-policy/policy/correct/detail/${id}`);
}

//注退待审核列表
export async function endorList(params) {
  return request(SERVERLOCAL + `ybyy-policy/policy/surrender/endorListSearch`,{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 注退待审通过
export async function pass(params) {
  return request(SERVERLOCAL + `ybyy-policy/policy/surrender/pass`,{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 注退待审打回
export async function back(params) {
  return request(SERVERLOCAL + `ybyy-policy/policy/surrender/back`,{
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 消息管理消息总数汇总
export async function messageCount(params) {
  return request (SERVERLOCAL + 'ybyy-policy/policy/message/messageCount');
}

// 消息管理查询消息
export async function queryMessage(params) {
  const payload = params.payload;
  return request (SERVERLOCAL + 'ybyy-policy/policy/message/queryMessage',{
    method: 'POST',
    body: {
      ...payload,
    }
  })
}

// 消息管理置顶消息查询
export async function queryTopMessage(params) {
  return request (SERVERLOCAL + 'ybyy-policy/policy/message/queryTopMessage',{
    method: 'POST',
    body: {
      ...params,
    }
  })
}

// 消息管理更新消息
export async function update(params) {
  return request (SERVERLOCAL + 'ybyy-policy/policy/message/update',{
    method: 'POST',
    body: params.payload
  })
}

// 消息管理删除消息
export async function deletes(params) {
  const id = params.payload;
  return request (SERVERLOCAL + `ybyy-policy/policy/message/delete/${id}`)
}

// 数据处理界面
export async function failPolicy(params) {
  const payload = params.payload;
  const extEnterpCode = params.payload.extEnterpCode;
  const date = params.payload.date;
  return request (SERVERLOCAL + `ybyy-channel/channel/policy/failPolicy/${extEnterpCode}/${date}`,{
    method: 'POST',
    body: {
      ...payload,
    },
  })
}

// 问题数据修改
export async function updateFailPolicy(params) {
  const payload = params.payload;
  return request(SERVERLOCAL + 'ybyy-channel/channel/policy/failPolicy',{
    method: 'POST',
    body: {
      ...payload,
    },
  })
}

// 消息读取界面
export async function getMessage(params) {
  const id = params.payload.id;
  return request (SERVERLOCAL + `ybyy-policy/policy/message/id/${id}`)
}

export async function removeRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function renwalInfo(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/renwalInfo', {
    method: 'PUT',
    body: params,
  });
}

export async function fakeSubmitForm(params) {
  return request(serverHost + 'policy/fastMoney/getTempRecord', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData() {
  return request('/api/fake_chart_data');
}

export async function queryTags() {
  return request('/api/tags');
}

export async function queryBasicProfile() {
  return request('/api/profile/basic');
}

export async function queryAdvancedProfile() {
  return request('/api/profile/advanced');
}

export async function queryFakeList(params) {
  return request(`/api/fake_list?${stringify(params)}`);
}

export async function fakeAccountLogin(params) {
  return request('/api/login/account', {
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('/api/notices');
}

// 取消续保批改查询
export async function cancelReviewQuery(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/renwal/renwalList',{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

// 取消续保批改查询
export async function executeRenwal(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/renwal',{
    method: 'PUT',
    body: {
      ...params,
    },
  })
}

// 批量注销查询
export async function queryCancelData(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/BatchCancel/queryCancelData', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 批量导入注销数据

export async function importPendingCancelExcl(params) {
  return HttpUtils.upload({
    url:'ybyy-policy/policy/BatchCancel/importPendingCancelExcl',
    body:params
  });
}

// 批量注销
export async function batchCancel(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/BatchCancel/batchCancel', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 批量清单导出
export async function exportPendingCancelExcl(params) {
  return request(`${SERVERLOCAL}ybyy-policy/policy/BatchCancel/exportPendingCancelExcl`, {
    method: 'POST',
    body: params,
  });
}

//销售方案查询
export async function planQuery(params) {
  const prodNo=params.prodNo;
  const extEnterpCode = params.extEnterpCode;
  return request(SERVERLOCAL + `ybyy-policy/policy/sellType/${extEnterpCode}/${prodNo}`);
}

//销售方案查询
export async function renwalPlanQuery(params) {
  const extEnterpCode=params.extEnterpCode;
  return request(SERVERLOCAL + `ybyy-policy/policy/renwalInfo/${extEnterpCode}`);
}

//电子发送详情查询
export async function queryEmailSendDetail(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/searchEmail',{
    method: 'POST',
    body: {
      ...params,
    },
  })
}

//清单详情
export async function queryPrintInfos(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/ExpressManage/queryPrintInfos',{
    method: 'POST',
    body: {
      ...params,
    },
  })
}


// export function uploadExpressBackFile(params) {
//   return HttpUtils.upload({
//     url:'ybyy-policy/policy/ExpressManage/uploadExpressBackFile',
//     body:params
//   });
// }


export async function uploadExpressBackFile(params) {
  let formData = new FormData();
  formData.append("file", params.file);
  return request(`${SERVERLOCAL}ybyy-policy/policy/ExpressManage/uploadExpressBackFile`, {
    method: 'POST',
    body: formData,
  });
}

export async function uploadSukeyBackFile(params) {
  let formData = new FormData();
  formData.append("file", params.param);
  return request(`${SERVERLOCAL}ybyy-policy/policy/DataFetch/uploadSukeyBackFile`,{
    method: 'POST',
    body: formData,
  });
}

export async function downloadSukeyBackFile(params) {
  return request(`${SERVERLOCAL}ybyy-policy/policy/DataFetch/exportSukeyExcel`,{
    method: 'POST',
    body: params,
  });
}


//团队和组别数量
export async function queryGroupNumAndTeamNum(params) {
  console.log('service queryGroupNumAndTeamNum called..');
  return request(SERVERHR + '/emp/queryGroupNumAndTeamNum', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}


//新单打印管理--导出excel
export async function expressRanks(data) {
  console.log('service expressRanks called..');
  // 10.11.32.88:8305
  return request(SERVERHR + 'emp/downloadRank', {
    method: 'POST',
    body: {
      data,
    },
  });
}

//薪酬查询--导出excel
export async function downloadSalary(params) {
  console.log('service downloadSalary called..');
  // 10.11.32.88:8305
  return request(SERVERHR + 'emp/exportSalaryResult', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function exportPolicyExcel(params){
  return request(SERVERHR + 'emp/exportPolicyExcel', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

//导出汇总金额
export async function expressSalary(params) {
  console.log('service expressSalary called..');
  // 10.11.32.88:8305
  return request(SERVERHR + 'emp/collectExport', {
    method: 'POST',
    body: {
      ...params,
    },
  });


}