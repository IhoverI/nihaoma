import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export default class groupService {

  // 团队与组别管理_查询团队与组别
  static searchGroup(body) {
    console.log('service searchGroup called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/queryGroupAndTeam', body});
  }

  // 团队与组别管理_新增团队(弹出框)
  static addGroup(body) {
    console.log('service addGroup called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/add_group', body});
  }

  // 团队与组别管理_新增组别(弹出框)
  static addTeam(body) {
    console.log('service addTeam called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/add_team', body});
  }

  // 编辑团队与组别信息_编辑团队
  static editGroup(body) {
    console.log('service editGroup called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/edit_group', body});
  }

  // 编辑团队与组别信息_查询组别信息
  static queryTeamInfo(body) {
    console.log('service queryTeamInfo called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/queryTeamInfo', body});
  }
 
  // 编辑团队与组别信息_查询组别成员
  static queryTeamMember(body) {
    console.log('service queryTeamMember called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/findTeamMember', body});
  }

   // 编辑团队与组别信息_查询组别成员
   static queryTeamCustomerMember(body) {
    console.log('service queryTeamCustomerMember called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/CusSer/findTeamMember', body});
  }
  
  // 编辑团队与组别信息_新增组别成员
  static addTeamMember(body) {
    console.log('service addTeamMember called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/addTeamMember', body});
  }
  
  // 编辑团队与组别信息_查询组别成员
  static searchMemebers(body) {
    console.log('service searchMemebers called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/queryPersonnelMain', body});
  }
  static searchCustomerMemebers(body) {
    console.log('service searchCustomerMemebers called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/CusSer/queryInfoMain', body});
  }
  
  // 编辑团队与组别信息_新增组别成员
  static updateTeam(body) {
    console.log('service updateTeam called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/edit_team', body});
  }
}