import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';
export const server = ''; // 测试
export const microservices = 'ybyy-integral/'; // 微服务名字

export default class integralService {
  // 积分查询
  static queryIntegralSearchInfo(body) {
    return HttpUtils.post({url: `${microservices}integral/detail`, server, body});
  }
  //积分管理
  static queryIntegralManageList(body) {
    return HttpUtils.post({url: `${microservices}integral/prod/conf`, server, body});
  }
  //积分管理行修改
  static updateIntegralManageRow(body) {
    return HttpUtils.put({url: `${microservices}integral/prod/conf`, server, body});
  }
   //赠送积分
   static queryIntegralGiftList(body) {
    return HttpUtils.post({url: `${microservices}integral/largessIntegarl/query`, server, body});
  }
  // 修改赠送积分
  static updateIntegralGiftRow(body) {
    return HttpUtils.put({url: `${microservices}integral/largessIntegral`, server, body});
  }
  // 删除赠送积分
  static delIntegralGiftRow(body) {
    return HttpUtils.delete({url: `${microservices}integral/largessIntegral`, server, body});
  }
  // 整体积分查询
   static queryIntergralAllList(body) {
    return HttpUtils.post({url: `${microservices}integral/report/whole`, server, body});
  }
   // 客户存留率查询
  static queryIntergralCustomerList(body) {
    return HttpUtils.post({url: `${microservices}integral/report/cusRetentionRate`, server, body});
  }
   // 客户整体
   static querycustomerAllSearchList(body) {
    return HttpUtils.post({url: `${microservices}integral/report/customer`, server, body});
  }
   // 红包查询
   static queryIntergralRedPacketSearch(body) {
    return HttpUtils.post({url: `ybyy-valueadded/valueAdded/extendBatch/selectAll`, server, body});
  }
  // 红包查询
  static queryIntergralRedPacketList(body) {
    return HttpUtils.post({url: `ybyy-valueadded/valueAdded/extendDetail/selectPushNo`, server, body});
  }
  // 增值服务查询
  static queryValueAddedSearchInfo(body) {
    return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/ServiceDetails/serviceDetailsQuery`, {
      method: 'POST',
      body,
    });
  }
  
 
}

//赠送积分模板下载
export async function integralGiftDownload(params) {
  return request(`${server || SERVERLOCAL }${microservices}integral/largess/download`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

//赠送积分文件上传失败下载
export async function integralGiftUploadFailDownload(params) {
  return request(`${server || SERVERLOCAL}${microservices}integral/largess/errorFile`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 导出整体积分
export async function integralAllDownList(params) {
  return request(`${server || SERVERLOCAL}${microservices}integral/report/whole/download`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 导出客户存留率
export async function integralCustomerDownList(params) {
  return request(`${server || SERVERLOCAL}${microservices}integral/report/cusRetentionRate/download`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
// 客户总体清单导出
export async function downloadCustomerAllList(params) {
  return request(`${server || SERVERLOCAL}${microservices}integral/report/customer/download`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
export async function exportRedPacketExcel(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/extendBatch/exportExcel`, {
    method: 'POST',
    body: params,
  });
} 