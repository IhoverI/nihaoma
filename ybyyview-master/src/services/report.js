import request from '../utils/request';
import HttpUtils from '../utils/HttpUtils';

export async function queryDynamicReport(params) {
  return request(`${SERVERLOCAL}ybyy-policy/policy/queryDynamicReport/${params.pageNum}/${params.pageSize}`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function findTemplate() {
  return request(`${SERVERLOCAL}ybyy-policy/policy/findTemplate`, {
    method: 'GET',
  });
}

export async function fetchDynamicReport(params) {
  return request(`${SERVERLOCAL}ybyy-policy/policy/dynamicReport/exportExcel`, {
  method: 'POST',
  body: {
    ...params,
  },
});
}

export async function addTemplate(params) {
  return request(`${SERVERLOCAL}ybyy-policy/policy/addTemplate`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function findAllDynamicLabel() {
  return request(`${SERVERLOCAL}ybyy-policy/policy/dynamicReport/findAllDynamicLabel`, {
    method: 'GET',
  });
}

export async function importQuery(params) {
  var formData = new FormData();
  formData.append("file", params.file);
  return request(`${SERVERLOCAL}ybyy-policy/policy/dynamicReport/importQuery`, {
    method: 'POST',
    body: formData,
  });
}

export async function downLoadTemplate(params) {
  return request(`${SERVERLOCAL}ybyy-policy/policy/dynamicReport/downLoadTemplate`);
}

