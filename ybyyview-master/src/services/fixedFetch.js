import request from '../utils/request';
import HttpUtils from '../utils/HttpUtils';

export async function postFixedFetch(params) { // 导出文件
    return request(SERVERLOCAL+`ybyy-policy/policy/FixedAccess/fixedAccessEntrance`, {
      method: 'POST',
      body: {
        ...params,
      },
    });
  }