import request from '../utils/request';
import HttpUtils from '../utils/HttpUtils';

export async function query() {
  return request('/api/users');
}

export function queryCurrent() {
  return HttpUtils.get({
    url:'sso/user',
    server:SSOSERVERLOCAL,
  });
}

export function queryMenuData() {
  return HttpUtils.get({
    url:'',
    server:MENUSERVER,
  });
}

