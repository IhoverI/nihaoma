import request from '../utils/request';
import HttpUtils from '../utils/HttpUtils';


export async function queryPayChargeInfo(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/PayCounteroffer/queryPayCounterofferInfo', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function exportFinanceQueryFile(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/PayCounteroffer/exportPayCounterofferInfoExcel', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

