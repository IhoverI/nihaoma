import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export default class recommendService {
  static fetchList(body) {
    // return HttpUtils.post({ url: 'emp/search_position', body });
    return request('/emp/search_position');
  }

  static searchUsers(body) {
    console.log("service searchUsers called...");
    console.log(body);
    let url = 'recommend/findUsers?userName=' + body.data.userName;
    console.log(url);
    return HttpUtils.get({ url: url, body });
    // return request('http://localhost:8080/emp/search_position');
  }

  static searchRecommend(body) {
    console.log("service searchRecommend called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/queryPushRepairMain', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  static searchProvinceData(body) {
    console.log("service searchProvinceData called...");
    console.log(body);
    return HttpUtils.get({ url: 'recommend/findAllProvince', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  static searchCityByPrveData(body) {
    console.log("service searchCityByPrveData called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findCityByPrv', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  static searchDistricData(body) {
    console.log("service searchDistricData called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findDistrictByCity', body });
    //  return request('http://localhost:8080/emp/search_position');
  }
  
  
  static addRecommendMain(body) {
    console.log("service addRecommendMain called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/addPushRepairMain', body });
  }

  static updatePushRepairMain(body) {
    console.log("service updatePushRepairMain called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/updatePushRepairMain', body });
  }

 
  static editRecommend(body) {
    console.log("service editRecommend...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/edit_recommend', body });
  }

  static searchClm(body) {
    console.log("service searchDim called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findThirdPart', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  static searchChaCodeByClm(body) {
    console.log("service searchChaCodeByClm called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findChaCodeByClm', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  static searchOuterCodeByChaCode(body) {
    console.log("service searchOuterCodeByChaCode called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findOuterNodeByChaCode', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  static searchOuterByClm(body) {
    console.log("service searchOuterByClm called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findChannelAndOuterCode', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  static searchChaByOuter(body) {
    console.log("service searchChaByOuter called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findChannelAndOuterCode', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  static searchOuterByClm(body) {
    console.log("service searchChaByOuter called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findOuterCode', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  // 查找车型
  static searchCarModel(body) {
    console.log("service searchCarModel called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/findCarModel', body });
    // return request('http://localhost:8080/emp/search_position');
  }
  
  // 保存单条规则
  static addRule(body) {
    console.log("service addRule called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/addPushRepairRule', body });
    // return request('http://localhost:8080/emp/search_position');
  }

   // 查询省市规则
   static searchRule(body) {
    console.log("service editPushRepairInfo called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/editPushRepairInfo', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  // 编辑单条规则
  static editRule(body) {
    console.log("service editRule called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/updatePushRepairRule', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  // 删除单条规则
  static deleteRule(body) {
    console.log("service deleteRule called...");
    console.log(body);
    return HttpUtils.post({ url: 'recommend/deletePushRepair', body });
  }


  // 查询用已分配的默认业务机构
  static searchUserDeptCode(body){
    console.log("service queryUserDeptCode called...");
    console.log(body);
    let url = 'um/user/' + body.data.userCode + "/department/default";
    console.log(url);
    return HttpUtils.get({ url: url, body });
  }

  // 审核规则
  static approveMain(body){
    console.log("service approveMain called...");
    console.log(body);
    let url = 'recommend/approvePushRepair';
    console.log(url);
    return HttpUtils.post({ url: url, body });
  }
}