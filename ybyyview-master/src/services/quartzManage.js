import request from '../utils/request';

export async function SearchJobBufferList(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/jobBuffer/findJobBufferList', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function UpdateJobBuffer(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/jobBuffer/updateJobBuffer', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function UpdateRow(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/job/findById', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function AddJob(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/job/addJob', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function UpdateJob(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/job/updateJob', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function DeleteJob(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/job/deleteJob', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}


export async function AddJobBuffer(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/jobBuffer/addJobBuffer', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}



export async function SearchJobList(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/job/findJobList', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export async function SearchSysCode(params) {
  return request(SERVERLOCAL + 'ybyy-quartz/quartz/conf/queryCode', {
    method: 'POST',
    body: {
      ...params,
    }
  })
}
