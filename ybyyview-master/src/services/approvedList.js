import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';



export async function findSalaryProcess() {
    return HttpUtils.get({url:'/emp/findSalaryProcess'});
  }

export async function updateSalaryResult(body) {
    console.log(body)
    return HttpUtils.post({url:'/emp/updateSalaryResult',body})
}

export async function approvedSalaryResult(body) {
    console.log(body)
    return HttpUtils.post({url:'/emp/approvedSalaryResult',body})
}
