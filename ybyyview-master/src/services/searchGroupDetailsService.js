import HttpUtils from '../utils/HttpUtils';

export default class searchGroupDetailsService {

  static searchGroupDetails(body) {
    console.log('searchGroupDetails called...');
    return HttpUtils.post({ url: 'emp/findTeamMember', body });
  }

  static deleteMember(body) {
    console.log('deleteMember called...');
    return HttpUtils.post({ url: 'emp/deleteTeamMember', body });
  }

  static addMember(body) {
    console.log('addMember called...');
    return HttpUtils.post({ url: 'emp/addTeamMember', body });
  }
  
}