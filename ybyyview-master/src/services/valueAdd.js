import request from '../utils/request';


export async function repServiceLink(params) {
  return request(`${SERVERLOCAL}/repServiceLink`, {
    methods: 'POST',
    body: params,
  })
}
// 数据提取查询
export async function valueAddedData(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/queryValueAddedService`, {
    method: 'POST',
    body: params,
  });
}
// 新单数据提取查询
export async function newValueAddData(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/queryValueAddedService`, {
    method: 'POST',
    body: {...params, type: '0'},
  });
}
// 保全提取导出数据
export async function exportNewValueAddData(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/exportValueAddedService`, {
    method: 'POST',
    body: {...params, type: '0'},
  })
}
// 保全数据提取查询
export async function securityPolicy(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/queryValueAddedService`, {
    method: 'POST',
    body: {...params, type: '1'},
  });
}
// 保全提取导出数据
export async function exportSecurityPolicy(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/exportValueAddedService`, {
    method: 'POST',
    body: {...params, type: '1'},
  })
}
// 结算维度取数
export async function getOrderMsg(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/search/getOrderMsg`, {
    method: 'POST',
    body: params,
  });
}
// 结算维度导出数据
export async function exportOrderMsg(params) {
  const data = {
    ...params,
  };
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/search/exportOrderMsg`, {
    method: 'POST',
    body: data,
  });
}
// 保单详情增值服务
export async function policyServiceQuery(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/ServiceDetails/policyServiceQuery`, {
    method: 'POST',
    body: params,
  });
}
// 增值服务详情已赠送
export async function serviceDetailsQuery(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/ServiceDetails/serviceDetailsQuery`, {
    method: 'POST',
    body: params,
  });
}
// 增值服务详情可赠送
export async function queryVipRank(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/ServiceDetails/queryVipRank`, {
    method: 'POST',
    body: params,
  });
}
// 增值服务赠送
export async function giveGifts(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/ServiceDetails/giveGifts`, {
    method: 'POST',
    body: params,
  });
}
// 查看已赠送的物流
export async function expressList(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/express/expressList`, {
    method: 'POST',
    body: params,
  });
}
// 增值服务重发服务
export async function retryNote(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/ServiceDetails/retryNote`, {
    method: 'POST',
    body: params,
  })
}
// 增值服务列表查询
export async function confQuery() {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/MVASConf/confQuery`, {
    method: 'POST',
    body:{},
  });
}
// 增值服务信息更新修改
export async function updateConf(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/MVASConf/updateConf`, {
    method: 'POST',
    body: params,
  })
}
// 新增服务信息更新修改
export async function addConf(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/MVASConf/addConf`, {
    method: 'POST',
    body: params,
  })
}
// 增值服务快递单打印查询
export async function queryPendingExpress(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/express/queryPendingExpress`, {
    method: 'POST',
    body: params,
  });
}
// 增值服务未打印快递单删除
export async function deleteExpressApply(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/express/deleteExpressApply`, {
    method: 'POST',
    body: params,
  });
}
// 库存详情查询
export async function queryStockInfo(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/queryStockInfo`, {
    method: 'POST',
    body: params,
  });
}
// 库存信息编辑
export async function editStockInfo(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/editStockInfo`, {
    method: 'POST',
    body: params,
  });
}
// 新增库存信息编辑
export async function addStockInfo(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/addStockInfo`, {
    method: 'POST',
    body: params,
  });
}

  // 新增库存信息编辑
export async function exportExpressOrder(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/express/exportExpressOrder`, {
    method: 'POST',
    body: params,
  });
}
  // 快递单批量打印
  export async function placeExpressOrder(params) {
    return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/express/placeExpressOrder`, {
      method: 'POST',
      body: params,
    });
  }
  // 快递报表查询
  export async function queryExpressReport(params) {
    return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/queryExpressReport`, {
      method: 'POST',
      body: params,
    });
  }
 // 物流管理导出
 export async function exportExpressReport(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/exportExpressReport`, {
    method: 'POST',
    body: params,
  });
}  
 // 打包寄送导出
 export async function downLoadPackageTemplate(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/downLoadPackageTemplate`, {
    method: 'GET',
    body: params,
  });
}
// 检测盒导出
export async function downLoadGeneBoxTemplate(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/downLoadGeneBoxTemplate`, {
    method: 'GET',
    body: params,
  });
}
 // 物流管理导出
 export async function downLoadLineTemplate(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/downLoadLineTemplate`, {
    method: 'GET',
    body: params,
  });
}
// 库存导出
export async function exportStockInfo(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/exportStockInfo`, {
    method: 'POST',
    body: params,
  });
}

// 发送邮件
export async function sendEmail(params) {
  return request(`${SERVERLOCAL}ybyy-valueadded/valueAdded/selectElectronicPolicy/senEmail`, {
    method: 'POST',
    body: params,
  });
}



