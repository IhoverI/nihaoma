import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export default class outSourceService {
  
  //查询所有公司
  static selectOutSourceInfo(body) {
    console.log("service selectOutSourceInfo called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/selectOutSourceInfo', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //查询团队名称
  static searchGroup(body) {
    console.log("service searchGroup called...");
    console.log(body);
    let url = 'emp/selectGroupName';
    console.log(url);
    return HttpUtils.post({ url: url, body });
    // return request('http://localhost:8080/emp/search_position');
  }

  //保存修改派遣公司信息
  static upadateOutSource(body) {
    console.log("service upadateOutSource called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/upadateOutSource', body });
    // return request('http://localhost:8080/emp/search_position');
  }
  
  //新增派遣公司
  static insertOutSource(body) {
    console.log("service insertOutSource called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/insertOutSource', body });
    // return request('http://localhost:8080/emp/search_position');
  }

  //删除派遣公司 
  static deleteOutSource(body) {
    console.log("service deleteOutSource called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/deleteOutSource', body });
    // return request('http://localhost:8080/emp/search_position');
  }
}