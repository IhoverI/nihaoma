import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export async function SubmitSelfchannelForm(params) {
  return request(SERVERLOCAL + 'ybyy-channel/channel/addPolicy', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function SubmitSelfUpdateChannelForm(params) {
  return request(SERVERLOCAL + 'ybyy-channel/channel/updatePolicy', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function getUserInfo(params) {
  return request(SERVERLOCAL + 'ybyy-policy/policy/baseMessage/getUserInfo', {
    method: 'GET',
  });
}

// 查询职业
export async function getItemAccOccupationCode() {
  return HttpUtils.get({
    url: `ybyy-policy/policy/public/queryCodeList/ItemAccOccupationCode`,
  })
}

// 查询国籍
export async function getCountryCode() {
  return HttpUtils.get({
    url: `ybyy-policy/policy/public/queryCodeList/Country`,
  });
}

// 查询客户关系字段
export async function getRelationBetweenCode() {
  return HttpUtils.get({
    url: `ybyy-policy/policy/public/queryCodeList/InsuredIdentity`,
  });
}
