import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export default class positionsService {
  static searchPosition(body) {
    // return HttpUtils.post({ url: 'emp/search_position', body });
    console.log('service searchPosition called...');
    console.log(body);
    return HttpUtils.post({url: 'emp/search_position', body});
  }

  static addPosition(body) {
    console.log("service editPosition called...");
    console.log(body);
    return HttpUtils.post({url: 'emp/add_position', body});
  }

  static editPosition(body) {
    console.log("service editPosition...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/edit_position', body });
  }

  static editRank(body) {
    console.log("service editRank...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/edit_rank', body });
  }

  static addRank(body) {
    console.log("service addRank...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/add_rank', body });
  }
}