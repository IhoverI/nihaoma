import request from '../utils/request';
import HttpUtils from '../utils/HttpUtils';


export async function queryFinanceInfo(params) { // 请求列表
  const pageSize = params.pageSize;
  const pageNum = params.pageNum;
  return request(SERVERLOCAL + `ybyy-policy/policy/CreateCheckingFile/queryFinanceInfo/${pageNum}/${pageSize}`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}


export async function exportFinanceFile(params) { // 导出文件
  return request(SERVERLOCAL + 'ybyy-policy/policy/CreateCheckingFile/exportFinanceFile', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

export function uploadFinanceFile(params) { //导入文件
  return HttpUtils.upload({
    url:'ybyy-policy/policy/CreateCheckingFile/uploadFinanceFile',
    body:params
  });
}

export function recountFileList(params) {　// 重新计算
  return HttpUtils.upload({
    url:'ybyy-policy/policy/CreateCheckingFile/updateCmmFees',
    body:params
  });
}

export async function importOfflineInfo(params) { // 线下清单导入
  return HttpUtils.upload({
    url:'ybyy-policy/policy/PayCounteroffer/importOfflineInfo',
    body:params
  });
}

export async function checkConsistency(params) { // 核对完成
  return request(SERVERLOCAL + 'ybyy-policy/policy/CreateCheckingFile/checkConsistency', {
    method: 'POST',
    body: {
      ...params,
    },
  });
}
