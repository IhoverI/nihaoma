import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';



export async function getGroupsInfo() {
    return HttpUtils.post({
        url: 'emp/selectGroupName',
        body: { data: '' }
    })
}

export async function getAllLaws() {
    return HttpUtils.post({
        url: '/emp/queryBasicLaw',
        body: { data: '' }
    })
}

export async function deleteRankById(rankId) {
    return HttpUtils.post({
        url: '/emp/deleteRankById',
        body: { data: { id: rankId } }
    })
}
export async function searchPositions(params) {
    return  HttpUtils.post({
        url: '/emp/search_position',
        body: { data: params }
    })
}

