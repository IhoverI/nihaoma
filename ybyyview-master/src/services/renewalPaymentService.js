import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export default class renewalPaymentService {
  
  //首期无请款续保单数据
  static queryRenewalPayment(params) {
    console.log("service queryRenewalPayment called...");
    console.log(params.pyear);
    return request(`${SERVERLOCAL}ybyy-policy/policy/RenewalQuery/queryRenewalList`, {
    // return request(`http://10.11.33.109:8300/policy/RenewalQuery/queryRenewalList`, {
      method: 'POST',
      body: {
        ...params,
      },
    });
  }

  static updateRenewalThirdFlag(params) {
    console.log("service updateRenewalThirdFlag called...");
    console.log(params)
    return request(`${SERVERLOCAL}ybyy-policy/policy/RenewalQuery/updateRenewalThirdFlag`, {
    // return request(`http://10.11.33.109:8300/policy/RenewalQuery/updateRenewalThirdFlag`, {
      method: 'POST',
      body: params,
    });
  }

}