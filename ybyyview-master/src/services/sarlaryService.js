import HttpUtils from '../utils/HttpUtils';
import request from '../utils/request';

export default class sarlaryService {

  static querySalaryTitle(body) {
    console.log('querySalaryTitle called...');
    return HttpUtils.get({ url: 'emp/querySalaryTitle', body });
  }

  static querySalaryInfo(body) {
    console.log('querySalaryInfo called...');
    console.log(body);
    return HttpUtils.post({ url: 'emp/querySalaryInfo', body });
  }
  
  static calculateSalary(body) {
    console.log('calculateSalary called...');
    console.log(body);
    return HttpUtils.post({ url: 'emp/calculateSalary', body });
  }

   //薪酬计算结果查询
  static queryPrivSalaryResult(body) {
    console.log("service queryPrivSalaryResult called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/queryPrivSalaryResult', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //薪酬调整
  static updateSalaryResult(body) {
    console.log("service updateSalaryResult called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/updateSalaryResult', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //审核
  static approvedSalaryResult(body) {
    console.log("service approvedSalaryResult called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/approvedSalaryResult', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //打回或通过的数据
  static querySalaryResult(body) {
    console.log("service querySalaryResult called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/querySalaryResult', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //查看汇总金额
  static collectSalaryResult(body) {
    console.log("service collectSalaryResult called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/collectSalaryResult', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //查询薪酬,无须权限
  static querySalaryResult(body) {
    console.log("service querySalaryResult called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/querySalaryResult', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  //查询岗位
  static searchPositions(body) {
    console.log("service querySalaryResult called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/search_position', body });
    //  return request('http://localhost:8080/emp/search_position');
  }

  // 清理
  static clearSalaryResult(body) {
    console.log("service clearSalaryResult called...");
    console.log(body);
    return HttpUtils.post({ url: 'emp/clearSalaryResult', body });
    //  return request('http://localhost:8080/emp/search_position');
  }
}