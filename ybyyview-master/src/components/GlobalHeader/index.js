import React, { PureComponent } from 'react';
import { Spin } from 'snk-web';
import styles from './index.less';
import { ThemeConfig, changeTheme } from '../../theme/themeConfig';
import '../../assets/icon/iconfont.js';

export default class GlobalHeader extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fullScreen: false,
    };
  }
  componentDidMount = () => {
    document.addEventListener('fullscreenchange', () => {
      this.fullScreenChange();
    });
    document.addEventListener('webkitfullscreenchange', () => {
      this.fullScreenChange();
    });
    document.addEventListener('mozfullscreenchange', () => {
      this.fullScreenChange();
    });
    document.addEventListener('MSFullscreenChange', () => {
      this.fullScreenChange();
    });
  }
  fullScreenChange = () => {
    this.setState({
      fullScreen: !this.state.fullScreen,
    }, () => {
    });
  }
  themeChange = (data) => {
    changeTheme(data);
  }

  fullScreen = () => {
    if (this.state.fullScreen) {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    } else {
      const el = document.documentElement;
      const rfs = el.requestFullScreen || el.webkitRequestFullScreen
        || el.mozRequestFullScreen || el.msRequestFullscreen;
      if (typeof rfs !== 'undefined' && rfs) {
        rfs.call(el);
      }
    }
  }
  exit = () => {
    // eslint-disable-next-line
    const img = document.createElement('img');
    // eslint-disable-next-line
    img.src = SSOLOGOUTSERVER;
    try {
      setTimeout(() => {
        this.props.dispatch({
          type: 'umssouserinfo/getUserInfo',
        }).then(() => {
        });
      }, 300);
    } catch (e) {
      console.log(e);
    }
  }
  render() {
    const isIE = !!window.ActiveXObject || "ActiveXObject" in window; 
    const {
      currentUser,
    } = this.props;
    let userName = currentUser.principal ? currentUser.principal.name : '';
    if (currentUser.principal
      && currentUser.principal.attributes && currentUser.principal.attributes.USERNAME) {
      userName = currentUser.principal.attributes.USERNAME;
    }
    const themeBlocks = [];
    // eslint-disable-next-line
    for (const key in ThemeConfig) {
      const item = ThemeConfig[key];
      themeBlocks.push(
        <div
          key={item.key}
          onClick={this.themeChange.bind(this, key)}
          style={{
            cursor: 'pointer',
            backgroundColor: item.color,
            marginRight: 20,
            width: 20,
            height: 20,
            display: 'inline-block',
          }}
        />
      );
    }
    return (
      <div className={styles.header}>
        {
          isIE? null : <a title={this.state.fullScreen ? '退出全屏' : '全屏'} onClick={this.fullScreen.bind(this)} style={{ position: 'absolute', top: 8, right: 420 }} className="primary-fill">
          <svg style={{ width: 20, height: 20 }} aria-hidden="true">
              <use xlinkHref={this.state.fullScreen ? '#icon-suoxiaopingmu' : '#icon-quanping1'} />
            </svg>
          </a>
        }
        <div style={{ position: 'absolute', top: 7, right: 240 }}>{themeBlocks}</div>
        <div className={styles.right}>
          {userName ? (
            <span className={`${styles.account}`}>
              <span className={styles.name}>{userName}</span>
              <a style={{ marginLeft: 10 }} onClick={this.exit.bind(this)}>退出</a>
            </span>
          ) : <Spin size="small" style={{ marginLeft: 8 }} />}
        </div>
      </div>
    );
  }
}

