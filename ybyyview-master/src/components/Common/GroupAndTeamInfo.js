
import { Icon} from 'snk-web';
import styles from './GroupAndTeamInfo.less'
import {queryGroupNumAndTeamNum} from '../../services/api'
export default class GroupAndTeamInfo extends React.Component{
    state={
        groupCount:'',
        teamCount:''
    }

    componentDidMount(){
        queryGroupNumAndTeamNum().then(response=>{
            console.log(response)
            this.setState({
              ...response.data
            })
          })
    }

    render(){
        return(
            <div className={styles.baseInfo}><span ><Icon type="info-circle" theme="filled" style={{color:'#108ee9'}} />   一共{this.state.groupCount}个团队;{this.state.teamCount}个组别</span></div>
        )
    }
}