import React, { PureComponent } from 'react' ;
import moment from 'moment';
import { connect } from 'dva';
import {
  Row,
  Col,
  Select,
  Form,
  Input,
  Checkbox,
  DatePicker,
  Divider,
  InputNumber,
  Button,
  message,
  Popconfirm,
  Icon,
} from 'snk-web';
import Styles from './index.less';
import { getSelectChild, getDataSource } from '../../utils/utils';
const FormItem = Form.Item;
const { Option } = Select;

const FieldsMap = {
  "insrncName": "benfClntNmeB", // 姓名
  "insrncSex": "beneficiarySex", // 性别
  "insrncCardType": "beneficiaryCertfCls", // 证件类型
  "insrncCardId": "beneficiaryCertfCde", // 证件号
  "identifyLongValidI": "identifyLongValidB", // 证件类型是否长期
  "identifyExpiryI": "identifyExpiryB", // 证件有效期
  "insrncAddress": "beneficiaryAddr", // 地址
  "insrncMobile": "beneficiaryMobile", // 手机号
  "countryI": "countryA", // 国籍
  "occupationCodeI": "occupationCodeB", // 职业
};

@connect(({ selfchannelForm, loading }) => ({
  selfchannelForm,
  loading: loading.effects,
}))
@Form.create()
export default class SingleShouYiRen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isBLongLess: props.policyMapInfo && props.policyMapInfo.identifyLongValidB === '1' ? true : false,
      relationIsSelf: false,
    }
  }

  componentDidMount = () => {
    this.props.dispatch({
      type: 'selfchannelForm/checkCodeType',
    });
  }

  nBenfPropBChange = (value) => {
    // console.log(value);
  }

  // 受益人同步被保人
  SameWithTouBaoRen = () => {
    for(const key in FieldsMap){
      const curKey = FieldsMap[key];
      const value = this.props.parentForm.getFieldValue(key);
      if (curKey === 'identifyLongValidB') {
        this.setState({
          isBLongLess: value,
        });
      }
      this.props.form.setFieldsValue({
        [curKey]: value,
      });
    }
  }

  // 确认删除受益人
  deleteBeneificiary = () => {
    const { index } = this.props;
    this.props.DeleteBeneificiary(index);
  }

  // 确认添加受益人
  confirmAddBeneificiary = () => {
    const { index } = this.props;
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!fieldsValue.identifyExpiryB && !fieldsValue.identifyLongValidB) {
        message.warning('请选择证件有效期');
        return false;
      }
      if (!fieldsValue.identifyLongValidB) {
        // 证件有止期， identifyLongValidB为0,；
        // 证件是否长期， 1表示长期， 0表示有止期
        fieldsValue.identifyLongValidB = '0'; // 证件是否长期， 1表示长期， 0表示有止期
        fieldsValue.identifyExpiryB = fieldsValue.identifyExpiryB.format("YYYY-MM-DD"); // 证件是否长期， 1表示长期， 0表示有止期
      } else {
        // 证件无止期 identifyLongValidB为0,；
        fieldsValue.identifyLongValidB = '1'; // 证件是否长期， 0表示长期， 1表示有止期
        fieldsValue.identifyExpiryB = '9999-12-30';
      }
      this.props.dispatch({
        type: 'selfchannelForm/savebeneficiaryList',
        payload: {
          [index]: fieldsValue,
        },
      });
      this.props.confirmAddBeneficiary(fieldsValue, index); // 确认添加受益人至父级组件
      message.success('添加成功');
    });
  }

  relationChange = (value) => {
    if (value === '120006') {
      this.setState({
        relationIsSelf: true,
      });
    } else {
      this.setState({
        relationIsSelf: false,
      });
    }
  }

  renderSelectOption = (data) => {
    return data.map(item => (
        <Option key={item.code} value={item.code}>{item.codeName || item.name}</Option>
    ));
  }

  render () {
    const {
      form: {
        getFieldDecorator
      },
      policyMapInfo = {},
      loading,
      selfchannelForm,
      isSameWithTouBaoRen,
      isDeleteBeneificiary,
    } = this.props;
    const {
      CountryCode,
      ItemAccOccupationCode,
      relationBetweenCode,
    } = selfchannelForm;
    // const { relation1 } = getDataSource('selectSource');
    return (
      <div className={Styles.warpper}>
        <Divider/>
        {
          !isSameWithTouBaoRen ? (
            <Button onClick={this.SameWithTouBaoRen.bind(this)} size='small' type='primary'>同被投保人</Button>
          ) : null
        }
        <Row gutter={{ md: 24, lg: 24}}>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="受益人与被保人关系">
              {getFieldDecorator('insrncRelation', {
                rules: [{ required: true, message: '受益人与被保人关系必填!' }],
                initialValue: policyMapInfo.insrncRelation,
              })(
                <Select optionFilterProp="children" showSearch disabled={isSameWithTouBaoRen} placeholder="请选择" style={{ width: '100%' }} onChange={this.relationChange}>
                  {this.renderSelectOption(relationBetweenCode)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="受益人姓名">
              {
                getFieldDecorator('benfClntNmeB', {
                  rules: [{ required: true, message: '受益人姓名必填!' }],
                  initialValue: policyMapInfo.benfClntNmeB,
                })(
                  <Input
                    // style={{color: policyMapInfo && (this.props.form.getFieldValue('benfClntNmeB') === policyMapInfo.benfClntNmeB) ? '' : '#ff0000'}}
                    readOnly={isSameWithTouBaoRen}
                    placeholder="请输入"
                  />
                )
              }
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="受益人性别">
              {getFieldDecorator('beneficiarySex', {
                rules: [{ required: true, message: '受益人性别必填!' }],
                initialValue: policyMapInfo.beneficiarySex,
              })(<Select disabled={isSameWithTouBaoRen} placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild('sex')}
              </Select>)}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="证件类型">
              {
                getFieldDecorator('beneficiaryCertfCls',{
                  initialValue: policyMapInfo.beneficiaryCertfCls,
                  rules: [
                    { required: true, message: '受益人证件类型必填!' }
                  ],
                })(<Select disabled={isSameWithTouBaoRen} placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild("certiDesc")}
                </Select>)
              }
            </FormItem>
          </Col>  
          <Col md={8} lg={8} sm={16}>
            <FormItem label="证件号码">
              {getFieldDecorator('beneficiaryCertfCde',{
                  initialValue: policyMapInfo.beneficiaryCertfCde,
                  rules: [
                    { required: true, message: '受益人证件号码必填!' },
                  ],
                })(
                <Input
                    // style={{color: policyMapInfo && (this.props.form.getFieldValue('beneficiaryCertfCde') === policyMapInfo.beneficiaryCertfCde) ? '' : '#ff0000'}}
                    readOnly={isSameWithTouBaoRen} placeholder="请输入"
                />)}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="联系方式">
              {getFieldDecorator('beneficiaryMobile',{
                initialValue: policyMapInfo.beneficiaryMobile,
                rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"},{ required: true, message: '受益人联系方式必填!' }],
              })(
                <Input
                  // style={{color: policyMapInfo && (this.props.form.getFieldValue('beneficiaryMobile') === policyMapInfo.beneficiaryMobile) ? '' : '#ff0000'}}
                  readOnly={isSameWithTouBaoRen} placeholder="请输入"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={8}  className="isBLongLess">
            <FormItem label="证件有效期">
              {getFieldDecorator('identifyLongValidB', {
                rules: [{ required: true, message: '证件有效期必填!' }],
                valuePropName: 'checked',
                initialValue: policyMapInfo.identifyLongValidB === '1' ? true : false,
              })(
                <Checkbox disabled={isSameWithTouBaoRen} onChange={() => this.setState((state) => ({
                  isBLongLess: !state.isBLongLess,
                }))}>长期</Checkbox>
              )}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={8}  className="isBLongLess">
            <FormItem label="证件有效止期">  
              {getFieldDecorator('identifyExpiryB', {
								initialValue: policyMapInfo.identifyExpiryB ? moment(moment(policyMapInfo.identifyExpiryB).format('YYYY-MM-DD'), 'YYYY-MM-DD') : '',
                })(
                  <DatePicker
                    disabled={isSameWithTouBaoRen || this.state.isBLongLess}
                    allowClear={true}
                    style={{ width: '100%' }}
                  />
                )
              }
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="地址">
              {getFieldDecorator('beneficiaryAddr',{
                initialValue: policyMapInfo.beneficiaryAddr,
                rules: [{ required: true, message: '受益人地址必填!' },{pattern:/^(?=([\u4e00-\u9fa5].*){5})/,'message':"地址不少于5个汉字"},],
              })(
                <Input
                  // style={{color: policyMapInfo && (this.props.form.getFieldValue('beneficiaryAddr') === policyMapInfo.beneficiaryAddr) ? '' : '#ff0000'}}
                  readOnly={isSameWithTouBaoRen} placeholder="请输入"
                />)}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={8}>
            <FormItem label="国籍">
              {getFieldDecorator('countryB',{
                initialValue: policyMapInfo.countryB || '1010090156',
                rules: [{ required: true, message: '国籍必填!' }],
              })(
                <Select optionFilterProp="children" showSearch disabled={isSameWithTouBaoRen} placeholder="请选择" style={{ width: '100%' }}>
                  {this.renderSelectOption(CountryCode)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="受益比例">
              {
                getFieldDecorator('nBenfProp', {
                  initialValue: policyMapInfo.nBenfProp,
                  rules: [{ required: true, message: '受益比例必填!' }],
                })(
                  <InputNumber
                    min={0}
                    max={100}
                    disabled={isSameWithTouBaoRen}
                    formatter={value => `${value}%`}
                    parser={value => value.replace('%', '')}
                    onChange={this.nBenfPropBChange}
                    // style={{color: policyMapInfo && (this.props.form.getFieldValue('nBenfProp') === policyMapInfo.nBenfProp) ? '' : '#ff0000'}}                    
                  />
                )
              }
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="受益人职业">
              {getFieldDecorator('occupationCodeB',{
                initialValue: policyMapInfo.occupationCodeB,
                rules: [{ required: true, message: '收益人职业必填!' }],
              })(
                <Select
                  disabled={isSameWithTouBaoRen}
                  showSearch
                  optionFilterProp="children"
                  placeholder="请选择"
                >
                  {this.renderSelectOption(ItemAccOccupationCode)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        {
          !isDeleteBeneificiary ? (
            <Popconfirm title="确认删除受益人？" onConfirm={this.deleteBeneificiary} okText="确认" cancelText="取消">
              <Button
                size='small'
                type="danger"
                style={{
                  marginRight: '10px',
                }}
                disabled={isSameWithTouBaoRen}
              >删除受益人</Button>
            </Popconfirm>
          ) : null
        }
        {
          !isDeleteBeneificiary ? (
          <Button
            size='small'
            loading={loading['selfchannelForm/saveBeneificiaryList']}
            type="primary"
            disabled={isSameWithTouBaoRen}
            onClick={this.confirmAddBeneificiary}
          >确认添加
          {
            Object.keys(policyMapInfo).length ? (<Icon type="check-circle" />) : null
          }
          </Button>
          ) : null
        }
      </div>
    );
  }
}