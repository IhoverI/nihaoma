import React from 'react';

export default class StateContainer extends React.Component{
    constructor(props){
        super(props);
        let step = 0;
        if(isNaN(props.step)){
            step = 0;
        }else{
            step = parseInt(props.step);
        }
        this.state={
            step
        };
    }
    componentWillReceiveProps(nextProps,nextState){
        if(nextProps.step!==this.state.step){
            this.setState({
                step:nextProps.step
            });
        }
    }
    render(){
        const children = [];
        let style = {};
        for(let i=0,j=this.props.stepChildren.length;i<j;i++){
            style = {};
            if(i>this.state.step){
                break;
            }

            if(i!==this.state.step){
                style={display:'none'};
            }
            children.push(<div style={style} key={i}>{this.props.stepChildren[i]}</div>);
        }
        return <div>{children}</div>;
    }
}