import React from 'react';
import { Form, Card, Row, Col,Input } from 'snk-web';
import { registerMap } from 'echarts';
const formItemLayout = {
    labelCol: {
      xs: { span: 12 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 12 },
      sm: { span: 16 },
    },
};
const formItemLayoutTwo = {
    labelCol: {
        xs: { span: 12 },
        sm: { span: 7 },
    },
    wrapperCol: {
        xs: { span: 12 },
        sm: { span: 17 },
    },
};
const FormItem = Form.Item;
//性别
const sexSwitchFunc=(sex)=>{
    if(sex ==='01') return '男';
    if(sex ==='02') return '女';
    return null;
}
export const cardTypeOptions = [
    {label:'身份证',value:'01'},
    {label:'护照',value:'02'},
    {label:'军官证',value:'03'},
    {label:'台胞证',value:'05'},
    {label:'港澳返乡证',value:'06'},
    {label:'出生证',value:'07'},
    {label:'其他',value:'99'}
];

// 投保证件类型
const cardTypeSwitchFunc = (cardType)=>{
    switch(cardType){
        case '01': return '身份证';
        case '02': return '护照';
        case '03': return '军官证';
        case '05': return '台胞证';
        case '06': return '港澳返乡证';
        case '07': return '出生证';
        case '99': return '其他';
        default: return '';
    }
}
const applicantTypeDesc=['初始化', '活跃客户', '脱保客户'];
const UserBaseInfoView = (props) =>{
    const { datas = {} } = props;
    return(
        <Form>
            <Card title="客户基本信息">
                <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="投保人姓名"
                            {...formItemLayout}
                        >
                            <Input value={datas.clientName} disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="性别"
                            {...formItemLayout}
                        >
                            <Input value={sexSwitchFunc(datas.sex)}  disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="年龄"
                            {...formItemLayout}
                        >
                            <Input value={datas.age}  disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="投保人证件类型"
                            {...formItemLayout}
                        >
                            <Input value={cardTypeSwitchFunc(datas.cCertfCls)} disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="投保人证件号码"
                            {...formItemLayout}
                        >
                            <Input value={datas.cCertfCde} disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="投保人手机号码"
                            {...formItemLayout}
                        >
                            <Input value={datas.cMobileNo} disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="投保人类型"
                            {...formItemLayout}
                        >
                            <Input value={applicantTypeDesc[datas.applicantType]}  disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="实名状态"
                            {...formItemLayout}
                        >
                            <Input value={datas.verifynameFlag}  disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="实名手机号"
                            {...formItemLayout}
                        >
                            <Input value={datas.cRegMobileNo}  disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="已购买产品数量"
                            {...formItemLayout}
                        >
                            <Input value={datas.prodCount}  disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="原始历史积分"
                            {...formItemLayout}
                        >
                            <Input value={datas.originHistIntegral} disabled />
                        </FormItem>
                    </Col>
                    <Col md={8} sm={22}>
                        <FormItem
                            label="已赠送服务数量"
                            {...formItemLayout}
                        >
                            <Input value={datas.serviceCount}  disabled />
                        </FormItem>
                    </Col>
                    <Col md={9} sm={22}>
                        <FormItem
                            label="微信ID"
                            {...formItemLayoutTwo}
                        >
                            <Input value={datas.openId}  disabled />
                        </FormItem>
                    </Col>
                </Row>
            </Card>
        </Form>   
    );
}

export default UserBaseInfoView;