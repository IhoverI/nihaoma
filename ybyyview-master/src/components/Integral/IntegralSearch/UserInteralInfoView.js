import React from 'react';
import { Form, Card, Row, Col,Input } from 'snk-web';
const formItemLayout = {
    labelCol: {
      xs: { span: 12 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 12 },
      sm: { span: 16 },
    },
};
const FormItem = Form.Item;

const UserInteralInfoView = (props) =>{
    const { datas = {} } = props;
    return(
        <Form>
            <Card bordered={false} title="积分情况">
                <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                    <Col md={6} sm={20}>
                        <FormItem
                            label="等级积分"
                            {...formItemLayout}
                        >
                            <Input value={datas.rankIntegral} disabled />
                        </FormItem>
                    </Col>
                    <Col md={6} sm={20}>
                        <FormItem
                            label="总消费积分"
                            {...formItemLayout}
                        >
                            <Input value={datas.totalConsumeIntegral} disabled />
                        </FormItem>
                    </Col>
                    <Col md={6} sm={20}>
                        <FormItem
                            label="可兑换积分"
                            {...formItemLayout}
                        >
                            <Input value={datas.availableIntegral} disabled />
                        </FormItem>
                    </Col>
                    <Col md={6} sm={20}>
                        <FormItem
                            label="已兑换积分"
                            {...formItemLayout}
                        >
                            <Input value={datas.exchangedIntegral} disabled />
                        </FormItem>
                    </Col>
                    <Col md={6} sm={20}>
                        <FormItem
                            label="赠送积分"
                            {...formItemLayout}
                        >
                            <Input value={datas.presentIntegral} disabled />
                        </FormItem>
                    </Col>
                    <Col md={6} sm={20}>
                        <FormItem
                            label="已冻结积分"
                            {...formItemLayout}
                        >
                            <Input value={datas.frozenIntegral} disabled />
                        </FormItem>
                    </Col>
                    <Col md={6} sm={20}>
                        <FormItem
                            label="已过期积分"
                            {...formItemLayout}
                        >
                            <Input value={datas.expiredIntegral} disabled />
                        </FormItem>
                    </Col>
                </Row>
            </Card>
        </Form>   
    );
}

export default UserInteralInfoView;