import moment from 'moment';

// 红包查询--table 头
export const redPacketColumns =(onlinkToPacketList)=>{
  return [
      {
        title: '序号',
        dataIndex: 'orderId',
        key: 'orderId',
        width:'80px',
        align: 'center',
        render: (text, record, index) => index+1,
    },
    {
        title: '推送日期',
        dataIndex: 'pushTime',
        key: 'pushTime',
        width:'120px',
        align: 'center',
        render: (text, record, index) => text ? moment(text).format('YYYY-MM-DD') : '',
    },
    {
        title: '推送批次',
        dataIndex: 'pushBatchNo',
        key: 'pushBatchNo',
        width:'180px',
        align: 'center',
    },
    {
        title: '推送客户总数',
        dataIndex: 'userTotal',
        key: 'userTotal',
        width:'120px',
        align: 'center',
    },
    {
        title: '红包投放总金额',
        dataIndex: 'totalMoney',
        key: 'totalMoney',
        width:'120px',
        align: 'center',
    },
    {
        title: '领取一级红包人数',
        dataIndex: 'getMoneyOneNum',
        key: 'getMoneyOneNum',
        width:'120px',
        align: 'center',
    },
    {
        title: '领取二级红包人数',
        dataIndex: 'getMoneyTwoNum',
        key: 'getMoneyTwoNum',
        width:'120px',
        align: 'center',
    },
    {
      title: '领取三级红包人数',
        dataIndex: 'getMoneyThreeNum',
        key: 'getMoneyThreeNum',
        width:'120px',
        align: 'center',
    },
    {
        title: '领取四级红包人数',
        dataIndex: 'getMoneyFourNum',
        key: 'getMoneyFourNum',
        width:'120px',
        align: 'center',
    },
    {
      title: '其他金额',
      dataIndex: 'otherUsedMoney',
      key: 'otherUsedMoney',
      width:'120px',
      align: 'center',
    },
    {
      title: '其他金额领取人数',
      dataIndex: 'getMoneyFiveNum',
      key: 'getMoneyFiveNum',
      width:'120px',
      align: 'center',
    },
    {
      title: '红包领取总金额',
      dataIndex: 'usedMoney',
      key: 'usedMoney',
      width:'120px',
      align: 'center',
    },
    {
      title: '备注',
      dataIndex: 'creator',
      key: 'creator',
      width:'120px',
      align: 'center',
      render: (text, record, index) => {
          return(
            <span>
                 <a
                    href="javascript:;"
                    onClick={() => onlinkToPacketList(record.pushBatchNo)}
                 >
                    客户红包领取清单
                 </a>
            </span>
          )
      }
    }
  ]
}
  
export const redPacketListColumns =()=>{
  return [
      {
        title: '序号',
        dataIndex: 'orderId',
        key: 'orderId',
        width:'80px',
        align: 'center',
        render: (text, record, index) => index+1,
    },
    {
        title: '客户姓名',
        dataIndex: 'userName',
        key: 'userName',
        width:'120px',
        align: 'center',
    },
    {
        title: '身份证号',
        dataIndex: 'cCertfCde',
        key: 'cCertfCde',
        width:'150px',
        align: 'center',
    },
    {
        title: '手机号',
        dataIndex: 'mobile',
        key: 'mobile',
        width:'120px',
        align: 'center',
    },
    {
        title: '红包金额',
        dataIndex: 'money',
        key: 'money',
        width:'120px',
        align: 'center',
    },
    {
        title: '是否打开H5链接', //  0否 1是
        dataIndex: 'isOpenH5',
        key: 'isOpenH5',
        width:'150px',
        align: 'center',
        render: (text, record, index) => !!text ? '是' : '否',
    },
    {
        title: '是否实名认证', // 0否 1是
        dataIndex: 'receiveMoneyStatus',
        key: 'receiveMoneyStatus',
        width:'120px',
        align: 'center',
        render: (text, record, index) => !!text ? '是' : '否',
    },
    {
        title: '是否领取红包', //  0否 1是
        dataIndex: 'isReceiveMoney',
        key: 'isReceiveMoney',
        width:'120px',
        align: 'center',
        render: (text, record, index) => !!text ? '是' : '否',
    },
    {
        title: '领取红包成功时间',
        dataIndex: 'accountDate',
        key: 'accountDate',
        width:'120px',
        align: 'center',
        render: (text, record, index) => text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : '',
    },
  ]
}
  
  //表单布局
  export const formItemLayout = {
    labelCol: {
      xs: { span: 12 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 12 },
      sm: { span: 16 },
    },
};


/**
 * @param {object} 传入数值 如：query
 * @returns {string} obj
 */
export const QueryStringParse = (locationSearch) => {
    const queryString = locationSearch.slice(1);
    const resultArr = queryString.split('&').map((item) => {
      const key = item.split('=')[0];
      const value = decodeURIComponent(item.split('=')[1]);
      return { [key]: value };
    });
    return resultArr.reduce((pre, cur) => {
      return { ...pre, ...cur };
    }, {});
  };