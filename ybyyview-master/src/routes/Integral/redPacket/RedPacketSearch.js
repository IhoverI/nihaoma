import React from 'react';
import { connect } from 'dva';
import { 
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  DatePicker,
  Table,
} from 'snk-web';

import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { formItemLayout, redPacketColumns,
} from './redPacketConf';

const FormItem = Form.Item;
const { RangePicker } = DatePicker;

@connect(({ integralModel,loading }) => ({
  integralModel,
  loading: loading.effects,
}))
@Form.create()
export default class RedPacketSearch extends React.Component {
  constructor(props) {
      super(props);
      this.fieldsValue={};
  }
  handleSearch = e => {
    //防止链接打开 URL
    if(e)e.preventDefault();
    const { dispatch, form } = this.props;
    //表单校验
    form.validateFields((err, fieldsValue) => {
        if (err) return;
        // console.log('Value',fieldsValue);
        const {datePick, pushBatchNo} = fieldsValue || {};
        if(datePick) {
            this.fieldsValue={
                startDate: datePick[0].format("YYYY-MM-DD 00:00:00"),
                endDate:datePick[1].format("YYYY-MM-DD 23:59:59"),
                pushBatchNo,
            };
        } else{
            this.fieldsValue={pushBatchNo};
        }
        this.handleTablePage();
    });
  }
  handleTablePage=(pageSizeOptions = {})=>{
      const { current = 1, pageSize = 10 } = pageSizeOptions;
      const formParams = this.fieldsValue;
      this.props.dispatch({
          type:'integralModel/queryIntergralRedPacketSearch',
          payload:{pageNum:current, pageSize, ...formParams}
      })
  }

     //重置表单
  handleReset = () => {
    this.props.form.resetFields();
  }
  onlinkToPacketList=(pushBatchNo)=>{
    this.props.history.push(`/integral/redPacket-integral/redPacketCustomerList?pushBatchNo=${pushBatchNo}`);
  }
  renderAdvancedForm(){
    const { getFieldDecorator } = this.props.form;
    const {effects = {}} = this.props.loading;
    console.log(this.props.loading);
    return(
        <Card bordered={false}>
            <Form onSubmit={this.handleSearch}>
                <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                    <Col md={10} sm={24}>
                        <FormItem
                            label="活动日期"
                            {...formItemLayout}
                            style={{marginBottom:'0'}}
                        >
                            {getFieldDecorator('datePick')
                            (
                             <RangePicker/>
                            )}
                        </FormItem>
                    </Col>
                    <Col md={10} sm={24}>
                        <FormItem
                              label="活动批次"
                              style={{marginBottom:'0'}}
                              {...formItemLayout}
                        >
                            {getFieldDecorator('pushBatchNo')(
                                <Input placeholder="请输入" />
                            )}
                        </FormItem>
                    </Col>
                </Row>
                <Row>
                    <Col style={{textAlign: 'center'}}>
                        <Button
                              type="primary"
                              htmlType="submit"
                              disabled={this.props.loading['integralModel/queryIntergralRedPacketSearch']}
                              loading={this.props.loading['integralModel/queryIntergralRedPacketSearch']}
                        >
                            搜索
                        </Button>
                        <Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
        </Card>)
  }

  render() {
    const {repacketSearchList = [], redPacketSearchListTotal } = this.props.integralModel || {};
    return(
      <PageHeaderLayout>
        <Card bordered={false}>
            {this.renderAdvancedForm()}
            <Table
                bordered
                columns={redPacketColumns(this.onlinkToPacketList)}
                dataSource={repacketSearchList}
                loading={this.props.loading['integralModel/queryIntergralRedPacketSearch']}
                size="small"
                rowKey={record => record.pushBatchNo}
                onChange={this.handleTablePage}
                pagination={{
                    pageSizeOptions: ['5','10','20','30','40','50'],
                    showQuickJumper: true,
                    showSizeChanger: true,
                    showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                    total:redPacketSearchListTotal,
                }}
            />
        </Card>
    </PageHeaderLayout>
    )
  }

}