import React, { Fragment } from 'react';
import { connect } from 'dva';
import { 
  Card,
  Table,
  Form,
  Button,
} from 'snk-web';

import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { formItemLayout, redPacketListColumns,QueryStringParse,
} from './redPacketConf';


@connect(({ integralModel,loading }) => ({
  integralModel,
  loading: loading.effects,
}))
@Form.create()
export default class RedPacketSearch extends React.Component {
  constructor(props) {
      super(props);
      const { search } = this.props.location;
      this.fieldsValue={};
      if(search) { 
        const { pushBatchNo } = QueryStringParse(search);
        this.fieldsValue={pushBatchNo};
      }
  }
  componentDidMount() {
    this.handleTablePage();
  }

  handleTablePage=(pageSizeOptions = {})=>{
      const { current = 1, pageSize = 10 } = pageSizeOptions;
      const formParams = this.fieldsValue;
      this.props.dispatch({
          type:'integralModel/queryIntergralRedPacketList',
          payload:{pageNum:current, pageSize, ...formParams}
      })
  }

	exportExcel = () => {
		// 导出数据
    const formParams = this.fieldsValue;
		this.props.dispatch({
			type: 'integralModel/extendBatch',
			payload: {
				...formParams,
			},
		}).then(response=>{
			if(response.code!==0){
				message.error(response.message)
			}
		});
	}

  render() {
    const {repacketList = [], redPacketListTotal } = this.props.integralModel || {};
    return(
      <PageHeaderLayout>
        <Card bordered={false}>
            <Table
                title={
									() => {
										return (
                      <Fragment>
                        <span>客户红包领取详情</span>
											  <Button style={{ marginLeft: '10px' }} type="primary"  loading={this.props.loading['integralModel/extendBatch']} onClick={this.exportExcel}>导出excel</Button>
                      </Fragment>
										);
									}
								}
                bordered
                columns={redPacketListColumns()}
                dataSource={repacketList}
                loading={this.props.loading['integralModel/queryIntergralRedPacketList']}
                size="small"
                rowKey={record => record.cCertfCde}
                onChange={this.handleTablePage}
                pagination={{
                    pageSizeOptions: ['5','10','20','30','40','50'],
                    showQuickJumper: true,
                    showSizeChanger: true,
                    showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                    total:redPacketListTotal,
                }}
            />
        </Card>
    </PageHeaderLayout>
    )
  }
}