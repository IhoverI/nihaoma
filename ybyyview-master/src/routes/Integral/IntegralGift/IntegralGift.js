import React from 'react';
import { connect } from 'dva';
import { 
    Table, 
    Row,
    Col,
    Card,
    Form,
    DatePicker,
    Modal,
    Button,
    Upload,
    message,
    Icon,
} from 'snk-web';
import moment from 'moment';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { formItemLayout, getIntegralListColumns,
 } from './IntegralGiftConfig';
 import {components} from './EditableCell';
 import {server, microservices} from '../../../services/integralService';

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const serverUrl= `${server || SERVERLOCAL }${microservices}`;
@connect(({ integralModel,loading }) => ({
    integralModel,
    loading,
}))
@Form.create()
export default class SearchIntegral extends React.Component { 
    constructor(props){
        super(props);
        this.state = { editingId: '', upLoadStatus: false };
        this.fieldsValue={};
    }
    
    //重置表单
    handleReset = () => {
        this.props.form.resetFields();
    }

    //下载模板
    downloadTemplate =()=>{
        this.props.dispatch({
            type:'integralModel/integralGiftDownloadTemplate',
            payload:{}
        })
    }
    handleSearch = e => {
        //防止链接打开 URL
        if(e)e.preventDefault();
        const { dispatch, form } = this.props;
        //表单校验
        form.validateFields((err, fieldsValue) => {
            if (err) return;
            // console.log('Value',fieldsValue);
            const {datePick = []} = fieldsValue || {};
            this.fieldsValue={
                startDate: datePick[0].format("YYYY-MM-DD 00:00:00"),
                endDate:datePick[1].format("YYYY-MM-DD 23:59:59"),
            };
            this.handleTablePage();
        });
    }
    handleTablePage=(pageSizeOptions = {})=>{
        const { current = 1, pageSize = 10 } = pageSizeOptions;
        const formParams = this.fieldsValue;
        this.props.dispatch({
            type:'integralModel/queryIntegralGiftList',
            payload:{pageNum:current, pageSize, ...formParams}
        })
    }
    uploadPersonInfo = {
        showUploadList:false,
        name: 'file',
        action: `${serverUrl}integral/largessIntegarl`,
        withCredentials: true,
        beforeUpload: ()=>{
            this.setState({
                upLoadStatus: true,
            })
            return true;
        },
        onChange: (info) => {
          const { response = {} } = info.file || {};
          if (info.file.status === 'done' && response.code === 0) {
              if( response.data && response.data.fileName){
                this.downUploadFailFile(response.data.fileName);
                Modal.error({
                    title: '赠送积分客户名单上传',
                    content: `部分客户名单因格式问题上传失败，具体问题详见《${response.data.fileName}》`,
                });
                this.setState({
                    upLoadStatus: false,
                })
                return;
              }
             message.success('赠送积分客户名单上传成',2);
             this.setState({
                upLoadStatus: false,
            })
             return;
          }
          if (info.file.status === 'done' && response.code) {
            Modal.error({
                title: '赠送积分客户名单上传',
                content:response.message,
            });
            this.setState({
                upLoadStatus: false,
            })
            return;
         }
         if(info.file.status === 'error') {
            Modal.error({
                title: '赠送积分客户名单上传',
                content: response.message || '赠送积分客户名单上传失败',
            });
            this.setState({
                upLoadStatus: false,
            })
          } 
        },
      };
     //下载失败上传失败的信息文件
    downUploadFailFile =(fileName)=>{
        this.props.dispatch({
            type:'integralModel/downIntegralGiftUpFailFile',
            payload:{fileName}
        })
    }

    // 是否可编辑
    isEditing = (record) => {
        return record.id === this.state.editingId;
      };
      // 编辑
      edit=(id)=> {
        this.setState({ editingId: id });
      }
      // 保存
      save=(form, id)=> {
        form.validateFields((error, row ={}) => {
            if (error) {
                return;
            }
            const {integralValue, validDate, invalidDay,remark} = row;
            const updateRowdata={
                integralValue,
                validDate: validDate.format("YYYY-MM-DD"),
                invalidDay: invalidDay.format("YYYY-MM-DD"),
                remark,
             }
            this.props.dispatch({
                type:'integralModel/updateIntegralGiftRow',
                payload:{updateRowdata, id}
            }).then(()=>{
                this.setState({ editingId: '' });
            }) 
          });
      }
      // 删除
      onDelete=(record)=>{
        this.props.dispatch({
            type:'integralModel/delIntegralGiftRow',
            payload:{...record},
        }).then(()=>{
            this.setState({ editingId: '' });
        }) 
      }
      // 取消
      cancel = () => {
        this.setState({ editingId: '' });
      };
    
      // table 头
      columns = ()=>{
        const isEditingFunc = this.isEditing;
        const editFunc = this.edit;
        const saveFunc = this.save;
        const delFunc = this.onDelete;
        const cancelFunc = this.cancel;
        return getIntegralListColumns({isEditingFunc, editFunc, saveFunc, cancelFunc, delFunc}).map((col) => {
            if (!col.editable) {
              return col;
            }
            return {
              ...col,
              onCell: record => ({
                record,
                dataIndex: col.dataIndex,
                title: col.title,
                editing: this.isEditing(record),
              }),
            };
          });
      }
    renderAdvancedForm(){
        const { getFieldDecorator } = this.props.form;
        const {effects} = this.props.loading;
        return(
            <Card bordered={false}>
                <Form onSubmit={this.handleSearch}>
                    <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                        <Col md={10} sm={24}>
                            <FormItem
                                label="赠送积分生效日期"
                                {...formItemLayout}
                                style={{marginBottom:'0'}}
                            >
                                {getFieldDecorator('datePick',{
                                     rules: [{
                                        required: true, message: '请选择日期!',
                                    }],
                                })
                                (
                                 <RangePicker/>
                                )}
                            </FormItem>
                        </Col>
                        <Col md={10} sm={24} style={{textAlign : 'center'}}>
                            <Button type="primary" htmlType="submit" loading={effects['integralModel/queryIntegralGiftList']}>
                                搜索
                            </Button>
                            <Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
                                重置
                            </Button>
                        </Col>
                    </Row> 
                </Form>
            </Card>)
    }
    renderFileBtn=()=>{
        const {effects} = this.props.loading;
        // console.log(effects)
        return(
            <Card bordered={false}>
                <Button
                 style={{marginRight: 20}} 
                 icon='download'
                 disabled={effects['integralModel/integralGiftDownloadTemplate']}
                 loading={effects['integralModel/integralGiftDownloadTemplate']}
                 onClick={this.downloadTemplate}
                >
                 下载模板
                </Button>
                <Upload {...this.uploadPersonInfo}>
                    <Button
                        icon='upload'
                        loading={this.state.upLoadStatus}
                        disabled={this.state.upLoadStatus}
                    >
                      导入赠送积分客户名单
                    </Button>
                </Upload>
            </Card>
        )
    }
    render() {
        const {integralGiftListInfo, giftTotal} = this.props.integralModel || {};
        const {effects} = this.props.loading;
        return (
            <PageHeaderLayout>
                <Card bordered={false}>
                    {this.renderAdvancedForm()}
                    {this.renderFileBtn()}
                    <Table
                        bordered
                        components={components}
                        columns={this.columns()}
                        dataSource={integralGiftListInfo}
                        loading={effects['integralModel/queryIntegralGiftList']}
                        size="small"
                        rowKey={record => record.id}
                        onChange={this.handleTablePage}
                        pagination={{
                            pageSizeOptions: ['10','20','30','40','50'],
                            showQuickJumper: true,
                            showSizeChanger: true,
                            showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                            total:giftTotal,
                        }}
                    />
                </Card>
            </PageHeaderLayout>
        );
    }
}