
import React from 'react';
import { Table, Input,  Popconfirm, Form,  DatePicker } from 'snk-web';
import moment from 'moment';
const FormItem = Form.Item;
// 创建context
export const EditableContext = React.createContext();
// 创建行
const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
      <tr {...props} />
    </EditableContext.Provider>
  );
const EditableFormRow = Form.create()(EditableRow);

// 单元格
class EditableCell extends React.Component {
    //生效日期验证
    onHandleValidDate =(form)=> (rule, value, callback) => {
        const {validDate, invalidDay} = form.getFieldsValue();
        const nowTime = moment().startOf('day').toDate().getTime();
        const startTime = validDate.toDate().getTime();
        const endTime = invalidDay.toDate().getTime();
        if(startTime - endTime > 0){
            callback('大于有效截止日期');
            return;
        }
        // Note: 必须总是返回一个 callback，否则 validateFieldsAndScroll 无法响应
        callback()
    }
    // 截止日期
    onHandleInvalidDay =(form)=> (rule, value, callback) => {
        const {validDate, invalidDay} = form.getFieldsValue();
        const nowTime = moment().startOf('day').toDate().getTime();
        const startTime = validDate.toDate().getTime();
        const endTime = invalidDay.toDate().getTime();
        if(endTime - startTime < 0){
            callback('小于生效日期');
            return;
        }
        if(endTime - nowTime < 0){
            callback('小于当前日期');
            return;
        }
        // Note: 必须总是返回一个 callback，否则 validateFieldsAndScroll 无法响应
        callback()
    }
    getInput = (form) => {
      const { getFieldDecorator } = form;
      const {
        dataIndex,
        record,
      } = this.props;
    //   console.log('props',this.props);
      if (this.props.dataIndex === 'integralValue') {
        return (
            <FormItem style={{ margin: 0 }}>
                {
                    getFieldDecorator(dataIndex, {
                        rules: [{
                            pattern: /^\d+$/,
                            required: true,
                            message: '输入整数',
                        }],
                        initialValue: record[dataIndex],
                    })(<Input />)
                }
            </FormItem>)
      }
      if (this.props.dataIndex === 'validDate') {
        return (
            <FormItem style={{ margin: 0 }}>
                {
                    getFieldDecorator(dataIndex, {
                        rules: [
                            {
                               validator: this.onHandleValidDate(form)
                            }
                        ],
                        initialValue: moment(record[dataIndex]),
                    })(<DatePicker allowClear={false}/>)
                }
            </FormItem>)
      }
      if (this.props.dataIndex === 'invalidDay') {
        return (
            <FormItem style={{ margin: 0 }}>
                {
                    getFieldDecorator(dataIndex, {
                        rules: [
                            {
                                validator: this.onHandleInvalidDay(form)
                             }
                        ],
                        initialValue:  moment(record[dataIndex]),
                    })(<DatePicker allowClear={false}/>)
                }
            </FormItem>)
      }
      if (this.props.dataIndex === 'remark') {
        return (
            <FormItem style={{ margin: 0 }}>
                {
                    getFieldDecorator(dataIndex, {
                        rules: [{
                            required: true,
                            message: "不能为空",
                        }],
                        initialValue: record[dataIndex],
                    })(<Input />)
                }
            </FormItem>)
      }
    };

    ShowFormCell =()=>{
        return this.props.children;
    }
  
    render() {
        const {
            editing,
            dataIndex,
            title,
            record,
            index,
            ...restProps
          } = this.props;
      return (
        <EditableContext.Consumer>
          {(form) => {
            return (
              <td {...restProps}>
                {editing ?  this.getInput(form) : this.ShowFormCell()}
              </td>
            );
          }}
        </EditableContext.Consumer>
      );
    }
  }

export  const components = {
    body: {
      row: EditableFormRow,
      cell: EditableCell,
    },
};