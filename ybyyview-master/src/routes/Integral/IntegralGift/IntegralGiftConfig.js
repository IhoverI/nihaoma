import {  Popconfirm } from 'snk-web';
import {EditableContext} from './EditableCell';
// 积分查询--table 头
export const getIntegralListColumns =({isEditingFunc, editFunc, saveFunc, cancelFunc,delFunc})=>{
    return [
        {
          title: '序号',
          dataIndex: 'orderId',
          width:'100px',
          align: 'center',
          render: (text, record, index) => index+1,
          editable: false,
      },
      {
          title: '用户名称',
          dataIndex: 'clientName',
          width:'120px',
          align: 'center',
          editable: false,
      },
      {
          title: '身份证号',
          dataIndex: 'cCertfCde',
          width:'250px',
          align: 'center',
          editable: false,
      },
      {
          title: '赠送分值',
          dataIndex: 'integralValue',
          width:'120px',
          align: 'center',
          editable: true,
      },
      {
          title: '积分生效日期',
          dataIndex: 'validDate',
          width:'180px',
          align: 'center',
          editable: true,
      },
      {
          title: '积分有效截止日期',
          dataIndex: 'invalidDay',
          width:'180px',
          align: 'center',
          editable: true,
      },
      {
          title: '赠送项目',
          dataIndex: 'remark',
          width:'300px',
          align: 'center',
          editable: true,
      },
      {
        title: '操作',
        dataIndex: 'operation',
        width:'120px',
        align: 'center',
        editable: false,
        render: (text, record) => {
            const editable = isEditingFunc(record);
            return (
              <div>
                {editable ? (
                  <span>
                    <EditableContext.Consumer>
                      {form => (
                        <Popconfirm
                            title="确定修改此行信息吗？"
                            onConfirm={() => saveFunc(form, record.id)}
                            onCancel={() => cancelFunc(record.id)}
                        >
                            <a  style={{ marginRight: 8 }}>保存</a>
                        </Popconfirm>
                      )}
                    </EditableContext.Consumer>
                    <a
                        onClick={() => cancelFunc(record.id)}
                    >
                      取消
                    </a>
                  </span>
                ) : (
                    <span>
                        <a onClick={() => editFunc(record.id)}>编辑</a>
                        <Popconfirm
                            title="确定删除此行信息吗？"
                            onConfirm={() => delFunc(record)}
                            onCancel={() => cancelFunc(record.id)}
                        >
                            <a  style={{ marginLeft: 8 }}>删除</a>
                        </Popconfirm>

                    </span>
                )}
              </div>
            );
          },
      },
    ]
  }
  
  
  //表单布局
  export const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 16 },
      },
  };
  