
// 积分查询--table 头
export const getIntegralListColumns =()=>{
  return [
      {
        title: '积分类型',
        dataIndex: 'type',
        width:'150px',
        align: 'center',
        filters:[
          { 
            text: '可兑换积分',
            value: '可兑换积分消费积分赠送积分',
          },
          { 
            text: '已兑换积分',
            value: '已兑换积分',
          },
          { 
            text: '过期积分',
            value: '过期积分',
          },
          { 
            text: '已冻结积分',
            value: '已冻结积分',
          },
        ],
        filterMultiple: false,
        onFilter: (value, record) => value.includes(record.type),
    },
    {
        title: '产品名称',
        dataIndex: 'prodName',
        width:'120px',
        align: 'center'
    },
    {
        title: '保单号',
        dataIndex: 'cPlyNo',
        width:'150px',
        align: 'center'
    },
    {
        title: '订单号',  
        dataIndex: 'caseDescribe',
        width:'120px',
        align: 'center'
    },
    {
        title: '年化保费',
        dataIndex: 'prm',
        width:'120px',
        align: 'center'
    },
    {
        title: '期次',
        dataIndex: 'period',
        width:'80px',
        align: 'center'
    },
    {
        title: '保险起期',
        dataIndex: 'insrncBeginDate',
        width:'120px',
        align: 'center'
    },
    {
        title: '积分分值',
        dataIndex: 'integralValue',
        width:'120px',
        align: 'center'
    },
    {
        title: '生效日期',
        dataIndex: 'validDate',
        width:'120px',
        align: 'center'
    },
    {
        title: '过期日期', 
        dataIndex: 'invalidDay',
        width:'120px',
        align: 'center'
    },
    {
        title: '备注',
        dataIndex: 'remark',
        width:'120px',
        align: 'center'
    }
  ]
}


//表单布局
export const formItemLayout = {
    labelCol: {
      xs: { span: 12 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 12 },
      sm: { span: 16 },
    },
};

//投保人证件类型：
export const cardTypeOptions = [
    {label:'身份证',value:'01'},
    {label:'护照',value:'02'},
    {label:'军官证',value:'03'},
    {label:'台胞证',value:'05'},
    {label:'港澳返乡证',value:'06'},
    {label:'出生证',value:'07'},
    {label:'其他',value:'99'}
];
