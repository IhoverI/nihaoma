import React, { Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';


import { 
	Modal,
    Table, 
    Row,
    Col,
    Card,
    Form,
    Input,
	Select,
	Popconfirm,
	Message,
    Button} from 'snk-web';
import UserBaseInfoView from 'components/Integral/IntegralSearch/UserBaseInfoView';
import UserInteralInfoView from 'components/Integral/IntegralSearch/UserInteralInfoView';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import { formItemLayout, cardTypeOptions, getIntegralListColumns } from './integralSearchConfig';
import {
	getUrlParam,
} from '../../../utils/utils';

const FormItem = Form.Item;

const columnService = [ // 服务评价列名
  {
    title: '服务评分',
    dataIndex: 'serviceScore',
    align: 'center',
  },
  {
    title: '服务评价',
    dataIndex: 'serviceComment',
    align: 'center',
  },
  // {
  //   title: '评价时间',
  //   dataIndex: 'commentDate',
  //   align: 'center',
  // },
];

const columnOrder = [ // 充值记录列名
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
    render: (text, record, index) => index + 1
  },
  {
    title: '充值账号',
    dataIndex: 'exchangePhone',
    align: 'center',
  },
  {
    title: '充值时间',
    dataIndex: 'rechargeDate',
    align: 'center',
  }
];

const columnExpress = [ // 物流信息列名
  {
    title: '寄送类型',
    dataIndex: 'expressType',
    align: 'center',
  },
  {
    title: '寄送方式',
    dataIndex: 'postType',
    align: 'center',
  },
  {
    title: '运单号',
    dataIndex: 'expressNo',
    align: 'center',	
  },
  {
    title: '快递状态',
    dataIndex: 'sendStatus',
    align: 'center',
  },
  {
    title: '路由信息',
    dataIndex: 'nodeDesc',
    align: 'center',
  },
  {
    title: '收件地址',
    dataIndex: 'receiverAddress',
    align: 'center',
  },
  {
    title: '收件电话',
    dataIndex: 'receiverPhone',
    align: 'center',
    render: (text, record) => {
      const { postType } = record;
      return postType === '寄送' ? text : '-';
    }
  },
  {
    title: '下单时间',
    dataIndex: 'orderDate',
    align: 'center',
    render: (text) => text ? moment(text).format('YYYY-MM-DD hh:mm:ss') : '',
  },
  {
    title: '签收时间',
    dataIndex: 'receivedDate',
    align: 'center',
  },
];

@connect(({ integralModel,valueAdd,loading }) => ({
	integralModel,
	valueAdd,
    loading: loading.effects,
}))
@Form.create()
export default class SearchIntegral extends React.Component {
	state = {
		isShowDetail: false, // 是否展示详情
    currentDetail: {}, // 当前记录的详情
    retryIndex: null,
  }

	componentDidMount() {
		const cCertfCde = getUrlParam(this.props.location.search).cCertfCde;
		if (cCertfCde) {
			this.props.dispatch({
				type:'integralModel/queryIntegralSearchInfo',
				payload: {
					cCertfCde,
				}
			})
		}
  }
  
  valueAddedColumns = [ // 增值服务详情
    {
      title: '序号',
      dataIndex: 'index',
      align: 'center',
      render: (text, record, index) => index + 1,
      width: 50,
      fixed: 'left',
    },
    {
      title: '服务类别',
      dataIndex: 'type',
      align: 'center',
      width: 70,
      fixed: 'left',
    },
    {
      title: '服务名称',
      dataIndex: 'serviceName',
      align: 'center',
      width: 100,
      fixed: 'left',
    },
    {
      title: '服务状态',
      dataIndex: 'status',
      align: 'center',
      width: 80,
      fixed: 'left',
    },
    {
      title: '服务次数',
      dataIndex: 'serviceCount',
      align: 'center',
      render: (text, record) => {
        const { serviceType } = record;
        if (parseInt(serviceType, 10) === 7  || parseInt(serviceType, 10) === 6 ) {
          return text;
        } else {
          return '-';
        }
      },
      width: 70,
    },
    {
      title: '保全日期',
      dataIndex: 'saveDate',
      align: 'center',
      width: 100,
    },
    {
      title: '首次赠送日期',
      dataIndex: 'sendDate',
      align: 'center',
      width: 100,
    },
    {
      title: '领用日期',
      dataIndex: 'receiveDate',
      align: 'center',
      width: 100,
    },
    {
      title: '失效日期',
      dataIndex: 'noUseDate',
      align: 'center',
      width: 100,
    },
    {
      title: '失效原因',
      dataIndex: 'invalidDesc',
      align: 'center',
      width: 100,
    },
    {
      title: '保单号',
      dataIndex: 'cPlyNo',
      align: 'center',
      width: 100,
    },
    {
      title: '产品名称',
      dataIndex: 'prpdName',
      align: 'center',
      width: 100,
    },
    {
      title: '款别',
      dataIndex: 'cTgtFld',
      align: 'center',
      width: 50,
      fixed: 'rigth',
    },
    {
      title: '原始订单号',
      dataIndex: 'transactionCode',
      align: 'center',
      width: 90,
      fixed: 'rigth',
    },
    {
      title: '订单状态',
      dataIndex: 'flag',
      align: 'center',
      render: (text, record) => {
        let describe = '';
        switch (record.flag) {
          case '1':
            describe = '有效';
            break;
          case '2':
            describe = '注销';
            break;
          case '3':
            describe = '退保';
            break;
          case '4':
            describe = '过期';
            break;
          default:
            describe = '';
        }
        return describe;
      },
      width: 70,
      fixed: 'rigth',
    },
    {
      title: '操作',
      align: 'center',
      render: (text, record, index) => {
        const { serviceNo, serviceName, status } = record; 
        return (
          <Fragment>
            <Popconfirm title="确定重发消息？" onText="重发" cancelText="取消" onConfirm={this.retryNote.bind(this, serviceNo, serviceName, index)}> 
              <Button size="small" loading={this.state.retryIndex === index ? this.props.loading['valueAdd/retryNote'] : false} type="primary" disabled={status === '已使用' || status === '可使用' ? false : true}>重发消息</Button> 
            </Popconfirm>
            <div style={{marginTop: '5px'}}>
              <Button size="small" type="primary" onClick={this.queryDetail.bind(this, record)}>查看详情</Button>
            </div>
          </Fragment>
        )
        
      },
      width: 80,
      fixed: 'rigth',
    },
  ];

	// 重发信息
	retryNote = (serviceNo, serviceName, index) => {
    const { cCertfCde, cCertfCls, cPlyNo } = this.state;
    this.setState({
      retryIndex: index,
    });
		this.props.dispatch({ 
			type: 'valueAdd/retryNote', 
			payload: {
				cCertfCde,
				cCertfCls,
				serviceNo,
				cPlyNo,
				serviceName,
			} 
		}).then((result) => {
			const { code, message } = result;
			if (code === 0) {
				Message.success(message || '重发成功');
			}
			if (code === 1) {
				Message.error(message);
			}
	  }); 
	};
	  
	// 查看详情
	queryDetail = (record) => {
	this.setState(() => ({
		isShowDetail: true,
		currentDetail: record,
	}));
	}

  //重置表单
  handleReset = () => {
      this.props.form.resetFields();
  }
  handleSearch = e => {
      //防止链接打开 URL
      if(e)e.preventDefault();
      const { dispatch, form } = this.props;
      //表单校验
      form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
          type:'integralModel/queryIntegralSearchInfo',
          payload:fieldsValue
      })
    });
	}

	// showMore = () => {
	// 	const addColumns = [
	// 		{
	// 			title: '服务评分',
	// 			dataIndex: 'serviceScore',
	// 			align: 'center',
	// 		},
	// 		{
	// 			title: '服务评价',
	// 			dataIndex: 'serviceComment',
	// 			align: 'center',
	// 		},
	// 		{
	// 			title: '快递类型',
	// 			dataIndex: 'postType',
	// 			align: 'center',
	// 		},
	// 		{
	// 			title: '快递单号',
	// 			dataIndex: 'expressNo',
	// 			align: 'center',
	// 		},
	// 		{
	// 			title: '快递状态',
	// 			dataIndex: 'sendStatus',
	// 			align: 'center',
	// 		},
	// 		{
	// 			title: '收件地址',
	// 			dataIndex: 'receiverAddress',
	// 			align: 'center',
	// 		},
	// 		{
	// 			title: '收件电话',
	// 			dataIndex: 'receiverPhone',
	// 			align: 'center',
	// 		},
	// 		// {
	// 		// 	title: '电子邮箱',
	// 		// 	dataIndex: 'email',
	// 		// 	align: 'center',
	// 		// },
	// 	];
	// 	const originAddColumns = [...this.state.valueAddedColumns];
	// 	this.setState({
	// 		valueAddedColumns: [
	// 			...originAddColumns,
	// 			...addColumns,
	// 		],
	// 	});
	// }
  renderAdvancedForm(){
      const { getFieldDecorator } = this.props.form;
      return(
          <Card bordered={false}>
              <Form onSubmit={this.handleSearch}>
                  <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                      <Col md={10} sm={24}>
                          {/* <FormItem
                              label="投保人证件类型"
                              {...formItemLayout}
                              style={{marginBottom:'0'}}
                          >
                              {getFieldDecorator('cCertfCls')
                              (
                                  <Select placeholder="请选择" >
                                      {
                                          cardTypeOptions.map((item)=>
                                              <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                                          )
                                      }
                                  </Select>
                              )}
                          </FormItem> */}
                          <FormItem
                            label="微信ID"
                            {...formItemLayout}
                            style={{marginBottom:'0'}}
                          >
                              {getFieldDecorator('openId')(
                                  <Input placeholder="请输入微信ID" />  
                              )}
                          </FormItem>
                      </Col>
                      <Col md={10} sm={24}>
                          <FormItem 
                              label="投保人证件号码"
                              style={{marginBottom:'0'}}
                              {...formItemLayout}
                          >
                              {getFieldDecorator('cCertfCde',{
								initialValue: getUrlParam(this.props.location.search).cCertfCde || ''
                              })(
                                  <Input placeholder="请输入" />  
                              )}
                          </FormItem>
                      </Col>
                      {/* <Col md={10} sm={24}>
                          <FormItem
                               label="投保人手机号码"
                               style={{marginBottom:'0'}}
                               {...formItemLayout}
                          >
                              {getFieldDecorator('cMobileNo')(
                                  <Input placeholder="请输入" />
                              )}
                          </FormItem>
                      </Col> */}
                  </Row> 
                  <Row>
                      <Col style={{textAlign: 'center'}}>
                          <Button
                            type="primary" 
                            htmlType="submit"
                            loading={this.props.loading['integralModel/queryIntegralSearchInfo']}
                          >
                              搜索
                          </Button>
                          <Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
                              重置
                          </Button>
                      </Col>
                  </Row>
              </Form>
          </Card>)
  }
  render() {
    const {integralSearchUserInfo, integralSearchListInfo, integralValueAddedListInfo} = this.props.integralModel || {};
    const { commentInfo, orderList, expressApplyList } = this.state.currentDetail;
    return (
      <Fragment>
        <PageHeaderLayout>
          <Card bordered={false}>
            {this.renderAdvancedForm()}
            {
              Object.keys(integralSearchUserInfo).length > 0 &&
              <div>
                <UserBaseInfoView datas={integralSearchUserInfo} />
                <UserInteralInfoView datas= {integralSearchUserInfo} />
              </div>
            }
            <Table
              title={() => '积分详情'}
              bordered
              columns={getIntegralListColumns()}
              loading={this.props.loading['integralModel/queryIntegralSearchInfo']}
              dataSource={integralSearchListInfo}
              size="small"
              rowKey={record => record.id}
              pagination={{
                pageSizeOptions: ['10','20','30','40','50'],
                showQuickJumper: true,
                showSizeChanger: true,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              }}
            />
            <Table
              // title={() => <div style={{ position: 'relative' }}><span>增值服务详情</span><Button type="primary" size="small" onClick={this.showMore} style={{ right: 0, position: 'absolute' }}>查看更多信息</Button></div>}
              title={() => '增值服务详情'}
              bordered
              columns={this.valueAddedColumns}
              loading={this.props.loading['integralModel/queryValueAddedSearchInfo']}
              dataSource={integralValueAddedListInfo}
              scroll={{x: 1400, y: 300}}
              size="small"
              // rowKey={record => record.receiveDate}
              rowKey={record => record.serviceNo}
              pagination={{
                pageSizeOptions: ['10','20','30','40','50'],
                showQuickJumper: true,
                showSizeChanger: true,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              }}
              style={{
                marginTop: '10px',
              }}
            />
          </Card>
              </PageHeaderLayout>
        <Modal
          maskClosable={false}
          title="查看详情"
          visible={this.state.isShowDetail}
          onOk={() => this.setState({ isShowDetail: false })}
          onCancel={() => this.setState({ isShowDetail: false })}
          width={800}
          >
          <Table
            columns={columnService}
            dataSource={commentInfo}
            bordered
            rowKey={item => item.transactionCode}
            pagination={false}
            size='small'
            title={() => '服务评价' }
          />
          <Table
            columns={columnOrder}
            dataSource={orderList}
            bordered
            rowKey={item => item.rechargeDate}
            pagination={false}
            size='small'
            title={() => '充值记录' }
            style={{marginTop: '10px'}}
          />
          <Table
            columns={columnExpress}
            dataSource={expressApplyList}
            bordered
            rowKey={item => item.expressNo}
            scroll={{x: 1400}}
            pagination={false}
            size='small'
            title={() => '物流信息' }
            style={{marginTop: '10px'}}
          />
        </Modal>
      </Fragment>
    );
  }
}