
import React from 'react';
import { Table, Input, InputNumber, Popconfirm, Form, Select, Divider } from 'snk-web';

const FormItem = Form.Item;
const dateAdd0 =(i)=>{
    return i<10 ? '0'+ String(i) : String(i);
 }
 //月份选项
 let monthOptions =[];
 for(let i =1;i<=12; i++){
     monthOptions.push(dateAdd0(i));
 }
 // 日选项
 let dayOptions =[];
 for(let i =1;i<=31; i++){
    dayOptions.push(dateAdd0(i));
 }
// 创建context
export const EditableContext = React.createContext();
// 创建行
const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
      <tr {...props} />
    </EditableContext.Provider>
  );
const EditableFormRow = Form.create()(EditableRow);

// 单元格
class EditableCell extends React.Component {
    getInput = (form) => {
      const { getFieldDecorator } = form;
      const {
        dataIndex,
        record,
      } = this.props;
    //   console.log('props',this.props);
      if (this.props.dataIndex === 'prodRate') {
        return (
            <FormItem style={{ margin: 0 }}>
                {
                    getFieldDecorator(dataIndex, {
                        rules: [{
                            required: true,
                            message: "请输入产品系数",
                        }],
                        initialValue: record[dataIndex],
                    })(<Input />)
                }
            </FormItem>)
      }
      if (this.props.dataIndex === 'validYear') {
          
        return (
            <FormItem style={{ margin: 0 }}>
                第
                {
                    getFieldDecorator(dataIndex, {
                        rules: [{
                            required: true,
                            message: "请输入公元年",
                        }],
                        initialValue:  record[dataIndex],
                    })(<InputNumber min={0} style={{width: '50px',margin:'0 5px'}} />)
                }
                公元年
            </FormItem>)
      }
      if (this.props.dataIndex === 'validDate') {
        const  dateArr = record[dataIndex].split('-');
        let monthinitalValue = '';
        let dayinitalValue = '';
        if(record[dataIndex]){
            const bdateArr = record[dataIndex].split('-');
            monthinitalValue = dateArr[0];
            dayinitalValue = dateArr[1];
        }
        return (
            <div>
            <FormItem style={{ margin: 0 }}>
                {
                    getFieldDecorator("validMonth", {
                        rules: [{
                            required: true,
                            message: "请输入有效截止日期",
                        }],
                        initialValue: monthinitalValue,
                    })(
                        <Select placeholder="请选择" >
                        {
                            monthOptions.map((item)=>
                                <Select.Option key={item} value={item}>{item}</Select.Option>
                            )
                        }
                        </Select>
                    )
                }
                月
            </FormItem>
            <FormItem style={{ margin: 0 }}>
                {
                    getFieldDecorator("validDay", {
                        rules: [{
                            required: true,
                            message: "请输入有效截止日期",
                        }],
                        initialValue: dayinitalValue,
                    })(
                        <Select placeholder="请选择" >
                        {
                            dayOptions.map((item)=>
                                <Select.Option key={item} value={item}>{item}</Select.Option>
                            )
                        }
                        </Select>
                    )
                }
                日
            </FormItem>
            </div>
            )
      }
    };

    ShowFormCell =()=>{
        const {
            dataIndex,
            record = {}
          } = this.props;
        if(dataIndex === 'integralCofe'){
            return record.validYear && Number(record.integralCofe).toFixed(3);
        }
        if(dataIndex === 'validYear'){
            return `第${record.validYear}公元年`;
        }
        return this.props.children;
    }
  
    render() {
        const {
            editing,
            dataIndex,
            title,
            record,
            index,
            ...restProps
          } = this.props;
      return (
        <EditableContext.Consumer>
          {(form) => {
            return (
              <td {...restProps}>
                {editing ?  this.getInput(form) : this.ShowFormCell()}
              </td>
            );
          }}
        </EditableContext.Consumer>
      );
    }
  }

export  const components = {
    body: {
      row: EditableFormRow,
      cell: EditableCell,
    },
};