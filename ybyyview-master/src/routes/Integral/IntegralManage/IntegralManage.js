
import React from 'react';
import { connect } from 'dva';

import { 
    Table, 
    Row,
    Col,
    Card,
    Form,
    Input,
    Select,
    Badge,
    Button,
    Icon,
    Dropdown,
    Modal,
    Tooltip,
    message,
    Divider} from 'snk-web';
import ChannelAndProduction from 'components/ChannelAndProduction';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import FormSearch from './FormSearch';
import {components} from './EditableCell';
import { getIntegralListColumns } from './integralManageConfig';
import { isMoment } from 'moment';

@connect(({ integralModel, loading }) => ({
    integralModel,
    loading: loading.models.integralModel,
}))
@Form.create()
export default class IntegralManage extends React.Component {
    constructor(props){
        super(props);
        this.state = { editingId: '' };
        this.fieldsValue={};
    }
    onHandleSearch=(fieldsValue)=>{
        this.fieldsValue= fieldsValue;
        this.handleTablePage();
    }
    handleTablePage=(pageSizeOptions = {})=>{
        const { current = 1, pageSize = 10 } = pageSizeOptions;
        const formParams = this.fieldsValue;
        this.props.dispatch({
            type:'integralModel/queryIntegralManageList',
            payload:{pageNum:current, pageSize, ...formParams}
        })
    }

  // 是否可编辑
  isEditing = (record) => {
    return record.id === this.state.editingId;
  };
  // 编辑
  edit=(id)=> {
    this.setState({ editingId: id });
  }
  // 保存
  save=(form, id)=> {
    form.validateFields((error, row ={}) => {
        if (error) {
            return;
        }
        const {prodRate, validYear, validMonth,validDay} = row;
        const validDate= `${validMonth}-${validDay}`;
        const updateRowdata={prodRate,validYear,validDate }
        this.props.dispatch({
            type:'integralModel/updateIntegralManageRow',
            payload:{updateRowdata, id}
        }).then(()=>{
            this.setState({ editingId: '' });
        }) 
      });
  }
  // 取消
  cancel = () => {
    this.setState({ editingId: '' });
  };

  // table 头
  columns = ()=>{
    const isEditingFunc = this.isEditing;
    const editFunc = this.edit;
    const saveFunc = this.save;
    const cancelFunc = this.cancel;
    return getIntegralListColumns({isEditingFunc, editFunc, saveFunc, cancelFunc}).map((col) => {
        if (!col.editable) {
          return col;
        }
        return {
          ...col,
          onCell: record => ({
            record,
            dataIndex: col.dataIndex,
            title: col.title,
            editing: this.isEditing(record),
          }),
        };
      });
  }
    render() {
        const {integralManageListInfo= [], manageTotal } = this.props.integralModel || {};
        return (
            <PageHeaderLayout>
                <Card bordered={false}>
                    <FormSearch
                        onSearch={this.onHandleSearch}
                    />
                    <Table
                        bordered
                        components={components}
                        columns={this.columns()}
                        dataSource={integralManageListInfo}
                        loading={this.props.loading}
                        size="small"
                        rowKey={record => record.id}
                        onChange={this.handleTablePage}
                        pagination={{
                            pageSizeOptions: ['5','10','20','30','40','50'],
                            showQuickJumper: true,
                            showSizeChanger: true,
                            showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                            total:manageTotal,
                        }}
                    />
                </Card>
            </PageHeaderLayout>
        );
    }
}