import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Button,
} from 'snk-web';
import ChannelAndProduction from 'components/ChannelAndProduction';

@connect(({loading }) => ({
  loading:loading.effects,
}))
@Form.create()
export default class FormSearch extends PureComponent {
  handleSearch= (e) => {
    //防止链接打开 URL
    if(e)e.preventDefault();
    //表单校验
    this.props.form.validateFields((err, fieldsValue) => {
        if (err) return;
        if(this.props.onSearch)this.props.onSearch(fieldsValue);
        
    });
  }
  handleReset=()=>{
    this.props.form.resetFields();
  }
  render() {
    return (
      <Form 
        className='custom-form-wrapper custom-search-form-wrapper'
        onSubmit={this.handleSearch}
        layout="inline">
        <ChannelAndProduction 
          channelFieldKey="extEnterpCode"
          productionFieldKey="prodNo"
          ChannelRule={{
              rules: [{
                  required: true, message: '请选择业务渠道!',
              }]
          }}
          {...this.props}
          allowClear
        />
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
          <Button loading={this.props.loading['integralModel/queryIntegralManageList']} type="primary" htmlType="submit">
              查询
          </Button>
          <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
              重置
          </Button>
        </div>
      </Form>
    );
  }
}
