import {  Popconfirm } from 'snk-web';
import {EditableContext} from './EditableCell';


// 业务渠道
const extEnterpDesc = [
    {name: '招商银行', code: '95555'},
    {name: '民生银行', code: '95568'},
    {name: '建设银行', code: '95533'},
    {name: '盛大', code: '80001'},
    {name: '江苏邮政', code: '95580'},
    {name: '深发展', code: '95501'},
    {name: '深圳陌call', code: '10010'},
    {name: '南昌电话中心',code:'11004'},
    {name: '成都自建', code: '11003'},
    {name: '分公司推广', code: '95556'},
    {name: '微信出单', code: '11002'},
    {name: '汇金保联', code: '11001'},
    {name: '中信银行', code: '95558'},
  ];
// 业务渠道匹配
const extEnterpDescFunc =(extEnterpCode)=>{
    const result = extEnterpDesc.find((item)=>item.code === extEnterpCode);
    return result && result.name;
}

// 积分查询--table 头
export const getIntegralListColumns =({isEditingFunc, editFunc, saveFunc, cancelFunc})=>{
  return [
      {
        title: '序号',
        dataIndex: 'orderId',
        key: 'orderId',
        width:'80px',
        align: 'center',
        render: (text, record, index) => index+1,
        editable: false,
    },
    {
        title: '业务渠道',
        dataIndex: 'extEnterpCode',
        key: 'extEnterpCode',
        width:'150px',
        align: 'center',
        render: (text, record, index) => extEnterpDescFunc(text),
        editable: false,
    },
    {
        title: '产品代码',
        dataIndex: 'prodNo',
        key: 'prodNo',
        width:'150px',
        align: 'center',
        editable: false,
    },
    {
        title: '产品名称',
        dataIndex: 'prodName',
        key: 'prodName',
        width:'120px',
        align: 'center',
        editable: false,
    },
    {
        title: '产品系数',
        dataIndex: 'prodRate',
        key: 'prodRate',
        width:'120px',
        align: 'center',
        editable: true,
    },
    {
        title: '有效期截止年份',
        dataIndex: 'validYear',
        key: 'validYear',
        width:'150px',
        align: 'center',
        editable: true,
    },
    {
        title: '有效期截止日期',
        dataIndex: 'validDate',
        key: 'validDate',
        width:'120px',
        align: 'center',
        editable: true,
    },
    {
        title: '操作',
        dataIndex: 'operation',
        key: 'operation',
        width:'120px',
        align: 'center',
        editable: false,
        render: (text, record) => {
            const editable = isEditingFunc(record);
            return (
              <div>
                {editable ? (
                  <span>
                    <EditableContext.Consumer>
                      {form => (
                        <Popconfirm
                            title="确定修改此行信息吗？"
                            onConfirm={() => saveFunc(form, record.id)}
                            onCancel={() => cancelFunc(record.id)}
                        >
                            <a  style={{ marginRight: 8 }}>保存</a>
                        </Popconfirm>
                      )}
                    </EditableContext.Consumer>
                    <a
                        href="javascript:;"
                        onClick={() => cancelFunc(record.id)}
                    >
                      取消
                    </a>
                  </span>
                ) : (
                  <a onClick={() => editFunc(record.id)}>编辑</a>
                )}
              </div>
            );
          },
    },
    {
        title: '更新日期',
        dataIndex: 'cTime',
        key: 'cTime',
        width:'120px',
        align: 'center',
        editable: false,
    },
    {
        title: '操作人',
        dataIndex: 'creator',
        key: 'creator',
        width:'120px',
        align: 'center',
        editable: false,
    }
  ]
}

