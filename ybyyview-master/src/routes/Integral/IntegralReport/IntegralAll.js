import React from 'react';
import { connect } from 'dva';

import { 
    Table, 
    Row,
    Col,
    Card,
    Form,
    Input,
    Select,
    DatePicker,
    Button} from 'snk-web';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import {formItemLayout, yearOptionsFunc, getIntegralAllColumns} from './IntegralReportConfig'

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
@connect(({ integralModel,loading }) => ({
    integralModel,
    loading: loading.effects,
}))
@Form.create()
export default class IntegralAll extends React.Component { 
    state={
        typeId: ''
    }
    //重置表单
    handleReset = () => {
        this.props.form.resetFields();
    }
    handleSearch = e => {
        //防止链接打开 URL
        if(e)e.preventDefault();
        const { dispatch, form } = this.props;
        //表单校验
        form.validateFields((err, fieldsValue) => {
            if (err) return;
            const {type, startTime} = fieldsValue;
            let params = {...fieldsValue};
            if(type === '0' ) {
                const endTime = startTime;
                params['endTime'] = startTime;
            }
            // console.log('params', params)
            this.searchParams = params;
            dispatch({
                type:'integralModel/queryIntergralAllList',
                payload:params,
            }).then(()=>{
                this.setState({
                    typeId:type,
                })
            });
        });
    }
    downloadList=()=>{
        this.props.dispatch({
            type:'integralModel/integralAllDownList',
            payload: this.searchParams,
        });
    }
    renderAdvancedForm(){
        const { getFieldDecorator } = this.props.form;
        let currentYear = new Date().getFullYear();
        return(
            <Card bordered={false}>
                <Form onSubmit={this.handleSearch}>
                    <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                        <Col md={10} sm={24}>
                                <FormItem
                                    label="数值显示"
                                    {...formItemLayout}
                                    style={{marginBottom:'0'}}
                                >
                                    {getFieldDecorator('type',{
                                        rules: [{
                                            required: true, message: '请选择数值显示!',
                                        }],
                                    })
                                    (
                                        <Select placeholder="请选择" >
                                        <Select.Option value="0">按月</Select.Option>
                                        <Select.Option value="1">按年</Select.Option>
                                        </Select>
                                    )}
                                </FormItem>
                            </Col>
                            {
                                this.props.form.getFieldValue('type') === '0' &&
                                <Col md={10} sm={24}>
                                    <FormItem
                                        label="积分生效日期"
                                        {...formItemLayout}
                                        style={{marginBottom:'0'}}
                                    >
                                        {getFieldDecorator('startTime',{
                                            initialValue: currentYear,
                                        })
                                        (
                                            <Select style={{ width: '120px' }} placeholder="请选择" >
                                                 {
                                                    yearOptionsFunc().map((item)=>
                                                    <Select.Option key={item} value={item}>{item}</Select.Option>
                                                    )
                                                 }
                                            </Select>
                                        )}
                                    </FormItem>
                                </Col>
                            }
                            {
                                this.props.form.getFieldValue('type') === '1' &&
                                <Col md={10} sm={24}>
                                    <FormItem
                                        label="积分生效日期"
                                        {...formItemLayout}
                                        style={{marginBottom:'0'}}
                                    >
                                        <Col span={11}>
                                            <FormItem >
                                                {getFieldDecorator('startTime',{
                                                    rules: [{
                                                        required: true, message: '请选择开始日期',
                                                    }],
                                                    initialValue: currentYear,
                                                })
                                                (
                                                <Select style={{ width: '100%' }} placeholder="请选择" >
                                                    {
                                                        yearOptionsFunc().map((item)=>
                                                        <Select.Option key={item} value={item}>{item}</Select.Option>
                                                        )
                                                    }
                                                </Select>
                                                )} 
                                            </FormItem>
                                        </Col>
                                        <Col span={2}>
                                            <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                            -
                                            </span>
                                        </Col>
                                        <Col span={11}>
                                            <FormItem >
                                                {getFieldDecorator('endTime',{
                                                    rules: [{
                                                        required: true, message: '请选择结束日期',
                                                    }],
                                                    initialValue: currentYear,
                                                })
                                                (
                                                    <Select style={{ width: '100%' }} placeholder="请选择" >
                                                        {
                                                            yearOptionsFunc().map((item)=>
                                                            <Select.Option key={item} value={item}>{item}</Select.Option>
                                                            )
                                                        }
                                                    </Select>
                                                )}
                                            </FormItem >
                                        </Col>
                                    </FormItem>
                                </Col>
                            }
                            <Col md={10} sm={24}>
                                <FormItem
                                    label="积分来源"
                                    {...formItemLayout}
                                    style={{marginBottom:'0'}}
                                >
                                    {getFieldDecorator('source',{
                                        rules: [{
                                            required: true, message: '请选择积分来源',
                                        }],
                                        initialValue: '0',
                                    })
                                    (
                                        <Select placeholder="请选择" >
                                            <Select.Option value="0">全部</Select.Option>
                                            <Select.Option value="1">消费</Select.Option>
                                            <Select.Option value="2">赠送</Select.Option>
                                        </Select>
                                    )}
                                </FormItem>
                            </Col>
                        </Row> 
                        <Row>
                            <Col style={{textAlign: 'center'}}>
                                <Button
                                     type="primary"
                                     htmlType="submit"
                                     disabled={this.props.loading['integralModel/queryIntergralAllList']}
                                     loading={this.props.loading['integralModel/queryIntergralAllList']}
                                >
                                    搜索
                                </Button>
                                <Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
                                    重置
                                </Button>
                            </Col>
                        </Row>
                </Form>
            </Card>)
    }
    render() {
        const {intergralAllListInfo = []} = this.props.integralModel || {};
        return (
            <PageHeaderLayout>
                <Card bordered={false}>
                    {this.renderAdvancedForm()}
                    <Table
                        bordered
                        columns={getIntegralAllColumns(this.state.typeId)}
                        loading={this.props.loading['integralModel/queryIntergralAllList']}
                        dataSource={intergralAllListInfo}
                        size="small"
                        rowKey={record => record.title}
                        pagination = {false}
                        scroll={{ y: 300 }}
                    />
                    {
                        intergralAllListInfo.length > 0 &&
                        <div align='right' style={{margin: '15px 20px'}}>
                            <Button  type='primary' icon='download' onClick={this.downloadList}>导出整体积分</Button>
                        </div>
                    }
                </Card>
            </PageHeaderLayout>
        );
    }
}