import React from 'react';
import { connect } from 'dva';
import { 
  Row,
  Col,
  Card,
  Form,
  Select,
  Button,
  DatePicker,
  Table,
} from 'snk-web';

import PageHeaderLayout from '../../../../layouts/PageHeaderLayout';
import { formItemLayout,yearOptionsFunc, customerAllColumns,
} from './CustomerAllConfig';

const FormItem = Form.Item;

@connect(({ integralModel,loading }) => ({
  integralModel,
  loading: loading.effects,
}))
@Form.create()
export default class RedPacketSearch extends React.Component {
  constructor(props) {
      super(props);
      this.fieldsValue={};
  }

  handleSearch = e => {
    //防止链接打开 URL
    if(e)e.preventDefault();
    const { dispatch, form } = this.props;
    //表单校验
    form.validateFields((err, fieldsValue) => {
        if (err) return;
        const { type, startTime } = fieldsValue;
        this.fieldsValue={
            ...fieldsValue,
        };
        if(type === '0') this.fieldsValue['endTime'] = startTime;
        this.handleTablePage();
    });
  }
  handleTablePage=()=>{
      const formParams = this.fieldsValue;
      this.props.dispatch({
          type:'integralModel/querycustomerAllSearchList',
          payload:{...formParams}
      })
  }

     //重置表单
  handleReset = () => {
    this.props.form.resetFields();
  }

  onDownloadCustomerAllList=()=>{
    const formParams = this.fieldsValue;
    const params = {
        ...formParams,
        isUser: '1'
      }
      this.props.dispatch({
        type:'integralModel/downloadCustomerAllList',
        payload:params,
    });
  }
  onDownloadCustomerAll=()=>{
      const formParams = this.fieldsValue;
      const params = {
        ...formParams,
        isUser: null,
      }
      this.props.dispatch({
        type:'integralModel/downloadCustomerAllList',
        payload: params,
    });
  }
  renderAdvancedForm(){
    const { getFieldDecorator } = this.props.form;
    const {effects = {}} = this.props.loading;
    let currentYear = new Date().getFullYear();
    return(
        <Card bordered={false}>
            <Form onSubmit={this.handleSearch}>
                <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                    <Col md={10} sm={24}>
                        <FormItem
                              label="客户类型"
                              style={{marginBottom:'0'}}
                              {...formItemLayout}
                        >
                              {getFieldDecorator('applicantType',{
                                  initialValue: '0'
                              })(
                                <Select 
                                    allowClear
                                    placeholder='请选择'
                                >
                                    <Select.Option value='0'>全部</Select.Option>
                                    <Select.Option value='1'>活跃客户</Select.Option>
                                    <Select.Option value='2'>脱保客户</Select.Option>
                                </Select>
                                )}
                        </FormItem>
                    </Col>
                    <Col md={10} sm={24}>
                        <FormItem
                            label="日期"
                            {...formItemLayout}
                            style={{marginBottom:'0'}}
                        >
                            <Col span={11}>
                                <FormItem >
                                    {getFieldDecorator('startTime',{
                                        rules: [{
                                            required: true, message: '请选择开始日期',
                                        }],
                                        initialValue: currentYear,
                                    })
                                    (
                                    <Select style={{ width: '100%' }} placeholder="请选择" >
                                        {
                                            yearOptionsFunc().map((item)=>
                                            <Select.Option key={item} value={item}>{item}</Select.Option>
                                            )
                                        }
                                    </Select>
                                    )} 
                                </FormItem>
                            </Col>
                            <Col span={2}>
                                <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                -
                                </span>
                            </Col>
                            <Col span={11}>
                                <FormItem >
                                    {getFieldDecorator('endTime',{
                                        rules: [{
                                            required: true, message: '请选择结束日期',
                                        }],
                                        initialValue: currentYear,
                                    })
                                    (
                                        <Select style={{ width: '100%' }} placeholder="请选择" >
                                            {
                                                yearOptionsFunc().map((item)=>
                                                <Select.Option key={item} value={item}>{item}</Select.Option>
                                                )
                                            }
                                        </Select>
                                    )}
                                </FormItem >
                            </Col>
                        </FormItem>
                    </Col>
                </Row>
                <Row>
                    <Col style={{textAlign: 'center'}}>
                        <Button
                              type="primary"
                              htmlType="submit"
                              disabled={this.props.loading['integralModel/querycustomerAllSearchList']}
                              loading={this.props.loading['integralModel/querycustomerAllSearchList']}
                        >
                            搜索
                        </Button>
                        <Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
        </Card>)
  }
  showHeader=(customerAllList)=>()=>{
      if(customerAllList && !customerAllList.length) return null;
      return(
          <React.Fragment>
              <Button 
                type="primary"
                icon="download"
                disabled={this.props.loading['integralModel/downloadCustomerAllList']}
                loading={this.props.loading['integralModel/downloadCustomerAllList']}
                onClick={this.onDownloadCustomerAll}
                style={{marginRight: '10px'}}
               >当前导出</Button>
               <Button 
                type="primary"
                icon="download"
                disabled={this.props.loading['integralModel/downloadCustomerAllList']}
                loading={this.props.loading['integralModel/downloadCustomerAllList']}
                onClick={this.onDownloadCustomerAllList}
               >客户清单导出</Button>
          </React.Fragment>
      )

  }
  render() {
    const {customerAllList = [], customerAllListTotal } = this.props.integralModel || {};
    const {applicantType = '0'} = this.props.form.getFieldsValue(['applicantType']);
    return(
      <PageHeaderLayout>
        <Card bordered={false}>
            {this.renderAdvancedForm()}
            <Table
                bordered
                columns={customerAllColumns(applicantType)}
                dataSource={customerAllList}
                title={this.showHeader(customerAllList)}
                loading={this.props.loading['integralModel/querycustomerAllSearchList']}
                size="small"
                rowKey={record => record.date}
                pagination={false}
            />
        </Card>
    </PageHeaderLayout>
    )
  }
}