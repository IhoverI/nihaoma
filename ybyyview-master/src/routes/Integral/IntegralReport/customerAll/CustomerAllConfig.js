//表单布局
export const formItemLayout = {
  labelCol: {
    xs: { span: 12 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 12 },
    sm: { span: 16 },
  },
};
// 年份选择
export const yearOptionsFunc =()=>{
  let result = [];
  let currentYear = new Date().getFullYear();
  for(let i = currentYear-20;i <= currentYear+20; i++){
      result.push(i);
   }
   return result;
}

// 红包查询--table 头
export const customerAllColumns =()=>{

  return [
    {
      title: '日期',
      dataIndex: 'date',
      width:'80px',
      align: 'center',
    },
    {
      title: '客户总数',
      dataIndex: 'customerTotal',
      width:'80px',
      align: 'center',
    },
    {
      title: '活跃客户总数',
      dataIndex: 'activeTotal',
      width:'80px',
      align: 'center',
    },
    {
      title: '脱保客户总数',
      dataIndex: 'deinsuranceTotal',
      width:'80px',
      align: 'center',
    },
    {
      title: '男性总数',
      dataIndex: 'maleTotal',
      width:'80px',
      align: 'center',
    },
    {
      title: '男性年龄(18-24岁)',
      dataIndex: 'maleSumOne',
      width:'80px',
      align: 'center',
    },
    {
      title: '男性年龄(25-29岁)',
      dataIndex: 'maleSumTwo',
      width:'80px',
      align: 'center',
    },
    {
      title: '男性年龄(30-39岁)',
      dataIndex: 'maleSumThree',
      width:'80px',
      align: 'center',
    },
    {
      title: '男性年龄(40-49岁)',
      dataIndex: 'maleSumFour',
      width:'80px',
      align: 'center',
    },
    {
      title: '男性年龄(50-59岁)',
      dataIndex: 'maleSumFive',
      width:'80px',
      align: 'center',
    },
    {
      title: '男性年龄(60-70岁)',
      dataIndex: 'maleSumSix',
      width:'80px',
      align: 'center',
    },
    {
      title: '女性总数',
      dataIndex: 'femaleTotal',
      width:'80px',
      align: 'center',
    },
    {
      title: '女性年龄(18-24岁)',
      dataIndex: 'femaleSumOne',
      width:'80px',
      align: 'center',
    },
    {
      title: '女性年龄(25-29岁)',
      dataIndex: 'femaleSumTwo',
      width:'80px',
      align: 'center',
    },
    {
      title: '女性年龄(30-39岁)',
      dataIndex: 'female3',
      width:'80px',
      align: 'center',
    },
    {
      title: '女性年龄(40-49岁)',
      dataIndex: 'femaleSumThree',
      width:'80px',
      align: 'center',
    },
    {
      title: '女性年龄(50-59岁)',
      dataIndex: 'femaleSumFive',
      width:'80px',
      align: 'center',
    },
    {
      title: '女性年龄(60-70岁)',
      dataIndex: 'femaleSumSix',
      width:'80px',
      align: 'center',
    },
  ]
}