//表单布局
export const formItemLayout = {
    labelCol: {
      xs: { span: 12 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 12 },
      sm: { span: 16 },
    },
};
// 年份选择
export const yearOptionsFunc =()=>{
    let result = [];
    let currentYear = new Date().getFullYear();
    for(let i = currentYear-20;i <= currentYear+20; i++){
        result.push(i);
     }
     return result;
}

// 积分整体--table 头
export const getIntegralAllColumns =(type)=>{
    let typeValue = '月份/年份';
    if(type === '0') typeValue = '月份';
    if(type === '1') typeValue = '年份';
    return [
        {
          title: typeValue,
          dataIndex: 'title',
          width:'80px',
          align: 'center',
      },
      {
        title: '总消费积分',
        dataIndex: 'totalConsumeIntegral',
        width:'120px',
        align: 'center'
     },
      {
          title: '赠送积分',
          dataIndex: 'presentIntegral',
          width:'120px',
          align: 'center'
      },
      {
          title: '可兑换积分',
          dataIndex: 'availableIntegral',
          width:'150px',
          align: 'center'
      },
      {
          title: '冻结积分',
          dataIndex: 'frozenIntegral',
          width:'120px',
          align: 'center'
      },
      {
          title: '已兑换积分',
          dataIndex: 'exchangedIntegral',
          width:'120px',
          align: 'center'
      },
      {
          title: '已过期积分',
          dataIndex: 'expiredIntegral',
          width:'120px',
          align: 'center'
      },
    ]
  }

  // 积分客户留存率--table 头
export const getIntegralCustomerColumns =(type)=>{
    let typeValue = '月份/年份';
    if(type === '0') typeValue = '月份';
    if(type === '1') typeValue = '年份';
    return [
        {
          title: typeValue, // 根据传入参数动态显示
          dataIndex: 'title',
          width:'150px',
          align: 'center',
      },
      {
          title: '操作过积分兑换的客户数',
          dataIndex: 'cusNumber',
          width:'120px',
          align: 'center'
      },
      {
          title: '兑换积分总值',
          dataIndex: 'integralValue',
          width:'150px',
          align: 'center'
      },
      {
          title: '发生兑换的人数中截止查询日期仍为活跃的客户人数',
          dataIndex: 'retentionCusNumber',
          width:'120px',
          align: 'center'
      },
      {
          title: '客户留存率',
          dataIndex: 'cusRententionRate',
          width:'120px',
          align: 'center'
      },
    ]
  }