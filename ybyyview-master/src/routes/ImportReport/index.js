import React, { PureComponent, Fragment, Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Card,
  Icon,
  Button,
  message,
  Upload,
  Progress,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './index.less';
import {getChannelNameByValue,getProductNameByValue} from '../../utils/utils'

const Dragger = Upload.Dragger;


@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))

export default class ImportReport extends PureComponent {
  state = {
    progress: 0,
    fileList: [],
    uploading: false,
    disPrgs: 'none',
  };

  handleUpload = () => {
    const { fileList } = this.state;
    setTimeout(() => {
      this.setState({progress: 35});
    }, 300);
    setTimeout(() => {
      this.setState({progress: 60});
    }, 500);
    const param = this.state.fileList[0];
    this.props.dispatch({
      type: 'rule/uploadSukeyBackFile',
      payload: {param},
    }).then(()=>{
      setTimeout(() => {
        this.setState({progress: 100});
      }, 800);
    }).catch((e)=>{
      this.setState({progress: 0})
      message.error(e.message || '网络异常');
    });
    this.setState({
      uploading: false,
    });
  }

  handleDownload = () => {
    this.props.dispatch({
      type: 'rule/downloadSukeyBackFile',
      payload: {},
    }).then(()=>{
    }).catch((e)=>{
      message.error(e.message || '网络异常');
    });
  }

  render() {
    const { uploading } = this.state;

    const param = {
      name: 'file',
      onRemove: (file) => {
        this.setState(({ fileList }) => {
          return {
            fileList: [],
            progress: 0,
          };
        });
      },
      beforeUpload: (file) => {
        this.setState({
          fileList: [file],
          progress: 0,
          disPrgs: ''
        });
        return false;
      },
      fileList: this.state.fileList,
    };


    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div style ={{marginLeft: 10}}>
            <Dragger {...param}>
            <Button>
            <Icon type="upload"/>拖拽/点击导入excel
            </Button>
            </Dragger>
            <div style={{ overflow: 'hidden',textAlign:'center',marginTop:50,paddingBottom:20  }}>
            <Button
                type="primary"
                onClick={this.handleDownload}
                loading={this.props.loading}
              >
                下载模版
              </Button>
            <Button
                type="primary"
                onClick={this.handleUpload}
                disabled={this.state.fileList.length === 0}
                loading={this.props.loading}
                style ={{marginLeft: 20}}
              >
                导出excel
              </Button>
              <Progress type="circle"
              style ={{display:this.state.disPrgs,paddingLeft:20}}
              percent={this.state.progress} width={30} />
            </div>
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
