import React, { PureComponent, Children } from 'react';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import PersonHistoryModal from './PersonHistoryModal';
import { getPersonListColumns2 } from './PersonColumns';
import { connect } from 'dva';
import { Card, Form, Row, Col, DatePicker, Select, Button, Table, Input, Upload, Icon, Modal, message } from 'snk-web'
import { routerRedux } from 'dva/router';

const { RangePicker } = DatePicker;
const { Option } = Select;
const formItemLayout = {
  labelCol: {
    sm: { span: 4 },
  },
  wrapperCol: {
    sm: { span: 20 },
  },
};

@connect(({ personCustomerModels, loading }) => ({
  personModels:personCustomerModels,
  loading: loading.models.personCustomerModels,
}))
@Form.create()
export default class SearchPerson extends PureComponent {

  state = {
    historyVisible: false,
    record: {},
    pageNum: 1,
    pageSize: 10,
    groupChildren:[],
    teamChildren:[]
  }

  componentWillMount(){
    // const {dispatch,form} = this.props;
    //  // 初始化团队列表、组别列表
    // dispatch({
    //   type: 'personCustomerModels/checkIfShowGroupForm',
    //   payload: null,
    // }).then(dispatch({
    //   type: 'personCustomerModels/selectGroupName',
    //   payload: {data: {}},
    // }).then(()=>{
    //   let {groupNames,groupFormVisibleData} = this.props.personModels;
    //   //团队下拉菜单
    //    let groupChildren = groupNames.data.map(e=><Option key={e.id} value={e.id}>{e.groupName}</Option>)
    //     this.setState({groupChildren})
    //   if(groupFormVisibleData&&groupFormVisibleData.data&&groupFormVisibleData.data.groupId){
    //     this.groupNameSelect(groupFormVisibleData.data.groupId)
    //   }
    // }))
  }
 

  formItemLayout = {
    labelCol: {
      sm: { span: 4 },
    },
    wrapperCol: {
      sm: { span: 20 },
    },
  };

  // 跳转到编辑页面
  editPerson = (personId) => {
    this.props.dispatch(routerRedux.push({pathname: '/person/editCustomerPerson', personId: personId}))
  }

  // 跳转到增加页面
//   addPerson = () => {
//     console.log('跳转')
//     this.props.dispatch(routerRedux.push({pathname: '/person/addPerson'}))
//   }

  //模板下载
//   downloadModal=()=>{
//     const {dispatch} = this.props
//     dispatch({
//       type: 'personCustomerModels/downloadModal',
//       payload: null,
//     });
//   }
//   downloadPerson =()=>{
//     console.log('downloadPerson called..');
//     let { form, dispatch } = this.props;
//     form.validateFields((err, fieldsValue) => {
//       if (err) {
//         return;
//       }
//       fieldsValue.pageNum = 1;
//       fieldsValue.pageSize = 10000;
//       let {entryDate,onlineDate,leaveDate} = fieldsValue;
//       if(entryDate){
//         fieldsValue.entryDate=[entryDate[0].format('YYYY-MM-DD'),entryDate[1].format('YYYY-MM-DD')]
//       }
//       if(onlineDate){
//         fieldsValue.onlineDate=[onlineDate[0].format('YYYY-MM-DD'),onlineDate[1].format('YYYY-MM-DD')]
//       }
//       if(leaveDate){
//         fieldsValue.leaveDate=[leaveDate[0].format('YYYY-MM-DD'),leaveDate[1].format('YYYY-MM-DD')]
//       }
//       let reqBody = {
//         data: fieldsValue,
//       }
//       console.log(fieldsValue);
//       dispatch({
//         type: 'personCustomerModels/downloadPerson',
//         payload: reqBody,
//       });
//     });    
//   }

  // 显示员工信息修改记录-弹框
//   showPersonHistory = (record) => {
//     this.setState({
//       historyVisible: true,
//       record: record,
//     });
//   }

  // 关闭员工修改历史弹框
//   handleOkHistory = (e) => {
//     this.setState({
//       historyVisible: false,
//     });
//   }

  // 关闭员工修改历史弹框
//   handleCancelHistory = (e) => {
//     this.setState({
//       historyVisible: false,
//     });
//   }

  // 点击搜索
  queryPersonnelMain = () => {
    this.props.form.validateFields({force:true},(err, fieldsValue)=>{
      if (err) return;
      this.setState({
        pageNum: 1,
        pageSize: 10,
      })
      this.searchData(1, 10);
    })
  }

  // 搜索员工信息
  searchData = (pageNum, pageSize) => {
    console.log('searchData called..');
    let { form, dispatch } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      fieldsValue.pageNum = pageNum;
      fieldsValue.pageSize = pageSize;
      let {entryDate,onlineDate,leaveDate} = fieldsValue;
      if(entryDate){
        fieldsValue.entryDate=[entryDate[0].format('YYYY-MM-DD'),entryDate[1].format('YYYY-MM-DD')]
      }
      if(onlineDate){
        fieldsValue.onlineDate=[onlineDate[0].format('YYYY-MM-DD'),onlineDate[1].format('YYYY-MM-DD')]
      }
      if(leaveDate){
        fieldsValue.leaveDate=[leaveDate[0].format('YYYY-MM-DD'),leaveDate[1].format('YYYY-MM-DD')]
      }
      let reqBody = {
        data: fieldsValue,
      }
      console.log(fieldsValue);
      dispatch({
        type: 'personCustomerModels/searchPerson',
        payload: reqBody,
      });
    });
  }

  // 分页
  handleTablePage =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current, pagination.pageSize);
    });
  }

  // 根据团队查询组别列表
//   groupNameSelect = (value) => {
//     if (value) {
//       this.props.dispatch({
//         type: 'personCustomerModels/selectTeamName',
//         payload: { data: {groupId: value}},
//       }).then(()=>{
//         let {teamNames} = this.props.personModels;
//         let teamChildren = teamNames.data.map(e=><Option key={e.id} value={e.id}>{e.teamName}</Option>)
//         this.setState({teamChildren})
//       })
//     }
//   }

  // 重置
  resetFields = () => {
    this.props.form.resetFields();
  }

  // 上传查询
//   uploadSearch = () => {
//     this.props.dispatch({
//       type: 'personCustomerModels/importExcelSelectPersonnelInfo',
//     })
//   }

  // 上传变化时
//   onChange = (info) => {
//     if (info.file.status === 'done') {
//       if (info.file.response.code === 0) {
//         message.success(`《${info.file.name}》: ${info.file.response.message}`,2);
//         console.log('this', this)
//         this.props.dispatch({
//           type: 'personCustomerModels/save',
//           payload: {
//             searchPersonResp: info.file.response,
//           }
//         })
//       } else {
//         message.error(`《${info.file.name}》上传失败: ${info.file.response.message}`, 4);
//       }
//     } else if (info.file.status === 'error') {
//       message.error(`《${info.file.name}》: file upload failed.`);
//     }
//   }

  // 上传参数
//   uploadParam = {
//     showUploadList:false,
//     name: 'file',
//     action: `${SERVERHR}emp/importExcelSelectPersonnelInfo`,
//     withCredentials: true,
//     onChange: this.onChange,
//   };


  //上传人员信息
//   uploadPersonInfo = {
//     showUploadList:false,
//     name: 'file',
//     action: `${SERVERHR}emp/importEmpInfoExcel`,
//     withCredentials: true,
//     onChange(info) {
//       if (info.file.status === 'done') {
//         if (info.file.response.code === 0) {
//           console.log('上传回调结果');
//           console.log(info.file.response);
//           const res = info.file.response;
//           if(res && res.data){
//             _this.setState({data: res.data});
//           }
//           message.success(`《${info.file.name}》: ${info.file.response.message}`,2);
//         } else {
//           message.error(`《${info.file.name}》上传失败: ${info.file.response.message}`, 4);
//         }
//       } else if (info.file.status === 'error') {
//         message.error(`《${info.file.name}》: file upload failed.`);
//       } 
//     },
//   };

//   validator =(rule, value, callback)=>{
//     let {leaveDate,status} = this.props.form.getFieldsValue();
//     console.log(leaveDate)
//     console.log(status)
//     if(status==='4'&&leaveDate&&leaveDate.length===2){
//         callback('在职状态为离职时和离职时间二选一');
//         return;
//     }
//     // Note: 必须总是返回一个 callback，否则 validateFieldsAndScroll 无法响应
//     callback()
//     }
  

  render() {

    const { loading, personModels, form } = this.props;
    const { getFieldDecorator } = form;
    const data = personModels.searchPersonResp.data||{};
    const datalist = data.list||[];

    

    return (
      <PageHeaderLayout>
        <Card bordered={false}>
          <Form>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="员工姓名">
                  {getFieldDecorator('empName')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="证件号码">
                  {getFieldDecorator('identityCode')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
            </Row>
            {/* <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              {this.props.personModels.groupFormVisibleData.flag?
               <Col md={12} sm={24}>
                 <Form.Item {...formItemLayout} label="团队名称">
                   {getFieldDecorator('groupId')(
                     <Select placeholder='请选择' allowClear
                       onChange={this.groupNameSelect}>
                       {this.state.groupChildren}
                     </Select>
                   )}
                 </Form.Item>
               </Col>:null
              }
                <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="组别名称">
                  {getFieldDecorator('teamId')(
                    <Select placeholder='请选择' allowClear>
                      {this.state.teamChildren}
                    </Select>
                  )}
                </Form.Item>
              </Col>
             </Row>
             <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="入职时间">
                  {getFieldDecorator('entryDate')(
                    <RangePicker />
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="上线时间">
                  {getFieldDecorator('onlineDate')(
                     <RangePicker/>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="在职状态">
                  {getFieldDecorator('status',{rules:[{validator:this.validator}]})(
                    <Select>
                      <Option key='1' value='1'>全部</Option>
                      <Option key='4' value='4'>离职三个月内</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="离职时间">
                  {getFieldDecorator('leaveDate')(
                    <RangePicker/>
                  )}
                </Form.Item>
              </Col>
            </Row> */}
         </Form>
          <div align='center' style={{marginBottom: 20}}>
            {/* <Upload {...this.uploadParam}>
              <Button disabled={loading}><Icon type="upload" />导入查询</Button>
            </Upload> */}
            <Button loading={loading} style={{marginLeft: 20}} type='primary' onClick={this.queryPersonnelMain} icon='search'>搜索</Button>
            <Button loading={loading} style={{marginLeft: 20}} onClick={this.resetFields}>重置</Button>
            {/* <Button type='default' onClick={this.downloadPerson} icon='download' style={{marginLeft: 15}}>导出人员信息</Button> */}
          </div>
          {/* <Button type='primary' onClick={this.addPerson} icon='plus' style={{marginBottom: 10}}>新增人员信息</Button> */}
          {/* <Upload {...this.uploadPersonInfo}>
            <Button disabled={loading} style={{marginLeft:15}} ><Icon type="upload" />导入人员信息</Button>
          </Upload> */}
          {/* <Button type='default' onClick={this.downloadModal} icon='download' style={{marginLeft: 15}}>模板下载</Button> */}

          <Table
            columns={getPersonListColumns2(this)}
            dataSource={datalist}
            loading={this.props.loading}
            size='small'
            bordered={true}
            rowKey={(record, index) => index}
            onChange={this.handleTablePage}
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:data.total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            >
          </Table>
          {/* <Modal
            title="员工信息修改记录"
            visible={this.state.historyVisible}
            onOk={this.handleOkHistory}
            onCancel={this.handleCancelHistory}
            width={1300}
            destroyOnClose={true}
            closable={false}
            >
            <PersonHistoryModal
              record={this.state.record}>

            </PersonHistoryModal>
          </Modal> */}
        </Card>
      </PageHeaderLayout>
    )
  }

}