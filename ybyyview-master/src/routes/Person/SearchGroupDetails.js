import {
  Table,
  Checkbox,
  Popconfirm,
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider
} from 'snk-web';
import React, { PureComponent, Fragment } from 'react';
import { routerRedux } from 'dva/router';
import { SelectInfo } from './SearchGroup.js';
import { PermissionWrapper, PermissinSelectOption } from 'snk-sso-um';
import moment from 'moment';
import { connect } from 'dva';
import styles from './SearchGroup.less';
import styless from './SearchGroupDetails.less';
//import ClmForm from '../../components/ClmForm';
import searchGroupDetailsService from '../../services/searchGroupDetailsService'
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { isArray } from 'util';

/**
 * 新增成员弹出框
 */

const AddMemeberForm = Form.create()(props => {
  const { memberVisible, okHandle, form, handleMemberSearch, handleMemberVisible, dataSource, rowSelection } = props;
  const memberSearch = () => {
    form.validateFields((err, fieldsValue) => {
      console.log(fieldsValue)
      if(!fieldsValue.empName&&!fieldsValue.employeeCode){
        message.error('查询条件不能为空');
        return;
      };
      if (err) return;
      form.resetFields();
      handleMemberSearch(fieldsValue);
    });
  }

  const columns = [{
    title: '姓名',
    dataIndex: 'empName',
  }, {
    title: '工号',
    dataIndex: 'employeeCode',
  }, {
    title: '团队',
    dataIndex: 'groupName',
  }, {
    title: '组别',
    dataIndex: 'teamName',
  }];

  return (
    <Modal
      title="新建成员"
      visible={memberVisible}
      onOk={okHandle}
      onCancel={() => handleMemberVisible(false)}
      width={680}
    >
      <div>
        <Form layout="inline" >
          <FormItem label='姓名:'>
            {form.getFieldDecorator('empName', {
            })(
              <Input placeholder="请输入姓名" />
            )}
          </FormItem>
          <FormItem label='工号:' >
            {form.getFieldDecorator('employeeCode', {
            })(
              <Input placeholder="请输入工号" />
            )}
          </FormItem>
          <FormItem>
            <Button type="primary" onClick={memberSearch} >
              搜索
          </Button>
          </FormItem>
        </Form>
        <Table className={styless.table}
          bordered
          columns={columns}
          rowSelection={rowSelection}
          // onRow={(record) => ({ onClick: () => { selectRow(record.id); }, })}
          rowKey='id'
          pagination={false}
          dataSource={dataSource} />
      </div>
    </Modal>
  );
});


const FormItem = Form.Item;

//table 行编辑的属性定义
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
const statusMap = ['生效', '待生效'];
const isFirstCityMap = ['一线', '非一线'];

//table 单元格的定义
class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [{
                      // required: true,
                      message: `请输入 ${title}!`,
                    }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>

          );
        }}
      </EditableContext.Consumer>
    );
  }

}



//装饰器
@connect(({ umssouserinfo, groupModel, shareModel }) => ({
  umssouserinfo,
  groupModel,
  shareModel,
}))


@Form.create()
export default class SearchGroupDetails extends React.Component {
  state = {
    dataSource: [],
    groupVisible: false,
    selectedRows: [],
    data: [],
    value: '',
    recommend: {},
    pageNum: 1,
    pageSize: 10,
    editingKey: '',
    groupData: {},
    memberVisible: false,
    memberTableList: [],
    teamId: '',
    groupId: '',
    selectMemberKey: '',
    addButtonFlage: true
  };


  //构造table结构
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: '序号',
        dataIndex: 'index',
        render: (text, record, index) => {
          return index + 1;
        },
      },
      {
        title: '组别名称',
        dataIndex: 'teamName',
        editable: true,
      },
      {
        title: '组别编码',
        dataIndex: 'id',
      },
      {
        title: '创建日期',
        dataIndex: 'createdTime',
        width: '7',
        render(val) {
          return <div> {moment(parseInt(val)).format("YYYY-MM-DD")} </div>
        },
      },
      {
        title: '操作',
        dataIndex: 'Name',
        width: '20%',
        render: (status, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <a
                        href="javascript:;"
                        onClick={() => this.save(form, record)}
                        style={{ marginRight: 8 }}
                      >
                        保存
                        </a>
                    )}
                  </EditableContext.Consumer>
                  <Popconfirm
                    title="确定取消吗"
                    onConfirm={() => this.cancel(record.key)}
                  >
                  </Popconfirm>
                  <a onClick={() => this.cancel(record.key)}>取消</a>
                </span>
              ) : (
                  <div>
                    <a onClick={() => this.edit(record.key)}>编辑</a>
                    <Divider type="vertical" />
                    <a onClick={() => this.handleSearchTeamMember(record)}>查看组别成员</a>
                  </div>
                )}
            </div>
          );
        }
      },
    ];
    this.memeberColumns = [
      {
        title: '成员',
        dataIndex: 'empName',
        width: '7',
        editable: true,
      },
      {
        title: '岗位',
        dataIndex: 'positionName',
      },
      {
        title: '工号',
        dataIndex: 'employeeCode',
        width: '7',
      },
      {
        title: '在职状态',
        dataIndex: 'status',
        width: '7',
        render(val) {
          // '1培训期2试用期3已转正4已离职',
          if (val == 1) {
            return '培训期'
          } else if (val == 2) {
            return '试用期'
          } else if (val == 3) {
            return '已转正'
          } else if (val == 4) {
            return '已离职'
          }
          return val;
        },
      },
      {
        title: '所属组别',
        dataIndex: 'teamName',
        width: '7',
      },
      {
        title: '挂靠开始时间',
        dataIndex: 'bgnDate',
        width: '7',
      },
      {
        title: '挂靠结束时间',
        dataIndex: 'endDate',
        width: '7',
      },
      {
        title: '操作',
        dataIndex: 'Name',
        editable: true,
        width: '20%',
        render: (status, record) => {
          return (<div>
            <div>
              <a onClick={()=>this.editMember(record)}>编辑</a>
                <Divider type="vertical" />
              <Popconfirm
                title="确定删除吗"
                onConfirm={() => this.deleteMember(record)}
              >
                <a >删除</a>
              </Popconfirm>
            </div>
          </div>)

        }
      }
    ]
  }
  deleteMember = (record) => {
    console.log(record)
    const data = {
      "data": {
        "teamId": this.state.teamId,
        "id": record.teamRecordId
      }
    }
    searchGroupDetailsService.deleteMember(data).then(response => {
      if (response.code == 0) {
        message.success('删除成功')
        //获取成员表
        searchGroupDetailsService.searchGroupDetails({ data: { id: this.state.teamId } }).then(response => {
          console.log(response)
          this.setState({ memberTableList: response.data })
        })
      } else {
        message.error('删除失败')
      }
    })
  }

  editMember=(record)=>{
    const {dispatch} = this.props
    console.log(record)
    dispatch({
      type: 'groupModel/isChuangXinGroup',
      payload:{groupId:this.state.groupId}
    }).then((flag) => {
      if(flag){
        dispatch(routerRedux.push({pathname: '/person/editCustomerPerson', personId: record.empId}))
      }else{
        dispatch(routerRedux.push({pathname: '/person/editPerson', personId: record.empId}))
      }
    }).catch((e) => {
      console.log(e);
    });
    
  }
  componentDidMount() {
    if(this.props.match.params==null)  this.props.dispatch(routerRedux.push({pathname: '/person/searchPerson'}))
    const params = this.props.match.params.id;
    console.log('--------传递的参数----------' + params)

    console.log('componentDidMount called..');
    const { dispatch, umssouserinfo: { currentUser } } = this.props;
    const { search } = this.props.location;
    console.log(search);
    const data = { id: params };
    //获取团队信息，用于显示与支持用户更新
    dispatch({
      type: 'groupModel/searchGroup',
      payload: data,
    }).then(() => {
      this.setState({ dataSource: this.props.groupModel.data.list, teamId: params, groupId: params});
      console.log(this.props.groupModel.data.list);
    }).catch((e) => {
      console.log(e);
    });
  }


  //table 行编辑事件
  isEditing = (record) => {
    return record.key === this.state.editingKey;
  };

  save(form, record) {
    const newData = [...this.state.dataSource];
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      // 根据团队id查询出来的只有可能是1个团队
      if (newData.length > 0) {
        let item = newData[0];
        let teamList = item.teams;
        const key = record.key;
        const index = teamList.findIndex(item => key === item.key);
        console.log('save called...');
        console.log(row);
        if (index > -1) {
          const team = teamList[index];
          team.teamName = row.teamName;
          teamList.splice(index, 1, { ...team, row });
          newData.splice(0, 1, { ...item, });
          // 向服务器更新组别信息
          const toSaveData = {
            id: team.id,
            teamName: team.teamName,
            // status: 0,
          }
          this.props.dispatch({
            type: 'groupModel/updateTeam',
            payload: toSaveData,
          }).then(() => {
            this.setState({ data: newData, editingKey: '' });
            message.success("保存成功");
          }).catch((e) => {
            console.log(e);
            message.error(e.message);
          });
        }
      } else {
        message.error('Error');
      }
    });
  };

  edit(record) {
    const { editingKey } = this.state;
    if (editingKey != '') {
      message.error("你有小组正在编辑状态，请先处理后再编辑！")
    } else {
      this.setState({ editingKey: record, editingRecord: record });
    }
  };

  cancel = (key) => {
    this.setState({ editingKey: '' });
  };


  handleMemberSearch = (data) => {
    data.groupId=this.state.groupId
    this.setState({ selectMemberKey: '' });
    this.props.dispatch({
      type: 'groupModel/searchMemebers',
      data
    }).then(()=>{
      console.log(this.state)
      this.setState({
        searchMemebers:this.props.groupModel.searchMemebers
      })
    })
  }

  handleMemberVisible = (flag) => {
    this.setState({
      memberVisible: flag
    })
  }



  // 查看组别成员
  handleSearchTeamMember(record) {
    const data = { id: record.id,groupId:this.state.groupId };
    this.props.dispatch({
      type: 'groupModel/queryTeamMember',
      payload: data,
    }).then(() => {
      this.setState({ memberTableList: this.props.groupModel.teamMembers, teamId: record.id });
      message.success("Success");
    }).catch((e) => {
      console.log(e);
      message.error(e.message);
    });
    this.setState({
      addButtonFlage: false
    })
  }

  handleUpdateGroup = e => {
    //防止链接打开 URL
    if (e) {
      e.preventDefault();
    }
    const { dispatch, form } = this.props;
    //表单校验
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue, // 表单所有组件的值
        id: this.state.groupId,
      };
      console.log('handleUpdateGroup called..');
      console.log(values);
      // const toSaveData = {

      // }
      this.props.dispatch({
        type: 'groupModel/editGroup',
        payload: values,
      }).then(() => {
        //this.setState({ data: newData, editingKey: ''});
        message.success("保存成功");
      }).catch((e) => {
        console.log(e);
        message.error(e.message);
      });
    });
  }

  //加载组件
  render() {
    //合并组件
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    //引入table列和编辑
    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    const { loading, form, groupModel } = this.props;
    let { selectedRows, groupVisible, addVisible, dataSource, groupData, } = this.state;
    let teams = [];


    let total = 0;
    if (groupModel.data) {   //模型 initgroupModelInfo 的参数
      total = groupModel.data.total;
    }

    //定义团队名称Options值
    let groupOptions = [];
    if (this.props.shareModel.groupsData) {
      groupOptions = this.props.shareModel.groupsData.map(group => <Select.Option key={group.id}>{group.groupName}</Select.Option>);
    }


    // const selectRow = (id) => {
    //   console.log(id)
    //   this.setState({ selectMemberKey: id });
    //   console.log(this.state.selectMemberKey)
    // }

    const memeberOKHandle = () => {
      this.setState({
        memberVisible: false
      })
      console.log(this.state)
      if (this.state.selectMemberKey == '') return;

      //回调
      console.log('添加ID：', this.state.selectMemberKey)
      searchGroupDetailsService.addMember({
        "data": {
          "id": this.state.selectMemberKey,
          "teamId": this.state.teamId
         
        }
      }).then(response => {
        if (response.code == 0) {
          message.success('添加成功')
          //获取成员表
          this.props.dispatch({
            type: 'groupModel/queryTeamMember',
            payload:  { id: this.state.teamId,groupId:this.state.groupId },
          }).then(() => {
            this.setState({ memberTableList: this.props.groupModel.teamMembers, selectedRowKeys: '' });
            message.success("Success");
          }).catch((e) => {
            console.log(e);
            message.error(e.message);
          });

          // searchGroupDetailsService.searchGroupDetails({ data: { id: this.state.teamId } }).then(response => {
          //   console.log(response)
          //   this.setState({ memberTableList: response.data, selectedRowKeys: '' })
          // })
        } else {
          message.success('添加失败')
        }
      }).catch(e=>message.error(e.message))
    }
    // rowSelection object indicates the need for row selection
    const memeberSelection = {
      onChange: (selectedRowKeys) => {
        console.log('selectedRowKeys: ', selectedRowKeys);
        this.setState({ selectMemberKey: selectedRowKeys[0] });
      },
      selectedRowKeys: this.state.selectMemberKey,
      type: 'radio',

    };


    const parentPropertities = {
      handleMemberSearch: this.handleMemberSearch,
      handleMemberVisible: this.handleMemberVisible,
      dataSource: this.state.searchMemebers,
      rowSelection: memeberSelection,
      // selectRow,
      okHandle: memeberOKHandle,
    };

    // groupData = {};
    // groupData.groupName = '';
    // groupData.deptCode = '';
    // groupData.isFirstCity = '';
    // groupData.lawId = '';
    if (dataSource && dataSource.length > 0) {
      groupData = dataSource[0];
      teams = groupData.teams;
    }
    console.log('groupData.. ');
    console.log(groupData);

    if (groupModel.data) {
      return (
        // <PageHeaderLayout> 
        <Card style={{overflow: 'auto', height: '100%'}}>
          <Card title="团队信息管理" type="inner" bordered={false} >
            <Form onSubmit={this.handleUpdateGroup} layout="inline" style={{ width: 1080 }}>
              <Row gutter={24}>
                <Col span={8}>
                  <FormItem label="团队名称">
                    {form.getFieldDecorator('groupName', { initialValue: groupData.groupName })
                      (<Input style={{ width: 110 }} placeholder="团队名称" />)
                    }
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem label="类别属性:">
                    {form.getFieldDecorator('isFirstCity', { initialValue: groupData.isFirstCity + '' })
                      (<Select style={{ width: 130 }} >
                        <Select.Option key='1'>一线城市</Select.Option>
                        <Select.Option key='0'>非一线城市</Select.Option>
                      </Select>)
                    }
                  </FormItem>
                </Col>
                <Col span={8}>
                  <FormItem  label="基本法:">
                    {form.getFieldDecorator('lawId', { initialValue: groupData.lawId + '' })
                      (<Select style={{ width: 130 }} >
                        <Select.Option key='2'>创新非自建</Select.Option>
                        <Select.Option key='1'>创新自建</Select.Option>
                      </Select>)
                    }
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={{ md: 12, lg: 12, xl: 24 }} style={{ paddingRight: 50 }}>
                <Col md={12} sm={24}>
                  <span className={styles.submitButtons}>
                    <Button type="primary" htmlType="submit">
                      更新
                    </Button>
                  </span>
                </Col>
              </Row>
            </Form>
          </Card>
          <Card title="组别信息管理" type="inner" bordered={false}>
            <div className={styles.positionList}>
              <br />
              <Table
                childrenColumnName={'teams'}
                components={components}
                selectedRows={selectedRows}
                loading={loading}
                dataSource={teams}
                state={teams}
                columns={columns}
                // rowClassName="editable-row"
                onSelectRow={this.handleSelectRows}
                addVisible={this.addVisible}
                onChange={this.handleStandardTableChange}
                pagination={false}
              />
            </div>
          </Card>

          <Card bordered={false} type="inner" title="组别成员管理">
          <Button type="primary" disabled={this.state.addButtonFlage} onClick={() => this.setState({ memberVisible: true, selectMemberKey: '',searchMemebers:[] })}> 新建成员
              </Button>
            <Table
              bordered
              columns={this.memeberColumns}
              rowKey='teamRecordId'
              pagination={false}
              scroll={{x:true,y:500}}
              dataSource={this.state.memberTableList} />
         
          </Card>
          <AddMemeberForm  {...parentPropertities} memberVisible={this.state.memberVisible} />
        </Card>

        // </PageHeaderLayout>

      );
    } else {
      return '404';
    }
  }
}




