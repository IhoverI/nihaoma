import React, { PureComponent } from 'react';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { Spin, Form, Input, Row, Col, Select, Button, message, DatePicker, Table, Card, Modal } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';
import {
  identityCodeValid, getBirthdayFromIdentityCode, getSexFromIdentityCode, getAgeFromIdentityCode
} from '../../utils/utils';
import TeamInfo from './TeamInfo';
import PositionAndRankMessage from './PositionAndRankMessage';
import { routerRedux } from 'dva/router';
import { isNull } from 'util';
import { fileToObject } from 'antd/lib/upload/utils';

const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;
const formItemLayout = {
  labelCol: {
    sm: { span: 7 },
  },
  wrapperCol: {
    sm: { span: 15 },
  },
};

@connect(({ personModels, loading }) => ({
  personModels,
  loading: loading.models.personModels,
}))
@Form.create()
export default class EditBasePersonMessage extends PureComponent {

  state = {
    isInternalRecommendation: false,
    isShowTeamInfo: false,
    groupId: '',
    groupName: "",
    positionAndRankDataSource: [],
    teamInfoDataSource: [],
  }

  // 初始化外包公司列表
  componentDidMount() {
    let personId = this.props.location.personId;
    if (!personId) {
      this.props.dispatch(routerRedux.push('/person/searchPerson'));
    }
    let reqBody = {
      data: {},
    }
    reqBody.data.id = personId;
    //公司名称
    this.props.dispatch({
      type: 'personModels/selectOutSourceName',
      payload: reqBody,
    }).then(() => {
      //团队信息
      this.props.dispatch({
        type: 'personModels/selectGroupName',
        payload: reqBody,
      })
    }).then(() => {
      // 用户信息
      this.props.dispatch({
        type: 'personModels/findPersonnel',
        payload: reqBody,
      }).then(() => {
        console.log('personDetail..');
        console.log(this.props.personModels.personDetail);
        let searchPisitionsData = {};
        const { personDetail } = this.props.personModels;
        console.log(personDetail)
        if (personDetail && personDetail.data) {
          const groupId = personDetail.data.groupId
          searchPisitionsData = { groupId: groupId };
          this.setState({ groupId: groupId });
          // 职位信息
        this.props.dispatch({
          type: 'personModels/selectPositionName',
          payload: searchPisitionsData,
        }).then(() => {
          let groupNames = this.props.personModels.groupNames.data || [];
          let groupName = "";
          for (let i = 0; i < groupNames.length; i++) {
            if (groupNames[i].id == groupId) {
              groupName = groupNames[i].groupName;
              let category = groupNames[i].isFirstCity === 1 ? "一类" : "非一类";
              this.props.form.setFieldsValue({ category: category })
            }
          }
        })
        }
        
      }).catch((e) => {
        message.error(e.message);
      });
    })
  }

  recommendPersonCheck(fieldValues) {
    if (fieldValues.isRecommended && fieldValues.isRecommended == '1') {
      if (!fieldValues.recommendPerson) {
        message.error("当内荐人选项为“是”，内荐人不能为空。");
        return false;
      }
    }
    return true;
  }

  // 提交
  handleSubmit = () => {
    this.props.form.validateFieldsAndScroll({force:true},(err, fieldValues) => {
      if (err) {
        return;
      }
      // 当内荐人选项为“是”，内荐人不能为空。
      if (!this.recommendPersonCheck(fieldValues)) {
        return;
      }
      let reqBody = this.dealFieldValues(fieldValues);
      console.log('fieldValues', fieldValues);
      console.log('fieldValues2', reqBody);
      this.props.dispatch({
        type: 'personModels/updatePersonnel',
        payload: reqBody,
      }).then(() => {
        let updatePersonnelResp = this.props.personModels.updatePersonnelResp;
        if (updatePersonnelResp.code === 0) {
          message.success(updatePersonnelResp.message)
        } else {
          message.error(updatePersonnelResp.message)
        }
      });
    });
  }

  // 处理请求参数
  dealFieldValues = (fieldValues) => {
    console.log(fieldValues)
    delete fieldValues.age;
    fieldValues.birthday = fieldValues.birthday.format('X') * 1000;

    if (fieldValues.entryDate) {
      fieldValues.entryDate = fieldValues.entryDate.format('X') * 1000;
    }

    if (fieldValues.leaveDate) {
      fieldValues.leaveDate = fieldValues.leaveDate.format('X') * 1000;
    }

    // if (!fieldValues.trainBgnAndEndDate || fieldValues.trainBgnAndEndDate.length === 0) {
    //   fieldValues.trainBgnDate = null;
    //   fieldValues.trainEndDate = null;
    // } else {
    //   fieldValues.trainBgnDate = fieldValues.trainBgnAndEndDate[0].format('X') * 1000;
    //   fieldValues.trainEndDate = fieldValues.trainBgnAndEndDate[1].format('X') * 1000;
    // }

    if (fieldValues.trainBgnAndEndDate[0]) {
      fieldValues.trainBgnDate = fieldValues.trainBgnAndEndDate[0].format('X') * 1000;
    }else{
      fieldValues.trainBgnDate = null;
    }

    if (fieldValues.trainBgnAndEndDate[1]) {
      fieldValues.trainEndDate = fieldValues.trainBgnAndEndDate[1].format('X') * 1000;
    }else{
      fieldValues.trainEndDate = null;
    }


    
    delete fieldValues.trainBgnAndEndDate;

    if (fieldValues.onlineDate) {
      fieldValues.onlineDate = fieldValues.onlineDate.format('X') * 1000;
    }

    if (fieldValues.formalDate) {
      fieldValues.formalDate = fieldValues.formalDate.format('X') * 1000;
    }
    if (fieldValues.changeGroupDate) {
      fieldValues.changeGroupDate = fieldValues.changeGroupDate.format('X') * 1000;
    }
    delete fieldValues.category;
    delete fieldValues.manpowerInterval;
    delete fieldValues.onlineDays;

    delete fieldValues.teamDate;

    delete fieldValues.extTrainPositionId;

    delete fieldValues.rankDate;

    fieldValues.id = this.props.location.personId;

    let reqBody = {
      data: fieldValues
    }
    return reqBody;
  }

  // 证件号码失去焦点时
  identityCodeOnBlur = () => {
    let identityCode = this.props.form.getFieldValue('identityCode');
    var resp = identityCodeValid(identityCode);
    if (resp) {
      message.error(resp);
    } else {
      let { form } = this.props;
      let birthday = getBirthdayFromIdentityCode(identityCode);
      let sex = getSexFromIdentityCode(identityCode);
      let age = getAgeFromIdentityCode(identityCode);
      form.setFieldsValue({ birthday: moment(birthday, "YYYY-MM-DD"), sex: sex, age: age })
    }
  }

  // 校验身份证号
  validateIdentityCode = (rule, value, callback) => {
    let resp = identityCodeValid(value);
    if (resp) {
      callback(resp);
    }
    callback();
  }

  // 选择内荐时，内荐人必填
  isRecommendedChange = (value) => {
    if (value === "1") {
      this.setState({
        isInternalRecommendation: true,
      }, () => {
        this.props.form.validateFields(['recommendPerson'], { force: true })
      })
    } else {
      this.setState({
        isInternalRecommendation: false,
      }, () => {
        this.props.form.validateFields(['recommendPerson'], { force: true })
      })
    }
  }

  // 根据团队显示类别;团队变化改变培训岗位options
  groupIdChange = (value) => {
    let groupNames = this.props.personModels.groupNames.data || [];
    let groupName = "";
    for (let i = 0; i < groupNames.length; i++) {
      if (groupNames[i].id == value) {
        groupName = groupNames[i].groupName;
        let category = groupNames[i].isFirstCity === 1 ? "一类" : "非一类";
        this.props.form.setFieldsValue({ category: category })
      }
    }
    // 根据团队查询组别列表
    if (value) {
      this.setState({
        isShowTeamInfo: true,
        groupId: value,
        groupName: groupName,
      })
    } else {
      this.setState({
        isShowTeamInfo: false,
        groupId: undefined,
        groupName: "",
      })
    }
    // 职位信息
    const { form, personModels } = this.props
    this.props.dispatch({
      type: 'personModels/selectPositionName',
      payload: { groupId: value },
    }).then(() => form.setFieldsValue({ trainPositionId: '' }))

  }

   onChange0= (value) => {
    let trainBgnAndEndDate = this.props.form.getFieldValue('trainBgnAndEndDate')
    trainBgnAndEndDate[0]=value
    this.props.form.setFieldsValue({trainBgnAndEndDate})
   
  }

   onChange1= (value) => {
    let trainBgnAndEndDate = this.props.form.getFieldValue('trainBgnAndEndDate')
    trainBgnAndEndDate[1]=value
    this.props.form.setFieldsValue({trainBgnAndEndDate})
  }


  handleConfirmTrainBgnAndEndDate = (rule, value, callback) => {
    const { getFieldValue } = this.props.form;
    const entryDate = getFieldValue('entryDate');
    console.log('handleConfirmTrainBgnAndEndDate');
    // if (!value[0] || !value[1]) {
    //   callback('培训时间不能为空')
    // }
    // if (entryDate == null || value[0] < entryDate) {
    //   callback('起始时间要大于入职时间')
    // }
    // Note: 必须总是返回一个 callback，否则 validateFieldsAndScroll 无法响应
    callback()
  }

  validateFieldOnlineDate = (rule, value, callback) => {
    const { getFieldsValue } = this.props.form;
    let {onlineDate,trainBgnAndEndDate} = getFieldsValue(['onlineDate','trainBgnAndEndDate']);
    let [trainBgnAndEndDate1,trainBgnAndEndDate2] = trainBgnAndEndDate
    console.log(onlineDate)
    if(onlineDate){
        if(!trainBgnAndEndDate1||!trainBgnAndEndDate2){
          callback('上线日期存在时，培训期不能为空')
        }
        if(onlineDate.isBefore(trainBgnAndEndDate2)){
          callback('上线日期必须晚于培训期')
        }
    }
   
    callback()
  }
  render() {

    const { loading, personModels, form } = this.props;
    const { getFieldDecorator } = form;

    let outsourceIdChild = [];
    let outsourceId = personModels.outsourceId.data || [];
    for (let i = 0; i < outsourceId.length; i++) {
      outsourceIdChild.push(<Option key={outsourceId[i].id.toString()}>{outsourceId[i].outSourceName}</Option>)
    }

    // 岗位下拉列表
    let positionNameChild = [];
    let positionNames = personModels.positionNames.data || [];
    for (let i = 0; i < positionNames.length; i++) {
      positionNameChild.push(<Option key={positionNames[i].id.toString()}>{positionNames[i].positionName}</Option>)
    }

    // 团队下拉列表
    let groupNames = personModels.groupNames.data || [];
    let groupChildren = [];
    for (let i = 0; i < groupNames.length; i++) {
      groupChildren.push(<Option key={groupNames[i].id.toString()}>{groupNames[i].groupName}</Option>)
    }

    // 员工详细信息
    let personDetail = personModels.personDetail.data;
    let teamDataSource = [];
    let rankDataSource = [];
    if (personDetail) {
      teamDataSource = personDetail.teamInfoDto;
      rankDataSource = personDetail.rankInfoDto;
    }

    if (personDetail) {
      return (
        <PageHeaderLayout>
          <Form>
            <Card title="员工基本信息">

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="员工姓名">
                    {getFieldDecorator('empName',
                      {
                        initialValue: personDetail.empName,
                        rules: [{ required: true, message: "谁没有姓名" }]
                      })(
                        <Input placeholder='请输入' />
                      )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="证件号码">
                    {getFieldDecorator('identityCode',
                      { initialValue: personDetail.identityCode, })(
                        <Input disabled />
                      )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="出生年月">
                    {getFieldDecorator('birthday', { initialValue: moment(personDetail.birthday, 'YYYY-MM-DD') })(
                      <DatePicker style={{ width: '100%' }} disabled></DatePicker>
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="性别">
                    {getFieldDecorator('sex', { initialValue: personDetail.sex })(
                      <Select placeholder="请选择" disabled>
                        <Option value="1">男</Option>
                        <Option value="0">女</Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="年龄">
                    {getFieldDecorator('age', { initialValue: personDetail.age })(
                      <Input disabled placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="入职日期">
                    {getFieldDecorator('entryDate', {
                      initialValue: parseInt(personDetail.entryDate) ? moment(parseInt(personDetail.entryDate)) : null,
                      rules: [{ required: true, message: "必填" }]
                    })(
                      <DatePicker style={{ width: '100%' }}></DatePicker>
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="电话号码">
                    {getFieldDecorator('mobile', {
                      initialValue: personDetail.mobile,
                      rules: [{ required: true, message: "必填" }]
                    })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="邮箱地址">
                    {getFieldDecorator('email', {
                      initialValue: personDetail.email
                    })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="银行名称">
                    {getFieldDecorator('bankName', { initialValue: personDetail.bankName })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="银行账号">
                    {getFieldDecorator('bankNo', { initialValue: personDetail.bankNo })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="社保账户">
                    {getFieldDecorator('socialAccount', { initialValue: personDetail.socialAccount })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="公积金账户">
                    {getFieldDecorator('accumulationAccount', { initialValue: personDetail.accumulationAccount })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
              </Row>


              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="外勤工号">
                    {getFieldDecorator('employeeCode', { initialValue: personDetail.employeeCode })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="内勤工号">
                    {getFieldDecorator('officeEmployeeCode', { initialValue: personDetail.officeEmployeeCode })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="所在团队">
                    {getFieldDecorator('groupId', {
                      initialValue: personDetail.groupId,
                      rules: [{ required: true, message: "必选" }]
                    })(
                      <Select placeholder="请选择" onChange={this.groupIdChange}>
                        {groupChildren}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="进入团队时间">
                    {getFieldDecorator('changeGroupDate', {
                      initialValue: personDetail.changeGroupDate?moment(personDetail.changeGroupDate, 'YYYY-MM-DD'):null,
                      rules: [{ required: true, message: "必选" }]
                    })(
                      <DatePicker style={{ width: '100%' }}></DatePicker>
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="类别">
                    {getFieldDecorator('category', { initialValue: personDetail.category })(//不用传到后台
                      <Input placeholder='请输入' disabled />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="是否薪酬保护">
                    {getFieldDecorator('isSalaryProtect', {
                      initialValue: personDetail.isSalaryProtect,
                      rules: [{ required: true, message: "必选" }]
                    })(
                      <Select placeholder="请选择">
                        <Option value="1">是</Option>
                        <Option value="0">否</Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="外包公司名称">
                    {getFieldDecorator('outsourceId', {
                      initialValue: personDetail.outsourceId,
                      rules: [{ required: true, message: "必选" }]
                    })(
                      <Select placeholder="请选择">
                        {outsourceIdChild}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="培训岗位">
                    {getFieldDecorator('trainPositionId', {
                      initialValue: personDetail.trainPositionId,
                      rules: [{ required: true, message: "必选" }]
                    })(
                      <Select placeholder="请选择">
                        {positionNameChild}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="是否为内荐">
                    {getFieldDecorator('isRecommended', {
                      initialValue: personDetail.isRecommended,
                      rules: [{ required: true, message: "必选" }]
                    })(
                      <Select placeholder="请选择" onChange={this.isRecommendedChange}>
                        <Option value="1">是</Option>
                        <Option value="0">否</Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="内荐人">
                    {getFieldDecorator('recommendPerson', {
                      initialValue: personDetail.recommendPerson,
                      rules: [{ required: this.state.isInternalRecommendation, message: "必填" }]
                    })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="紧急联系人">
                    {getFieldDecorator('emergencyPerson', { initialValue: personDetail.emergencyPerson })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="紧急联系人电话号码">
                    {getFieldDecorator('emergencyTel', { initialValue: personDetail.emergencyTel })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="家庭住址">
                    {getFieldDecorator('homeAddress', { initialValue: personDetail.homeAddress })(
                      <Input placeholder='请输入' />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="在职状态">
                    {getFieldDecorator('status', {
                      initialValue: personDetail.status,
                      rules: [{ required: true, message: "必选" }]
                    })(
                      <Select placeholder="请选择">
                        <Option value="1">培训期</Option>
                        <Option value="2">试用期</Option>
                        <Option value="3">已转正</Option>
                        <Option value="4">已离职</Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="离职日期">
                    {getFieldDecorator('leaveDate', { initialValue: parseInt(personDetail.leaveDate) ? moment(parseInt(personDetail.leaveDate)) : null })(
                      <DatePicker style={{ width: '100%' }}></DatePicker>
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="培训期">
                    {getFieldDecorator('trainBgnAndEndDate', {
                      initialValue:
                        [parseInt(personDetail.trainBgnDate) ? moment(parseInt(personDetail.trainBgnDate)) : null,
                        parseInt(personDetail.trainEndDate) ? moment(parseInt(personDetail.trainEndDate)) : null],
                      // rules: [{ required: true, message: "必填" }, {
                      //   validator: this.handleConfirmTrainBgnAndEndDate
                      // }]
                    })(
                      <div>
                        <DatePicker onChange={this.onChange0} value={this.props.form.getFieldValue('trainBgnAndEndDate')[0]} style={{ width: '48%' }}></DatePicker>
                      ~
                      <DatePicker onChange={this.onChange1} value={this.props.form.getFieldValue('trainBgnAndEndDate')[1]} style={{ width: '48%' }}></DatePicker>
                      </div>
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="上线日期">
                    {getFieldDecorator('onlineDate', { initialValue: parseInt(personDetail.onlineDate) ? moment(parseInt(personDetail.onlineDate)) : null,
                      rules: [ {
                        validator: this.validateFieldOnlineDate
                      }] },
                    )(
                      <DatePicker style={{ width: '100%' }}></DatePicker>
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="转正时间">
                    {getFieldDecorator('formalDate', { initialValue: parseInt(personDetail.formalDate) ? moment(parseInt(personDetail.formalDate)) : null })(
                      <DatePicker style={{ width: '100%' }}></DatePicker>
                    )}
                  </Form.Item>
                </Col>
              </Row>


              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="人力区间">
                    {getFieldDecorator('manpowerInterval', { initialValue: personDetail.manpowerInterval })(//新增编辑都不用传到后台
                      <Input placeholder='请输入' disabled />
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="上线天数">
                    {getFieldDecorator('onlineDays', { initialValue: personDetail.onlineDays })(//新增编辑者不用传到后台
                      <Input placeholder='请输入' disabled />
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <Button onClick={this.handleSubmit} loading={loading} type='primary' style={{ textAlign: 'center', marginTop: 20 }}>
                更新基本信息
              </Button>
            </Card>


            <Card title="组别信息" style={{ marginTop: 10 }}>
              <TeamInfo
                teamDataSource={teamDataSource}
                groupChildren={groupChildren}
                empId={this.props.location.personId}
                groupId={this.props.personModels.personDetail.data.groupId}
              >
              </TeamInfo>
            </Card>

            <Card title="职级信息" style={{ marginTop: 10 }}>
              <PositionAndRankMessage
                rankDataSource={rankDataSource}
                positionNameChild={positionNameChild}
                empId={this.props.location.personId}
              >
              </PositionAndRankMessage>
            </Card>
          </Form>
        </PageHeaderLayout>


      )
    } else {
      return (
        <PageHeaderLayout>
          <div style={{
          textAlign: 'center',
          borderRadius: 4,
          marginBottom: 20,
          padding: [30, 50],
          margin: [20, 0]
        }}><Spin /></div>
        </PageHeaderLayout>)
    }

  }
}