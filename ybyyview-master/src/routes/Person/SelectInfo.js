import React from 'react';
import { Icon } from 'snk-web';
import Styles from './SelectInfo.less';

function SelectInfo({ number, right, ...props }) {
  return (
    <div className={Styles.wrap} {...props}>
      <Icon type="info-circle-o" className={Styles.icon} />
      { props.title ? (<span className={Styles.title}>{props.title}</span>) : null }
      { !number && number !== 0 ? null :
        (<span>已选择<span className={Styles.num}>{number}</span>项</span>)}
      <div className={Styles.right} >
        {right}
      </div>
    </div>
  );
}

export default SelectInfo;
