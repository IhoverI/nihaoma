import React, { PureComponent } from 'react';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { Form, Input, Row, Col, Select, Button, message, DatePicker, Table, Card, Modal } from 'snk-web';
import {Link, routerRedux} from 'dva/router';
import { connect } from 'dva';
import moment from 'moment';
import { identityCodeValid, getBirthdayFromIdentityCode, getSexFromIdentityCode, getAgeFromIdentityCode
 } from '../../utils/utils';
import TeamInfo from './TeamInfo';
import PositionAndRankMessage from './PositionAndRankMessage';

const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;
const formItemLayout = {
  labelCol: {
    sm: { span: 7 },
  },
  wrapperCol: {
    sm: { span: 15 },
  },
};

@connect(({ personModels, loading }) => ({
  personModels,
  loading: loading.models.personModels,
}))
@Form.create()
export default class BasePersonMessage extends PureComponent {

  state = {
    isInternalRecommendation: false,
    isShowTeamInfo: false,
    isShowRank: false,
    groupId: {},
    groupName: "",
    positionAndRankDataSource: [],
    teamInfoDataSource: [],
  }

  // 初始化外包公司列表
  componentDidMount() {
    let reqBody = {
      data: {},
    }
    this.props.dispatch({
      type: 'personModels/selectOutSourceName',
      payload: reqBody,
    })
    this.props.dispatch({
      type: 'personModels/selectPositionName',
      payload: reqBody,
    })
    this.props.dispatch({
      type: 'personModels/selectGroupName',
      payload: reqBody,
    })
    reqBody.data.id = 6;
    this.props.dispatch({
      type: 'personModels/findPersonnel',
      payload: reqBody,
    })
  }

  // 提交
  handleSubmit = () => {
    
    this.props.form.validateFieldsAndScroll({force:true},(err, fieldValues) => {
      if (err) {
        return;
      }
     
      let reqBody = this.dealFieldValues(fieldValues);
      this.props.dispatch({
        type: 'personModels/addPersonnel',
        payload: reqBody,
      }).then(() => {
        let addResp = this.props.personModels.addResp;
        if (addResp.code === 0) {
          message.success(addResp.message)
        } else {
          message.error(addResp.message)
        }
      
      });
    });
  }

  // 处理请求参数
  dealFieldValues = (fieldValues) => {
    delete fieldValues.age;
    fieldValues.birthday = fieldValues.birthday.format('X') * 1000;
    fieldValues.entryDate = fieldValues.entryDate.format('X') * 1000;
    if(fieldValues.leaveDate){
      fieldValues.leaveDate = fieldValues.leaveDate.format('X') * 1000;
    }
    
    

    // if (!fieldValues.trainBgnAndEndDate || fieldValues.trainBgnAndEndDate.length === 0) {
    //   fieldValues.trainBgnDate = null;
    //   fieldValues.trainEndDate = null;
    // } else {
    //   fieldValues.trainBgnDate = fieldValues.trainBgnAndEndDate[0].format('X') * 1000;
    //   fieldValues.trainEndDate = fieldValues.trainBgnAndEndDate[1].format('X') * 1000;
    // }

    
    if (fieldValues.trainBgnAndEndDate[0]) {
      fieldValues.trainBgnDate = fieldValues.trainBgnAndEndDate[0].format('X') * 1000;
    }else{
      fieldValues.trainBgnDate = null;
    }

    if (fieldValues.trainBgnAndEndDate[1]) {
      fieldValues.trainEndDate = fieldValues.trainBgnAndEndDate[1].format('X') * 1000;
    }else{
      fieldValues.trainEndDate = null;
    }
    delete fieldValues.trainBgnAndEndDate;

    if(fieldValues.onlineDate){
      fieldValues.onlineDate = fieldValues.onlineDate.format('X') * 1000;
    }
    
    if(fieldValues.formalDate){
      fieldValues.formalDate = fieldValues.formalDate.format('X') * 1000;
    }
    if(fieldValues.changeGroupDate){
      fieldValues.changeGroupDate = fieldValues.changeGroupDate.format('X') * 1000;
    }
    
    delete fieldValues.category;
    delete fieldValues.manpowerInterval;
    delete fieldValues.onlineDays;

    
    if (!fieldValues.teamDate || fieldValues.teamDate.length === 0) {
      fieldValues.teamBgnDate = null;
      fieldValues.teamEndDate = null;
    } else {
      fieldValues.teamBgnDate = fieldValues.teamDate[0].format('X') * 1000;
      fieldValues.teamEndDate = fieldValues.teamDate[1].format('X') * 1000;
    }
    delete fieldValues.teamDate;

    delete fieldValues.extTrainPositionId;

    if (!fieldValues.rankDate || fieldValues.rankDate.length === 0) {
      fieldValues.rankBgnDate = null;
      fieldValues.rankEndDate = null;
    } else {
      fieldValues.rankBgnDate = fieldValues.rankDate[0].format('X') * 1000;
      fieldValues.rankEndDate = fieldValues.rankDate[1].format('X') * 1000;
    }
    delete fieldValues.rankDate;
    
    let reqBody = {
      data: fieldValues
    }
    return reqBody;
  }

  // 证件号码失去焦点时
  identityCodeOnBlur = () => {
    let identityCode = this.props.form.getFieldValue('identityCode');
    var resp = identityCodeValid(identityCode);
    if (resp) {
      message.error(resp);
    } else {
      this.checkIsExist(identityCode);
      let { form } = this.props;
      let birthday = getBirthdayFromIdentityCode(identityCode);
      let sex = getSexFromIdentityCode(identityCode);
      let age = getAgeFromIdentityCode(identityCode);
      form.setFieldsValue({birthday: moment(birthday, "YYYY-MM-DD"), sex: sex, age: age})
    }
  }

  // 根据身份证判断员工是否已存在系统里
  checkIsExist = (identityCode) => {
    let reqBody = {data: {identityCode: identityCode}};
    this.props.dispatch({
      type: 'personModels/checkIdentityCode',
      payload: reqBody,
    }).then(() => {
      let data = this.props.personModels.checkExistResp.data;
      if (data.length > 0) {
        this.showConfirm(identityCode, data[0].id);
      }
    }).catch(() => message.error('网络异常，请稍后重试'))
  }

  // 是否跳转到编辑页面
  showConfirm = (identityCode, personId) => {
    Modal.confirm({
      title: '是否跳转到编辑页面？',
      content: `已存在身份证号为 ${identityCode} 的(在职或离职)员工`,
      onOk: () => this.jumpToEdit(personId),
      onCancel() {},
    });
  }

  jumpToEdit = (personId) => {
    this.props.dispatch(routerRedux.push({pathname: '/person/editPerson', personId: personId}))
  }

  validateFieldOnlineDate = (rule, value, callback) => {
    const { getFieldsValue } = this.props.form;
    let {onlineDate,trainBgnAndEndDate} = getFieldsValue(['onlineDate','trainBgnAndEndDate']);
    let [trainBgnAndEndDate1,trainBgnAndEndDate2] = trainBgnAndEndDate
    console.log(onlineDate)
    if(onlineDate){
        if(!trainBgnAndEndDate1||!trainBgnAndEndDate2){
          callback('上线日期存在时，培训期不能为空')
        }
        if(onlineDate.isBefore(trainBgnAndEndDate2)){
          callback('上线日期必须晚于培训期')
        }
    }
   
    callback()
  }
  // 校验身份证号
  validateIdentityCode = (rule, value, callback) => {
    const {form} = this.props
    let  identityCode= value.trim()
    if(identityCode.charAt(identityCode.length - 1)=='x') identityCode=identityCode.substring(0,identityCode.length-1)+'X'
    form.setFieldsValue({"identityCode":identityCode});
    let resp = identityCodeValid(identityCode);
    if (resp) {
      callback(resp);
    }
    callback();
  }

  // 选择内荐时，内荐人必填
  isRecommendedChange = (value) => {
    if (value === "1") {
      this.setState({
        isInternalRecommendation: true,
      }, () => {
        this.props.form.validateFields(['recommendPerson'], {force: true})
      })
    } else {
      this.setState({
        isInternalRecommendation: false,
      }, () => {
        this.props.form.validateFields(['recommendPerson'], {force: true})
      })
    }
  }

  // 根据团队显示类别
  groupIdChange = (value) => {
    let groupNames = this.props.personModels.groupNames.data || [];
    let groupName = "";
    for (let i = 0; i < groupNames.length; i++) {
      if (groupNames[i].id === value) {
        groupName = groupNames[i].groupName;
        let category = groupNames[i].isFirstCity === 1 ? "一类" : "非一类";
        this.props.form.setFieldsValue({category: category})
      }
    }
    console.log(value)
    // 根据团队查询组别列表
    if (value) {
      let reqBody = {
        data: {groupId: String(value)},
      }
      this.props.dispatch({
        type: 'personModels/selectTeamName',
        payload: reqBody,
      })
    }
    if (value) {
      this.setState({
        isShowTeamInfo: true,
      })
    } else {
      this.setState({
        isShowTeamInfo: false,
      })
    }
     // 职位信息
     const{form,personModels}=this.props
     this.props.dispatch({
       type: 'personModels/selectPositionName',
       payload: { groupId: value },
     }).then(()=> form.setFieldsValue({trainPositionId: ''}))
  }
  onChange0= (value) => {
    let trainBgnAndEndDate = this.props.form.getFieldValue('trainBgnAndEndDate')
    trainBgnAndEndDate[0]=value
    this.props.form.setFieldsValue({trainBgnAndEndDate})
   
  }

   onChange1= (value) => {
    let trainBgnAndEndDate = this.props.form.getFieldValue('trainBgnAndEndDate')
    trainBgnAndEndDate[1]=value
    this.props.form.setFieldsValue({trainBgnAndEndDate})
  }

  // 根据岗位查询职业
  handlePositionChange = (value) => {
    console.log('handlePositionChange..');
    if (value) {
      let reqBody = {
          data:{positionId: String(value)}
      }
      this.props.dispatch({
        type: "personModels/selectRankName",
        payload: reqBody,
      })
      this.setState({ isShowRank: true })
    } else {
      this.setState({ isShowRank: false })
    }
  }

  handleConfirmTrainBgnAndEndDate = (rule, value, callback) => {
    const { getFieldValue } = this.props.form;

    // 删除此管控，uat测试中发现有该情况的员工
    // const entryDate =  getFieldValue('entryDate')
    // if (entryDate==null|| value[0] <entryDate) {
    //     callback('起始时间要大于入职时间')
    // }

    // Note: 必须总是返回一个 callback，否则 validateFieldsAndScroll 无法响应
    callback()
}

  render() {

    const { loading, personModels, form } = this.props;
    const { getFieldDecorator } = form;

    // 外包公司下拉列表
    let outsourceIdChild = [];
    let outsourceId = personModels.outsourceId.data || [];
    for (let i = 0; i < outsourceId.length; i++) {
      outsourceIdChild.push(<Option key={outsourceId[i].id} value={outsourceId[i].id}>{outsourceId[i].outSourceName}</Option>)
    }
    
    // 岗位下拉列表
    let positionNameChild = [];
    let positionNames = personModels.positionNames.data || [];
    for (let i = 0; i < positionNames.length; i++) {
      positionNameChild.push(<Option key={positionNames[i].id} value={positionNames[i].id}>{positionNames[i].positionName}</Option>)
    }

    // 团队下拉列表
    let groupNames = personModels.groupNames.data || [];
    let groupChildren = [];
    for (let i = 0; i < groupNames.length; i++) {
      groupChildren.push(<Option key={groupNames[i].id} value={groupNames[i].id}>{groupNames[i].groupName}</Option>)
    }

    // 员工详细信息
    let personDetail = personModels.personDetail.data;
    let teamInfos = [];
    if (personDetail) {
      teamInfos = personDetail.teamInfoDto;
    }

    // 组别下拉列表
    let teamNames = personModels.teamNames.data || [];
    let teamChildren = [];
    if (this.state.isShowTeamInfo) {
      for (let i = 0; i < teamNames.length; i++) {
        teamChildren.push(<Option key={teamNames[i].id} value={teamNames[i].id}>{teamNames[i].teamName}</Option>)
      }
    }

    // 职级下拉列表
    let rankChildren = [];
    if (this.state.isShowRank) {
      let rankNames = personModels.rankNames.data || [];
      for (let i = 0; i < rankNames.length; i++) {
        rankChildren.push(<Option key={rankNames[i].id} value={rankNames[i].id}>{rankNames[i].rankName}</Option>)
      }
    }

   
    return (
      <PageHeaderLayout>
        <Form>
          <Card title="员工基本信息">
          
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="员工姓名"> 
                  {getFieldDecorator('empName',
                    {initialValue: '',
                      rules: [{required: true, message: "谁没有姓名"}]})(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="证件号码">
                  {getFieldDecorator('identityCode',
                    {initialValue: '',
                      rules: [{required: true, message: "谁没有身份证"}, {validator: this.validateIdentityCode}]})(
                    <Input onBlur={this.identityCodeOnBlur} placeholder='请输入'/>
                    // <Input  placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="出生年月">
                  {getFieldDecorator('birthday')(
                    <DatePicker style={{width: '100%'}} disabled></DatePicker>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="性别">
                  {getFieldDecorator('sex')(
                    <Select placeholder="请选择" disabled>
                      <Option value="1">男</Option>
                      <Option value="2">女</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="年龄">{/*no*/}
                  {getFieldDecorator('age')(
                    <Input disabled placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="入职日期">
                  {getFieldDecorator('entryDate', {initialValue: null, rules: [{required: true, message: "必填"}]})(
                    <DatePicker style={{width: '100%'}} ></DatePicker>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="电话号码">
                  {getFieldDecorator('mobile', {initialValue: '',
                      rules: [{min: 8, message: '最短8位'}, {max: 11, message: '最长11位'}, {required: true, message: "必填"}]})(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="邮箱地址">
                  {getFieldDecorator('email', {initialValue: '',
                      rules: [{type: 'email', message: '邮箱格式不正确'}]})(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="银行名称">
                  {getFieldDecorator('bankName')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="银行账号">
                  {getFieldDecorator('bankNo')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="社保账户">
                  {getFieldDecorator('socialAccount')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="公积金账户">
                  {getFieldDecorator('accumulationAccount')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="外勤工号">
                  {getFieldDecorator('employeeCode')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="内勤工号">
                  {getFieldDecorator('officeEmployeeCode')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="所在团队">
                  {getFieldDecorator('groupId', {
                      rules: [{required: true, message: "必选"}]})(
                    <Select placeholder="请选择" onChange={this.groupIdChange}>
                      {groupChildren}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="进入团队时间">
                  {getFieldDecorator('changeGroupDate', {
                      rules: [{required: true, message: "必选"}]})(
                   <DatePicker  width={{width:'100%'}}/>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="类别">
                  {getFieldDecorator('category')(//不用传到后台
                    <Input placeholder='请输入' disabled/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="是否薪酬保护">
                  {getFieldDecorator('isSalaryProtect', {
                      rules: [{required: true, message: "必选"}]})(
                    <Select placeholder="请选择">
                      <Option value="1">是</Option>
                      <Option value="0">否</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="外包公司名称">
                  {getFieldDecorator('outsourceId', {
                      rules: [{required: true, message: "必选"}]})(
                    <Select placeholder="请选择">
                      {outsourceIdChild}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="培训岗位">
                  {getFieldDecorator('trainPositionId', {
                      rules: [{required: true, message: "必选"}]})(
                    <Select placeholder="请选择">
                      {positionNameChild}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="是否为内荐">
                  {getFieldDecorator('isRecommended', {
                      rules: [{required: true, message: "必选"}]})(
                    <Select placeholder="请选择" onChange={this.isRecommendedChange}>
                      <Option value="1">是</Option>
                      <Option value="0">否</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="内荐人">
                  {getFieldDecorator('recommendPerson', {rules: [{required: this.state.isInternalRecommendation, message: "必填"}]})(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="紧急联系人">
                  {getFieldDecorator('emergencyPerson')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="紧急联系人电话号码">
                  {getFieldDecorator('emergencyTel')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="家庭住址">
                  {getFieldDecorator('homeAddress')(
                    <Input placeholder='请输入'/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="在职状态">
                  {getFieldDecorator('status', {
                      rules: [{required: true, message: "必选"}]})(
                    <Select placeholder="请选择">
                      <Option value="1">培训期</Option>
                      <Option value="2">试用期</Option>
                      <Option value="3">已转正</Option>
                      <Option value="4">已离职</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="离职日期">
                  {getFieldDecorator('leaveDate')(
                    <DatePicker style={{width: '100%'}}></DatePicker>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="培训期">
                  {getFieldDecorator('trainBgnAndEndDate', {initialValue:[null,null]
                  //     rules: [{required: true,message: "必填"},{
                  //       validator: this.handleConfirmTrainBgnAndEndDate,message:'起始时间必须大于入职时间'
                  // }]
                })(
                  <div>
                  <DatePicker onChange={this.onChange0} value={this.props.form.getFieldValue('trainBgnAndEndDate')[0]} style={{ width: '48%' }}></DatePicker>
                ~
                <DatePicker onChange={this.onChange1} value={this.props.form.getFieldValue('trainBgnAndEndDate')[1]} style={{ width: '48%' }}></DatePicker>
                </div>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="上线日期">
                  {getFieldDecorator('onlineDate',{rules: [ {
                        validator: this.validateFieldOnlineDate
                      }]})(
                    <DatePicker style={{width: '100%'}}></DatePicker>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="转正时间">
                  {getFieldDecorator('formalDate')(
                    <DatePicker style={{width: '100%'}}></DatePicker>
                  )}
                </Form.Item>
              </Col>
            </Row>

           

            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="人力区间">
                  {getFieldDecorator('manpowerInterval')(//新增编辑都不用传到后台
                    <Input placeholder='请输入' disabled/>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="上线天数">
                  {getFieldDecorator('onlineDays')(//新增编辑者不用传到后台
                    <Input placeholder='请输入' disabled/>
                  )}
                </Form.Item>
              </Col>
            </Row>

          </Card>

          <Card title="组别信息" style={{marginTop: 10}}>
            {/* <TeamInfo groupId={this.state.groupId} groupName={this.state.groupName}
              isShowTeamInfo={this.state.isShowTeamInfo}
              saveTeamInfoMessage={(dataSource) => this.setState({ teamInfoDataSource: dataSource })}>
            </TeamInfo> */}
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="组别名称">
                  {getFieldDecorator('teamId')(
                    <Select placeholder="请选择">
                      {teamChildren}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="挂靠起止时间">
                  {getFieldDecorator('teamDate')(
                    <RangePicker></RangePicker>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Card title="职级信息" style={{marginTop: 10}}>
              {/* <PositionAndRankMessage savePositionAndRankMessage={(dataSource) => this.setState({ positionAndRankDataSource: dataSource })}/> */}
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="岗位">{/*no*/}
                  {getFieldDecorator('extTrainPositionId')(
                    <Select placeholder="请选择" onChange={this.handlePositionChange}>
                      {positionNameChild}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="职级">
                  {getFieldDecorator('rankId')(
                    <Select placeholder="请选择">
                      {rankChildren}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col md={12} sm={24}>
                <Form.Item {...formItemLayout} label="有效起止时间">
                  {getFieldDecorator('rankDate')(
                    <RangePicker></RangePicker>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Card>
          <div style={{textAlign: 'center', marginTop: 20}}>
            <Button loading={loading} type="primary" onClick={this.handleSubmit}>
              提交
            </Button>
          </div>
        </Form>

        <Modal>

        </Modal>
      </PageHeaderLayout>
    )
  }
}