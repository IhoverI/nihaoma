
import styles from './RankRecordAudit.less'
import { Table, Form, Row, Col, Input, Button, Icon, Select, DatePicker, message, Card } from 'snk-web';
import HttpUtils from '../../utils/HttpUtils';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';


const FormItem = Form.Item;

class AdvancedSearchForm extends React.Component {
    state = {
        groupNames: [],
    };

    componentDidMount() {
        HttpUtils.post({
            url: '/emp/selectGroupName',
        }).then((response) => {
            console.log(response)
            this.setState(
                {
                    groupNames: response.data,
                }
            )
        });
    }


    handleSearch = (e) => {
        if (e) e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (err) return
            this.props.setSeachPara(values)
        });
    }
   


    getFields() {
        const { getFieldDecorator } = this.props.form;
        const children = [];
        children.push(
            <Col span={6} key='groupId' style={{ display: 'block' }}>
                <FormItem label='团队名称'>
                    {getFieldDecorator('groupId', {
                        // initialValue: this.state.groupNames[0] ? String(this.state.groupNames[0].id) : '',
                    })(
                        <Select placeholder="请输入团队名称" allowClear={true} >
                            {this.state.groupNames.map((group) => <Select.Option key={group.id}>{group.groupName}</Select.Option>)}
                        </Select>
                    )}
                </FormItem>
            </Col>
        );
        children.push(
            <Col span={6} key='empName' style={{ display: 'block' }}>
                <FormItem label='员工姓名'>
                    {getFieldDecorator('empName',)(
                      <Input/>
                    )}
                </FormItem>
            </Col>
        );
        children.push(
            <Col span={6} key='identityCode' style={{ display: 'block' }}>
                <FormItem label='身份证'>
                    {getFieldDecorator('identityCode',)(
                      <Input/>
                    )}
                </FormItem>
            </Col>
        );
        children.push(
            <FormItem key='submet'>
                <Button type="primary" htmlType="submit"> 搜索 </Button>
              
            </FormItem>
        )
        return children;
    }

    render() {
        return (
            <Form
                className={styles.serchForm}
                onSubmit={this.handleSearch}
            >
                <Row gutter={24}>{this.getFields()}
                </Row>

            </Form>
        );
    }
}

const WrappedAdvancedSearchForm = Form.create()(AdvancedSearchForm);

@connect(({ personModels, loading }) => ({
    personModels,
    loading: loading.models.personModels,
  }))
export default class RankApproved extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            loading: false,
            rankList: [],
            options: null,
            pagination: {
                pageNum: 1, pageSize: 10, showSizeChanger: true, showQuickJumper: true,
            },
            searchPara:null
        };
        this.columns =
            [ {
                title: '序号',
                dataIndex: 'record',
                width:20,
                render: (row, record, index) => index + 1
            },{
                title: '姓名',
                width:60,
                dataIndex: 'empName',
            }, {
                title: '身份证',
                width:100,
                dataIndex: 'identityCode',
            }, {
                title: '团队',
                width:60,
                dataIndex: 'groupName',
            },
            {
                title: '岗位',
                width:60,
                dataIndex: 'positionName',
            }, {
                title: '职级',
                width:100,
                render:row=>row.oldRankName?`${row.oldRankName}->${row.rankName}`:row.rankName
            }, {
                title: '职级有效期',
                width:120,
                render: (row,record,index)=>`${record.bgnDate}~${record.endDate}`,
            }, {
                title: '状态',
                width:20,
                dataIndex:'status',
                render: row=>row==1?'无效':'有效',
            }, {
                title:'操作',
                width:20,
                dataIndex:'id',
                render:id=><a  onClick={()=>{this.audit(id)}}>审核</a>
            }
        ]
    }
    audit=(id)=>{
        console.log(this.props)
        this.props.dispatch({
            type: 'personModels/auditRankRecord',
            payload: {data:{id}},
          }).then(()=>{
            let flag = this.props.personModels.auditRankRecord;
            if (flag) {
              message.success('审核成功');
              let newdata = this.state.data
              let  data = newdata.filter(e=>e.id!==id)
              this.setState({data})
            } else {
              message.error('审核失败')
            }
          })
    }


    seriesAudit=()=>{
        HttpUtils.post({
            url: 'emp/seriesAudit',
            body: { data: this.state.selectedRowKeys }
        }).then((response) => {
            message.info('审核成功')
            this.setState(
                { selectedRowKeys: [],loading: false},()=>{this.fetch()}
            )
        }).catch(e => {
            console.log(e)
            message.error(e.message)
            this.setState(
                {
                    loading: false,
                })
        });
    }
    fetch = () => {
        this.setState({ loading: true });
        const response = HttpUtils.post({
            url: 'emp/findRankList',
            body: { data: this.state.searchPara }
        }).then((response) => {
            this.setState(
                {
                    data: response.data.list,
                    loading: false,
                    total: response.data.total,
                }
            )
        }).catch(e => {
            console.log(e)
            message.error(e.message)
            this.setState(
                {
                    loading: false,
                })
        });
    }


    setSeachPara=(searchPara)=>{
        let pagination = this.state.pagination
        pagination.pageNum=1
        this.setState({searchPara,pagination},()=>this.fetch())
    }

    pageOnchange=(pagination)=>{
            pagination.pageNum=pagination.current
            this.setState({pagination})
            this.fetch();
    }
    onSelectChange = (selectedRowKeys) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
    }

    render() {
        const { loading, selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        }
        return (
            <PageHeaderLayout>
            <Card >
               <Card>
                <WrappedAdvancedSearchForm  setSeachPara={this.setSeachPara}
                  />
               </Card>
               
                <div className={styles.serchForm}>
                    <Table bordered
                        columns={this.columns}
                        rowKey={record => record.id}
                        dataSource={this.state.data}
                        rowSelection={rowSelection}
                        loading={this.state.loading}
                        pagination={this.state.pagination}
                        onChange={this.pageOnchange}
                        title={()=>{
                            return <Button type="primary" onClick={this.seriesAudit}  loading={this.state.loading} > 批量审核  </Button>  }}
                    />
                </div>
            </Card>
             </PageHeaderLayout>
        );
    }
}