import React, { PureComponent } from 'react';
import { getTeamColumns } from './PersonColumns';
import { Form, Input, Row, Col, Select, Button, message, DatePicker, Table, Modal, Popconfirm } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';
import { getRankInfoColumns } from './PersonColumns';

const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;
const formItemLayout = {
  labelCol: {
    sm: { span: 7 },
  },
  wrapperCol: {
    sm: { span: 15 },
  },
};

@connect(({ personCustomerModels, loading }) => ({
  personModels:personCustomerModels,
  loading: loading.models.personModels,
}))
@Form.create()
export default class PositionAndRankCustomerMessage extends PureComponent {

  state = {
    empId: {},
    editVisible: false,
    addVisible: false,
    rankDataSource: [],
    groupChildren: [],
    teamChildren: [],
    rankChildren: [],
    updateRankRecord: {},
    ifHasAuditRole:false
  }

  componentDidMount = () => {
    // this.setState({
    //   groupChildren: this.props.groupChildren,
    //   rankDataSource: this.props.rankDataSource,
    //   empId: this.props.empId,
    // })
    // console.log('componentDidMount');
    this.props.dispatch({
      type: 'personCustomerModels/checkIfHasAuditRole',
      payload: null,
    }).then(() => {
      var flag = this.props.personModels.ifHasAuditRole;
      console.log(flag)
      this.setState({ifHasAuditRole:flag})

     
    })
  }

  // 编辑职级
  edit = (record) => {
    this.setState({ editVisible: true, updateRankRecord: record})
  }

  // 编辑确定
  editHandleOk = () => {
    this.props.form.validateFieldsAndScroll(['etrainPositionId','erankId','erankDate'],(err, fieldValues) => {
      fieldValues.id = this.state.updateRankRecord.id;
      if (err) {
        return;
      }
      if (!fieldValues.erankDate || fieldValues.erankDate.length === 0) {
        fieldValues.bgnDate = null;
        fieldValues.endDate = null;
      } else {
        fieldValues.bgnDate = (fieldValues.erankDate[0].format('X') * 1000).toString();
        fieldValues.endDate = (fieldValues.erankDate[1].format('X') * 1000).toString();
      }
      delete fieldValues.erankDate;
      delete fieldValues.etrainPositionId;
      delete fieldValues.erankId;
      fieldValues.empId = this.props.empId
      console.log('fieldValues', fieldValues);

      // 请求后台
      let reqBody = {
        data: fieldValues
      }
      this.props.dispatch({
        type: 'personCustomerModels/updateRankRecord',
        payload: reqBody,
      }).then(() => {
        let resp = this.props.personModels.updateRankRecordResp;
        if (resp.code === 0) {
          message.success('更新成功');
          this.setState({ editVisible: false })
          this.updateTeamDataSource()
        } else {
          message.error('更新失败')
        }
      })
    })
  }

  // 编辑取消
  editHandleCancle = () => {
    this.setState({ editVisible: false })
  }

  // 添加组别
  handleAdd = () => {
    this.props.form.resetFields()
    this.setState({ addVisible: true })
  }

  // 添加确定
  addHandleOk = () => {
    this.props.form.validateFieldsAndScroll(['atrainPositionId','arankId','arankDate'],(err, fieldValues) => {
      fieldValues.empId = this.props.empId;
      if (err) {
        return;
      }
      if (!fieldValues.arankDate || fieldValues.arankDate.length === 0) {
        fieldValues.bgnDate = null;
        fieldValues.endDate = null;
      } else {
        fieldValues.bgnDate = (fieldValues.arankDate[0].format('X') * 1000).toString();
        fieldValues.endDate = (fieldValues.arankDate[1].format('X') * 1000).toString();
      }
      fieldValues.rankId = fieldValues.arankId.toString();
      delete fieldValues.arankDate;
      delete fieldValues.atrainPositionId;

      // 请求后台
      let reqBody = {
        data: fieldValues
      }
      this.props.dispatch({
        type: 'personCustomerModels/insertRankRecord',
        payload: reqBody,
      }).then(() => {
        let resp = this.props.personModels.insertRankRecordResp;
        if (resp.code === 0) {
          message.success('保存成功');
          this.setState({ addVisible: false })
          this.updateTeamDataSource();
        } else {
          message.error(resp.message);
        }
      })
    })
  }

  // 刷新职级信息
  updateTeamDataSource = () => {
    let reqBody = {
      data: {
        id: this.props.empId,
      }
    }
    this.props.dispatch({
      type: 'personCustomerModels/findCustomerPersonnel',
      payload: reqBody,
    }).then(()=>{
      // this.setState({
      //   rankDataSource: this.props.personModels.updateRankDataSourceResp.data.rankInfoDto,
      // })
    })
  }

  // 添加取消
  addHandleCancle = () => {
    this.setState({ addVisible: false })
  }

  // 删除职级
  handleDelete = (record) => {
    let reqBody = {
      data: {
        id: record.id.toString(),
      }
    }

    this.props.dispatch({
      type: 'personCustomerModels/deleteRankRecord',
      payload: reqBody,
    }).then(()=>{
      let resp = this.props.personModels.deletePersonnelResp;
      if (resp.code === 0) {
        message.success('删除成功');
        this.updateTeamDataSource()
        // let rankDataSource = this.state.rankDataSource;
        // this.setState({ rankDataSource: rankDataSource.filter(item => item.id !== record.id) });
      } else {
        message.error('删除失败')
      }
    })
    let rankDataSource = this.props.rankDataSource;
    this.setState({ rankDataSource: rankDataSource.filter(item => item.id !== record.id) });
  }

  
     // 职级审核
     audit = (record) => {
       if(record.status==0){
        message.info('无需审核');
        return;
       } 
      let reqBody = {
        data: {
          id: record.id.toString(),
        }
      }
      this.props.dispatch({
        type: 'personCustomerModels/auditRankRecord',
        payload: reqBody,
      }).then(()=>{
        let flag = this.props.personModels.auditRankRecord;
        if (flag) {
          message.success('审核成功');
          this.updateTeamDataSource()
        } else {
          message.error('审核失败')
        }
      })
      let rankDataSource = this.props.rankDataSource;
      this.setState({ rankDataSource: rankDataSource.filter(item => item.id !== record.id) });
    }
  // 根据岗位查询职级
  handlePositionChange = (value) => {
    console.log('handlePositionChange..');
    if (value) {
      let reqBody = {
        data: {positionId: value},
     }
      this.props.dispatch({
        type: 'personCustomerModels/selectRankName',
        payload: reqBody,
      }).then(() => {
          let rankNames = this.props.personModels.rankNames.data || [];
          let rankChildren = [];
          for (let i = 0; i < rankNames.length; i++) {
            rankChildren.push(<Option key={rankNames[i].id} value={rankNames[i].id}>{rankNames[i].rankName}</Option>)
          }
          this.setState({ rankChildren: rankChildren });
          this.props.form.resetFields('rankId')
          console.log('handlePositionChange');
          console.log(rankChildren);
        })
    } else {
      console.log('handlePositionChange');
      this.setState({ rankChildren: [], });
    }
  }

  render() {
    const { loading, form } = this.props;
    const { getFieldDecorator } = form;

    return (
      <div>
        <Table
          columns={getRankInfoColumns(this)}
          dataSource={this.props.rankDataSource}
          bordered={true}
          pagination={false}
          loading={loading}>
        </Table>
        <Button onClick={this.handleAdd} type="primary" icon="plus" style={{marginTop: 10}}>新增职级记录</Button>
        <Modal
          title="编辑职级"
          visible={this.state.editVisible}
          onOk={this.editHandleOk}
          onCancel={this.editHandleCancle}
          destroyOnClose={true}
          confirmLoading={loading}
          maskClosable={false}
          closable={false}
          okText="更新"
          >
          <Form>
            <Form.Item {...formItemLayout} label="岗位">
              {getFieldDecorator('etrainPositionId', {initialValue: this.state.updateRankRecord.positionName})(
                <Select placeholder="请选择" disabled onChange={this.handlePositionChange}>
                  {this.props.positionNameChild}
                </Select>
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="职级">
              {getFieldDecorator('erankId', {initialValue: this.state.updateRankRecord.rankName})(
                <Select placeholder="请选择" disabled>
                  {this.state.rankChildren}
                </Select>
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="有效起止时间">
              {getFieldDecorator('erankDate', {initialValue: 
                      [parseInt(this.state.updateRankRecord.bgnDate) ? moment(parseInt(this.state.updateRankRecord.bgnDate)) : null, 
                        parseInt(this.state.updateRankRecord.endDate) ? moment(parseInt(this.state.updateRankRecord.endDate)) : null],
                        rules: [{required: true, message: "必填"}]})(
                <RangePicker></RangePicker>
              )}
            </Form.Item>
          </Form>
        </Modal>

        <Modal
          title="新增职级"
          visible={this.state.addVisible}
          onOk={this.addHandleOk}
          onCancel={this.addHandleCancle}
          destroyOnClose={true}
          confirmLoading={loading}
          maskClosable={false}
          closable={false}
          okText="保存"
          >
          <Form>
            <Form.Item {...formItemLayout} label="岗位">{/*no*/}
              {getFieldDecorator('atrainPositionId', {initialValue: null,rules: [{required: true, message: '必选'}]})(
                <Select placeholder="请选择" onChange={this.handlePositionChange}>
                  {this.props.positionNameChild}
                </Select>
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="职级">
              {getFieldDecorator('arankId',{initialValue: null,rules: [{required: true, message: '必选'}]})(
                <Select placeholder="请选择">
                  {this.state.rankChildren}
                </Select>
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="有效起止时间">
              {getFieldDecorator('arankDate',{initialValue: [null,null],rules: [{required: true, message: '必选'}]})(
                <RangePicker></RangePicker>
              )}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    )
  }
}