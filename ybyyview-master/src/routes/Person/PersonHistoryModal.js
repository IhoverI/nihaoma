import React, { PureComponent } from 'react';
import { Form, Input, Row, Col, Select, Button, message, DatePicker, Table } from 'snk-web';
import { connect } from 'dva';
import moment, { isMoment } from 'moment';
import { isDate } from 'util';

const FormItem = Form.Item;
const Option = Select.Option

@connect(({ personModels, loading }) => ({
  personModels,
  loading: loading.models.personModels,
}))
@Form.create()
export default class PersonHistoryModal extends PureComponent {

  columns = [
    {
      title: '序号',
      dataIndex: 'key',
      align: 'center',
      render: (text, row, index) => {
        return index + 1
      }
    },
    {
      title: '员工轨迹id',
      dataIndex: 'id',
      align: 'center',
    },
    {
      title: '员工姓名',
      dataIndex: 'empName',
      align: 'center',
    },
    {
      title: '身份证号码',
      dataIndex: 'identityCode',
      align: 'center',
    },
    {
      title: '生日',
      dataIndex: 'birthday',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },

    {
      title: '性别',
      dataIndex: 'sex',
      align: 'center',
      render:(text)=>{
        return text==1?'男':'女'
      }
    },
    {
      title: '入司日期',
      dataIndex: 'entryDate',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },
    {
      title: '联系电话',
      dataIndex: 'mobile',
      align: 'center',
    },
    {
      title: '邮箱地址',
      dataIndex: 'email',
      align: 'center',
    },
    {
      title: '社保账号',
      dataIndex: 'socialAccount',
      align: 'center',
    },

    {
      title: '公积金账号',
      dataIndex: 'accumulationAccount',
      align: 'center',
    },
    {
      title: '派遣公司id',
      dataIndex: 'outsourceId',
      align: 'center',
    },
    {
      title: '培训岗位id',
      dataIndex: 'trainPositionId',
      align: 'center',
    },
    {
      title: '是否内荐',
      dataIndex: 'isRecommended',
      align: 'center',
    },

    {
      title: '推荐人',
      dataIndex: 'recommendPerson',
      align: 'center',
    },
    {
      title: '紧急联系人',
      dataIndex: 'emergencyPerson',
      align: 'center',
    },
    {
      title: '紧急联系人电话',
      dataIndex: 'emergencyTel',
      align: 'center',
    },
    {
      title: '家庭地址',
      dataIndex: 'homeAddress',
      align: 'center',
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      render: (text) => {
        // 1培训期2试用期3已转正4已离职
        if (text == 1) {
          return '培训期'
        } else if (text == 2) {
          return '试用期'
        } else if (text == 3) {
          return '已转正'
        } else if (text == 4) {
          return '已离职'
        }
        return text
      }
    },
    {
      title: '离职日期',
      dataIndex: 'leaveDate',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },
    {
      title: '培训开始日期',
      dataIndex: 'trainBgnDate',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },
    {
      title: '培训结束日期',
      dataIndex: 'trainEndDate',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },
    {
      title: '上线日期',
      dataIndex: 'onlineDate',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },
    {
      title: '转正时间',
      dataIndex: 'formalDate',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },

    {
      title: '工号',
      dataIndex: 'employeeCode',
      align: 'center',
    },
    {
      title: '数据创建人',
      dataIndex: 'createdUser',
      align: 'center',
    },
    {
      title: '数据更新人',
      dataIndex: 'updatedUser',
      align: 'center',
    },
    {
      title: '数据创建时间',
      dataIndex: 'createdTime',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },
    {
      title: '数据更新时间',
      dataIndex: 'updatedTime',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },

    {
      title: '备注',
      dataIndex: 'remark',
      align: 'center',
    },
    {
      title: '团队id',
      dataIndex: 'groupId',
      align: 'center',
    },
    {
      title: '进入团队时间',
      dataIndex: 'changeGroupDate',
      align: 'center',
      render: (text) => {
        const tmp = moment(Number(text))
        return tmp.isValid() ? tmp.format('YYYY-MM-DD') : '未设置'
      }
    },
  ]

  state = {
    pageNum: 1,
    pageSize: 10,
    record: {},
  }

  componentWillMount() {

    let record = this.props.record;
    this.setState({
      pageNum: 1,
      pageSize: 10,
      record: record,
    }, () => this.searchData(1, 10))

  }

  // 搜索员工历史
  searchData = (pageNum, pageSize) => {
    let { dispatch } = this.props;
    let reqBody = {
      data: {
        pageNum: pageNum,
        pageSize: pageSize,
        empId: this.state.record.id,
      },
    }
    dispatch({
      type: 'personModels/findPersonnelHisInfo',
      payload: reqBody,
    });
  }

  // 分页
  handleTablePage = (pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    }, () => {
      this.searchData(pagination.current, pagination.pageSize);
    });
  }

  render() {

    const { loading, personModels } = this.props;
    const data = personModels.personChange.data || {};
    let dataList = data.list || [];
    console.log(dataList)

    return (
      <Table
        columns={this.columns}
        dataSource={dataList}
        size='small'
        bordered={true}
        rowKey={(record, index) => index}
        scroll={{ x: 3000 }}
        onChange={this.handleTablePage}
        loading={loading}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
          current: this.state.pageNum,
          pageSize: this.state.pageSize,
          total: data.total,
          pageSizeOptions: ['10', '20', '30', '40', '50'],
        }}
      >
      </Table>
    )
  }
}