import React, { PureComponent } from 'react';
import { Form, Input, Row, Col, Select, Button, message, DatePicker, Table } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';

const FormItem = Form.Item;
const Option = Select.Option

@connect(({ personModels, loading }) => ({
  personModels,
  loading: loading.models.personModels,
}))

@Form.create()
export default class EditTeamMemberModal extends PureComponent {

  columns = [
    {
      title: '员工姓名',
      dataIndex: 'empName',
      align: 'center',
    },
    {
      title: '工号',
      dataIndex: 'employeeCode',
      align: 'center',
    },
    {
      title: '组别',
      dataIndex: 'teamName',
      align: 'center',
    },
  ]

  state = {
    pageNum: 1,
    pageSize: 10,
    record: {},
  }

  componentWillMount() {
    
    let record = this.props.record;
    this.setState({
      pageNum: 1,
      pageSize: 10,
      record: record,
    }, () => this.searchData(1, 10))
    
  }
  
  // 搜索员工历史
  searchData = (pageNum, pageSize) => {
    let { dispatch } = this.props;
    let reqBody = {
      data: {
        pageNum: pageNum,
        pageSize: pageSize,
        empId: this.state.record.id,
      },
    }
    dispatch({
      type: 'personModels/findPersonnelHisInfo',
      payload: reqBody,
    });
  }

  // 分页
  handleTablePage =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current, pagination.pageSize);
    });
  }

  render() {

    const { loading, personModels } = this.props;
    const data = personModels.personChange.data || {};
    let dataList = data.list || [];

    return (
      <Table
        columns={this.columns}
        dataSource={dataList}
        size='small'
        bordered={true}
        rowKey={(record, index) => index}
        scroll={{x: 3000}}
        onChange={this.handleTablePage}
        loading={loading}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
          current:this.state.pageNum,
          pageSize:this.state.pageSize,
          total:data.total,
          pageSizeOptions:['10','20','30','40','50'],
        }}
        >
      </Table>
    )
  }
}