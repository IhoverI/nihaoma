import { 
  Table, 
  Checkbox,
  Popconfirm,
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider} from 'snk-web';
import React,{ PureComponent, Fragment } from 'react';
import { routerRedux } from 'dva/router';
import { SelectInfo } from './SearchGroup.js';
import {PermissionWrapper,PermissinSelectOption} from 'snk-sso-um';
import moment from 'moment';
import { connect } from 'dva';
import styles from './SearchGroup.less';
//import ClmForm from '../../components/ClmForm';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import GroupAndTeamInfo from '../../components/Common/GroupAndTeamInfo'
import SearchResult from '../../components/Common/SearchResult'
import { isArray } from 'util';


/*
 * 新增团队弹出框
*/
const CreateGroupForm = Form.create()(props => {
   const { groupVisible, form, handleAddGroup, handleGroupVisible} = props;
   const okHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleAddGroup(fieldsValue);
      });
    };
  
  return (
    <Modal
      title="新增团队"
      visible={groupVisible}
      onOk={okHandle}
      onCancel={() => handleGroupVisible(false)}
    >
     <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="团队名称" >
        {form.getFieldDecorator('groupName', {
          rules: [{ required: true, message: '团队名称不能为空' }],
        })
        (<Input placeholder="团队名称" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别属性">
         {form.getFieldDecorator('isFirstCity', {initialValue: '1'})
            (<Select style={{ width: 180 }} >
              <Select.Option key={1}>一线城市</Select.Option>
              <Select.Option key={0}>非一线城市</Select.Option>
            </Select>)
          }
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="基本法">
         {form.getFieldDecorator('lawId', {initialValue: '2'})
              (<Select style={{ width: 180 }} >
                <Select.Option key='2'>创新非自建</Select.Option>
                <Select.Option key='1'>创新自建</Select.Option>
                {/*{lawOptions}*/}
              </Select>)
          }
      </FormItem>
    </Modal>
  );
});

/*
 * 新增组别弹出框
*/
const CreateRankForm = Form.create()(props => {
   const { rankVisible, form, handleAddRank, handleRankVisible} = props;
   const okHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleAddRank(fieldsValue);
      });
    };
  
  return (
    <Modal
      title="新增组别"
      visible={rankVisible}
      onOk={okHandle}
      onCancel={() => handleRankVisible(false)}
    >
     <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="组别名称" >
        {form.getFieldDecorator('rankName', {
          rules: [{ required: true, message: '组别名称不能为空' }],
        })
        (<Input placeholder="组别名称" type='text' />)}
     </FormItem>
    </Modal>
  );
});


const FormItem = Form.Item;

//table 行编辑的属性定义
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
const statusMap = ['生效', '待生效'];
const isFirstCityMap = [ '非一线','一线'];

//table 单元格的定义
class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (      
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [{
                      // required: true,
                      message: `请输入 ${title}!`,
                    }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>
               
          );
        }}
      </EditableContext.Consumer>
    );
  }

}



//装饰器
@connect(({umssouserinfo, groupModel, personModels,shareModel,loading}) => ({
  umssouserinfo,
  groupModel,
  personModels,
  shareModel,
  loading:loading.models.groupModel
}))


@Form.create()
export default class SearchGroup extends React.Component {
    state = {
      dataSource: [],
      groupVisible: false,
      rankVisible: false, 
      selectedRows: [],
      data: [],
      value: '',
      recommend: {},
      pageNum: 1,
      pageSize: 10,
      editingKey: '',
      groupData:[],
      groupId:'',
    };

    //构造table结构
    constructor(props){
      super(props);
      this.columns = [
        {
          title: '团队名称',
          dataIndex: 'groupName',
          width: '70px',
          editable: true,
        },
        {
          title: '详情',
          dataIndex: 'isFirstCity',
          width: '70px',
          render: (val, record) => {
            if(record.teamName){
                return <Badge status={record.teamName} text={record.teamName}  />;
            }else{
                return <Badge status={isFirstCityMap[val]} text={isFirstCityMap[val]} />;
            }
          },
        },  
        {
          title: '团队总监/组别领导',
          dataIndex: 'empName',
          width: '70px',
        },
        {
          title: '状态',
          dataIndex: 'status',
          width: '70px',
          render(val) {
            return <Badge status={statusMap[val]} text={statusMap[val]} />;
          },
        },
        {
          title: '基本法名称',
          dataIndex: 'lawName',
          width: '70px',
        },
        {
          title: '创建日期',
          dataIndex: 'createdTime',
          width: '70px',
          render(val){
            return <div> {moment(parseInt(val)).format("YYYY-MM-DD")} </div>
          },
        },
        {
          title: '操作',
          dataIndex: 'Name',
          editable: true,
          width: '70px',
          render: (status, record) => {
            const editable = this.isEditing(record);
            record.key = 'key' + Math.random() * 200;
            if (record.key.indexOf('team') > -1) {
              return (<div>
                {editable ? (
                  <span>
                    <EditableContext.Consumer>
                      {form => (
                        <a
                          href="javascript:;"
                          onClick={() => this.save(form, record.key, 'add')}
                          style={{ marginRight: 8 }}
                        >
                          保存
                        </a>
                      )}
                    </EditableContext.Consumer>
                    {/* <Popconfirm
                      title="确定取消吗"
                      onConfirm={() => this.addCancel(record.key)}
                    >
                    </Popconfirm> */}
                      <a onClick={()=>this.addCancel(record.key)}>取消</a>
                  </span>
                ) : (
                    <div>
                        <Popconfirm
                      title="确定删除吗"
                      onConfirm={() => this.delete(record.key)}
                    >
                      <a >删除</a>
                    </Popconfirm>
                    </div>
                  )}
              </div>)
            }
            if(record.groupName != null){
              return (
                <div>
                  {editable ? (
                    <span>
                      <EditableContext.Consumer>
                        {form => (
                          <a
                            href="javascript:;"
                            onClick={() => this.save(form, record.key,'update')}
                            style={{ marginRight: 8 }}
                          >
                            保存
                          </a>
                        )}
                      </EditableContext.Consumer>
                      {/* <Popconfirm
                        title="确定取消吗"
                        onConfirm={() => this.cancel(record.key)}
                      >
                      </Popconfirm> */}
                        <a onClick={() => this.cancel(record.key)}>取消</a>
                    </span>
                  ) : (
                      <div>
                        <a onClick={() => this.edit(record)}>编辑</a>
                        <Divider type="vertical" />
                        <a onClick={() => this.handleShowRankModal(record)}>新增组别</a>
                      </div>

                    )}
                </div>
              );
            }            
          }
        },
      ];
    }
    componentWillMount(){
      const {dispatch} = this.props;
      dispatch({
        type: 'personModels/checkIfShowGroupForm',
    }).then(()=>{
      const {personModels} = this.props;
      let id=personModels.groupFormVisibleData.flag?null:personModels.groupFormVisibleData.data.groupId
      // 准备第一次加载时候的初始数据
      dispatch({
        type: 'groupModel/searchGroup',
        payload:{id,status:'0'}
      }).then(()=>{
        this.setState({
          dataSource: this.props.groupModel.data.list,
          count:this.props.groupModel.count,
        });
      }).catch((e)=>{
        console.log(e);
      });
  
      // 获取团队名称的数据
      dispatch({
        type: 'shareModel/searchGroups',
      }).then(()=>{
      }).catch((e)=>{
        console.log(e);
      });
    })
  }

    // componentDidMount() {
    //   const { dispatch, umssouserinfo:{currentUser} ,personModels} = this.props;
    //   console.log(this.props.personModels)
    //   let groupId=personModels.groupFormVisibleData.flag?null:personModels.groupFormVisibleData.data.groupId
    //   // 准备第一次加载时候的初始数据
    //   dispatch({
    //     type: 'groupModel/searchGroup',
    //     data:{groupId}
    //   }).then(()=>{
    //     this.setState({
    //       dataSource: this.props.groupModel.data.list,
    //       count:this.props.groupModel.count,
    //     });
    //   }).catch((e)=>{
    //     console.log(e);
    //   });
  
    //   // 获取团队名称的数据
    //   dispatch({
    //     type: 'shareModel/searchGroups',
    //   }).then(()=>{
    //   }).catch((e)=>{
    //     console.log(e);
    //   });

    
   
    // }



    //查询团队与组别
    handleSearch = e => {
      //防止链接打开 URL
      if(e){
        e.preventDefault();
      }
      const { dispatch, form } = this.props;

      //表单校验
      form.validateFields((err, fieldsValue) => {
        if (err) return;

        const values = {
          ...fieldsValue, 
          status:fieldsValue.flag?'':0
        };
        console.log(values)
       
        //查询请求
        dispatch({
          type: 'groupModel/searchGroup',
          payload: values,
        }).then(()=>{
          console.log(this.props.groupModel)
          this.setState({
            dataSource: this.props.groupModel.list, 
            count:this.props.groupModel.count,
             searchPara:values
            });
        }).catch((e)=>{
          console.log(e);
        });
      });
    }

    //重置
    handleFormReset = () => {
      const { form, dispatch } = this.props;
      form.resetFields();
      this.setState({
        formValues: {},
      });
    };

    //团队名称改变事件
    handleGroupChange = (recordSelected) => { 
      const {data} = this.state;
      const newData = [...this.state.dataSource];
      const {key} = this.state.editingRecord;
      const index = newData.findIndex(item => key === item.key);
      console.log('index是行位置索引：'+ index);
      console.log('key是派遣公司主键：'+ key);
      console.log('recordSelected是团队主键：'+ recordSelected);
          
      let item = newData[index];
      item.groupId = recordSelected;
      newData.splice(index, 1, {
        ...this.state.editingRecord,
        ...item,
      });
      this.setState({ dataSource: newData });
    }


    //分页 
    handleStandardTableChange = (pagination, filtersArg, sorter) => {
        console.log("handleStandardTableChange called..");
        console.log(pagination);
        this.setState({
          pageNum: pagination.current,
          pageSize: pagination.pageSize,
        }, ()=> {
          this.handleSearch('');
        });
    }

    //手动选中
    handleSelectRows = rows => {     
      this.setState({
        selectedRows: rows,
      });
    }

    //间接过渡存储团队id
    handleShowRankModal = record => {      
      this.setState({
        groupId: record.id,
      });
      this.handleRankVisible(true);
    }

    // 新建职级弹出框
    handleRankVisible = flag => {
      console.log("list handleRankVisible called");
      this.setState({
        rankVisible: !!flag,
      });
    };

    // 新建团队弹出框
    handleGroupVisible = flag => {
      console.log("list handleGroupVisible called");
      this.setState({
        groupVisible: !!flag,
      });
    };

    // 新建组别
    handleAddRank = fields => {
      const {groupId} = this.state;
      const {dispatch} = this.props;
 
      var params = {
        groupId:groupId,
        teamName:fields.rankName
      }

      dispatch({
        type: 'groupModel/addTeam',
        data: {
          payload: params,
        },
      }).then(()=>{
        this.setState({
          rankVisible: false
        });
        let id= this.props.personModels.groupFormVisibleData.flag?null:this.props.personModels.groupFormVisibleData.data.groupId
        let payload  = id ?{id,status:'0'}:{...this.state.searchPara}
        //查询请求
        dispatch({
          type: 'groupModel/searchGroup',
          payload
        }).then(()=>{
          this.setState({dataSource: this.props.groupModel.list});
        }).catch((e)=>{
          console.log(e);
        });
        message.success("新增成功")
      }).catch((e)=>{
          message.error(e.message);
          console.log(e);
      });

    };

    // 新建团队
    handleAddGroup = fields => {
      console.log('handleAddGroup called...');
      console.log(fields);
      const params = {
        groupName: fields.groupName,
        deptCode: fields.deptCode,
        lawId: fields.lawId,
        isFirstCity: fields.isFirstCity,
      }
      console.log(params);
      this.props.dispatch({
        type: 'groupModel/addGroup',
        data: {
          payload: params,
        },
       }).then(()=>{
          // TODO: 团队增加成功后，设置对应的dataSource，使Table刷新增加新团队信息
          //  const newData = [...this.state.dataSource];
          const key = this.props.groupModel.groupId;
          let createdTime = moment(Date.now()) + '';
          console.log('after add group...');
          console.log(createdTime);
          console.log(fields.lawId);
          const newData = {
            key: key,
            id: key,
            groupId: this.props.groupId,
            groupName: fields.groupName,
            deptCode: fields.deptCode,
            lawId: fields.lawId,
            isFirstCity: fields.isFirstCity,
            createdTime: createdTime,
            status: '0',
            lawName: fields.lawId==1?'创新自建':fields.lawId==2?'创新非自建':'???'
          };
          this.setState({
            dataSource: [...this.state.dataSource, newData]});
            console.log(this.state.dataSource);
           // 获取团队名称的数据
            this.props.dispatch({
              type: 'shareModel/searchGroups',
            }).then(()=>{
              this.setState({groupData: this.props.groupsData});
            }).catch((e)=>{
              console.log(e);
            });
          this.handleGroupVisible(false);
      }).catch((e)=>{
          message.error(e.message);
          console.log(e);
      });
    }
    
    //table 行编辑事件
    isEditing = (record) => {
      return record.key === this.state.editingKey;   
    };

  edit(record) {
    console.log(record)
    try {
      console.log(this.props)
      this.props.history.push(`/person/searchGroupdetails/${record.id}`)
    }catch (e) {
      message.error(e.message);
      console.log(e);
    }
  }

    save(form, key) {
      form.validateFields((error, row) => {

        //获取数据源
        const newData = [...this.state.dataSource];     
 
        const index = newData.findIndex(item => key === item.key);  
        if (index > -1) {
          const item = newData[index];
          
          newData.splice(index, 1, {
            ...item,
            ...row,
          });
          
          //获取本行 index 的 jsonObj
          let outData = newData[index];
          this.setState({ dataSource: newData, editingKey: ''});
     
          //拼接参数
          let toSaveData = {
              id:outData.id,
              groupId: outData.groupId,
              groupModelName: outData.groupModelName,
              contactPerson: outData.contactPerson,
              contactTel: outData.contactTel,
              contactEmail: outData.contactEmail,
              address: outData.address,
              bankAccountName: outData.bankAccountName,
              bankAccount: outData.bankAccount,
              remark: outData.remark
          }
        
          //定义path
          const {dispatch} = this.props;
          if(this.state.editingKey && ((this.state.editingKey + '').indexOf('new') != -1)){
            dispatch({
              type: 'groupModel/insertgroupModel',
              data: toSaveData,
            }).then(()=>{
              // 保存后，把ruleId更新到data对应的record中
              const newData = [...this.state.dataSource];
              
              const index = newData.findIndex(item => key === item.key);
              if (index > -1) {
                const item = newData[index];
                
                // item.key = this.props.groupModel.;
                newData.splice(index, 1, {
                  ...this.state.editingRecord,
                  ...item,
                });                
              }
              this.setState({ dataSource: newData });
              message.success("新增公司信息成功");
              
            }).catch((e)=>{
              message.error(e.message);
              
            });
          }else{
            dispatch({
              type: 'groupModel/upadategroupModel',
              data: toSaveData,
            }).then(()=>{             
              message.success("保存公司信息成功");
              // this.setState({ dataSource: newData, editingKey: '' });
            }).catch((e)=>{
              message.error(e.message);
            });
          }
        } else {
          newData.push(Row);
          this.setState({ dataSource: newData, editingKey: '' });
        }
      });

    };

    //取消保存
    cancel = (key) => {
      this.setState({ editingKey: '' });
   
      // 如果是新增的规则未保存就放弃编辑，删除该行
      if((key + '').indexOf('new') != -1){
        this.onDelete(key);
      }
    };

    onDelete = (key) => {
      const data = [...this.state.dataSource];
      this.setState({ dataSource: data.filter(item => item.key !== key) });
    }

    //删除
    deleteRecord = (key) => {
      const {dispatch} = this.props;
      const deleteData = {
        id: key,
      }
      dispatch({
        type: 'groupModel/deletegroupModel',
        data: deleteData,
      }).then(()=>{
        const data = [...this.state.dataSource];
        this.setState({ dataSource: data.filter(item => item.key !== key),
                        editingKey: '' ,
                      });
        message.success('删除成功');
      }).catch((e)=>{
        message.error(e.message);
      });
    }

    

    //加载组件
    render() {
      //合并组件
      const components = {
        body: {
          row: EditableFormRow,
          cell: EditableCell,
        },
      };

      //引入table列和编辑
      const columns = this.columns.map((col) => {
        if (!col.editable) {
          return col;
        }
        return {
          ...col,
          onCell: record => ({
            record,
            inputType: col.dataIndex,
            dataIndex: col.dataIndex,
            title: col.title,
            editing: this.isEditing(record),
          }),
        };
      });

      const {loading,form, groupModel} = this.props;
      let { selectedRows, groupVisible, rankVisible, addVisible, dataSource} = this.state;

      console.log('render dataSource...');
      console.log(dataSource);

      const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
          this.setState({
            selectedRows: selectedRows,
          });
        },
        getCheckboxProps: record => ({
          disabled: record.name === 'Disabled User', // Column configuration not to be checked
          name: record.name,
        }),
      };    

      let total = 0;
      if(groupModel.data){   //模型 initgroupModelInfo 的参数
        total = groupModel.data.total;    
      }

      

      const parentMethods = {
        handleAddGroup: this.handleAddGroup,
        handleGroupVisible: this.handleGroupVisible,
        handleAddRank: this.handleAddRank,
        handleRankVisible: this.handleRankVisible,
      };
      
      return (
        <PageHeaderLayout title="团队和组别管理">
        <Card>
        {this.props.personModels.groupFormVisibleData.flag?
        <Form onSubmit={this.handleSearch} layout="inline">
          <Row gutter={{ md: 12, lg: 12, xl: 24 }} style={{paddingRight:50}}>
            <Col md={10} sm={10}>
              <FormItem label="团队名称">
                  {form.getFieldDecorator('id')
                    (<Select style={{ width: 220 }} >
                      {this.props.shareModel.groupsData.map(group => <Select.Option key={group.id}>{group.groupName}</Select.Option>)}
                    </Select>)
                  }
              </FormItem>
            </Col>
            {/* <Col md={12} sm={24}>
               <FormItem label="团队总监/组别领导">
                  {form.getFieldDecorator('empName')(
                    <Input placeholder='请输入'/>
                  )}
              </FormItem>
            </Col> */}
            <Col md={12} sm={24}>
              <Form.Item  >
                {form.getFieldDecorator('flag')(
                  <Checkbox >含已失效</Checkbox>
                )}
              </Form.Item>
            </Col>
          </Row>
           <Row gutter={{ md: 12, lg: 12, xl: 24 }} style={{paddingRight:50}}>
            <Col md={12} sm={24}>
                <span className={styles.submitButtons}>
                  <Button type="primary" htmlType="submit">
                    搜索
                  </Button>
                  <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                    重置
                  </Button>
                </span>
            </Col>
          </Row>
        </Form>
        :null}
            {/*<SelectInfo number='1' right='1' props={this.props}></SelectInfo>*/}
            <Card bordered={false}>
              <div className={styles.positionList}>
              {this.props.personModels.groupFormVisibleData.flag?
              <div>
              <SearchResult count={this.state.count} />
                <div className={styles.positionListOperator}>
                    <Button icon="plus" type="primary" onClick={() => this.handleGroupVisible(true)}>
                      新增团队
                    </Button>
                </div>
                <GroupAndTeamInfo  />
                </div>
                :null}
                <br/>
                <Table
                bordered
                indentSize={0}
                  childrenColumnName={'teams'}
                  components={components}
                  scroll={{ y: 480 }}
                  // selectedRows={selectedRows}
                  // rowSelection={rowSelection}
                  loading={loading}
                  dataSource={dataSource}
                  state={dataSource}
                  columns={columns}
                  // rowClassName="editable-row"
                  // onSelectRow={this.handleSelectRows}
                  addVisible={this.addVisible}
                  onChange={this.handleStandardTableChange}
                  pagination={false}
                  />
              </div>
            </Card>
            <CreateGroupForm { ...parentMethods } groupVisible={ groupVisible } />
            <CreateRankForm { ...parentMethods } rankVisible={ rankVisible } />
            </Card>
        </PageHeaderLayout>
      );
    }
}



