import React, { PureComponent } from 'react';
import { getTeamColumns } from './PersonColumns';
import { Form, Input, Row, Col, Select, Button, message, DatePicker, Table, Modal, Popconfirm } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';
import { getTeamInfoColumns } from './PersonColumns';

const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;
const formItemLayout = {
  labelCol: {
    sm: { span: 7 },
  },
  wrapperCol: {
    sm: { span: 15 },
  },
};

@connect(({ personModels, loading }) => ({
  personModels,
  loading: loading.models.personModels,
}))
@Form.create()
export default class TeamInfo extends PureComponent {

  state = {
    empId: {},
    editVisible: false,
    addVisible: false,
    teamDataSource: [],
    groupChildren: [],
    teamChildren: [],
    updateTeamRecord: {},
    groupId: '',
  }

  componentDidMount = () => {
    this.setState({
      // groupChildren: this.props.groupChildren,
      // teamDataSource: this.props.teamDataSource,
      // groupId: this.props.groupId,
      // empId: this.props.empId,
    })
    console.log('componentDidMount.groupId');
    console.log(this.props.teamDataSource);

    const value = this.props.groupId;
     if (value) {
      let reqBody = {
        data: {groupId: value},
      }
      this.props.dispatch({
        type: 'personModels/selectTeamName',
        payload: reqBody,
      }).then(() => {
          let teamNames = this.props.personModels.teamNames.data || [];
          let teamChildren = [];
          for (let i = 0; i < teamNames.length; i++) {
            teamChildren.push(<Option key={teamNames[i].id} value={teamNames[i].id}>{teamNames[i].teamName}</Option>)
          }
          this.setState({ teamChildren: teamChildren });
          console.log('itemInfo.componentDidMount selectTeamName...');
          console.log(teamChildren);
      });
  
    } else {
      this.setState({ teamChildren: [], })
    }
  }

  // 编辑组别
  edit = (record) => {
    this.setState({ editVisible: true, updateTeamRecord: record })
  }

  // 编辑确定
  editHandleOk = () => {
    console.log('编辑确定')
    this.props.form.validateFieldsAndScroll(['editGroupId','editTeamId','editTeamDate'],(err, fieldValues) => {
      fieldValues.id = this.state.updateTeamRecord.id;
      if (err) {
        console.log(err)
        return;
      }
      if (!fieldValues.editTeamDate || fieldValues.editTeamDate.length === 0) {
        fieldValues.bgnDate = null;
        fieldValues.endDate = null;
      } else {
        fieldValues.bgnDate = (fieldValues.editTeamDate[0].format('X') * 1000).toString();
        fieldValues.endDate = (fieldValues.editTeamDate[1].format('X') * 1000).toString();
      }
      delete fieldValues.editTeamDate;
      delete fieldValues.editTeamId;
      console.log('fieldValues', fieldValues);

      // 请求后台
      let reqBody = {
        data: fieldValues
      }
      this.props.dispatch({
        type: 'personModels/updateTeamRecord',
        payload: reqBody,
      }).then(() => {
        let resp = this.props.personModels.updateTeamRecordResp;
        
        if (resp.code === 0) {
          message.success('更新成功');
          this.setState({ editVisible: false })
          this.updateTeamDataSource();
        } else {
          message.error('更新失败')
        }
      })
    })
  }

  // 编辑取消
  editHandleCancle = () => {
    this.setState({ editVisible: false })
  }

  // 添加组别
  handleAdd = () => {
    //重置表单
    this.setState({ addVisible: true })
    this.props.form.resetFields()

  }

  // 添加确定
  addHandleOk = () => {
    this.props.form.validateFieldsAndScroll(['addGroupId','addTeamId','addTeamDate'],(err, fieldValues) => {
      fieldValues.empId = this.props.empId;
      if (err) {
        return;
      }
      if (!fieldValues.addTeamDate || fieldValues.addTeamDate.length === 0) {
        fieldValues.bgnDate = null;
        fieldValues.endDate = null;
      } else {
        fieldValues.bgnDate = (fieldValues.addTeamDate[0].format('X') * 1000).toString();
        fieldValues.endDate = (fieldValues.addTeamDate[1].format('X') * 1000).toString();
      }
      fieldValues.teamId = fieldValues.addTeamId.toString();
      delete fieldValues.addTeamDate;
      delete fieldValues.addGroupId;
      console.log('fieldValues', fieldValues);

      // 请求后台
      let reqBody = {
        data: fieldValues
      }
      this.props.dispatch({
        type: 'personModels/insertTeamRecord',
        payload: reqBody,
      }).then(() => {
        let resp = this.props.personModels.insertTeamRecordResp;
        console.log(resp);
        if (resp.code === 0) {
          message.success('保存成功');
          this.setState({ addVisible: false })
          this.updateTeamDataSource()
        } else {
          message.error(resp.message);
        }
      })
    })
  }

  // 更新组别信息
  updateTeamDataSource = () => {
    let reqBody = {
      data: {
        id: this.props.empId,
      }
    }
    this.props.dispatch({
      type: 'personModels/findPersonnel',
      payload: reqBody,
    }).then(() => {
      // this.setState({ 
      //   teamDataSource: this.props.personModels.updateTeamDataSourceResp.data.teamInfoDto,
      // })
    })
  }

  // 添加取消
  addHandleCancle = () => {
    this.setState({ addVisible: false })
  }

  // 删除组别
  handleDelete = (record) => {
    let reqBody = {
      data: {
        id: record.id.toString(), 
      }
    }
    this.props.dispatch({
      type: 'personModels/deleteTeamRecord',
      payload: reqBody,
    }).then(()=>this.updateTeamDataSource())
    // let teamDataSource = this.props.teamDataSource;
    // console.log(record.id)
    // console.log(teamDataSource)
    // this.setState({ teamDataSource: teamDataSource.filter(item => item.id !== record.id) });
  }

  // 根据团队查询组别
  groupIdChange = (value,props) => {
    if (value) {
      let reqBody = {
         data: {groupId: value},
      }
      this.props.dispatch({
        type: 'personModels/selectTeamName',
        payload: reqBody,
      }).then(() => {
        console.log(this.props.personModels)
          let teamNames = this.props.personModels.teamNames.data || [];
          let teamChildren = [];
          for (let i = 0; i < teamNames.length; i++) {
            teamChildren.push(<Option key={teamNames[i].id} value={teamNames[i].id}>{teamNames[i].teamName}</Option>)
          }
          this.props.form.resetFields('addTeamId')
          this.setState({ teamChildren: teamChildren })
        })
    } else {
      this.setState({ teamChildren: []})
    }
  }

  render() {
    const { loading, form } = this.props;
    const { getFieldDecorator } = form;

    return (
      <div>
        <Table
          columns={getTeamInfoColumns(this)}
          dataSource={this.props.teamDataSource}
          bordered={true}
          pagination={false}
          loading={loading}>
        </Table>
        <Button onClick={this.handleAdd} type="primary" icon="plus" style={{marginTop: 10}}>新增组别记录</Button>
        <Modal
          title="编辑组别"
          visible={this.state.editVisible}
          onOk={this.editHandleOk}
          onCancel={this.editHandleCancle}
          destroyOnClose={true}
          confirmLoading={loading}
          maskClosable={false}
          closable={false}
          okText="更新"
          >
          <Form>
            <Form.Item {...formItemLayout} label="团队名称">
              {getFieldDecorator('editGroupId',{initialValue: this.props.groupId,})(
                <Select placeholder="请选择"  disabled>
                  {this.props.groupChildren}
                </Select>
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="组别名称">
              {getFieldDecorator('editTeamId', {initialValue: this.state.updateTeamRecord.teamName,})(
                <Select placeholder="请选择" disabled>
                  {this.state.teamChildren}
                </Select>
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="挂靠起止时间">
              {getFieldDecorator('editTeamDate', {initialValue: 
                [parseInt(this.state.updateTeamRecord.bgnDate) ? moment(parseInt(this.state.updateTeamRecord.bgnDate)) : null, 
                  parseInt(this.state.updateTeamRecord.endDate) ? moment(parseInt(this.state.updateTeamRecord.endDate)) : null],
                rules: [{required: true, message: '必选'}]})(
                <RangePicker></RangePicker>
              )}
            </Form.Item>
          </Form>
        </Modal>

        <Modal
          title="新增组别"
          visible={this.state.addVisible}
          onOk={this.addHandleOk}
          onCancel={this.addHandleCancle}
          destroyOnClose={true}
          confirmLoading={loading}
          maskClosable={false}
          closable={false}
          okText="保存"
          >
          <Form>
            <Form.Item {...formItemLayout} label="团队名称">
              {/* {getFieldDecorator('groupId', {rules: [{required: true, message: '必选'}]})( */}
                {getFieldDecorator('addGroupId', {initialValue: null,rules: [{required: true, message: '必选'}]})(
                <Select placeholder="请选择" onChange={this.groupIdChange}>
                  {this.props.groupChildren}
                </Select>
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="组别名称">
              {getFieldDecorator('addTeamId', {initialValue: null,rules: [{required: true, message: '必选'}]})(
                <Select placeholder="请选择">
                  {this.state.teamChildren}
                </Select>
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="挂靠起止时间">
              {getFieldDecorator('addTeamDate', {initialValue: [null,null],rules: [{required: true, message: '必选'}]})(
                <RangePicker>

                </RangePicker>
              )}
            </Form.Item>
          </Form>
        </Modal>
      </div>
    )
  }
}