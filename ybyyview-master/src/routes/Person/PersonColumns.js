import React, { PureComponent } from 'react';
import { DatePicker, Popconfirm, Select } from "snk-web";
import moment from "moment";

const jobStatus={1:"培训期",2:"试用期",3:'已转正',4:'已离职'}
// 创新业务团队
const jobStatus2={1:"在职",2:"离职",3:'离退休',4:'不在职'}

export function getPersonListColumns(parentThis) {
  return [
    {
      title: '员工姓名',
      dataIndex: 'empName',
      align: 'center',
    },
    {
      title: '证件号码',
      dataIndex: 'identityCode',
      align: 'center',
    },
    {
      title: '员工邮箱',
      dataIndex: 'email',
      align: 'center',
    },
    {
      title: '员工电话',
      dataIndex: 'mobile',
      align: 'center',
    },
    {
      title: '团队名称',
      dataIndex: 'groupName',
      align: 'center',
    },
    {
      title: '组别名称',
      dataIndex: 'teamName',
      align: 'center',
    },
    {
      title: '开户银行',
      dataIndex: 'bankName',
      align: 'center',
    },
    {
      title: '银行卡账号',
      dataIndex: 'bankNo',
      align: 'center',
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      render:(status)=>jobStatus[status]?jobStatus[status]:'未设置'
    },
    {
      title: '操作',
      align: 'center',
      render: (text, record) => {
        return <div>
            <a onClick={() => parentThis.editPerson(record.id)}>编辑</a>
            <a onClick={() => parentThis.showPersonHistory(record)} style={{marginLeft: 5}}>历史记录</a>
          </div>
      }
    },
  ]
}

export function getPersonListColumns2(parentThis) {
  return [
    {
      title: '员工姓名',
      dataIndex: 'empName',
      align: 'center',
    },
    {
      title: '证件号码',
      dataIndex: 'identityCode',
      align: 'center',
    },
    {
      title: '员工邮箱',
      dataIndex: 'email',
      align: 'center',
    },
    {
      title: '员工电话',
      dataIndex: 'mobile',
      align: 'center',
    },
    {
      title: '团队名称',
      dataIndex: 'groupName',
      align: 'center',
    },
    {
      title: '组别名称',
      dataIndex: 'teamName',
      align: 'center',
    },
    {
      title: '开户银行',
      dataIndex: 'bankName',
      align: 'center',
    },
    {
      title: '银行卡账号',
      dataIndex: 'bankNo',
      align: 'center',
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      render:(status)=>jobStatus2[status]?jobStatus2[status]:'未设置'
    },
    {
      title: '操作',
      align: 'center',
      render: (text, record) => {
        return <div>
            <a onClick={() => parentThis.editPerson(record.id)}>编辑</a>
            {/* <a onClick={() => parentThis.showPersonHistory(record)} style={{marginLeft: 5}}>历史记录</a> */}
          </div>
      }
    },
  ]
}
export function getTeamInfoColumns(parentThis) {
  return [
    {
      title: '序号',
      dataIndex: 'id',
      align: 'center',
      width: '10%',
    },
    {
      title: '组别编码',
      dataIndex: 'teamId',
      align: 'center',
      width: '18%',
    },
    {
      title: '组别名称',
      dataIndex: 'teamName',
      align: 'center',
      editable: true,
      width: '18%',
      render: (text, record) => {
        return  record.teamName
      }
    },
    {
      title: '挂靠开始时间',
      dataIndex: 'bgnDate',
      align: 'center',
      editable: true,
      width: '18%',
      render: (text) => {
        let isMoment = moment.isMoment(text);
        return isMoment ? text : moment(parseInt(text)).format('YYYY-MM-DD')
      }
    },
    {
      title: '挂靠结束时间',
      dataIndex: 'endDate',
      align: 'center',
      editable: true,
      width: '18%',
      render: (text) => {
        let isMoment = moment.isMoment(text);
        return isMoment ? text : moment(parseInt(text)).format('YYYY-MM-DD')
      }
    },
    {
      title: '操作',
      align: 'center',
      width: '18%',
      render: (text, record) => {
        return <div>
                <a onClick={() => parentThis.edit(record)}>修改</a>
                <Popconfirm title="确定删除吗?" onConfirm={() => parentThis.handleDelete(record)}>
                  <a style={{marginLeft: 5}}>删除</a>
                </Popconfirm>
          </div>
      }
    },
  ]
}

export function getRankInfoColumns(parentThis) {
  return [
    {
      title: '序号',
      dataIndex: 'id',
      align: 'center',
      width: '10%',
    },
    {
      title: '岗位',
      dataIndex: 'positionName',
      align: 'center',
      width: '15%',
      editable: true,
      // render: (text, record) => {
      //   return <Select disabled placeholder="请选择" defaultValue={text}>
      //     {record.positionChildren}
      //   </Select>
      // }
    },
    {
      title: '职级',
      dataIndex: 'rankName',
      align: 'center',
      width: '15%',
      editable: true,
      // render: (text, record) => {
      //   return <Select disabled placeholder="请选择" defaultValue={text}>
      //     {record.rankChildren}
      //   </Select>
      // }
    },
    {
      title: '职级有效起期',
      dataIndex: 'bgnDate',
      align: 'center',
      editable: true,
      width: '15%',
      render: (text) => {
        let isMoment = moment.isMoment(text);
        return isMoment ? text : moment(parseInt(text)).format('YYYY-MM-DD')
      }
    },
    {
      title: '职级有效止期',
      dataIndex: 'endDate',
      align: 'center',
      editable: true,
      width: '15%',
      render: (text) => {
        let isMoment = moment.isMoment(text);
        return isMoment ? text : moment(parseInt(text)).format('YYYY-MM-DD')
      }
    },
    {
      title: '操作',
      align: 'center',
      width: '15%',
      render: (text, record) => {
        return <div>
                <a onClick={() => parentThis.edit(record)}>修改</a>
                <Popconfirm title="确定删除吗?" onConfirm={() => parentThis.handleDelete(record)}>
                  <a style={{marginLeft: 5}}>删除</a>
                </Popconfirm>
                {parentThis.state.ifHasAuditRole?<a onClick={() => parentThis.audit(record)} style={{marginLeft: 5}}>审核</a>:null}
          </div>
      }
    },
    {
      title: '状态',
      align: 'center',
      dataIndex: 'status',
      width: '15%',
      render: (text ) => {
        return  text==0?'有效':'无效';
      }
    },
  ]
}