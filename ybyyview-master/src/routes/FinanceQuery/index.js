import React, { PureComponent } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Card,
  Table,
  Tabs,
  Button,
  message,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './FinanceQuery.less';
import SearchForm from './SearchForm';
import SearchExactForm from './SearchExactForm';




const columns = [ 
  {
    title: '投保单号',
    dataIndex: 'cPlyAppNo',
    align: 'center',
  },{
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },{
    title: '款别',
    dataIndex: 'cTgtFld1',
    align: 'center',
  },{
    title: '最新回盘日期',
    dataIndex: 'chargeDate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },
  {
    title: '当前扣款状态',
    dataIndex: 'chargeStatus',
    align: 'center',
  },
  {
    title: '最近回盘代码',
    dataIndex: 'chargeFlag',
    align: 'center',
  },
  {
    title: '最近扣款描述',
    dataIndex: 'chargeName',
    align: 'center',
  },
  {
    title: '扣款渠道代码',
    dataIndex: 'extEnterpCode',
    align: 'center',
  },{
    title: '首次保险首期扣款成功日期',
    dataIndex: 'firstChargeDate',
    align: 'center',
    render(val){
      return <div>{moment(val).format("YYYY-MM-DD")} </div>
    },
  },{
    title: '首次保险起期',
    dataIndex: 'firstInsrncBeginDate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },{
    title: '保单状态',
    dataIndex: 'flag',
    align: 'center',
  },{
    title: '保险起期',
    dataIndex: 'insrncBeginDate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },{
    title: '业务渠道代码',
    dataIndex: 'oldEnterpCode',
    align: 'center',
  },{
    title: '是否继续请款',
    dataIndex: 'payAgainFlag',
    align: 'center',
  },{
    title: '初次请款日期',
    dataIndex: 'payDateFirst',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },{
    title: '实际请款日期',
    dataIndex: 'payDateLast',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },{
    title: '请款金额',
    dataIndex: 'payReqPrm',
    align: 'center',
  },{
    title: '请款次数',
    dataIndex: 'payTime',
    align: 'center',
  },{
    title: '请款类型',
    dataIndex: 'payType',
    align: 'center',
  },{
    title: '最新请款期次',
    dataIndex: 'period',
    align: 'center',
  },{
    title: '总保费',
    dataIndex: 'prm',
    align: 'center',
  },{
    title: '产品（中文名称）',
    dataIndex: 'prodName',
    align: 'center',
  },{
    title: '续保次数',
    dataIndex: 'renewalTime',
    align: 'center',
  },{
    title: '投保人证件号',
    dataIndex: 'cAppCertNo',
    align: 'center',
  },
  {
    title: '投保人姓名',
    dataIndex: 'cAppNme',
    align: 'center',
  },
  {
    title: '出单机构代码',
    dataIndex: 'cDptCde',
    align: 'center',
  },
  {
    title: '保单类型',
    dataIndex: 'cElcFlag',
    align: 'center',
  },{
    title: '被保人证件号',
    dataIndex: 'cInsrntCde',
    align: 'center',
  },{
    title: '被保人姓名',
    dataIndex: 'cInsrntCnm',
    align: 'center',
  },{
    title: '卡号',
    dataIndex: 'thirdCardId',
    align: 'center',
  },{
    title: '销售人员姓名',
    dataIndex: 'tsrName',
    align: 'center',
  },
  {
    title: 'TSRCODE',
    dataIndex: 'tsrCode',
    align: 'center',
  },
  {
    title: '投保人手机号码',
    dataIndex: 'cAppMobile',
    align: 'center',
  },
  {
    title: '是否发送电子保单',
    dataIndex: 'ifElecPly',
    align: 'center',
  },
  {
    title: '新单快递状态',
    dataIndex: 'cExpressCondition',
    align: 'center',
  },
  {
    title: '名单类型',
    dataIndex: 'nameListType',
    align: 'center',
  },
  {
    title: '销售区域',
    dataIndex: 'saleArea',
    align: 'center',
  },
];

@connect(({ financeQuery, loading }) => ({
  financeQuery,
  loading: loading.models.financeQuery,
}))

export default class FinanceQuery extends PureComponent {
  state = {
    pageSize:10,
    pageNum:1,
    channelName:'-',
    searchType:'',
  };

  componentWillMount(){
    Object.keys(this.props.financeQuery).forEach(key => delete this.props.financeQuery[key]);
  }

  handleFormReset = (form) => {
    form.resetFields();
  };

  handleSearch = (e,form) => {
    e.preventDefault();
    this.form = form;
    this.setState({pageNum:1,searchType:'nomal'},()=>{
      this.searchData(form);
    });
  };

  searchData(form){
    const { dispatch } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'financeQuery/queryPayChargeInfo',
        payload: this.perpareValues(fieldsValue),
      }).then(()=>{
      }).catch(e=>{

      })
    });
  }

  downLoad(form){
    const { dispatch} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'financeQuery/exportFinanceQueyrFile',
        payload: this.perpareValues(fieldsValue),
      }).then(()=>{
      }).catch(e=>{
        message.error('网络异常请稍后再试');
      });
    });
  }


  handleExactSearch = (e,form) => {
    e.preventDefault();
    this.form = form;
    this.setState({pageNum:1,searchType:'exact'},()=>{
      this.searchExactData(form);
    });
  }

  searchExactData(form){
    const { dispatch} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'financeQuery/queryPayChargeInfo',
        payload: fieldsValue,
      }).then(()=>{
      }).catch(e=>{
        message.error('网络异常请稍后再试');
      });
    });
  }

  perpareValues(values){
    if(!values.chargeDate||values.chargeDate.length===0){
      values.chargeDateFrom = '';
      values.chargeDateTo = '';
    } else {
      values.chargeDateFrom = values.chargeDate[0].format("YYYY-MM-DD");
      values.chargeDateTo = values.chargeDate[1].format("YYYY-MM-DD");
    }
    if(!values.payDateFirst||values.payDateFirst.length===0){
      values.payDateFirstFrom = '';
      values.payDateFirstTo = '';
    } else {
      values.payDateFirstFrom = values.payDateFirst[0].format("YYYY-MM-DD");
      values.payDateFirstTo = values.payDateFirst[1].format("YYYY-MM-DD");
    }
    if(!values.payDateLast||values.payDateLast.length===0){
      values.payDateLastFrom = '';
      values.payDateLastTo = '';
    } else {
      values.payDateLastFrom = values.payDateLast[0].format("YYYY-MM-DD");
      values.payDateLastTo = values.payDateLast[1].format("YYYY-MM-DD");
    }

    delete values.chargeDate;
    delete values.payDateFirst;
    delete values.payDateLast;

    values.pageNum = this.state.pageNum;
    values.pageSize = this.state.pageSize;
    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  renderAdvancedForm() {
    return (
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab={<span>模糊查询</span>} key="1">
          <SearchForm {...this.props} root={this}/>
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span>精确查询</span>} key="2">
         <SearchExactForm {...this.props} root={this}/>
        </Tabs.TabPane>
      </Tabs>
      
    );
  }


  handleTableChange = (pagination) => {
    this.setState({
      pageSize: pagination.pageSize,
      pageNum: pagination.current,
    },()=>{
      if(this.state.searchType === 'nomal'){
        this.searchData(this.form);
      }else{
        this.searchExactData(this.form);
      }
    });
    
  };

  render() {
    const { financeQuery: { dataSource }, loading } = this.props;
    let datalist = [];
    let total;
    if(!(null==dataSource)){
      datalist = dataSource.list;
      total = dataSource.total;
    }
    const p = {};

    if(this.state.searchType==='nomal'){
      p.title=() => {
        return <div className={styles.TableTitle}>
          <Button style={{ marginLeft: 8 }} type="primary" onClick={()=>{this.downLoad(this.nomalForm)}}>
              导出excel
            </Button>
        </div>
      } 
    } else{
      p.title=() => {
        return <div className={styles.TableTitle}>
          <Button style={{ marginLeft: 8 }} type="primary" onClick={()=>{this.downLoad(this.exactForm)}}>
              导出excel
            </Button>
        </div>
      }
    }
   
  

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
            <div>{this.renderAdvancedForm()}</div>
            <Table
              loading={loading}
              rowKey={record => record.cPlyNo}
              dataSource={datalist}
              bordered
              scroll={{ x: 2700 }}
              size="small"
              {...p}
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                ...{
                  current:this.state.pageNum,
                  pageSize:this.state.pageSize,
                  showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                  total:total,
                  pageSizeOptions:['10','20','30','40','50'],
                },
              }}
              columns={columns}
              onChange={this.handleTableChange}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
