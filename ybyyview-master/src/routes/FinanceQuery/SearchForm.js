import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  DatePicker,
  Input,
  InputNumber,
  message
} from 'snk-web';
import ChannelAndProduction from 'components/ChannelAndProduction';
import { getSelectChild } from '../../utils/utils'


const InputGroup = Input.Group;
const FormItem = Form.Item;
const { Option } = Select;

@Form.create()
export default class SearchForm extends PureComponent {
  
  constructor(props){
    super(props);
    this.state = {
      hasPayDateLast: false,
      disabledArea:true,
      disabledAppDate:false,
      showArea:false
    };
    props.root.nomalForm = props.form;
  }
  
  handleSearch(e){
    e.preventDefault();
    if(this.checkRule()){
      this.props.root.handleSearch(e,this.props.form);
    } else{
      message.warning('查询条件日期区间，三选一必录一个');
    }
  }

  onTypeChange(value){
    const { form} = this.props;
    const { getFieldValue,setFieldsValue } = form;
    const extEnterpCode = getFieldValue('extEnterpCode');
  }

  checkRule(){
     if((!this.props.form.getFieldValue('payDateLast')||this.props.form.getFieldValue('payDateLast').length===0)
     && (!this.props.form.getFieldValue('payDateFirst')||this.props.form.getFieldValue('payDateFirst').length===0)
     && (!this.props.form.getFieldValue('chargeDate')||this.props.form.getFieldValue('chargeDate').length===0)){
       return false;
      }
      return true;
    }

  onChannelChange(value){
    if(value === '95555'){
      this.setState({showArea: true});
    } else{
      this.setState({showArea: false});
    }
  }

  pyearChange = (value) => {
    if(value.length>=4){
      value.shift();
      message.warning('查询条件:"保单年份"最多支持跨三年查询。');
    }
  }
  payDateLastChange = (date) => {
    if(date.length) {
      this.props.form.resetFields('chargeFlag');
      this.setState({
        hasPayDateLast: true,
      })
    } else {
      this.setState({
        hasPayDateLast: false,
      })
    }
  }
  render() {
    const { form , root } = this.props;
    const { getFieldDecorator,getFieldValue } = form;
    const disabledArea = false;
  
    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' 
      onSubmit={this.handleSearch.bind(this)} 
      layout="inline">
        <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="保单年份">
              {getFieldDecorator('pYear',
              {rules: [{required: true, message: '保单年份必选'}]},
              {initialValue: pyear})(
                <Select mode='multiple'
                onChange = {this.pyearChange.bind(this)} 
                placeholder="请选择"
                style={{ width: '100%' }}>
                  {getSelectChild('pyear')}
                </Select>
                )}
            </FormItem>
          </Col>
          {this.state.showArea ?
          <Col md={6} sm={24}>
            <Form.Item label="地区">
              {getFieldDecorator('area')(
              <Select allowClear placeholder='请选择'>
                <Option value='N'>上海</Option>
                <Option value='C'>成都</Option>
              </Select>
              )}
            </Form.Item>
          </Col> : null}
        </Row>
        <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
         <Col md={12} sm={24}>
          <Form.Item label='扣款渠道'>
              {getFieldDecorator('extEnterpCode',
              {rules: [{required: true, message: '渠道必选'}]},
              {initialValue:''})(
                <Select onChange = {this.onChannelChange.bind(this)} >
                  {getSelectChild('payChannel1',null,null,true)}
                </Select>
              )}
          </Form.Item>
         </Col>
          <Col md={12} sm={24}>
            <FormItem label="期次区间">
              <InputGroup compact>
              {getFieldDecorator('periodFrom')( 
              <InputNumber min={1} max={12} style={{ width: 149, textAlign: 'center' }} placeholder="开始期次" />
              )}
              <Input style={{ width: 30, borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="~" disabled />
              {getFieldDecorator('periodTo')( 
              <InputNumber min={1} max={12} style={{ width: 149, textAlign: 'center', borderLeft: 0 }} placeholder="结束期次" />
            )}
              </InputGroup>
              </FormItem>
          </Col>
           </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
        <Col md={12} sm={24}>
            <FormItem label="请款类型">
              {getFieldDecorator('payType',
              {rules: [{required: true, message: '请款类型必选'}]})(
                <Select 
                  placeholder="请选择"
                  style={{ width: '100%' }}
                  allowClear
                >
                  <Select.Option key="1">首期请款</Select.Option>
                  <Select.Option key="2">续期请款</Select.Option>
                  <Select.Option key="3">首期请款+续期请款</Select.Option>
                  <Select.Option key="4">退费</Select.Option>
                </Select>)}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="是否继续请款">
              {getFieldDecorator('payAgainFlag')(
                <Select 
                  placeholder="请选择"
                  style={{ width: '100%' }}
                  allowClear
                >
                  <Select.Option key="1">是</Select.Option>
                  <Select.Option key="0">否</Select.Option>
                </Select>)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
        <Col md={12} sm={24}>
            <FormItem label="扣款状态">
              {getFieldDecorator('chargeFlag', {
                initialValue: this.state.hasPayDateLast?'1':null,
              })(
                <Select 
                  placeholder="请选择"
                  style={{ width: '100%' }}
                  allowClear
                >
                  <Select.Option key="1">成功</Select.Option>
                  <Select.Option key="2" disabled={this.state.hasPayDateLast}>失败</Select.Option>
                  <Select.Option key="3" disabled={this.state.hasPayDateLast}>未回盘</Select.Option>
                </Select>)}
            </FormItem>
          </Col>
         <Col md={12} sm={24}>
            <FormItem label="回盘日期">
              {getFieldDecorator('chargeDate')( 
                <DatePicker.RangePicker allowClear style={{ width: '100%' }}/>
              )}
            </FormItem>
          </Col>
        
        </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
         <Col md={12} sm={30}>
            <FormItem label="初次请款日期">
              {getFieldDecorator('payDateFirst')(
                <DatePicker.RangePicker   style={{ width: '100%' }}/>
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="实际请款日期">
              {getFieldDecorator('payDateLast')(
                <DatePicker.RangePicker onChange={this.payDateLastChange} style={{ width: '100%' }}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
          <Button type="primary" htmlType="submit">
              查询
          </Button>
          <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form); this.setState({hasPayDateLast: false})}}>
              重置
          </Button>
        </div>
      </Form>
    );
  }
}
