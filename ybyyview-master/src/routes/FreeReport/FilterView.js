import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  message,
} from 'snk-web';
import TemplateView from './Template';
import Classify from './Classify';
const { Option } = Select;

export default class FilterView extends React.Component {
  render() {
    if(!this.props.show){
      return null;
    }
    return (
      <div>
        <TemplateView {...this.props}/>
        <div style={{borderBottom:'1px solid #eee',marginTop:5,marginBottom:5}}></div>
        <Classify {...this.props}/>
      </div>
    );
  }
}
