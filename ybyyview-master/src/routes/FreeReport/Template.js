import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  message,
} from 'snk-web';
import Styles from './index.less';
const { Option } = Select;


export default class Template extends PureComponent {
  componentDidMount(){
    this.props.dispatch({
      type:'report/findTemplate'
    }).catch(()=>{
      message.error("网络异常~请刷新再试");
    });
  }
  itemClick(index,item){
    const fields = {};
    for(let i = 0,j=item.field.length;i<j;i+=1){
      fields[item.field[i]] = item.label[i];
    }
    this.props.dispatch({
      type:'report/templateSelected',
      payload:{
        index:index,
        fields:fields
      }
    });
  }
  render() {
    const templates = this.props.report.TemplateData || [];
    const labels = [];
    for (let i = 0, j = templates.length; i < j; i += 1) {
      const item = templates[i];
      let Style = {};
      if(i===this.props.report.selectedTemplateIndex){
        Style.backgroundColor = '#40a9ff';
        Style.color = '#fff';
      }
      labels.push(<span onClick={this.itemClick.bind(this,i,item)} style={Style} className={Styles.ClassifyLabel} key={i}>{item.templateName}</span>);
    }
    return (
      <div className={Styles.ClassifyLabelWrapper}>
        <Row>
         <Col md={4} sm={24}>模板</Col>
         <Col md={20} sm={24}>{labels}</Col>
        </Row>
      </div>
    );
  }
}
