import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Popconfirm,
  Upload
} from 'snk-web';
import FilterView from './FilterView';
import Styles from './index.less'
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ChannelAndProduction from 'components/ChannelAndProduction';
import { getDataSource,getSelectChild } from '../../utils/utils'



const { RangePicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;



@connect(({ report, loading }) => ({
  report,
  tableLoading: loading.effects['report/queryDynamicReport'],
  loading,
}))
@Form.create()
export default class FreeReport extends React.Component {
  
  constructor(props){
    super(props);
    this.tableFields = props.report.tableFields;
    
    this.state = {
      pageSize:10,
      pageNum:1,
      privilege:"0",
      templateName:'',
      visible:false,
      showArea:false,
      confirmLoading:false, 
      showFilterView:false,
      downloading:false,
      searchLoading:false,
    }
  }
  onChannelChange(value){
    let showArea = false;
    if(value.length===1){
      showArea = value[0]==='95555';
    }
    this.setState({
      showArea
    });
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    this.setState({pageNum:1,pageSize:10});
    form.resetFields();
  };

  handleSearch = e => {
    e.preventDefault();
    this.setState({pageNum:1,pageSize:10});
    this.searchData(1,10);
  };

  searchData = (pageNum,pageSize) => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue = this.perpareValues(fieldsValue);
      if(fieldsValue===null){
        return;
      }
      dispatch({
        type: 'report/queryDynamicReport',
        payload: {
          ...fieldsValue,
          pageNum: pageNum,
          pageSize: pageSize
        },
      })
    });
  }

  handleDownload = e => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      let insrncBeginDate = form.getFieldValue('insrncBeginDate');
      let insrncEndDate = form.getFieldValue('insrncEndDate');
      let payDateFirst = form.getFieldValue('payDateFirst');
      let chargeDate = form.getFieldValue('chargeDate');
      let appDate = form.getFieldValue('appDate');
      let underwritingTime = form.getFieldValue('underwritingTime');
      let inputDate = form.getFieldValue('inputDate');
      let payDate = form.getFieldValue('payDate');
      if (insrncBeginDate===undefined && insrncEndDate===undefined
        && payDateFirst===undefined && chargeDate===undefined && appDate===undefined
        && underwritingTime===undefined && inputDate===undefined && payDate===undefined ){
        message.error("查询条件中必须至少选择一段时间区间。")
        return;
      }
      fieldsValue = this.perpareValues(fieldsValue);
      if(fieldsValue===null){
        return;
      }
      this.setState({
        downloading:true
      });
      dispatch({
        type: 'report/fetchDynamicReport',
        payload: {
          ...fieldsValue,
          pageNum: this.state.pageNum,
          pageSize: this.state.pageSize,
        },
      }).then(()=>{
        this.setState({
          downloading:false
        });
      }).catch((e)=>{
        this.setState({
          downloading:false
        });
        console.error(e);
        message.error("导出失败")
      });
    });
  };


  getYears(){
    const re = [];
    const year = (new Date()).getFullYear();
    for(let i=year-10;i<=year+10;i+=1){
      re.push(<Option key={i} value={i}>{i}</Option>);
    }
    return re;
  }

  perpareValues(values){
    const dateArr = ['insrncBeginDate','insrncEndDate','chargeDate','payDateFirst','payDate','inputDate','underwritingTime','appDate'];
    for(let i = 0, j = dateArr.length; i < j; i += 1) {
      const key = dateArr[i];
      if(!values[key]||values[key].length===0){
        delete values[key]
      }else{
        values[key] =JSON.stringify([values[key][0].format("YYYY-MM-DD"),values[key][1].format("YYYY-MM-DD")]);
      }
    }
   
    if(values.extEnterpCode && values.extEnterpCode.length>0){
      values.extEnterpCode = values.extEnterpCode.join(",");
    }
    if(values.prodNo && values.prodNo.length>0){
      values.prodNo = values.prodNo.join(",");
    }
    values.field = this.props.report.tableFields;
    let seed = 0;
    for(var key in values.field){
      seed+=1;
    }
    if(seed===0){
      message.error("请选择相应的表格字段或者模板");
      return null;
    }
    if(values.renewalTime===''){
      delete values.renewalTime;
    }
    if(values.payAgainFlag===''){
      delete values.payAgainFlag;
    }
    return values;
  }

  renderAdvancedForm() {

    const { getFieldDecorator } = this.props.form;
    const date = new Date();
    const pyear = date.getFullYear();
    const param = {
      showUploadList:false,
      name: 'file',
      beforeUpload: (file) => {
        this.setState({searchLoading:true});
        this.props.dispatch({
          type: 'report/importQuery',
          payload: {file}
        }).then(()=>{
          this.setState({searchLoading:false});
        }).catch((e)=>{
          message.error(e.message || '网络异常');
        });
        return false;
      },
    };
    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch} layout="inline">
        <ChannelAndProduction 
        ChannelRule={{rules:[{ required: true, message:'必填！' }]}}
        onChannelChange={this.onChannelChange.bind(this)} multiple channelFieldKey="extEnterpCode" productionFieldKey="prodNo" {...this.props} allowClear/>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={30}>
              <FormItem label="保单起期">
                {getFieldDecorator('insrncBeginDate')(
                  <RangePicker style={{ width: '100%' }} />
                )}
              </FormItem>
            </Col>
            <Col md={12} sm={30}>
              <FormItem label="保单止期">
                {getFieldDecorator('insrncEndDate')(
                  <RangePicker style={{ width: '100%' }} />
                )}
              </FormItem>
            </Col>
          <Col md={6} sm={30}>
            <FormItem label="保单年份">
            {getFieldDecorator('pYear',{initialValue:pyear})(
              <Select placeholder="请选择" style={{ width: '100%' }}>
              {getSelectChild('pyear')}
              </Select>
            )}
          </FormItem>
          </Col>
           <Col md={6} sm={30}>
            <FormItem label="扣款状态">
              {getFieldDecorator('chargeFlag')(
                <Select allowClear style={{ width: '100%' }} >
                  <Option value='01'>未回盘</Option>
                  <Option value='00'>成功</Option>
                  <Option value='02'>失败</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={30}>
            <FormItem label="是否电子保单">
              {getFieldDecorator('cElcFlag')(
                <Select allowClear style={{ width: '100%' }} >
                  <Option value='1'>是</Option>
                  <Option value='0'>否</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          { this.state.showArea ? (<Col md={6} sm={30}>
            <FormItem label="地区">
              {getFieldDecorator('area')(
                <Select placeholder="请选择" style={{ width: '100%' }} allowClear disabled={this.state.disabledArea}>
                  <Option value={2}>上海招行</Option>
                  <Option value={3}>成都招行</Option>
                </Select>)}
            </FormItem>
          </Col>):null }
        </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={6} sm={30}>
            <FormItem label="是否继续请款">
              {getFieldDecorator('payAgainFlag',{initialValue: ''})(
                <Select allowClear style={{ width: '100%' }} >
                  <Option value='1'>是</Option>
                  <Option value='0'>否</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={30}>
            <FormItem label="保单状态">
              {getFieldDecorator('flag')(
                <Select style={{ width: '100%' }} >
                  <Option value='1'>有效</Option>
                  <Option value='2'>注销</Option>
                  <Option value='3'>退保</Option>
                  <Option value='4'>过期</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={30}>
            <FormItem label="是否为续单">
              {getFieldDecorator('renewalTime',{initialValue: ''})(
                <Select allowClear style={{ width: '100%' }} >
                  <Option value='1'>是</Option>
                  <Option value='0'>否</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={30}>
            <FormItem label="请款类型">
              {getFieldDecorator('payType')(
                <Select style={{ width: '100%' }} >
                  {getSelectChild("payType")}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="实际请款日期">
              {getFieldDecorator('payDate')(
                <RangePicker style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="投保单导入日期">
              {getFieldDecorator('inputDate')(
                <RangePicker style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="退保确认时间">
              {getFieldDecorator('underwritingTime')(
                <RangePicker style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="投保日期">
              {getFieldDecorator('appDate')(
                <RangePicker style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="回盘日期">
              {getFieldDecorator('chargeDate')(
                <RangePicker style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="初次请款日期">
              {getFieldDecorator('payDateFirst')(
                <RangePicker style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Button type='primary' loading={this.state.downloading} onClick={this.handleDownload}>提交</Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
          
            <Button onClick={()=>{
                  let c = 0;
                  for(var key in this.props.report.tableFields){
                    c+=1;
                  }
                  if(c>0){
                    this.setState({
                      visible:true
                    });
                  }else{
                    message.info('请选择相应的字段');
                  }
                  
                }} style={{marginLeft:13}}>保存该搜索过滤器</Button>
                <Upload {...param}>
                  <Button style={{marginLeft:13}} loading={this.state.searchLoading} type='primary'>批量导入查询</Button>
                </Upload>
                <Button style={{marginLeft:13}} type='primary' onClick={this.downloadTemplate}>下载批量导入的模板</Button>
              <a
              onClick={()=>{
                this.setState({
                  showFilterView:!this.state.showFilterView
                });
              }}
             className={Styles.togglebtn}
            >
             <Icon type={this.state.showFilterView?'up':'down'}/>&nbsp;{this.state.showFilterView?'收起':'展开'}
            </a>
        </div>
      </Form>
    );
  }

  handleOk = ()=>{
    const name = this.state.templateName.replace(/(^\s*)|(\s*$)/g, '');
    if (name.length === 0) {
      message.info("请输入模板名称！");
      return;
    }
    this.setState({
      confirmLoading:true
    });
    this.props.dispatch({
      type:'report/addTemplate',
      payload:{
        privilege:this.state.privilege,
        templateName:name,
        dataJs:JSON.stringify(this.props.report.tableFields),
      }
    }).then(()=>{
      message.success('保存成功');
      this.resetTemForm();
    }).catch((e)=>{
      console.error(e);
      this.setState({
        confirmLoading:false,
      });
    });
  }

  resetTemForm = () => {
    this.setState({
      visible:false,
      templateName:'',
      confirmLoading:false,
    });
  }

  handleCancel= ()=>{
    this.resetTemForm();
  }


  handleTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    this.setState({
      pageSize: pagination.pageSize,
      pageNum: pagination.current,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  };

  onTemplatePrivilegeChange = (value) => {
    this.setState({
      privilege:value
    });
  }
  onTemplateNameChange = (e) => {
    this.setState({
      templateName:e.target.value
    });
  }
  getColumns = () => {
    const columns = [
      
    ];
    for(var key in this.props.report.tableFields){
      columns.push({
        title: this.props.report.tableFields[key],
        dataIndex: key,
        align: 'center',
        width:130
      })
    }
    return columns;
  }


  downloadTemplate = () => {
    this.props.dispatch({
      type: 'report/downLoadTemplate'
    });
  }


  render() {
    const { report, tableLoading } = this.props;
    const { visible, confirmLoading } = this.state;
    const data = report.DynamicReportData||{};
    let dataList = [];
    let allcount = 0;
    if(data&&data.pageInfo){
      dataList = data.pageInfo.list;
      allcount = data.pageInfo.total;
    }

  
    // const columns = this.getColumns();
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
            <div>{this.renderAdvancedForm()}</div>
            <FilterView {...this.props} show={this.state.showFilterView}  parent={this}/>
          </div>
        </Card>
        <Modal title="保存模板"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >
           <FormItem label="模板名称">
             <Input onChange={this.onTemplateNameChange.bind(this)} value={this.state.templateName} placeholder='模板名称'/>
           </FormItem>
           <FormItem label="共享给">
              <Select  onChange={this.onTemplatePrivilegeChange.bind(this)} value={this.state.privilege} style={{ width: '100%' }} >
                <Option value='1'>自己</Option>
                <Option value='0'>任何人</Option>
              </Select>
            </FormItem>
        </Modal>
      </PageHeaderLayout>
    );
  }
}
