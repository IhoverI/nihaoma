import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  message,
} from 'snk-web';
import Styles from './index.less';
const { Option } = Select;

export default class Classify extends React.Component {

  constructor(props){
    super(props);
    this.state={
      keyword:''
    }
  }
  componentDidMount(){
    this.props.dispatch({
      type:'report/findAllDynamicLabel'
    }).catch((e)=>{
      message.error("网络异常~请刷新再试");
    });
  }

  SearchChange(v){
    this.setState({
      keyword:v
    });
  }
  itemClick = (item) => {
    let selectedFields = JSON.parse(JSON.stringify(this.props.report.tableFields));
    if(selectedFields[item.fieldId]){
      delete selectedFields[item.fieldId];
    }else{
      selectedFields[item.fieldId] = item.label;
    }
    this.props.dispatch({
      type:'report/updateTableField',
      payload:{
        fileds:selectedFields
      }
    });
  }
  render() {
    const labels = [];
    for (let i = 0, j = this.props.report.DynamicLabelData.length; i < j; i += 1) {
      const item = this.props.report.DynamicLabelData[i];
      let Style = {};
      if(this.props.report.tableFields[item.fieldId]){
        Style.backgroundColor = '#40a9ff';
        Style.color = '#fff';
      }else{
        if(this.state.keyword!==''&&item.label.indexOf(this.state.keyword)>=0){
          Style.backgroundColor = 'gray';
          Style.color = '#fff';
        }
      }
      labels.push(<span onClick={this.itemClick.bind(this,item)} style={Style} className={Styles.ClassifyLabel} key={i}>{item.label}</span>);
    }
    return (
      <div className={Styles.ClassifyLabelWrapper}>
        <Row>
         <Col md={4} sm={24}>(多选)我是分类</Col>
         <Col md={20} sm={24}>
          <Input.Search
            placeholder="分类搜索"
            value={this.state.keyword}
            onChange={(v)=>{this.SearchChange(v.target.value)}}
            onSearch={value => this.SearchChange(value)}
            style={{ width: 200 }}
          />
          <div>
             {labels}
          </div></Col>
        </Row>
      </div>
    );
  }
}
