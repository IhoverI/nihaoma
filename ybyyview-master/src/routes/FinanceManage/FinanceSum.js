import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Table,
  Divider,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild,getpayChannelNameByValue } from '../../utils/utils'

import styles from './FinanceSum.less';

const { RangePicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;

const statusMap = ['default', 'processing', 'success', 'error'];
const status = ['关闭', '运行中', '已上线', '异常'];
const finTypeColumns = [
  {
    title: '序号',
    dataIndex: 'order',
    align: 'center',
  },
  {
    title: '渠道',
    dataIndex: 'extEnterpCode',
    align: 'center',
    render: val => {
      return getpayChannelNameByValue(val);
    },
  },
  {
    title: '请款日期',
    dataIndex: 'payDate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },
  {
    title: '请款总数',
    dataIndex: 'payAmount',
    align: 'center',
  },
  {
    title: '首期请款总数',
    dataIndex: 'payAmountFirst',
    align: 'center',
  },
  {
    title: '续期请款总数',
    dataIndex: 'payAmountAgain',
    align: 'center',
  },
  {
    title: '退款请款总数',
    dataIndex: 'reFundPayAmount',
    align: 'center',
  },
  {
    title: '应请未请总数',
    dataIndex: 'noPayAmount',
    align: 'center',
  },
];

const finLogColumns = [
  {
    title: '序号',
    dataIndex: 'order',
    align: 'center',
  },
  {
    title: '渠道',
    dataIndex: 'extEnterpCode',
    align: 'center',
    render: val => {
      return getpayChannelNameByValue(val);
    },
  },
  {
    title: '请款日期',
    dataIndex: 'payDate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },
  {
    title: '请款总数',
    dataIndex: 'payAmount',
    align: 'center',
  },
  {
    title: '回盘日期',
    dataIndex: 'chargeDate',
    align: 'center',
  },
  {
    title: '回盘总数',
    dataIndex: 'chargeAmount',
    align: 'center',
  },
  {
    title: '扣款成功导入数据库数',
    dataIndex: 'chargeInputAmount',
    align: 'center',
  },
  {
    title: '回盘成功数',
    dataIndex: 'chargeSuccAmount',
    align: 'center',
  },
  {
    title: '回盘不成功数',
    dataIndex: 'chargeFailAmount',
    align: 'center',
  },
  {
    title: '未回盘数',
    dataIndex: 'chargeNotAmount',
    align: 'center',
  },
];

@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.models.searchlist,
}))
@Form.create()
export default class FinanceSum extends PureComponent {
  state = {
    pageSize:10,
    pageNum:1,
    checkType:"1",
    selPicker: '1',
  };

  componentWillMount(){
    Object.keys(this.props.searchlist).forEach(key => {
      if(key !== 'allPolicyQueryData' && key!== 'policyQuerySearchFormData' && key !== 'selfChannelData' && key!== 'policyQueryDefaultTab' && key!== 'policyMapInfo') { delete this.props.searchlist[key] }
    });
  }

  componentDidMount() {
    const { dispatch } = this.props;
  }

  handleTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData();
    });
  };

  getColumns(){
    if(this.state.checkType==="1"){
      return finTypeColumns;
    }
    return finLogColumns;
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    this.setState({pageNum:1,pageSize:10});
    form.resetFields();
  };

  searchData(){
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue = this.perpareValues(fieldsValue);
      this.setState({
        checkType:fieldsValue.checkType
      })
      dispatch({
        type: 'searchlist/SearchFinanceSum',
        payload: fieldsValue,
      }).catch( e =>{
        message.error('网络异常请稍后再试');
      });

    });
  }

  perpareValues(values){
    if(!values.payDate||values.payDate.length===0){
      values.payDateStart = null;
      values.payDateEnd = null;
    }else{
      values.payDateStart = values.payDate[0].format("YYYY-MM-DD");
      values.payDateEnd = values.payDate[1].format("YYYY-MM-DD");
    }
    if(!values.chargeDate||values.chargeDate.length===0){
      values.chargeDateStart = null;
      values.chargeDateEnd = null;
    }else{
      values.chargeDateStart = values.chargeDate[0].format("YYYY-MM-DD");
      values.chargeDateEnd = values.chargeDate[1].format("YYYY-MM-DD");
    }
    delete values.payDate;
    delete values.chargeDate;
    values.pageNum = this.state.pageNum;
    values.pageSize = this.state.pageSize;
    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  handleSearch = e => {
    e.preventDefault();
    this.setState({pageNum:1,pageSize:10});
    this.searchData();
  };

  handleColumnsChange = (val) => {
    this.props.form.resetFields('chargeDate');
    this.setState({
      selPicker: val
    });
  };

  render() {
    const { searchlist: { FinanceSumData },loading } = this.props;
    const { getFieldDecorator } = this.props.form;
    let datalist, total;
    if (FinanceSumData) {
      if (FinanceSumData.code == 0) {
        datalist = FinanceSumData.data.list;
        total = FinanceSumData.data.total;
      } else {
        datalist = [];
        total = 0;
      }
    }

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
            <Form className='custom-form-wrapper'  onSubmit={this.handleSearch} layout="inline">
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <FormItem label="扣款渠道">
                {getFieldDecorator('extEnterpCodeList')(
                <Select
                placeholder="请选择"
                style={{ width: '100%' }}
                >
                  {getSelectChild('payChannel1',null,null,true)}
                </Select>
                )}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
              <FormItem label="清单类型">
                {getFieldDecorator('checkType', {
                    initialValue:"1",
                  })(
                    <Select
                    placeholder="请选择"
                    style={{ width: '100%' }}
                    onChange = {this.handleColumnsChange}
                    >
                      {getSelectChild('financeSumDesc')}
                    </Select>
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <FormItem label="请款日期">
                  {getFieldDecorator('payDate')(
                    <RangePicker style={{ width: '100%' }}/>)}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label="扣款回盘日期">
                  {getFieldDecorator('chargeDate')(
                    <RangePicker disabled={this.state.selPicker === '1' ? true : false } style={{ width: '100%' }}/>)}
                </FormItem>
              </Col>
            </Row>
            <div className={styles.financeSumOperator} style={{ overflow: 'hidden',textAlign:'center' }}>
              <span style={{ marginBottom: 24 }}>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </span>
            </div>
            </Form>
            <div className={styles.financeSumOperator}>
            <Table
              loading={loading}
              rowKey={record => record.order}
              dataSource={datalist}
              bordered
              size="small"
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                ...{
                  current:this.state.pageNum,
                  pageSize:this.state.pageSize,
                  total:total
                },
              }}
              columns={this.getColumns()}
              onChange={this.handleTableChange}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
