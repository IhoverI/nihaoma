import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Table,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Popconfirm,
  Upload,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild,getChannelNameByValue } from '../../utils/utils'
import UploadForm from './uploadForm';

import styles from './FinanceList.less';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

const FormItem = Form.Item;

@connect(({ financeList,loading }) => ({
  financeList,
  loading: loading.models.financeList,
}))
@Form.create()
export default class FinanceList extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    extEnterp: ''
  };

  componentWillMount(){
    Object.keys(this.props.financeList).forEach(key => delete this.props.financeList[key]);
  }

  componentDidMount() {
    const { dispatch } = this.props;
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(fieldsValue.fileTimeList[1].diff(fieldsValue.fileTimeList[0],'day')>31){
        message.warn("[请款文件生成日期]间隔需要小于31天！");
        return;
      }
      const fileTimeList = [fieldsValue.fileTimeList[0].format("YYYYMMDD"),fieldsValue.fileTimeList[1].format("YYYYMMDD")]
      dispatch({
        type: 'financeList/SearchFinanceList',
        payload: {
          fileTypeList: fieldsValue.financeFile,
          fileChannel: fieldsValue.extEnterp,
          fileTimeList,
        },
      });
    });
  };

  handleModalVisible(){
    this.setState({
      modalVisible: false,
    });
  }


  onUploadSuccess(){
    this.handleModalVisible();
  }


  downloadConfirm = record =>{
    this.props.dispatch({
      type: 'financeList/financeListDownload',
      payload: {
        pathFordownload: record.path,
        fileNameFordownload:record.fileName
      },
    });
  }

  render() {
    const { loading, financeList: { financeList } } = this.props;
    const { selectedRows, modalVisible } = this.state;
    const { getFieldDecorator } = this.props.form;

    let data;
    if (financeList) {
      if (financeList.code == 0) {
        data = financeList.data;
      } else {
        data = [];
      }
    }

    const columns = [
      {
        title: '序号',
        dataIndex: 'order',
        align: 'center',
        key: 1
      },
      {
        title: '渠道名称',
        dataIndex: 'fileChannel',
        align: 'center',
        render:(val)=>{
          return getChannelNameByValue(val);
        },
        key: 2
      },
      {
        title: '清单类型',
        dataIndex: 'fileType',
        align: 'center',
        key: 3
      },
      {
        title: '清单报表名称',
        dataIndex: 'fileName',
        align: 'center',
        key: 4
      },
      {
        title: '导入日期',
        dataIndex: 'date',
        align: 'center',
        key: 5
      },
      {
        title: '操作',
        align: 'center',
        key: 6,
        render: (e,record) => (
          <Fragment>
            <Popconfirm placement="topLeft" title={"确认下载吗？"} onConfirm={this.downloadConfirm.bind(this,record)} okText="确定" cancelText="取消">
              <a>下载</a>
           </Popconfirm>
          </Fragment>
        ),
      },
    ];

    const param = {
      showUploadList:false,
      name: 'file',
      action: `${SERVERLOCAL}ybyy-policy/policy/report/importUnionPayDetail`,
      withCredentials: true,
      onChange(info) {
        if (info.file.status === 'done') {
          if (info.file.response.code === 0) {
            message.success(`《${info.file.name}》: ${info.file.response.message}`, 2);
          } else {
            message.error(`《${info.file.name}》上传失败: ${info.file.response.message}`, 4);
          }
        } else if (info.file.status === 'error') {
          message.error(`《${info.file.name}》: 网络异常.`);
        }
        
      }
    };

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
            <Form className='custom-form-wrapper'  onSubmit={this.handleSearch}>
            <Row gutter={16}>
              <Col md={12} sm={24}>
                <FormItem label="扣款渠道">
                {getFieldDecorator('extEnterp', {
                    rules: [{ required: true, message: '必填' }],
                  })(
                <Select
                placeholder="请选择"
                style={{ width: '100%' }}
                >
                  {getSelectChild('payChannel1',null,null,true)}
                </Select>
                )}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
              <FormItem label="清单类型">
                {getFieldDecorator('financeFile', {
                  rules: [{ required: true, message: '必填' }],
                })(
                <Select
                mode={'multiple'}
                allowClear
                placeholder="请选择"
                style={{ width: '100%' }}
                >
                  <Select.Option key='1+100010'>请款文件</Select.Option>
                  <Select.Option key='2+100010'>退保请款文件</Select.Option>
                  <Select.Option key='3+100010'>代付请款文件</Select.Option>
                  <Select.Option key='1+100011'>请款回盘文件</Select.Option>
                  <Select.Option key='2+100011'>退保退费回盘文件</Select.Option>
                  <Select.Option key='1+100032'>强扣请款文件</Select.Option>
                  <Select.Option key='1+100033'>强扣回盘文件</Select.Option>
                </Select>
                )}
                </FormItem>
              </Col>
              <Col md={12} sm={30}>
                <FormItem label="请款文件生成日期">
                  {getFieldDecorator('fileTimeList', {
                    rules: [{ required: true, message: '必填' }],
                  })(
                    <DatePicker.RangePicker  disabled={this.state.disabledAppDate} style={{ width: '100%' }} />
                  )}
                </FormItem>
              </Col>
            </Row>
            <div className={styles.financeListOperator} style={{ overflow: 'hidden',textAlign:'center' }}>
              <span style={{ marginBottom: 24 }}>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={()=>{
                  this.setState({modalVisible:true});
                }}>
                  上传
                </Button>
                <Upload {...param}>
                  <Button style={{ marginLeft: 8 }}>
                    <Icon type="upload" />上传银联交易明细
                  </Button>
                </Upload>
              </span>
            </div>
            </Form>
            <div className={styles.financeListOperator}>
            <Table
              loading={loading}
              dataSource={data}
              columns={columns}
              pagination={false}
            />
          </div>
          <Modal
            title="上传文件"
            width={400}
            footer={null}
            style={{ top: 20 }}
            onCancel={() => this.handleModalVisible(false)}
            visible={modalVisible}
          >
            <UploadForm 
              dispatch={this.props.dispatch}
              onUploadSuccess={this.onUploadSuccess.bind(this)}
              onCancel={() => this.handleModalVisible(false)} /> 
          </Modal>
        </Card>
      </PageHeaderLayout>
    );
  }
}
