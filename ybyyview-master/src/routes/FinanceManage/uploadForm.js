import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { getDataSource,getSelectChild } from '../../utils/utils'
import {
  Form,
  Row,
  Col,
  Input,
  DatePicker,
  Select,
  Button,
  Card,
  InputNumber,
  Radio,
  Icon,
  Tooltip,
  message,
  Collapse,
  Upload,
} from 'snk-web';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const serverHost = SERVERLOCAL;


@Form.create()
export default class UploadFile extends PureComponent {

  constructor(props){
    super(props);
    this.state={
      fileList:[],
      loading:false
    }
  }
  componentDidMount(){
  }

  handleSubmit = e => {
    e.preventDefault();
    const {dispatch,form} = this.props;
    if(this.state.fileList.length!==1){
      message.info('请选择上传的文件');
      return;
    }
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({
          loading:true
        });
        values.uploadFile = this.state.fileList[0];
        dispatch({
          type: 'financeList/uploadFTPFile',
          payload: values,
        }).then(()=>{
          message.success('上传成功');
          form.resetFields();
          this.setState({
            fileList:[],
            loading:false
          });
          this.props.onUploadSuccess();
        }).catch(e=>{
          this.setState({
            loading:false
          });
          console.log(e.message);
          message.error(e.message||'网络异常请稍后再试');
        });
      }
    });
  };



  onCancel = e => {
    this.props.form.resetFields();
    this.props.onCancel();
  };


  render() {

    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { loading,quartzmanage } = this.props;
    const prams = {
      showUploadList:true,
      action: '',
      onRemove: (file) => {
        this.setState(({ fileList }) => {
          const index = fileList.indexOf(file);
          const newFileList = fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: (file) => {
        this.setState(({ fileList }) => ({
          fileList: [file],
        }));
        return false;
      },
      fileList: this.state.fileList,
    }
    return (
        <Form style={{ marginTop: 8 }}  onSubmit={this.handleSubmit.bind(this)} >
          <Row gutter={16}>
              <Col md={24}>
                <FormItem label="渠道名称">
                {getFieldDecorator('fileChannel', {
                    rules: [{ required: true, message: '必填' }],
                  })(
                <Select
                placeholder="请选择"
                style={{ width: '100%' }}
                >
                  {getSelectChild('extEnterpDesc',null,null,true)}
                  <Option value='95516'>95516-银联</Option>
                  <Option value='90001'>90001-快钱</Option>
                </Select>
                )}
                </FormItem>
              </Col>
              <Col md={24}>
              <FormItem label="清单类型">
                {getFieldDecorator('fileType', {
                  rules: [{ required: true, message: '必填' }],
                })(
                <Select
                allowClear
                placeholder="请选择"
                style={{ width: '100%' }}
                >
                  <Select.Option key='1+100011'>请款回盘文件</Select.Option>
                  <Select.Option key='2+100011'>退保退费回盘文件</Select.Option>
                  <Select.Option key='1+100033'>强扣回盘文件</Select.Option>
                </Select>
                )}
                </FormItem>
              </Col>
            </Row>
            <Upload {...prams}>
              <Button>
                <Icon type="upload" /> 选择文件
              </Button>
            </Upload>
            <div style={{ margin: 12 }}>
              <Button loading={this.state.loading} type="primary" htmlType="submit">
                上传
               </Button>
              <Button style={{ margin: 12 }} onClick={this.onCancel}>
                取消
              </Button>
            </div>
        </Form>
    );
  }
}
