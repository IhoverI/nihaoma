import React, { PureComponent, Fragment } from 'react';
import { Table, Alert } from 'snk-web';
import styles from './index.less';

function initTotalList(columns) {
  const totalList = [];
  columns.forEach(column => {
    if (column.needTotal) {
      totalList.push({ ...column, total: 0 });
    }
  });
  return totalList;
}

class FinanceListTable extends PureComponent {
  constructor(props) {
    super(props);
    const { columns } = props;
    const needTotalList = initTotalList(columns);

    this.state = {
      selectedRowKeys: [],
      needTotalList,
    };
  }

  componentWillReceiveProps(nextProps) {
    // clean state
    if (nextProps.selectedRows.length === 0) {
      const needTotalList = initTotalList(nextProps.columns);
      this.setState({
        selectedRowKeys: [],
        needTotalList,
      });
    }
  }

  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    let needTotalList = [...this.state.needTotalList];
    needTotalList = needTotalList.map(item => {
      return {
        ...item,
        total: selectedRows.reduce((sum, val) => {
          return sum + parseFloat(val[item.dataIndex], 10);
        }, 0),
      };
    });

    if (this.props.onSelectRow) {
      this.props.onSelectRow(selectedRows);
    }

    this.setState({ selectedRowKeys, needTotalList });
  };

  render() {
    const { selectedRowKeys, needTotalList } = this.state;
    const { data, loading, columns } = this.props;
    // const paginationProps = {
    //   showSizeChanger: true,
    //   showQuickJumper: true,
    //   defaultPageSize: 2,
    //   ...{
    //       current:this.state.pageNum,
    //       pageSize:this.state.pageSize,
    //   },
    // };

    return (
      <div className={styles.financeListTable}>
        <Table
          loading={loading}
          rowKey={record => record.key}
          dataSource={data}
          columns={columns}
          // pagination={paginationProps}
          onChange={this.handleTableChange}
          bordered
        />
      </div>
    );
  }
}

export default FinanceListTable;
