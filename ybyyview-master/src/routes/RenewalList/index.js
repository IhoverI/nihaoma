import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './index.less';
import { getDataSource,getSelectChild,getChannelNameByValue,getProductNameByValue,getProductNameByExtEnterpCodeAndProdNo } from '../../utils/utils'

const { RangePicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;

const formaUnit = (val,code) => {
  let text = '';
  if (code === 'flag') {
    switch (val) {
      case '1': text = '有效'; break;
      case '2': text = '注销'; break;
      case '3': text = '退保'; break;
      case '4': text = '过期'; break;
      default: break;
    }
  } else if (code === 'insrncSex') {
    switch (val) {
      case '106001': text = '男';break;
      case '106002': text = '女';break;
      case 'M': text = '男';break;
      case 'F': text = '女';break;
      default: break;
    }
  } else {
    switch (val) {
      case '1': text = '取消续保'; break;
      case '2': text = '向招行申请续保'; break;
      case '3': text = '招行反馈续保不寄送续保通知函'; break;
      case '4': text = '招行反馈续保且寄送续保通知函'; break;
      case '6': text = '已生成续保单'; break;
      default: break;
    }
  }
  
    return text;
}

const formatBackStatus = (value) => {
  switch (value) {
    case "00": return "是"; break;
    default: return "否"; break;
  }
}

const columns = [
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '业务渠道',
    dataIndex: 'extEnterpCode',
    align: 'center',
    render:(val)=>{
      return getChannelNameByValue(val);
    },
  },
  {
    title: '产品名称',
    dataIndex: 'prodNo',
    align: 'center',
     render:(text, record)=>{
      return getProductNameByExtEnterpCodeAndProdNo(record.extEnterpCode, text);
    },
  },
  {
    title: '投保人姓名',
    dataIndex: 'cAppNme',
    align: 'center',
  },
  {
    title: '保单状态',
    dataIndex: 'flag',
    align: 'center',
    render: (text) => {
      return (<span>{formaUnit(text,'flag')}</span>)
    }
  },
  {
    title: '续保状态',
    dataIndex: 'renewalFlag',
    align: 'center',
  },
  {
    title: '保险止期',
    dataIndex: 'insrncEndDate',
    align: 'center',
    render(val){
      return <div> {moment(val).format("YYYY-MM-DD")} </div>
    },
  },
  {
    title: '投保金额',
    dataIndex: 'prm',
    align: 'center',
  },
  {
    title: '最后扣款成功的期次',
    dataIndex: 'period',
    align: 'center',
  },
];

const columns1 = [
  {
    title: '保单编号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '保险止期',
    dataIndex: 'insrncEndDate',
    align: 'center',
    render(val){
      return <div> {moment(val).format("YYYY-MM-DD")} </div>
    },
  },
  {
    title: '被保险人姓名',
    dataIndex: 'cInsrntCnm',
    align: 'center',
  },

  {
    title: '保险产品',
    dataIndex: 'prodName',
    align: 'center',
  },
  {
    title: '续保通知函类型',
    dataIndex: 'noticeType',
    align: 'left',
  },
  
  {
    title: '快递地址',
    dataIndex: 'insrncAddress',
    align: 'center',
  },
  {
    title: '邮编',
    dataIndex: 'zipCode',
    align: 'center',
  },
];

// 首期无请款续保单
const columns3 = [
  {
    title: '渠道',
    dataIndex: 'extEnterpCode',
    align: 'center',
    render:(val)=>{
      return getChannelNameByValue(val);
    },
  },
  {
    title: '新保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '旧保单号',
    dataIndex: 'cPlyNoOld',
    align: 'center',
  },

  {
    title: '新订单号',
    dataIndex: 'cPlyAppNo',
    align: 'center',
  },
  {
    title: '旧订单号',
    dataIndex: 'cPlyAppNoOld',
    align: 'center',
  },
  
  {
    title: '投保人姓名',
    dataIndex: 'cAppNme',
    align: 'center',
  },
  {
    title: '续保单保险起期',
    dataIndex: 'insrncBeginDate',
    align: 'center',
    render(val){
      return <div> {moment(val).format("YYYY-MM-DD")} </div>
    },
  },
  {
    title: '产品',
    dataIndex: 'prodNo',
    align: 'center',
     render:(text, record)=>{
      return getProductNameByExtEnterpCodeAndProdNo(record.extEnterpCode, text);
    },
  },
  {
    title: '保费',
    dataIndex: 'prm',
    align: 'center',
  },
  {
    title: '上年度扣款成功保费',
    dataIndex: 'totalPRM',
    align: 'center',
  },
  {
    title: '上年度扣款成功期次',
    dataIndex: 'periodLast',
    align: 'center',
  },
  {
    title: '最后请款日期',
    dataIndex: 'payDate',
    align: 'center',
    render(val){
      return <div> {moment(val).format("YYYY-MM-DD HH:mm:ss")} </div>
    },
  },

  {
    title: '身份证',
    dataIndex: 'insrncCardNo',
    align: 'center',
  },
  {
    title: '联系方式',
    dataIndex: 'cMobileNo',
    align: 'center',
  },
  {
    title: '被保险人夜间电话',
    dataIndex: 'insrncNightPhone',
    align: 'center',
  },

  {
    title: '投递地址',
    dataIndex: 'insrncAddress',
    align: 'center',
  },
  {
    title: '续保次数',
    dataIndex: 'renewalTime',
    align: 'center',
  },
  {
    title: '首次保险起期',
    dataIndex: 'insrncBeginDateFirst',
    align: 'center',
    render(val){
      return <div> {moment(val).format("YYYY-MM-DD")} </div>
    },
  },
];

const columns2 = [
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '取消续保时间',
    dataIndex: 'cancelRenewalDate',
    align: 'center',
    render(val){
      return <div> {moment(val).format("YYYY-MM-DD HH:mm:ss")} </div>
    },
  },
  {
    title: '取消续保原因',
    dataIndex: 'noRenwalReason',
    align: 'center',
  },

  {
    title: '客户姓名',
    dataIndex: 'cInsrntCnm',
    align: 'center',
  },
   {
    title: '身份证',
    dataIndex: 'insrncCardNo',
    align: 'center',
  },
  {
    title: '性别',
    dataIndex: 'insrncSex',
    align: 'center',
  },
  {
    title: '城市',
    dataIndex: 'area',
    align: 'center',
  },
  {
    title: '手机号',
    dataIndex: 'cMobileNo',
    align: 'center',
  },
  {
    title: '险种',
    dataIndex: 'prodNo',
    align: 'center',
  },
   {
    title: '款别',
    dataIndex: 'cTgtFld1',
    align: 'center',
  },
   {
    title: '保费',
    dataIndex: 'prm',
    align: 'center',
  },
   {
    title: '保险止期',
    dataIndex: 'insrncEndDate',
    align: 'center',
    render(val){
      return <div> {moment(val).format("YYYY-MM-DD")} </div>
    },
  },

   {
    title: '是否续单',
    dataIndex: 'checkRenwal',
    align: 'center',
  },
   {
    title: '续保次数',
    dataIndex: 'renewalTime',
    align: 'center',
  },
   {
    title: '最后发起请款期次',
    dataIndex: 'latestPeriod',
    align: 'center',
  },
   {
    title: '是否请款',
    dataIndex: 'checkAsk',
    align: 'center',
  },
  {
    title: '是否扣款',
    dataIndex: 'checkDecode',
    align: 'center',
    render: (text) => {
      return (<span>{formatBackStatus(text)}</span>)
    }
  },
];


@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.models.searchlist,
}))
@Form.create()
export default class ApplyList extends PureComponent {
  state = {
    formValues: {},
    pageNum: 1,
    pageSize: 10,
    selOrderAtrr: [],
    selectedRows: [],
    selectedRowKeys: [],
    scrollX: 0,
    extEnterpCodeFlag: '',
    insrncedDateFlag: '',
    insrncDateFlag: '',
    cancelDateFlag: '',
    tableFlag: '1',
    types: '',
  };

  componentWillMount(){
    Object.keys(this.props.searchlist).forEach(key => {
      if(key !== 'allPolicyQueryData' && key!== 'policyQuerySearchFormData' && key !== 'selfChannelData' && key!== 'policyQueryDefaultTab' && key!== 'policyMapInfo') { delete this.props.searchlist[key] }
    });
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
     this.setState({
      extEnterpCodeFlag: '',
      insrncedDateFlag: '',
      insrncDateFlag: '',
      cancelDateFlag: '',
      selectedRowKeys:[],
      selectedRows:[]
    });
  };

  handleSearch = e => {
    this.selType();
    e.preventDefault();
    this.setState({
      pageNum: 1,
      pageSize: 10,
      selectedRowKeys:[],
      selectedRows:[]
    },()=>{
      this.searchData(1,10);
    });
  };

  searchData = (pageNum,pageSize) => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (fieldsValue.insrncDate.length > 0) {
        if(fieldsValue.insrncDate[1].diff(fieldsValue.insrncDate[0],'day')>31){
          message.warn("[保险起期生成日期]间隔需要小于31天！");
          return;
        }
      }
      if (fieldsValue.insrncedDate.length > 0) {
        if(fieldsValue.insrncedDate[1].diff(fieldsValue.insrncedDate[0],'day')>31){
          message.warn("[保险止期生成日期]间隔需要小于31天！");
          return;
        }
      }
      if (fieldsValue.cancelDate.length > 0) {
        if(fieldsValue.cancelDate[1].diff(fieldsValue.cancelDate[0],'day')>31){
          message.warn("[取消续保生成日期]间隔需要小于31天！");
          return;
        }
      }
      
      fieldsValue = this.perpareValues(fieldsValue);
      this.setState({
        formValues:fieldsValue
      });
      dispatch({
        type: 'searchlist/SearchRenewalList',
        payload: {
          ...fieldsValue,
          pageNum: pageNum,
          pageSize: pageSize
        },
      }).then(()=>{});
    });
  }

  perpareValues(values){
    let { insrncDate, insrncedDate, cancelDate, prodNameList } = values;
    let newValues = {};
    if(!insrncDate||insrncDate.length===0){
      newValues = {
        insrncBeginDateStart: '',
        insrncBeginDateEnd: '',
        ...values,
      };
    }else{
      newValues = {
        insrncBeginDateStart: insrncDate[0].format("YYYY-MM-DD"),
        insrncBeginDateEnd: insrncDate[1].format("YYYY-MM-DD"),
        ...values,
      };
    }

    if(!insrncedDate||insrncedDate.length===0){
      newValues = {
        insrncEndDateStart: '',
        insrncEndDateEnd: '',
        ...newValues,
      };
    }else{
      newValues = {
        insrncEndDateStart: insrncedDate[0].format("YYYY-MM-DD"),
        insrncEndDateEnd: insrncedDate[1].format("YYYY-MM-DD"),
        ...newValues,
      };
    }

    if(!cancelDate||cancelDate.length===0){
      newValues = {
        cancelRenewalDateStart: '',
        cancelRenewalDateEnd: '',
        ...newValues,
      };
    }else{
      newValues = {
        cancelRenewalDateStart: cancelDate[0].format("YYYY-MM-DD"),
        cancelRenewalDateEnd: cancelDate[1].format("YYYY-MM-DD"),
        ...newValues,
      };
    }

    if (!prodNameList) {
      newValues = {
        prodNo: '',
        ...newValues,
      };
    } else {
      newValues = {
        prodNo: prodNameList,
        ...newValues,
      };
    }

    delete newValues.insrncDate;
    delete newValues.insrncedDate;
    delete newValues.cancelDate;
    delete newValues.prodNameList;

    return newValues;
  }

  handleTablePage =(pagination, filters, sorter) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
      selectedRowKeys: [],
      selectedRows: [],
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }

  handleDownload = () => {
    this.props.dispatch({
        type: 'searchlist/QueryDownload',
        payload: {
          ...this.state.formValues,
        },
    }).then(()=>{
    }).catch(e=>{
      message.error('网络异常请稍后再试');
    });
  }

  selType = () => {
   let val = this.props.form.getFieldValue('type');
    if (val === '1') {
      this.setState({
        tableFlag: '1',
        scrollX: 0,
      });
    } else if (val === '2'){
      this.setState({
        tableFlag: '2',
        scrollX: 0,
      });
    } else if (val === '3') {
      this.setState({
        tableFlag: '3',
        scrollX: 2000,
      });
    } else {
       this.setState({
        tableFlag: '4',
        scrollX: 2200,
      });
    }
  }

  typeChange = (value) => {
      this.setState({types:value});
      if (value === '2' || value === '4') {
        this.setState({extEnterpCodeFlag: '1'});
      } else if (value === '1' || value === '3') {
        this.setState({extEnterpCodeFlag: '0'});
      }
      if (value === '1' || value === '2' || value === '3') {
        this.setState({insrncedDateFlag: '1'});
      } else if ( value === '4') {
        this.setState({insrncedDateFlag: '0'});
      }
      value === '4' ? this.setState({insrncDateFlag: '1'}) : this.setState({insrncDateFlag: '0'});
      value === '3' ? this.setState({cancelDateFlag: '1'}) : this.setState({cancelDateFlag: '0'});
      
      if (value === '1') {
        this.props.form.setFieldsValue({insrncDate: '', cancelDate: '', extEnterpCode: ''});
      } else if (value === '2') {
        this.props.form.setFieldsValue({insrncDate: '', cancelDate: ''});
      } else if (value === '3') {
        this.props.form.setFieldsValue({insrncDate: '', insrncedDate: '', extEnterpCode: ''})
      } else if (value === '4') {
        this.props.form.setFieldsValue({cancelDate: '', insrncedDate: ''})
      }
  }

  

  renderAdvancedForm() {

    const { getFieldDecorator } = this.props.form;
    const date = new Date();
    const pyear = date.getFullYear();
    let insrncedDateRules,cancelDateRules,insrncDateRules;
    if (this.state.types === '1' || this.state.types === '2') {
       insrncedDateRules = {rules:[{ required: true, message: '必填！' }]}
    } else {
      insrncedDateRules = {}
    }
    if (this.state.types === '3') {
      cancelDateRules = {rules:[{ required: true, message: '必填！' }]}
    } else {
      cancelDateRules = {};
    }
    if (this.state.types === '4') {
      insrncDateRules= {rules:[{ required: true, message: '必填！' }]}
    } else {
      insrncDateRules = {};
    }

    return (
      <div>
        <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch} layout="inline">
           <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
              <Col md={6} sm={30}>
                <FormItem label="保单年份">
                {getFieldDecorator('pyear',{initialValue:pyear})(
                  <Select placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild('pyear')}
                  </Select>
                )}
              </FormItem>
              </Col>
               <Col md={6} sm={24}>
              <FormItem label="清单类型">
                  {getFieldDecorator('type',{initialValue: '',rules:[{ required: true, message: '必选！' }]})(
                    <Select placeholder="请选择" style={{ width: '100%' }} onChange = {this.typeChange.bind(this)}>
                      {getSelectChild('checkType')}
                    </Select>)}
              </FormItem>
              </Col>
              <Col md={12} sm={24}>
              <FormItem label="取消续保时间">
                {getFieldDecorator('cancelDate',cancelDateRules)(
                  <RangePicker style={{ width: '100%' }} disabled={ this.state.cancelDateFlag === '0' ? true : false }/>
                )}
              </FormItem>
              </Col>
          </Row> 
          <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
              <Col md={12} sm={30}>
              <FormItem label="保险起期">
                {getFieldDecorator('insrncDate',insrncDateRules)(
                  <RangePicker style={{ width: '100%' }} disabled={ this.state.insrncDateFlag === '0' ? true : false }/>
                )}
              </FormItem>
              </Col>
             <Col md={12} sm={30}>
                <FormItem label="保险止期">
                  {getFieldDecorator('insrncedDate',insrncedDateRules)(
                    <RangePicker style={{ width: '100%' }} disabled={ this.state.insrncedDateFlag === '0' ? true : false }/>
                  )}
                </FormItem>
              </Col>       
          </Row>

          <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
          </div>
        </Form>
      </div>
    );
  }

  getColumns(){
      if (this.state.tableFlag === '1') {
        return columns;
      } else if(this.state.tableFlag === '2'){
        return columns1;
      } else if (this.state.tableFlag === '3'){
        return columns2;
      } else {
        return columns3
      } 
  }

  render() {
    const { searchlist } = this.props;
    const { searchlist: { renewalList }, loading } = this.props;
    const { scrollX } = this.state;
    let data = [];
    let total;
    if(renewalList){
      if(renewalList.code == 0){
        data = renewalList.data.list;
        total = renewalList.data.total;
      }
    }
    const { selectedRowKeys, selectedRows } = this.state;
    const columns = this.getColumns();
    const rowSelection = {
      selectedRowKeys, 
      selectedRows,
       onChange: (selectedRowKeys, selectedRows) => {
         this.setState({ selectedRowKeys,selectedRows });
        },
    };
    const parentMethods = {
      handleAdd: this.handleAdd,
    };

    const rowKey = (record, index) => {
      return index
    }

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div className={styles.ApplyList}>
            <div className={styles.ApplyListForm}>{this.renderAdvancedForm()}</div>
            <Table
              loading={loading}
              scroll={{x: scrollX}}
              rowKey={rowKey}
              columns={columns}
              dataSource={data}
              bordered
              title={()=>{
                return <Button icon='download' type='primary' style={{marginRight:10}} onClick={this.handleDownload}>下载导出</Button>
              }}
              size="small"
              pagination={{
                showQuickJumper: true,
                showSizeChanger: true,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                total:total,
                ...{
                    current:this.state.pageNum,
                    pageSize:this.state.pageSize,
                    pageSizeOptions:['10','20','30','40','50'],
                }
              }}
              onChange={this.handleTablePage}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
