import React, { Fragment , PureComponent,} from 'react';
import { Button, Row, Col, Icon, Steps, Card } from 'snk-web';
import Result from 'components/Result';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';


/**
  * 跳转实现路由重新加载
  * 快速刷新，清空当前页数据
  */

export default class BasicAudit extends PureComponent  {
  componentWillMount(){
    this.props.history.push({
      pathname: '/policy-print/apply-resend',
    });
  }
  
  render() {
    return (
      <PageHeaderLayout>
        <Card bordered={false}>
          <Result
            title="跳转中..."
            description="此空白页面用于跳转，实现当前页面快速刷新。"
          />
        </Card>
      </PageHeaderLayout>
    )
  }
}
