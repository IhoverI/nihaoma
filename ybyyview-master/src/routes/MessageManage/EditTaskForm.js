import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form,
  Row,
  Col,
  Input,
  DatePicker,
  Select,
  Button,
  Card,
  InputNumber,
  Radio,
  Icon,
  Tooltip,
  message,
  Collapse,
} from 'snk-web';
import { getSelectChild } from '../../utils/utils';
import ChannelAndProduction from 'components/ChannelAndProduction';
import moment from 'moment';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;


@connect(({ messageManage,loading }) => ({
  messageManage,
  loading: loading.models.messageManage,
}))
@Form.create()
export default class EditTaskForm extends PureComponent {
  constructor(props){
    super(props);
    this.state={
      record:props.record
    };
  }
  componentDidMount(){}

  componentWillReceiveProps(nextProps){
    if(JSON.stringify(nextProps.record)!==JSON.stringify(this.state.record)){
    this.setState({record:nextProps.record});
    }
  }
  handleSubmit = e => {
    this.props.root.handleSubmit(e,this.props.form);
  };

  onCancel = e => {
    this.props.root.onCancel(e,this.props.form);
  };

  format = (value,code) => {
    let text = '';
    if (code === 'appSex' || code === 'insrncSex') {
      switch (value) {
        case "106001": text = "男"; break;
        case "106002": text = "女"; break;
        case "01": text = "男"; break;
        case "02": text = "女"; break;
        default:break;
      }
    } else if (code === 'feeType1') {
        switch (value) {
            case "1": text = "月缴"; break;
            default:break;
        }
    }
    return text;
  }

  getExtEnterpName = (extEnterCode) => {
    let extEnterName;
    if (getSelectChild('extEnterpDesc')) {
        for (const i in getSelectChild('extEnterpDesc')) {
        if (getSelectChild('extEnterpDesc')[i].props.value === extEnterCode) {
            extEnterName = getSelectChild('extEnterpDesc')[i].props.children;
        }
        }
        return extEnterName;
    }
  }

  getProName = (proNo,extEnterCode) => {
  let proName;
  if (getSelectChild('prodDesc')) {
    for (const i in getSelectChild('prodDesc',null,null,false,'group')) {
      if (getSelectChild('prodDesc')[i].props.value === proNo
    && getSelectChild('prodDesc',null,null,false,null,'group')[i].ref === extEnterCode) {
        proName = getSelectChild('prodDesc')[i].props.children;
      }
    }
    return proName;
  }
}

getRiskOption(){
    const curRiskCode = this.props.form.getFieldValue('extEnterpCode');
    if(this.props.multiple){
      var re = [];
      for(var i=0,j=curRiskCode.length;i<j;i+=1){
        re.push(
          getSelectChild('prodDesc',null,(index,source)=>{
            if(source[index].group!==curRiskCode[i]){
              return false;
            }
            if(this.props.productOnSale){
              if(source[index].state!=='在售'){
                return false;
              }
            }
            return true;
          },true)
        );
      }
      return re;
    }
    return getSelectChild('prodDesc',null,(index,source)=>{
      if(source[index].group!==curRiskCode){
        return false;
      }
      if(this.props.productOnSale){
        if(source[index].state!=='在售'){
          return false;
        }
      }
      return true;
    },true);
  }

  render() {
    const { loading,form } = this.props;
    const { getFieldDecorator, getFieldValue } = form;
    const Panel = Collapse.Panel;
    const { record } = this.state;
    return (
        <Form className='custom-form-wrapper' layout="inline">
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保单号'>
                    {getFieldDecorator('cPlyAppNo',{initialValue: record === undefined ? '' : record.cPlyAppNo })(
                      <Input />
                )}  
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='订单号'>
                    {getFieldDecorator('transationCode',{initialValue: record === undefined ? '' : record.transationCode })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='原订单号'>
                     {getFieldDecorator('oldProposalNo',{initialValue: record === undefined ? '' : record.oldProposalNo })(
                    <Input /> 
                     )}                 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='卡号'>
                    {getFieldDecorator('cardId',{initialValue: record === undefined ? '' : record.cardId })(
                      <Input />
                )}  
                  </Form.Item>
                </Col>
                 <Col md={8} lg={8} sm={16}>
                  <Form.Item label='渠道名称'>
                    {getFieldDecorator('extEnterpCode',{initialValue: record === undefined ? '' : this.getExtEnterpName(record.extEnterpCode) })(
                      <Select>
                          {getSelectChild('extEnterpDesc')}
                      </Select>
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='产品名称'>
                     {getFieldDecorator('prodNo',{initialValue: record === undefined ? '' : this.getProName(record.prodNo,record.extEnterpCode) })(
                        <Select>
                            {this.getRiskOption()}
                        </Select>
                     )}                 
                  </Form.Item>
                </Col> 
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                   <Form.Item label='信用卡发卡日期'>
                     {getFieldDecorator('cardDate',{initialValue: record.cardDate === null ? null : moment(record.cardDate, 'YYYY-MM-DD')})(
                       <DatePicker style={{ width: '100%' }}/> 
                    )}   
                  </Form.Item> 
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='信用卡种类'>
                    {getFieldDecorator('cardType',{initialValue: record === undefined ? '' : record.cardType })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='商户代码'>
                     {getFieldDecorator('code',{initialValue: record === undefined ? '' : record.code })(
                    <Input /> 
                     )}                 
                  </Form.Item>
                </Col>
              </Row>
               <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='产品数量'>
                    {getFieldDecorator('prodNum',{initialValue: record === undefined ? '' : record.prodNum })(
                      <Input />
                )}  
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='产品档次'>
                    {getFieldDecorator('prodType',{initialValue: record === undefined ? '' : record.prodType })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='销售方式'>
                     {getFieldDecorator('saleType',{initialValue: record === undefined ? '' : record.saleType })(
                    <Input /> 
                     )}                 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='导入日期'>
                    {getFieldDecorator('inputDate',{initialValue: record.inputDate === null ? null : moment(record.inputDate, 'YYYY-MM-DD')})(
                        <DatePicker style={{ width: '100%' }}/> 
                    )}     
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='生效日期'>
                       {getFieldDecorator('effectDate',{initialValue: record.effectDate === null ?  null : moment(record.effectDate, 'YYYY-MM-DD')})(
                       <DatePicker style={{ width: '100%' }}/> 
                  )}   
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='清单生成日期'>
                      {getFieldDecorator('inputDate',{initialValue: record.inputDate === null ? null : moment(record.inputDate, 'YYYY-MM-DD')})(
                        <DatePicker style={{width: '100%'}} />  
                     )}                     
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保人签单日期'>
                       {getFieldDecorator('signDate',{initialValue: record.signDate === null ? null : moment(record.signDate, 'YYYY-MM-DD')})(
                       <DatePicker style={{width: '100%'}}/>  
                )}    
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='缴费方式'>
                    {getFieldDecorator('feeType1',{initialValue: record === undefined ? '' : this.format(record.feeType1,"feeType1") })(
                      <Select placeholder="请选择" style={{ width: '100%' }}>
                        <Option value='1'>月缴</Option>
                      </Select>
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='款别'>
                     {getFieldDecorator('feeType',{initialValue: record === undefined ? '' : record.feeType })(
                    <Input /> 
                     )}                 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                  <Col md={8} lg={8} sm={16}>
                   <Form.Item label='年化保费'>
                    {getFieldDecorator('prmTmp',{initialValue: record === undefined ? '' : record.prmTmp})(
                    <Input  />
                )}
                  </Form.Item>
                </Col> 
              </Row>
             
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保人姓名'>
                    {getFieldDecorator('appName',{initialValue: record === undefined ? '' : record.appName })(
                      <Input />
                )}  
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保人性别'>
                    {getFieldDecorator('appSex',{initialValue: record === undefined ? '' : this.format(record.appSex,"appSex") })(
                      <Select placeholder="请选择" style={{ width: '100%' }}>
                        <Option value='01'>男</Option>
                        <Option value='02'>女</Option>
                      </Select>
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保人生日'>
                  {getFieldDecorator('appBoth',{initialValue: record.appBoth === null ? null : moment(record.appBoth, 'YYYY-MM-DD')})(
                     <DatePicker style={{width: '100%'}}/>  
                     )}                   
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保人日间电话'>
                    {getFieldDecorator('appDayTel',{
                      initialValue: record === undefined ? '' : record.appDayTel,
                      rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"}],
                       })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                  <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保人证件号'>
                    {getFieldDecorator('appCardId',{initialValue: record === undefined ? '' : record.appCardId })(
                      <Input />
                  )}
                  </Form.Item>
                </Col> 
                 <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保人帐单地址邮编'>
                    {getFieldDecorator('appTracyPost',{
                      initialValue: record === undefined ? '' : record.appTracyPost,
                      rules: [{
                        type: 'email', message: '请输入正确的格式',
                      }],
                       })(
                    <Input />
                     )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人姓名'>
                    {getFieldDecorator('insrncName',{initialValue: record === undefined ? '' : record.insrncName })(
                      <Input />
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人证件类型'>
                    {getFieldDecorator('insrncCardType',{initialValue: record === undefined ? '' : record.insrncCardType })(
                       <Select placeholder="请选择" style={{ width: '100%' }} >
                        {getSelectChild('certiDesc')}
                      </Select>
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人证件号码'>
                    {getFieldDecorator('insrncCardId',{initialValue: record === undefined ? '' : record.insrncCardId })(
                      <Input />
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                   <Form.Item label='被保险人出生日期'>
                      {getFieldDecorator('insrncBoth',{initialValue: record.insrncBoth === null ? null : moment(record.insrncBoth, 'YYYY-MM-DD') })(
                       <DatePicker style={{width: '100%'}}/> 
                    )}  
                  </Form.Item> 
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人日间电话'>
                    {getFieldDecorator('insrncDayTel',{
                      initialValue: record === undefined ? '' : record.insrncDayTel,
                      rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"}],
                    })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人夜间电话'>
                    {getFieldDecorator('insrncNightTel',{
                      initialValue: record === undefined ? '' : record.insrncNightTel,
                      rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"}],
                       })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
              </Row>
               <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险财产地址邮编'>
                    {getFieldDecorator('homePost',{
                      initialValue: record === undefined ? '' : record.homePost,
                      rules: [{
                        type: 'email', message: '请输入正确的格式',
                      }],
                  })(
                      <Input />
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人公司电话'>
                    {getFieldDecorator('inssrncComTel',
                      {
                        initialValue: record === undefined ? '' : record.inssrncComTel,
                        rules: [{
                        pattern: /^\d{8,}$/, message: '号码格式有误',
                        }],
                       })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险财产地址'>
                    {getFieldDecorator('homeAddress',{initialValue: record === undefined ? '' : record.homeAddress })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                  <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人手机'>
                    {getFieldDecorator('insrncPhone',{
                      initialValue: record === undefined ? '' : record.insrncPhone, 
                       rules: [{
                        pattern: /^[1][3,4,5,7,8][0-9]{9}$/, message: '格式有误，不允许空格等特殊符号',
                      }],
                    })(
                      <Input  />
                  )}
                  </Form.Item>
                </Col> 
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人邮编'>
                    {getFieldDecorator('insrncPostCode',{
                      initialValue: record === undefined ? '' : record.insrncPostCode,
                      })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                 <Form.Item label='分期付款次数'>
                    {getFieldDecorator('payTimes',{initialValue: record === undefined ? '' : record.payTimes })(
                    <Input  />
                )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保人与被保险人的关系'>
                    {getFieldDecorator('insrncRelation',{initialValue: record === undefined ? '' : record.insrncRelation })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人职业'>
                    {getFieldDecorator('insrncJob',{initialValue: record === undefined ? '' : record.insrncJob })(
                       <Input />
                  )} 
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='被保险人性别'>
                    {getFieldDecorator('insrncSex',{initialValue: record === undefined ? '' : this.format(record.appSex,"insrncSex") })(
                        <Select placeholder="请选择" style={{ width: '100%' }}>
                            <Option value='01'>男</Option>
                            <Option value='02'>女</Option>
                        </Select>
                    )} 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='TSR姓名'>
                    {getFieldDecorator('tsrName',{initialValue: record === undefined ? '' : record.tsrName })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='TSR编号'>
                    {getFieldDecorator('tsrCode',{initialValue: record === undefined ? '' : record.tsrCode })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='受益人姓名'>
                    {getFieldDecorator('benfName',{initialValue: record === undefined ? '' : record.benfName })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='受益类型'>
                    {getFieldDecorator('benfType',{initialValue: record === undefined ? '' : record.benfType })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='受益人与被保险人的关系'>
                    {getFieldDecorator('insrncRelation1',{initialValue: record === undefined ? '' : record.insrncRelation1 })(
                    <Input  />
                )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='所交保费'>
                    {getFieldDecorator('prm',{initialValue: record === undefined ? '' : record.prm })(
                    <Input  />
                )}
                  </Form.Item>
                </Col>
              </Row>
               <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
               <Col md={12} lg={12} sm={16}>
                    <Form.Item label='投保人帐单地址'>
                    {getFieldDecorator('appTracyAdd',{initialValue: record === undefined ? '' : record.appTracyAdd })(
                    <Input />
                     )}
                  </Form.Item>
                </Col>
                <Col md={12} lg={12} sm={16}>
                  <Form.Item label='被保险人在职公司名称'>
                    {getFieldDecorator('insrncCompany',{initialValue: record === undefined ? '' : record.insrncCompany })(
                        <Input />
                    )} 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={12} lg={12} sm={16}>
                  <Form.Item label='失败原因'>
                    {getFieldDecorator('failReason',{initialValue: record === undefined ? '' : record.failReason })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={12} lg={12} sm={16}>
                  <Form.Item label='失败原因描述'>
                    {getFieldDecorator('failReasonMsg',{initialValue: record === undefined ? '' : record.failReasonMsg })(
                      <Input />
                )}  
                  </Form.Item>
                </Col>
              </Row>
               <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={12} lg={12} sm={16}>
                  <Form.Item label='客户要求投递地址'>
                    {getFieldDecorator('postAddress',{initialValue: record === undefined ? '' : record.postAddress })(
                      <Input />
                  )}
                  </Form.Item>
                </Col>
                <Col md={12} lg={12} sm={16}>
                <Form.Item label='被保险人地址'>
                    {getFieldDecorator('insrncAddr',{initialValue: record === undefined ? '' : record.insrncAddr })(
                        <Input />
                    )} 
                  </Form.Item>
                </Col>
                  
              </Row>
            <div style={{ margin: 12 }}>
              <Button loading={loading}  type="primary" onClick={this.handleSubmit.bind(this)}>
                保存
              </Button>
              <Button style={{ margin: 12 }} onClick={this.onCancel.bind(this)}>
                取消
              </Button>
            </div>
        </Form>
    );
  }
}
