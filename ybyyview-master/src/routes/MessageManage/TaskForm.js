import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Steps,
  Select,
  Table,
  Card,
  Button,
  Dropdown,
  Badge,
  Input,
  Popconfirm,
  Modal,
  message,
} from 'snk-web';

import EmpTable from '../../components/EmpTable';
import EditableCell from '../../components/EmpTable/EditableCell';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import EditTaskForm from './EditTaskForm';
import { getSelectChild } from '../../utils/utils';
import styles from './TaskForm.less'

const { Option } = Select;
const Step = Steps.Step;

const EditableCell2 = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
      : value
    }
  </div>
);

@connect(({  loading,messageManage }) => ({
  loading: loading.models.messageManage,
  messageManage,
}))
export default class TaskForm extends PureComponent {
  constructor(props){
    super(props)
    let dates;
    if (props.location.state.date !== null || props.location.state.date !== undefined || props.location.state.date !== ''){
       const year = props.location.state.date.slice(0,4) || '';
        const month = props.location.state.date.slice(5,7) || '';
        const day = props.location.state.date.slice(8,10) || '';
        dates = year +''+ month +''+ day
    } else {
      dates = '';
    }
    this.state={
      extEnterpCode: props.location.state.extEnterpCode || '',
      // date: props.location.state.date || '',
      // extEnterpCode: '95555',
      date: dates,
      dataSource: [],
      editingKey: '',
      pageNum: 1,
      pageSize: 10,
      selectedRows: [],
      saveArr: [],
      modalVisible: false,
      editData: {},
    }
  }
  componentDidMount() {
    this.setState({pageNum: 1, pageSize: 10});
    this.searchData(this.state.date,this.state.extEnterpCode,1,10);
  }

  searchData = (date,extEnterpCode,pageNum,pageSize) => {
    this.props.dispatch({
      type: 'messageManage/failPolicy',
      payload: {
        extEnterpCode,
        date,
        pageNum,
        pageSize,
      }
    });
  }

  onCancel = (e,form) => {
    this.setState({modalVisible: false});
  }

  handleStandardTableChange = (pagination, filters, sorter) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    });
    this.searchData(this.state.date,this.state.extEnterpCode,pagination.current,pagination.pageSize);
  }

   handleSubmit = (e, form) => {
    form.validateFields((err, fieldsValue) => {

       if (/^(?=([\u4e00-\u9fa5].*){5})/.test(fieldsValue.homeAddress) === false) {
        message.warn(`[被保险财产地址]格式有误，不少于5个汉字`);
        return;
      } else if (/^(?=([\u4e00-\u9fa5].*){5})/.test(fieldsValue.appTracyAdd) === false && fieldsValue.appTracyAdd != null) {
        message.warn(`[投保人帐单地址]格式有误，不少于5个汉字`);
        return;
      } else if (/^(?=([\u4e00-\u9fa5].*){5})/.test(fieldsValue.postAddress) === false) {
        message.warn(`[客户要求投递地址]格式有误，不少于5个汉字`);
        return;
      } else if (/^(?=([\u4e00-\u9fa5].*){5})/.test(fieldsValue.insrncAddr) === false) {
        message.warn(`[被保险人地址]格式有误，不少于5个汉字`);
        return;
      }
      
      const appDate = fieldsValue.appDate ? fieldsValue.appDate.format('YYYY-MM-DD') : null;
      const inputDate = fieldsValue.inputDate ? fieldsValue.inputDate.format('YYYY-MM-DD') : null;
      const appBoth = fieldsValue.appBoth ? fieldsValue.appBoth.format('YYYY-MM-DD') : null;
      const insrncBoth = fieldsValue.insrncBoth ? fieldsValue.insrncBoth.format('YYYY-MM-DD') : null;
      const signDate = fieldsValue.signDate ? fieldsValue.signDate.format('YYYY-MM-DD') : null;
      const cardDate = fieldsValue.cardDate ? fieldsValue.cardDate.format('YYYY-MM-DD') : null;
      const effectDate = fieldsValue.effectDate ? fieldsValue.effectDate.format('YYYY-MM-DD') : null;

       let appSex,insrncSex,extEnterpCode,prodNo;
      if (fieldsValue.appSex === '男') {
        appSex = '01'
      } else if (fieldsValue.appSex  === '女') {
        appSex = '02'
      } else {
         appSex = fieldsValue.appSex;
      }
      
      if (fieldsValue.insrncSex  === '男') {
        insrncSex = '01'
      } else if (fieldsValue.insrncSex  === '女') {
        insrncSex = '02'
      } else {
         insrncSex = fieldsValue.insrncSex;
      }
      if(/^[\u4e00-\u9fa5]/.test(fieldsValue.extEnterpCode)){
        if (getSelectChild('extEnterpDesc')) {
          for (const i in getSelectChild('extEnterpDesc')) {
            if (getSelectChild('extEnterpDesc')[i].props.children === fieldsValue.extEnterpCode) {
                extEnterpCode = getSelectChild('extEnterpDesc')[i].props.value;
            }
          }
        }
      } else {
        extEnterpCode = fieldsValue.extEnterpCode
      }
      if(/^[\u4e00-\u9fa5]/.test(fieldsValue.prodNo)){
        if (getSelectChild('prodDesc')) {
          for (const i in getSelectChild('prodDesc')) {
            if (getSelectChild('prodDesc')[i].props.children === fieldsValue.prodNo
                && getSelectChild('prodDesc',null,null,false,null,'group')[i].ref === extEnterpCode) {
                prodNo = getSelectChild('prodDesc')[i].props.value;
            }
          }
        }
      } else {
        prodNo = fieldsValue.prodNo
      }

      if (!err) {
        this.props.dispatch({
           type: 'messageManage/updateFailPolicy',
            payload: {
              ...fieldsValue,
              insrncBoth,
              appBoth,
              appDate,
              inputDate,
              signDate,
              cardDate,
              effectDate,
              insrncSex,
              appSex,
              extEnterpCode,
              prodNo,
            },
        }).then(()=>{
            this.setState({modalVisible: false});
            setTimeout(()=>{
              this.searchData(this.state.date,this.state.extEnterpCode,this.state.pageNum,this.state.pageSize);
            },200);
        }).catch(e=>{
          message.error(e.message||'网络异常请稍后再试');
        });
      }
    });
  };

   handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  edit = (record) => {
    this.setState({modalVisible: true, editData: record});
  }

  render() {
    const {messageManage:{failPolicyData,pageInfo},loading} = this.props;
    let {dataSource} = this.state;
    let date ='';
    let year;
    let month;
    let day;
    let extEnterpName;
    let money;
    let total;

    if (failPolicyData) {
      extEnterpName = failPolicyData.extEnterpName || '';
      money = failPolicyData.money || '';
    }
    if (pageInfo) {
      total = pageInfo.total || 0;
      dataSource = pageInfo.list || [];
    }
    if (this.state.date) {
        year = this.state.date.slice(0,4);
        month = this.state.date.slice(4,6);
        day = this.state.date.slice(6,8);
    }
    this.state.dataSource = dataSource;

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            this.setState({
              selectedRows: selectedRows
            });
        },
    };
    const rowKey = (record,index) => {
      return index
    }
    const columns = [
      {
          title: '订单号',
          dataIndex: 'cPlyAppNo',
          align: 'center',
        },
        {
          title: 'TSR姓名',
          dataIndex: 'tsrName',
          align: 'center',
        },{
          title: '产品名称',
          dataIndex: 'prodName',
          align: 'center',
        },
        {
          title: '被保人地址',
          dataIndex: 'insrncAddr',
          align: 'center',
          width: 160,
        },
        {
          title: '被保人证件号',
          dataIndex: 'insrncCardId',
          align: 'center',
        },
        {
          title: '产品编号',
          dataIndex: 'prodNo',
          align: 'center',
        },
        {
          title: '签单时间',
          dataIndex: 'appDate',
          align: 'center',
        },
      {
          title: '状态',
          dataIndex: 'failReasonMsg',
          align: 'center',
          render: (test, record) => {
              return (
                  <span><Badge status= {`${test}`.indexOf('已存在') ? "success" : "error" }/>{test}</span>
              );
          }
        },{
          title: '操作',
          dataIndex: 'action',
          align: 'center',
          render: (text, record) => {
            return (
              <span>
                <a onClick={() => this.edit(record)}>修改</a>
              </span>
            );
          }
        }];

    return (
      <PageHeaderLayout title={`${extEnterpName}渠道保单数据确认界面—— ${year}/${month}/${day}`}>
         <Card bordered={false}>
          <div>
            <Button style={{marginBottom: 10}} onClick={()=>this.props.history.push('/message-manage/message-form')}>返回消息列表</Button>
             <Table
              loading={loading}
              dataSource={dataSource}
              state={dataSource}
              columns={columns}
              onChange={this.handleStandardTableChange}
              rowKey={rowKey}
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                ...{
                    current:this.state.pageNum,
                    pageSize:this.state.pageSize,
                    showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                    total:total,
                    pageSizeOptions:['10','20','30','40','50'],
                },
              }} 
              title={() => {
                  return <div>
                    <span className={styles.commStyle}>{year}-{month}-{day}</span>
                    <span className={styles.commStyle}>总计：<span>{total}单</span></span>
                    <span className={styles.commStyle}>涉及金额：<span style={{color:'#008000'}}>{money}元</span></span>
                    <span className={styles.channelName}>渠道名称：<span>{extEnterpName}</span></span>
                  </div>
                }}
            />
          </div>
        </Card> 
        <Modal
          title={'修改问题数据'}
          width={1000}
          footer={null}
          style={{ top: 20 }}
          onCancel={() => this.handleModalVisible(false)}
          visible={this.state.modalVisible}
        >
          <EditTaskForm
            record={this.state.editData}
            root={this}
          />
        </Modal>
      </PageHeaderLayout>
    );
  }
}
