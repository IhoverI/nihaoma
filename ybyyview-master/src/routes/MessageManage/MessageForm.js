import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Steps,
  Select,
  Table,
  Card,
  Button,
  Dropdown,
  Badge,
  Divider,
  Input,
  Popconfirm,
  Collapse,
  Radio,
  message,
  Icon,
  Form,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './TaskForm.less'

const Search = Input.Search;
const Panel = Collapse.Panel;
const { Option } = Select;
const Step = Steps.Step;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;


const steps = [{
    title: '注退上报',
    content: '注退上报',
  }, {
    title: '注退审核',
    content: '注退审核',
  }, {
    title: '完成',
    content: '完成',
  }];

  const statusMap = ['success', 'default'];
  const status = ['待审核', '审核完成'];
  
@connect(({  loading,messageManage }) => ({
  loading: loading.models.messageManage,
  messageManage,
}))
@Form.create()
export default class MessageForm extends PureComponent {
  constructor(props) {
    super(props);
    const { current, condition, toDoFlag, pageNum, pageSize, readFlag } = props.messageManage.messageFormData;
    this.state = {
      current: pageNum || 1,
      selVal: '',
      pageNum: pageNum || 1,
      pageSize: pageSize || 10,
      readFlag: readFlag || '',
      condition: condition || '',
      toDoFlag: toDoFlag || '',
    };
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'messageManage/messageCount'
    });
    const { toDoFlag, readFlag, condition, pageNum, pageSize } = this.state;
    this.searchDataAll(toDoFlag, readFlag, condition, pageNum, pageSize)
    this.qureyTop();
  }

  qureyTop = () => {
    this.props.dispatch({
        type: 'messageManage/queryTopMessage',
        payload: {
          pageNum: 0,
          pageSize: 10,
        }
      }); 
  }

  topOrNot = (e,record) => {
    e.stopPropagation();
    let isTop = record.isTop == '0' ? '1' : '0'
    this.props.dispatch({
      type: 'messageManage/update',
      payload: {
        id: record.id,
        isTop: isTop,
        readDate: record.readDate,
        readFlag: record.readFlag,
        toDoFlag: record.toDoFlag
      }
    }).then(()=>{
      setTimeout(()=>{
        this.qureyTop();
        this.searchDataAll(this.state.readFlag,this.state.condition,1,10)
      },200);
    }).catch((e)=>{
      message.error('网络异常');
    });
    
  }

  remove = (e,id) =>{
    e.stopPropagation();
    this.props.dispatch({
        type: 'messageManage/deletes',
        payload: id
    });
      setTimeout(()=>{
      this.qureyTop();
    },500);
    setTimeout(()=>{
      this.searchDataAll(this.state.readFlag,this.state.condition,1,10)
    },500);
  }
  
  blinkTitle = (content, searchKeys='') => {
    if (content) {
      return content.replace(searchKeys, `<span class='blinkwords'>${searchKeys}</span>`);
    }
  }

  getColumns(){
    const {loading} = this.props;
    const { getFieldDecorator } = this.props.form;
    return [{
          dataIndex: 'code0',
          width: '40%',
          render: (text, record) => {
            if (this.state.readFlag === '') {
              return (
                  <span>
                      <div>
                      <Icon type="profile" style={{ paddingRight: 5 }}/>
                        <a dangerouslySetInnerHTML={{__html: this.blinkTitle(record.title, this.state.condition ) || ''}} />
                      </div>
                  </span>
              );
            } else if (this.state.readFlag === '0') {
              return (
                  <span>
                      <div>
                      <Icon type="mail" style={{ paddingRight: 5 }}/>
                        <a dangerouslySetInnerHTML={{__html: this.blinkTitle(record.title, this.state.condition ) || ''}} />
                      </div>
                  </span>
              );
            } else {
              return (
                  <span>
                      <div>
                      <Icon type="folder-open" style={{ paddingRight: 5 }}/>
                        <a dangerouslySetInnerHTML={{__html: this.blinkTitle(record.title, this.state.condition ) || ''}} />
                      </div>
                  </span>
              );
            }  
          }
        },
        {
          dataIndex: 'sentDate',
          width: '40%',
          render: (text) => {
              return (
                  <span>
                      <div>时间</div>
                      <div>
                        <a dangerouslySetInnerHTML={{__html: this.blinkTitle(text, this.state.condition ) || ''}} />
                      </div>
                  </span>
              );
          }
        },{
          dataIndex: 'action',
          width: '20%',
          render : (text, record) => {
            return (
                <span>
                    <Button onClick={(e)=>this.remove(e,record.id)} style={{ border: 0, color: '#1890ff', background: 'transparent' }}>
                        删除
                    </Button>
                    <Divider type="vertical" />
                    <Button onClick={(e)=>this.topOrNot(e,record)} 
                        style={{ border: 0, color: '#1890ff', background: 'transparent' }}>
                         {`${record.isTop}` == 1 ? '取消置顶' : '星标置顶' }
                    </Button>
                </span>
            );
          }
        }];
  }
  getColumns1(){
    const {loading} = this.props;
    return [{
          dataIndex: 'code0',
          width: '40%',
          render: (text, record) => {
              return (
                  <span>
                      <div>
                      <Icon type="star-o" style={{ paddingRight: 5 }}/>
                      <a>{record.title}</a>
                      </div>
                  </span>
              );
          }
        },
        {
          dataIndex: 'sentDate',
          width: '40%',
          render: (text) => {
              return (
                  <span>
                      <div>时间</div>
                      <div>{text}</div>
                  </span>
              );
          }
        },{
          dataIndex: 'action',
          width: '20%',
          render : (text, record) => {
            return (
                <span>
                    <Button onClick={(e)=>this.remove(e,record.id)} style={{ border: 0, color: '#1890ff', background: 'transparent' }}>
                        删除
                    </Button>
                    <Divider type="vertical" />
                    <Button onClick={(e)=>this.topOrNot(e,record)} 
                        style={{ border: 0, color: '#1890ff', background: 'transparent' }}>
                         {`${record.isTop}` == 1 ? '取消置顶' : '星标置顶' }
                    </Button>
                </span>
            );
          }
        }];
  }
  selChange = e => {  
    this.setState({
      readFlag: e.target.value,
      pageNum: 1,
    });
    const { toDoFlag, condition } = this.state;
    this.searchDataAll(toDoFlag, e.target.value, condition, 1, 10)
  }
  
  handleTableChange = (pagination, filters, sorter) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchDataAll(this.state.toDoFlag,this.state.readFlag,this.state.condition,pagination.current,pagination.pageSize);
    });
  }

  onSearch = (value) => {
    const condition = value.trim();
    if (!condition) {
      return false;
    }
    this.setState({
      condition: condition,
    });
    this.searchDataAll(this.state.toDoFlag,this.state.readFlag,condition,this.state.pageNum,this.state.pageSize);
  }

  searchDataAll = (toDoFlag,readFlag,condition,pageNum,pageSize) => {
    if (toDoFlag === '') {
      this.props.dispatch({
        type: 'messageManage/queryMessage',
        payload: {
          condition: condition,
          pageNum: pageNum,
          pageSize: pageSize,
          readFlag: readFlag,
          isTop: "0",
        }
      });
    } else {
      this.props.dispatch({
        type: 'messageManage/queryMessage',
        payload: {
          toDoFlag,
          condition: condition,
          pageNum: pageNum,
          pageSize: pageSize,
          readFlag: readFlag,
          isTop: "0",
        }
      });
    }
    
  }

  searchDataNotice = (readFlag,condition,pageNum,pageSize) => {
    this.props.form.setFieldsValue({selType: '',searchCondition:''});
    this.setState({toDoFlag: '0',pageNum:1,pageSize:10,readFlag: '',condition: ''});
    this.props.dispatch({
      type: 'messageManage/queryMessage',
      payload: {
        condition: condition,
        pageNum: pageNum,
        pageSize: pageSize,
        readFlag: readFlag,
        toDoFlag: "0",
        isTop: "0",
      }
    });
  }

  searchDataUntreat = (readFlag,condition,pageNum,pageSize) => {
    this.props.form.setFieldsValue({selType: '',searchCondition:''});
    this.setState({toDoFlag: '1',pageNum:1,pageSize:10,readFlag: '',condition: ''});
    this.props.dispatch({
      type: 'messageManage/queryMessage',
      payload: {
        condition: condition,
        pageNum: pageNum,
        pageSize: pageSize,
        readFlag: readFlag,
        toDoFlag: "1",
        isTop: "0",
      }
    });
  }

  searchMessageAll = (readFlag,condition,pageNum,pageSize) => {
    this.props.form.setFieldsValue({selType: '',searchCondition:''});
    this.setState({toDoFlag: '',pageNum:1,pageSize:10,readFlag: '',condition: ''});
    this.props.dispatch({
      type: 'messageManage/queryMessage',
      payload: {
        condition: condition,
        pageNum: pageNum,
        pageSize: pageSize,
        readFlag: readFlag,
        isTop: "0",
      }
    });
  }

  clickRow = (e,record) => {
    if (record) {
     this.props.history.push({
        pathname: '/message-manage/read-message',
          state:{
            record: record
          }
      })
      this.props.dispatch({
      type: 'messageManage/update',
      payload: {
          id: record.id,
          readDate: record.readDate,
          readFlag: 1,
        }
      });
    } 
  }

  render() {
    const {messageManage:{count,queryData,queryTopData},form:{getFieldDecorator},loading} = this.props;
    const { current } = this.state;
    let noReadCount = 0;
    let readCount = 0;
    let totalCount = 0;
    let total;
    let data = [];
    let topData = [];
    if (count) {
      noReadCount = count.noReadCount;
      readCount = count.readCount;
      totalCount = count.totalCount
    }
    if (queryData) {
      data = queryData.list;
      total = queryData.total;
    if (queryTopData) {
      topData = queryTopData.list;
    }
    }

    return (
      <PageHeaderLayout title="">
      <Card bordered={false}>
        <div className={styles.messTableTitle}>
            <div className={styles.messComStyle}>
                <div>通知消息</div>
                <a onClick={() => this.searchDataNotice('','',1,10)}><h3>{noReadCount}条</h3></a>
            </div>
            <div className={`${styles.messComStyle} ${styles.messCenter}`}>
                <div>需处理消息</div>
                <a onClick={() => this.searchDataUntreat('','',1,10)}><h3>{readCount}条</h3></a>
            </div>
            <div className={styles.messComStyle}>
                <div>消息总数</div>
                <a onClick={() => this.searchMessageAll('','',1,10)}><h3>{totalCount}条消息</h3></a>
            </div>
        </div>
        <div style={{display: `${topData}`== []  ? 'none' : 'block'}}>
          <Table
            size="small"
            rowKey={record => record.id}
            columns={this.getColumns1()} 
            dataSource={topData} 
            onRow={(record) => {
             return {
              onClick: (e) => {this.clickRow(e,record)}, 
            }; 
          }}
            />
        </div>
        <div>
          <div style={{textAlign:'center'}}>
            {getFieldDecorator('selType', {initialValue: this.state.readFlag})(
            <RadioGroup onChange={this.selChange}>
                <RadioButton value="">全部</RadioButton>
                <RadioButton value="0">待读</RadioButton>
                <RadioButton value="1">已读</RadioButton>
            </RadioGroup>
            )}
        </div>  
        <div style={{marginTop:10,textAlign:'right',marginBottom:10}}>
           {getFieldDecorator('searchCondition')(
            <Search
                placeholder="可输入邮件的主题、邮件内容、日期进行搜索"
                onSearch={this.onSearch}
                enterButton
                style={{ width: 340, marginLeft: 20 }}
            />
            )}
        </div>
      </div> 
        <Table
          size="small"
          rowKey={record => record.id}
          columns={this.getColumns()} 
          loading={loading}
          dataSource={data} 
          pagination={{
            showSizeChanger: true,
            showQuickJumper: true,
            ...{
                current:this.state.pageNum,
                pageSize:this.state.pageSize,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                total: total,
                pageSizeOptions:['10','20','30','40','50'],
            },
            }}
          onChange={this.handleTableChange}
          onRow={(record) => {
              return {
              onClick: (e) => {this.clickRow(e,record)},   
            };  
          }}
          />
      </Card>
      </PageHeaderLayout>
    );
  }
}
