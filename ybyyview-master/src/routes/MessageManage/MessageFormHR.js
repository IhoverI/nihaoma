import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Steps,
  Select,
  Table,
  Card,
  Button,
  Dropdown,
  Badge,
  Divider,
  Input,
  Popconfirm,
  Collapse,
  Radio,
  message,
  Icon,
  Modal 
} from 'snk-web';
import styles from './TaskForm.less'
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

const Search = Input.Search;
const Panel = Collapse.Panel;
const { Option } = Select;
const Step = Steps.Step;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;


const steps = [{
    title: '注退上报',
    content: '注退上报',
  }, {
    title: '注退审核',
    content: '注退审核',
  }, {
    title: '完成',
    content: '完成',
  }];

  const statusMap = ['success', 'default'];
  const status = ['待审核', '审核完成'];
  
@connect(({  loading,messageManageHR }) => ({
  loading: loading.models.messageManageHR,
  messageManage:messageManageHR,
}))
export default class MessageForm extends PureComponent {
  state = {
   current:0,
   selVal: '',
   pageNum: 1,
   pageSize: 10,
   readFlag: '',
   condition: '',
  };
  componentDidMount() {
    console.log('component')
    this.props.dispatch({
      type: 'messageManageHR/messageCount'
    });
    this.setState({pageNum:1,pageSize:10});
    this.searchDataAll(this.state.readFlag,this.state.condition,1,10)
    this.qureyTop();
  }

  qureyTop = () => {
    this.props.dispatch({
        type: 'messageManageHR/queryTopMessage',
        payload: {
          pageNum: 0,
          pageSize: 10,
        }
      }); 
  }

  topOrNot = (e,record) => {
    e.stopPropagation();
    let isTop = record.isTop == '0' ? '1' : '0'
    this.props.dispatch({
      type: 'messageManageHR/update',
      payload: {
        id: record.id,
        isTop: isTop,
        readDate: record.readDate,
        readFlag: record.readFlag,
        toDoFlag: record.toDoFlag
      }
    }).then(()=>{
      setTimeout(()=>{
        this.qureyTop();
        this.searchDataAll(this.state.readFlag,this.state.condition,1,10)
      },200);
    }).catch((e)=>{
      message.error('网络异常');
    });
    
  }

  remove = (e,id) =>{
    e.stopPropagation();
    this.props.dispatch({
        type: 'messageManageHR/deletes',
        payload: {id}
    });
      setTimeout(()=>{
      this.qureyTop();
    },500);
    setTimeout(()=>{
      console.log(this.state.condition)
      this.searchDataAll(this.state.readFlag,this.state.condition,1,10)
    },500);
  }

  getColumns(){
    const {loading} = this.props;
    return [{
          dataIndex: 'code0',
          width: '40%',
          render: (text, record) => {
            if (this.state.readFlag === '') {
              return (
                  <span>
                      <div>
                      <Icon type="profile" style={{ paddingRight: 5 }}/>
                        <a>{record.title}</a>
                      </div>
                  </span>
              );
            } else if (this.state.readFlag === '0') {
              return (
                  <span>
                      <div>
                      <Icon type="mail" style={{ paddingRight: 5 }}/>
                        <a>{record.title}</a>
                      </div>
                  </span>
              );
            } else {
              return (
                  <span>
                      <div>
                      <Icon type="folder-open" style={{ paddingRight: 5 }}/>
                        <a>{record.title}</a>
                      </div>
                  </span>
              );
            }  
          }
        },
        {
          dataIndex: 'sentDate',
          width: '40%',
          render: (text) => {
              return (
                  <span>
                      <div>时间</div>
                      <div>{text}</div>
                  </span>
              );
          }
        },{
          dataIndex: 'action',
          width: '20%',
          render : (text, record) => {
            return (
                <span>
                    <Button onClick={(e)=>this.remove(e,record.id)} style={{ border: 0, color: '#1890ff', background: 'transparent' }}>
                        删除
                    </Button>
                    <Divider type="vertical" />
                    <Button onClick={(e)=>this.topOrNot(e,record)} 
                        style={{ border: 0, color: '#1890ff', background: 'transparent' }}>
                         {`${record.isTop}` == 1 ? '取消置顶' : '星标置顶' }
                    </Button>
                </span>
            );
          }
        }];
  }
  getColumns1(){
    const {loading} = this.props;
    return [{
          dataIndex: 'code0',
          width: '40%',
          render: (text, record) => {
              return (
                  <span>
                      <div>
                      <Icon type="star-o" style={{ paddingRight: 5 }}/>
                      <a>{record.title}</a>
                      </div>
                  </span>
              );
          }
        },
        {
          dataIndex: 'sentDate',
          width: '40%',
          render: (text) => {
              return (
                  <span>
                      <div>时间</div>
                      <div>{text}</div>
                  </span>
              );
          }
        },{
          dataIndex: 'action',
          width: '20%',
          render : (text, record) => {
            return (
                <span>
                    <Button onClick={(e)=>this.remove(e,record.id)} style={{ border: 0, color: '#1890ff', background: 'transparent' }}>
                        删除
                    </Button>
                    <Divider type="vertical" />
                    <Button onClick={(e)=>this.topOrNot(e,record)} 
                        style={{ border: 0, color: '#1890ff', background: 'transparent' }}>
                         {`${record.isTop}` == 1 ? '取消置顶' : '星标置顶' }
                    </Button>
                </span>
            );
          }
        }];
  }
    selChange = e => {
      this.setState({
        readFlag: e.target.value
      });
       this.searchDataAll(e.target.value,this.state.condition,1,10)
    }
  
    handleTableChange = (pagination, filters, sorter) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchDataAll(this.state.readFlag,this.state.condition,pagination.current,pagination.pageSize);
    });
  }

  onSearch = (condition) => {
    this.setState({
      condition: condition,
    });
    this.searchDataAll(this.state.readFlag,condition,this.state.pageNum,this.state.pageSize);
  }

  searchDataAll = (readFlag,condition,pageNum,pageSize) => {
    this.props.dispatch({
      type: 'messageManageHR/queryMessage',
      payload: {
        condition: condition,
        pageNum: pageNum,
        pageSize: pageSize,
        readFlag: readFlag,
        isTop: "0",
      }
    });
  }

  searchDataNotice = (readFlag,condition,pageNum,pageSize) => {
    this.props.dispatch({
      type: 'messageManageHR/queryMessage',
      payload: {
        condition: condition,
        pageNum: pageNum,
        pageSize: pageSize,
        readFlag: readFlag,
        toDoFlag: "0",
        isTop: "0",
      }
    });
  }

  searchDataUntreat = (readFlag,condition,pageNum,pageSize) => {
    this.props.dispatch({
      type: 'messageManageHR/queryMessage',
      payload: {
        condition: condition,
        pageNum: pageNum,
        pageSize: pageSize,
        readFlag: readFlag,
        toDoFlag: "1",
        isTop: "0",
      }
    });
  }
 
  clickRow = (e,record) => {
    if(e)e.preventDefault
    console.log(record)
    if (record) {
       Modal.info({
         width:600,
        title: '消息详情',
        className:styles.modalDiv,
        maskClosable:true,
        content: <span dangerouslySetInnerHTML={{ __html: record.content }} />,
      });
      const  addListener = setInterval(()=>{
        const a = document.getElementsByClassName('ant-confirm-content')
        if(a.length>0){
          console.log('加载监听事件')
          clearInterval(addListener)
          a[0].firstElementChild.getElementsByTagName('a')[0].addEventListener('click',e=>{
            //关闭modal
            document.getElementsByClassName('ant-modal-wrap ')[0].click()
            //标记为已读

            if (record.readFlag!=1) {
              this.props.dispatch({
                type: 'messageManageHR/update',
                payload: {
                  id: record.id,
                  readDate: record.readDate,
                  readFlag: 1,
                }
              });
            }
          })
        }
      },100)
     
    
     


    //  this.props.history.push({
    //     pathname: '/message-manage/read-message',
    //       state:{
    //         record: record
    //       }
    //   })
     
 
    } 
  }

  render() {
    console.log('render')
    const {messageManage:{count,queryData,queryTopData},loading} = this.props;
    const { current } = this.state;
    let noReadCount = 0;
    let readCount = 0;
    let totalCount = 0;
    let total;
    let data = [];
    let topData = [];
    console.log(queryData)
    if (count) {
      noReadCount = count.noReadCount;
      readCount = count.readCount;
      totalCount = count.totalCount
    }
    if (queryData) {
      data = queryData.list;
      total = queryData.total;
    if (queryTopData) {
      topData = queryTopData.list;
    }
    }

    return (
      <PageHeaderLayout>
      <Card bordered={false}>
        <div className={styles.messTableTitle}>
            <div className={styles.messComStyle}>
                <div>通知类消息</div>
                <a onClick={() => this.searchDataNotice()}><h3>{noReadCount}条</h3></a>
            </div>
            <div className={`${styles.messComStyle} ${styles.messCenter}`}>
                <div>需处理消息</div>
                <a onClick={() => this.searchDataUntreat()}><h3>{readCount}条</h3></a>
            </div>
            <div className={styles.messComStyle}>
                <div>消息总数</div>
                <a onClick={() => this.searchDataAll()}><h3>{totalCount}条消息</h3></a>
            </div>
        </div>
        <div style={{display: `${topData}`== []  ? 'none' : 'block'}}>
          <Table
            size="small"
            rowKey={record => record.id}
            columns={this.getColumns1()} 
            dataSource={topData} 
            onRow={(record) => {
             return {
              onClick: (e) => {this.clickRow(e,record)}, 
            }; 
          }}
            />
        </div>
        <div>
        <div style={{textAlign:'center'}}>
          <RadioGroup onChange={this.selChange} defaultValue="">
              <RadioButton value="">全部</RadioButton>
              <RadioButton value="0">待读</RadioButton>
              <RadioButton value="1">已读</RadioButton>
          </RadioGroup>
        </div>
        <div style={{marginTop:10,textAlign:'right',marginBottom:10}}>
            <Search
                placeholder="可输入邮件的主题、邮件内容、日期进行搜索"
                onSearch={this.onSearch}
                enterButton
                style={{ width: 340, marginLeft: 20 }}
            />
        </div>
      </div> 
        <Table
          size="small"
          rowKey={record => record.id}
          columns={this.getColumns()} 
          loading={loading}
          dataSource={data} 
          // pagination={false}
          pagination={{
            showSizeChanger: true,
            showQuickJumper: true,
            ...{
                current:this.state.pageNum,
                pageSize:this.state.pageSize,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                total: total,
                pageSizeOptions:['10','20','30','40','50'],
            },
            }}
          onChange={this.handleTableChange}
          onRow={(record) => {
              return {
              onClick: (e) => {this.clickRow(e,record)},   
            };  
          }}
          />
      </Card>
      </PageHeaderLayout>
    );
  }
}
