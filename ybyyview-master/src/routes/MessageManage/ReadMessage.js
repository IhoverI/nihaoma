import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Steps,
  Select,
  Table,
  Card,
  Button,
  Dropdown,
  Badge,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

@connect(({  loading,messageManage }) => ({
  loading: loading.models.messageManage,
  messageManage,
}))
export default class ReadMessage extends PureComponent {
  constructor(props){
    super(props)
    this.state={
        record: props.location.state === undefined ? '' : props.location.state.record,
        dataSource: {},
        editingKey: '',
        pageNum: 1,
        pageSize: 5,
        flag: 0,
        hasCancelCorrect: false,
    }
  }
  componentDidMount(){
  }

  render() {
    const {loading,messageManage:{readData}} = this.props;
    let title;
    let content;
    let router;
    let year;
    let month;
    let day;
    let extEnterpCode;
    let sentDate;

    if (this.state.record) {
      content = this.state.record.content || '';
      extEnterpCode = this.state.record.extEnterpCode || '';
      sentDate = this.state.record.sentDate || '';
      if (content.search('/message-manage/task-form') != -1) {
        router = '/message-manage/task-form'
      } else if (content.search('/apply-manage/selfchannel-audit') != -1){
        router = '/apply-manage/selfchannel-audit'
      } else if (content.search('/correction/cancel-audit') != -1) {
        router = '/correction/cancel-audit'
      } else if (content.search('/message-manage/correction') != -1) {
        this.setState({hasCancelCorrect: true});
      } else if (content.search('/correction/basic-correction') != -1) {
        this.setState({hasCancelCorrect: true});
      } else if (content.search('/correction/cancel-correction') != -1) {
        this.setState({hasCancelCorrect: true});
      } else if (content.search('href') === -1) {
        this.setState({hasCancelCorrect: true});
      }
     }

    return (
        <Card bordered={false}>
            <Button onClick={()=>this.props.history.goBack()}>返回消息列表</Button>
            <div style={{marginTop: 20}}>
                  <div style={{margin: '0px 20px'}} 
                    onClick={ this.state.hasCancelCorrect === false 
                    ?  
                    ()=>this.props.history.push({
                      pathname: `${router}`,
                      state:{extEnterpCode: extEnterpCode ,date: sentDate}
                    }) 
                    :
                    () => {}
                  }
                > 
                     <p dangerouslySetInnerHTML={{ __html: content }} />  
                     <span>{this.state.hasCancelCorrect}</span>  
                </div>  
            </div>
        </Card>
    );
  }
}
