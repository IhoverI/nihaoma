import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Steps,
  Select,
  Table,
  Card,
  Button,
  DatePicker,
  Dropdown,
  Input,
  Popconfirm,
  Form,
  message,
} from 'snk-web';
import { getDataSource,getSelectChild } from '../../utils/utils'
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import EmpTable from '../../components/EmpTable';
import EditableCell from '../../components/EmpTable/EditableCell';
import styles from './index.less';

const { Option } = Select;
const Step = Steps.Step;
const FormItem = Form.Item;

const EditableCell2 = ({ editable, value, onChange, remarkDisplay }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0', display : remarkDisplay }} 
      value={value} 
      onChange={e => onChange(e.target.value)} 
      placeholder='请填写打回原因'
      />
      : value
    }
  </div>
);

const steps = [{
    title: '保单登记',
    content: '保单登记',
  }, 
  {
    title: '录音质检',
    content: '录音质检',
  }, 
  {
    title: '完成',
    content: '完成',
  }];
  
@connect(({  loading, searchlist }) => ({
  loading: loading.models.searchlist,
  searchlist
}))
@Form.create()
export default class SelfChannelAudit extends PureComponent {
  constructor(props) {
    super(props);
    const { selfChannelData } = props.searchlist;
    this.state = {
      current:0,
      dataSource: [],
      formValues: {...selfChannelData.postValue},
      pageNum: selfChannelData.pageNum || 1,
      pageSize: selfChannelData.pageSize || 10,
      originMomentTime: selfChannelData.originMomentTime,
      auditDisplay: 'none',
      remarkDisplay : 'none',
    }
  }
  componentDidMount() {
    this.props.dispatch({
      type: 'searchlist/queryChannelPolicy',
      payload: {
        pageNum: this.state.pageNum,
        pageSize: this.state.pageSize,
        postValue:this.state.formValues,
      }
    }).then(() => {
      this.props.form.setFieldsValue({
        status: this.state.formValues.status,
        cPlyAppNo: this.state.formValues.cPlyAppNo,
        appDate: this.state.originMomentTime,
      });
    });
  }

  handleTablePage = (pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData();
    });
  }

  searchData = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const originMomentTime = values.appDate;
        values = this.prepareValue(values);
        this.props.dispatch({
          type: 'searchlist/queryChannelPolicy',
          payload: {
            pageNum: this.state.pageNum,
            pageSize: this.state.pageSize,
            postValue:values,
            originMomentTime, 
          }
        });
      }
    });
    
  }

  handleFormReset=()=>{
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({pageNum:1,pageSize:10});
  }

  handleSearch=(e)=>{
    e.preventDefault();
    this.searchData();
  }

  prepareValue(values){
    if(!values.cPlyAppNo) {
      values.cPlyAppNo = null;
    }else{
      values.cPlyAppNo = values.cPlyAppNo.replace(/^\s+|\s+$/gm,'');
    }
    
    if(!values.appDate||values.appDate.length===0) {
      values.appDate = null;
    }else{
      const re = [values.appDate[0].format("YYYY-MM-DD"),values.appDate[1].format("YYYY-MM-DD")];
      values.appDate = JSON.stringify(re);
    }

    for(var key in values){
      if(key!=='appDate'&&key!=='cPlyAppNo'&&key!=='status'){
        delete values[key];
      }
    }
    return values;
  }

  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className='custom-form-wrapper' onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={5} sm={30}>
            <FormItem label="审核状态">
              {getFieldDecorator('status')(
                <Select style={{width: 80}}>
                  <Option value="3">待审核</Option>
                  <Option value="4">已打回</Option>
                </Select>
               )}
            </FormItem>
          </Col>
          <Col md={7} sm={30}>
            <FormItem label="投保单号">
              {getFieldDecorator('cPlyAppNo',
              {
                rules: [{max: 30, message: '请输入正确的投保单号',}]
              },
              {initialValue: ''})
              (<Input placeholder="投保单号" />)}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="投保日期">  
              {getFieldDecorator('appDate')(
                <DatePicker.RangePicker allowClear={true} style={{ width: '100%' }} onChange={this.onChange}/>
              )}
            </FormItem>
          </Col>
        </Row>
        
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
        </div>
      </Form>
    );
  }

  renderColumns(text, record, column, remarkDisplay) {
    return (
      <EditableCell2
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record, column)}
        remarkDisplay = {remarkDisplay}
      />
    );
  }

  edit(record,dataSource) { 
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => item === record)[0];
    if (target) {
      record.editable = true;
      this.setState({
        data: newData,
        auditDisplay: 'block',
      });
    }
  }

  remarkCheck(record){
    if (this.state.remarkDisplay=='block' 
    && (record.remark==null || record.remark.replace(/<\/?.+?>/g,"")==0)){
      message.warning("请填写打回原因。");
      return false;
    } else{
      return true;
    }
  }

  save(record) {
    if(this.remarkCheck(record)){
    const { dispatch, form } = this.props;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => item === record)[0];
    let payload = [];
    if (target) {
      delete record.editable;
      this.setState({
        data: newData,
        auditDisplay: 'none',
      });
    }
    form.validateFields((err, fieldsValue) => {
      let operation = '';
        let value = record.cPlyAppNo;
        if (err) return;
        let list = {
          cPlyAppNo: record.cPlyAppNo,
          operation: fieldsValue[value],
          remark: record.remark
        }
        payload.push(list)
         this.props.dispatch({
          type: 'searchlist/approvedChannel',
          payload: payload
        }).then(()=>{
          setTimeout(()=>{
            this.setState({
              pageNum:1,
              pageSize: 10,
            },()=>{
              this.searchData();
            });
          },200);
        });
      })
    }
  }
    

  cancel(record) {
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => item === record)[0];
    if (target) {
      delete record.editable;
      this.setState({
        data: newData,
        auditDisplay: 'none',
      });
    }
  }

  handleChange(value, record, column) {
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => item === record)[0];
    if (target) {
      target[column] = value;
      this.setState({
        dataSource: newData,
      });
    }
  }
  remarkDisplay = (value) => {
    if("通过" === this.props.form.getFieldValue(value)){
      this.setState({remarkDisplay: 'block'});
    } else{
      this.setState({remarkDisplay: 'none'});
    }
  }
  getProName = (val) => {
    let proName;
    if (getSelectChild('prodDesc')) {
      for (const i in getSelectChild('prodDesc')) {
        if (getSelectChild('prodDesc')[i].key === val) {
          proName = getSelectChild('prodDesc')[i].props.children;
        }
      }
      return proName;
    }
  }
	applyEdit = (policyMapInfo, cPlyAppNo) => {
		this.props.dispatch({
			type: 'searchlist/goToEdit',
			payload: {
				...policyMapInfo,
        cPlyAppNoEditable: cPlyAppNo,
        isSelfchannelAudit: true,
        isButtonConfirm: true,
			}
		});
	}
  render() {
    const {searchlist:{queryChannelData},loading} = this.props;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    let total;
    let {dataSource} = this.state;
    if (queryChannelData) {
      dataSource = queryChannelData.list;
      total = queryChannelData.total || 0;
    }
    this.state.dataSource = dataSource;
    const { current } = this.state;
    const rowKey = (record,index) => {
      return index
    }

  const format = (val, code) => {
    let text;
    switch (val) {
      case '3': text = '待审核'; break;
      case '4': text = '已打回'; break;
      default: break;
    }
    return text;
  }

    const columns = [{
          title: '投保单号',
          dataIndex: 'cPlyAppNo',
          align: 'center'
        },
          
        {
          title: '销售录音电话',
          dataIndex: 'saleManTel',
          align: 'center'
        },
        {
          title: '销售人员姓名',
          dataIndex: 'teleLeadName',
          align: 'center'
        },
        {
          title: '销售人员编号',
          dataIndex: 'teleLeadCode',
          align: 'center'
        },
        {
          title: '产品名称',
          dataIndex: 'prodCName',
          align: 'center',
        }, 
        {
          title: '款别',
          dataIndex: 'feeType',
          align: 'center'
        }, 
        {
          title: '被保人姓名',
          dataIndex: 'insrncName',
          align: 'center'
        }, 
        {
          title: '投保日期',
          dataIndex: 'appDate',
          align: 'center'
        }, 
        {
          title: '审核状态',
          dataIndex: 'status',
          align: 'center',
          render: (text) => {
            return (
              <span>{format(text,'status')}</span>
            );
          }
        }, 
        {
          title: '审核结果',
          dataIndex: 'operation',
          align: 'center',
          width: 100,
          render:(text,index,record) => {
              return <FormItem>
                 {getFieldDecorator(`${index.cPlyAppNo}`,{initialValue: '通过'})(
                    <Select style={{width: 100,display : `${this.state.auditDisplay}`}}
                    onChange = {() => this.remarkDisplay(index.cPlyAppNo)}
                    >
                      <Option value="通过">通过</Option>
                      <Option value="打回">打回</Option>
                    </Select>
                  )} 
              </FormItem>
          }
        },
        {
          title: '备注',
          dataIndex: 'remark',
          align: 'center',
          render: (text, record) => this.renderColumns(text, record, 'remark', `${this.state.remarkDisplay}` ),
        },
        {
          title: '操作',
          dataIndex: 'action',
          align: 'center',
          render: (text, record) => {
            const { editable } = record;
            return (
              <Fragment>
                  {
                    editable ?
                      (<span>
                        <a onClick={() => this.save(record)}>保存</a>
                        <a onClick={() => this.cancel(record)}> 取消</a>
                      </span>)
                      : (<span>
                        <a onClick={() => this.edit(record)}> 编辑</a>
                        <a onClick={this.applyEdit.bind(this, record.policyMapInfo, record.cPlyAppNo)}> 查看</a>
                      </span>)
                  }
              </Fragment>
            );
          }
        }];
    return (
      <PageHeaderLayout title="">
            <Card bordered={false}>
                <div>{this.renderAdvancedForm()}</div>
                <Steps style={{width:'70%', margin:'0px auto 20px auto'}} current={current}>
                    {steps.map(item => <Step key={item.title} title={item.title} />)}
                </Steps>
                <Table 
                  rowKey={rowKey}
                  pagination={false} 
                  size='small' 
                  bordered
                  pagination={{
                    showQuickJumper: true,
                    showSizeChanger: true,
                    ...{
                        current:this.state.pageNum,
                        pageSize:this.state.pageSize,
                        total:total,
                    },
                  }} 
                  onChange={this.handleTablePage}
                  columns={columns} 
                  loading={loading}
                  dataSource={dataSource} />
            </Card>
      </PageHeaderLayout>
    );
  }
}

