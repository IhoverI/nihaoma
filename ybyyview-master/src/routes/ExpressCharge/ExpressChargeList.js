import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Popconfirm,
  Tabs,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './index.less';
import {getChannelNameByValue,getProductNameByValue,getSelectChild} from '../../utils/utils'
import SearchForm from './SearchForm';
import SearchExactForm from './SearchExactForm';

const { RangePicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;

@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
export default class ExpressCharge extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    pageNum: 1,
    rowNum: 10,
    flag: '1',
    farmatVal: {},
    disabled: true,
    expressCodeStatus : '1',
    feeDateRecordedFlag: [],
  };

  componentWillMount(){
    Object.keys(this.props.rule).forEach(key => delete this.props.rule[key]);
  }

  componentDidMount() {
    const { dispatch } = this.props;
  }
  handleFormReset = (form) => {
    const { dispatch } = this.props;
    form.resetFields();
    form.resetFields();
    this.setState({expressCodeStatus: '1',formValues: {},pageNum:1,rowNum: 10});
  };

  formaUnit = (val,code) => {
    let text = '';
    if (code === 'expressCode') {
      switch (val) {
        case 'E1003': text = '顺丰'; break;
        case 'E1010': text = '德邦'; break;
        case 'E1008': text = 'EMS'; break;
        default: break;
      }
    } else {
      switch (val) {
        case '0': text = '在途'; break;
        case '0': text = '签收'; break;
        case '2': text = '非本人签收'; break;
        case '3': text = '问题件'; break;
        case '4': text = 'EMS'; break;
        case '5': text = '初始化'; break;
        case '5': text = '电子保单'; break;
        default: break;
      }
    }
  
    return text;
}

getProName = (text, record) => {
  let proName;
  if (record.extEnterpCode === null) {
    proName = null;
  } else if (record.extEnterpCode !== null && record.prodNo !== null) {
    for (const i in getSelectChild('prodDesc')) {
      if (getSelectChild('prodDesc')[i].ref === record.extEnterpCode && getSelectChild('prodDesc')[i].props.value === record.prodNo) {
        proName = getSelectChild('prodDesc')[i].props.children
      }
    }
  }
  return proName;
}

  getColumns(){
    var _this = this;
    return [
      {
        title: '保单号',
        dataIndex: 'cPlyNo',
        align: 'center'
      },
      {
        title: '业务渠道',
        dataIndex: 'extEnterpCode',
        align: 'center',
         render:(val)=>{
          return getChannelNameByValue(val);
        },
      },
      {
          title: '产品名称',
          dataIndex: 'prodName',
          align: 'center',
          render:(text, record, index)=>{
            return (
              <span>{this.getProName(text, record)}</span>
            );
        },
      },
      {
        title: '快递公司',
        dataIndex: 'expressCode',
        align: 'center',
        render: (text) => {
          return (<span>{this.formaUnit(text,'expressCode')}</span>)
        }
      },
      {
        title: '快递单号',
        dataIndex: 'postNo',
        align: 'center',
      },
       {
        title: '快递状态',
        dataIndex: 'postStatus',
        align: 'center',
        render: (text) => {
          return (<span>{this.formaUnit(text,'postStatus')}</span>)
        }
      },
      {
        title: '快递签收时间',
        dataIndex: 'contractTime',
        align: 'center'
      },
      {
        title: '投保人姓名', 
        dataIndex: 'postName',
        align: 'center'
      },
      {
        title: '投递地址',
        dataIndex: 'postAddress',
        align: 'center'
      },
      {
        title: '回执入库时间', 
        dataIndex: 'scanCodeDate',
        align: 'center'
      },
      {
        title: '费用入账时间', 
        dataIndex: 'feeDateRecorded',
        align: 'center'
      },
      {
        title: '快递费用',
        dataIndex: 'expressPrm',
        align: 'center'
      }
      ];
  }

  handleSearch = (e,form) => {
    e.preventDefault();
    const { dispatch } = this.props;
    this.form = form;
    this.setState({pageNum:1,rowNum: 10});
    this.searchData(form,1,10);
  };

    handleTableChange = (pagination) => {
     this.setState({
      pageNum: pagination.current,
      rowNum: pagination.pageSize,
    },()=>{
      this.searchData(this.form,pagination.current,pagination.pageSize);
    });
  }

searchData =(form,pageNum,rowNum) => {
  const { dispatch } = this.props;
  form.validateFields((err, fieldsValue) => {
      this.setState({feeDateRecordedFlag: fieldsValue.feeDateRecorded});
      if (err) return;
      fieldsValue = this.perpareValues(fieldsValue);
      this.setState({
        farmatVal: {
          ...fieldsValue,
          pageNum: this.state.pageNum,
          rowNum: this.state.rowNum
        },
        disabled: false,
      });
      dispatch({
        type: 'rule/expressSettlementDeal',
        payload: {
          ...fieldsValue,
           pageNum: pageNum,
           rowNum: rowNum
        },
      });
    });
}

   perpareValues(values){
    if(!values.postDate||values.postDate.length===0){
      values.startTime = '';
      values.endTime = '';
    } else {
      values.startTime = values.postDate[0].format("YYYY-MM-DD 00:00:00");
      values.endTime = values.postDate[1].format("YYYY-MM-DD 23-59-59");
    }
    if(!values.feeDateRecorded||values.feeDateRecorded.length===0){
      values.startFeeDateRecorded = '';
      values.endFeeDateRecorded = '';
    } else {
      values.startFeeDateRecorded = values.feeDateRecorded[0].format("YYYY-MM-DD 00:00:00");
      values.endFeeDateRecorded = values.feeDateRecorded[1].format("YYYY-MM-DD 23-59-59");
    }
    if(!values.scanCodeDate||values.scanCodeDate.length===0){
      values.startScanCodeDate = '';
      values.endScanCodeDate = '';
    } else {
      values.startScanCodeDate = values.scanCodeDate[0].format("YYYY-MM-DD 00:00:00");
      values.endScanCodeDate = values.scanCodeDate[1].format("YYYY-MM-DD 23-59-59");
    }
    if (values.extEnterpNameList !== undefined) {
      values.extEnterpCode = values.extEnterpNameList
    } else {
      values.extEnterpCode = ''
    }
    if (values.prodNameList !== undefined) {
      values.prodNo = values.prodNameList || '';
    } else {
      values.prodNo = '';
    }

    delete values.postDate;
    delete values.feeDateRecorded;
    delete values.scanCodeDate;
    delete values.extEnterpNameList;
    delete values.prodNameList;

    for(const key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = '';
      }
    }
    return values;
  }

  selExpressCode = (val) => {
    if (val !== '' || val !== null) {
      this.setState({expressCodeStatus: '0'});
    }
  }
   

  renderAdvancedForm() {

    return (
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab={<span>模糊查询</span>} key="1">
          <SearchForm {...this.props} root={this}/>
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span>精确查询</span>} key="2">
        <SearchExactForm {...this.props} root={this}/>
        </Tabs.TabPane>
      </Tabs>
    );

  }

  printExcel = () => {
    this.props.dispatch({
      type: 'rule/exportFeeCollectExcel',
      payload: this.state.farmatVal,
    }).then(()=>{
    }).catch(e=>{
      // message.error('网络异常请稍后再试');
    });
  }

  printListExcel = () => {
     this.props.dispatch({
      type: 'rule/exportSettlementDealExcel',
      payload: this.state.farmatVal,
    }).then(()=>{
    }).catch(e=>{
      // message.error('网络异常请稍后再试');
    });
  }

  render() {
    const { loading, rule: {expressData,expressFeeCollectData} } = this.props;
    const { selectedRows, modalVisible } = this.state;
    let data;
    let total;
    if (expressData) {
      data = expressData.list || [];
      total = expressData.total || 0
    }
    const parentMethods = {
      handleAdd: this.handleAdd,
    };

    const rowKey = (recode,index) => {
      return index
    }

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div className={styles.ApplyList}>
            <div className={styles.ApplyListForm}>{this.renderAdvancedForm()}</div>
            <Table
                className='custom-table'
                size='small'
                loading={loading}
                scroll={{x:1500}}
                rowKey={rowKey}
                title={() => {
                  return <div>
                      <Button 
                        type='primary' 
                        disabled={this.state.feeDateRecordedFlag === undefined || this.state.feeDateRecordedFlag.length === 0 ? true : false}
                        onClick={this.printExcel}>
                        汇总导出
                      </Button>
                      <Button 
                        type='primary' 
                        onClick={this.printListExcel}
                        style={{marginLeft: 10}}>
                        清单导出
                      </Button>
                  </div>
                }}
                pagination={{
                showQuickJumper: true,
                showSizeChanger: true,
                ...{
                    current:this.state.pageNum,
                    pageSize:this.state.rowNum,
                    showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                    total:total,
                    pageSizeOptions:['10','20','30','40','50'],
                },
              }}
                bordered
                columns={this.getColumns()}
                onChange={this.handleTableChange}
                dataSource={data} />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
