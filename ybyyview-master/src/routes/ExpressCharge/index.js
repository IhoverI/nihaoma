import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import ExpressChargeList from './ExpressChargeList';
import SumBalance from './SumBalance';
import StateContainer from 'components/StateContainer';




@connect(({ rule, loading }) => ({
  rule,
}))
export default class ExpressCharge extends PureComponent {
  state = {
    step:0,
  };

  componentDidMount() {
    const { dispatch } = this.props;
   
  }

  render() {
    return (
      <StateContainer
        step={this.state.step}
        stepChildren={[
            <ExpressChargeList parent={this}/>,
            <SumBalance parent={this}/>
        ]}
      ></StateContainer>
    );
  }
}
