import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  Input,
  DatePicker,
} from 'snk-web';
import { getSelectChild } from '../../utils/utils'
import ChannelAndProduction from 'components/ChannelAndProduction';

const FormItem = Form.Item;
const { Option } = Select;

@Form.create()
export default class SearchForm extends PureComponent {
  
  constructor(props){
    super(props);
    this.state = {
      
    };
    props.root.nomalForm = props.form;
  }

  handleSearch(e){
    this.props.root.handleSearch(e,this.props.form);
  }
 
  render() {
    const { form,root } = this.props;
    const { getFieldDecorator,getFieldValue } = form;
    const { RangePicker } = DatePicker;
    const { loading } =this.props;

    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch.bind(this)} layout="inline">
        <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="保单年份">
              {getFieldDecorator('pyear',{initialValue: pyear})(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild('pyear')}
                </Select>
                )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="快递公司">
              {getFieldDecorator('expressCode',{initialValue: ''})(
                <Select  placeholder="请选择" style={{ width: '100%' }} onChange={this.selExpressCode}>
                  <Option value="E1003">顺丰</Option>
                  <Option value="E1010">德邦</Option>
                  <Option value="E1008">EMS</Option>
                </Select>)}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="费用入账日期">
              {getFieldDecorator('feeDateRecorded')( // ---- startFeeDateRecorded  endFeeDateRecorded
                <RangePicker  style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="快递下单时间">
              {getFieldDecorator('postDate')( // startTime  endTime
                <RangePicker  style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="回执入库日期">
              {getFieldDecorator('scanCodeDate')( //  startScanCodeDate  endScanCodeDate
                <RangePicker  style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
        </Row>
        <ChannelAndProduction allowClear ChannelRule={this.state.mustInputRule} {...this.props} />
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
          <Button type="primary" htmlType="submit" >
              查询
          </Button>
          <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
              重置
          </Button>
        </div>
      </Form>
    );
  }
}
