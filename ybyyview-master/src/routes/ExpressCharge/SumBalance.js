import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild } from '../../utils/utils'

const FormItem = Form.Item;

@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
export default class SumBalance extends PureComponent {
  constructor(props){
    super(props)
    this.state={
      codeValue: '',
      extEnterpCodeValue : '',
    }
  }
  goBack = ()=>{
      this.props.parent.setState({
          step:0
      });
  }

  render() {
    const { loading, rule: {expressData,expressFeeCollectData} } = this.props;
    const { getFieldDecorator } = this.props.form;
    let allGuaranteeSignCount,allGuaranteeUnSignCount,allGuaranteeUnSignCountAmount,allGuaranteeSignCountAmount,
        allExpressFee,allextEnterpCode,allProdNo,feeCollectTypes;
    if (expressFeeCollectData) {
        allGuaranteeSignCount = expressFeeCollectData.allGuaranteeSignCount || 0;
        allGuaranteeUnSignCount = expressFeeCollectData.allGuaranteeUnSignCount || 0;
        allGuaranteeUnSignCountAmount = expressFeeCollectData.allGuaranteeUnSignCountAmount || 0;
        allGuaranteeSignCountAmount = expressFeeCollectData.allGuaranteeSignCountAmount || 0;
        allExpressFee = expressFeeCollectData.allExpressFee || 0;
        allextEnterpCode = expressFeeCollectData.allextEnterpCode || [];
        allProdNo = expressFeeCollectData.allProdNo || [];
        feeCollectTypes = expressFeeCollectData.feeCollectTypes || [];

    }
    let rows = [];
    for (const i in feeCollectTypes) {
      if (feeCollectTypes[i]) {
        const item = (
          <tr>
            <td>{feeCollectTypes[i].feeType}</td>
            <td>{feeCollectTypes[i].guaranteeSignCount}</td>
            <td>{feeCollectTypes[i].guaranteeUnSignCount}</td>
          </tr>
        );
        rows.push(item)
      }
    }

    let extEnterpCodeList = [];
    let extEnterpDescList = [];

    if (getSelectChild('extEnterpDesc')) {
      for (const j in allextEnterpCode) {
        for (const i in getSelectChild('extEnterpDesc')) {
          if (allextEnterpCode[j] === getSelectChild('extEnterpDesc')[i].key) {
            extEnterpDescList.push(getSelectChild('extEnterpDesc')[i].props.children);
          }
        }
      }
    }
    if (getSelectChild('prodDesc')) {
      for (const j in allProdNo) {
        for (const i in getSelectChild('prodDesc')) {
          if (allProdNo[j] === getSelectChild('prodDesc')[i].key) {
            extEnterpCodeList.push(getSelectChild('prodDesc')[i].props.children);
          }
        }
      }
    }
    const codeList = [];
    const descList = [];
    for (const k in extEnterpDescList) {
      this.setState({extEnterpCodeValue: extEnterpDescList[0] ? extEnterpDescList[0] : ''});
      const item = (
        <Option value={extEnterpDescList[k]}>{extEnterpDescList[k]}</Option>
      );  
      descList.push(item)
    }
    for (const k in extEnterpCodeList) {
      this.setState({codeValue: extEnterpCodeList[0] ? extEnterpCodeList[0] : ''});
      const item = (
        <Option value={extEnterpCodeList[k]}>{extEnterpCodeList[k]}</Option>
      );
      codeList.push(item)
    }

    return (
      <PageHeaderLayout title="汇总结算">
        <Card bordered={false}>
          <Button onClick={this.goBack.bind(this)}>返回</Button>
          <div style={{margin: '50px 100px'}}>
            <div style={{marginBottom: 24, marginLeft: "8%"}}>
               <Form className='custom-form-wrapper' layout="inline">
                 <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
                  <Col md={8} sm={24}>
                      <FormItem label="渠道名称">
                        {getFieldDecorator('transationCode',{initialValue: this.state.extEnterpCodeValue})(
                          <Select placeholder="" style={{ width: '100%' }}>
                            {descList.map((item) => { return item})}
                          </Select>
                        )}
                    </FormItem>
                  </Col>
                  <Col md={8} sm={24}>
                      <FormItem label="产品名称">
                        {getFieldDecorator('transationCode',{initialValue: this.state.codeValue})(
                            <Select placeholder="" style={{ width: '100%' }}>
                                {codeList.map((item) => { return item})}
                            </Select>
                        )}
                    </FormItem>
                  </Col>
                  </Row>
               </Form>
          </div> 
            <table border="1" style={{width: '80%', textAlign: 'center'}}>
              <tbody>
                <tr>
                  <td>费用种类（元/单）</td>
                  <td>签收件数</td>
                  <td>未签收件数</td>
                </tr>
                {
                  rows.map((item) => { return item; })
                }
                <tr>
                  <td>数量汇总：</td>
                  <td>{allGuaranteeSignCount}</td>
                  <td>{allGuaranteeUnSignCount}</td>
                </tr>
                <tr>
                  <td>结算金额：</td>
                  <td>{allGuaranteeSignCountAmount}</td>
                  <td>{allGuaranteeUnSignCountAmount}</td>
                </tr>
                <tr>
                  <td>总金额：</td>
                  <td colSpan='2'>{allExpressFee}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
