import {
    Row,
    Col,
    Card,
    Form,
    Select,
    Button,
    Input,
    DatePicker,
} from 'snk-web';
import React from 'react';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getSelectChild } from '../../utils/utils'

const { RangePicker } = DatePicker;

const FormItem = Form.Item;

//装饰器
@connect(({ fixesFetch }) => ({
	fixesFetch,
}))

@Form.create()
export default class FixedFetch extends React.Component {
  state = {
    isBuildWin: false,
  }
    // 提交
  handleSubmit = e => {
		e.preventDefault();
		const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue = this.perpareValues(fieldsValue);
			dispatch({
				type: 'fixesFetch/getFixedFetch',
				payload: {
					...fieldsValue,
				},
			});
		});
  };
  handleColumnsChange = (value, option) => {
    // 2代表'民生build-win取数'
    if (value === '2') {
      this.setState({
        isBuildWin: true,
      });
    } else {
      this.setState({
        isBuildWin: false,
      })
    }
  }
  //重置表单
  handleReset = () => {
		this.props.form.resetFields();
  }

  //格式化表单时间
	perpareValues(values){
		if(!values.payDate||values.payDate.length===0){
			values.begin_date = null;
			values.end_date = null;
		}else{
			values.begin_date = values.payDate[0].format("YYYY-MM-DD");
			values.end_date = values.payDate[1].format("YYYY-MM-DD");
		}
		delete values.payDate;
		delete values.chargeDate;
		for(var key in values){
			if(values[key]===undefined||values[key]===""){
				values[key] = null;
			}
		}
		return values;
	}
  render() {
		const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderLayout title="固定取数">
				<Card>
				<Form className='custom-form-wrapper'  onSubmit={this.handleSubmit} layout="inline">
          <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
            <Col md={12} sm={24}>
							<FormItem label="清单类型">
								{getFieldDecorator('accessType', {
										initialValue:"1",
									})(
										<Select
                      placeholder="请选择"
                      style={{ width: '100%' }}
                      onChange = {this.handleColumnsChange}
                      >
                        {getSelectChild('fixedFetchAccessType')}
										</Select>
									)}
              </FormItem>
            </Col>
            <Col md={12} sm={24}>
              <FormItem label="请款日期">
                {
                  this.state.isBuildWin ? (
                    getFieldDecorator('buildWinDate', {
                      rules: [{required: true, message: '请输入请款日期'}]
                    })(
                      <Input placeholder="格式为2017-11,2017-12,并用,隔开" />)
                  ) : (
                    getFieldDecorator('payDate',{
                      rules: [{required: true, message: '请填入请款日期'}]
                    })(
                    <RangePicker style={{ width: '100%' }}/>)
                  )
                }
              </FormItem>
            </Col>
          </Row>
          <div style={{ overflow: 'hidden',textAlign:'center', marginTop: 24 }}>
            <span style={{ marginBottom: 24 }}>
              <Button type="primary" htmlType="submit">
                发送至邮箱
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </div>
        </Form>
				</Card>
			</PageHeaderLayout>
    );
  }
}