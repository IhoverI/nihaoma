import { 
  Table, 
  Popconfirm,
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider, } from 'snk-web';
import React, { PureComponent, Fragment } from 'react';
import styles from './EditableTable.less';
import moment from 'moment';
import { connect } from 'dva';
//import ClmForm from '../../components/ClmForm';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

const data = [];
for (let i = 0; i < 100; i++) {
  data.push({
    key: i.toString(),
    name: `Edrward ${i}`,
    age: 32,
    address: `London Park no. ${i}`,
  });
}
const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component{
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };
  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [{
                      required: true,
                      message: `Please Input ${title}!`,
                    }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>
          );
        }}
      </EditableContext.Consumer>
    );
  }
}

export default class EditableTable2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data, editingKey: '' };
    this.columns = [
      {
        title: '优先级',
        dataIndex: 'orderCode',
        editable: false,
        width: '15',
       },
      {
        title: '修理厂代码',
        dataIndex: 'thirdPartCode',
        editable:false,
        render: (text, record) => {
          // this.setState({editingRecord: record});
          // this.state.editingRecord = record;
          const {thirdPartCode} = record;
          return (
            <div>
              {thirdPartCode ? (
                <Fragment>
                <span>
                  <a onClick={() => this.handleClmVisible(true)}>{thirdPartCode}</a>
                </span>
               </Fragment> 
              ): 
              (<Fragment>
                  <span>
                  <a onClick={() => this.handleClmVisible(true)}>配置修理厂</a>
                  </span>
               </Fragment>
              )}
            </div>)
        },
      },
      {
        title: '修理厂名称',     
        dataIndex: 'clmName',
        editable:false,
        render: (text, record) => {
          const {thirdPartName} = record;
          return (
            <div>
              {thirdPartName ? (
                <Fragment>
                <span>
                   {thirdPartName}
                </span>
                </Fragment> 
              ): 
              (<Fragment>
                  <span>
                    自动带出
                  </span>
                </Fragment>
              )}
            </div>)
        },
      },
      {
        title: '网点代码',
        dataIndex: 'ruleCode',
        editable:true,
      },
      {
        title: '网点名称',
        dataIndex: 'ruleName',
        editable:false,
      },
      {
        title: '合作机构代码',
        dataIndex: 'chaCode',
        editable:false,
      },
      {
        title: '合作机构名称',
        dataIndex: 'chaName',
        editable:false,
      },
      {
        title: '修理厂组织机构编码',
        dataIndex: 'clmOrgCode',
        editable:false,
      },
      {
        title: '修理厂联系人',
        dataIndex: 'clmContactPerson',
        editable:false,
      },
      {
        title: '修理厂联系电话',
        dataIndex: 'clmContactTel',
        editable:false,
      },
      {
        title: '是否默认修理厂',
        dataIndex: 'clmDefault',
        editable:false,
      },
      {
        title: '优惠活动说明',
        dataIndex: 'clmBenefit',
        editable:false,
      },
      {
        title: '维度设置',
        width: '15',
        fixed: 'right',
        render: (text, record) => {
          const { editable } = record;
          return (
            <Fragment>
                {
                    <span>
                      <a onClick={() => this.handleDimVisible(true)}>维度设置</a>
                    </span>
                }
            </Fragment>
          );
        },
      },
      {
        title: 'operation',
        dataIndex: 'operation',
        
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <a
                        href="javascript:;"
                        onClick={() => this.save(form, record.key)}
                        style={{ marginRight: 8 }}
                      >
                        Save
                      </a>
                    )}
                  </EditableContext.Consumer>
                  <Popconfirm
                    title="Sure to cancel?"
                    onConfirm={() => this.cancel(record.key)}
                  >
                    <a>Cancel</a>
                  </Popconfirm>
                </span>
              ) : (
                <a onClick={() => this.edit(record.key)}>Edit</a>
              )}
            </div>
          );
        },
      },
      {
        title: '操作',
        dataIndex: 'operation2',
        width: '25',
        fixed: 'right',
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <a
                        href="javascript:;"
                        onClick={() => this.save(form, record.key)}
                        style={{ marginRight: 8 }}
                      >
                        保存
                      </a>
                    )}
                  </EditableContext.Consumer>
                  <Popconfirm
                    title="确定放弃编辑?"
                    onConfirm={() => this.cancel(record.key)}
                  >
                    <a>取消</a>
                  </Popconfirm>
                </span>
              ) : (
                <a onClick={() => this.edit(record)}>编辑</a>
              )}
            </div>
          );
        },
      },
    ];
  }
  isEditing = (record) => {
    return record.key === this.state.editingKey;
  };
  edit(key) {
    this.setState({ editingKey: key });
  }
  save(form, key) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.data];
      const index = newData.findIndex(item => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        this.setState({ data: newData, editingKey: '' });
      } else {
        newData.push(data);
        this.setState({ data: newData, editingKey: '' });
      }
    });
  }
  cancel = () => {
    this.setState({ editingKey: '' });
  };
  render() {
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === 'age' ? 'number' : 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    return (
      <PageHeaderLayout title="新增修理厂规则">
      <Card bordered={false}>
        <div className={styles.positionList}>
          {/* <div className={styles.positionListForm}>{this.renderForm()}</div> */}
          <div className={styles.positionListOperator}>
            <Button icon="plus" type="primary" onClick={this.handleAdd}>
              新建修理厂规则
            </Button>
          </div>
          <Table
            components={components}
            bordered
            dataSource={this.state.data}
            columns={columns}
            rowClassName="editable-row"
            scroll={{ x: 1750 }}
          />
        </div>
      </Card>
    </PageHeaderLayout>
    );
  }
}