import { 
  Table, 
  Popconfirm,
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider} from 'snk-web';
import React, { PureComponent, Fragment } from 'react';
import { routerRedux } from 'dva/router';
import styles from './EditableTable.less';
import {PermissionWrapper,PermissinSelectOption} from 'snk-sso-um';
import moment from 'moment';
import { connect } from 'dva';
//import ClmForm from '../../components/ClmForm';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { isArray } from 'util';

const FormItem = Form.Item;

const CreateDimForm = Form.create()(props => {
  const {dimVisible, handleDimVisible, form, parentProps, parentState, handleDimDataChange, editingRecord} = props;
  const {recommend, recommendForm, recommendDim, dispatch} = parentProps;
  const {districtData, provinceData} = recommend;
  let {carModelsData} = recommendDim;
  let districtOptions = [];
  districtOptions = districtData.map(city => <Select.Option key={city.value}>{city.text}</Select.Option>);
  
  // 定义编辑
  let classTypeField = [];
  let areaTypeField = [];
  let carTypeField = [];
  let uncarTypeField = [];

  const {key} = editingRecord;
  const newData = [...parentState.data];
  const index = newData.findIndex(editingRecord => key === editingRecord.key);
  let tmp = null;
  if (index > -1) {
     tmp = newData[index];
  }

  if(tmp && tmp.pushRepairDimDto){
    for(let i = 0; i < tmp.pushRepairDimDto.length; i ++){
      const item = tmp.pushRepairDimDto[i];
      if(item.recommendType == '1'){
        if(item.value && item.value.indexOf(',') != -1){
          classTypeField = item.value.split(',');
        }else{
          classTypeField = item.value;
        }
      }
      if(item.recommendType == '2'){
        if(item.value && item.value.indexOf(',') != -1){
          areaTypeField = item.value.split(',');
        }else{
          areaTypeField = item.value;
        }
      }
      if(item.recommendType == '3'){
        if(item.value && item.value.indexOf(',') != -1){
          carTypeField = item.value.split(',');
        }else{
          carTypeField = item.value;
        }
      }
      if(item.recommendType == '4'){
        if(item.value && item.value.indexOf(',') != -1){
          uncarTypeField = item.value.split(',');
        }else{
          uncarTypeField = item.value;
        }
      }
    }
    console.log('item...');
    console.log(classTypeField);
    console.log(areaTypeField);
    console.log(carTypeField);
    console.log(uncarTypeField);
  }

  const checkCarTypeAndUNCarType = () =>{
    const carInfo = form.getFieldsValue(["carType",'uncarType','classType']);
    const {carType,uncarType,classType} = carInfo;
    // if(carType.length===0&&uncarType.length===0){
    //   message.error("车型和排除车型必须填写其中一个");
    //   return false;
    // }
    if(carType.length>0&&uncarType.length>0){
      message.error("车型和排除车型只能填写其中一个");
      return false;
    }
    if(classType.length==0){
      message.error("请填写车辆大类");
      return false;
    }
    return true;
  }

  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(!checkCarTypeAndUNCarType()){
        return;
      }
      form.resetFields();
      console.log(fieldsValue);
      handleDimVisible(false);
      // 更新相关的data用于后续保存
      //handleAdd(fieldsValue);
      handleDimDataChange(fieldsValue);
    });
  };

  let currentValue = '';
  const handleCarTypeChange = (value, selectedOptions) => {
      console.log("handleCarTypeChange called...");
      console.log(value);
      let reg = new RegExp("[\\u4E00-\\u9FFF]+","g");
      if(reg.test(value)){
        //fetch(value, data => this.setState({ data }));
        currentValue = value;
        let carBrandData = {
          carBrand: value,
        };
        const {dispatch, recommend: { carModelsData }} = parentProps;
        console.log(carModelsData);
        dispatch({
          type: 'recommendDim/searchCarModels',
          data: carBrandData,
        }).then(()=>{
          if (currentValue === value) {
            console.log(parentProps.recommendDim.carModelsData);
            //const carModels = this.props.recommend.carModelsData;
            // callback(users);
          }
        }).catch((e)=>{
          console.log(e);
        });
      }else{
        

        
      }
  };

  // 车辆大类
  const carClassTypeOptions = [];
  carClassTypeOptions.push(<Select.Option key="1">1-标的车</Select.Option>);
  carClassTypeOptions.push(<Select.Option key="2">2-三者车</Select.Option>);
  carClassTypeOptions.push(<Select.Option key="3">3-异地车</Select.Option>);

  const onChange=(date, dateString) => {
    console.log(date, dateString);
  }
  

  // 车型查询
  let carModelsOptions = [];
  let unCarModelsOptions = [];
  if(!carModelsData){
    carModelsData = [];
  }

  carModelsOptions = carModelsData.map(carModel => <Select.Option key={carModel.value}>{carModel.text}</Select.Option>);
  unCarModelsOptions = carModelsData.map(carModel => <Select.Option key={carModel.value}>{carModel.text}</Select.Option>);
  
  let currentDate = Date.parse(new Date());

  return (
    <Modal
      title="维度设置"
      visible={dimVisible}
      onOk={okHandle}
      onCancel={() => {form.resetFields(); handleDimVisible(false);}}
    >
        <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
          <Col md={20} sm={20}>
            <FormItem label="车辆大类" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}> 
              {/* {form.getFieldDecorator('classType', {initialValue: classTypeField}) */}
              {form.getFieldDecorator('classType', {initialValue: classTypeField})
                (<Select style={{ width: 180 }} mode="multiple" >
                  {carClassTypeOptions}
                </Select>)
              }
            </FormItem>
          </Col>
          <Col md={20} sm={20}>
            <FormItem label="出险区域" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}> 
              {form.getFieldDecorator('areaType', {initialValue: areaTypeField})
                (<Select style={{ width: 180 }} mode="multiple">
                  {districtOptions}
                </Select>)
              }
            </FormItem>
          </Col>
          <Col md={20} sm={20}>
            <FormItem label="车型" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}> 
              {form.getFieldDecorator('carType', {initialValue: carTypeField})
                (<Select 
                  style={{ width: 180 }} 
                  mode="multiple" 
                  defaultActiveFirstOption={false}
                  showArrow={false}
                  filterOption={false}
                  onSearch={handleCarTypeChange}
                  >
                  {carModelsOptions}
                </Select>)
              }
            </FormItem>
          </Col>
          <Col md={20} sm={20}>
            <FormItem label="排除车型" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}> 
              {form.getFieldDecorator('uncarType', {initialValue: uncarTypeField})
                (<Select 
                  style={{ width: 180 }} 
                  mode="multiple" 
                  defaultActiveFirstOption={false}
                  showArrow={false}
                  filterOption={false}
                  onSearch={handleCarTypeChange}
                  >
                  {unCarModelsOptions}
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
    </Modal>
  );
});

let tableKey = 1;
const CreateClmForm = Form.create()(props => {
  const {clmVisible, handleClmVisible, form, parentProps, handleProvinceChange, handleClmSelected} = props;
  console.log(parentProps.recommendDim.selectedRowData)
  const {recommend, recommendForm, recommendDim, dispatch, selectedData} = parentProps;
  const {provinceData, cityData} = recommend;
  let state = {
    date: recommendDim.list,
    data: [],
    //rowSelection: {},
    //rowkey:'',
  }
  
  const columns = [
    {
      title: '修理厂代码',
      dataIndex: 'thirdPartCode',
      editable:false,
      width: '25%',
    },
    {
      title: '修理厂名称',
      dataIndex: 'thirdPartName',
      width: '25%',
      editable:false,
    },
    {
      title: '所在省',
      dataIndex: 'occurProvinceName',
      width: '20%',
      editable:false,
    },
    {
      title: '所在市',
      dataIndex: 'occurCityName',
      width: '20%',
      editable:false,
    },
  ];
  
  let cityOptions = [];
  let provinceOptions = [];
  cityOptions = cityData.map(city => <Option key={city.value}>{city.text}</Option>);
  provinceOptions = provinceData.map(province => <Option key={province.value}>{province.text}</Option>);

  console.log(props);

  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      console.log(fieldsValue);
      // 如果已经选中修理厂，则回填相关修理厂信息
      const {selectedRowData} = parentProps.recommendDim;
      if(selectedRowData.length > 0){
        if(handleClmSelected(selectedRowData)===false){
          return;
        }
      }else{
        message.error("请先选择修理厂");
      }
      handleClmVisible(false);
    });
  };


  const handleSearch = () => {
    console.log("handleSearch called...");
    const { dispatch} = parentProps;
    tableKey=new Date().valueOf();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      // this.setState({
      //   formValues: values,
      // });
      console.log(">>>>>>>>>>>>>handleSearch");
      console.log(fieldsValue);
      dispatch({
        type: 'recommendDim/searchClm',
        data: fieldsValue,
      }).then(()=>{
        console.log(props);
        console.log(recommendDim.list);
      }).catch((e)=>{
        console.log(e);
        message.error(e.message);
      })
    });
  };

  let currentDate = Date.parse(new Date());

  console.log(parentProps.recommendDim.selectedRowData);
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      const {dispatch} = parentProps;
      console.log(parentProps.recommendDim.selectedRowData)

      dispatch({
        type: 'recommendDim/setupSelectedRowData',
        data: selectedRows,
      });
      selectedRowKeys = [parentProps.recommendDim.selectedRowData.key];
    },
    
    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
    type: "radio",
  };

  console.log();
  
  return (
    <Modal
      title="修理厂查询"
      visible={clmVisible}
      onOk={okHandle}
      onCancel={() => handleClmVisible(false)}
    >
        <Row gutter={{ md: 12, lg: 12, xl: 24 }}>
          <Col md={10} sm={10}>
            <FormItem label="修理厂代码" labelCol={{ span: 12 }} wrapperCol={{ span: 12 }}> 
              {form.getFieldDecorator('thirdPartCode')
                (<Input style={{ width: 110 }} placeholder="输入修理厂代码" />)
              }
            </FormItem>
          </Col>
          <Col md={10} sm={10}>
            <FormItem label="修理厂名称" labelCol={{ span: 12 }} wrapperCol={{ span: 12 }}> 
              {form.getFieldDecorator('thirdPartName')
                (<Input style={{ width: 110 }} placeholder="输入修理厂名称" />)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 12, lg: 12, xl: 24 }}>
          <Col md={10} sm={10}>
              <FormItem label="修理厂省 :" labelCol={{ span: 12 }} wrapperCol={{ span: 12 }}> 
                {form.getFieldDecorator('occurProvinceCode', {initialValue: recommendForm.addMainRule.occurProvinceCode})
                  (<Select style={{ width: 160 }} onChange={handleProvinceChange} allowClear={true}>
                    {provinceOptions}
                  </Select>)
                }
              </FormItem>
            </Col>
        </Row>
        <Row gutter={{ md: 12, lg: 12, xl: 24 }}>
          <Col md={10} sm={10}>
            <FormItem label="修理厂市 :" labelCol={{ span: 12 }} wrapperCol={{ span: 12 }}> 
              {form.getFieldDecorator('occurCityCode',{initialValue: recommendForm.addMainRule.occurCityCode})
                (<Select style={{ width: 160 }} allowClear={true}>
                  {cityOptions}
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 12, lg: 12, xl: 24 }}>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
            <FormItem >
              {form.getFieldDecorator('button')
                (<Button type="primary" htmlType="submit" onClick={handleSearch} >
                  查询
                </Button>)
              }
            </FormItem>
            </span>
          </Col>
        </Row>
          
        <Table
            bordered
            dataSource={recommendDim.list}
            columns={columns}
            type={"radio"}
            key={tableKey}
            rowSelection={rowSelection} 
            rowClassName="editable-row"
          />
    </Modal>
  );
});


const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };
  
  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;


    return (
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [{
                      required: true,
                      message: `请输入 ${title}!`,
                    }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>
          );
        }}
      </EditableContext.Consumer>
    );
  }
}

@connect(({recommend, recommendForm, recommendDim, loading}) => ({
  recommend,
  recommendForm,
  recommendDim,
  loading: loading.models.recommend,
}))

@Form.create()
export default class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    // alert(props.match.params.rulecode);
    this.state = { 
      data: [], 
      editingKey: '', 
      dimVisible: false,
      clmVisible: false,
      editingRecord: {},
    };

    // 优先级
    let orderArray = [];
    // <Select.Option value="">请选择</Select.Option>
    //orderArray.push(<Select.Option value="0">0</Select.Option>)
    for(let i = 1; i < 40; i ++){
       orderArray.push(<Select.Option key={i}>{i}</Select.Option>);
    }

    this.columns = [
      {
        title: '操作',
        dataIndex: 'operation',
        width: '180px',
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <a
                        href="javascript:;"
                        onClick={() => this.save(form, record.key)}
                        style={{ marginRight: 8 }}
                      >
                        保存
                      </a>
                    )}
                  </EditableContext.Consumer>
                    <Popconfirm
                      title="确定放弃编辑?"
                      onConfirm={() => {
                        if(this.isExistDefaultClumFlag(this.state.data)){
                          message.error("只能存在一个默认的修理厂");
                          return;
                        }
                        this.cancel(record.key)
                      }}
                    >
                      <a style={{ marginRight: 8 }}>取消</a>
                    </Popconfirm>

                    {(record.key + '').indexOf('new') == -1 ?
                      (<Popconfirm
                        title="确定删除?"
                        onConfirm={() => {
                          this.deleteRecord(record.key);
                        }}
                      >
                        <a style={{ marginRight: 8 }}>删除</a>
                      </Popconfirm>):(<div></div>)}
                  <EditableContext.Consumer>
                    
                    {form => (
                      <a  style={{ marginRight: 8 }} onClick={() => this.handleDimVisible(true)}>维度设置</a>
                    )}
                  </EditableContext.Consumer>
                </span>
              ) : (
                <span>
                   <a onClick={() => this.edit(record)}>编辑</a>
                </span>
              )}
            </div>
          );
        },
      },
      {
        title: '优先级(1为优先级最高)',
        dataIndex: 'orderCode',
        editable: false,
        width: '5',
        render: (text, record) => {
          const {orderCode} = record;
          return (
            <div>
               {(this.state.editingKey == record.key) ? (
                  <Select style={{ width: 100 }} 
                    onChange={(recordSelected) => {this.handleOrderCodeChange(recordSelected,record)}}
                    defaultValue = {orderCode} 
                    >
                    {orderArray}
                  </Select>
                ): 
                  orderCode ?
                  (<span>
                      <Select style={{ width: 100 }} 
                        onChange={(recordSelected) => this.handleOrderCodeChange(recordSelected,record)}
                        defaultValue = {orderCode} 
                        disabled
                        >
                        {orderArray}
                      </Select>
                    </span>)
                      :
                      (<span>
                        <Select style={{ width: 100 }} 
                          onChange={(recordSelected) => this.handleOrderCodeChange(recordSelected,record)}
                          disabled
                          >
                          {orderArray}
                        </Select>
                      </span>)
                }
            </div>)
        },
       },
       {
        title: '是否默认修理厂',
        dataIndex: 'defaultClumFlag',
        editable:false,
        render: (text, record) => {
          const {defaultClumFlag} = record;
          return (
            <div>
               {(this.state.editingKey == record.key) ? (
                  <Select style={{ width: 100 }} 
                    onChange={(recordSelected) => this.handleDefaultChange(recordSelected, record)}
                    defaultValue = {defaultClumFlag}
                    >
                    <Select.Option value="0">否</Select.Option>
                    <Select.Option value="1">是</Select.Option>
                  </Select>
                ): 
                  defaultClumFlag ?
                    (<span>
                        <Select style={{ width: 100 }} 
                          onChange={(recordSelected) => this.handleDefaultChange(recordSelected, record)}
                          defaultValue = {defaultClumFlag}
                          disabled
                          >
                          <Select.Option value="0">否</Select.Option>
                          <Select.Option value="1">是</Select.Option>
                        </Select>
                      </span>)
                       :
                        (<span>
                          <Select style={{ width: 100 }} 
                            onChange={(recordSelected) => this.handleDefaultChange(recordSelected, record)}
                            defaultValue = '1'
                            disabled
                            >
                            <Select.Option value="0">否</Select.Option>
                            <Select.Option value="1">是</Select.Option>
                          </Select>
                        </span>)
                }
            </div>)
        },
      },
      {
        title: '修理厂代码',
        dataIndex: 'thirdPartCode',
        editable:false,
        render: (text, record) => {
          // this.setState({editingRecord: record});
          // this.state.editingRecord = record;
          const {thirdPartCode} = record;
          return (
            <div>
              {thirdPartCode ? (
                <Fragment>
                <span>
                  {!this.isEditing(record)?thirdPartCode:<a onClick={() => this.handleClmVisible(true)}>{thirdPartCode}</a>}
                </span>
               </Fragment> 
              ): 
                (this.state.editingKey == record.key) ?
                  (<Fragment>
                      <span>
                      <a onClick={() => this.handleClmVisible(true)}>配置修理厂</a>
                      </span>
                  </Fragment>
                  )
                  :
                  (<Fragment>
                    <span>
                    <a>配置修理厂</a>
                    </span>
                 </Fragment>
                )
            }
            </div>)
        },
      },
      {
        title: '修理厂名称',     
        dataIndex: 'thirdPartName',
        editable:false,
        render: (text, record) => {
          const {thirdPartName} = record;
          return (
            <div>
              {thirdPartName ? (
                <Fragment>
                <span>
                   {thirdPartName}
                </span>
                </Fragment> 
              ): 
              (<Fragment>
                  <span>
                    请配置
                  </span>
                </Fragment>
              )}
            </div>)
        },
      },
      {
        title: '合作渠道',
        dataIndex: 'chaCode',
        editable:false,
        render: (text, record) => {
          const {chaList} = record;
          // console.log('合作机构....' + record);
          // console.log(record);
          let chaOptions = [];
          if(chaList){
            chaOptions = chaList.map(cha => <Select.Option key={cha.value}>{cha.text}</Select.Option>);
          }
          return (
            <div>
              {(this.state.editingKey == record.key)  ? (
                <Select style={{ width: 250 }} 
                onChange={this.handleChaChange.bind(this)} 
                value={record.chaCode}
                onFocus={() => this.handleShowChaCode(record)}
                //defaultValue={chaOptions[0].key}
                >
                    {chaOptions.length===0 && record.chaCode?(
                      <Select.Option key={record.chaCode}>{record.cName}</Select.Option>
                    ):chaOptions}
                </Select>
                ) : 
                  chaList ?
                    (<span>{record.cName?record.cName:record.chaCode}</span>)
                    :
                    (!record.chaCode?<span>请配置</span>:(
                      <span>{record.cName}</span>
                    ))
              }
            </div>
          )
        },
      },
      {
        title: '渠道网点',
        dataIndex: 'chaOuterNodeId',
        editable:false,
        render: (text, record) => {
          const {outerList} = record;
          let outerOptions = [];
          if(outerList){
            outerOptions = outerList.map(outer => <Select.Option key={outer.value}>{outer.text}</Select.Option>);
          }
          return (
            <div>
              {/* {(outerList && this.state.editingKey == record.key) ? (
                <Select style={{ width: 250 }} onChange={this.handleOuterNoteChange}>
                    {outerOptions}
                </Select>
                ) : 
                  outerList ? 
                    (
                      <Select style={{ width: 250 }} dis>
                          {outerOptions}
                      </Select>
                    )
                    :
                    (
                    <span>
                      请选择
                    </span>
                    )
                } */}
     
                {(this.state.editingKey == record.key)  ? (
                  <Select style={{ width: 250 }} 
                  value={record.chaOuterNodeId}
                  onChange={this.handleOuterCodeChange}
                  onFocus={() => this.handleShowOuterCode(record)}
                  >
                      {outerOptions.length===0 && record.chaCode?(
                        <Select.Option key={record.chaOuterNodeId}>{record.outerNodeName}</Select.Option>
                      ):outerOptions}
                  </Select>
                  ) : 
                  outerList ?
                      (<span>{record.outerNodeName?record.outerNodeName:record.chaOuterNodeId}</span>)
                      :
                      (!record.chaOuterNodeId?<span>请配置</span>:(
                        <span>{record.outerNodeName?record.outerNodeName:record.chaOuterNodeId}</span>
                      ))
                }
        
            </div>
          )
        },
      },
      {
        title: '修理厂组织机构编码',
        dataIndex: 'businessCode',
        editable:false,
        render: (text, record) => {
          const {businessCode} = record;
          return (
            <div>
              {businessCode ? (
                <Fragment>
                <span>
                   {businessCode}
                </span>
                </Fragment> 
              ): 
              (<Fragment>
                  <span>
                    请配置
                  </span>
                </Fragment>
              )}
            </div>)
        },
      },
      {
        title: '修理厂联系人',
        dataIndex: 'contactName',
        editable:false,
        render: (text, record) => {
          const {contactName} = record;
          return (
            <div>
              {contactName ? (
                <Fragment>
                <span>
                   {contactName}
                </span>
                </Fragment> 
              ): 
              (<Fragment>
                  <span>
                    请配置
                  </span>
                </Fragment>
              )}
            </div>)
        },
      },
      {
        title: '修理厂联系电话',
        dataIndex: 'contactMobile',
        editable:false,
        render: (text, record) => {
          const {contactMobile} = record;
          return (
            <div>
              {contactMobile ? (
                <Fragment>
                <span>
                   {contactMobile}
                </span>
                </Fragment> 
              ): 
              (<Fragment>
                  <span>
                    请配置
                  </span>
                </Fragment>
              )}
            </div>)
        },
      },
      {
        title: '优惠活动说明',
        dataIndex: 'clmBenefit',
        editable:false,
      },
    ];
  }

  componentDidMount() {
    console.log('componentDidMount called..');
    const {dispatch, recommend, recommendDim, recommendForm} = this.props;

    if(!recommendForm.addMainRule.occurProvinceCode){
       message.error('请先查询再编辑~');
       dispatch(routerRedux.push(`/recommend/search`));
    }

    console.log(this.state);
    console.log(this.props.recommend);
    console.log(this.props.recommendForm);
    // todo:出险区域方法,应该在dimform初始化的时候做

    // 所有的省份
    dispatch({
      type: 'recommend/searchProvinceData',
    }).then(()=>{
      console.log(this.props.recommend.provinceData);
    }).catch((e)=>{
      console.log(e);
    });

    let occurCityCodeSearch = {
      codeCode: recommendForm.addMainRule.occurProvinceCode,
    }

    let occurDistrictCodeSearch = {
      codeCode: recommendForm.addMainRule.occurCityCode,
    }
  
    if(recommendForm.addMainRule.occurProvinceCode){
          //查询对应的所有城市
        dispatch({
          type: 'recommend/searchCityByPrveData',
          data: occurCityCodeSearch,
        }).then(()=>{
          console.log("cityData.....>>>>>>>>");
          console.log(this.props.recommend.cityData);
        }).catch((e)=>{
          console.log(e);
        });
    }
    
    if(recommendForm.addMainRule.occurCityCode){
      //查询对应的所有区
      dispatch({
        type: 'recommend/searchDistrictByCityData',
        data: occurDistrictCodeSearch,
      }).then(()=>{

      }).catch((e)=>{
        console.log(e);
      });
    }
    console.log("setState....");
    console.log(recommendForm.pushRepairRule);
    this.setState({data:recommendForm.pushRepairRule.list});
  }

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps called...");
    console.log(nextProps.recommendDim);
    if(this.parentClmMethods) {
      this.parentClmMethods.parentProps = nextProps;
    }
    if(this.handleClmSelected){
      this.handleClmSelected.recommendDim = nextProps.recommendDim;
      console.log(">>>>this.handleClmSelected");
    }
  }

  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    let currentValue = '';

    const handleChangeSelection = (value) => {
      console.log(value);
      this.setState({ value });
      fetch(value, data => this.setState({ data }));
    }

    const handleChangeStatusSelection = (value) => {
      console.log(value);
      this.setState({ value });
    }
   
    function onChange(value, selectedOptions) {
      console.log(value, selectedOptions);
    }

    const handleProvinceChange = (value) => {
      console.log("handleProvinceChange called..");
      console.log(value);
      const { dispatch } = this.props;
      let assembleCityDate = {
        codeCode: value,
      };
      dispatch({
        type: 'recommend/searchCityByPrveData',
        data: assembleCityDate,
      }).then(()=>{
        console.log(recommend);
        cityOptions = this.props.recommendDim.cityData.map(city => <Option key={city.value}>{city.text}</Option>);
      }).catch((e)=>{
        console.log(this.state.cityData);
        console.log(e);
      });
    }
  
    const onSecondCityChange = (value) => {
      console.log(value);
      this.setState({
        secondCity: value,
      });
    }

    // 保存主规则信息
    const handleSaveMain = () => {
      console.log('handleSaveMain called..');
      const {dispatch,form} = this.props;
      console.log(form);
      form.validateFields((err, fieldsValue) => {
        if (err) return;
  
        const values = {
          ...fieldsValue,
          updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
        };

        const valueData = {
          ...fieldsValue,
        }
        console.log(fieldsValue);
        dispatch({
          type: 'recommend/editRecommendMain',
          data: valueData,
        }).then(()=>{
          message.success("保存成功！");
        }).catch((e)=>{
          console.log(e);
          message.error(e.message);
        });
      });
    }

    const provinceOptions = this.props.recommend.provinceData.map(province => <Select.Option key={province.value}>{province.text}</Select.Option>);
    let cityOptions = this.props.recommend.cityData.map(city => <Select.Option key={city.value}>{city.text}</Select.Option>);
    const {addMainRule} = this.props.recommendForm;
    console.log(addMainRule);
    return (
      <Form onSubmit={() => handleSaveMain()} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="出险省"> 
              {getFieldDecorator('occurProvinceCode',{initialValue: addMainRule.occurProvinceCode})
                (<Select disabled style={{ width: 180 }} onChange={handleProvinceChange}>
                  {provinceOptions}
                </Select>)
              }
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="出险市">
              {getFieldDecorator('occurCityCode',{initialValue: addMainRule.occurCityCode})
                (<Select disabled style={{ width: 180 }} onChange={this.onSecondCityChange}>
                 {cityOptions}
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="规则编码">
              {getFieldDecorator('ruleCode',{initialValue: addMainRule.ruleCode})(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="规则名称">
              {getFieldDecorator('ruleName',{initialValue: addMainRule.ruleName})(<Input placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="生效时间">
              {getFieldDecorator('bgnTime',{initialValue: moment(parseInt(addMainRule.bgnTime))})(<DatePicker onChange={onChange} />)}
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="失效时间">
              {getFieldDecorator('endTime',{initialValue: moment(parseInt(addMainRule.endTime))})(<DatePicker onChange={onChange} />)}
            </FormItem>
          </Col>
        </Row>   
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="规则操作时间">
              {getFieldDecorator('updatedTime',{initialValue: moment(parseInt(addMainRule.updatedTime))})(<DatePicker disabled onChange={onChange} />)}
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="规则操作人">
              {getFieldDecorator('updatedUser',{initialValue: addMainRule.updatedUser})(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="是否有效">   
            {addMainRule.status && addMainRule.status == 1 ? 
              getFieldDecorator('status', {initialValue: addMainRule.status})
              (<Select style={{ width: 120 }}>
                <Select.Option value="1">有效</Select.Option>
                <Select.Option value="0">失效</Select.Option>
              </Select>) : 
            
              getFieldDecorator('status', {initialValue: addMainRule.status})
              (<Select style={{ width: 120 }} disabled>
                <Select.Option value="1">有效</Select.Option>
                <Select.Option value="0">失效</Select.Option>
              </Select>) 
            }  
            </FormItem>  
          </Col>
          <Col md={10} sm={10}>
            <FormItem label="审批状态">
              {getFieldDecorator('approveStatus', {initialValue: addMainRule.approveStatus})
                (<Select style={{ width: 120 }} onChange={handleChangeStatusSelection}>
                  <Select.Option value="0" disabled>待审批</Select.Option>
                  <Select.Option value="1">审批通过</Select.Option>
                  <Select.Option value="2">审批不通过</Select.Option>
                  <Select.Option value="3">暂存</Select.Option>
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <Button type="primary" htmlType="submit">
                保存主规则信息
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderSimpleForm();
  };

  /**
   *  选择网点代码时触发
   *  2018-05-29
   */
  handleOuterNoteChange = (outerCode) => {
    console.log("handleOuterNoteChange called..");
    console.log(outerCode);
    const {data} = this.state;
    const newData = [...this.state.data];
    const {key} = this.state.editingRecord;
    const index = newData.findIndex(item => key === item.key);
    if (index > -1) {
      let item = newData[index];
      item.chaOuterNodeId = outerCode;
      newData.splice(index, 1, {
        ...this.state.editingRecord,
        ...item,
      });
      this.setState({ data: newData });
      console.log(newData);
    }
  }

   /**
   *  选择渠道代码时触发
   *  2018-05-29
   */
  handleChaChange = (chaCode) => {
    console.log("handleChaChange called..");
    //console.log(chaCode);
    console.log(this.state.data);
    const {data} = this.state;
    const newData = [...this.state.data];
    const {key} = this.state.editingRecord;
    const index = newData.findIndex(item => key === item.key);
    if (index > -1) {
      let item = newData[index];
      item.chaCode = chaCode;
      // item.cName = 
      newData.splice(index, 1, {
        ...this.state.editingRecord,
        ...item,
      });
      this.setState({ data: newData });
      console.log(newData);
    }
    // // 选中渠道后开始加载外部网点
    // const chaCodeData = {
    //   chaCode: chaCode,
    // }
    // const {dispatch} = this.props;
    // dispatch({
    //   type: 'recommendDim/searchOuterCodeByChaCode',
    //   data: chaCodeData,
    // }).then(()=>{
    //   const index = newData.findIndex(item => key === item.key);
    //   if (index > -1) {
    //     const item = newData[index];
    //     item.outerList = this.props.recommendDim.outerList;
    //     newData.splice(index, 1, {
    //       ...this.state.editingRecord,
    //       ...item,
    //     });
    //     console.log(newData);
    //     this.setState({ data: newData });
    //   }
    // }).catch((e)=>{
    //   console.log(e);
    // });

     //console.log(chaCode);
    //  console.log(this.state.data);
    //  const {data} = this.state;
    //  const newData = [...this.state.data];
    //  const {key} = this.state.editingRecord;
    //  const index = newData.findIndex(item => key === item.key);
    //  if (index > -1) {
    //    let item = newData[index];
    //    item.chaCode = chaCode;
    //    newData.splice(index, 1, {
    //      ...this.state.editingRecord,
    //      ...item,
    //    });
    //    this.setState({ data: newData });
    //    console.log(newData);
    //  }
  }


   /**
   *  点击渠道代码时查询渠道
   *  2018-05-29
   */
  handleShowChaCode = (record) => {
    console.log("handleShowChaCode called..");
    console.log(record);
     // 根据修理厂找到渠道信息
     const {dispatch, recommendDim} = this.props;
     const thirdPartCodeData = {thirdPartCode: record.thirdPartCode} ;
     dispatch({
       type: 'recommendDim/searchChaCodeByClm',
       data: thirdPartCodeData,
     }).then(()=>{
       console.log(recommendDim);
       const newData = [...this.state.data];
       const key = record.key;
       const index = newData.findIndex(item => key === item.key);
       if (index > -1) {
         const item = newData[index];
         //delete record.key;
         item.chaList = this.props.recommendDim.chaList;
         console.log('searchChaCodeByClm-------------');
         console.log(record);
         newData.splice(index, 1, {
           ...item,
         });
         console.log(this.props.recommendDim);
         console.log(newData);
         this.setState({ data: newData });
         this.setState({outerList:recommendDim.outerList});
       }
     }).catch((e)=>{
       console.log(e);
     });
  }

   /**
   *  选择渠道代码时触发
   *  2018-05-29
   */
  handleOuterCodeChange = (chaOuterNodeId) => {
    console.log("handleChaChange called..");
    console.log(this.state.data);
    const {data} = this.state;
    const newData = [...this.state.data];
    const {key} = this.state.editingRecord;
    const index = newData.findIndex(item => key === item.key);
    if (index > -1) {
      let item = newData[index];
      item.chaOuterNodeId = chaOuterNodeId;
      newData.splice(index, 1, {
        ...this.state.editingRecord,
        ...item,
      });
      this.setState({ data: newData });
      console.log(newData);
    }
  }

   /**
   *  点击外部网点时查询
   *  2018-05-29
   */
  handleShowOuterCode = (record) => {
    console.log("handleShowOuterCode called..");
    console.log(record);
     // 根据渠道查外部网点信息
     const {dispatch, recommendDim} = this.props;

     const chaCodeData = {
        chaCode: record.chaCode,
      }
     const {data} = this.state;
     const newData = [...this.state.data];
     dispatch({
      type: 'recommendDim/searchOuterCodeByChaCode',
      data: chaCodeData,
     }).then(()=>{
      const key = record.key;
      const index = newData.findIndex(item => key === item.key);
      if (index > -1) {
        const item = newData[index];
        item.outerList = this.props.recommendDim.outerList;
        newData.splice(index, 1, {
          ...this.state.editingRecord,
          ...item,
        });
        console.log(newData);
        this.setState({ data: newData });
      }
    }).catch((e)=>{
      console.log(e);
    });
  }

  /**
   *  优先级有变化时触发
   *  2018-05-29
   */
  handleOrderCodeChange = (recordSelected, record) => {
    console.log("handleOrderCodeChange called..");
    console.log(recordSelected);
    console.log(record);
    const {data} = this.state;
    const newData = [...this.state.data];
 
    const {key} = this.state.editingRecord;
    const index = newData.findIndex(record => key === record.key);
    if (index > -1) {
      let item = newData[index];
      item.orderCode = recordSelected;
      console.log(record.orderCode);
      newData.splice(index, 1, {
        ...this.state.editingRecord,
        ...item,
      });
      this.setState({ data: newData },()=>{
        if(this.isExistSameOrderCode(this.state.data)){
          message.error('不能包含相同的优先级！请修改');
          return;
        }
      });
    }
  };

  isExistSameOrderCode(data){
    var dict = {};
    var re = false;
    for(let i = 0,j=data.length;i<j;i+=1){
      const item = data[i];
      if(dict[item.orderCode]){
        re = true;
        break;
      }else{
        dict[item.orderCode] = true;
      }
    }
    return re;
  }
  isExistOrderCode(key, newData){
    var dict = {};
    const item = newData[0];
    const index = newData.findIndex(item => key === item.key);
    if (index > -1) {
      let item = newData[index];
      console.log('isExistOrderCode');
      console.log(item.orderCode);
      if(item.orderCode){
        return true;
      }
    }
    return false;
  }

  isExistDefaultClumFlag = (data) => {
    var count = 0;
    for(let i = 0,j=data.length;i<j;i+=1){
      const item = data[i];
      if(item.defaultClumFlag==="1"){
        count+=1;
      }
    }
    return count>=2;
  }
  /**
   *  默认修理厂有变化时触发
   *  2018-05-29
   */
  handleDefaultChange = (recordSelected, record) => {
    console.log("handleDefaultChange called..");
    console.log(recordSelected);
    console.log(record);
    const {data} = this.state;
    const newData = [...this.state.data];
    const {key} = this.state.editingRecord;
    const index = newData.findIndex(item => key === item.key);
    if (index > -1) {
      let item = newData[index];
      item.defaultClumFlag = recordSelected;
      newData.splice(index, 1, {
        ...this.state.editingRecord,
        ...item,
      });
      this.setState({ data: newData },()=>{
        if(this.isExistDefaultClumFlag(this.state.data)){
          message.error("只能存在一个默认的修理厂");
        }
      });
    }
  }

  /**
   *  为规则添加一行
   *  2018-05-29
   */
  handleAdd = () => {
    console.log("add.js handleAdd called..");
    const {data, editingKey} = this.state;
    if(editingKey != ''){
      message.error("你有规则在编辑状态，请先处理后再新增！");
    }else{
      console.log(data);
      const key = 'new' + Math.random() * 1000;
      const newData = {
        key: key,
        defaultClumFlag: '0',
        orderCode: '',
      };
      if(data){
        this.setState({
          data: [...data, newData],
        });
      }   
      this.edit(newData); // 状态更新为“编辑中”,规则编码默认new
    }
  }

  /*
    * DIM Form(维度设置弹出框)
    * 维度设置弹出框 
    */
  // -----start-----
  handleDimVisible = flag => {
    console.log("list handleDimVisible called");
    this.setState({
      dimVisible: !!flag,
      recommendDim: {},
    });
    console.log(this.state.dimVisible);
  };

  // 设置data
  handleDimDataChange = fieldsValue => {
    console.log("handleDimDataChange called..");
    console.log(fieldsValue);
    const newData = [...this.state.data];
    const {key} = this.state.editingRecord;
    const index = newData.findIndex(item => key === item.key);

    if (index > -1) {
      const item = newData[index];
      let pushRepairDimDto = [];
      let classTypeList = {"recommendType":"1","value":fieldsValue.classType};
      let areaTypeList = {"recommendType":"2","value":fieldsValue.areaType};
      let carTypeList = {"recommendType":"3","value":fieldsValue.carType};
      let uncarTypeList = {"recommendType":"4","value":fieldsValue.uncarType};

      if(isArray(fieldsValue.classType) && fieldsValue.classType.length > 0){
        pushRepairDimDto.push(classTypeList);
      }
      if(isArray(fieldsValue.areaType) && fieldsValue.areaType.length > 0){
        pushRepairDimDto.push(areaTypeList);
      }
      if(isArray(fieldsValue.carType) && fieldsValue.carType.length > 0){
        pushRepairDimDto.push(carTypeList);
      }
      if(isArray(fieldsValue.uncarType) && fieldsValue.uncarType.length > 0){
        pushRepairDimDto.push(uncarTypeList);
      }
      item.pushRepairDimDto = pushRepairDimDto;
      
      //delete record[0].key;
      newData.splice(index, 1, {
        ...this.state.editingRecord,
        ...item,
      });
      
      this.setState({ data: newData });
      console.log("设置维度保存到data...");
      console.log(newData);
    }
  };
  // -----end-----

  /*
    * CLM Form(修理厂查询弹出框)
    * 维度设置弹出框 
    */
  // -----start-----
  handleClmVisible = flag => {
    console.log("list handleClmVisible called");
    if(!flag){
      const {dispatch} = this.props;
      dispatch({
        type: 'recommendDim/clearClmDim',
        data: {},
      });
    }
    this.setState({
      clmVisible: !!flag,
    });
    console.log(this.state.clmVisible);
  };

  // 回填信息修理厂选中的信息
  handleClmSelected = record => {
    console.log("list handleClmSelected called");

    const {data} = this.state;
    const newData = [...this.state.data];

    for(var i = 0,j=newData.length;i<j;i+=1){
      const curRowdata = newData[i];
      if(curRowdata.key!==this.state.editingRecord.key){
        if(curRowdata.thirdPartCode===record[0].thirdPartCode){
          message.error("同一个修理厂不能配两条数据,请修改");
          return false;
        }
      }else{
        curRowdata.chaCode = null;
        curRowdata.outerCode = null;
      }
    }    
    const {key} = this.state.editingRecord;
    const index = newData.findIndex(item => key === item.key);
    if (index > -1) {
      const item = newData[index];
      console.log('newData....');
      console.log(item);
      console.log(record[0]);
      delete record[0].key;
      delete record[0].id;
      delete record[0].defaultClumFlag;
      delete record[0].orderCode;
      delete record[0].pushRepairDimDto;
      newData.splice(index, 1, {
        ...item,
        ...record[0],
      });
      this.setState({ data: newData });
      console.log(newData);
      /*// 根据修理厂找到渠道信息
      const {dispatch, recommendDim} = this.props;
      const thirdPartCodeData = {thirdPartCode: record[0].thirdPartCode} ;
      dispatch({
        type: 'recommendDim/searchChaCodeByClm',
        data: thirdPartCodeData,
      }).then(()=>{
        console.log(recommendDim);
        const index = newData.findIndex(item => key === item.key);
        if (index > -1) {
          const item = newData[index];
          delete record[0].key;
          //record[0].outerList = this.props.recommendDim.outerList;
          record[0].chaList = this.props.recommendDim.chaList;
          newData.splice(index, 1, {
            ...item,
            ...record[0],
          });
          console.log(this.props.recommendDim);
          console.log(newData);
          this.setState({ data: newData });
          //this.setState({outerList:recommendDim.outerList});
        }
      }).catch((e)=>{
        console.log(e);
      });*/
    } else {
      newData.push(data);
    }
  };
  // -----end-----


   /*
    * 根据省找到市
    * 维度设置弹出框 
    */
  handleProvinceChange = (value) => {
    console.log("handleProvinceChange called..");
    console.log(value);
    const { dispatch } = this.props;
    let assembleCityDate = {
      codeCode: value,
    };
    dispatch({
      type: 'recommend/searchCityByPrveData',
      data: assembleCityDate,
    }).then(()=>{
      console.log(recommend);
      cityOptions = this.props.recommendDim.cityData.map(city => <Option key={city.value}>{city.text}</Option>);
    }).catch((e)=>{
      console.log(this.state.cityData);
      console.log(e);
    });
  };

  /**
   *  Table编辑涉及的方法
   *  2018-05-29
   */
  isEditing = (record) => {
    return record.key === this.state.editingKey;
  };

  edit(record) {
    const {editingKey} = this.state;
    if(editingKey != ''){
       message.error("你有规则正在编辑状态，请先处理后再编辑！")      
    }else{
      this.setState({ editingKey: record.key, editingRecord: record});
    }
  }

  save2(form, key) {
    const newData = [...this.state.data];
    form.validateFields((error, row) => {
      const index = newData.findIndex(item => key === item.key);
      console.log(row);
        if (index > -1) {
          let item = newData[index];
          newData.splice(index, 1, {
            ...item,
            ...row,
          });
          this.setState({ data: newData, editingKey: '' });
          console.log("caoo------");
          console.log(newData);
        }
    });
  }

  isCurrentChaCodeUnEidt(key,data){
    const index = data.findIndex(item => key === item.key);
    if (index > -1) {
      let item = data[index];
      if(!item.chaCode){
        message.error("请配置合作机构");
        return true;
      }
    }
    return false;
  }

  isCurrentThirdPartCodeUnEidt(key,data){
    const index = data.findIndex(item => key === item.key);
    if (index > -1) {
      let item = data[index];
      if(!item.thirdPartCode){
        message.error("请配置修理厂");
        return true;
      }
    }
    return false;
  }

  isCurrentDimUnEidt(key,data){
    const index = data.findIndex(item => key === item.key);
    if (index > -1) {
      let item = data[index];
      if(!item.pushRepairDimDto){
        message.error("请配置维度信息");
        return true;
      }
    }
    return false;
  }

  save(form, key) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.data];
      if(this.isCurrentThirdPartCodeUnEidt(key,newData)){
        return;
      }
      if(this.isCurrentChaCodeUnEidt(key,newData)){
        return;
      }
      if(this.isCurrentDimUnEidt(key,newData)){
        return;
      }
      if(!this.isExistOrderCode(key,newData)){
        message.error("请选择优先级");
        return;
      }
      if(this.isExistDefaultClumFlag(newData)){
        message.error("只能存在一个默认的修理厂");
        return;
      }
      if(this.isExistSameOrderCode(newData)){
        message.error('不能包含相同的优先级！请修改');
        return;
      }
      const index = newData.findIndex(item => key === item.key);
      if (index > -1) {
        let item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        this.setState({ data: newData, editingKey: ''});
        console.log(newData);
        // 配置好后的rule保存到数据库
        /**格式
         * {
            "data": {
              "pushRepairRuleDto": {
                "key": "value"
              },
              "pushRepairDimDto": [{
                "recommendType": "1",
                "value": "123,123"
              }, {
                "recommendType": "2",
                "value": "123,123"
              }]
            }
          }
         */
        let ruleData = newData[index];
        console.log("配置好后的rule保存到数据库");
        console.log(this.props.recommendForm.addMainRule);
        let toSaveData = {
          pushRepairRuleDto:{
            chaCode: ruleData.chaCode,
            chaOuterNodeId: ruleData.chaOuterNodeId,
            ruleCode: this.props.recommendForm.addMainRule.ruleCode,
            id: ruleData.id,
            defaultClumFlag: ruleData.defaultClumFlag,
            orderCode: ruleData.orderCode,
            thirdPartCode: ruleData.thirdPartCode,
            orderCode: ruleData.orderCode,
          },
          pushRepairDimDto: ruleData.pushRepairDimDto,
        }
        console.log(toSaveData);

        const {dispatch} = this.props;
        // 如果key里包含了new%,说明此rule时新增的,调用addRule，否则使用editRule
        console.log(this.state.editingKey);
        if(this.state.editingKey && ((this.state.editingKey + '').indexOf('new') != -1)){
          dispatch({
            type: 'recommend/addRule',
            data: toSaveData,
          }).then(()=>{
            console.log(toSaveData);
            // 保存后，把ruleId更新到data对应的record中
            const newData = [...this.state.data];
            const {key} = this.state.editingRecord;
            const index = newData.findIndex(item => key === item.key);
            if (index > -1) {
              console.log("保存rule后，把ruleId更新到data对应的record中");
              console.log(this.props.recommend.ruleId);
              const item = newData[index];
              item.key = this.props.recommend.ruleId;
              newData.splice(index, 1, {
                ...this.state.editingRecord,
                ...item,
              });
              console.log(this.props.recommendDim);
              console.log(newData);
              this.setState({ data: newData });
              //this.setState({outerList:recommendDim.outerList});
            }
            message.success("新增规则成功");
          }).catch((e)=>{
            console.log(e);
            message.error(e.message);
          });
        }else{
          dispatch({
            type: 'recommend/editRule',
            data: toSaveData,
          }).then(()=>{
            console.log("toSaveData%%%%%%%%%%");
            console.log(toSaveData);
            message.success("保存规则成功");
          }).catch((e)=>{
            console.log(e);
            message.error(e.message);
          });
        }
      } else {
        newData.push(data);
        this.setState({ data: newData, editingKey: '' });
      }
    });
  }
  
  cancel = (key) => {
    this.setState({ editingKey: '' });
    // 如果是新增的规则未保存就放弃编辑，删除该行
    if((key + '').indexOf('new') != -1){
      this.onDelete(key);
    }
  };

  deleteRecord = (key) => {
    const {dispatch} = this.props;
    const deleteData = {
      id: key,
    }
    dispatch({
      type: 'recommend/deleteRule',
      data: deleteData,
    }).then(()=>{
      const data = [...this.state.data];
      this.setState({ data: data.filter(item => item.key !== key),
                      editingKey: '' ,
                    });
      message.success('删除成功');
    }).catch((e)=>{
      console.log(e);
      message.error(e.message);
    });
  }

  onDelete = (key) => {
    const data = [...this.state.data];
    this.setState({ data: data.filter(item => item.key !== key) });
  }

  parentClmMethods = {};

  render() {
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };
    console.log("render....");
    console.log(this.state.data);

    const {dimVisible, clmVisible, editingKey} = this.state;

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    const parentDimMethods = {
      handleDimVisible: this.handleDimVisible,
      handleDimDataChange: this.handleDimDataChange,
      parentProps: this.props,
      parentState: this.state,
    };
    
    this.parentClmMethods = {
      handleClmVisible: this.handleClmVisible,
      handleProvinceChange: this.handleProvinceChange,
      handleClmSelected: this.handleClmSelected,
      parentProps: this.props,
    };


    return (
      <PageHeaderLayout title="编辑修理厂规则">
        <Card bordered={false}>
          <div className={styles.positionList}>
            <div className={styles.positionListForm}>{this.renderForm()}</div>
            <br/>
            <div className={styles.positionListOperator}>
              <Button icon="plus" type="primary" onClick={this.handleAdd}>
                新建修理厂规则
              </Button>
            </div>
            <br/>
            <Table
              components={components}
              bordered
              dataSource={this.state.data}
              columns={columns}
              rowClassName="editable-row"
              scroll={{ x: 1750 }}
              pagination={false}
            />
          </div>
        </Card>
        <CreateDimForm {...parentDimMethods} dimVisible={dimVisible} editingRecord={this.state.editingRecord}/>
        <CreateClmForm {...this.parentClmMethods} clmVisible={clmVisible}/>
      </PageHeaderLayout>
    );
  }
}

