import React, { PureComponent, Fragment } from 'react';
import { Table,Popconfirm } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';

import EmpTable from '../../components/EmpTable';
import StandardTable from '../../components/StandardTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './Add.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const approveStatusMap = ['待审批', '审批通过', '审批不通过', '暂存'];
const statusMap = ['生效', '待生效'];

const CreateDimForm = Form.create()(props => {
  const {dimVisible, handleDimVisible, form, parentProps} = props;
  const {recommend, recommendForm, dispatch} = parentProps;
  const {cityData} = recommend;
  let cityOptions = [];
  
  cityOptions = cityData.map(city => <Option key={city.value}>{city.text}</Option>);
  console.log("-------");
  console.log(parentProps);
  console.log(cityOptions);

  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      console.log(fieldsValue);
      handleAdd(fieldsValue);
    });
  };

  const handleCarTypeChange = (value, selectedOptions) => {
    console.log(value, selectedOptions);
  };

  // function handleProvinceChange (value) {
  //   console.log("handleProvinceChange called..");
  //   console.log(value);
  //   const { dispatch } = this.props;
  
  //   let assembleCityDate = {
  //     codeCode: value,
  //   };
  //   dispatch({
  //     type: 'recommend/searchCityByPrveData',
  //     data: assembleCityDate,
  //   }).then(()=>{
  //     console.log(this.props.recommend);
  //     cityOptions = cityData.map(city => <Option key={city.value}>{city.text}</Option>);
  //   }).catch((e)=>{
  //     console.log(e);
  //   });
  // }

  // 车辆大类
  const carClassTypeOptions = [];
  carClassTypeOptions.push(<Option key="1">1-标的车</Option>);
  carClassTypeOptions.push(<Option key="2">2-三者车</Option>);
  carClassTypeOptions.push(<Option key="3">3-异地车</Option>);

  let currentDate = Date.parse(new Date());
  return (
    <Modal
      title="维度设置"
      visible={dimVisible}
      onOk={okHandle}
      onCancel={() => handleDimVisible(false)}
    >
        <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
          <Col md={20} sm={20}>
            <FormItem label="车辆大类" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}> 
              {form.getFieldDecorator('carType')
                (<Select style={{ width: 180 }} mode="multiple" onChange={form.handleCarTypeChange}>
                  {carClassTypeOptions}
                </Select>)
              }
            </FormItem>
          </Col>
          <Col md={20} sm={20}>
            <FormItem label="出险区域" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}> 
              {form.getFieldDecorator('areaType', {initialValue: recommendForm.occurCityCode})
                (<Select style={{ width: 180 }} mode="multiple" onChange={form.handleCarTypeChange}>
                  {cityOptions}
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
    </Modal>
  );
});

const EditableCell = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
      : value
    }
  </div>
);

@connect(({recommend, recommendForm, loading}) => ({
  recommend,
  recommendForm,
  loading: loading.models.recommend,
}))

@Form.create()
export default class Add extends PureComponent {
  state = {
    dataSource: [],
    cities:[],
    modalVisible: false,
    selectedRows: [],
    formValues: {},
    recommendForm: {},
    dimVisible: false,
    cityData: {},
  };
  

  componentDidMount() {
    console.log('componentDidMount called..');
    const {dispatch,positions,dataSource} = this.props;

    console.log(this.state);
    console.log(this.props.recommend);
    // todo:出险区域方法,应该在dimform初始化的时候做
    let assembleCityDate = {
      codeCode: '440000',
    };
    dispatch({
      type: 'recommend/searchCityByPrveData',
      data: assembleCityDate,
    }).then(()=>{
      console.log("cityOptions....>>>>>>>>>>>>");
      console.log(cityOptions);
    }).catch((e)=>{
      console.log(e);
    });
    
    // dispatch({
    //   type: 'recommend/fetchList',
    // }).then(()=>{
    //   this.setState({dataSource:positions.data});
    //   console.log(this.state);
    // }).catch((e)=>{
    //   console.log(e);
    // });
  }

  renderColumns(text, record, column) {
    return (
      <EditableCell
        editable={record.editable}
        value={text}
        style={{ width: 15 }} 
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  edit(record,dataSource) { 
    console.log('edit called...');
    console.log(this.state);
    const {key} = record;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    console.log(target);
    if (target) {
      record.editable = true;
      this.setState({ data: newData });
    }
  }

  save(record) {
    const {key} = record;
    console.log('save called...');
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    const { dispatch } = this.props;
    if (target) {
      delete record.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
      console.log('record is..');
      console.log(record);
      let updatePosition = {
        id: record.id,
        positionName: record.positionName,
      };
      dispatch({
        type: 'positions/editPosition',
        data: updatePosition,
      }).catch((e)=>{
        console.log(e);
      });
    }
  }

  cancel(record) {
    console.log('cancel called...');
    const {key} = record;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    if (target) {
      console.log(this.cacheData);
      //Object.assign(target, this.cacheData.filter(item => key === record.key)[0]);
      delete record.editable;
      this.setState({ data: newData });
    }
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      //const dataSource = this.state.dataSource;
      // const target = dataSource.find(item => item.key === key);
      // if (target) {
      //   target[dataIndex] = value;
      //   this.setState({ dataSource });
      //   alert(this.state.dataSource[0].name);
      // }
    };
  }

  handleChange(value, key, column) {
    const newData = [...this.state.dataSource];
    console.log('handleChange called');
    console.log(newData);
    const target = newData.filter(item => key === item.key)[0];
    console.log(target);
    console.log(column);
    if (target) {
      target[column] = value;
      this.setState({ dataSource: newData });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'rule/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  };


  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            no: selectedRows.map(row => row.no).join(','),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'rule/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };


  handleAdd = () => {
    console.log("add.js handleAdd called..");
    const { dataSource } = this.state;
    console.log(dataSource);
    const newData = {
      //key: count,
      orderCode: `Edward King`,
      age: 32,

    };
    if(dataSource){
      this.setState({
        dataSource: [...dataSource, newData],
      });
    }   
  }

  expandedRowRender = () => {
  return (<p>展示</p>);
  }

  handleExpandChange = (enable) => {
    this.setState({ expandedRowRender: enable ? expandedRowRender : undefined });
  }

  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    let currentValue = '';
    let timeout = 2;

    const handleChangeSelection = (value) => {
      console.log(value);
      this.setState({ value });
      fetch(value, data => this.setState({ data }));
    }

    const handleChangeStatusSelection = (value) => {
      console.log(value);
      this.setState({ value });
    }
   
    function onChange(value, selectedOptions) {
      console.log(value, selectedOptions);
    }

    function fetch(value, callback) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      currentValue = value;
    
      function fake() {
        const str = querystring.encode({
          code: 'utf-8',
          q: value,
        });
        jsonp(`https://suggest.taobao.com/sug?${str}`)
          .then(response => response.json())
          .then((d) => {
            if (currentValue === value) {
              const result = d.result;
              const data = [];
              result.forEach((r) => {
                data.push({
                  value: r[0],
                  text: r[0],
                });
              });
              callback(data);
            }
          });
      }
        timeout = setTimeout(fake, 300);
    }

    const handleProvinceChange = (value) => {
      console.log(value);
      this.setState({
        cities: cityData[value],
        secondCity: cityData[value][0],
      });
    }
  
    const onSecondCityChange = (value) => {
      console.log(value);
      this.setState({
        secondCity: value,
      });
    }


    const provinceData = [];
    const cityData = {
      Zhejiang: ['Hangzhou', 'Ningbo', 'Wenzhou'],
      Jiangsu: ['Nanjing', 'Suzhou', 'Zhenjiang'],
    };

    const provinceOptions = provinceData.map(province => <Option key={province}>{province}</Option>);
    const cityOptions = this.state.cities.map(city => <Option key={city}>{city}</Option>);
    const {addMainRule} = this.props.recommendForm;
    console.log(addMainRule);
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="出险省"> 
              {getFieldDecorator('occurCityCode',{initialValue: addMainRule.occurCityCode})
                (<Select style={{ width: 90 }} onChange={handleProvinceChange}>
                  {provinceOptions}
                </Select>)
              }
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="出险市">
              {getFieldDecorator('occurDistrictCode',{initialValue: addMainRule.occurDistrictCode})
                (<Select  style={{ width: 90 }} onChange={this.onSecondCityChange}>
                 {cityOptions}
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="规则编码">
              {getFieldDecorator('groupName',{initialValue: addMainRule.groupName})(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="规则名称">
              {getFieldDecorator('ruleName',{initialValue: addMainRule.ruleName})(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="生效时间">
              {getFieldDecorator('bgnTime',{initialValue: moment(addMainRule.bgnTime)})(<DatePicker onChange={onChange} />)}
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="失效时间">
              {getFieldDecorator('endTime',{initialValue: moment(addMainRule.endTime)})(<DatePicker onChange={onChange} />)}
            </FormItem>
          </Col>
        </Row>   
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="规则操作时间">
              {getFieldDecorator('updatedTime',{initialValue: moment(addMainRule.updatedTime)})(<DatePicker onChange={onChange} />)}
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="规则操作人">
              {getFieldDecorator('updatedUser',{initialValue: addMainRule.updatedUser})(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="是否有效">     
              {getFieldDecorator('status', {initialValue: addMainRule.status})
                (<Select>
                  <Option value="1">有效</Option>
                  <Option value="0">失效</Option>
                </Select>)
              }
            </FormItem>  
          </Col>
          <Col md={10} sm={10}>
            <FormItem label="审批状态">
              {getFieldDecorator('aprroveStatus', {initialValue: addMainRule.aprroveStatus})
                (<Select disabled style={{ width: 120 }} onChange={handleChangeStatusSelection}>
                  <Option value="0">待审批</Option>
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderSimpleForm();
  }


  /*
  * DIM Form(维度设置弹出框)
  * 维度设置弹出框 
  */
 // -----start-----
  handleDimVisible = flag => {
    console.log("list handleDimVisible called");
    this.setState({
      dimVisible: !!flag,
    });
    console.log(this.state.dimVisible);
  };
 // -----end-----



  render() {
    console.log('render called..');
    const {loading, recommend} = this.props;
    let { selectedRows, dimVisible, dataSource} = this.state;
    // dataSource = recommend.data.list;
    //this.setState({dataSource:dataSource});
    //this.cacheData = dataSource.map(item => ({ ...item }));
    // this.state.dataSource = dataSource;
    console.log(">>>>>>>****");
    console.log(this.state);
    const columns = [
      {
        title: '优先级',
        dataIndex: 'orderCode',
        editable:false,
        width: '20%',
        render: (text, record) => this.renderColumns(text, record, 'orderCode'),
      },
      {
        title: '合作机构代码',
        dataIndex: 'chaCode',
        width: '7',
        editable:false,
      },
      {
        title: '合作机构名称',
        dataIndex: 'chaName',
        width: '7',
        editable:false,
      },
      {
        title: '网点代码',
        dataIndex: 'ruleCode',
        width: '7',
        editable:true,
      },
      {
        title: '网点名称',
        dataIndex: 'ruleName',
        width: '7',
        editable:false,
      },
      {
        title: '修理厂代码',
        dataIndex: 'clumCode',
        width: '7',
        editable:false,    
      },
      {
        title: '修理厂名称',     
        dataIndex: 'clumName',
        width: '7',
        editable:false,
        render(val){
          return <div> {moment(val).format("YYYY-MM-DD HH:mm:ss")} </div>
        },
      },
      {
        title: '修理厂组织机构编码',
        dataIndex: 'clumOrgCode',
        width: '7',
        editable:false,
      },
      {
        title: '修理厂联系人',
        dataIndex: 'clumContactPerson',
        width: '7',
        editable:false,
        filters: [
          {
            text: approveStatusMap[0],
            value: 0,
          },
          {
            text: approveStatusMap[1],
            value: 1,
          },
          {
            text: approveStatusMap[2],
            value: 2,
          },
          {
            text: approveStatusMap[3],
            value: 3,
          },
        ],
        onFilter: (value, record) => record.status.toString() === value,
        render(val) {
          return <Badge status={approveStatusMap[val]} text={approveStatusMap[val]} />;
        },
      },
      {
        title: '修理厂联系电话',
        dataIndex: 'clumContactTel',
        width: '7',
        editable:false,
        filters: [
          {
            text: statusMap[0],
            value: 0,
          },
          {
            text: statusMap[1],
            value: 1,
          },
        ],
        onFilter: (value, record) => record.status.toString() === value,
        render(val) {
          return <Badge status={statusMap[val]} text={statusMap[val]} />;
        },
      },
      {
        title: '是否默认修理厂',
        dataIndex: 'clumDefault',
        width: '7',
        editable:false,
        render(val){
          return <div> {moment(val).format("YYYY-MM-DD HH:mm:ss")} </div>
        },
      },
      {
        title: '优惠活动说明',
        dataIndex: 'clumBenefit',
        width: '7',
        editable:false,
        render(val){
          return <div> {moment(val).format("YYYY-MM-DD HH:mm:ss")} </div>
        },
      },
      {
        title: '维度设置',
        width: '25',
        render: (text, record) => {
          const { editable } = record;
          return (
            <Fragment>
                {
                    <span>
                      <a onClick={() => this.handleDimVisible(record)}>纬度设置</a>
                    </span>
                }
            </Fragment>
          );
        },
      },
      {
        title: '操作',
        width: '25',
        render: (text, record) => {
          const { editable } = record;
          return (
            <Fragment>
                {
                  editable ?
                    <span>
                      <a onClick={() => this.save(record)}>保存</a>
                      <Popconfirm title="确定要放弃编辑吗?" onConfirm={() => this.cancel(record)}>
                        <a> 取消</a>
                      </Popconfirm>
                    </span>
                    : <a onClick={() => this.edit(record)}> 编辑</a>
                }
            </Fragment>
          );
        },
      },
    ];

    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentDimMethods = {
      handleDimVisible: this.handleDimVisible,
      parentProps: this.props,
    };

    return (
      <PageHeaderLayout title="新增修理厂规则">
        <Card bordered={false}>
          <div className={styles.positionList}>
            <div className={styles.positionListForm}>{this.renderForm()}</div>
            <br/>
            <div className={styles.positionListOperator}>
              <Button icon="plus" type="primary" onClick={this.handleAdd}>
                新建修理厂规则
              </Button>
            </div>
            <br/>
            <Table
             selectedRows={selectedRows}
             loading={loading}
             dataSource={dataSource}
             state={dataSource}
             columns={columns}
             onSelectRow={this.handleSelectRows}
             rankVisible={this.rankVisible}
             onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
        <CreateDimForm {...parentDimMethods} dimVisible={dimVisible} />
      </PageHeaderLayout>
    );
  }
}
