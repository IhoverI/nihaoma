import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import moment from 'moment';
import {PermissionWrapper,PermissinSelectOption} from 'snk-sso-um';
import jsonp from 'fetch-jsonp';
import querystring from 'querystring';

import {
  Table, 
  Popconfirm, 
  Cascader,
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';

import EmpTable from '../../components/EmpTable';
import EditableCell from '../../components/EmpTable/EditableCell';
import StandardTable from '../../components/StandardTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './Search.less';
import { toASCII } from 'punycode';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const approveStatusMap = ['待审批', '审批通过', '审批不通过', '暂存'];
const statusMap = ['无效', '有效'];

/*
 * 新增规则弹出框
*/
const CreateAddForm = Form.create()(props => {

  const state = {
    UsersData: [],
    value: '',
    cities:[],
    secondCity:[],
    provinceData: [],
    cityData: [],
    recommend: {},
    recommendForm: {},
  }
 
  const { addVisible, form, handleAdd, handleAddVisible, handleProvinceChange, parentProps} = props;
  let {provinceData, cityData} = parentProps.recommend;
  let {addMainRule} = parentProps.recommendForm;

  const provinceOptions = provinceData.map(province => <Option key={province.value}>{province.text}</Option>);
  let cityOptions = cityData.map(city => <Option key={city.value}>{city.text}</Option>);
  
  // console.log(provinceOptions);
  // console.log(cityOptions);

  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      console.log(fieldsValue);
      handleAdd(fieldsValue);
    });
  };

  function onChange(value, selectedOptions) {
    console.log(value, selectedOptions);
  }

  let currentDate = Date.parse(new Date());
  return (
    <Modal
      title="新建推修规则"
      visible={addVisible}
      onOk={okHandle}
      onCancel={() => handleAddVisible(false)}
    >
        <Row gutter={{ md: 8, lg: 12, xl: 48 }}>
          <Col md={10} sm={10}>
            <FormItem label="出险省"> 
              {form.getFieldDecorator('occurProvinceCode', 
                {
                 rules: [{ required: true, message: '出险省不能为空' }],
                })
                (<Select style={{ width: 180 }} onChange={handleProvinceChange} >
                  {provinceOptions}
                </Select>)
              }
            </FormItem>
          </Col>
          <Col md={10} sm={10}>
            <FormItem label="出险市">
              {form.getFieldDecorator('occurCityCode',
               {
                rules: [{ required: true, message: '出险市不能为空' }],
               })
                (<Select  style={{ width: 180 }}>
                 {cityOptions}
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 12, xl: 48 }}>
          <Col md={10} sm={10}>
            <FormItem label="规则编码">
              {form.getFieldDecorator('ruleCode')(<Input disabled placeholder="自动生成" />)}
            </FormItem>
          </Col>
          <Col md={10} sm={10}>
            <FormItem label="规则名称">
              {form.getFieldDecorator('ruleName',
               {
                rules: [{ required: true, message: '规则名称不能为空' }],
               })
              (<Input  placeholder="请输入规则名称" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={10} sm={10}>
            <FormItem label="规则设置操作人">
              {form.getFieldDecorator('updatedUser')(<Input disabled placeholder="admin" />)}
            </FormItem>
          </Col>
          <Col md={10} sm={10}>
            <FormItem label="规则设置操作时间">
              {form.getFieldDecorator('updatedTime', {initialValue: moment(currentDate).format('YYYY-MM-DD HH:mm:ss')} )(<Input disabled placeholder="当前时间" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={10} sm={10}>
            <FormItem label="是否有效">     
              {form.getFieldDecorator('status', {initialValue:"1"})
              (<Select>
                <Option value="1">有效</Option>
                <Option value="0">失效</Option>
              </Select>)
              }
            </FormItem>  
          </Col>
          <Col md={10} sm={10}>
            <FormItem label="审批状态">
              {form.getFieldDecorator('approveStatus', {initialValue:"0"})(
              <Select disabled style={{ width: 100 }} onChange={form.handleChangeStatusSelection}>
                <Option value="0">待审批</Option>
              </Select>)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={10} sm={10}>
            <FormItem label="生效时间">
              {form.getFieldDecorator('bgnTime',
              {
               rules: [{ required: true, message: '生效时间不能为空' }],
              })(<DatePicker />)}
            </FormItem>
          </Col>
          <Col md={10} sm={10}>
            <FormItem label="失效时间">
              {form.getFieldDecorator('endTime',
              {
                rules: [{ required: true, message: '失效时间不能为空' }],
              })(<DatePicker onChange={onChange} />)}
            </FormItem>
          </Col>
        </Row>   
    </Modal>
  );
});


@connect(({recommend, recommendForm, recommendUser, umssouserinfo, loading, positions}) => ({
  recommend,
  recommendForm,
  recommendUser,
  umssouserinfo,
  loading: loading.models.recommend,
}))

@Form.create()
export default class Search extends PureComponent {
  state = {
    dataSource: [],
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    data: [],
    userData: [],
    value: '',
    cities:[],
    secondCity:[],
    provinceData:[],
    cityData:[],
    recommend: {},
    recommendForm: {},
    pageNum: 1,
    pageSize: 10,
    deptCode: '',
  };
  

  componentDidMount() {
    
    console.log('componentDidMount called..');
    const { dispatch,recommend,dataSource, umssouserinfo:{currentUser} } = this.props;
    console.log(currentUser.principal.name);

    // 查询登录者的所属机构代码
    const searchDeptData = {
      userCode: currentUser.principal.name,
    }
    dispatch({
      type: 'recommendUser/searchUserDeptCode',
      data: searchDeptData,
    }).then(()=>{
      console.log(this.props.recommendUser.deptCode);
      this.setState({deptCode: this.props.recommendUser.deptCode});
    }).catch((e)=>{
      console.log(e);
    });

    dispatch({
      type: 'recommend/searchProvinceData',
    }).then(()=>{
      console.log(recommend);
      this.setState({provinceData:recommend.provinceData.data});
    }).catch((e)=>{
      console.log(e);
    });
  }


  renderColumns(text, record, column) {
    return (
      <EditableCell2
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  edit(record,dataSource) { 
    console.log('edit called...');
    console.log(this.state);
    const {key} = record;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    console.log(target);
    if (target) {
      record.editable = true;
      this.setState({ data: newData });
    }
  }

  save(record) {
    const {key} = record;
    console.log('save called...');
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    const { dispatch } = this.props;
    if (target) {
      delete record.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
      console.log('record is..');
      console.log(record);
      let updatePosition = {
        id: record.id,
        positionName: record.positionName,
      };
      dispatch({
        type: 'positions/editPosition',
        data: updatePosition,
      }).catch((e)=>{
        console.log(e);
      });
    }
  }

  cancel(record) {
    console.log('cancel called...');
    const {key} = record;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    if (target) {
      console.log(this.cacheData);
      //Object.assign(target, this.cacheData.filter(item => key === record.key)[0]);
      delete record.editable;
      this.setState({ data: newData });
    }
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource = this.state.dataSource;
      // const target = dataSource.find(item => item.key === key);
      // if (target) {
      //   target[dataIndex] = value;
      //   this.setState({ dataSource });
      //   alert(this.state.dataSource[0].name);
      // }
    };
  }

  handleChange(value, key, column) {
    const newData = [...this.state.dataSource];
    console.log('handleChange called');
    console.log(newData);
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ dataSource: newData });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
      console.log("handleStandardTableChange called..");
      console.log(pagination);
      this.setState({
        pageNum: pagination.current,
        pageSize: pagination.pageSize,
      }, ()=> {
        this.handleSearch('');
      });
  };

  handleProvinceChange = (value) => {
    console.log("handleProvinceChange called..");
    console.log(value);
    const { dispatch, recommend } = this.props;
    let assembleCityDate = {
      codeCode: value,
    };
    dispatch({
      type: 'recommend/searchCityByPrveData',
      data: assembleCityDate,
    }).then(()=>{
      console.log(recommend);
      this.setState({
        cities: cityData[value],
        secondCity: cityData[value],
      });
      cityOptions = cityData.map(city => <Option key={city.value}>{city.text}</Option>);

    }).catch((e)=>{
      console.log(this.state.cityData);
      console.log(e);
    });
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
  };


  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            no: selectedRows.map(row => row.no).join(','),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleAddVisible = flag => {
    console.log("list handleAddVisible called");
    this.setState({
      addVisible: !!flag,
    });
    console.log(flag);
    console.log(!!flag);
    console.log(this.state.addVisible);
  };


  handleAdd = fields => {
    console.log('form handleAdd called...');
    console.log(fields);
    const { dispatch, recommend, recommendForm, dataSource } = this.props;
 
    fields.operDeptCode = this.props.recommendUser.deptCode;
    dispatch({
      type: 'recommend/addRecommendMain',
      payload: {
        addMainRule: fields,
      },
    }).then(()=>{
      this.setState({dataSource:recommend.data});
      message.success('添加成功');
      fields.mainId = this.props.recommend.mainId;
      console.log(">>>>>>保存成功->mainId:");
      console.log(this.props.recommend);
      dispatch({
        type: 'recommendForm/submitAddForm',
        data: {
          addMainRule: fields,
        },
      }).then(()=>{
        dispatch(routerRedux.push('/recommend/add'));
      }).catch((e)=>{
        console.log(e);
      });
    }).catch((e)=>{
      message.success(e.message);
    });

    this.setState({
      modalVisible: false,
    });
  };


  expandedRowRender = () => {
  return (<p>展示</p>);
  }

  handleExpandChange = (enable) => {
    this.setState({ expandedRowRender: enable ? expandedRowRender : undefined });
  }


  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  //搜索使用的提交方法
  handleSearch = e => {
    if(e){
      e.preventDefault();
    }
    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
        pageNum: this.state.pageNum,
        pageSize: this.state.pageSize,
      };

      // console.log(">>>>>>>>>>>_____");
      // console.log(this.state.pageNum);
      // console.log(this.state.pageSize);

      this.setState({
        formValues: values,
      });
      
      dispatch({
        type: 'recommend/searchRecommend',
        data: values,
      });
    });
  };

  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    const { recommend, dispatch } = this.props;
    const { provinceData, cityData, users } = recommend;
    const provinceOptions = provinceData.map(province => <Option key={province.value}>{province.text}</Option>);
    let cityOptions = cityData.map(city => <Option key={city.value}>{city.text}</Option>);
    let currentValue = '';
    let timeout = 2;
    
    const userOptions = this.state.userData.map(d => <Option key={d.text}>{d.text}</Option>);
    const userOptions2 = this.state.userData.map(d => <Option key={d.text}>{d.text}</Option>);
    console.log(userOptions);
    
    const handleChangeSelection = (value) => {
      console.log("handleChangeSelection called...");
      console.log(value);
      this.setState({ value });
      //let reg = "/[^/u4E00-/u9FA5]/g";
      let reg = new RegExp("[\\u4E00-\\u9FFF]+","g");
      if(reg.test(value)){
        //fetch(value, data => this.setState({ data }));
        currentValue = value;
        let userData = {
          userName: value,
        };
        const {dispatch, recommend: { users }} = _this.props;
        console.log(users);
        dispatch({
          type: 'recommend/findUsers',
          data: userData,
        }).then(()=>{
          if (currentValue === value) {
            console.log(this.props.recommend.users);
            const users = this.props.recommend.users;
            this.setState({userData: users});
            // callback(users);
          }
        }).catch((e)=>{
          console.log(e);
        });
      }else{
        
      }
    }

    const handleChangeSelectionApprove = (value) => {
      console.log("handleChangeSelection called...");
      console.log(value);
      this.setState({ value });
      //let reg = "/[^/u4E00-/u9FA5]/g";
      let reg = new RegExp("[\\u4E00-\\u9FFF]+","g");
      if(reg.test(value)){
        //fetch(value, data => this.setState({ data }));
        currentValue = value;
        let userData = {
          userName: value,
        };
        const {dispatch, recommend: { users }} = _this.props;
        console.log(users);
        dispatch({
          type: 'recommend/findUsers',
          data: userData,
        }).then(()=>{
          if (currentValue === value) {
            console.log(this.props.recommend.users);
            const users = this.props.recommend.users;
            this.setState({userData: users});
            // callback(users);
          }
        }).catch((e)=>{
          console.log(e);
        });
      }else{
        
      }
    }

    const handleChangeStatusSelection = (value) => {
      console.log(value);
      this.setState({ value });
    }
   
    function onChange(value, selectedOptions) {
      console.log(value, selectedOptions);
    }
    const _this = this;
    // fetch = () => {
    //   this
    // }
    const fetch = (value, callback) => {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      currentValue = value;

      let userData = {
        userName: value,
      };
      const {dispatch} = _this.props;
      dispatch({
        type: 'recommend/findUsers',
        data: userData,
      }).then(()=>{
        if (currentValue === value) {
          const result = recommend;
          console.log(users);
          const data = [];
          for(user in users){

          }
          //callback(data);
        }
      }).catch((e)=>{
        console.log(e);
      });
    }

    const onSecondCityChange = (value) => {
      console.log(value);
      this.setState({
        secondCity: value,
      });
    }
    
    // const provinceData = ['Zhejiang', 'Jiangsu'];
    // const cityData = {
    //   Zhejiang: ['Hangzhou', 'Ningbo', 'Wenzhou'],
    //   Jiangsu: ['Nanjing', 'Suzhou', 'Zhenjiang'],
    // };
  
    let currentDate = Date.parse(new Date());
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="操作机构">
              {getFieldDecorator('operDeptCode', {initialValue: this.state.deptCode})
              (<Input disabled placeholder="自动带出" />)
            }
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="规则编码">
              {getFieldDecorator('ruleCode')(<Input  placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={10}>
            <FormItem label="规则设置操作时间">
              {getFieldDecorator('updatedTime')(<DatePicker onChange={onChange} format={"YYYY-MM-DD"}/>)}
            </FormItem>
          </Col>
          <Col md={12} sm={10}>
            <FormItem label="规则设置审核时间">
              {getFieldDecorator('approveTime')(<DatePicker onChange={onChange} format={"YYYY-MM-DD"}/>)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="规则设置操作人">
              {getFieldDecorator('updatedUser')
              (<Select
                mode="combobox"
                setFieldsValue={this.state.value}
                placeholder={this.props.placeholder}
                style={this.props.style}
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onChange={handleChangeSelection}
                >
                {userOptions}
              </Select>)
             }
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="规则设置审核人">     
              {getFieldDecorator('approveUser')
              (<Select
                mode="combobox"
                placeholder={this.props.placeholder}
                style={this.props.style}
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onChange={handleChangeSelectionApprove}
                >
                {userOptions2}
              </Select>)
              }
            </FormItem>  
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="审批状态">
              {getFieldDecorator('approveStatus')(
                <Select onChange={handleChangeStatusSelection} allowClear={true}>
                <Option value="0">待审批</Option>
                <Option value="1">审批通过</Option>
                <Option value="2">审批不通过</Option>
                <Option value="3">暂存</Option>
              </Select>)
              }
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="生效时间">
              {getFieldDecorator('bgnTime')(<DatePicker onChange={onChange} />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="出险省"> 
              {getFieldDecorator('occurProvinceCode')
                (<Select onChange={this.handleProvinceChange} allowClear={true}>
                  {provinceOptions}
                </Select>)
              }
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="出险市">
              {getFieldDecorator('occurCityCode')
                (<Select  onChange={this.onSecondCityChange} allowClear={true}>
                 {cityOptions}
                </Select>)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
          <Col md={12} sm={24}>
              <span className={styles.submitButtons}>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleAddVisible}>
                  添加
                </Button>
              </span>
            </Col>
          </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderSimpleForm();
  }

  // 跳转到修改页面
  handleEditMain = (value) => {
    console.log('handleEditMain called..');
    console.log(value);
    const {selectedRows} = this.state;
    console.log(selectedRows);
    if(selectedRows.length > 1){
      message.error("请选择1条规则进行修改");
    }else if (selectedRows.length == 0){
      message.error("请选择需要的修改的规则,再进行修改");
    }else{
        // 开始查询主规则与省市规则
        const {dispatch, recommendForm} = this.props;
        const data = {
          ruleCode: selectedRows[0].ruleCode,
        };
        dispatch({
          type: 'recommendForm/searchRule',
          data: data,
        }).then(()=>{
          let rule = `${selectedRows[0].ruleCode}`;
          if(rule){
            dispatch(routerRedux.push(`/recommend/edit?`));
          }else{
            dispatch(routerRedux.push(`/recommend/edit`));
          }
        }).catch((e)=>{
          console.log(e);
        });
    }
  }

  // 审核选中
  handleApproveMain = (value) => {
    console.log("handleApproveMain called..");
    const {selectedRows} = this.state;
    if(selectedRows.length > 0){
      const {dispatch} = this.props;
      let approveData = [];
      for(let i = 0; i < selectedRows.length; i ++){
        const rowSelected = selectedRows[i];
        console.log(rowSelected);
        approveData.push({ruleCode:rowSelected.ruleCode, approveStatus:'1'});
      };
      console.log(approveData);
      dispatch({
        type: 'recommend/approveMain',
        data: approveData,
      }).then(()=>{
          //dispatch(routerRedux.push('/recommend/edit'));
          message.success("操作成功!");
          this.handleSearch();
      }).catch((e)=>{
        console.log(e);
        message.error(e.message);
        this.handleSearch();
      });
    }else{
      message.error("请选择需要的审核的规则,再进行审核");
    }
  }

  // 失效选中
  handleDisableMain = (value) => {
    console.log(value);
    console.log(value);
    const {selectedRows} = this.state;
    console.log(selectedRows);
    if(selectedRows.length > 0){
      const {dispatch} = this.props;
      let approveData = [];
      for(let i = 0; i < selectedRows.length; i ++){
        const rowSelected = selectedRows[i];
        console.log(rowSelected);
        approveData.push({ruleCode:rowSelected.ruleCode, approveStatus:'0'});
      };
      console.log(approveData);
      dispatch({
        type: 'recommend/approveMain',
        data: approveData,
      }).then(()=>{
          //dispatch(routerRedux.push('/recommend/edit'));
          message.success("操作成功!");
          this.handleSearch();
      }).catch((e)=>{
        console.log(e);
        message.error(e.message);
        this.handleSearch();
      });
    }else{
      message.error("请选择需要的拒绝的规则,再进行拒绝审批");
    }
  }

  render() {
    const {loading, recommend} = this.props;
    let { selectedRows, modalVisible, addVisible, dataSource} = this.state;
   
    //this.setState({dataSource:dataSource});
    //this.cacheData = dataSource.map(item => ({ ...item }));

    const columns = [
      // {
      //   title: '序号',
      //   dataIndex: 'id',
      //   editable:false,
      //   width: '5',
      // },
      {
        title: '操作机构',
        dataIndex: 'operDeptName',
        width: '7',
        editable:false,
      },
      {
        title: '规则名称',
        dataIndex: 'ruleName',
        width: '7',
        editable:false,
      },
      {
        title: '操作时间',
        dataIndex: 'updatedTime',
        width: '7',
        editable:false,
        render(val){
            return <div> {moment(parseInt(val)).format("YYYY-MM-DD HH:mm:ss")} </div>
        },
      },
      {
        title: '操作人',
        dataIndex: 'updatedUser',
        width: '7',
        editable:false,    
      },
      {
        title: '审批时间',
        dataIndex: 'approveTime',
        width: '7',
        editable:false,
        render(val){
          return (
            <div>
              {val == 'null' ? (
                <Fragment>
                <span>
                    (待审核)
                </span>
                </Fragment> 
              ): 
              (<Fragment>
                  <span>
                    {moment(parseInt(val)).format("YYYY-MM-DD HH:mm:ss")}
                  </span>
                </Fragment>
              )}
            </div>)
          // return
          // (<div>
          //   {val == 'null' ? 
          //      (moment(parseInt(val)).format("YYYY-MM-DD HH:mm:ss"))
          //     :
          //      (待审核)
          //   }
          // </div>)
        },
      },
      {
        title: '审批人',
        dataIndex: 'approveUser',
        width: '7',
        editable:false,
      },
      {
        title: '审批状态',
        dataIndex: 'approveStatus',
        width: '7',
        editable:false,
        filters: [
          {
            text: approveStatusMap[0],
            value: 0,
          },
          {
            text: approveStatusMap[1],
            value: 1,
          },
          {
            text: approveStatusMap[2],
            value: 2,
          },
          {
            text: approveStatusMap[3],
            value: 3,
          },
        ],
        onFilter: (value, record) => record.status.toString() === value,
        render(val) {
          return <Badge status={approveStatusMap[val]} text={approveStatusMap[val]} />;
        },
      },
      {
        title: '是否有效',
        dataIndex: 'status',
        width: '7',
        editable:false,
        filters: [
          {
            text: statusMap[0],
            value: 0,
          },
          {
            text: statusMap[1],
            value: 1,
          },
        ],
        onFilter: (value, record) => record.status.toString() === value,
        render(val) {
          return <Badge status={statusMap[val]} text={statusMap[val]} />;
        },
      },
      {
        title: '生效时间',
        dataIndex: 'bgnTime',
        width: '7',
        editable:false,
        render(val){
          return <div> {moment(parseInt(val)).format("YYYY-MM-DD HH:mm:ss")} </div>
        },
      },
      {
        title: '无效时间',
        dataIndex: 'endTime',
        width: '7',
        editable:false,
        render(val){
          return <div> {moment(parseInt(val)).format("YYYY-MM-DD HH:mm:ss")} </div>
        },
      },
         {
        title: '规则编码',
        dataIndex: 'ruleCode',
        width: '7',
        editable:false,
      },
    ];

    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleAddVisible: this.handleAddVisible,
      handleModalVisible: this.handleModalVisible,
      handleProvinceChange: this.handleProvinceChange,
      onSecondCityChange: this.onSecondCityChange,
      parentProps: this.props,
    };

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        this.setState({
          selectedRows: selectedRows,
        });
      },
      getCheckboxProps: record => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    };    

    dataSource = recommend.list;

    let total = 0;
    if(recommend.data){
      total = recommend.data.total;
    }

    return (
      <PageHeaderLayout title="推修规则查询">
        <Card bordered={false}>
          <div className={styles.positionList}>
            <div className={styles.positionListForm}>{this.renderForm()}</div>
            <br/>
            <Table
             selectedRows={selectedRows}
             rowSelection={rowSelection}
             loading={loading}
             dataSource={dataSource}
             state={dataSource}
             columns={columns}
             onSelectRow={this.handleSelectRows}
             addVisible={this.addVisible}
             onChange={this.handleStandardTableChange}
             scroll={{ x: 1750 }}
             pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              ...{
                current: this.state.pageNum,
                pageSize: this.state.pageSize,
                total: total,
              }
            }}
            />
            <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
              <Col md={12} sm={24}>
                  <span className={styles.submitButtons}>
                    
                  <PermissionWrapper permissionKey='修改'>
                    <Button style={{ marginLeft: 8 }} onClick={this.handleEditMain}>
                        修改
                      </Button>
                   </PermissionWrapper>
                   <PermissionWrapper permissionKey='审批'>
                      <Button style={{ marginLeft: 8 }} onClick={this.handleApproveMain}>
                        审批
                      </Button>
                  </PermissionWrapper>
                   {/*<PermissionWrapper permissionKey='审批'>
                    <Button style={{ marginLeft: 8 }} onClick={this.handleDisableMain}>
                        拒绝审批
                    </Button>
                  </PermissionWrapper>*/}
                  </span>
                </Col>
            </Row>
          </div>
        </Card>
        <CreateAddForm {...parentMethods} addVisible={addVisible} />
      </PageHeaderLayout>
    );
  }
}

