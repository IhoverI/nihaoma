import React, { PureComponent, Fragment } from 'react';
import {
 Spin
} from 'snk-web';
import { connect } from 'dva';

import { getRouterData} from '../../common/router';

@connect(({ user }) => ({
  user,
}))
export default class AuthorityPage extends PureComponent {
  state = {
    initState:'loading',
  };

  componentDidMount(){
    this.initUserInfo();
  }

  initUserInfo(){
    this.setState({
      initState:'loading'
    },()=>{
      this.props.dispatch({
        type: 'user/fetchCurrent',
      }).then(()=>{
        this.setState({
          initState:'success'
        });
      }).catch(()=>{
        this.setState({
          initState:'error'
        });
      });
    });
  }
 
  render() {
    if(this.state.initState === 'loading'){
      return <div style={{textAlign:'center',paddingTop:100,color:'#333',fontSize:18}}><Spin/>正在初始化用户信息...</div>;
    } else if(this.state.initState === 'error'){
      return <div style={{textAlign:'center',paddingTop:100,color:'#333',fontSize:18}}>登录失效,<a onClick={()=>{this.initUserInfo();}}>点击重试</a></div>;
    } else if (this.state.initState === 'success'){
      const routerData = getRouterData(this.props.app,this.props.user.serverMenuData);
      const BasicLayout = routerData['/'].component;
      return this.props.renderContent(BasicLayout);
    }
  }
}
