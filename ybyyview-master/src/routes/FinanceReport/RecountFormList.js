import React, { PureComponent } from 'react';
import {
  Modal,
  Form,
  Button,
  Icon,
  Upload,
  message,
} from 'snk-web';
import { formItemLayout, extEnterpCodeOptions } from './ReportConfig';
const FormItem = Form.Item;

@Form.create()
export default class UploadFormList extends PureComponent {
  state={
    fileList: [],
  }
  onHandleUpload=(e)=>{
    //防止链接打开 URL
    if(e)e.preventDefault();
    const { form } = this.props;
    //表单校验
    form.validateFields((err, fieldsValue) => {
        if (err) return;
        // console.log(this.state.fileList);
        if(!this.state.fileList.length){
          message.info('请选择上传的文件');
          return;
        }
        fieldsValue.uploadFile= this.state.fileList[0];
        if(this.props.onConfirm) this.props.onConfirm(fieldsValue);
    });
  }
  onCancel=()=>{
    if(this.props.onCancel) this.props.onCancel();
  }
  uploadParams=()=>{
    return {
      showUploadList:true,
      action: '',
      onRemove: (file) => {
        // console.log('file', file);
        this.setState({
          fileList: [],
        })
      },
      beforeUpload: (file) => {
        this.setState({
          fileList: [file],
        })
        return false;
      },
      fileList: this.state.fileList,
    }
  }
  renderContent=()=>{
    const { getFieldDecorator, getFieldValue } = this.props.form;
    return(
      <Form onSubmit={this.onHandleUpload} layout="inline">
          <FormItem  style={{width: "100%",textAlign: 'center'}}>
            <Upload {...this.uploadParams()}>
              <Button>
                <Icon type="upload" /> 选择文件
              </Button>
            </Upload>
          </FormItem>
          <FormItem  style={{width: "100%", textAlign: 'center'}}>
            <Button
             loading={this.props.loading}
             type="primary"
             htmlType="submit"
            >
              上传
            </Button>
            <Button
              style={{ margin: 12 }}
              onClick={this.onCancel}
            >
              取消
            </Button>
          </FormItem>
      </Form>
    )
  }
  render(){
    const {isVisibleRecountModal} = this.props;
    return(
      <Modal
        title="导入文件"
        visible={isVisibleRecountModal}
        footer={null}
        width="350px"
        closable={false}
      >
        { 
          this.renderContent()
        }
      </Modal>
    )
  }
}