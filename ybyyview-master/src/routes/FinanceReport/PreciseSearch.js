import React from 'react';
import { connect } from 'dva';
import {
  Form,
  Row,
  Col,
  Button,
  Input
} from 'snk-web';
import { formItemLayout } from './ReportConfig';
const FormItem = Form.Item;
@connect(({ loading }) => ({
  loading: loading.effects,
}))
@Form.create()
export default class PreciseSearch extends React.Component { 
    //重置表单
    handleReset = () => {
        this.props.form.resetFields();
    }
    handleSearch = e => {
        //防止链接打开 URL
        if(e)e.preventDefault();
        const { dispatch, form } = this.props;
        //表单校验
        form.validateFields((err, fieldsValue) => {
						if (err) return;
						const params = fieldsValue;
						if(this.props.onSearch) this.props.onSearch(params);
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
          <Form onSubmit={this.handleSearch}>
            <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
							<Col md={10} sm={24}>
								<FormItem
										label="保单号"
										{...formItemLayout}
										style={{marginBottom:'0'}}
								>
										{
											getFieldDecorator('cPlyNo',{
												rules: [{
														required: true, message: '请输入正确的保单号!',
												}],
											})(<Input placeholder="请输入" />)
										}
								</FormItem>
							</Col>
                <Col md={10} sm={24} style={{textAlign : 'center'}}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={this.props.loading['financeReport/queryFinanceList']}
                    >
                        搜索
                    </Button>
                    <Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
                        重置
                    </Button>
                </Col>
            </Row> 
        </Form>
        );
    }
}