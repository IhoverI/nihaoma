//表单布局
export const formItemLayout = {
    labelCol: {
      xs: { span: 12 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 12 },
      sm: { span: 16 },
    },
};
// 保单年号选项
export const getPyearListOptions = () => {
    const curYear = new Date().getFullYear();
    const result = [];
    for(let i= curYear;i>=2005; i--){
      result.push(i);
    }
    return result;
}
// 渠道名称选项
export const extEnterpCodeOptions =[
  {name: '招商银行',code: '95555'},
  {name: '民生银行', code: '95568'},
  {name: '建设银行', code: '95533'},
  {name: '盛大', code: '80001'},
  {name: '江苏邮政', code: '95580'},
  {name: '深圳发展银行', code: '95501'},
  {name: '深圳陌call', code: '10010'},
  {name: '华安保险分公司推广', code: '95556'},
  {name: '微信出单', code: '11002'},
  {name: '汇金保联', code: '11001'},
  {name: '中信银行', code: '95558'}, 
  {name: '转银联', code: '95516'},
  {name: '转快钱', code: '90001'},
  {name: '南昌电话中心',code:'11004'},
  {name: '成都自建', code: '11003'},
];
// 自动对账--table 头
export const getFinanceReportColumns =()=>{
    return [
      {
        title: '序号',
        dataIndex: 'orderId',
        width: 40,
        align: 'center',
        render: (text, record, index) => index+1,
      },
      {
        title: '业务渠道代码',
        dataIndex: 'oldExtEnterpCode',
        width: 100,
        align: 'center',
      },
      {
        title: '原订单号',
        dataIndex: 'transationCode',
        width: 120,
        align: 'center',
      },
      {
        title: '实际请款日期',
        dataIndex: 'payDateFirst',
        width: 100,
        align: 'center',
      },
      {
        title: '初次请款日期',
        dataIndex: 'payDateLast',
        width: 100,
        align: 'center',
      },
      {
        title: '请款金额',
        dataIndex: 'dxsPrm',
        width: 80,
        align: 'center',
      },
      {
        title: '请款类型',
        dataIndex: 'payType',
        width: 80,
        align: 'center',
      },
      {
        title: '银行手续费',
        dataIndex: 'bankCmmFees',
        width: 100,
        align: 'center',
      },
      {
        title: '协议佣金率',
        dataIndex: 'bankCmmRate',
        width: 80,
        align: 'center',
      },
      {
        title: '电销手续费',
        dataIndex: 'dxsCmmFees',
        width: 80,
        align: 'center',
      },
      {
        title: '增调佣金率',
        dataIndex: 'dxsCmmRate',
        width: 80,
        align: 'center',
      },
      {
        title: '保单号',
        dataIndex: 'cPlyNo',
        width: 120,
        align: 'center',
      },
      {
        title: '请款期次',
        dataIndex: 'period',
        width: 50,
        align: 'center',
      },
      {
        title: '产品代码', 
        dataIndex: 'prodNo',
        width: 80,
        align: 'center',
      },
      {
        title: '扣款卡号', 
        dataIndex: 'cardId',
        width: 80,
        align: 'center',
      },
       {
        title: '银行手续费对账状态',
        dataIndex: 'bankCmmCheckFlag',
        width: 80,
        align: 'center',
      },
      {
        title: '银行保费对账状态',
        dataIndex: 'bankPrmCheckFlag',
        width: 80,
        align: 'center',
      },
      {
        title: '保费实收状态',
        dataIndex: 'prmPayInFlag',
        width: 80,
        align: 'center',
      },
      {
        title: '手续费实付状态',
        dataIndex: 'cmmPayOutFlag',
        width: 80,
        align: 'center',
      },
      {
        title: '保费收付登记号',
        dataIndex: 'policyPaymentNo',
        width: 120,
        align: 'center',
      },
      {
        title: '手续费收付登记号',
        dataIndex: 'commissionPaymentNo',
        width: 120,
        align: 'center',
      },
      {
        title: '银保总批次号',
        dataIndex: 'batchNo',
        width: 120,
        align: 'center',
      },
      {
        title: '银保分批次号',
        dataIndex: 'finFileBatchNo',
        width: 100,
        align: 'center',
      },
    ];
}