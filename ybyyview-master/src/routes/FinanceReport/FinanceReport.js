import React from 'react';
import { connect } from 'dva';
import { Form, Card, Tabs, Table, Divider, Button, message } from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import PreciseSearch from './PreciseSearch';
import BlurSearch from './BlurSearch';
import UploadFormList from './UploadFormList';
import DownLoadFormList from './DownLoadFormList';
import RecountFormList from './RecountFormList';
import ImportOfflineInfo from './ImportOfflineInfo';
import { getFinanceReportColumns, } from './ReportConfig';

const FormItem = Form.Item;
const { TabPane } = Tabs;
@connect(({financeReport,loading}) => ({
	financeReport,
	loading: loading.effects,
}))
@Form.create()
export default class FinanceReport extends React.Component {
	state={
		isVisibleUpModal: false,
		isVisibleDownModal:false,
		isVisibleRecountModal: false,
		isCanMakeSure: false,
		isOfflineInfo: false,
	}

	//发送请求--列表
	 onFetchFinanceList=(params)=>{
			this.fieldsValue = params;
			this.handleTablePage();
	 }
	 handleTablePage=(pageSizeOptions = {})=>{
		const { current:pageNum = 1, pageSize = 10 } = pageSizeOptions;
		const formParams = this.fieldsValue;
		this.props.dispatch({
			type:'financeReport/queryFinanceList',
			payload:{pageNum, pageSize, ...formParams},
		}).then((code)=>{
			this.onCanMakeCompleted(code, formParams);
		});
	}
	// 判断是否可以核对
	onCanMakeCompleted=(code, formParams = {})=>{
		// console.log(!!code);
		if(code !== '0'){ 
			this.setState({isCanMakeSure: false});
			return;
		}
		const {prmPayInFlag, cmmPayOutFlag} = formParams;
		if((prmPayInFlag === '2' || prmPayInFlag === '3') && (cmmPayOutFlag === '2' || cmmPayOutFlag === '3')  ){
			this.setState({isCanMakeSure: true});
			return;
		}
		this.setState({isCanMakeSure: false});
	}

	//核对完成
	onMakeCompleted=()=>{
		this.props.dispatch({
      type: 'financeReport/checkConsistency',
      payload: this.fieldsValue,
    });
	}
	// 导入excel
	onHandleUpLoadButton=()=>{
		this.setState({
			isVisibleUpModal: true,
		})
	}
	onUploadFormList=(params)=>{
		this.props.dispatch({
			type: 'financeReport/uploadFinanceFile',
			payload: params,
		}).then(()=>{
			this.setState({
				isVisibleUpModal: false,
			})
		})
	}
	onUploadFormCancel=()=>{
		this.setState({
			isVisibleUpModal: false,
		})
	}

	// 下载excel
	onHandleDownLoadButton=()=>{
		this.setState({
			isVisibleDownModal: true,
		})
	}
	onDownloadFormList=(params)=>{
		// console.log('params',params);
		const searchParams = this.fieldsValue;
		if(!searchParams){
			message.info('请先查询，然后导出');
			return;
		}
		this.props.dispatch({
			type: 'financeReport/exportFinanceFile',
			payload: { ...params, ...searchParams},
		}).then(()=>{
			this.setState({
				isVisibleDownModal: false,
			})
		})
	}
	onDownloadFormCancel=()=>{
		this.setState({
			isVisibleDownModal: false,
		})
	}

	// 重新计算
	onHandleRecountButton=()=>{
		this.setState({
			isVisibleRecountModal: true,
		})
	}

	// 线下退费清单导入
	onHandleOfflineInfo=()=>{
		this.setState({
			isOfflineInfo: true,
		})
	}

	onRecountFormList=(params)=>{
		// console.log('params',params);
		this.props.dispatch({
			type: 'financeReport/recountFileList',
			payload: params,
		}).then(()=>{
			this.setState({
				isVisibleRecountModal: false,
			})
		})	
	}

	importOfflineInfo = (params) => {
		this.props.dispatch({
			type: 'financeReport/importOfflineInfo',
			payload: params,
		}).then(()=>{
			this.setState({
				isOfflineInfo: false,
			})
		})
	}

	importOfflineInfoCancel = () => {
		this.setState({
			isOfflineInfo: false,
		})
	}

	onRecountFormCancel=()=>{
		this.setState({
			isVisibleRecountModal: false,
		})
	}

  renderAdvancedForm = ()=>{
        return (
					<Tabs style={{marginBottom: '20px'}}>
						<TabPane tab="模糊查询" key="1">
								<BlurSearch onSearch={this.onFetchFinanceList}/>
						</TabPane>
						<TabPane tab="精确查询" key="2">
								<PreciseSearch onSearch={this.onFetchFinanceList}/>
						</TabPane>
					</Tabs>
          );
		}
		renderTableTitle=()=>{
			return(
				<div>
					<Button
						 type='primary'
						 style={{marginRight:10}} 
						 onClick={this.onMakeCompleted}
						 loading={this.props.loading['financeReport/checkConsistency']}
						 disabled={!this.state.isCanMakeSure}
					>
             核对完成
          </Button>
					<Button
						 icon='upload' 
						 style={{marginRight:10}} 
						 onClick={this.onHandleUpLoadButton}
					>
            导入Excel清单
          </Button>
					<Button
						 icon='download' 
						 style={{marginRight:10}} 
						 onClick={this.onHandleDownLoadButton}
					>
            导出Excel清单
          </Button>
					<Button
						 icon='upload' 
						 style={{marginRight:10}} 
						 onClick={this.onHandleRecountButton}
					>
             重新计算
          </Button>
					<Button
						 icon='upload' 
						 style={{marginRight:10}} 
						 onClick={this.onHandleOfflineInfo}
					>
             线下退费清单导入
          </Button>
				</div>
			)
		}
    render() {
				const { financeInfoList = [], financeInfoCount } = this.props.financeReport || {};
				// console.log('props',this.props);
        return (
          <PageHeaderLayout>
              <Card bordered={false}>
									{this.renderAdvancedForm()}
									<Table
											bordered
											columns={getFinanceReportColumns()}
											dataSource={financeInfoList}
											loading={this.props.loading['financeReport/queryFinanceList']}
											size="small"
											rowKey={(record, index)=>index}
											scroll={{ x: 2400,y: 400 }}
											title={this.renderTableTitle}
											onChange={this.handleTablePage}
											pagination={{
													pageSizeOptions: ['10','20','30','40','50'],
													showQuickJumper: true,
													showSizeChanger: true,
													showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
													total:financeInfoCount,
											}}
									/>
              </Card>
							{
								this.state.isVisibleUpModal &&
								<UploadFormList
									isVisibleUpModal= {this.state.isVisibleUpModal}
									onConfirm={this.onUploadFormList}
									onCancel = {this.onUploadFormCancel}
									loading={this.props.loading['financeReport/uploadFinanceFile']}
								/>
							}
						  {
								this.state.isVisibleDownModal &&
								<DownLoadFormList
									isVisibleDownModal= {this.state.isVisibleDownModal}
									onConfirm={this.onDownloadFormList}
									onCancel = {this.onDownloadFormCancel}
									loading={this.props.loading['financeReport/exportFinanceFile']}
								/>
							}
							
							{
								this.state.isVisibleRecountModal &&
								<RecountFormList
									isVisibleRecountModal= {this.state.isVisibleRecountModal}
									onConfirm={this.onRecountFormList}
									onCancel = {this.onRecountFormCancel}
									loading={this.props.loading['financeReport/recountFileList']}
								/>
							}

							{
								this.state.isOfflineInfo &&
								<ImportOfflineInfo
									isOfflineInfo={this.state.isOfflineInfo}
									onConfirm={this.importOfflineInfo}
									onCancel = {this.importOfflineInfoCancel}
									loading={this.props.loading['financeReport/importOfflineInfo']}
								/>
							}
          </PageHeaderLayout>
        );
    }
}