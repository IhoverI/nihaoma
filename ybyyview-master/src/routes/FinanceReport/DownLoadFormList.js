import React, { PureComponent } from 'react';
import {
  Modal,
  Form,
  Select,
  Button,
} from 'snk-web';
import { formItemLayout, extEnterpCodeOptions } from './ReportConfig';
const FormItem = Form.Item;

@Form.create()
export default class UploadFormList extends PureComponent {
  onHandleUpload=(e)=>{
    //防止链接打开 URL
    if(e)e.preventDefault();
    const { form } = this.props;
    //表单校验
    form.validateFields((err, fieldsValue) => {
        if (err) return;
        if(this.props.onConfirm) this.props.onConfirm(fieldsValue);
    });
  }
  onCancel=()=>{
    if(this.props.onCancel) this.props.onCancel();
  }
  renderContent=()=>{
    const { getFieldDecorator, getFieldValue } = this.props.form;
    return(
      <Form onSubmit={this.onHandleUpload} layout="inline">
         <FormItem
            label="文件类型"
            style={{width: "100%"}}
            >
              {
                getFieldDecorator('fileType',{
                  rules: [{required: true, message: '文件类型',}]
                })(
                  <Select
                    placeholder="请选择文件类型"
                    style={{width: "200px"}}
                  >
                    <Select.Option value="0">保费实收</Select.Option>
                    <Select.Option value="1">手续费实付</Select.Option>
                  </Select>
                )
              }
          </FormItem>
          <FormItem  style={{width: "100%", textAlign: 'center'}}>
            <Button
             loading={this.props.loading}
             type="primary"
             htmlType="submit"
            >
              导出
            </Button>
            <Button
              style={{ margin: 12 }}
              onClick={this.onCancel}
            >
              取消
            </Button>
          </FormItem>
      </Form>
    )
  }
  render(){
    const {isVisibleDownModal} = this.props;
    return(
      <Modal
        title="导出文件"
        visible={isVisibleDownModal}
        footer={null}
        width="350px"
        closable={false}
      >
        { 
          this.renderContent()
        }
      </Modal>
    )
  }
}