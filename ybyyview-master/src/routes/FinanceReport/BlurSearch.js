import React from 'react';
import { connect } from 'dva';
import {
  Form,
  Row,
  Col,
  Button,
	Input,
	Select,
	DatePicker,
	message
} from 'snk-web';
import { formItemLayout, getPyearListOptions, extEnterpCodeOptions } from './ReportConfig';
const FormItem = Form.Item;
const { RangePicker } = DatePicker;
@connect(({ loading }) => ({
  loading: loading.effects,
}))
@Form.create()
export default class PreciseSearch extends React.Component { 
		state = {
			hasBatchNo: false,
		}
		//重置表单
    handleReset = () => {
			this.props.form.resetFields();
		}
		handleSearch = e => {
				//防止链接打开 URL
				if(e)e.preventDefault();
				const { dispatch, form } = this.props;
				//表单校验
				form.validateFields((err, fieldsValue) => {
						if (err) return;
						const { inputDate, ...restParams } = fieldsValue;
						const inputBeginDate = inputDate ? inputDate[0].format("YYYY-MM-DD") : '';
						const inputEndDate = inputDate ? inputDate[1].format("YYYY-MM-DD") : '';
						const params = {...restParams,inputBeginDate,inputEndDate}
						if(this.props.onSearch) this.props.onSearch(params);
				});
		}
		pyearChange = (value) => {
			if(value.length>=4){
				value.shift();
				message.warning("查询条件保单年份最多支持跨三年查询。");
			}
		}
		batchNoChange = (e) => {
			if(e.target.value) {
				this.setState({
					hasBatchNo: true,
				})
			} else {
				this.setState({
					hasBatchNo: false,
				})
			}
		}
    render() {
				const { getFieldDecorator } = this.props.form;
				const pyear = new Date().getFullYear();
				const { hasBatchNo } = this.state;
        return (
          <Form onSubmit={this.handleSearch}>
            <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
							<Col md={10} sm={24}>
								<FormItem
									label="保单年份"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('pyearList',{
												initialValue:[pyear]
											})(
												<Select
													mode='multiple'
													placeholder="请选择"
													onChange = {this.pyearChange}
												>
													{
														getPyearListOptions().map((item)=>(
															<Select.Option key={item} value={item}>{item}</Select.Option>
														))
													}
												</Select>
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="回盘导入时间"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('inputDate', {
												rules: [{required: !hasBatchNo, message: '请选择回盘导入时间！',}]
											})(
												<RangePicker/>
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="渠道名称"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('extEnterpCode',{
												rules: [{required: !hasBatchNo, message: '请选择渠道名称',}]
											})(
												<Select
													placeholder="请选择渠道名称"
												>
													{
														extEnterpCodeOptions.map((item)=>(
															<Select.Option key={item.code} value={item.code}>{item.code}-{item.name}</Select.Option>
														))
													}
												</Select>
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="扣款类型"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('chargeType',{
												initialValue: '0',
											})(
												<Select
													placeholder="请选择扣款类型"
												>
													<Select.Option value="0">请款文件</Select.Option>
													<Select.Option value="1">退保文件</Select.Option>
												</Select>
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="批次号"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('batchNo',{
											})(
												<Input autoComplete="off" onChange={this.batchNoChange} placeholder="请输入" />
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="保费实收状态"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('prmPayInFlag',{
											})(
												<Select
													placeholder="请选择保费实收状态"
												>
													<Select.Option value="0">实收成功</Select.Option>
													<Select.Option value="1">已推送收付</Select.Option>
													<Select.Option value="2">推送失败</Select.Option>
													<Select.Option value="3">未推送</Select.Option>
												</Select>
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="手续费实付状态"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('cmmPayOutFlag',{
											})(
												<Select
													placeholder="请选择手续费实付状态"
												>
													<Select.Option value="0">实付成功</Select.Option>
													<Select.Option value="1">已推送收付</Select.Option>
													<Select.Option value="2">推送失败</Select.Option>
													<Select.Option value="3">未推送</Select.Option>
												</Select>
											)
										}
									</FormItem>
							</Col>
            </Row>
						<Row>
							<Col style={{textAlign: 'center'}}>
								<Button
									 type="primary"
									 htmlType="submit"
									 loading={this.props.loading['financeReport/queryFinanceList']}
								>
										搜索
								</Button>
								<Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
										重置
								</Button>
							</Col>
					</Row>
        </Form>
        );
    }
}