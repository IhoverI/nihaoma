import { 
    Table, 
    Steps,
    Popconfirm,
    Row,
    Col,
    Card,
    Form,
    Input,
    Select,
    Icon,
    Button,
    Dropdown,
    Menu,
    InputNumber,
    DatePicker,
    Modal,
    message,
    Badge,
    Divider} from 'snk-web';
import React from 'react';
import styles from './querySarlary.less';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
 

const FormItem = Form.Item;
//定义进度条step
const Step = Steps.Step;
//table 行编辑的属性定义
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

//table 单元格的定义
class EditableCell extends React.Component {
  getInput = () => {
      const {dataIndex} = this.props
        if(dataIndex=='status'){
            return (
                <Select >
                    <Select.Option value={this.props.status}>通过</Select.Option>
                    <Select.Option value='6'>打回</Select.Option>
                </Select>
            )
        }

    return <Input/>;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (      
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    // rules: [{
                    //   required: true,
                    //   message: `请输入${title}!`,
                    // }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>
               
          );
        }}
      </EditableContext.Consumer>
    );
  }

}


  //装饰器
  @connect(({umssouserinfo, sarlaryMdl,loading}) => ({
    umssouserinfo,
    sarlaryMdl,
    loading:loading.models.sarlaryMdl,
  }))
  
  
  @Form.create()
  export default class auditingSarlary extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            dataSource: [],
            editingKey: '',
            list:[],
            status:'',
            paramData:{},
            pagination:{current:1,pageSize:50,showSizeChanger:true,pageSizeOptions:['30','50','80','100']}
        };
        this.columns = [
            {
                title: '员工姓名',
                dataIndex: 'empName',
                width: '120px',
                editable:false,
                fixed: 'left'
            },
            {
                title: '编辑',
                dataIndex: 'operation',
                width: '120px',
                editable:false,
                render: (text, record) => {
                  const editable = this.isEditing(record);                
                  return (
                    <div>
                      {editable ? (
                        <span>
                        <EditableContext.Consumer>
                          {form => (
                            <a
                              href="javascript:;"
                              onClick={() => this.save(form, record)}
                              style={{ marginRight: 8 }}
                            >
                              确认
                            </a>
                          )}
                        </EditableContext.Consumer>
                          <Popconfirm
                            title="确定放弃编辑?"
                            onConfirm={() => {                     
                              this.cancel(record.key)
                            }}
                          >
                            <a style={{ marginRight: 8 }}>取消</a>
                          </Popconfirm>
                      </span>
                      ) : (
                        <span>
                           <a onClick={() => this.edit(record.key)}>编辑</a>
                        </span>
                      )}
                    </div>
                  );
                },
            },
            {
                title:"序号",
                dataIndex:"id",
                width: '70px',
                key:"id",
                editable:false,
                render: (text, record,index) => {
                    return (
                      <span>
                        {(this.state.pagination.current-1)*this.state.pagination.pageSize+index+1}
                      </span>
                    );
                },   
            },
            {
                title: '系统调整',
                dataIndex: 'sysAdjust',
                width: '130px',
                editable: true,
                // className:styles.columnsBcolor
            },
            {
                title: '系统备注',
                dataIndex: 'sysRemark',
                width: '130px',
                editable:true,
                // className:styles.columnsBcolor
            },
            {
                title: '手工调整',
                dataIndex: 'adjust',
                width: '120px',
                editable: false,
                // className:styles.columnsBcolor
            },
            {
                title: '备注',
                dataIndex: 'remark',
                width: '120px',
                editable:false,
                // className:styles.columnsBcolor
            },
            {
                title: '操作',
                dataIndex: 'status',
                width: '120px',
                editable: true,
                className:styles.columnsBcolor,
                render: (text, ) => {
                    return text==6?'打回':'通过'
                    // return (
                    // <div>
                    //     <Select style={{ width: 100 }} 
                    //         // onChange={(recordSelected) => this.handleDefaultChange(recordSelected, record)}
                    //         // defaultValue = '0'
                    //         >
                    //             <Select.Option value={this.state.status}>通过</Select.Option>
                    //             <Select.Option value='6'>打回</Select.Option>
                    //     </Select>


                    //     {/* {(this.state.editingKey == record.key) ? (
                    //         <Select style={{ width: 100 }} 
                    //             onChange={(recordSelected) => this.handleDefaultChange(recordSelected)}
                    //             defaultValue = {status}
                    //             >
                    //             <Select.Option value= {status}>通过</Select.Option>
                    //             <Select.Option value='6'>打回</Select.Option>
                    //         </Select>
                    //         ): 
                    //         status ?
                    //             (<span>
                    //                 <Select style={{ width: 100 }} 
                    //                 onChange={(recordSelected) => this.handleDefaultChange(recordSelected)}
                    //                 defaultValue = {status}
                    //                 disabled
                    //                 >
                    //                 <Select.Option value= {status} >通过</Select.Option>
                    //                 <Select.Option value='6'>打回</Select.Option>
                    //                 </Select>
                    //             </span>)
                    //             :
                    //                 (<span>
                    //                 <Select style={{ width: 100 }} 
                    //                     onChange={(recordSelected) => this.handleDefaultChange(recordSelected)}
                    //                     defaultValue =  {status}
                    //                     disabled
                    //                     >
                    //                     <Select.Option value={status}>通过</Select.Option>
                    //                     <Select.Option value='6'>打回</Select.Option>
                    //                 </Select>
                    //                 </span>)
                    //         } */}
                    // </div>
                    // )
                },
            },
            {
                title: '原因',
                dataIndex: 'approveRemark',
                width: '110px',
                editable:true,
                className:styles.columnsBcolor
            },
            {
                title: '团队名称',
                dataIndex: 'groupName',
                width: '120px',
                editable:false
            },
            {
                title: '工号',
                dataIndex: 'employeeCode',
                width: '120px',
                editable:false,
            },
            {
                title: '组别名称',
                dataIndex: 'teamName',
                width: '120px',
                editable:false,
            },
            {
                title: '职级',
                dataIndex: 'rankName',
                width: '120px',
                editable:false,
            },
            {
                title: '入职时间',
                dataIndex: 'entryDate',
                width: '140px',
                editable:false,
            },
            {
                title: '年限',
                dataIndex: 'workAge',
                width: '100px',
                editable:false,
            },
            {
                title: '离职时间',
                dataIndex: 'leaveDate',
                width: '150px',
                editable:false,
            },
            {
                title: '上线时间',
                dataIndex: 'onlineDate',
                width: '150px',
                editable:false,
            },
            {
                title: '身份证号码',
                dataIndex: 'identityCode',
                width: '170px',
                editable:false,
            },
            {
                title: '一卡通(银行卡号)',
                dataIndex: 'bankNo',
                width: '170px',
                editable:false,
            },
            {
                title: '请款业务',
                dataIndex: 'requestPrm',
                width: '110px',
                editable:false,
            },
            {
                title: '3月内退保',
                dataIndex: 'cancePrm',
                width: '110px',
                editable:false,
            },
            {
                title: '计薪保费',
                dataIndex: 'payPremium',
                width: '110px',
                editable:false,
            },
            {
                title: '基本工资',
                dataIndex: 'salaryBasic',
                width: '110px',
                editable:false,
            },
            {
                title: '职级津贴',
                dataIndex: 'salaryAllowance',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO奖',
                dataIndex: 'salaryMbo',
                width: '110px',
                editable:false,
            },
            {
                title: '提成',
                dataIndex: 'salaryPerform',
                width: '110px',
                editable:false,
            },
            {
                title: '新人津贴',
                dataIndex: 'salaryNew',
                width: '110px',
                editable:false,
            },
            {
                title: '服务津贴',
                dataIndex: 'salaryService',
                width: '110px',
                editable:false,
            },
            {
                title: '內荐费',
                dataIndex: 'salaryRec',
                width: '110px',
                editable:false,
            },
            {
                title: '激励方案兑换',
                dataIndex: 'salaryStimulate',
                width: '110px',
                editable:false,
            },
            {
                title: '出勤扣',
                dataIndex: 'salaryAttendance',
                width: '110px',
                editable:false,
            },
            {
                title: '餐补',
                dataIndex: 'salarySupple',
                width: '110px',
                editable:false,
            },
            {
                title: '通勤车费代扣',
                dataIndex: 'salaryCar',
                width: '110px',
                editable:false,
            },
            {
                title: '客诉扣罚',
                dataIndex: 'salaryComplaint',
                width: '110px',
                editable:false,
            },
            {
                title: '品质违规罚款',
                dataIndex: 'salaryQuality',
                width: '110px',
                editable:false,
            },
            {
                title: '业务品质奖',
                dataIndex: 'salaryBusiness',
                width: '110px',
                editable:false,
            },
            {
                title: '培训费',
                dataIndex: 'salaryTrain',
                width: '110px',
                editable:false,
            },
            {
                title: '过节费',
                dataIndex: 'salaryFestival  ',
                width: '110px',
                editable:false,
            },
            {
                title: '员工应发工资(含税)',
                dataIndex: 'sumSalaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '应发工资',
                dataIndex: 'sumSalaryPay',
                width: '110px',
                editable:false,
            },
            {
                title: '社保个人',
                dataIndex: 'salaryPersonalSb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金个人',
                dataIndex: 'salaryPersonalGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '所得税',
                dataIndex: 'salaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '独子费',
                dataIndex: 'salarySingleChild',
                width: '110px',
                editable:false,
            },
            {
                title: '实发合计',
                dataIndex: 'salaryReal',
                width: '110px',
                editable:false,
            },
            {
                title: '社保公司',
                dataIndex: 'salaryCpySb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金公司',
                dataIndex: 'salaryCpyGjj',
                width: '110px',
                editable:false,
            },

            {
                title: '年终奖',
                dataIndex: 'salaryYear',
                width: '110px',
                editable:false,
            },
            {
                title: '年终奖个税',
                dataIndex: 'salaryYearTax',
                width: '110px',
                editable:false,
            },
            {
                title: '公司社保费用',
                dataIndex: 'salarySbGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '派遣公司管理费用',
                dataIndex: 'salaryManage',
                width: '110px',
                editable:false,
            },
            {
                title: '薪酬费用合计',
                dataIndex: 'sumSalaryFee',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO系数',
                dataIndex: 'mbo',
                width: '110px',
                editable:false,
            },
            {
                title: '本月工作日',
                dataIndex: 'workDay',
                width: '110px',
                editable:false,
            },

            {
                title: '实际出勤天数',
                dataIndex: 'realWorkDay',
                width: '110px',
                editable:false,
            },
            {
                title: '培训天数',
                dataIndex: 'trainDay',
                width: '110px',
                editable:false,
            },
            {
                title: '年假',
                dataIndex: 'yearHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '事假',
                dataIndex: 'affairHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '病假',
                dataIndex: 'sickHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '旷工',
                dataIndex: 'absence',
                width: '110px',
                editable:false,
            },
            
            {
                title: '婚假',
                dataIndex: 'marriageHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '丧假',
                dataIndex: 'loseHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '陪产假',
                dataIndex: 'paternityHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产假',
                dataIndex: 'childBirthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产检假',
                dataIndex: 'productionHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '生日假',
                dataIndex: 'birthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '迟到',
                dataIndex: 'arriveLate',
                width: '110px',
                editable:false,
            },
            {
                title: '是否当月离职',
                dataIndex: 'isNowMonthLeave',
                width: '110px',
                editable:false,
            },
            {
                title: '是否长病假',
                dataIndex: 'isLongSick',
                width: '110px',
                editable:false,
            },
            {
                title: '培训期病假',
                dataIndex: 'trainSickHoliday',
                width:120
              },
              {
                title: '培训期事假',
                dataIndex: 'trainAffairHoliday',
                width:120
              },
            {
                title: '当月是否转岗',
                dataIndex: 'isChangePosition',
                width: '110px',
                editable:false,
                render:(text)=>text==1?'是':'否'
            },
        ];
    }
  
    componentDidMount() {
        this.fetch()

    }

    //初始化数据
    fetch=()=>{
        const { dispatch} = this.props;
        let search = this.props.location.search; 
        var params = [];
        //?groupId=2&calcMonth=7&calcYear=2018
        params= search.slice(1).split('&');        
        
        var pData = [];
        for(let i=0;i<params.length;i++){
            pData.push(params[i].split('=')[1]);
        }
        //参数
        const paramData = {
            groupId:pData[0],
            calcMonth:pData[1],
            calcYear:pData[2]
        };     

        dispatch({
            type: 'sarlaryMdl/queryPrivSalaryResult',
            data:paramData
        }).then(()=>{
            const dataSource = this.props.sarlaryMdl.sarlaryData;
             // console.log(Object.keys(dataSource[0]).length)
            const status = dataSource[0].status;  
            this.setState({dataSource,status,paramData});
        }).catch((e)=>{
            console.log(e);
        });
    }

    // handleDefaultChange = (recordSelected, record) => {
    //   const newData = [...this.state.dataSource];
    //   const {key} = this.state.editingRecord;
    //   const index = newData.findIndex(item => key === item.key);

    //   let item = newData[index];
    //   if(index > -1 ){
        
    //     if(recordSelected === '6'){
    //         item.status = recordSelected;
    //     }

    //     newData.splice(index, 1, {
    //         ...this.state.editingRecord,
    //         ...item,
    //     });  
    //   }
    //   this.setState({ dataSource: newData }); 
 
    // }

    //编辑
    edit(key){
        const {editingKey} = this.state;
        if(editingKey != ''){
          message.error("你有规则正在编辑状态，请先处理后再编辑！")      
        }else{
          this.setState({ editingKey: key});
        }
    }

    save(form, record) {
        form.validateFields((error, row) => {
          if (error) {
            return;
          }
        //检测是否打回，打回则原因必填   
        const flag1 = row.status==6 //是否打回
        const flag2 =  this.state.list.findIndex(item => record.key == item.id); //是否存在于list中
        let list =this.state.list;
            const objectArg = {
                id: record.key,
                approveRemark: row.approveRemark,
            }
            if (flag1) {
                const val = row.approveRemark
                if (!val) {
                    message.error('原因必填')
                    return
                }
                if (flag2 > -1){
                    list[flag2] = objectArg
                } else{
                    list.push(objectArg);
                }
            } else {
                if (flag2 > -1) {
                    list = list.filter(e => e.id != record.key);
                }
            }
            console.log(list)


          //刷新更改的行
          const newData =this.state.dataSource;
          const index = newData.findIndex(item => record.key == item.key);
          if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, {
              ...item,
              ...row,
            });
            this.setState({ dataSource: newData, editingKey: '',list });
          } else {
            newData.push(row);
            this.setState({ dataSource: newData, editingKey: '',list });
          }
        });
      }

    //table 行编辑事件
    isEditing = (record) => {
        return record.key === this.state.editingKey;   
    };

    //取消
    cancel = () => {
        this.setState({ editingKey: '' });
    };


    handleConfirmSubmit = () => {
        //获取数据源
        const {dataSource,paramData,status} = this.state;
        const { dispatch } = this.props;
        const index =  this.state.dataSource.findIndex(e=>e.status==6)
        //请求参数
        const newDataObj = {
            status:index>-1?6:(Number(status)+1).toString(),     //如果是6，直接打回，如果通过，状态加1，提交下一节点
            calcMonth:paramData.calcMonth,
            calcYear:paramData.calcYear,
            groupId:paramData.groupId,
            groupName:dataSource[0].groupName,
            list:this.state.list
        }        

        dispatch({
            type: 'sarlaryMdl/approvedSalaryResult',
            data: newDataObj
        }).then(()=>{
            dispatch({
                type: 'sarlaryMdl/queryPrivSalaryResult',
                data:paramData
            }).then(()=>{
                this.setState({dataSource: this.props.sarlaryMdl.sarlaryData});
            }).catch((e)=>{
                console.log(e);
            });
            message.success("提交审核成功");
        }).catch((e)=>{
            console.log(e);
        });
    }

    pageOnChange=(pagination, filters, sorter)=>{
        this.setState({pagination})
    }
   

      //加载组件
    render() {
        
        const {loading} = this.props;
        const {dataSource} = this.state;

        //引入table列和编辑
        const columns = this.columns.map((col) => {
            if (!col.editable) {
            return col;
            }
            return {
            ...col,
            onCell: record => ({
                record,
                // inputType: col.dataIndex,
                dataIndex: col.dataIndex,
                title: col.title,
                editing: this.isEditing(record),
                status:this.state.status,
            }),
            };
        });


        //组件组合
        const components = {
            body: {
              row: EditableFormRow,
              cell: EditableCell,
            },
          };
        
        return (
            <PageHeaderLayout>
              <Card title="薪酬计算的结果" bordered={false}>
                <div>
                    <Steps current={Number(this.state.status)}>
                        <Step title="薪酬修改"  />
                        <Step title="人事主任复核"  />
                        <Step title="财务初审"  />
                        <Step title="财务主任复核"  />
                        <Step title="完成"  />
                        <Step title="已打回"  />
                    </Steps>
                </div>
                <div className={styles.tableList}>
                  <Table
                    size="small"
                    components={components}
                    loading={loading}
                    dataSource={dataSource}
                    columns={columns}
                    pagination={this.state.pagination}
                    onChange={this.pageOnChange}
                    scroll={{ x: 8000,y:300 }}
                    // rowClassName={(record)=>{return record.status==1?styles.rowBcColor:null}}
                    />
                </div>
                <div className={styles.tableListForm}>
                    <Button type="primary" onClick={this.handleConfirmSubmit}>
                        确认提交
                    </Button>
                  </div>
              </Card>
              </PageHeaderLayout>
        );
      }
}
  
  
  
  