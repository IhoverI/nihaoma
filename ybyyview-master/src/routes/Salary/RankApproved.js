
import styles from './EmailSalary.less'
import { Table, Form, Row, Col, Input, Button, Icon, Select, DatePicker, Popconfirm, message, Card } from 'snk-web';
const { MonthPicker, RangePicker } = DatePicker;
import moment from 'moment';
import { expressRanks } from '../../services/api'
import HttpUtils from '../../utils/HttpUtils';
import { selectRankName } from '../../services/personService'
import { resolve } from 'url';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
// import _ from 'lodash'



const FormItem = Form.Item;
const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
class EditableCell extends React.Component {
    state = {
        options: null
    }
    getInput = () => {
        const { record } = this.props
        if (this.props.dataIndex == 'rankName' && this.state.options == null) {
            HttpUtils.post({
                url: '/emp/selectRankName',
                body: { "data": { "positionId": String(record.positionId) } }
            }).then(response => {
                this.setState({
                    options: <Select style={{ width: 120 }}>
                        {response.data.map((rank) => <Select.Option key={rank.id + '-' + rank.rankName}>{rank.rankName}</Select.Option>)}
                    </Select>
                })
            }).catch(e => message.error(e.message))
        } else {
            return this.state.options
        }
        return <Input />;
    };



    render() {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            ...restProps
        } = this.props;
        return (
         
            <EditableContext.Consumer>
                {(form) => {
                    const { getFieldDecorator } = form;
                    return (
                        <td {...restProps}>
                            {editing ? (
                                <FormItem style={{ margin: 0 }}>
                                    {getFieldDecorator(dataIndex, {
                                        rules: [{
                                            required: true,
                                            message: `请选择 ${title}!`,
                                        }],
                                        initialValue: dataIndex == 'rankName' ? record['rankId'] + '-' + record[dataIndex] : record[dataIndex],
                                    })(this.getInput())}
                                </FormItem>
                            ) : restProps.children}
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        );
    }
}

class AdvancedSearchForm extends React.Component {
    state = {
        groupNames: [],
    };

    componentDidMount() {
        // // 初始化参数
        // this.props.form.validateFields((err, values) => {
        //     if (err) return
        //     const calcYear = values.calDate==null?null:values.calDate.format('YYYY')
        //     const calcMonth = values.calDate==null?null:values.calDate.format('MM')
        //     this.props.setDefaultValue({
        //         "groupId": Number(values.teamName),
        //         "calcYear": calcYear,
        //         "calcMonth": calcMonth
        //     })
        // });
        //获取相关option 数据
        HttpUtils.post({
            url: '/emp/selectGroupName',
        }).then((response) => {
            console.log(response)
            this.setState(
                {
                    groupNames: response.data,
                }
            )
        });

    }





    handleSearch = (e) => {
        if (e) e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (err) return
            const calcYear = values.calDate==null?null:values.calDate.format('YYYY')
            const calcMonth = values.calDate==null?null:values.calDate.format('MM')
            // {data”:[],calcMonth:"",calcYear;""}
            this.props.fetch({
                "groupId": Number(values.teamName),
                "calcYear": calcYear,
                "calcMonth": calcMonth
            })
        });
    }
    downLoad = (e) => {
        if(e) e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (err) return
            const calcYear = values.calDate==null?null:values.calDate.format('YYYY')
            const calcMonth = values.calDate==null?null:values.calDate.format('MM')
            // {data”:[],calcMonth:"",calcYear;""}
            const data={
                "groupId": Number(values.teamName),
                "calcYear": calcYear,
                "calcMonth": calcMonth
            }
            this.setState({loading:true})

              // this.setState({ loading:true  })
        expressRanks(data).then(response => {
            console.log(response)
            this.setState({ loading:false  })
            const fileName = "职级信息清单.xls";
            if ('msSaveOrOpenBlob' in navigator) {
                window.navigator.msSaveOrOpenBlob(response, fileName);
            } else {
                const a = document.createElement('a');
                const url = window.URL.createObjectURL(response);
                a.href = url;
                a.download = fileName;
                a.click();
                window.URL.revokeObjectURL(url);
            }
            this.setState({loading:false})
        }).catch(e =>{
            this.setState({loading:false})
            message.error(e.message)
        } )
        });
      
      
    }



    getFields() {
        const { getFieldDecorator } = this.props.form;
        const children = [];
        children.push(
            <Col span={6} key='teamName' style={{ display: 'block' }}>
                <FormItem label='团队名称'>
                    {getFieldDecorator('teamName', {
                        initialValue: this.state.groupNames[0] ? String(this.state.groupNames[0].id) : '',
                    })(
                        <Select placeholder="请输入团队名称" >
                            {this.state.groupNames.map((group) => <Select.Option key={group.id}>{group.groupName}</Select.Option>)}
                        </Select>
                    )}
                </FormItem>
            </Col>
        );
        const monthFormat = 'YYYY-MM';
        children.push(
            <Col span={6} key='calDate' style={{ display: 'block' }}>
                <FormItem label='考核时间'>
                    {getFieldDecorator('calDate', {
                        initialValue: moment().subtract(1, 'months')
                    })(
                        <MonthPicker format={monthFormat}  allowClear={false}/>
                    )}
                </FormItem>
            </Col>
        );
        children.push(
            <FormItem key='submet'>
                <Button type="primary" htmlType="submit"> 搜索 </Button>
                <Button style={{ marginLeft: 20 }}
                                type="primary"
                                onClick={this.downLoad}
                                loading={this.state.loading}
                                icon="download"
                                ghost={true}
                            >
                                导出Excel清单
                      </Button>
            </FormItem>
        )
        return children;
    }

    render() {
        return (
            <Form
                className={styles.serchForm}
                onSubmit={this.handleSearch}
            >
                <Row gutter={24}>{this.getFields()}
                </Row>

            </Form>
        );
    }
}

const WrappedAdvancedSearchForm = Form.create()(AdvancedSearchForm);


export default class RankApproved extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            loading: false,
            editingKey: '',
            rankList: [],
            options: null
        };
        this.columns =
            [{
                title: '姓名',
                width:100,
                dataIndex: 'empName',
                fixed: 'left' 
            }, {
                title: '序号',
                dataIndex: 'record',
                width:80,
                render: (row, record, index) => index + 1
            }, {
                title: '工号',
                width:100,
                dataIndex: 'employeeCode',
            }, {
                title: '团队',
                width:100,
                dataIndex: 'groupName',
            },
            {
                title: 'N月计薪保费',
                width:130,
                dataIndex: 'currentMonthPremium',
            }, {
                title: 'N-1月计薪保费',
                width:130,
                dataIndex: 'precedingMonthPremium',
            }, {
                title: 'N-2月计薪保费',
                width:130,
                dataIndex: 'lastMonthPremium',
            }, {
                title: 'M季度保费合计',
                width:130,
                dataIndex: 'premium',
            }, {
                title: 'M季度职级拟定',
                width:160,
                dataIndex: 'rankName',
                editable: true,
            },
            {
                title: '上线时间/入司时间',
                width:130,
                dataIndex: 'onlineDate',
                render: (row) => moment(row).format('YYYY-MM-DD')
            }, {
                title: 'M季度工作日',
                width:100,
                dataIndex: 'workDay',
            },
            {
                title: '上线天数',
                width:80,
                dataIndex: 'onlineDays',
            }, {
                title: '折算后业绩',
                width:100,
                dataIndex: 'covPrm',
            }, {
                title: '状态',
                width:100,
                dataIndex: 'status',
                render: (row) => row == 1 ? '已确认' : '未确认'
            }, {
                title: '操作',
                width:100,
                // fixed: 'right', 
                render: (text, record) => {
                    const editable = this.isEditing(record);
                    return (
                        <div>
                            {editable ? (
                                <span>
                                    <EditableContext.Consumer>
                                        {form => (
                                            <a
                                                href="javascript:;"
                                                onClick={() => this.save(form, record)}
                                                style={{ marginRight: 8 }}
                                            >
                                                保存
                                  </a>
                                        )}
                                    </EditableContext.Consumer>
                                    <Popconfirm
                                        title="确定取消吗"
                                        onConfirm={() => this.cancel(record.id)}
                                    >
                                        <a>取消</a>
                                    </Popconfirm>
                                </span>
                            ) : <a onClick={() => this.edit(record.id)}>编辑</a>
                            }
                        </div>
                    );
                }
            }]

    }


    fetch = (params = {}) => {
        console.log(params)
        this.setState({ loading: true });
        const response = HttpUtils.post({
            url: 'emp/searchRankResult',
            body: { data: params }
        }).then((response) => {
            this.setState(
                {
                    data: response.data,
                    loading: false,
                    searchPara: params
                }
            )
        }).catch(e => {
            console.log(e)
            message.error(e.message)
            this.setState(
                {
                    loading: false,
                })
        });
    }

    

    start = () => {
        if (this.state.editingKey == '') {
            if(this.state.data.length==0){
                message.info('请先查询信息后再提交')
                return;
            }
            const list =  this.state.data.map(e=> {return {id:e.id,rankId:e.rankId}})
                HttpUtils.post({
                    url: '/emp/confirmRank',
                    body: { data: {list},...this.state.searchPara }
                }).then(() => {
                    this.fetch(this.state.searchPara)
                }).catch(e => message.error(e.message))
        } else {
            message.info('当前有正在编辑')
        }
    }

    isEditing = (record) => {
        return record.id === this.state.editingKey;
    };
    edit(id) {
        this.setState({ editingKey: id });
    }
    save(form, record) {
        const key = record.id
        form.validateFields((error, row) => {
            if (error) {
                return;
            }
            const newData = [...this.state.data];
            const index = newData.findIndex(item => key === item.id);
            if (index > -1) {
                const item = newData[index];
                const values = row.rankName.split('-')
                const data= {"id":record.id,"rankId":Number(values[0]),"rankName":values[1]}
                HttpUtils.post({
                    url: 'emp/updateRank',
                    body: {data}
                }).then(() => {
                    newData.splice(index, 1, {
                        ...item,
                        rankId: Number(values[0]),
                        rankName: values[1],
                        // status:"1"
                    });
                    this.setState({
                        data: newData,
                        editingKey: '',
                    });
                    message.info("保存成功")
                }).catch(e => {
                    message.error(e.message)
                });
            } else {
                this.setState({ data: newData, editingKey: '' });
            }
        });
    }

    cancel = () => {
        this.setState({ editingKey: '' });
    };




    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell,
            },
        };

        const columns = this.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });

        return (
            <PageHeaderLayout>
            <Card >
               <Card>
                <WrappedAdvancedSearchForm fetch={this.fetch}
                //  setDefaultValue={this.setDefaultValue}
                  />
               </Card>
               
                <div className={styles.serchForm}>
                    <Table bordered
                        components={components}
                        columns={columns}
                        rowKey={record => record.id}
                        dataSource={this.state.data}
                        // disabled={!hasSelected}
                        loading={this.state.loading}
                        scroll={{x:1700,y:400}}
                        pagination={false}
                        footer={() => <div style={{ margin: 30, float: "right" }}>
                            
                            <Button
                                type="primary"
                                onClick={this.start}
                                // disabled={!hasSelected}
                                loading={this.state.loading}
                            >
                                确认职级信息
                         </Button>
                        </div>}
                    />
                </div>
            </Card>
            </PageHeaderLayout>
        );
    }
}

