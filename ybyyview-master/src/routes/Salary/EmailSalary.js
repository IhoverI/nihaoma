import  styles  from './EmailSalary.less'
import { Table, Form, Row, Col, Input, Button, Icon, Select, DatePicker, AutoComplete, message,Card } from 'snk-web';
const { MonthPicker, RangePicker } = DatePicker;
import moment from 'moment';
import HttpUtils from '../../utils/HttpUtils';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';


const FormItem = Form.Item;

class AdvancedSearchForm extends React.Component {
    state = {
        groupNames:[],
        jobNames: [],
        nameDatasoucre: [],
        teamNames: [],
    };
    componentDidMount(){
            this.handleSearch()
          //获取相关option 数据
          HttpUtils.post({
            url: '/emp/selectGroupName',
        }).then((response) => {
            console.log(response)
            this.setState(
                {
                    groupNames: response.data,
                }
            )
        });
        HttpUtils.post({
            url: '/emp/selectPositionName',
            body: { data: {} }
        }).then((response) => {
            console.log(response)
            this.setState(
                {
                    jobNames: response.data,
                }
            )
        });
    }

    handleReset = () => {
        this.props.form.resetFields();
    }

    nameHandleSearch = (value) => {
        console.log(value)
        HttpUtils.post({
            url: '/emp/likeEmpName',
            body: { "data": { "empName": value } }
        }).then((response) => {
            this.setState({
                nameDatasoucre: response.data.list.map((person) => person.id + '-' + person.empName)
            })
        })

    }

    handleSearch = (e) => {
        if(e)e.preventDefault();
        this.props.form.validateFields((err, values) => {
            let date = values.calDate
            let calcYear = ''
            let calcMonth = ''
            console.log(date)
            if (date) {
                calcYear = date.year()
                calcMonth = date.month() + 1
            }
            let data = {
                "calcYear": calcYear,
                "calcMonth": calcMonth,
                "groupId": values.teamName,
                "teamId": values.groupName,
                "empId": values.empName.split('-')[0],
                "positionId": values.jobName,
                "status":values.status
            
            }
            this.props.fetch(data)

        });
    }

    teamChange = (e) => {
        console.log(e)
        HttpUtils.post({
            url: '/emp/selectTeamName',
            body: { "data": { "groupId": Number(e) } }
        }).then((response) => {
            this.setState({
                teamNames: response.data
            })
        })


    }

    getFields() {
        const { getFieldDecorator } = this.props.form;
        const children = [];
        children.push(
            <Col span={6} key='teamName' style={{ display: 'block' }}>
                <FormItem label='团队名称'>
                    {getFieldDecorator('teamName', {
                        initialValue: '',
                    })(
                        <Select placeholder="请输入团队名称" onChange={this.teamChange} allowClear>
                            {this.state.groupNames.map((group) => <Select.Option key={group.id}>{group.groupName}</Select.Option>)}
                        </Select>
                    )}
                </FormItem>
            </Col>
        );
        children.push(
            <Col span={6} key='groupName' style={{ display: 'block' }}>
                <FormItem label='组别名称'>
                    {getFieldDecorator('groupName', {
                        initialValue: '',
                    })(
                        <Select placeholder="请输入组别名称" allowClear>
                            {this.state.teamNames.map((team) => <Select.Option key={team.id}>{team.teamName}</Select.Option>)}
                        </Select>
                    )}
                </FormItem>
            </Col>
        );
        children.push(
            <Col span={6} key='jobName' style={{ display: 'block' }}>
                <FormItem label='岗位'>
                    {getFieldDecorator('jobName', {
                        initialValue: '',
                    })(
                        <Select placeholder="请输入岗位" allowClear>
                            {this.state.jobNames.map((job) => <Select.Option key={job.id}>{job.positionName}</Select.Option>)}
                        </Select>
                    )}
                </FormItem>
            </Col>
        );
        children.push(
            <Col span={6} key='status' style={{ display: 'block' }}>
                <FormItem label='发送标志'>
                    {getFieldDecorator('status', {
                        initialValue: '',
                    })(
                        <Select placeholder="发送状态"  allowClear>
                            <Select.Option value="1">发送成功</Select.Option>
                            <Select.Option value="0">发送失败</Select.Option>
                            <Select.Option value="2">未发送</Select.Option>
                        </Select>
                    )}
                </FormItem>
            </Col>
        );
        children.push(
            <Col span={6} key='empName' style={{ display: 'block' }}>
                <FormItem label='员工姓名'>
                    {getFieldDecorator('empName', {
                        initialValue: '',
                    })(
                        <AutoComplete
                            dataSource={this.state.nameDatasoucre}
                            // style={{ width: 200 }}
                            onSearch={this.nameHandleSearch}
                            placeholder="请输入员工姓名"
                        />
                    )}
                </FormItem>
            </Col>
        );
        const monthFormat = 'YYYY/MM';
        children.push(
            <Col span={6} key='calDate' style={{ display: 'block' }}>
                <FormItem label='计算年月'>
                    {getFieldDecorator('calDate', {
                        initialValue: moment().subtract(1, 'months')
                    })(
                        <MonthPicker format={monthFormat}  />
                    )}
                </FormItem>
            </Col>
        );

        return children;
    }

    render() {
        return (
            <Form  
                className={styles.serchForm}
                onSubmit={this.handleSearch}
            >
                <Row gutter={24}>{this.getFields()}
                </Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'center' }}>
                        <Button type="primary" htmlType="submit"> 搜索 </Button>
                        <Button style={{ marginLeft: 8 }} onClick={this.handleReset}> 重置 </Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}

const WrappedAdvancedSearchForm = Form.create()(AdvancedSearchForm);


export default class EmailSalary extends React.Component {
    state = {
        data: [],
        pagination: {
            pageNum: 1, pageSize: 10, showSizeChanger: true, showQuickJumper: true,
        },
        loading: false,
        selectedRowKeys: [],
    };
    
 
    handleTableChange = (pagination, filters, sorter) => {
        const pager = { ...this.state.pagination };
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });
        this.fetch({...this.state.searchPara,pageNum: pager.current, pageSize: pager.pageSize });
    }

    fetch = (params = {}) => {
        this.setState({ loading: true });
        let response = HttpUtils.post({
            url: '/emp/querySalaryEmail',
            body: { data: params }
        }).then((response) => {
            console.log(response)
            this.setState(
                {
                    data: response.data,
                    loading: false,
                    pagination: { pageSize: response.data.pageSize, current: response.data.pageNum, total: response.data.total },
                    searchPara:params
                }
            )
        });


    }



    start = () => {
        this.setState({ loading: true });
        // ajax request after empty completing
        const data= this.state.data.list.filter(e=>this.state.selectedRowKeys.indexOf(e.id)>-1)
        // setTimeout(() => {
            HttpUtils.post({
                url: 'emp/sendSalaryEmail',
                body: { data }
            }).then((response) => {
                this.setState({
                    selectedRowKeys: [],
                    loading: false,
                });
                if (response.code == 0) {
                    message.success('发送成功');
                    console.log(this.state.searchPara)
                    this.fetch(this.state.searchPara)
                } else {
                    message.error(response.message)
                }
            }).catch(e=>message.error('发送失败'))
        // }, 100);
    }
    getColumn = () => {
        return [{
            title: '员工姓名',
            dataIndex: 'empName',
            width: '10%',
        }, {
            title: '证件号码',
            dataIndex: 'identityCode',
            width: '10%',
        }, {
            title: '邮箱地址',
            dataIndex: 'email',
            width: '10%',
        }, {
            title: '邮件内容',
            dataIndex: 'content',
            width: '50%',
        }, {
            title: '发送状态',
            dataIndex: 'status',
            width: '10%',
            render: (status) => {
                if (status == 1) {
                    return '发送成功'
                } else if (status == 0) { return '发送失败' } else {
                    return '未发送'
                }
            }
        }];

    }
    onSelectChange = (selectedRowKeys) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
    }


    render() {
        const { loading, selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;
        return (
            <PageHeaderLayout>
                <Card>
                <WrappedAdvancedSearchForm  fetch={this.fetch}  />
                <div className={styles.serchForm}>
                    <div style={{ marginBottom: 16 }}>
                        <Button 
                            type="primary"
                            onClick={this.start}
                            disabled={!hasSelected}
                            loading={loading}
                        >
                            发送邮件
                </Button>
                
                    </div>
                    <Table bordered  
                        columns={this.getColumn()}
                        rowKey={record => record.id}
                        dataSource={this.state.data.list}
                        pagination={this.state.pagination}
                        loading={this.state.loading}
                        scroll={{y:500}}
                        onChange={this.handleTableChange}
                        rowSelection={rowSelection}
                    />
                </div>
                </Card>
            </PageHeaderLayout>
        );
    }
}
