import { 
    Transfer, 
    Row,
    Col,
    Card,
    Form,
    Input,
    Select,
    // Icon,
    Button,
    // Dropdown,
    // Menu,
    // InputNumber,
    // DatePicker,
    Modal,
    message,
    // Badge,
    // Divider,
} from 'snk-web';
import React from 'react';
import { connect } from 'dva';
import styles from './personTempConfig.less';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';


const FormItem = Form.Item;
/*
 * 新增弹出框
*/
const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      title="新建导入因素"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="因素名称" >
        {form.getFieldDecorator('factorName', {
          rules: [{ required: true, message: '因素名称' }],
        })
        (<Input placeholder="请输入因素名称" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注" >
        {form.getFieldDecorator('remark', {
          rules: [{ required: false, message: '备注不能为空' }],
        })
        (<Input placeholder="请输入岗位" type='text' />)}
      </FormItem>
      
    </Modal>
  );
});


//装饰器
@connect(({ personTempCfgMdl,umssouserinfo}) => ({
  personTempCfgMdl,
  umssouserinfo
}))


export default class personTempConfig extends React.Component {
    //定义状态变量
    state = {
      dataSource: [],
      modalVisible: false,
      targetKeys: [],
      sourceKys:[],
      direction:"",     //移动方位
      moveKeys:[]       //移动的key
     
    }
    
    //初始化加载
    componentDidMount() {
      // this.getMock();
     
      const targetKeys = [];
      const dataSource = [];
      const {dispatch} = this.props;

      dispatch({
        type: 'personTempCfgMdl/selectLmporFactor',
        data:{}
      }).then(()=>{
        let dataArray = [];
        //数据集合
        dataArray = this.props.personTempCfgMdl.personData;
    
        for (let i = 0; i < dataArray.length; i++) {             
          const data = {
            key: dataArray[i].key,
            title: `${dataArray[i].title }`,
            description: `${dataArray[i].description}`,
            status:`${dataArray[i].status}`
          };
          //改变 targetKey
          if (data.status === "1") {
            targetKeys.push(dataArray[i].key);
          }
          dataSource.push(data);
        }

        this.setState({ dataSource, targetKeys });
      }).catch((e)=>{
        console.log(e);
      });

    }

   
    //实际添加请求
    handleAdd = fields => {
      const{ umssouserinfo:{currentUser} } = this.props;
      const{ targetKeys} = this.state;
      const createUser = {
        userCode: currentUser.principal.name,
      }
     
      //组装参数
      const data = {
        createUser:createUser.userCode,
        factorName: fields.factorName,
        remark:fields.remark
      }

      //发送请求
      this.props.dispatch({
        type: 'personTempCfgMdl/insertImportFactor',
        data:data
      }).then(()=>{
        const newData = [...this.state.dataSource];
        //获取新增数据,model传递的对象 
        let addData = [];
        addData = this.props.personTempCfgMdl.addData;
console.log('data类型是：' + typeof(addData) )
        for (let i = 0; i < addData.length; i++) {             
          const dataValue = {
            key: addData[i].key,
            title: `${addData[i].title }`,
            description: `${addData[i].description}`,
            status:`${addData[i].status}`
          };
          //改变 targetKey
          if (dataValue.status === "1") {
            targetKeys.push(addData[i].key);
          }
          newData.push(dataValue);         
        }
        //关闭模态窗，刷新左右数据
        this.setState({
          modalVisible: false,
          dataSource:newData,
          targetKeys 
        });
        message.success("新增成功");

      }).catch((e)=>{
        console.log(e);
      }); 
    };


    //拼装id
    transferIds = (sourceSelectedKeys) => {
      const sourceKys = [];
      for(let i = 0;i < sourceSelectedKeys.length; i ++ ){
        sourceKys.push(sourceSelectedKeys[i]);        
      } 
      this.setState({sourceKys});    
    }

    //删除因素
    deleteImprotFactor = () => {
      const {sourceKys} = this.state;
      let key = sourceKys.join(",");
      
      console.log('key的结果：'+ key + ',key的类型：' + typeof(key));      
      const data = {
        // id:sourceKys,
        id:key
      }

      if(sourceKys.length > 0 ){
         //请求
        this.props.dispatch({
          type: 'personTempCfgMdl/deleteImprotFactor',
          data: data
        }).then(()=>{
          const data = [...this.state.dataSource];
          //
          for(let j = 0;j<sourceKys.length;j++){
            //查找下标
            const index = data.findIndex(item => item.key === sourceKys[j]);
            data.splice(index,1);
          }
    
console.log('长度：'+ data.length);
          //单个id，可以用查找过滤的方法        
          //console.log('去掉之后剩下的结果是：'+ data.filter(item => item.key !== Number(key) ) )  
          //刷新 
          // this.setState({ dataSource: data.filter(item => item.key !== Number(key)) });
          this.setState({ dataSource: data });
          message.success("删除成功");
        }).catch((e)=>{
          console.log(e);
        }); 

      }     

    } 
    
    //保存配置实际调用的是修改,修改state=1
    updateImportFactor = () => {
      const{targetKeys,direction,moveKeys} = this.state;
      const {dispatch} = this.props;
      const { umssouserinfo:{currentUser} } = this.props;
      const updateUser = {
        userCode: currentUser.principal.name,
      }
console.log('key长度：' + moveKeys.join(",") + '移动方位字段类型是：' + typeof(direction) )

      var data = {};

      if( targetKeys.length  > 0 ){
        
        if(direction === "right"){
          data = {
            id:moveKeys.join(","),
            updatedUser:updateUser.userCode,
            status:"1"
          }

        }else if(direction === "left"){
          //把剔除的 key 置为失效
          data = {
            id:moveKeys.join(","),
            updatedUser:updateUser.userCode,
            status:"0"
          }

        }

        //请求
        dispatch({
          type: 'personTempCfgMdl/updateImportFactor',
          data: data
        }).then(()=>{
          // const newData = [...this.state.dataSource];
          // //刷新 
          // for (let i = 0; i < dataSource.length; i++) {
          //   const data = {
          //     key: dataSource[i].key,
          //     title: `${dataSource[i].title }`,
          //     description: `${dataSource[i].description}`,
          //     status:`${dataSource[i].status}`
          //   };
          //   if (data.status === "1") {
          //     targetKeys.push(dataSource[i].key);
          //   }
          //   newData.push(data);
          // }
          this.setState({  targetKeys });
          message.success("保存成功");
        }).catch((e)=>{
          console.log(e);
        });
         
     }  

    }

    //模态框隐藏
    handleModalVisible = flag => {
      this.setState({
        modalVisible: !!flag,
      });
    };

    //改变穿梭框
    handleChange = (targetKeys, direction, moveKeys) => {     
      this.setState({ targetKeys,direction,moveKeys });
    }

    
    //加载组件
    render() {
      //定义模态框的事件
      const parentMethods = {
        handleAdd: this.handleAdd,
        handleModalVisible: this.handleModalVisible,
      };

      let {modalVisible} = this.state;

      return (
        <PageHeaderLayout title="人员筛选外部因素导入模版配置">
          <Card bordered={false}>
            <div >
              <div >
                <Button
                  size="small"
                  style={{ float: 'left', margin: 5 }}
                  onClick={this.handleModalVisible}
                >
                  新增
                </Button>
                <Button
                  size="small"
                  style={{ float: 'left', margin: 5 }}
                  onClick={this.deleteImprotFactor}
                >
                  删除
                </Button>
                <Button
                  size="small"
                  style={{ float: 'left',position:'relative',left:'25%',margin: 5 }}
                  onClick={this.updateImportFactor}  
                >
                  保存配置
                </Button>
              </div><br/><br/>
              <Transfer
                dataSource={this.state.dataSource}
                showSearch
                listStyle={{
                  width: 280,
                  height: 360
                }}
                operations={['右移', '左移']}
                targetKeys={this.state.targetKeys}
                onChange={this.handleChange}
                render={item => `${item.description}`}
                footer={this.renderFooter}
                onSelectChange={this.transferIds}
              />
            </div>
            
          </Card>
          <CreateForm {...parentMethods} modalVisible={modalVisible} />
        </PageHeaderLayout>
      );
    }
  
}

