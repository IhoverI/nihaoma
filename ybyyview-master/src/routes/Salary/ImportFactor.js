import React, { PureComponent } from 'react';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { connect } from 'dva';
import { Spin } from 'antd';
import UploadForm from './FactorUpload';
import { Popconfirm, message, Checkbox, Card, Form, Row, Col, DatePicker, Select, Button, Table, Input, Upload, Icon, Modal } from 'snk-web'
const { MonthPicker,RangePicker } = DatePicker;
import moment from 'moment'
const formItemLayout = {
  labelCol: {
    sm: { span: 4 },
  },
  wrapperCol: {
    sm: { span: 20 },
  },
};

@connect(({shareModel, salaryModel ,loading}) => ({
  shareModel,
  salaryModel,
  loading: loading.models.salaryModel,
}))

@Form.create()
export default class ImportFactor extends PureComponent {

  state = {
    visible: false,
    uploadParamData: {},
    groupsOptionals: '',
    columns: [],
    pageNum: 1,
    pageSize: 10,
    data:[],
    modalVisible: false,
    loading: false,
    pagination:{current:1,pageSize:50,showSizeChanger:true,pageSizeOptions:['30','50','80','100']},
    fileList:[]
  }

  formItemLayout = {
    labelCol: {
      sm: { span: 4 },
    },
    wrapperCol: {
      sm: { span: 20 },
    },
  };

  // 点击查询
  queryHistory = () => {
    this.searchDate();
  }

  // 查询历史记录信息
  searchDate = () => {
    let { form, dispatch } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      console.log('searchDate fieldsValue...');
      console.log(fieldsValue);
      let arr = fieldsValue.calcMonth.format("YYYY-MM").split('-');
      if(arr && arr.length > 1){
        fieldsValue.calcYear = arr[0];
        fieldsValue.calcMonth = arr[1];
      }

      dispatch({
        type: 'salaryModel/searchFactoryHistory',
        data: fieldsValue,
      }).then(()=>{
        console.log('salaryModel.factoryHistoryList');
        console.log(this.props.salaryModel.factoryHistoryList);
        this.setState({data: this.props.salaryModel.factoryHistoryList});
      }).catch((e)=>{
        console.log(e);
      });
    });
  }

  // 清理薪酬计算的数据
  handleClearFactors = () => {
     const { dispatch, form } = this.props;

     form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      console.log('searchDate fieldsValue...');
      console.log(fieldsValue);
      let arr = fieldsValue.calcMonth.format("YYYY-MM").split('-');
      
      if(arr && arr.length > 1){
        fieldsValue.calcYear = arr[0];
        fieldsValue.calcMonth = arr[1];
      }
      
      dispatch({
        type: 'salaryModel/clearSalaryResult',
        data: fieldsValue,
      }).then((response)=>{
        if(response.code==0 )  message.info('清理成功');
        console.log('salaryModel.clearSalaryResult');
      }).catch((e)=>{
        console.log(e);
        message.error(e.message);
      });

     });
  }

  // 下载筛选模版 
  downloadFactors = () => {
    console.log('downloadFactors called...');
    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      console.log('searchDate fieldsValue...');
      console.log(fieldsValue);
      let arr = fieldsValue.calcMonth.format("YYYY-MM").split('-');
      if(arr && arr.length > 1){
        fieldsValue.calcYear = arr[0];
        fieldsValue.calcMonth = arr[1];
      }
      if(fieldsValue.policyCalcDate){
        fieldsValue.firstDay = fieldsValue.policyCalcDate[0].format('YYYY-MM-DD');
        fieldsValue.lastDay = fieldsValue.policyCalcDate[1].format('YYYY-MM-DD');
      }
      dispatch({
        type: 'salaryModel/downloadFactor',
        data: fieldsValue,
      }).then(()=>{
        // const groupsOptionals = this.props.shareModel.groupsData.map(item => <Select.Option key={item.id}>{item.groupName}</Select.Option>);
        // this.setState({columns: this.props.salaryModel.titleList});
      }).catch((e)=>{
        console.log(e);
      });

     });
  }

  exportPolicyExcel=()=>{
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      let data={}
      if(fieldsValue.policyCalcDate){
        data.firstDay = fieldsValue.policyCalcDate[0].format('YYYY-MM-DD');
        data.lastDay = fieldsValue.policyCalcDate[1].format('YYYY-MM-DD');
      }
      data.groupId=fieldsValue.groupId;
      dispatch({
        type: 'salaryModel/exportPolicyExcel',
        data: data,
      }).catch((e)=>{
        console.log(e);
      });
     });
  }
  
  // 薪酬计算
  handleCalculate = (e) =>{
   if(e)  e.preventDefault();
    const { form } = this.props;
    console.log('handleCalculate called...');
    form.validateFields((err, fieldsValue) => {
        if (err) {
          return;
        }
        let arr = fieldsValue.calcMonth.format("YYYY-MM").split('-');
        if(arr && arr.length > 1){
          fieldsValue.calcYear = arr[0];
          fieldsValue.calcMonth = arr[1];
        }else{
          message.error('请选择薪酬计算的日期');
          return;
        }
        if(fieldsValue.policyCalcDate){
          fieldsValue.policyCalcDate=[fieldsValue.policyCalcDate[0].format('YYYY-MM-DD'),
          fieldsValue.policyCalcDate[1].format('YYYY-MM-DD')]
        }
        
        // 组装薪酬计算请求报文
        let empIds = [];
        let empNames = [];
        const { data } = this.state;
        for(let i = 0; i < data.length; i ++){
          const emp = data[i];
          empIds.push(emp.empId);
          empNames.push(emp.empName);
        }
        let val = {};
        val.groupId = fieldsValue.groupId;
        val.calcYear = fieldsValue.calcYear;
        val.calcMonth = fieldsValue.calcMonth;
        val.firstDay = fieldsValue.policyCalcDate[0];
        val.lastDay = fieldsValue.policyCalcDate[1];
        val.empIds = empIds;
        val.empNames = empNames;

        // 如果已选择计算的人数为零，拒绝计算
        if(empIds && empIds.length == 0){
          message.error('请上传您需要计算的人员信息');
          return;
        }else{
          this.setState({loading: true});
        }

        const { dispatch } = this.props;
        dispatch({
          type: 'salaryModel/calculateSalary',
          data: val,
        }).then(()=>{
          // const groupsOptionals = this.props.shareModel.groupsData.map(item => <Select.Option key={item.id}>{item.groupName}</Select.Option>);
          // this.setState({columns: this.props.salaryModel.titleList});
          this.setState({loading: false});
          Modal.success({
            title: '薪酬计算',
            content: '薪酬计算成功！',
          });
        }).catch((e)=>{
          console.log(e);
          this.setState({loading: false});
          Modal.error({
            title: '薪酬计算',
            content: '薪酬计算失败：'+e.message,
          });;
        });
        //console.log(uploadParamData);
    });
  }

  dataSynchronize=(e)=>{
    if(e) e.preventDefault();
    const {form,dispatch} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      const calcMonth = fieldsValue.policyCalcDate;
      let data={}
      if(calcMonth){
        data={
          beginDate:calcMonth[0].format('YYYY-MM-DD'),
          endDate:calcMonth[1].format('YYYY-MM-DD')
        }
      }
    dispatch({
      type: 'salaryModel/updateBatchPolicy',
      data
    }).then(()=>{
      message.success('数据同步成功')
    }).catch((e)=>{
      console.log(e);
      message.error('数据同步失败')
    });
    })
  }


  setupColumns(){
       const columns = [{
        title: '员工姓名',
        dataIndex: 'empName',
        fixed: 'left',
        width:120
      }, {
        title: '团队名称',
        dataIndex: 'groupName',
        width:120
      }, {
        title: '薪酬年份',
        dataIndex: 'calcYear',
        width:100
      }, {
        title: '薪酬月份',
        dataIndex: 'calcMonth',
        width:100
      }, {
        title: '身份证号码',
        dataIndex: 'identityCode',
        width:180
      }, {
        title: '派遣公司',
        dataIndex: 'dispatchCompany',
        width:160
      }, {
        title: '组别名称',
        dataIndex: 'teamName',
        width:120
      }, {
        title: '内推',
        dataIndex: 'isRecommended',
        width:100,
      }, {
        title: '社保个人',
        dataIndex: 'salaryPersonalSb',
        width:120
      }, {
        title: '公积金个人',
        dataIndex: 'salaryPersonalGjj',
        width:120
      }, {
        title: '社保公司',
        dataIndex: 'salaryCpySb',
        width:120
      }, {
        title: '公积金公司',
        dataIndex: 'salaryCpyGjj',
        width:120
      }, {
        title: '激励方案兑现',
        dataIndex: 'salaryStimulate',
        width:120
      }, {
        title: '培训费',
        dataIndex: 'salaryTrain',
        width:120
      }, {
        title: '品质违规罚款',
        dataIndex: 'salaryQuality',
        width:120
      }, {
        title: '客诉扣罚',
        dataIndex: 'salaryComplaint',
        width:120
      }, {
        title: '派遣公司管理费用',
        dataIndex: 'salaryManage',
        width:120
      }, {
        title: '业务品质奖',
        dataIndex: 'salaryBusiness',
        width:120
      }, {
        title: '通勤车费代扣',
        dataIndex: 'salaryCar',
        width:120
      }, {
        title: '餐补',
        dataIndex: 'salarySupple',
        width:120
      }, {
        title: '內荐费',
        dataIndex: 'salaryRec',
        width:120
      }, {
        title: 'mbo系数(小数)',
        dataIndex: 'mbo',
        width:120
      }, {
        title: '工作日天数',
        dataIndex: 'workDay',
        width:120
      }, {
        title: '实际出勤天数',
        dataIndex: 'realWorkDay',
        width:120
      }, {
        title: '是否倒班(1是 0否)',
        dataIndex: 'isNight',
        width:120
      }, {
        title: '招聘人数',
        dataIndex: 'advNumber',
        width:120
      }, {
        title: '培训天数',
        dataIndex: 'trainDay',
        width:120
      }, {
        title: '旷工',
        dataIndex: 'absence',
        width:120
      }, {
        title: '年假',
        dataIndex: 'yearHoliday',
        width:120
      }, {
        title: '事假',
        dataIndex: 'affairHoliday',
        width:120
      }, {
        title: '病假',
        dataIndex: 'sickHoliday',
        width:120
      }, {
        title: '婚假',
        dataIndex: 'marriageHoliday',
        width:120
      }, {
        title: '丧假',
        dataIndex: 'loseHoliday',
        width:120
      }, {
        title: '陪产假',
        dataIndex: 'paternityHoliday',
        width:120
      }, {
        title: '产假',
        dataIndex: 'childBirthHoliday',
        width:120
      }, {
        title: '产检假',
        dataIndex: 'productionHoliday',
        width:120
      }, 
      {
        title: '生日假',
        dataIndex: 'birthHoliday',
        width:120
      }, {
        title: '迟到',
        dataIndex: 'arriveLate',
        width:120
      }, {
        title: '是否当月离职(1 是 0 否)',
        dataIndex: 'isNowMonthLeave',
        width:120
      }, {
        title: '年终奖',
        dataIndex: 'salaryYear',
        width:120
      }, {
        title: '年终奖个税',
        dataIndex: 'salaryYearTax',
        width:120
      }, {
        title: '是否长病假(1是0否)',
        dataIndex: 'isLongSick',
        width:120
      },
      {
        title: '培训期病假',
        dataIndex: 'trainSickHoliday',
        width:120
      },
      {
        title: '培训期事假',
        dataIndex: 'trainAffairHoliday',
        width:120
      },
      {
        title: '独子费',
        dataIndex: 'salarySingleChild',
        width:120
      },
      {
        title: '当月是否转岗',
        dataIndex: 'isChangePosition',
        width: '110px',
        editable:false,
        render:(text)=>text==1?'是':'否'
    },
  ];
      this.setState({columns: columns});
  }

  componentDidMount() {
    console.log('componentDidMount called..');
    const { dispatch } = this.props;
    // 初始化表格列
    this.setupColumns();

    // 获取团队
    dispatch({
      type: 'shareModel/searchGroups',
    }).then(()=>{
      const groupsOptionals = this.props.shareModel.groupsData.map(item => <Select.Option key={item.id}>{item.groupName}</Select.Option>);
      this.setState({groupsOptionals: groupsOptionals});
    }).catch((e)=>{
      console.log(e);
    });

  }

  pageOnChange=(pagination, filters, sorter)=>{
    this.setState({pagination})
}

//表单改变后清空表格数据
clearTable=()=>{
  this.setState({data:[]})
}


  render() {
    const { loading } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { columns } = this.state;
    const _this = this;
    const { data,uploadParamData }= this.state;
    let width = 0;
    width = columns.length * 300;

    const uploadParam = {
      // showUploadList:false,
      name: 'file',
      data: uploadParamData,
      action: `${SERVERHR}emp/importExcel`,
      withCredentials: true,
      beforeUpload: (file) => {
        const { form,shareModel } = this.props;
        let flag = true
        form.validateFields((err, fieldsValue) => {
          if (err) {
            flag = false
            return;
          }
          if (flag) {
            let arr = fieldsValue.calcMonth.format("YYYY-MM").split('-');
            if (arr && arr.length > 1) {
              fieldsValue.calcYear = arr[0];
              fieldsValue.calcMonth = arr[1];
            }
            const groupName = shareModel.groupsData.filter(e => e.id == fieldsValue.groupId)[0].groupName
            const uploadParamData = { groupId: fieldsValue.groupId, calcMonth: fieldsValue.calcMonth, calcYear: fieldsValue.calcYear, groupName };
            this.setState({ uploadParamData });
          }
        });
        console.log(flag)
          return flag;
      },
      onChange(info) {
          _this.setState({fileList:info.fileList})
        if (info.file.status === 'done') {
          if (info.file.response.code === 0) {
            const res = info.file.response;
            if(res && res.data){
              _this.setState({data: res.data});
            }
            message.success(`《${info.file.name}》: ${info.file.response.message}`,2);
          } else {
            message.error(`《${info.file.name}》上传失败: ${info.file.response.message}`, 4);
          }
          setTimeout(()=>{
          _this.setState({fileList:[]})
        },2000)
        } else if (info.file.status === 'error') {
          message.error(`《${info.file.name}》: file upload failed.`);
        }
      },

    };

    return (
      <PageHeaderLayout>
        <Spin tip="薪酬计算中..." spinning={this.state.loading}>
          <Card bordered={false}>
            <Form>
              <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="考核团队: ">
                    {getFieldDecorator(
                      'groupId',{
                      rules: [{required: true, message: `请选择团队`,}]
                    })
                      (<Select placeholder='请选择考核团队'  onChange={this.clearTable}

                      >
                        {this.state.groupsOptionals}
                      </Select>)
                    }
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="考核月份: ">
                    {getFieldDecorator(
                      'calcMonth',{
                      rules: [{required: true, message: `请输入考核月份`,}]
                    })
                      (<MonthPicker format={"YYYYMM"} allowClear={false} onChange={this.clearTable}
                        />)
                    }
                  </Form.Item>
                </Col>
                </Row>
              <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout} label="保费计算周期: ">
                    {getFieldDecorator(
                      'policyCalcDate',{initialValue: [moment().subtract(1, 'months').date(1),moment().date(0)],
                      rules: [{required: true, message: `保费计算周期`,},]
                    })
                      (<RangePicker format={"YYYYMMDD"} allowClear={false} onChange={this.clearTable}
                        />)
                    }
                  </Form.Item>
                </Col>
              </Row>
            
              <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
                <Col md={12} sm={24}> 
                  <Form.Item {...formItemLayout} >
                    {getFieldDecorator('lastMonthGjj')(
                      <Checkbox >保留上月的社保数据</Checkbox>
                    )}
                  </Form.Item>
                </Col>
                <Col md={12} sm={24}>
                  <Form.Item {...formItemLayout}>
                    {getFieldDecorator('lastMonthSb')(
                      <Checkbox>保留上月的公积金数据</Checkbox>
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Form>
            <div align='right' style={{marginBottom: 20}}>
              <Button style={{marginLeft: 20}} type='primary' icon='search' onClick={this.queryHistory} loading={loading}>查询</Button>
              <Button style={{marginLeft: 20}}  icon='download' onClick={this.exportPolicyExcel} loading={loading}>导出保费</Button>
            </div>

            <Table
              columns={columns}
              dataSource={data}
              bordered={true}
              loading={loading}
              size="small"
              scroll={{ x: 5500, y:400}}
              pagination={this.state.pagination}
              onChange={this.pageOnChange}
              >
            </Table>
            <br></br>
            <div align='right' style={{marginBottom: 20}}>
              <Popconfirm title="确定要清理吗?" onConfirm={this.handleClearFactors}  okText="是" cancelText="否">
                 <Button style={{marginRight: 20}} type='primary' >清理</Button>
              </Popconfirm>
              <Button style={{marginRight: 20}} type='primary' icon='download' onClick={this.downloadFactors}>下载</Button>
              <Button style={{marginRight: 20}} type='primary' onClick={this.dataSynchronize} >数据同步</Button>
              <Button style={{marginRight: 20}} type='primary' onClick={this.handleCalculate} >薪酬计算</Button>
              <Upload {...uploadParam} fileList={this.state.fileList}>
                <Button >
                  <Icon type="upload" />导入
                </Button>
              </Upload>
            </div>
          </Card>
        </Spin>
      </PageHeaderLayout>
    )
  }

}