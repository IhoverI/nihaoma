import { 
    Table, 
    Popconfirm,
    Row,
    Col,
    Card,
    Form,
    Input,
    Button,
    Steps,
    message,
    Select,
    DatePicker
} from 'snk-web';
import React from 'react';
import styles from './querySarlary.less';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
const Step = Steps.Step;



  //装饰器
  @connect(({umssouserinfo, sarlaryMdl,personModels,loading}) => ({
    umssouserinfo,
    sarlaryMdl,
    personModels,
    loading:loading.models.sarlaryMdl,
    
  }))
  
  
  @Form.create()
  export default class querySarlary extends React.PureComponent {

    constructor(props){
        super(props);
        this.state = {
            status:null,
            dataSource: [],
            pagination:{current:1,pageSize:50,showSizeChanger:true,pageSizeOptions:['30','50','80','100']}
        };
        this.columns = [
            {
                title: '员工姓名',
                dataIndex: 'empName',
                width: '120px',
                editable:false,
                fixed: 'left'
            },
            {
                title:"序号",
                dataIndex:"id",
                key:"id",
                width: '70px',
                render: (text, record,index) => {
                    return (
                      <span>
                        {index + 1}
                      </span>
                    );
                },   
            },
            {
                title: '系统调整',
                dataIndex: 'sysAdjust',
                width: '130px',
                editable: true,
                // className:styles.columnsBcolor
            },
            {
                title: '系统备注',
                dataIndex: 'sysRemark',
                width: '130px',
                editable:true,
                // className:styles.columnsBcolor
            },
            {
                title: '手工调整',
                dataIndex: 'adjust',
                width: '130px',
                editable: true,
                // className:styles.columnsBcolor
            },
            {
                title: '备注',
                dataIndex: 'remark',
                width: '130px',
                editable:true,
                // className:styles.columnsBcolor
            },
            {
                title: '团队名称',
                dataIndex: 'groupName',
                width: '120px',
                editable:false
            },
            {
                title: '工号',
                dataIndex: 'employeeCode',
                width: '120px',
                editable:false,
            },
            {
                title: '组别名称',
                dataIndex: 'teamName',
                width: '120px',
                editable:false,
            },
            {
                title: '职级',
                dataIndex: 'rankName',
                width: '120px',
                editable:false,
            },
            {
                title: '入职时间',
                dataIndex: 'entryDate',
                width: '140px',
                editable:false,
            },
            {
                title: '年限',
                dataIndex: 'workAge',
                width: '100px',
                editable:false,
            },
            {
                title: '离职时间',
                dataIndex: 'leaveDate',
                width: '150px',
                editable:false,
            },
            {
                title: '上线时间',
                dataIndex: 'onlineDate',
                width: '150px',
                editable:false,
            },
            {
                title: '身份证号码',
                dataIndex: 'identityCode',
                width: '170px',
                editable:false,
            },
            {
                title: '一卡通(银行卡号)',
                dataIndex: 'bankNo',
                width: '170px',
                editable:false,
            },
            {
                title: '请款业务',
                dataIndex: 'requestPrm',
                width: '110px',
                editable:false,
            },
            {
                title: '3月内退保',
                dataIndex: 'cancePrm',
                width: '110px',
                editable:false,
            },
            {
                title: '计薪保费',
                dataIndex: 'payPremium',
                width: '110px',
                editable:false,
            },
            {
                title: '基本工资',
                dataIndex: 'salaryBasic',
                width: '110px',
                editable:false,
            },
            {
                title: '职级津贴',
                dataIndex: 'salaryAllowance',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO奖',
                dataIndex: 'salaryMbo',
                width: '110px',
                editable:false,
            },
            {
                title: '提成',
                dataIndex: 'salaryPerform',
                width: '110px',
                editable:false,
            },
            {
                title: '新人津贴',
                dataIndex: 'salaryNew',
                width: '110px',
                editable:false,
            },
            {
                title: '服务津贴',
                dataIndex: 'salaryService',
                width: '110px',
                editable:false,
            },
            {
                title: '內荐费',
                dataIndex: 'salaryRec',
                width: '110px',
                editable:false,
            },
            {
                title: '激励方案兑换',
                dataIndex: 'salaryStimulate',
                width: '110px',
                editable:false,
            },
            {
                title: '出勤扣',
                dataIndex: 'salaryAttendance',
                width: '110px',
                editable:false,
            },
            {
                title: '餐补',
                dataIndex: 'salarySupple',
                width: '110px',
                editable:false,
            },
            {
                title: '通勤车费代扣',
                dataIndex: 'salaryCar',
                width: '110px',
                editable:false,
            },
            {
                title: '客诉处罚',
                dataIndex: 'salaryComplaint',
                width: '110px',
                editable:false,
            },
            {
                title: '品质违规罚款',
                dataIndex: 'salaryQuality',
                width: '110px',
                editable:false,
            },
            {
                title: '业务品质奖',
                dataIndex: 'salaryBusiness',
                width: '110px',
                editable:false,
            },
            {
                title: '培训费',
                dataIndex: 'salaryTrain',
                width: '110px',
                editable:false,
            },
            {
                title: '过节费',
                dataIndex: 'salaryFestival  ',
                width: '110px',
                editable:false,
            },
            {
                title: '员工应发工资(含税)',
                dataIndex: 'sumSalaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '应发工资',
                dataIndex: 'sumSalaryPay',
                width: '110px',
                editable:false,
            },
            {
                title: '社保个人',
                dataIndex: 'salaryPersonalSb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金个人',
                dataIndex: 'salaryPersonalGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '本月抵扣项',
                dataIndex: 'sumDeduction',
                width: '110px',
                editable:false,
            },
            {
                title: '累计应税工资',
                dataIndex: 'sumTaxSalary',
                width: '110px',
                editable:false,
            },
            {
                title: '累计至上月应缴所得税总额',
                dataIndex: 'sumPrepaidSalary',
                width: '110px',
                editable:false,
            },
            {
                title: '累计所得税',
                dataIndex: 'sumTax',
                width: '110px',
                editable:false,
            },
            {
                title: '累计至上月所得税合计',
                dataIndex: 'sumPrepaidTax',
                width: '110px',
                editable:false,
            },
            {
                title: '本月实扣个税',
                dataIndex: 'salaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '独子费',
                dataIndex: 'salarySingleChild',
                width: '110px',
                editable:false,
            },
            {
                title: '实发合计',
                dataIndex: 'salaryReal',
                width: '110px',
                editable:false,
            },
            {
                title: '社保公司',
                dataIndex: 'salaryCpySb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金公司',
                dataIndex: 'salaryCpyGjj',
                width: '110px',
                editable:false,
            },

            {
                title: '年终奖',
                dataIndex: 'salaryYear',
                width: '110px',
                editable:false,
            },
            {
                title: '年终奖个税',
                dataIndex: 'salaryYearTax',
                width: '110px',
                editable:false,
            },
            {
                title: '公司社保费用',
                dataIndex: 'salarySbGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '派遣公司管理费用',
                dataIndex: 'salaryManage',
                width: '110px',
                editable:false,
            },
            {
                title: '薪酬费用合计',
                dataIndex: 'sumSalaryFee',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO系数',
                dataIndex: 'mbo',
                width: '110px',
                editable:false,
            },
            {
                title: '本月工作日',
                dataIndex: 'workDay',
                width: '110px',
                editable:false,
            },

            {
                title: '实际出勤天数',
                dataIndex: 'realWorkDay',
                width: '110px',
                editable:false,
            },
            {
                title: '培训天数',
                dataIndex: 'trainDay',
                width: '110px',
                editable:false,
            },
            {
                title: '年假',
                dataIndex: 'yearHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '事假',
                dataIndex: 'affairHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '病假',
                dataIndex: 'sickHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '旷工',
                dataIndex: 'absence',
                width: '110px',
                editable:false,
            },
            
            {
                title: '婚假',
                dataIndex: 'marriageHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '丧假',
                dataIndex: 'loseHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '陪产假',
                dataIndex: 'paternityHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产假',
                dataIndex: 'childBirthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产检假',
                dataIndex: 'productionHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '生日假',
                dataIndex: 'birthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '迟到',
                dataIndex: 'arriveLate',
                width: '110px',
                editable:false,
            },
            {
                title: '是否当月离职',
                dataIndex: 'isNowMonthLeave',
                width: '110px',
                editable:false,
            },
            {
                title: '是否长病假',
                dataIndex: 'isLongSick',
                width: '110px',
                editable:false,
            },
            {
                title: '培训期病假',
                dataIndex: 'trainSickHoliday',
                width:120
              },
              {
                title: '培训期事假',
                dataIndex: 'trainAffairHoliday',
                width:120
              },
            {
                title: '当月是否转岗',
                dataIndex: 'isChangePosition',
                width: '110px',
                editable:false,
                render:(text)=>text==1?'是':'否'
            },
            {
                title: '保费计算周期',
                width: '200px',
                editable:false,
                render:(text,row)=>row.firstDay&&row.lastDay?row.firstDay+'~'+row.lastDay:''
            }
        ];
    }
  
    componentDidMount() {
        const { dispatch} = this.props;
        //初始加载团队名称
        const reqBody = {
            data: "",
        }
        dispatch({
            type: 'personModels/selectGroupName',
            payload: reqBody,
        })

        //初始化加载岗位
        dispatch({
            type: 'sarlaryMdl/searchPositions',
            data: "",
        })
    }

    // 下载筛选模版 
    downloadFactors = () => {
        console.log('downloadFactors called...');
        const { dispatch, form } = this.props;

        form.validateFields((err, fieldsValue) => {
            console.log('downloadFactors fieldsValue...');
            
            if (err) {
                return;
            }
            let arr = fieldsValue.calcMonth.format("YYYY-MM").split('-');
            if(arr && arr.length > 1){
                fieldsValue.calcYear = arr[0];
                fieldsValue.calcMonth = arr[1];
            }
            console.log(fieldsValue);
            dispatch({
                type: 'sarlaryMdl/exportSalaryResult',
                data: fieldsValue,
            }).catch((e)=>{
                console.log(e);
            });
        });
    }
  

  

    //重置表单
    handleReset = () => {
        this.props.form.resetFields();
    }

    // 根据团队查询组别列表
    groupNameSelect = (value) => {
        console.log('groupNameSelect called..');
        console.log(value);
        if (value) {
            let reqBody = {
                data: {groupId: value},
            }
            this.props.dispatch({
                type: 'personModels/selectTeamName',
                payload: reqBody,
            })
        }
    }

    pageOnChange=(pagination, filters, sorter)=>{
        this.setState({pagination})
    }

    //查询
    query = () => {
        console.log('searchData called..');
        const { form, dispatch } = this.props;
        form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }
            let arr = fieldsValue.calcMonth.format("YYYY-MM").split('-');
            if(arr && arr.length > 1){
                fieldsValue.calcYear = arr[0];
                fieldsValue.calcMonth = arr[1];
            }
            console.log(fieldsValue);
            dispatch({
                type: 'sarlaryMdl/querySalaryResult',
                data: fieldsValue,
            }).then(()=>{
                this.setState({dataSource: this.props.sarlaryMdl.allSalary});
                if(this.props.sarlaryMdl.allSalary.length>0){
                    this.setState({status:this.props.sarlaryMdl.allSalary[0].status})
                }
            }).catch((e)=>{
                console.log(e);
            });
        });
      }
    
    //加载组件
    render() {
        //引入state
        const {loading,personModels,form} = this.props;
        const { MonthPicker } = DatePicker;
        const {dataSource} = this.state;
        const { getFieldDecorator } = form;

        // 团队下拉列表
        let groupNames = personModels.groupNames.data || [];
        let groupChildren = [];
        for (let i = 0; i < groupNames.length; i++) {
            groupChildren.push(<Select.Option key={groupNames[i].id} value={groupNames[i].id}>{groupNames[i].groupName}</Select.Option>)
        }

        // 组别下拉列表
        let teamNames = personModels.teamNames.data || [];
        let teamChildren = [];
        if (form.getFieldValue('groupId')) {
            for (let i = 0; i < teamNames.length; i++) {
                teamChildren.push(<Select.Option key={teamNames[i].id} value={teamNames[i].id}>{teamNames[i].teamName}</Select.Option>)
            }
        }

        //岗位下拉
        let positions = this.props.sarlaryMdl.positionData || [];  
      
        let positionChildren = [];
        for (let i = 0; i < positions.length; i++) {
            positionChildren.push(<Select.Option key={positions[i].id} value={positions[i].id}>{positions[i].positionName}</Select.Option>)
        }

          
        return (
            <PageHeaderLayout>
            <Card >
              <Card>
                <Form  className={styles.serchForm}>
                    <Row gutter={30}>
                        <Col span={8} >
                            <Form.Item  label="团队名称">
                            {getFieldDecorator('groupId',{
                                rules: [{ required: true, message: '请选择团队!' }],
                            })(
                                <Select placeholder='请选择' allowClear 
                                onChange={this.groupNameSelect}>
                                {groupChildren}
                                </Select>
                            )}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item  label="组别名称">
                            {getFieldDecorator('teamId')(
                                <Select placeholder='请选择' allowClear>
                                {teamChildren}
                                </Select>
                            )}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item  label="岗位">
                                {getFieldDecorator('positionId')(
                                    <Select placeholder='请选择' allowClear>
                                    {positionChildren}
                                    </Select>
                                )}
                            </Form.Item>
                        </Col>  
                    </Row>

                    <Row gutter={30}>
                        <Col span={8}>
                            <Form.Item  label="员工姓名">
                                {getFieldDecorator('empName')(
                                    <Input placeholder='请输入'/>
                                )}
                            </Form.Item>
                        </Col> 


                       <Col span={8}>
                            <Form.Item  label="考核月份: ">
                                {getFieldDecorator(
                                'calcMonth',{
                                rules: [{required: true, message: `请输入考核月份`,}]
                                })
                                (<MonthPicker  format={"YYYYMM"}
                                    />)
                                }
                            </Form.Item>
                        </Col>
                        <Col span={8} >
                            <Button style={{ marginTop: 36 }} onClick={this.query} type="primary" htmlType="submit">搜索</Button>
                            <Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
                                重置
                            </Button>   
                        </Col>
                    </Row>
                </Form>
              </Card>  
              <Card type="inner" bordered={false}>
                <div className={styles.tableList}>
                  <Table
                  title={() =>  <div>
                    <Steps current={Number(this.state.status)} size='small'>
                        <Step title="薪酬修改"  />
                        <Step title="人事主任复核"  />
                        <Step title="财务初审"  />
                        <Step title="财务主任复核"  />
                        <Step title="完成"  />
                        <Step title="已打回"  />
                    </Steps>
                </div>}
                  size="small"
                    loading={loading}
                    dataSource={dataSource}
                    state={dataSource}
                    columns={this.columns}
                    pagination={this.state.pagination}
                    onChange={this.pageOnChange}
                    scroll={{ x: 8000,y:400 }}
                    // pagination={false}
                    />
                </div><br/><br/><br/>
                <div className={styles.tableListForm}>
                    <Button style={{marginRight: 20}} type='primary' icon='download' onClick={this.downloadFactors}>下载</Button>
                </div>
              </Card>
            </Card>
            </PageHeaderLayout>
        );
      }
}
  
  
  
  