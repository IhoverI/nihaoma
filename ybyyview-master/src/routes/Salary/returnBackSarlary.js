import { 
    Table, 
    Popconfirm,
    Row,
    Col,
    Card,
    Form,
    Input,
    Select,
    Icon,
    Button,
    InputNumber,
    message
} from 'snk-web';
import React from 'react';
import styles from './querySarlary.less';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
 

const FormItem = Form.Item;
//table 行编辑的属性定义
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

//table 单元格的定义
class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (      
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [{
                      // required: true,
                      message: `请输入 ${title}!`,
                    }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>
               
          );
        }}
      </EditableContext.Consumer>
    );
  }

}


  //装饰器
  @connect(({umssouserinfo, sarlaryMdl,loading}) => ({
    umssouserinfo,
    sarlaryMdl,
    loading:loading.models.sarlaryMdl,
  }))
  
  
  @Form.create()
  export default class editSarlary extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            dataSource: [],
            selectedRows: [],
            editingKey: '',
            list:[],
            params:{},
            pagination:{current:1,pageSize:50,showSizeChanger:true,pageSizeOptions:['30','50','80','100']}
            
        };
        this.columns = [
            {
                title: '员工姓名',
                dataIndex: 'empName',
                width: '120px',
                editable:false,
                fixed: 'left'
            },
            {
                title: '操作',
                dataIndex: 'operation',
                width: '120px',
                render: (text, record) => {
                  const editable = this.isEditing(record);                
                  return (
                    <div>
                      {editable ? (
                        <span>
                        <EditableContext.Consumer>
                          {form => (
                            <a
                              href="javascript:;"
                              onClick={() => this.save(form, record.key)}
                              style={{ marginRight: 8 }}
                            >
                              保存
                            </a>
                          )}
                        </EditableContext.Consumer>
                          <Popconfirm
                            title="确定放弃编辑?"
                            onConfirm={() => {                     
                              this.cancel(record.key)
                            }}
                          >
                            <a style={{ marginRight: 8 }}>取消</a>
                          </Popconfirm>
                      </span>
                      ) : (
                        <span>
                           <a onClick={() => this.edit(record)}>编辑</a>
                        </span>
                      )}
                    </div>
                  );
                },
            },
            {
                title:"序号",
                dataIndex:"id",
                key:"id",
                width:'70px',
                render: (text, record,index) => {
                    return (
                        <span>
                          {(this.state.pagination.current-1)*this.state.pagination.pageSize+index+1}
                        </span>
                      );
                },   
            },
            {
                title: '系统调整',
                dataIndex: 'sysAdjust',
                width: '130px',
                editable: true,
                // className:styles.columnsBcolor
            },
            {
                title: '系统备注',
                dataIndex: 'sysRemark',
                width: '130px',
                editable:true,
                // className:styles.columnsBcolor
            },
            {
                title: '手工调整',
                dataIndex: 'adjust',
                width: '120px',
                editable: true,
                className:styles.columnsBcolor
            },
            {
                title: '备注',
                dataIndex: 'remark',
                width: '120px',
                editable:true,
                className:styles.columnsBcolor
            },
            {
                title: '操作',
                dataIndex: 'status',
                width: '110px',
                editable: false,
                // className:styles.columnsBcolor,
                render: () => {
                    return (
                    <div>
                        {/* <Select style={{ width: 100 }} 
                            onChange={(recordSelected) => this.handleDefaultChange(recordSelected, record)}
                            defaultValue = '6' 
                            >
                                <Select.Option value='0'>通过</Select.Option>
                                <Select.Option value='6'>打回</Select.Option>
                        </Select> */}
                        <span>打回</span>
                    </div>
                    )
                },
            },
            {
                title: '原因',
                dataIndex: 'approveRemark',
                width: '120px',
                editable:false,
                // className:styles.columnsBcolor
            },
            {
                title: '团队名称',
                dataIndex: 'groupName',
                width: '120px',
                editable:false
            },
            {
                title: '工号',
                dataIndex: 'employeeCode',
                width: '120px',
                editable:false,
            },
            {
                title: '组别名称',
                dataIndex: 'teamName',
                width: '120px',
                editable:false,
            },
            {
                title: '职级',
                dataIndex: 'rankName',
                width: '120px',
                editable:false,
            },
            {
                title: '入职时间',
                dataIndex: 'entryDate',
                width: '140px',
                editable:false,
            },
            {
                title: '年限',
                dataIndex: 'workAge',
                width: '100px',
                editable:false,
            },
            {
                title: '离职时间',
                dataIndex: 'leaveDate',
                width: '150px',
                editable:false,
            },
            {
                title: '上线时间',
                dataIndex: 'onlineDate',
                width: '150px',
                editable:false,
            },
            {
                title: '身份证号码',
                dataIndex: 'identityCode',
                width: '170px',
                editable:false,
            },
            {
                title: '一卡通(银行卡号)',
                dataIndex: 'bankNo',
                width: '170px',
                editable:false,
            },
            {
                title: '请款业务',
                dataIndex: 'requestPrm',
                width: '110px',
                editable:false,
            },
            {
                title: '3月内退保',
                dataIndex: 'cancePrm',
                width: '110px',
                editable:false,
            },
            {
                title: '计薪保费',
                dataIndex: 'payPremium',
                width: '110px',
                editable:false,
            },
            {
                title: '基本工资',
                dataIndex: 'salaryBasic',
                width: '110px',
                editable:false,
            },
            {
                title: '职级津贴',
                dataIndex: 'salaryAllowance',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO奖',
                dataIndex: 'salaryMbo',
                width: '110px',
                editable:false,
            },
            {
                title: '提成',
                dataIndex: 'salaryPerform',
                width: '110px',
                editable:false,
            },
            {
                title: '新人津贴',
                dataIndex: 'salaryNew',
                width: '110px',
                editable:false,
            },
            {
                title: '服务津贴',
                dataIndex: 'salaryService',
                width: '110px',
                editable:false,
            },
            {
                title: '內荐费',
                dataIndex: 'salaryRec',
                width: '110px',
                editable:false,
            },
            {
                title: '激励方案兑换',
                dataIndex: 'salaryStimulate',
                width: '110px',
                editable:false,
            },
            {
                title: '出勤扣',
                dataIndex: 'salaryAttendance',
                width: '110px',
                editable:false,
            },
            {
                title: '餐补',
                dataIndex: 'salarySupple',
                width: '110px',
                editable:false,
            },
            {
                title: '通勤车费代扣',
                dataIndex: 'salaryCar',
                width: '110px',
                editable:false,
            },
            {
                title: '客诉扣罚',
                dataIndex: 'salaryComplaint',
                width: '110px',
                editable:false,
            },
            {
                title: '品质违规罚款',
                dataIndex: 'salaryQuality',
                width: '110px',
                editable:false,
            },
            {
                title: '业务品质奖',
                dataIndex: 'salaryBusiness',
                width: '110px',
                editable:false,
            },
            {
                title: '培训费',
                dataIndex: 'salaryTrain',
                width: '110px',
                editable:false,
            },
            {
                title: '过节费',
                dataIndex: 'salaryFestival  ',
                width: '110px',
                editable:false,
            },
            {
                title: '员工应发工资(含税)',
                dataIndex: 'sumSalaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '应发工资',
                dataIndex: 'sumSalaryPay',
                width: '110px',
                editable:false,
            },
            {
                title: '社保个人',
                dataIndex: 'salaryPersonalSb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金个人',
                dataIndex: 'salaryPersonalGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '所得税',
                dataIndex: 'salaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '独子费',
                dataIndex: 'salarySingleChild',
                width: '110px',
                editable:false,
            },
            {
                title: '实发合计',
                dataIndex: 'salaryReal',
                width: '110px',
                editable:false,
            },
            {
                title: '社保公司',
                dataIndex: 'salaryCpySb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金公司',
                dataIndex: 'salaryCpyGjj',
                width: '110px',
                editable:false,
            },

            {
                title: '年终奖',
                dataIndex: 'salaryYear',
                width: '110px',
                editable:false,
            },
            {
                title: '年终奖个税',
                dataIndex: 'salaryYearTax',
                width: '110px',
                editable:false,
            },
            {
                title: '公司社保费用',
                dataIndex: 'salarySbGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '派遣公司管理费用',
                dataIndex: 'salaryManage',
                width: '110px',
                editable:false,
            },
            {
                title: '薪酬费用合计',
                dataIndex: 'sumSalaryFee',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO系数',
                dataIndex: 'mbo',
                width: '110px',
                editable:false,
            },
            {
                title: '本月工作日',
                dataIndex: 'workDay',
                width: '110px',
                editable:false,
            },

            {
                title: '实际出勤天数',
                dataIndex: 'realWorkDay',
                width: '110px',
                editable:false,
            },
            {
                title: '培训天数',
                dataIndex: 'trainDay',
                width: '110px',
                editable:false,
            },
            {
                title: '年假',
                dataIndex: 'yearHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '事假',
                dataIndex: 'affairHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '病假',
                dataIndex: 'sickHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '旷工',
                dataIndex: 'absence',
                width: '110px',
                editable:false,
            },
            
            {
                title: '婚假',
                dataIndex: 'marriageHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '丧假',
                dataIndex: 'loseHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '陪产假',
                dataIndex: 'paternityHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产假',
                dataIndex: 'childBirthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产检假',
                dataIndex: 'productionHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '生日假',
                dataIndex: 'birthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '迟到',
                dataIndex: 'arriveLate',
                width: '110px',
                editable:false,
            },
            {
                title: '是否当月离职',
                dataIndex: 'isNowMonthLeave',
                width: '110px',
                editable:false,
            },
            {
                title: '是否长病假',
                dataIndex: 'isLongSick',
                width: '110px',
                editable:false,
            },
            {
                title: '培训期病假',
                dataIndex: 'trainSickHoliday',
                width:120
              },
              {
                title: '培训期事假',
                dataIndex: 'trainAffairHoliday',
                width:120
              },
            {
                title: '当月是否转岗',
                dataIndex: 'isChangePosition',
                width: '110px',
                editable:false,
                render:(text)=>text==1?'是':'否'
            },
            
        ];
    }
  
    componentWillMount(){
        const dom = document.getElementsByClassName('ant-btn ant-btn-primary')
        console.log(dom)
       if(dom.length>0){
        dom[0].click()
       }
    }
    componentDidMount() {
        const { dispatch, umssouserinfo:{currentUser} } = this.props;
        let search = this.props.location.search; 
        var params = [];
        //?groupId=2&calcMonth=7&calcYear=2018
        params= search.slice(1).split('&');        
        
        var pData = [];
        for(let i=0;i<params.length;i++){
            pData.push(params[i].split('=')[1]);
        }
        //参数
        const paramData = {
            groupId:pData[0],
            calcMonth:pData[1],
            calcYear:pData[2],
            status:'6'
        };  
        console.log(paramData);
        dispatch({
            type: 'sarlaryMdl/querySalaryRTData',
            data:paramData
        }).then(()=>{
            const newData = [...this.props.sarlaryMdl.sarlaryRTdata];      
            console.log(newData);     
            if(newData){
                const status = Number(newData[0].status - 1).toString();                 
                this.setState({dataSource: newData,status:status,params:paramData});
            }
        }).catch((e)=>{
            console.log(e);
        });

    }
  
    

    //table 行编辑事件
    isEditing = (record) => {     
        return record.key === this.state.editingKey;   
    };

    //编辑
    edit(record){
        const {editingKey} = this.state;
        if(editingKey != ''){
          message.error("你有规则正在编辑状态，请先处理后再编辑！")      
        }else{
          this.setState({ editingKey: record.key, editingRecord: record});
        }
    }

    //保存
    save(form, key) {
        form.validateFields((error, row) => {
          if (error) {
            return;
          }

          //定义list对象
          const list = [...this.state.list];
          const objectArg = {
              id:key,
              adjust:row.adjust,
              remark:row.remark
          }
          list.push(objectArg);          

          const newData = [...this.state.dataSource];
          const index = newData.findIndex(item => key === item.key);
          if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, {
              ...item,
              ...row,
            });
            this.setState({ dataSource: newData, editingKey: '',list });
          } else {
            newData.push(row);
            this.setState({ dataSource: newData, editingKey: '',list });
          }
        });
      }

    cancel = () => {
        this.setState({ editingKey: '' });
    };

    pageOnChange=(pagination, filters, sorter)=>{
        this.setState({pagination})
    }

    handleConfirmSubmit = () =>{
        //获取数据源
        const {dataSource,params} = this.state;
        const { dispatch } = this.props;

        // status !== '6'? (Number(status) + 1).toString():status;
        // console.log('现在的状态是：'+status)    
        var statusVal = '';
        dataSource.forEach(element => {
            if(element.status === '6'){
                statusVal = element.status;
                return;
            }
        });

        //请求参数
        const paramData = {
            calcMonth:params.calcMonth,
            calcYear:params.calcYear,
            groupId:params.groupId,
            groupName:params.groupName,
            list:this.state.list
        }

        dispatch({
            type: 'sarlaryMdl/updateSalaryResult',
            data: paramData
        }).then(()=>{
            dispatch({
                type: 'sarlaryMdl/querySalaryRTData',
                data: params
            }).then(()=>{
                this.setState({dataSource: this.props.sarlaryMdl.sarlaryRTdata});
            }).catch((e)=>{
                console.log(e);
            });
            message.success("提交审核成功");
        }).catch((e)=>{
            console.log(e);
        });
    }

    //加载组件
    render() {
        
        const {loading} = this.props;
        const {dataSource,selectedRows} = this.state;

        //引入table列和编辑
        const columns = this.columns.map((col) => {
            if (!col.editable) {
            return col;
            }
            return {
            ...col,
            onCell: record => ({
                record,
                inputType: col.dataIndex,
                dataIndex: col.dataIndex,
                title: col.title,
                editing: this.isEditing(record),
            }),
            };
        });


        //组件组合
        const components = {
            body: {
              row: EditableFormRow,
              cell: EditableCell,
            },
          };
        
        return (
            <PageHeaderLayout>
              <Card title="薪酬计算的结果" bordered={false}>
                <div className={styles.tableList}>
                  <Table
                    size="small"
                    components={components}
                    loading={loading}
                    dataSource={dataSource}
                    state={dataSource}
                    columns={columns}
                    pagination={this.state.pagination}
                    onChange={this.pageOnChange}
                    scroll={{ x: 7000,y:400 }}
                    rowClassName={record=>{return record.status==6?styles.rowBcColor:null}}
                    />
                </div><br/><br/>
                <div className={styles.tableListForm}>
                    <Button type="primary" onClick={this.handleConfirmSubmit}>
                        再次提交
                    </Button>
                  </div>
              </Card>
              </PageHeaderLayout>
        );
      }
}
  