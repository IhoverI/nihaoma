
import { 
    Table, 
    Popconfirm,
    Row,
    Col,
    Card,
    Form,
    Input,
    Select,
    Icon,
    Button,
    InputNumber,
    message
} from 'snk-web';
import React from 'react';
import styles from './querySarlary.less';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
 

const FormItem = Form.Item;
//table 行编辑的属性定义
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

//table 单元格的定义
class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType == 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (      
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    // rules: [{
                    //   // required: true,
                    //   message: `请输入 ${title}!`,
                    // }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>
               
          );
        }}
      </EditableContext.Consumer>
    );
  }

}


  //装饰器
  @connect(({umssouserinfo, sarlaryMdl,loading}) => ({
    umssouserinfo,
    sarlaryMdl,
    loading:loading.models.sarlaryMdl,
  }))
  
  
  @Form.create()
  export default class editSarlary extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            dataSource: [],
            editingKey: '',
            list:[],
            params:{},
            pagination:{current:1,pageSize:10,showSizeChanger:true,pageSizeOptions:['10','20','30','50']},
            searchText: '',
            filtered: false,
            tableList:[]
        };
      
   }
  
    componentDidMount() {
        this.fetch()
    }
    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
      }

      reseSearch=()=>{
        this.setState({ searchText: ''},()=>this.onSearch());
      }
      onSearch = () => {
        const { searchText } = this.state;
        const reg = new RegExp(searchText, 'gi');
        this.setState({
          filterDropdownVisible: false,
          filtered: !!searchText,
          dataSource: this.state.tableList.map((record) => {
            const match = record.empName.match(reg);
            if (!match) {
              return null;
            }
            return {
              ...record,
              empName: (
                <span>
                  {record.empName.split(reg).map((text, i) => (
                    i > 0 ? [<span className={styles.highlight}>{match[0]}</span>, text] : text
                  ))}
                </span>
              ),
            };
          }).filter(record => !!record),
        });
      }

    fetch=()=>{
        const { dispatch } = this.props;
        //使用this.props.location.params接收值,ApprovedList.js 传递过来的
        const params = this.props.location.params;   
        if(params) {
            params.id=null  
            dispatch({
                type: 'sarlaryMdl/queryPrivSalaryResult',
                data:params
            }).then(()=>{
                this.setState({dataSource: this.props.sarlaryMdl.sarlaryData,params:params,
                                tableList:this.props.sarlaryMdl.sarlaryData});
            }).catch((e)=>{
                message.error('error')
                console.log(e);
            });
        }else{
          //刷新调回审核页面
          dispatch(routerRedux.push('/salary/approvedList'))
        }
    }
  
    

    //table 行编辑事件
    isEditing = (record) => {     
        return record.key === this.state.editingKey;   
    };

    //编辑
    edit(record){
        const {editingKey} = this.state;
        if(editingKey != ''){
          message.error("你有规则正在编辑状态，请先处理后再编辑！")      
        }else{
          this.setState({ editingKey: record.key, editingRecord: record});
        }
    }

    //保存
    save(form, key) {
        form.validateFields((error, row) => {
            const{adjust,remark} = row
            console.log(row)
          if (error) {
            return;
          }
          if(adjust!=0&&!remark){
              message.warn('当调整金额不为0时，备注必填')
              return
          }

          //定义list对象
          const objectArg = {
              id:key,
              adjust:row.adjust,
              remark:row.remark
          }
          const newList = [...this.state.list];
          const listIndex = newList.findIndex(item => key == item.id);
          if(listIndex>-1){
            newList[listIndex] = objectArg
          }else{
            newList.push(objectArg);          
          }
          

          const newData = [...this.state.dataSource];
          const index = newData.findIndex(item => key === item.key);
          if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, {
              ...item,
              ...row,
            });
          } else {
            newData.push(row);
        }
        this.setState({ dataSource: newData, editingKey: '',list:newList });
        console.log(newList)
        });
      }

    cancel = () => {
        this.setState({ editingKey: '' });
    };

    pageOnChange=(pagination, filters, sorter)=>{
        this.setState({pagination})
    }

    handleConfirmSubmit = () =>{
        const {editingKey} = this.state;
        if(editingKey != ''){
          message.error("你有规则正在编辑状态，请先处理后再提交！")
        return
    }
        //获取数据源
        const {params} = this.state;
        const { dispatch } = this.props;

        //请求参数
        const paramData = {
            calcMonth:params.calcMonth,
            calcYear:params.calcYear,
            groupId:params.groupId,
            groupName:params.groupName,
            list:this.state.list
        }

        dispatch({
            type: 'sarlaryMdl/updateSalaryResult',
            data: paramData
        }).then(()=>{
            dispatch({
                type: 'sarlaryMdl/queryPrivSalaryResult',
                data:params
            }).then(()=>{
                this.setState({dataSource: this.props.sarlaryMdl.sarlaryData});
            }).catch((e)=>{
                console.log(e);
            });
            message.success("修改成功");
            this.setState({list:[]})
        }).catch((e)=>{
           message.error(e)
        });
    }

    approvedClick = () => {
        const { dispatch } = this.props
        console.log()
        const param = {...this.props.location.params,status:"1"}
        dispatch({
          type: 'sarlaryMdl/updateStatus',
          payload: { ...param }
        }).then(reponse=>{
           if(reponse.code==0){
            this.fetch()
           } 
        }
        )
      }

    //加载组件
    render() {
        
        const {loading} = this.props;
        const {dataSource} = this.state;

        const column = [
            {
                title: '员工姓名',
                dataIndex: 'empName',
                width: '100px',
                editable:false,
                fixed: 'left',
                key: 'empName',
                filterDropdown: (
                    <div className={styles.customfilterdropdown}>
                      <Input
                        ref={ele => this.searchInput = ele}
                        placeholder="Search name"
                        value={this.state.searchText}
                        onChange={this.onInputChange}
                        onPressEnter={this.onSearch}
                      />
                      <Button type="primary" size={"small"} onClick={this.onSearch}>搜索</Button>
                      <Button type="primary"  size={"small"} style={{marginLeft:12}} onClick={this.reseSearch}>重置</Button>

                    </div>
                  ),
                  filterIcon: <Icon type="search" style={{ color: this.state.filtered ? '#108ee9' : '#aaa' }} />,
                  filterDropdownVisible: this.state.filterDropdownVisible,
                  onFilterDropdownVisibleChange: (visible) => {
                    this.setState({
                      filterDropdownVisible: visible,
                    }, () => this.searchInput.focus());
                  },
            },
            {
                title: '操作',
                dataIndex: 'operation',
                width: '100px',
                render: (text, record) => {
                  const editable = this.isEditing(record);                
                  return (
                    <div>
                      {editable ? (
                        <span>
                        <EditableContext.Consumer>
                          {form => (
                            <a
                              href="javascript:;"
                              onClick={() => this.save(form, record.key)}
                              style={{ marginRight: 8 }}
                            >
                              保存
                            </a>
                          )}
                        </EditableContext.Consumer>
                          <Popconfirm
                            title="确定放弃编辑?"
                            onConfirm={() => {                     
                              this.cancel(record.key)
                            }}
                          >
                            <a style={{ marginRight: 8 }}>取消</a>
                          </Popconfirm>
                      </span>
                      ) : (
                        <span>
                           <a onClick={() => this.edit(record)}>编辑</a>
                        </span>
                      )}
                    </div>
                  );
                },
            },
            {
                title:"序号",
                dataIndex:"id",
                key:"id",
                width:'60px',
                render: (text, record,index) => {
                    return (
                        <span>
                          {(this.state.pagination.current-1)*this.state.pagination.pageSize+index+1}
                        </span>
                      );
                },   
            },
            {
                title: '系统调整',
                dataIndex: 'sysAdjust',
                width: '100px',
                editable: false,
                // className:styles.columnsBcolor
            },
            {
                title: '系统备注',
                dataIndex: 'sysRemark',
                width: '180px',
                editable:false,
                // className:styles.columnsBcolor
            },
            {
                title: '手工调整',
                dataIndex: 'adjust',
                width: '180px',
                editable: true,
            },
            {
                title: '备注',
                dataIndex: 'remark',
                width: '130px',
                editable:true,
            },
            {
                title: '团队名称',
                dataIndex: 'groupName',
                width: '120px',
                editable:false
            },
            {
                title: '工号',
                dataIndex: 'employeeCode',
                width: '120px',
                editable:false,
            },
            {
                title: '组别名称',
                dataIndex: 'teamName',
                width: '120px',
                editable:false,
            },
            {
                title: '职级',
                dataIndex: 'rankName',
                width: '120px',
                editable:false,
            },
            {
                title: '入职时间',
                dataIndex: 'entryDate',
                width: '140px',
                editable:false,
            },
            {
                title: '年限',
                dataIndex: 'workAge',
                width: '100px',
                editable:false,
            },
            {
                title: '离职时间',
                dataIndex: 'leaveDate',
                width: '150px',
                editable:false,
            },
            {
                title: '上线时间',
                dataIndex: 'onlineDate',
                width: '150px',
                editable:false,
            },
            {
                title: '身份证号码',
                dataIndex: 'identityCode',
                width: '170px',
                editable:false,
            },
            {
                title: '一卡通(银行卡号)',
                dataIndex: 'bankNo',
                width: '170px',
                editable:false,
            },
            {
                title: '请款业务',
                dataIndex: 'requestPrm',
                width: '110px',
                editable:false,
            },
            {
                title: '3月内退保',
                dataIndex: 'cancePrm',
                width: '110px',
                editable:false,
            },
            {
                title: '计薪保费',
                dataIndex: 'payPremium',
                width: '110px',
                editable:false,
            },
            {
                title: '基本工资',
                dataIndex: 'salaryBasic',
                width: '110px',
                editable:false,
            },
            {
                title: '职级津贴',
                dataIndex: 'salaryAllowance',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO奖',
                dataIndex: 'salaryMbo',
                width: '110px',
                editable:false,
            },
            {
                title: '提成',
                dataIndex: 'salaryPerform',
                width: '110px',
                editable:false,
            },
            {
                title: '新人津贴',
                dataIndex: 'salaryNew',
                width: '110px',
                editable:false,
            },
            {
                title: '服务津贴',
                dataIndex: 'salaryService',
                width: '110px',
                editable:false,
            },
            {
                title: '內荐费',
                dataIndex: 'salaryRec',
                width: '110px',
                editable:false,
            },
            {
                title: '激励方案兑换',
                dataIndex: 'salaryStimulate',
                width: '110px',
                editable:false,
            },
            {
                title: '出勤扣',
                dataIndex: 'salaryAttendance',
                width: '110px',
                editable:false,
            },
            {
                title: '餐补',
                dataIndex: 'salarySupple',
                width: '110px',
                editable:false,
            },
            {
                title: '通勤车费代扣',
                dataIndex: 'salaryCar',
                width: '110px',
                editable:false,
            },
            {
                title: '客诉扣罚',
                dataIndex: 'salaryComplaint',
                width: '110px',
                editable:false,
            },
            {
                title: '品质违规罚款',
                dataIndex: 'salaryQuality',
                width: '110px',
                editable:false,
            },
            {
                title: '业务品质奖',
                dataIndex: 'salaryBusiness',
                width: '110px',
                editable:false,
            },
            {
                title: '培训费',
                dataIndex: 'salaryTrain',
                width: '110px',
                editable:false,
            },
            {
                title: '过节费',
                dataIndex: 'salaryFestival  ',
                width: '110px',
                editable:false,
            },
            {
                title: '员工应发工资(含税)',
                dataIndex: 'sumSalaryTax',
                width: '130px',
                editable:false,
            },
            {
                title: '应发工资',
                dataIndex: 'sumSalaryPay',
                width: '110px',
                editable:false,
            },
            {
                title: '社保个人',
                dataIndex: 'salaryPersonalSb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金个人',
                dataIndex: 'salaryPersonalGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '本月抵扣项',
                dataIndex: 'sumDeduction',
                width: '110px',
                editable:false,
            },
            {
                title: '累计应税工资',
                dataIndex: 'sumTaxSalary',
                width: '150px',
                editable:false,
            },
            {
                title: '累计至上月应缴所得税总额',
                dataIndex: 'sumPrepaidSalary',
                width: '180px',
                editable:false,
            },
            {
                title: '累计所得税',
                dataIndex: 'sumTax',
                width: '110px',
                editable:false,
            },
            {
                title: '累计至上月所得税合计',
                dataIndex: 'sumPrepaidTax',
                width: '150px',
                editable:false,
            },
            {
                title: '本月实扣个税',
                dataIndex: 'salaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '独子费',
                dataIndex: 'salarySingleChild',
                width: '110px',
                editable:false,
            },
            {
                title: '实发合计',
                dataIndex: 'salaryReal',
                width: '110px',
                editable:false,
            },
            {
                title: '社保公司',
                dataIndex: 'salaryCpySb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金公司',
                dataIndex: 'salaryCpyGjj',
                width: '110px',
                editable:false,
            },

            {
                title: '年终奖',
                dataIndex: 'salaryYear',
                width: '110px',
                editable:false,
            },
            {
                title: '年终奖个税',
                dataIndex: 'salaryYearTax',
                width: '110px',
                editable:false,
            },
            {
                title: '公司社保费用',
                dataIndex: 'salarySbGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '派遣公司管理费用',
                dataIndex: 'salaryManage',
                width: '110px',
                editable:false,
            },
            {
                title: '薪酬费用合计',
                dataIndex: 'sumSalaryFee',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO系数',
                dataIndex: 'mbo',
                width: '110px',
                editable:false,
            },
            {
                title: '本月工作日',
                dataIndex: 'workDay',
                width: '110px',
                editable:false,
            },

            {
                title: '实际出勤天数',
                dataIndex: 'realWorkDay',
                width: '110px',
                editable:false,
            },
            {
                title: '培训天数',
                dataIndex: 'trainDay',
                width: '110px',
                editable:false,
            },
            {
                title: '年假',
                dataIndex: 'yearHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '事假',
                dataIndex: 'affairHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '病假',
                dataIndex: 'sickHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '旷工',
                dataIndex: 'absence',
                width: '110px',
                editable:false,
            },
            
            {
                title: '婚假',
                dataIndex: 'marriageHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '丧假',
                dataIndex: 'loseHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '陪产假',
                dataIndex: 'paternityHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产假',
                dataIndex: 'childBirthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产检假',
                dataIndex: 'productionHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '生日假',
                dataIndex: 'birthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '迟到',
                dataIndex: 'arriveLate',
                width: '110px',
                editable:false,
            },
            {
                title: '是否当月离职',
                dataIndex: 'isNowMonthLeave',
                width: '110px',
                editable:false,
            },
            {
                title: '是否长病假',
                dataIndex: 'isLongSick',
                width: '110px',
                editable:false,
            },
            {
              title: '培训期病假',
              dataIndex: 'trainSickHoliday',
              width:120
            },
            {
              title: '培训期事假',
              dataIndex: 'trainAffairHoliday',
              width:120
            },
            {
                title: '当月是否转岗',
                dataIndex: 'isChangePosition',
                width: '110px',
                editable:false,
                render:(text)=>text==1?'是':'否'
            },
        ];
    

        //引入table列和编辑
        const columns = column.map((col) => {
            if (!col.editable) {
            return col;
            }
            return {
            ...col,
            onCell: record => ({
                record,
                inputType: col.dataIndex=='adjust'?'number':'',
                dataIndex: col.dataIndex,
                title: col.title,
                editing: this.isEditing(record),
            }),
            };
        });


        //组件组合
        const components = {
            body: {
              row: EditableFormRow,
              cell: EditableCell,
            },
          };
        
        return (
            <PageHeaderLayout>
              <Card title="薪酬计算的结果" bordered={false}>
                <div className={styles.tableList}>
                  <Table
                    size="small"
                    bordered={true}
                    components={components}
                    loading={loading}
                    dataSource={dataSource}
                    state={dataSource}
                    columns={columns}
                    pagination={this.state.pagination}
                    onChange={this.pageOnChange}
                    // pagination={false}
                    scroll={{ x: 9000,y:400 }}
                    />
                </div><br/><br/>
                <div className={styles.tableListForm}>
                    <Button type="default" onClick={this.handleConfirmSubmit}>
                        确认薪酬修改
                    </Button>
                    <Button style={{marginLeft: 20}} type="primary" onClick={this.approvedClick}>
                        确认薪酬提交审核
                    </Button>
                  </div>
              </Card>
              </PageHeaderLayout>
        );
      }
}

