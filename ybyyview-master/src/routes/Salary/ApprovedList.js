import { Table, Divider,Card, Tag } from 'snk-web';
import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import styles from './ApprovedList.less';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import moment from 'moment';
const { Column, ColumnGroup } = Table;


@connect(({ approvedList,loading }) => ({
  approvedList,
  loading:loading.models.approvedList
}))
export default class ApprovedList extends PureComponent {
  state = {

  }
  componentDidMount() {
    console.log('mount')
    this.props.dispatch({
      type: 'approvedList/fetch'
    });
  }


  approvedClick = (record) => {
    const { dispatch } = this.props
    record.status="1"
    dispatch({
      type: 'approvedList/updateStatus',
      payload: { ...record }
    }).then(reponse=>{
      dispatch({ type: 'approvedList/fetch' })
    }
    )
  }
  routerTo = (record) => {
    const { dispatch } = this.props
    console.log('record 是：'+ JSON.stringify(record))
    // dispatch(routerRedux.push('/person/searchPerson'))
    dispatch(routerRedux.push({ 
      pathname: '/salary/editSarlary',
      params: record
    }))

  }

  render() {
    return (
      <PageHeaderLayout>
      <Card title="待审核任务" >
      <Table dataSource={this.props.approvedList.dataList}
        bordered={true}
        rowKey={record => record.id}
        scroll={{y:500}}
        pagination={false}
        loading={this.props.loading}
      // columns={this.getColumns()}
      >
        <Column
          title="序号"
          dataIndex="id"
          key="id"
          width="50px"
          render={(text, record, index) => (
            <span>
              {index + 1}
            </span>
          )}
        />
        <Column
          title="团队名称"
          dataIndex="groupName"
          key="groupName"
          width="120px"
        />
        <Column
          title="薪酬年月"
          dataIndex="calcMonth"
          key="calcMonth"
          width="80px"
          render={(text,row)=>row.calcYear+'年'+row.calcMonth+'月'}
        />
        <Column
          title="薪酬计算日期"
          dataIndex="createdTime"
          key="createdTime"
          width="100px"
          render={createdTime => (
            <span>
              {moment(createdTime).format('YYYY-MM-DD')}
            </span>
          )}
        />
        <Column
          title="操作"
          key="action"
          width="120px"
          render={(text, record) => (
            <span>
              <a className={styles.red} onClick={() => { this.approvedClick(record) }}>提交审核</a>
              <Divider type="vertical" />
              <a href="javascript:;" className={styles.gray} onClick={() => { this.routerTo(record) }}>查看详情</a>
            </span>
          )}
        />
      </Table>
      </Card>
      </PageHeaderLayout>
    )
  }
}