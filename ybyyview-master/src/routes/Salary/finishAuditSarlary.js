import { 
    Table, 
    Steps,
    Popconfirm,
    Card,
    Form,
    Input,
    Select,
    Button,
    InputNumber,
    Modal,
    message} from 'snk-web';
import React from 'react';
import styles from './querySarlary.less';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
 

//定义进度条step
const Step = Steps.Step;



const CreateForm = Form.create()(props => {
    const { collectSalary,modalVisible, form,  handleModalVisible,downloadSalary } = props;

    const columns = [
        {
            title: '派遣公司',
            dataIndex: 'outsourceName',
            editable:false,
            width: '120px',
        },
        {
            title: '应发员工工资(含税)',
            dataIndex: 'sumSalaryChild',
            width: '150px',
            editable:false,
        },
        {
            title: '公司社保费用',
            dataIndex: 'salarySbGjj',
            width: '130px',
            editable:false,
        },
        {
            title: '派遣公司管理费用',
            dataIndex: 'salaryManage',
            width: '130px',
            editable:false,
        },
        {
            title: '应付薪酬合计',
            dataIndex: 'remunerationePay',
            width: '130px',
            editable:false,
        },
        {
            title: '扣除代付内推费用',
            dataIndex: 'deduction',
            width: '130px',
            editable:false,
        },
        {
            title: '薪酬费用合计',
            dataIndex: 'sumSalary',
            width: '130px',
            editable:false,
        },
        {
            title: '税率',
            dataIndex: 'taxRate',
            width: '130px',
            editable:false,
        },
        {
            title: '实付费用合计',
            dataIndex: 'totalPayment',
            width: '130px',
            editable:false,
        },
    
    ];

    return (
      <Modal
        title="查看汇总金额"
        width={880}
        visible={modalVisible}
        footer={null}
        onCancel={() => handleModalVisible(false)}
      >
       <Table
        rowKey={record => record.id}
        dataSource={collectSalary}
        columns={columns}
        scroll={{ x: 1500,y:360 }}
        pagination={false}
        />
        <Button style={{marginRight: 20}} type='primary' icon='download' onClick={() => downloadSalary()}>导出</Button>
      </Modal>
    );
});



  //装饰器
  @connect(({umssouserinfo, sarlaryMdl,loading}) => ({
    umssouserinfo,
    sarlaryMdl,
    loading:loading.models.sarlaryMdl,
  }))
  
  
  export default class finishAuditSarlary extends React.PureComponent {
    
    constructor(props){
        super(props);
        this.state = {
            dataSource: [],
            status:'',
            modalVisible: false,
            collectSalary:[],
            dataObj:{},
            pagination:{current:1,pageSize:50,showSizeChanger:true,pageSizeOptions:['30','50','80','100']}
        };
        this.columns = [
            {
                title: '员工姓名',
                dataIndex: 'empName',
                width: '120px',
                fixed: 'left'
            },
            {
                title:"序号",
                dataIndex:"id",
                key:"id",
                width: '70px',
                render: (text, record,index) => {
                    return (
                        <span>
                          {(this.state.pagination.current-1)*this.state.pagination.pageSize+index+1}
                        </span>
                      );
                },   
            },
            {
                title: '系统调整',
                dataIndex: 'sysAdjust',
                width: '130px',
                editable: true,
                // className:styles.columnsBcolor
            },
            {
                title: '系统备注',
                dataIndex: 'sysRemark',
                width: '130px',
                editable:true,
                // className:styles.columnsBcolor
            },
            {
                title: '手工调整',
                dataIndex: 'adjust',
                width: '130px',
            },
            {
                title: '备注',
                dataIndex: 'remark',
                width: '130px',
            },
            {
                title: '操作',
                dataIndex: 'status',
                width: '110px',
                editable: false,
                render: (text, record) => {
                    const {status} = record;
                    return (
                    <div>
                        <span>通过</span>
                    </div>
                    )
                },
            },
            {
                title: '原因',
                dataIndex: 'approveRemark',
                width: '110px',
                editable:false,
                // className:styles.columnsBcolor
            },
            {
                title: '团队名称',
                dataIndex: 'groupName',
                width: '120px',
                editable:false
            },
            {
                title: '工号',
                dataIndex: 'employeeCode',
                width: '120px',
                editable:false,
            },
            {
                title: '组别名称',
                dataIndex: 'teamName',
                width: '120px',
                editable:false,
            },
            {
                title: '职级',
                dataIndex: 'rankName',
                width: '120px',
                editable:false,
            },
            {
                title: '入职时间',
                dataIndex: 'entryDate',
                width: '140px',
                editable:false,
            },
            {
                title: '年限',
                dataIndex: 'workAge',
                width: '100px',
                editable:false,
            },
            {
                title: '离职时间',
                dataIndex: 'leaveDate',
                width: '150px',
                editable:false,
            },
            {
                title: '上线时间',
                dataIndex: 'onlineDate',
                width: '150px',
                editable:false,
            },
            {
                title: '身份证号码',
                dataIndex: 'identityCode',
                width: '170px',
                editable:false,
            },
            {
                title: '一卡通(银行卡号)',
                dataIndex: 'bankNo',
                width: '170px',
                editable:false,
            },
            {
                title: '请款业务',
                dataIndex: 'requestPrm',
                width: '110px',
                editable:false,
            },
            {
                title: '3月内退保',
                dataIndex: 'cancePrm',
                width: '110px',
                editable:false,
            },
            {
                title: '计薪保费',
                dataIndex: 'payPremium',
                width: '110px',
                editable:false,
            },
            {
                title: '基本工资',
                dataIndex: 'salaryBasic',
                width: '110px',
                editable:false,
            },
            {
                title: '职级津贴',
                dataIndex: 'salaryAllowance',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO奖',
                dataIndex: 'salaryMbo',
                width: '110px',
                editable:false,
            },
            {
                title: '提成',
                dataIndex: 'salaryPerform',
                width: '110px',
                editable:false,
            },
            {
                title: '新人津贴',
                dataIndex: 'salaryNew',
                width: '110px',
                editable:false,
            },
            {
                title: '服务津贴',
                dataIndex: 'salaryService',
                width: '110px',
                editable:false,
            },
            {
                title: '內荐费',
                dataIndex: 'salaryRec',
                width: '110px',
                editable:false,
            },
            {
                title: '激励方案兑换',
                dataIndex: 'salaryStimulate',
                width: '110px',
                editable:false,
            },
            {
                title: '出勤扣',
                dataIndex: 'salaryAttendance',
                width: '110px',
                editable:false,
            },
            {
                title: '餐补',
                dataIndex: 'salarySupple',
                width: '110px',
                editable:false,
            },
            {
                title: '通勤车费代扣',
                dataIndex: 'salaryCar',
                width: '110px',
                editable:false,
            },
            {
                title: '客诉扣罚',
                dataIndex: 'salaryComplaint',
                width: '110px',
                editable:false,
            },
            {
                title: '品质违规罚款',
                dataIndex: 'salaryQuality',
                width: '110px',
                editable:false,
            },
            {
                title: '业务品质奖',
                dataIndex: 'salaryBusiness',
                width: '110px',
                editable:false,
            },
            {
                title: '培训费',
                dataIndex: 'salaryTrain',
                width: '110px',
                editable:false,
            },
            {
                title: '过节费',
                dataIndex: 'salaryFestival  ',
                width: '110px',
                editable:false,
            },
            {
                title: '员工应发工资(含税)',
                dataIndex: 'sumSalaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '应发工资',
                dataIndex: 'sumSalaryPay',
                width: '110px',
                editable:false,
            },
            {
                title: '社保个人',
                dataIndex: 'salaryPersonalSb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金个人',
                dataIndex: 'salaryPersonalGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '所得税',
                dataIndex: 'salaryTax',
                width: '110px',
                editable:false,
            },
            {
                title: '独子费',
                dataIndex: 'salarySingleChild',
                width: '110px',
                editable:false,
            },
            {
                title: '实发合计',
                dataIndex: 'salaryReal',
                width: '110px',
                editable:false,
            },
            {
                title: '社保公司',
                dataIndex: 'salaryCpySb',
                width: '110px',
                editable:false,
            },
            {
                title: '公积金公司',
                dataIndex: 'salaryCpyGjj',
                width: '110px',
                editable:false,
            },

            {
                title: '年终奖',
                dataIndex: 'salaryYear',
                width: '110px',
                editable:false,
            },
            {
                title: '年终奖个税',
                dataIndex: 'salaryYearTax',
                width: '110px',
                editable:false,
            },
            {
                title: '公司社保费用',
                dataIndex: 'salarySbGjj',
                width: '110px',
                editable:false,
            },
            {
                title: '派遣公司管理费用',
                dataIndex: 'salaryManage',
                width: '110px',
                editable:false,
            },
            {
                title: '薪酬费用合计',
                dataIndex: 'sumSalaryFee',
                width: '110px',
                editable:false,
            },
            {
                title: 'MBO系数',
                dataIndex: 'mbo',
                width: '110px',
                editable:false,
            },
            {
                title: '本月工作日',
                dataIndex: 'workDay',
                width: '110px',
                editable:false,
            },

            {
                title: '实际出勤天数',
                dataIndex: 'realWorkDay',
                width: '110px',
                editable:false,
            },
            {
                title: '培训天数',
                dataIndex: 'trainDay',
                width: '110px',
                editable:false,
            },
            {
                title: '年假',
                dataIndex: 'yearHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '事假',
                dataIndex: 'affairHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '病假',
                dataIndex: 'sickHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '旷工',
                dataIndex: 'absence',
                width: '110px',
                editable:false,
            },
            
            {
                title: '婚假',
                dataIndex: 'marriageHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '丧假',
                dataIndex: 'loseHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '陪产假',
                dataIndex: 'paternityHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产假',
                dataIndex: 'childBirthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '产检假',
                dataIndex: 'productionHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '生日假',
                dataIndex: 'birthHoliday',
                width: '110px',
                editable:false,
            },
            {
                title: '迟到',
                dataIndex: 'arriveLate',
                width: '110px',
                editable:false,
            },
            {
                title: '是否当月离职',
                dataIndex: 'isNowMonthLeave',
                width: '110px',
                editable:false,
            },
            {
                title: '是否长病假',
                dataIndex: 'isLongSick',
                width: '110px',
                editable:false,
            },
            {
                title: '培训期病假',
                dataIndex: 'trainSickHoliday',
                width:120
              },
              {
                title: '培训期事假',
                dataIndex: 'trainAffairHoliday',
                width:120
              },
            {
                title: '当月是否转岗',
                dataIndex: 'isChangePosition',
                width: '110px',
                editable:false,
                render:(text)=>text==1?'是':'否'
            },
        ];
    }
    
  
    componentDidMount() {
        const dom = document.getElementsByClassName('ant-modal-wrap')
        if(dom.length>0){
            dom[0].click()
        }
        const { dispatch, umssouserinfo:{currentUser} } = this.props;
        //获取消息页面传递过来的参数
        const search = this.props.location.search;   
        var params = [];
        //?groupId=2&calcMonth=7&calcYear=2018
        params= search.slice(1).split('&');        
        
        var pData = [];
        for(let i=0;i<params.length;i++){
            pData.push(params[i].split('=')[1]);
        }

        //参数
        const dataObj = {
            groupId:pData[0],
            calcMonth:pData[1],
            calcYear:pData[2],
            status:'4'
        }
       
        dispatch({
            type: 'sarlaryMdl/querySalaryRTData',
            data:dataObj
        }).then(()=>{
            const newData = [...this.props.sarlaryMdl.sarlaryRTdata];
            const status = newData[0].status;        
            this.setState({dataSource: this.props.sarlaryMdl.sarlaryRTdata,status,dataObj});
        }).catch((e)=>{
            console.log(e);
        });

    }


  

    

 

 

    //发送薪资邮件
    sendSalaryEmail = () => {
        const { dispatch } = this.props
        dispatch(routerRedux.push('/salary/emailSalary'))
    }

    //查看汇总金额
    handleModalVisible = flag => {
        const { dispatch } = this.props
        const {dataObj,collectSalary} = this.state;
        if(flag){
            if(collectSalary.length>0){
                this.setState({modalVisible:true})
            }else{
                //查询汇总
                dispatch({
                    type: 'sarlaryMdl/collectSalaryResult',
                    data:dataObj
                }).then(()=>{      
                    const array = [];
                console.log('-----------------回调--------------------')     
                    array.push(this.props.sarlaryMdl.collectSalary)
                    this.setState({ modalVisible: flag, collectSalary: array});
                }).catch((e)=>{
                    console.log(e);
                });
            }
        }else{
            this.setState({modalVisible:false})
        }
      

    };

    // 下载薪酬汇总模版 
    downloadSalary = () => {
        console.log('sarlaryMdl called...');
        const { dispatch } = this.props;
        const {dataObj} = this.state;
        //发送请求
        dispatch({
            type: 'sarlaryMdl/downloadSalary',
            data: dataObj,
        }).then(()=>{
        }).catch((e)=>{
            console.log(e);
        });
       
    }

    pageOnChange=(pagination, filters, sorter)=>{
        this.setState({pagination})
    }

    //加载组件
    render() {
        
        const {loading} = this.props;
        const {dataSource,modalVisible} = this.state;

   
        
        const parentMethods = {
            // handleAdd: this.handleAdd,
            handleModalVisible: this.handleModalVisible,
            collectSalary:this.state.collectSalary,
            downloadSalary:this.downloadSalary
        };

        return (
            <PageHeaderLayout>
              <Card title="薪酬计算的结果" bordered={false}>
                <div>
                <Steps current={Number(this.state.status)}>
                        <Step title="薪酬修改"  />
                        <Step title="人事主任复核"  />
                        <Step title="财务初审"  />
                        <Step title="财务主任复核"  />
                        <Step title="完成"  />
                        <Step title="已打回"  />
                    </Steps>
                </div>
                <div className={styles.tableList}>
                  <Table
                    size="small"
                    loading={loading}
                    columns={this.columns}
                    dataSource={dataSource}
                    pagination={this.state.pagination}
                    onChange={this.pageOnChange}
                    scroll={{x: 8000,y:450}}
                    />
                </div><br/><br/>
                <div className={styles.tableListForm}>                  
                    <Button type="primary" style={{ float: 'left', margin: 8 }} onClick={() => this.handleModalVisible(true)}>
                        查看汇总金额
                    </Button>
                    <Button type="primary" style={{ float: 'left', margin: 8 }} onClick={this.sendSalaryEmail}>
                        发送薪资邮件
                    </Button>
                </div>
              </Card>
              <CreateForm {...parentMethods} modalVisible={modalVisible} />
            </PageHeaderLayout>  
        );
      }
}
  
  
  
  