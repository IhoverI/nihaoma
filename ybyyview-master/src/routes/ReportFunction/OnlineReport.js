import React, { PureComponent } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Input,
  Select,
  Button,
  message,
  Table,
  Form,
  DatePicker,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ChannelAndProduction from '../../components/ChannelAndProduction';
import { getDataSource,getSelectChild } from '../../utils/utils'

const { Option } = Select;

const columns = [
  {
    title: '业务渠道代码',
    dataIndex: 'oldEnterpCode',
    align: 'center',
  },
  {
    title: '出单机构代码',
    dataIndex: 'deptCode',
    align: 'center',
  },
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '投保单号',
    dataIndex: 'cPlyAppNo',
    align: 'center',
  },
  {
    title: '保险起期',
    dataIndex: 'insrncBeginDate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },

  {
    title: '总保费',
    dataIndex: 'prm',
    align: 'center',
  },
  {
    title: '产品',
    dataIndex: 'prodNo',
    align: 'center',
  },
  {
    title: '款别',
    dataIndex: 'cTgtFld1',
    align: 'center',
  },
  {
    title: '名单类型',
    dataIndex: 'nameListType',
    align: 'center',
  },
  {
    title: '销售区域',
    dataIndex: 'saleArea',
    align: 'center',
  },

  {
    title: '被保人姓名',
    dataIndex: 'insurantName',
    align: 'center',
  },
  {
    title: '被保人证件号',
    dataIndex: 'insurantIdNo',
    align: 'center',
  },
  {
    title: '被保人出生日期',
    dataIndex: 'insurantBirthdate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },
  {
    title: '被保人性别',
    dataIndex: 'insurantSex',
    align: 'center',
  },
  {
    title: '投保人姓名',
    dataIndex: 'appName',
    align: 'center',
  },
  {
    title: '投保人证件号',
    dataIndex: 'appIdNo',
    align: 'center',
  },
  {
    title: '投保人出生日期',
    dataIndex: 'appBirthdate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },
  {
    title: '投保人性别',
    dataIndex: 'appSex',
    align: 'center',
  },
  {
    title: '保单类型',
    dataIndex: 'cElcFlag',
    align: 'center',
  },
  {
    title: '客户要求投递地址',
    dataIndex: 'insrncAddress',
    align: 'center',
  },

  {
    title: '邮编',
    dataIndex: 'appZipCode',
    align: 'center',
  },
  {
    title: '投保人联系地址',
    dataIndex: 'appAddress',
    align: 'center',
  },
  {
    title: '电子邮箱',
    dataIndex: 'appEmail',
    align: 'center',
  },
  {
    title: '卡号',
    dataIndex: 'cardId',
    align: 'center',
  },
  {
    title: '销售人员姓名',
    dataIndex: 'tsrName',
    align: 'center',
  },

  {
    title: 'TSR工号',
    dataIndex: 'tsrCode',
    align: 'center',
  },
  {
    title: '缴费次数',
    dataIndex: 'payTime',
    align: 'center',
  },
];

@connect(({ reportModels, loading }) => ({
  reportModels,
  loading: loading.models.reportModels,
}))
@Form.create()
export default class OnlineReport extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    searchValues: {},
    total: 0,
    showArea: false,
  };

  componentWillMount(){
    if(this.props.history.location.state){
      this.props.history.push({state: undefined});
    } else{
      Object.keys(this.props.reportModels).forEach(key => delete this.props.reportModels[key]);
    }
  }

  handleFormReset = () => {
    this.props.form.resetFields();
    this.setState({
      showArea: false,
    });
  };

  handleSearch = ( e ) => {
    e.preventDefault();
    if(this.checkRule()){
      this.searchData(true);
    } else{
      message.warning('投保单导入日期和保险起期，必录其一');
    }
   };


  checkRule(){
     if((!this.props.form.getFieldValue('inputDate')||this.props.form.getFieldValue('inputDate').length===0)
     && (!this.props.form.getFieldValue('insrncBeginDate')||this.props.form.getFieldValue('insrncBeginDate').length===0)){
       return false;
      }
      return true;
    }
   
  searchData(isClickSearch){
    if (isClickSearch) {
      this.setState({
        pageNum: 1,
        pageSize: 10
      })
    }
    const { dispatch } = this.props;
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue = this.prepareValues(fieldsValue);
      if (isClickSearch) {
        fieldsValue.pageNum = 1;
        fieldsValue.pageSize = 10;
      }
      this.setState({
        searchValues: fieldsValue
      })
      dispatch({
        type: 'reportModels/onlineReport',
        payload: fieldsValue,
      }).then(() => {
        let { reportModels } = this.props;
        let data = reportModels.onlineReportResp.data;
        if (data) {
          this.setState({
            total: data.total
          })
        }
      }).catch(()=>{
        message.error('网络异常请稍后再试');
      });
      
    });
  }

  prepareValues(values){

    if(!values.insrncBeginDate||values.insrncBeginDate.length===0){
      values.insrncBeginDateStart = null;
      values.insrncBeginDateEnd = null;
    }else{
      values.insrncBeginDateStart = values.insrncBeginDate[0].format("YYYY-MM-DD");
      values.insrncBeginDateEnd = values.insrncBeginDate[1].format("YYYY-MM-DD");
    }
    delete values.insrncBeginDate;

    if(!values.inputDate||values.inputDate.length===0){
      values.inputDateStart = null;
      values.inputDateEnd = null;
    }else{
      values.inputDateStart = values.inputDate[0].format("YYYY-MM-DD");
      values.inputDateEnd = values.inputDate[1].format("YYYY-MM-DD");
    }
    delete values.inputDate;

    values.pageNum = this.state.pageNum;
    values.pageSize = this.state.pageSize;
    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  exportExcel = () => {
    let total = this.state.total;
    if (total === 0) {
      message.error('请搜索出结果后，再进行导出操作！');
      return;
    }
    let { dispatch } = this.props;
    let searchValues = this.state.searchValues;
    dispatch({
      type: 'reportModels/exportOnlineReport',
      payload: searchValues,
    }).then(() => {
      
    }).catch(()=>{
      message.error('网络异常请稍后再试');
    });
  }
  
   // 列表状态发生改变
   handleTableChange = (pagination, filtersArg, sorter) => {
    this.setState({
      pageSize: pagination.pageSize,
      pageNum: pagination.current,
    },()=>{
      this.searchData(false,this.form);
    }
  );
  };

  onChannelChange(value){
    if(value === '95555'){
      this.setState({showArea: true});
    } else{
      this.setState({showArea: false});
    }
  }

  render() {
    const { loading, reportModels: { onlineReportResp }, form } = this.props;
    const { getFieldDecorator } = form;
    let datalist = [];
    let total;
    if(onlineReportResp){
      if(onlineReportResp.data){
        datalist = onlineReportResp.data.list;
        total = onlineReportResp.data.total
      }
    }
    const { RangePicker } = DatePicker;

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <Form className='custom-form-wrapper' layout="inline">
            <ChannelAndProduction
                oldEnterpCodeMultiple = {false}
                prodNoMultiple = {true}
                onChannelChange={this.onChannelChange.bind(this)}
                channelFieldKey="oldEnterpCode"
                productionFieldKey="prodNoList"
                ChannelRule={{rules: [{required: true, message: '业务渠道必填'}]}}
                {...this.props} allowClear
              />
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <Form.Item label="保单年份">
                  {getFieldDecorator('pyear', {rules: [{required: true, message: '保单年份必填'}]})(
                    <Select allowClear placeholder="请选择">
                      {getSelectChild('pyear')}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="保险起期">
                  {getFieldDecorator('insrncBeginDate')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <Form.Item label="投保单导入日期">
                  {getFieldDecorator('inputDate')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
              {this.state.showArea ?
              <Col md={6} sm={24}>
                <Form.Item label="地区">
                  {getFieldDecorator('area')(
                    <Select allowClear placeholder='请选择'>
                      <Option value='N'>上海</Option>
                      <Option value='C'>成都</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col> : null}
              {this.state.showArea ?
              <Col md={6} sm={24}>
                <Form.Item label="合规标志">
                  {getFieldDecorator('compliance')(
                    <Select allowClear placeholder='请选择'>
                      <Option value='1'>合规</Option>
                      <Option value='0'>不合规</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col> : null}
            </Row>
          </Form>
          <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
            <span style={{ marginBottom: 24 }}>
              <Button loading={loading} onClick={this.handleSearch} type="primary" >
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </div>
          <Table
            loading={loading}
            rowKey={record => record.cPlyNo}
            dataSource={datalist}
            scroll={{x: 3400}}
            bordered
            title={()=>{
              return <Button loading={loading} type='primary' onClick={this.exportExcel}>导出excel</Button>
            }}
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            columns={columns}
            onChange={this.handleTableChange}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}
