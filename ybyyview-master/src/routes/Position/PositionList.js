import React, { PureComponent, Fragment } from 'react';
import { Table,Popconfirm } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';

import EmpTable from '../../components/EmpTable';
import EditableCell from '../../components/EmpTable/EditableCell';
import StandardTable from '../../components/StandardTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './PositionList.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const statusMap = ['default', 'processing', 'success', 'error'];
const firstLineFlag = ['一线', '非一线'];
const status = ['生效', '待生效'];

/*
 * 新增弹出框
*/
const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      title="新建职级"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
     <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="岗位" >
        {form.getFieldDecorator('positionName', {
          rules: [{ required: true, message: '岗位名称不能为空' }],
        })
        (<Input placeholder="请输入岗位" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="职级" >
        {form.getFieldDecorator('rankName', {
          rules: [{ required: true, message: '职级名称不能为空' }],
        })
        (<Input placeholder="请输入岗位" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="序号">
        {form.getFieldDecorator('order', {
          rules: [{ required: true, message: '序号不能为空' }],
        })(<Input placeholder="请输入职级" />)}
      </FormItem>
    </Modal>
  );
});

/*
 * 新增职级弹出框
*/
const CreateRankForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleRankVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      title="新建岗位"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleRankVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="岗位">
        {form.getFieldDecorator('desc', {
          rules: [{ required: true, message: '岗位名称不能为空' }],
        })(<Input placeholder="请输入" />)}
      </FormItem>
    </Modal>
  );
});

const EditableCell2 = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
      : value
    }
  </div>
);


@connect(({ positions, loading }) => ({
  positions,
  loading: loading.models.positions,
}))

@Form.create()
export default class positionList extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    dataSource: [],
  };

  componentDidMount() {
    const { dispatch, positions } = this.props;
    
    dispatch({
      type: 'positions/searchPosition',
      payload: {},
    }).catch((e)=>{
      this.setState({dataSource: this.props.positions.list});
      console.log(e);
    });
  }

  renderColumns(text, record, column) {

    return (
      <EditableCell2
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  edit(key, record) { 
    console.log(key);
    const { dataSource } = this.state;
    console.log(dataSource);
    const newData = [...dataSource];
    const target = newData.filter(item => key === item.key)[0];
    console.log(target);
    if (target) {
      target.editable = true;
      this.setState({ data: newData });
    }
  }

  save(key,dataSource) {
    console.log(this.state.dataSource);
    const newData = [...dataSource];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      delete target.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
    }
  }


  cancel(key,dataSource) {
    const newData = [...dataSource];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
      delete target.editable;
      this.setState({ data: newData });
    }
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      alert(value + 'this.state.dataSource[0].name');
      const dataSource = this.state.dataSource;
      // const target = dataSource.find(item => item.key === key);
      // if (target) {
      //   target[dataIndex] = value;
      //   this.setState({ dataSource });
      //   alert(this.state.dataSource[0].name);
      // }
    };
  }

  handleChange(value, key, column) {
    console.log(value);
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === item.key)[0];
    console.log('index...');
    const index = newData.findIndex(item => key === item.key);
    
    console.log(index);
    if (target) {
      console.log('target');
      console.log(target);
      console.log(target[column]);
      target[column] = value;
      // newData.splice(index, 1, {
      //   ...target,
      // });
      this.setState({ dataSource: newData });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'rule/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  };


  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            no: selectedRows.map(row => row.no).join(','),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };
      this.setState({
        formValues: values,
      });
      console.log('values...');
      console.log(values);
      console.log({fieldsValue});
      dispatch({
        type: 'positions/searchPosition',
        payload: {},
      }).then(()=>{
        this.setState({dataSource: this.props.positions.list});
      }).catch((e)=>{
        console.log(e);
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleRankVisible = flag => {
    this.setState({
      rankVisible: !!flag,
    });
  };


  handleAdd = fields => {
    this.props.dispatch({
      type: 'rule/add',
      payload: {
        positionName: fields.desc,
      },
    });

    message.success('添加成功');
    this.setState({
      modalVisible: false,
    });
  };


  expandedRowRender = () => {
  return (<p>展示</p>);
  }

  handleExpandChange = (enable) => {
    this.setState({ expandedRowRender: enable ? expandedRowRender : undefined });
  }

  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
          <Col md={16} sm={24}>
            <FormItem label="关键字查询">
              {getFieldDecorator('no')(<Input placeholder="可根据岗位名称、职级进行搜索" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="关键字查询">
              {getFieldDecorator('no')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <span style={{ float: 'right', marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              收起 <Icon type="up" />
            </a>
          </span>
        </div>
      </Form>
    );
  }

  renderForm() {
    return this.state.expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  render() {
    const { loading, positions } = this.props;
  
    const ranks = 'ranks';
    let { selectedRows, modalVisible, rankVisible } = this.state;
    const { dataSource } = this.state;
   
    console.log('positions ...');
    console.log(positions);
    //this.cacheData = dataSource.positions.map(item => ({ ...item }));

    const columns = [
      {
        title: '岗位名称',
        dataIndex: 'positionName',
        editable:false,
        width: '20%',
        render: (text, record) => this.renderColumns(text, record, 1),
      },
      {
        title: '职级',
        dataIndex: 'rank',
        width: '25%',
        editable:false,
      },
      {
        title: '类别',
        dataIndex: 'firstLineFlag',
        sorter: true,
        width: '15%',
        align: 'right',
        filters: [
          {
            text: firstLineFlag[0],
            value: 0,
          },
          {
            text: firstLineFlag[1],
            value: 1,
          },
        ],
      },
      {
        title: '状态',
        dataIndex: 'status',
        width: '15%',
        filters: [
          {
            text: status[0],
            value: 0,
          },
          {
            text: status[1],
            value: 1,
          },
        ],
        onFilter: (value, record) => record.status.toString() === value,
        render(val) {
          return <Badge status={statusMap[val]} text={status[val]} />;
        },
      },
      {
        title: '操作',
        width: '25%',
        render: (text, record) => {
          const { editable } = record;
          return (
            <Fragment>
                {
                  editable ?
                    <span>
                      <a onClick={() => this.save(record.key,dataSource)}>保存</a>
                      <Popconfirm title="确定要放弃编辑吗?" onConfirm={() => this.cancel(record.key,dataSource)}>
                        <a> 取消</a>
                      </Popconfirm>
                    </span>
                    : <a onClick={() => this.edit(record.key, record)}> 编辑</a>
                }
              <Divider type="vertical" />
              <a onClick={() => this.handleModalVisible(true)}>新增职级</a>
            </Fragment>
          );
        },
      },
    ];


    // rowSelection objects indicates the need for row selection
  const rowSelection2 = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
      console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      console.log(selected, selectedRows, changeRows);
    },
  };

    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    return (
      <PageHeaderLayout title="查询与编辑岗位职级">
        <Card bordered={false}>
          <div className={styles.positionList}>
            <div className={styles.positionListForm}>{this.renderForm()}</div>
            <div className={styles.positionListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新建岗位
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Button>批量操作</Button>
                  <Dropdown overlay={menu}>
                    <Button>
                      更多操作 <Icon type="down" />
                    </Button>
                  </Dropdown>
                </span>
              )}
            </div>
            <EmpTable
             selectedRows={selectedRows}
             loading={loading}
             dataSource={dataSource}
             state={dataSource}
             columns={columns}
             onSelectRow={this.handleSelectRows}
             onChange={this.handleStandardTableChange}
             childrenColumnName={ranks}
            />
           
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <CreateRankForm {...parentMethods} rankVisible={rankVisible} />
      </PageHeaderLayout>
    );
  }
}
