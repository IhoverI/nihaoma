import { Card,Table, Divider, Form, Icon, Input, InputNumber, Button, Popconfirm, message, Row, Col, Checkbox, Select, Modal } from 'snk-web';
import { connect } from 'dva';
import React, { PureComponent } from 'react';
import styles from './EditPositon.less'
import HttpUtils from '../../utils/HttpUtils';
import GroupAndTeamInfo from '../../components/Common/GroupAndTeamInfo'
import SearchResult from '../../components/Common/SearchResult'
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import { getGroupsInfo, getAllLaws, deleteRankById, searchPositions } from '../../services/editPositonService'
import moment from 'moment';



const FormItem = Form.Item;

// 类型
const  rankTypes={"1":"tsr","2":"主管","3":"总监","4":"招聘","5":"行政","6":"培训","7":"质检","8":"经理"}

//搜索组件
class AdvancedSearchForm extends React.Component {
  state = {
    selectOptions: []
  };
  componentDidMount() {
    this.getGroupsInfos()
  }


  getGroupsInfos = () => {
    getGroupsInfo().then(response => {
      this.setState({
        selectOptions: response.data
      })
    })
  }

  //搜索
  handleSearch = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log(values)
      this.props.fetch({ id: values.groupName })
    });
  }


  getFields() {
    const { getFieldDecorator } = this.props.form;
    const children = [];
    children.push(
      <Col span={6} key='groupName' style={{ display: 'block' }}>
        <FormItem label='团队名称:'>
          {getFieldDecorator('groupName', {
            initialValue: String(this.props.id),
          })(
            <Select placeholder="请选择团队名称"  allowClear={true}>
              <Select.Option key='ALL'>所有</Select.Option>
              {this.state.selectOptions.map((group) => <Select.Option key={group.id}>{group.groupName}</Select.Option>)}
            </Select>
          )}
        </FormItem>
      </Col>
    );
    children.push(
      <FormItem  key='submit'>
        <Button type="primary" htmlType="submit"> 搜索 </Button>
      </FormItem>
    );

    // children.push(
    //   <Col span={6} key='positionName' style={{ display: 'block' }}>
    //     <FormItem label='岗位名称:'>
    //       {getFieldDecorator('positionName', {
    //         initialValue: '',
    //       })(
    //         <Input />
    //       )}
    //     </FormItem>
    //   </Col>
    // );
    // children.push(
    //   <Col span={6} key='rankName' style={{ display: 'block' }}>
    //     <FormItem label='职级名称'>
    //       {getFieldDecorator('rankName', {
    //         initialValue: '',
    //       })(
    //         <Input />
    //       )}
    //     </FormItem>
    //   </Col>
    // );
    // children.push(
    //   <Col span={6} key='flag' style={{ display: 'block' }}>
    //     <FormItem label='含待生效'>
    //       {getFieldDecorator('flag', {
    //         initialValue: false,
    //       })(
    //         <Checkbox />
    //       )}
    //     </FormItem>
    //   </Col>
    // );
    return children;
  }

  render() {
    return (
      <Form
        className={styles.block}
        onSubmit={this.handleSearch}
      >
        <Row gutter={24}>
          {this.getFields()}
        </Row>
      </Form>
    );
  }
}

const WrappedAdvancedSearchForm = Form.create()(AdvancedSearchForm);



// //表格
// // rowSelection objects indicates the need for row selection
// const rowSelection = {
//   onChange: (selectedRowKeys, selectedRows) => {
//     console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
//   },
//   onSelect: (record, selected, selectedRows) => {
//     console.log(record, selected, selectedRows);
//   },
//   onSelectAll: (selected, selectedRows, changeRows) => {
//     console.log(selected, selectedRows, changeRows);
//   },
// };


const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
  
  getInput = () => {
    if (this.props.dataIndex == 'lawId') {
      return <Select  style={{width:'100%'}}  initialValue={String(this.props.record.lawId)}>
        {this.props.lawoptions.map(law => <Select.Option value={law.id} key={law.id}>{law.lawName}</Select.Option>)}
      </Select>
    }
    if (this.props.dataIndex == 'rankType') {
      return <Select  style={{width:'100%'}} initialValue={String(this.props.record.rankType)}>
        {Object.keys(rankTypes).map(key => <Select.Option value={key} key={key}>{rankTypes[key]}</Select.Option>)}
      </Select>
    }
    if (this.props.dataIndex == 'rankOrder') {
      return <InputNumber style={{width:'100%'}} />;
    }
    return <Input/>;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (
      <EditableContext.Consumer>
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [{
                      required: true,
                      message: `请输入有效 ${title}!`,
                    }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>
          );
        }}
      </EditableContext.Consumer>
    );
  }
}


export default class EditPositon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableList: [],
      pagination: {
        pageNum: 1,
        pageSize: 10,
        showSizeChanger: true,
        showQuickJumper: true,
      },
      loading: false,
      editingKey: '',
      editingIndex: '',
      lawOptions: [],
      urlpara:'ALL',
      expandedRowKeys:[]
    }
    this.columns = [
      {
        title: '岗位名称',
        dataIndex: 'positionName',
        width: '12.5%',
        editable: true,
      }, {
        title: '职级',
        dataIndex: 'rankName',
        editable: true,
        width: '12.5%',
        render: (text, row) => {
          return row.ranks != null ? '' : row.rankName
        }
      },
      {
        title: '基本法',
        dataIndex: 'lawId',
        editable: true,
        width: '12.5%',
        render: (text, row) => {
          let name = null;
          this.state.lawOptions.forEach(ele => {
            if (ele.id == text) {
              name = ele.lawName
            }
          })
          return name
        }
      },
      {
        title: '类型',
        dataIndex: 'rankType',
        editable: true,
        width: '12.5%',
        render: (text, row) => {
          for(let key in rankTypes){
            if(text==key){
              return rankTypes[key]
            }
          }
        }
      },
      {
        title: '描述',
        dataIndex: 'remark',
        editable: true,
        width: '12.5%',
        // render: (text, row) => {
        //   return rankTypes.filter(e=e)
        // }
      },
    
      {
        title: '创建日期',
        dataIndex: 'createdTime',
        editable: true,
        width: '12.5%',
        // editable: true,
        render: (text) => {
          return moment(text).format('YYYY-MM-DD HH:mm:ss')
        }
      },
      {
        title: '操作',
        dataIndex: 'Name',
        editable: true,
        width: '25%',
        render: (status, record) => {
          const editable = this.isEditing(record);
          const ilogUrl = `${ILOGURl}`;
          return (<div>
            {editable ? (
              <span>
                <EditableContext.Consumer>
                  {form => (
                    <Popconfirm placement="leftTop" title={this.state.popTitleGroupNames?`修改该岗位/职级会影响所有使用该岗位/职级的团队:${this.state.popTitleGroupNames}，请确认是否要修改`:'确定保存吗'} onConfirm={() => this.save(form, record.key)} onCancel={() => this.cancel(record)} okText="确定" cancelText="取消">
                     <a
                      href="javascript:;"
                      style={{ marginRight: 8 }}
                    >
                      保存
                      </a>
                  </Popconfirm>
                  
                  )}
                </EditableContext.Consumer>
                <a onClick={() => this.cancel(record)}>取消</a>
                
              </span>
            ) : (
                <div>
                  <a onClick={() => this.edit(record)}>编辑</a>
                  <Divider type="vertical" />
                  {record.key.indexOf('rank') > -1 ? <Popconfirm
                    title="确定删除吗"
                    onConfirm={() => this.delete(record.key)}
                  >
                    <a >删除</a>
                  </Popconfirm>
                    :
                    <span>
                      <a onClick={() => this.add(record.key)}>新增职级</a>
                      <Divider type="vertical" />
                      <a href={ilogUrl} target='blank'>iLog规则修改</a>
                   </span>
                    }
                </div>
              )}
          </div>)
        }
      },
    ];
  }
  componentDidMount() {
    //获取所有
    const urlpara = this.props.location.params || 'ALL'
    this.setState({
      urlpara: urlpara
    })
    this.fetch({id:urlpara})
    this.getLaws()
  

    // let a=  setInterval( ()=>console.log(this.props),1000)
  }
  getLaws = () => {
    getAllLaws().then(response => {
      this.setState({
        lawOptions: response.data
      })
    })
  }

 

  delete = (key) => {
    const editingKey = this.state.editingKey
    if (editingKey != '') {
      message.error("你有规则在编辑状态，请先处理后再操作！");
    } else {
      const rankId = key.split('_')[1]
      deleteRankById(rankId).then(response => {
        if (response.code == 0) {
          message.success("删除成功！");
          const tmpData = [...this.state.tableList];
          tmpData.forEach(ele => {
            const children = ele.ranks
            if (children != undefined) {
              for (let i = 0; i < children.length; i++)
                if (children[i].key == key) {
                  children.splice(i, 1)
                }
            }
          })
          this.setState({
            tableList: tmpData,
          })
        } else {
          message.error("删除失败！");
        }
      })
    }


  }
  isEditing = (record) => {
    return record.key === this.state.editingKey
  };
  isEditings = (record, dataIndex) => {
    if (record.key === this.state.editingKey) {
      if (record.key.indexOf('position') > -1) {
        return dataIndex == 'positionName' || dataIndex == 'lawId'||dataIndex == 'rankType'
      } else {
        return dataIndex == 'rankName' || dataIndex == 'remark'
      }
    } else {
      return false
    }
  };

  edit(record) {
    const { editingKey, tableList } = this.state;
    let editingId=null
    if (editingKey != '') {
      message.error("你有规则在编辑状态，请先处理后再操作！");
    } else {
      const index = this.state.tableList.findIndex(item => record.key === item.key);
      if (index > -1) {
        this.setState({ editingKey: record.key, editingIndex: index });
        editingId=this.state.tableList[index].id
      } else {
        let positionIndex = 0
        for (let i = 0; i < tableList.length; i++) {
          const ranks = tableList[i].ranks
          if (ranks != undefined) {
            for (let k = 0; k < ranks.length; k++) {
              if (ranks[k].key == record.key) {
                positionIndex = i
                break
              }
            }
          }
        }
        this.setState({ editingKey: record.key, editingIndex: positionIndex }, () => console.log(this.state.tableList));
        editingId=this.state.tableList[positionIndex].id
      }
      //获取所有团队
        HttpUtils.post({
        url: '/emp/queryGroupInfoByPositionId',
        body: { data: {positionId:editingId} }
        }).then(response => {
          this.setState({
            popTitleGroupNames:response.data.map(e=>e.groupName)
          })
        })
    }
  }

  addPotion = () => {
    const { editingKey, tableList } = this.state;
    if (editingKey != '') {
      message.error("你有规则在编辑状态，请先处理后再操作！");
    } else {
      const time = moment()
      const key = 'position_' + time.format('x')
      const data = {
        createdTime: time, createdUser: "",
        flag: 0, id: time, isDeleted: 0, key: key, lawId: null, lawName: null, positionName: "",
        rankName: null, ranks: [], status: "0", updatedTime: '', updatedUser: ""
      }
      tableList.unshift(data)
      this.setState({ 
        editingKey: key,
         tableList: tableList,
          editingIndex: 0 ,
          popTitleGroupNames:null});
    }
  }




  add(key) {
    const { editingKey } = this.state;
    if (editingKey != '') {
      message.error("你有规则在编辑状态，请先处理后再操作！");
    } else {
      const newData = [...this.state.tableList];
      const index = newData.findIndex(item => key === item.key);
      let newKey = 'rank_' + moment().format('x')

      if (index > -1) {
        const item = newData[index];
        let data = {
          createdTime: moment(), createdUser: "", flag: 0, id: moment(),
          isDeleted: 0, key: newKey, position: null, rankName: '',
          rankOrder: 1, status: "0", updatedTime: '', updatedUser: '', positionId: item.id
        }
        if (item.ranks == undefined) {
          item.ranks = [data]
        } else {
          item.ranks.unshift(data)
        }
      }
      const expandedRowKeys=this.state.expandedRowKeys
      if(expandedRowKeys.indexOf(key)==-1)expandedRowKeys.push(key)
      this.setState({ tableList: newData, editingKey: newKey, editingIndex: index ,expandedRowKeys:expandedRowKeys})
    }
  }
  save = (form, key) => {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.tableList];
      const editingIndex = this.state.editingIndex
      const item = newData[editingIndex];
      if (item.key == key) {
        newData.splice(editingIndex, 1, {
          ...item,
          ...row,
        });
        //updateTime 为“” 时 说明为新添加数据
        const saveUrl = item.updatedTime == '' ? '/emp/add_position' : '/emp/edit_position'
        console.log(newData[editingIndex])
        HttpUtils.post({
          url: saveUrl,
          body: { data: newData[editingIndex] }
        }).then(response => {
          if (response.code == 0) {
            message.success("保存成功！");
            if (saveUrl.indexOf('add') > -1) {
              const data = response.data
              data.key = "position_" + data.id
              newData.splice(editingIndex, 1, {
                ...response.data
              });
            }else{
              newData[editingIndex].ranks.forEach(e=>e.rankType=row.rankType)
              console.log(row.rankType)
            }
            this.setState({ tableList: newData, editingKey: '' });
          } else {
            message.error("保存失败！");
          }
        })
      } else {
        let rankIndex = 0
        const ranks = newData[editingIndex].ranks
        ranks.forEach((ele, index, ranks) => {
          if (key == ele.key) {
            rankIndex = index
          }
        })
        newData[editingIndex].ranks.splice(rankIndex, 1, {
          ...newData[editingIndex].ranks[rankIndex],
          ...row,
        });
        const saveUrl2 = newData[editingIndex].ranks[rankIndex].updatedTime == '' ? '/emp/add_rank' : '/emp/edit_rank'
        HttpUtils.post({
          url: saveUrl2,
          body: { data: newData[editingIndex].ranks[rankIndex] }
        }).then(response => {
          if (response.code == 0) {
            message.success("保存成功！");
            if (saveUrl2.indexOf('add') > -1) {
              console.log(response.data)
              newData[editingIndex].ranks.splice(rankIndex, 1, {
                ...response.data
              });
            }
            this.setState({ tableList: newData, editingKey: '' });
          } else {
            message.error("保存失败！");
          }
        })
      }
    });
  }

  cancel = (record) => {
    const tmpData = [...this.state.tableList];
    if (record.updatedTime == '') {
      if (record.key.indexOf('rank') > -1) {
        tmpData[this.state.editingIndex].ranks.shift()
      } else {
        tmpData.splice(this.state.editingIndex, 1)
      }
      this.setState({
        tableList: tmpData,
        editingKey: ''
      })
    } else {
      this.setState({
        editingKey: ''
      })
    }
  };
  fetch = (params = {}) => {
    console.log(params)
    this.setState({ loading: true });
    searchPositions(params).then((response) => {
      this.setState({
        tableList: response.data,
        count:response.count,
        // tableList: response.data.list,
        // pagination: {
        //   pageNum: response.data.pageNum,
        //   pageSize: response.data.pageSize,
        //   total: response.data.total
        // },
        loading: false
      })
      console.log(response)
    })
    
  }

  handleTableChange = (pagination, filters, sorter) => {
    const pager = { ...this.state.pagination };
    pager.pageNum = pagination.current;
    this.setState({
      pagination: pager,
    });
    // this.fetch({ pageNum: pager.pageNum, pageSize: pager.pageSize });
    this.fetch();

  }
  // onExpandedRowsChange=(expandedRows)=>{
  //    console.log(this.state.expandedRowKeys)
  //   console.log(expandedRows)
  // }
   
  onExpand=(expanded, record)=>{
    let expandedRowKeys=this.state.expandedRowKeys
    if(expanded){
      expandedRowKeys.push(record.key)
    }else{
      expandedRowKeys =  expandedRowKeys.filter(key=>key!=record.key)
    }
    this.setState({
      expandedRowKeys:expandedRowKeys
    })
  }

  render() {
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: 'text',
          // inputType: col.dataIndex === 'status' ? 'number' : 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditings(record, col.dataIndex),
          lawoptions:this.state.lawOptions
        }),
      };
    });
    
    return (
      <PageHeaderLayout>
      <Card title="岗位职级管理" >
      <div>
        <WrappedAdvancedSearchForm fetch={this.fetch} id={this.state.urlpara} />
        <SearchResult count={this.state.count}/>
        <Button type="primary" style={{ marginBottom: 10 }} onClick={this.addPotion}>新增岗位</Button>
        <GroupAndTeamInfo  />
        <Table 
          bordered
          // expandRowByClick={true}
          indentSize={0}
          scroll={{y:480}}
          // onExpandedRowsChange={this.onExpandedRowsChange.bind(this)}
          expandedRowKeys={this.state.expandedRowKeys}
          onExpand={this.onExpand.bind(this)}
          childrenColumnName={'ranks'}
          // defaultExpandAllRows={true}
          components={components}
          // rowClassName="editable-row"
          columns={columns}
          rowKey={record => record.key}
            // rowSelection={rowSelection}
          dataSource={this.state.tableList}
          pagination={false}
          loading={this.state.loading}
          // onChange={this.handleTableChange}
          className={styles.tableForm}
        />
      </div>
      </Card>
      </PageHeaderLayout>
    );
  }
}
