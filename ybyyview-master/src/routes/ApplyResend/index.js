import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Popconfirm,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import EmpTable from '../../components/EmpTable';
import EditableCell from '../../components/EmpTable/EditableCell';
import styles from './index.less';
import { getDataSource,getSelectChild,getChannelNameByValue } from '../../utils/utils'
import { Link } from 'dva/router';

const { RangePicker } = DatePicker;
const { TextArea } = Input;
const FormItem = Form.Item;
const { Option } = Select;
const InputGroup = Input.Group;

@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
export default class ApplyResend extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    pyear: '',
    dataSource: {},
    extEnterpCode: '',
    prodNo: '',
    cPlyNo: '',
    sureCommit: false,
    selectType: [],
    selectData: [],
    disable: '',
    cReprintFlag: '',
    showTabel: false,
    cSendEmailFlag: '',
    typeRules: '',
    typeRulesEle: '',
    disableRule: false,
    typeRulesData: '',
    disableRuleQue: false,
    isReplaceSubmit: false,
  };

  componentDidMount() {
    const { dispatch } = this.props;
  }
  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      modalVisible: false,
      expandForm: false,
      selectedRows: [],
      formValues: {},
      pyear: '',
      dataSource: {},
      extEnterpCode: '',
      prodNo: '',
      cPlyNo: '',
      sureCommit: false,
      selectType: [],
      selectData: [],
      disable: '',
      cReprintFlag: '',
      showTabel: false,
      cSendEmailFlag: '',
      typeRules: '',
      typeRulesEle: '',
      disableRule: false,
      disableRuleQue: false,
      typeRulesData: '',
    });
  };

  formatUtils = (val,code) => {
    let text = '';
    if (code === 'cSendType') {
      switch (val) {
        case '0': text = '纸质保单'; break;
        case '1': text = '电子保单'; break;
        case '2': text = '电子+纸质'; break;  
        case '3': text = '纸质保单'; break; 
        case '4': text = '纸质转电子'; break; 
        default: break;
      } 
    }
    return text;
  }

  // cancel(record) {
  //   const newData = [...this.state.dataSource];
  //   const target = newData.filter(item => item === record)[0];
  //   if (target) {
  //     delete record.editable;
  //     this.setState({ dataSource: newData });
  //   }
  // }

  // edit(record,dataSource) { 
  //   const newData = [...this.state.dataSource];
  //   const target = newData.filter(item => item === record)[0];
  //   if (target) {
  //     record.editable = true;
  //     this.setState({ dataSource: newData });
  //   }
  // }

  // save(record) {
  //   const { dispatch, form } = this.props;
  //   const newData = [...this.state.dataSource];
  //   const target = newData.filter(item => item === record)[0];
  //   if (target.sendAddress === '' || target.sendAddress === null) {
  //     message.warning('寄送地址不能为空');
  //   } else {
  //     let payload = [];
  //     if (target) {
  //       delete record.editable;
  //       this.setState({ dataSource: newData });
  //     }
  //   }
    
  //   form.validateFields((err, fieldsValue) => {
  //     if (err) return;
  //    })
  // }

  bujiziliao = (val) => {
    this.setState({selectData: val});
  }

  bujileixing = (val) => {
    this.setState({selectType: val});
    this.selectRule(val);
    this.inputRule(val);
  }

  selectRule = (val) => {
    if (val.indexOf('cExpressQuestionnaire') >= 0) {
      this.setState({disable:'0',cReprintFlag: '1'})
    } else if (val.indexOf('cRepairSendFlag') >= 0) {
      this.setState({disable: '1', cReprintFlag: '1',});
    } else if (val.indexOf('cExpressQuestionnaire') === -1 && val.indexOf('cRepairSendFlag') === -1 && val.indexOf('cSendEmailFlag') >= 0) {
      this.setState({cSendEmailFlag: '0', disable: '', cReprintFlag: '1'});
    } else if (val.indexOf('cSendEmailFlag') >= 0) {
      this.setState({cSendEmailFlag: '0', cReprintFlag: '1'});
    } else if (val.indexOf('cReprintFlag') >= 0) {
      this.setState({cReprintFlag: '0'})
    }else {  
      this.setState({disable: '',cReprintFlag: '', cSendEmailFlag: ''});
    }
  }

  inputRule = (val) => {
    if (val.indexOf('cSendEmailFlag')>=0){
      this.setState({typeRulesEle: '1'});
    } else{
      this.setState({typeRulesEle: ''});
    }
    if(val.indexOf('cExpressQuestionnaire')>=0 || val.indexOf('cRepairSendFlag')>=0){
      this.setState({typeRules: '1'});
    } else{
      this.setState({typeRules: ''});
    }
    if(val.indexOf('cExpressQuestionnaire')>=0){
      this.setState({disableRuleQue: true });
      this.props.form.resetFields('bujiziliao');
    } else{
      this.setState({disableRuleQue: false });
    }

    if(val.indexOf('cRepairSendFlag')>=0){
      this.setState({typeRulesData: '1'});
    } else{
      this.setState({typeRulesData: ''});
    }
    if (val.indexOf('cReprintFlag')>=0){
      this.setState({disableRule: true});
    } else{
      this.setState({disableRule: false});
    }
  }

  getProName = (proNo,record) => {
  let proName;
  if (getSelectChild('prodDesc')) {
    for (const i in getSelectChild('prodDesc',null,null,false,'group')) {
      if (getSelectChild('prodDesc')[i].props.value === proNo
    && getSelectChild('prodDesc',null,null,false,null,'group')[i].ref === record.extEnterpCode) {
        proName = getSelectChild('prodDesc')[i].props.children;
      }
    }
    return proName;
  }
}

  getColumns(){
    const { getFieldDecorator } = this.props.form;
    var _this = this;
    return [
       {
          title: '投保人姓名',
          dataIndex: 'cAppNme',
          align: 'center',
          width: 150,
           render: (text, record) => {
              return <span id='cAppNme'>{text}</span>
            },
        }, 
         {
            title: '被保人姓名',
            dataIndex: 'cInsrntCnm',
            align: 'center',
            width: 150,
        },
        {
          title: '渠道名称',
          dataIndex: 'extEnterpCode',
          align: 'center',
          width: 100,
          render:(val)=>{
            return getChannelNameByValue(val);
          },
      },
           {
            title: '产品名称',
            dataIndex: 'prodNo',
            align: 'center',
            width: 160,
            render: (text,record) => {
              return (<span>{this.getProName(text,record)}</span>);
            }
        },
          {
          title: '保单类型',
          dataIndex: 'cSendType',
          align: 'center',
          width: 100,
          render: (text) => {
            return (
              <span>{this.formatUtils(text,'cSendType')}</span>
            );
          }
        },
         {
          title: '电子邮箱',
          dataIndex: 'cEmail',
          width: 200,
          align: 'center',
          render: (text, record) => {
             return <span id='cEmail'>{text}</span>
          },
        }, 
        {
            title: '投递地址',
            dataIndex: 'sendAddress',
            align: 'center',
            render: (text, record) => {
              return <span id='sendAddress'>{text}</span>
            },
        },
        {
            title: '联系方式',
            dataIndex: 'cMobileNo',
            align: 'center',
            width: 160,
            render: (text, record) => {
              return (
                <span id='cMobileNo'>{text}</span>
              );
            }
        },
      ];
  }

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const fieldsValue = this.props.form.getFieldsValue();
      this.setState({
        pyear: fieldsValue.pyear
      });
      delete fieldsValue.cEmail;
      delete fieldsValue.bujileixing;
      delete fieldsValue.bujiziliao;
      delete fieldsValue.cRemark;
      delete fieldsValue.sendAddress;
      delete fieldsValue.cMobileNo;
      delete fieldsValue.cAppNme;
      dispatch({
        type: 'rule/guaranteeReissueQuery',
        payload: fieldsValue,
      })
  };

  handleSearchForm = () => {
    if(this.addrRule()){
      this.setState({sureCommit: true});
    }
  }

  onCancel = () => {
    this.setState({sureCommit: false});
    this.props.form.resetFields({
      bujileixing: '',
      bujiziliao: '',
      cEmail: '',
      cAppNme: '',
      cMobileNo: '',
      sendAddress: '',
      cRemark: '',
      sendAddressA: '',
      sendAddressB: '',
      sendAddressC: '',
      sendAddressD: ''
    });
  }

  addrRule() {
    const { getFieldValue, setFieldsValue } = this.props.form;
    let sendAddressA = getFieldValue('sendAddressA')===''
    || getFieldValue('sendAddressA')===undefined
    ? '' : `${getFieldValue('sendAddressA')}省`;
    let sendAddressB = getFieldValue('sendAddressB')===''
    || getFieldValue('sendAddressB')===undefined
    ? '' : `${getFieldValue('sendAddressB')}市`;
    let sendAddressC = getFieldValue('sendAddressC')===''
    || getFieldValue('sendAddressC')===undefined
    ? '' : `${getFieldValue('sendAddressC')}区`;
    let sendAddressD = getFieldValue('sendAddressD')===''
    || getFieldValue('sendAddressD')===undefined
    ? '' : getFieldValue('sendAddressD');
    setFieldsValue({'sendAddress':`${sendAddressA}${sendAddressB}${sendAddressC}${sendAddressD}`});
    let lenAddr = `${sendAddressD}`.replace(/[\u0391-\uFFE5]/g,"aa").length;


    if(this.state.typeRules==='1'){
      if(getFieldValue('sendAddress')===''){
        message.warning(`补寄类型为“补寄”或“快递问题件”时候，投递地址必录！`);
        return false;
      } else if(lenAddr<10){
        message.warning(`“详细地址”栏请补充完整！`);
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
      
   

  onConfirm = () => {
    const {form,dispatch,rule:{queryRuleData}} = this.props;
    if(!queryRuleData || !Object.keys(queryRuleData).length){
      message.warning('请先查询确认保单信息后再操作', 2);
      this.setState({
        sureCommit: false,
      })
      return;
    }
    let queryData = {};
    if (queryRuleData) {
      queryData = {
        cInsrntCnm : queryRuleData.cInsrntCnm,
        flag : queryRuleData.flag,
        cSendType : queryRuleData.cSendType,
        expressCode :'E1010',
        receiveStatus : queryRuleData.receiveStatus,
        appDate : queryRuleData.appDate,
        extEnterpCode : queryRuleData.extEnterpCode,
        prodNo : queryRuleData.prodNo,
        cPlyAppNo: queryRuleData.cPlyAppNo,
        pyear: queryRuleData.pyear,
        cAppNme: queryRuleData.cAppNme,
        cPlyNo: queryRuleData.cPlyNo,
      }
    }
   
    let sendAddress,cEmail,cMobileNo,cAppNme;
    let selectType = {};
    let selectData = {};
    form.validateFields((err,values)=>{
      if (err) return;
      const bujileixing = {
        cRepairSendFlag: 0,
        cExpressQuestionnaire: 0,
        cSendEmailFlag: 0,
        cReprintFlag: 0
      }
      const bujiziliao = {
        cGuarPrint: 0,
        cReceiptPrint: 0, 
        cRiderPrint: 0, 
        cInvoicePrint: 0
      }

      for (const i in this.state.selectType) {
        if (this.state.selectType !== []) {
          selectType[this.state.selectType[i]] = 1;
        }
      }

      for (const i in this.state.selectData) {
        if (this.state.selectData !== []) {
          selectData[this.state.selectData[i]] = 1;
        } 
      }

      let formBujiziliaoValues = {
        ...bujiziliao,
        ...selectData,
      }

      let formBujileixingValues = {
        ...bujileixing,
        ...selectType,
      }

      sendAddress = values.sendAddress === '' || values.sendAddress === undefined 
        ? 
        document.getElementById('sendAddress').innerText 
        : 
        values.sendAddress;
      cEmail = values.cEmail === '' || values.cEmail === undefined  
        ?
        document.getElementById('cEmail').innerText 
        : 
        values.cEmail;
      cMobileNo = values.cMobileNo === '' || values.cMobileNo === undefined
        ? 
        document.getElementById('cMobileNo').innerText 
        : 
        values.cMobileNo;
      cAppNme = values.cAppNme === '' || values.cAppNme === undefined
        ? 
        document.getElementById('cAppNme').innerHTML 
        : 
        values.cAppNme;

      delete values.bujileixing;
      delete values.bujiziliao;
      const payload = {
            ...values,
            ...formBujileixingValues,
            ...formBujiziliaoValues,
            ...queryData,
            sendAddress,
            cMobileNo,
            cAppNme,
            cEmail,
          }
      this.setState({
        sureCommit: false,
        isReplaceSubmit: true,
      })
      this.props.dispatch({
          type: 'rule/guaranteeReissueRegister',
          payload: {
            ...values,
            ...formBujileixingValues,
            ...formBujiziliaoValues,
            ...queryData,
            sendAddress,
            cEmail,
            cMobileNo,
            cAppNme,
          }
      }).then(()=>{
        this.setState({isReplaceSubmit: false, showTabel: true, cReprintFlag: '', cReprintFlag: '', disable: ''});
        setTimeout(()=>{
          this.props.form.resetFields();
          // this.props.form.setFieldsValue(
          //   {
          //     bujileixing: [],
          //     bujiziliao: [],
          //     cAppNme: '',
          //     cMobileNo: '',
          //     sendAddress: '',
          //     cEmail: '',
          //     cRemark: '',
          //     sendAddressA: '',
          //     sendAddressB: '',
          //     sendAddressC: '',
          //     sendAddressD: ''
          //   }
          // );
        },150);
      });
    })
  }
   

  renderAdvancedForm() {


    const { getFieldDecorator } = this.props.form;
    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
         <Col md={8} sm={24}>
            <FormItem label="保单号">
              {getFieldDecorator('cPlyNo',{
                 rules: [
                  {required: true, message: '必填!'},
                  {max: 30, message: '请输入正确的格式'},
                ],
              },{initialValue: ''})(<Input placeholder="保单号" />)}
            </FormItem>
          </Col>
        </Row>

        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
        </div>
      </Form>
    );
  }

  renderForm () {
     const { getFieldDecorator } = this.props.form;
     return (
        <Form className='custom-form-wrapper custom-search-form-wrapper' layout="inline" style={{marginTop:10}}>
          <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
            <Col md={6} sm={24}>
              <FormItem label="补寄类型"> 
                {getFieldDecorator('bujileixing',
                {
                  rules: [{ required: true, message: '必填！' }],
                },
                {initialValue: []})(
                      <Select  placeholder="请选择" style={{ width: '100%' }} onChange={this.bujileixing} mode="multiple">
                        <Option key="cRepairSendFlag" 
                          disabled={this.state.disable === '0' || this.state.cReprintFlag === '0'? true : false}>
                          补寄
                        </Option>
                        <Option key="cExpressQuestionnaire" 
                          disabled={this.state.disable === '1' || this.state.cReprintFlag === '0'? true : false}>
                          快递问题件
                        </Option>
                        <Option key='cSendEmailFlag' 
                          disabled={this.state.cReprintFlag === '0' ? true : false}>
                          电子保单
                        </Option>
                        <Option key='cReprintFlag' disabled={this.state.cReprintFlag === '1' ? true : false}>补充打印</Option>
                      </Select>
                  )}
              </FormItem> 
            </Col>
            <Col md={6} sm={24}>
              <FormItem label="补寄资料"> 
                {getFieldDecorator('bujiziliao',
                { 
                  rules: [{ required: this.state.typeRulesData==='1', message: '必填！' }],
                },
                {initialValue: []}
                )(
                      <Select  placeholder="请选择" 
                        style={{ width: '100%' }} 
                        onChange={this.bujiziliao} 
                        mode="multiple"
                        disabled = {this.state.disableRule||this.state.disableRuleQue}
                        >
                        <Option value="cGuarPrint">保单</Option>
                        <Option value="cReceiptPrint">回执</Option>
                        <Option value='cRiderPrint'>批单</Option>
                        <Option value='cInvoicePrint'>发票</Option>
                      </Select>
                  )}
              </FormItem> 
            </Col>
            <Col md={6} sm={24}>
              <FormItem label="收件人姓名">
                {getFieldDecorator('cAppNme',
                { 
                  rules: [{ required: this.state.typeRules==='1', message: '必填！' }],
                },
                {initialValue: ''},
                )(
                  <Input placeholder="" 
                  disabled = {this.state.disableRule}
                  />
                )}
              </FormItem>
            </Col>
             <Col md={6} sm={24}>
              <FormItem label="联系电话">
                {getFieldDecorator('cMobileNo',
                {
                  rules: [
                    { required: this.state.typeRules==='1', message: '必填！' },
                    { pattern: /(^\d{8,}$)/, message: '电话号码不小于8位' },
                ],
                },
                {initialValue: ''},
                )(
                  <Input placeholder="" 
                  disabled = {this.state.disableRule}
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
            <Col md={12} sm={24}>
              <FormItem label="电子邮箱">
                {getFieldDecorator('cEmail',
                {
                  rules: [{ required: this.state.typeRulesEle==='1', message: '必填！' }],
                },
                {initialValue: ''},
                )(
                <Input placeholder="" 
                disabled = {this.state.disableRule}
                />
                )}
              </FormItem>
            </Col>
            <Col md={12} sm={24}>
              <FormItem label="备注">
                {getFieldDecorator('cRemark',{initialValue: ''})(
                <Input placeholder="" 
                disabled = {this.state.disableRule}
                />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
            <Col md={24} sm={24}>
              <FormItem label="投递地址">
                  <InputGroup 
                  compact
                  style={{ width: '100%' }}
                  >{getFieldDecorator('sendAddress'
                  )(
                    <Input style={{ display: 'none' }}/>
                  )}
                  {getFieldDecorator('sendAddressA'
                  )(
                    <Input style={{ width: '5%' }} 
                    disabled = {this.state.disableRule}
                    />
                  )}
                    <Input style={{ width: '5%', borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="省" disabled />
                    {getFieldDecorator('sendAddressB'
                  )(
                    <Input style={{ width: '5%' }}
                    disabled = {this.state.disableRule}
                    />
                  )}

                    <Input style={{ width: '5%', borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="市" disabled />
                    {getFieldDecorator('sendAddressC'
                  )(
                    <Input style={{ width: '5%' }}
                    disabled = {this.state.disableRule}
                    />
                  )}
                    <Input style={{ width: '5%', borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="区" disabled />
                    {getFieldDecorator('sendAddressD'
                  )(
                    <Input style={{ width: '70%' }}
                    disabled = {this.state.disableRule}
                    placeholder="详细地址：如道路、门牌号、小区、楼栋号、单元室等"/>
                  )}
                  </InputGroup>
              </FormItem>
            </Col>
          </Row>
          <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Popconfirm title={'确定提交'} onConfirm={this.onConfirm} onCancel={this.onCancel} visible={this.state.sureCommit}>
              <Button type="primary" disabled={this.state.isReplaceSubmit} onClick={this.handleSearchForm}>提交补发登记信息</Button>
            </Popconfirm>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>取消</Button>
        </div>
        </Form>
     )
  }

  render() {
    const { loading,rule:{queryRuleData} } = this.props;
    const { selectedRows, modalVisible } = this.state;
    let data;
    let total;
    if (queryRuleData) {
      data = [queryRuleData] || []
    }
    this.state.dataSource = data;
    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedRows: selectedRows
        });
        },
    };
    const parentMethods = {
      handleAdd: this.handleAdd,
    };

    const rowKey =(record,index) => {
      return index;
    }

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div className={styles.ApplyList}>
            <div className={styles.ApplyListForm}>{this.renderAdvancedForm()}</div>
            <Table scroll={{ x: 1400 }} 
                className='custom-table'
                size='small'
                rowKey={rowKey}
                loading={loading}
                pagination={{
                hideOnSinglePage: true,
              }} 
                bordered 
                columns={this.getColumns()}
                dataSource={data} />
            <div style={{marginTop: 20}}>{this.renderForm()}</div>
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
