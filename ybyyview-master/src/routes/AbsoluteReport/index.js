import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Popconfirm,
  Radio,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ChannelAndProduction from 'components/ChannelAndProduction';
import { getDataSource,getSelectChild } from '../../utils/utils'


const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const Option = Select.Option;


const getExtEnterpName = (extEnterNo) => {
  let extEnterName;
  if (getSelectChild('extEnterpDesc')) {
    for (const i in getSelectChild('extEnterpDesc')) {
      if (getSelectChild('extEnterpDesc')[i].props.value === extEnterNo) {
        extEnterName = getSelectChild('extEnterpDesc')[i].props.children;
      }
    }
    return extEnterName;
  }
}

const columns = [
  {
    title: '扣款渠道',
    dataIndex: 'extEnterpCode',
    align: 'center',
  },
  {
    title: '业务渠道',
    dataIndex: 'oldEnterpCode',
    align: 'center',
  },
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '原保单号',
    dataIndex: 'cPlyNoOld',
    align: 'center',
  },
  {
    title: '年化保费', 
    dataIndex: 'prm',
    align: 'center',
  },
  {
    title: '投保日期',
    dataIndex: 'appDate',
    align: 'center',
    render(val){
      return <div> {moment(val).format("YYYY-MM-DD")} </div>
    },
  },
  {
    title: '缴费金额',
    dataIndex: 'totalPrm',
    align: 'center',
  },
  {
    title: '成功扣款期次',
    dataIndex: 'latestPeriod',
    align: 'center',
  },
  {
    title: '最新初次请款日期',
    dataIndex: 'lastTimeForPrm',
    align: 'center',
  },
];

const columns1 = [
  {
    title: '扣款渠道',
    dataIndex: 'extEnterpCode',
    align: 'center',
  },
  {
    title: '保单起期月', 
    dataIndex: 'tMonth',
    align: 'center',
  },
  {
    title: '记录数', 
    dataIndex: 'recCount',
    align: 'center',
  },
  {
    title: '年化保费总额',
    dataIndex: 'totalPrm',
    align: 'center',
  },
  {
    title: '未缴保费总额', 
    dataIndex: 'lefttotalPrm',
    align: 'center',
  },
];


@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.models.searchlist,
}))
@Form.create()
export default class AbsoluteReport extends PureComponent {
  state = {
    formValues: {},
    SelValue: {},
    insrncBeginDateStart: '',
    insrncBeginDateEnd: '',
    appDateStart: '',
    appDateEnd: '',
    payDateFirstStart: '',
    payDateFirstEnd: '',
    selectedRowKeys:[], 
    selectedRows:[],
    pageSize: 10,
    pageNum: 1,
    radioType: '0',
    ifSum: false,
    xScroll: 1400,
  };

  componentWillMount(){
    Object.keys(this.props.searchlist).forEach(key => {
      if(key !== 'allPolicyQueryData' && key!== 'policyQuerySearchFormData' && key !== 'selfChannelData' && key!== 'policyQueryDefaultTab' && key!== 'policyMapInfo') { delete this.props.searchlist[key] }
    });
  }
  

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      selectedRowKeys:[], 
      selectedRows:[],
    });
  };

  insdDateChange = (value) => {
    if (value.length > 0) {
      this.setState({
        appDateStart: value[0].format("YYYY-MM-DD 00:00:00"),
        appDateEnd: value[1].format("YYYY-MM-DD 23:59:59"),
      });
    } else {
       this.setState({
        appDateStart: '',
        appDateEnd: '',
      });
    }
  }

  handleDownload = () => {
    this.props.dispatch({
      type: 'searchlist/FixedReportDownload',
      payload:{
        ...this.state.formValues,
        pageNum: 1,
        pageSize: 10,
      },
    }).then(()=>{
    });
  }

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(1,10);
  };

  handleTablePage =(pagination, filters, sorter) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
      selectedRowKeys:[], 
      selectedRows:[],
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }

  searchData = (pageNum,pageSize) => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (fieldsValue.ifSUM === false) {
        this.setState({xScroll: 1400});
      } else {
        this.setState({xScroll: 1000});
      }
      this.setState({ifSum: fieldsValue.ifSUM});
      fieldsValue = this.perpareValues(fieldsValue);
      this.setState({formValues:fieldsValue});
        if (err) return;
        dispatch({
          type: 'searchlist/FixedReport',
          payload:{
            ...fieldsValue,
            pageNum,
            pageSize,
          },
        });
    });
  }

  perpareValues(values){
    if (!values.insuredDate || values.insuredDate.length === 0) {
      values.appDateStart = '';
      values.appDateEnd = '';
    } else {
      values.appDateStart = values.insuredDate[0].format("YYYY-MM-DD 00:00:00");
      values.appDateEnd = values.insuredDate[1].format("YYYY-MM-DD 23:59:59");
    }
    delete values.insuredDate;
    return values
  }

  getColumns(){
    if (this.state.ifSum === false) {
      return columns;
    } else {
      return columns1;
    }
  }


  renderAdvancedForm() {

    const { getFieldDecorator } = this.props.form;
    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={6} sm={30}>
            <FormItem label="保单年份">
            {getFieldDecorator('pYear',{initialValue:pyear})(
              <Select placeholder="请选择" style={{ width: '100%' }} allowClear>
                {getSelectChild('pyear')}
              </Select>
            )}
          </FormItem>
          </Col>
          <Col md={6} sm={30}>
            <FormItem label="清单类型">
              {getFieldDecorator('type',{initialValue: '1',})(
                  <Select  placeholder="请选择" style={{ width: '100%' }} allowClear>
                     <Option value='1'>新单首期无请款</Option>
                     <Option value='2'>新单首期请款失败</Option>
                     <Option value='3'>新单续期请款失败</Option>
                     <Option value='4'>续单首期无成功请款</Option>
                     <Option value='5'>续单续期请款失败</Option>
                  </Select>
              )}
            </FormItem>
          </Col> 
           <Col md={8} sm={30}>
            <FormItem label="投保日期">
              {getFieldDecorator('insuredDate')(
                <RangePicker style={{ width: '100%' }} onChange={this.insdDateChange} />
              )}
            </FormItem>
          </Col> 
        </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
           <Col md={6} sm={30}>
            <FormItem label="查询类型选择">
              {getFieldDecorator('ifSUM',{initialValue: false,})(
                  <Select  placeholder="请选择" style={{ width: '100%' }} allowClear>
                     <Option value={true}>统计汇总</Option>
                     <Option value={false}>明细清单</Option>
                  </Select>
              )}
            </FormItem>
          </Col> 
         
        </Row>
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
        </div>
      </Form>
    );
  }

  render() {
    const { searchlist: { FixedReportData }, loading } = this.props;
    const {selectedRowKeys, selectedRows} =this.state;
    const columns = this.getColumns();
    let data = []
    let total;
    if (FixedReportData) {
      if(FixedReportData.code==0){
        data = FixedReportData.data.list;
        total = FixedReportData.data.total;
      }
    }

    const parentMethods = {
      handleAdd: this.handleAdd,
    };
    const rowKey = (record, index) => { 
        return index;
    }
    
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
            <div>{this.renderAdvancedForm()}</div>
            <Table
              loading={loading}
              rowKey={rowKey}
              scroll={{x: this.state.xScroll}}
              dataSource={data}
              bordered
              size="small"
              pagination={{
                showQuickJumper: true,
                showSizeChanger: true,
                ...{
                    current:this.state.pageNum,
                    pageSize:this.state.pageSize,
                    showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                    total:total,
                    pageSizeOptions:['10','20','30','40','50'],
                },
              }}
              onChange={this.handleTablePage}
              title={() => {
                 return  <div>
                    <Button type='primary' style={{marginRight:10}} onClick={this.handleDownload}>下载导出</Button>
                 </div>
                ; 
            }}
            columns={columns}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
