import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Table,
  Radio,
  Tabs
} from 'snk-web';
import ReprintManageTable from './components/ReprintManageTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import SearchFormReprint from './SearchFormReprint';
import SearchExactFormReprint from './SearchExactFormReprint';
import {getChannelNameByValue,getProductNameByValue,getProductNameByExtEnterpCodeAndProdNo } from '../../utils/utils'

import styles from './ReprintManage.less';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { Option } = Select;

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const InputGroup = Input.Group;


const _formatUnit = (val,code) => {
  let text = '';
  if (code === 'flag') {
    switch (val) {
      case '1': text = '有效'; break;
      case '2': text = '注销'; break;
      case '3': text = '退保'; break;
      case '4': text = '过期'; break;
      default: break; 
    }
  } else if (code === 'reprintType') {
    switch (val) {
      case '1': text = '补打'; break;
      case '2': text = '补寄'; break;
      case '3': text = '电子邮件'; break;
      default: break; 
    }
  } else if (code === 'expressCode') {
    switch (val) {
      case 'E1003': text = '顺丰'; break;
      case 'E1010': text = '德邦'; break;
      case 'E1008': text = 'EMS'; break;
      default: break; 
    }
  } else {
    switch (val) {
      case '1': text = '待补打'; break;
      case '2': text = '已补打'; break;
      case '3': text = '补打失败'; break;
      default: break; 
    }
  }
    return text;
}

const columns = [
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '渠道', 
    dataIndex: 'extEnterpCode',
    align: 'center',
    render:(val)=>{
      return getChannelNameByValue(val);
    },
   },
  {
    title: '产品名称',
    dataIndex: 'prodNo',
    align: 'center',
     render:(text, record)=>{
      return getProductNameByExtEnterpCodeAndProdNo(record.extEnterpCode, text);
    },
   },
  {
    title: '投保人姓名',
    dataIndex: 'cAppNme',
    align: 'center',
  },
   {
    title: '被投保人姓名', 
    dataIndex: 'cInsrntCnm',
    align: 'center',
  },
   {
    title: '保险起期',
    dataIndex: 'insrncBeginDate',
    align: 'center',
  },
  {
    title: '联系电话',
    dataIndex: 'cMobileNo',
    align: 'center',
  },
  {
    title: '快递公司',
    dataIndex: 'expressCode',
    align: 'center',
    render: (text,record) => {
      return <span>{_formatUnit(text,'expressCode')}</span>
    }
  },
  {
    title: '收件人姓名',
    dataIndex: 'cAppNme1',
    align: 'center',
    render: (text,record) => {
      return (
        <span>{record.cAppNme}</span>
      )
    }
  },
   {
    title: '投递地址',
    dataIndex: 'sendAddress',
    align: 'center',
  },
   {
    title: '补寄类型', 
    dataIndex: 'repairSendType',
    align: 'center',
  },
   {
    title: '补寄资料',
    dataIndex: 'repairSendData',
    align: 'center',
  },
   {
    title: '补发登记日期',
    dataIndex: 'cRegisterDate',
    align: 'center',
  },
   {
    title: '补发登记人员',
    dataIndex: 'cRegisterPerson',
    align: 'center',
  },
  {
    title: '备注',
    dataIndex: 'cRemark',
    align: 'center',
  },
];


@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
export default class ReprintManage extends PureComponent {
  state = {
    modalVisible: false,
    formValues: {},
    selectedRows: [],
    selectedRowKeys: [],
    selValue: '',
    selPrintTypes: '',
    value: 1,
    allPrint: '',
    radioTag: '1',
    printStatus: true,
    printFlag: '',
    startPage: '',
    endPage: '',
    disableFlag: '0',
    reprintTypeFlag: '1',
    fieldsValue:'',
    selectedType: '',
    data: [],
    searchFieldsValue: {},
  };

  componentWillMount(){
    Object.keys(this.props.rule).forEach(key => delete this.props.rule[key]);
}

  okHandle = () => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      let allPrint = false;
      let newPrint ={};
      let types = { 
          'cGuarPrint': '0',
          'cExpressPrint': '0',
          'cReceiptPrint': '0',
      };
      
      if (this.state.selPrintTypes.length > 0) {
        for (const i in this.state.selPrintTypes) {
          newPrint[this.state.selPrintTypes[i]] = '1'
        }
      }
 
      let printTypes = {
        ...types,
        ...newPrint
      }
      let payload = {};
      if (this.state.radioTag === '1') {
        payload = {
          ...this.state.selectedRows[0],
          ...printTypes,
          allPrint: true,
        };
      } else if (this.state.radioTag === '2' && this.state.disableFlag === '0'){
        if (this.props.form.getFieldValue('startPage') === '' || this.props.form.getFieldValue('endPage') === '') {
          message.warning('请填写打印页数',1);
        }
        payload = {
          ...this.state.selectedRows[0],
          ...printTypes,
          allPrint: false,
          endPage: fieldsValue.endPage,
          startPage: fieldsValue.startPage,
        }
      } else {
         payload = {
          ...this.state.selectedRows[0],
          ...printTypes,
        }
      }
      this.props.dispatch({
        type: 'rule/guaranteeReprintPrint',
        payload: payload
      }).then(()=>{
        setTimeout(()=>{
          this.props.dispatch({
              type: 'rule/guaranteeReprintQuery',
              payload: this.state.payloadVlaue,
            }).then(()=>{
            const {rule:{guaranteeReprintQueryData}} = this.props;
            this.setState({data:guaranteeReprintQueryData});
          });
        },200);
        this.setState({
          modalVisible: false,
          selPrintTypes: '',
          printFlag: '',
          selectedRows:[],
          selectedRowKeys:[]
        });
        this.props.form.setFieldsValue({
          printType: [],
          startPage: '',
          endPage: ''
        });
      });
    })
  }

handleModalVisible =() => {
  this.props.form.setFieldsValue({
    printType: [],
    startPage: '',
    endPage: ''
  });
  this.setState({
    modalVisible: false,
    selPrintTypes: '',
    printFlag: '',
    selectedRowKeys: [], 
    selectedRows: [],
  });
}

   handleFormReset = () => {
    const { dispatch, form } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
      printStatus: true,
      selectedRows:[],
      selectedRowKeys:[],
    });
  };
  /*  
    需求变更，不再调用该方法
    bug id-363
  */
  ruleVerify = () =>{
    let rulesOpera = [
      this.props.form.getFieldValue('startCPlyNo'),
      this.props.form.getFieldValue('endCPlyNo'),
      this.props.form.getFieldValue('cPlyNo'),
      this.props.form.getFieldValue('cPlyAppNo'),
      this.props.form.getFieldValue('extEnterpCode') || "",   
    ].filter(function(item) {
      return  item.length>0;
    });

    if (rulesOpera == ""){
      this.setState({
        cPlyNoGroupType : true,
      })
      message.error("检查必填项！");
      return false;
     } else {
      this.setState({
        cPlyNoGroupType : false,
      })
      return true;
    }
  }

  cPlyNoGroupChange = () =>{
    let rulesOpera = [
      this.props.form.getFieldValue('startCPlyNo')||"",
      this.props.form.getFieldValue('endCPlyNo')||"",
      this.props.form.getFieldValue('cPlyNo')||"",
      this.props.form.getFieldValue('cPlyAppNo')||"",
    ].filter(function(item) {
      return  item.length>0;
    });
    if (rulesOpera == ""){
      this.setState({
        cPlyNoGroupType : true,
      })
     } else {
      this.setState({
        cPlyNoGroupType : false,
      })
    }
  }

  handleSearch = (e) => {
      e.preventDefault();
      this.setState({selectedRowKeys:[],selectedRows:[]});
      const { dispatch, form } = this.props;
      let selType = {};
      form.validateFields((err, fieldsValue) => {
        const types = {
          cRepairSendFlag: 0,
          cReprintFlag: 0,
          cExpressQuestionnaire: 0,
          printStatus: 1,
        }
        if (fieldsValue.selType !== '') {
          if (fieldsValue.selType === 'printStatus') {
            selType[fieldsValue.selType] = 3
          } else {
            selType[fieldsValue.selType] = 1
          }
        }
        if (err) return; 
        fieldsValue = this.perpareValues(fieldsValue);
        const payloadVlaue = {
          ...fieldsValue,
          ...types,
          ...selType,
        }
        this.setState({payloadVlaue,searchFieldsValue:payloadVlaue});
        dispatch({
          type: 'rule/guaranteeReprintQuery',
          payload: payloadVlaue,
        }).then(()=>{
          const {rule:{guaranteeReprintQueryData}} = this.props;
          this.setState({data:guaranteeReprintQueryData});
        });
      });
  };

   perpareValues(values){
    const { cRegisterDate } = values;
    let newValues = {};
    if(!cRegisterDate||cRegisterDate.length===0){
      newValues = {
        startcRegisterDate: '',
        endcRegisterDate: '',
        ...values,
      }
    }else{
      newValues = {
        startcRegisterDate: cRegisterDate[0].format("YYYY-MM-DD 00:00:00"),
        endcRegisterDate: cRegisterDate[1].format("YYYY-MM-DD 23:59:59"),
        ...values,
      }
    }
    delete newValues.startPage;
    delete newValues.endPage;
    delete newValues.endPage;
    delete newValues.cRegisterDate;
    delete newValues.selType;
    for(var key in newValues){
      if(newValues[key]===undefined||newValues[key]===""){
        newValues[key] = '';
      }
    }
    return newValues;
  }

  radioSelted = (e) => {
    if (e.target.value === 1) {
      this.setState({
        allPrint: 'true',
        radioTag: '1',
        endPage: '',
        startPage: '',
      });
    } else {
      this.setState({
        allPrint: 'false',
        radioTag: '2',
      });
    }
    this.setState({
      value: e.target.value,
    });
  }

  reprintPrint = () => {
    let typeArr = [];
    if (this.state.selectedRows) {
      for (let i in this.state.selectedRows) {
        typeArr.push(this.state.selectedRows[i].reprintType)
        if (this.state.selectedRows[i].reprintType === '2'){
          this.setState({
            reprintTypeFlag : '2'
          });
        } else {
          this.setState({
            reprintTypeFlag: '1'
          });
        }
      }
    }
    if (this.state.selectedRows.length > 1) {
      for (let i in this.state.selectedRows) {
        if (this.state.selectedRows[i].repairSendType !== '补充打印') {
           message.warning('请选择补寄类型为[补充打印]的数据进行打印');
        } else {
          this.setState({modalVisible:　true});
        }
      }
    } else if (this.state.selectedRows.length === 1) {
      if (this.state.selectedRows[0].repairSendType === '补充打印') {
        this.setState({modalVisible:　true});
      } else {
         message.warning('请选择补寄类型为[补充打印]的数据进行打印');
      }
    } else if (this.state.selectedRows.length === 0) {
      message.warning('请勾选要打印的数据');
    }
  }

  repairSendPrint = (printSource) => {
    const { rule: { guaranteeReprintQueryData }, dispatch,form } = this.props;
    const {selectedRows} = this.state;
    let data;
    if (selectedRows.length > 0 ) {
      data = selectedRows || []
    } else {
      data = guaranteeReprintQueryData;
    }

    if (this.state.selectedRows.length > 1) {
        for (const i in this.state.selectedRows) {
        if (this.state.selectedRows[i].repairSendType !== '补充打印') {
          dispatch({
            type: 'rule/guaranteeRepairSendPrint',
            payload: {guaranteeReprintDeals: data, printSource},
          }).then(()=>{
            setTimeout(()=>{
              this.props.dispatch({
                  type: 'rule/guaranteeReprintQuery',
                  payload: this.state.payloadVlaue,
                }).then(()=>{
                const {rule:{guaranteeReprintQueryData}} = this.props;
                this.setState({data:guaranteeReprintQueryData,selectedRowKeys: [], selectedRows: []});
              });
            },200);
        });
        } else {
          message.warning('请选择补寄类型为[补寄]的数据进行打印');
        }
      } 
    } else if (selectedRows.length === 1) {
      if (this.state.selectedRows[0].repairSendType !== '补充打印') {
        dispatch({
          type: 'rule/guaranteeRepairSendPrint',
          payload: {
            guaranteeReprintDeals: data,
            printSource,
          },
        }).then(()=>{
           setTimeout(()=>{
              this.props.dispatch({
                  type: 'rule/guaranteeReprintQuery',
                  payload: this.state.payloadVlaue,
                }).then(()=>{
                const {rule:{guaranteeReprintQueryData}} = this.props;
                this.setState({data:guaranteeReprintQueryData,selectedRowKeys: [], selectedRows: []});
              });
            },200);
        });
      } else {
        message.warning('请选择补寄类型为[补寄]的数据进行打印');
      }
    } else if(this.state.selectedRows.length === 0 && this.state.selectedType === 'cRepairSendFlag') {
        dispatch({
          type: 'rule/guaranteeRepairSendPrint',
          payload: {guaranteeReprintDeals: data, printSource},
        }).then(()=>{
          setTimeout(()=>{
            this.setState({data:[],selectedRowKeys: [], selectedRows: []});
          },200);
        });
    }
  }

  guaranteeReprintExport = () => {
    this.props.dispatch({
      type: 'rule/guaranteeReprintExport',
      payload: this.state.searchFieldsValue,
    });
  }

  deleteGuaranteeReprintInfo = () => {
    const {selectedRows} = this.state;
    let data;
    if (selectedRows.length > 0 ) {
      data = selectedRows || []
    } else {
      message.warning('请勾选要删除的数据');
    }
    this.props.dispatch({
      type: 'rule/deleteGuaranteeReprintInfo',
      payload: {guaranteeReprintDeals: data},
    });
  }

  selOption = (val) => {
    this.setState({
      selPrintTypes: val
    });
    const item = new Array(val)
    if (val) {
      if (val === []) {
         this.setState({printFlag: ''});
      }
      if (val.indexOf("cGuarPrint") >= 0) {
        this.setState({disableFlag: '0'})
      } else {
         this.setState({disableFlag: '1'});
      }
      
      if (val.indexOf('cExpressPrint') >= 0) {
        this.setState({printFlag: '0'});
      } else if (val.indexOf('newExpressPrint') >= 0) {
        this.setState({printFlag: '1'});
      } else if (val.indexOf('cExpressPrint') === -1 || val.indexOf('newExpressPrint') === -1) {
        this.setState({printFlag: ''});
      } else {
        this.setState({printFlag: ''});
      }
    }
  }

  selRepintType = (val) => {
    if (val === '1' || val === '2') {
      this.setState({
        printStatus: false
      });
    }
  }

  selectedType = (val) => {
    this.setState({selectedType:val});
  }

  renderAdvancedForm() {
    const { getFieldDecorator,getFieldValue } = this.props.form;
    const { loading } = this.props;
    return (
      <Form className='custom-form-wrapper'  onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 24, lg: 24, xl: 24 }}>
          <Col md={12} lg={12} sm={12}>
            <FormItem label="保单号">
              {getFieldDecorator('cPlyNo',{
                 rules: [{
                    max: 30, message: '请输入正确的格式',
                  }],
              },{initialValue: ''})(
                <Input  placeholder='请输入'/>
                )}
            </FormItem>
          </Col>
          <Col md={12} lg={12} sm={12}>
            <FormItem label="补寄资料">
              {getFieldDecorator('cGuarPrint')(
                <Select placeholder='请选择'>
                  <Option value='1'>含保单</Option>
                  <Option value='0'>不含保单</Option>
                </Select>
                )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 24, lg: 24, xl: 24 }}>
        <Col md={12} lg={12} sm={12}>
            <FormItem label="补寄类型">
              {getFieldDecorator('selType',{initialValue: '',rules:[{required: true, message: '必选!'}]})(
                <Select  placeholder="请选择" style={{ width: '100%' }} onChange={this.selectedType}>
                  <Option value='cRepairSendFlag'>补寄</Option>
                  <Option value='cReprintFlag'>补充打印</Option>
                  <Option value='cExpressQuestionnaire'>快递问题件</Option>
                  <Option value='printStatus'>电子保单生成失败</Option>
                </Select> 
                )}               
            </FormItem>
          </Col>
          <Col md={12} lg={12} sm={12}>
            <FormItem label="补发登记日期">
              {getFieldDecorator('cRegisterDate',{initialValue: ''})(
                <RangePicker />
                )}
            </FormItem>
          </Col> 
        </Row>
        <div className={styles.reprintManageOperator} style={{ overflow: 'hidden',textAlign:'center' }}>
          <span style={{ marginBottom: 24 }}>
            <Button loading={loading} type="primary" htmlType="submit">
                  查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
            </Button>
          </span>
        </div>
      </Form>
    );
  }

  handleReprintManageTableChange = () => {
    this.setState({ selectedRowKeys: [], selectedRows: []});
  }

  render() {
    const { rule: { guaranteeReprintQueryData }, loading } = this.props;
    const {selectedRowKeys, selectedRows,data} = this.state;
    const { getFieldDecorator } = this.props.form;
    let total;
    const rowSelection = {
      selectedRowKeys, 
      selectedRows,
       onChange: (selectedRowKeys, selectedRows) => {
          this.setState({
            selectedRows,
            selectedRowKeys,
          });
        },
    };

    const date = new Date();
    const pyear = date.getFullYear();

    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
    };

    const rowKey = (record,index) => {
      return index
    }

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
            <div>{this.renderAdvancedForm()}</div>
            <div className={styles.reprintManageOperator}>
              <Button loading={loading} onClick={this.reprintPrint}>补充打印</Button>
              <Button loading={loading} onClick={this.repairSendPrint.bind(this, 'PA')}>AC2060补寄打印</Button>
              <Button loading={loading} onClick={this.repairSendPrint.bind(this, 'PB')}>备注机补寄打印</Button>
              <Button loading={loading} onClick={this.guaranteeReprintExport}>清单导出</Button>
              <Button onClick={this.deleteGuaranteeReprintInfo} type="primary">删除</Button>
            </div>
            <Table
              loading={loading}
              scroll={{x:1800}}
              rowKey={rowKey}
              dataSource={data}
              bordered
              size="small"
              columns={columns}
              rowSelection={rowSelection}
              onChange={this.handleReprintManageTableChange}
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                total:total,
                pageSizeOptions:['10','20','30','40','50'],
              }}
            />
        </Card>
        <Modal
          title=""
          visible={this.state.modalVisible}
          onOk={this.okHandle}
          onCancel={this.handleModalVisible}
        >
          <FormItem label="">
            <span style={{marginRight: 8}}>打印类型:</span>
             {getFieldDecorator('printType')(
              <Select placeholder="请选择" style={{ width: '40%' }} onChange={this.selOption} mode="multiple">
                <Option value='cGuarPrint'>保单</Option>
                <Option value='cExpressPrint' >
                    原快递单补打
                </Option>
                <Option value='cReceiptPrint'>回执单</Option>
              </Select>
              )}
        </FormItem>
        <FormItem>
            <RadioGroup 
              initialValue = {this.state.radioTag}
              onChange={this.radioSelted}
              style={{display:this.state.disableFlag == '0' ? 'block' : 'none'}}>
            <Radio value={1} style={radioStyle}>打印全部</Radio>
            <Radio value={2} style={radioStyle}>
              <span style={{marginRight: 8}}>保单打印页数:</span>
              {getFieldDecorator('startPage',{initialValue: this.state.startPage})
              (<Input  style={{width: 40}}/>)}页
              <span style={{marginLeft: 3,marginRight: 3}}>--</span>
              {getFieldDecorator('endPage',{initialValue: this.state.endPage})
              (<Input style={{width: 40}} />)}页
            </Radio>
          </RadioGroup>
        </FormItem>
        </Modal>
      </PageHeaderLayout>
    );
  }
}