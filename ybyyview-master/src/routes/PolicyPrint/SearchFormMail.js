import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  Input,
  DatePicker,
} from 'snk-web';
import ChannelAndProduction from 'components/ChannelAndProduction';
import { getSelectChild } from '../../utils/utils'
import styles from './MailDetail.less';


const FormItem = Form.Item;
const { Option } = Select;
const InputGroup = Input.Group;


@Form.create()
export default class SearchFormMail extends PureComponent {
  
  constructor(props){
    super(props);
    this.state = {
      
    };
    props.root.nomalForm = props.form;
  }

  handleSearch(e){
    this.props.root.handleSearch(e,this.props.form);
  }
 
  render() {
    const { form,root } = this.props;
    const { getFieldDecorator,getFieldValue } = form;
    const { RangePicker } = DatePicker;

    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper'  onSubmit={this.handleSearch.bind(this)} layout="inline">
        <ChannelAndProduction
          oldEnterpCodeMultiple = {true}
          prodNoMultiple = {true}
          channelFieldKey="oldEnterpCodeList"
          productionFieldKey="prodNoList"
          {...this.props} allowClear
        />
        <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="保单年份">
              {getFieldDecorator('pyear',{initialValue:pyear})(
                <Select allowClear placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild('pyear')}
                </Select>
                )}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="发送状态">
              {getFieldDecorator('sendStatus')(
                <Select placeholder='请选择'>
                  <Option value='0'>未发送</Option>
                  <Option value='1'>发送成功</Option>
                  <Option value='4'>发送失败</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="投保日期">
              {getFieldDecorator('appDate')(
                <RangePicker style={{ width: '100%' }}  />
                  )}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="保险起期">
              {getFieldDecorator('insrncBeginDate')(
                <RangePicker style={{ width: '100%' }}  />
                  )}
            </FormItem>
          </Col>
        </Row>
        <div className={styles.mailDetailOperator} style={{ overflow: 'hidden',textAlign:'center' }}>
          <span style={{ marginBottom: 50 }}>
            <Button type="primary" htmlType="submit">
                  查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
                  重置
            </Button>
          </span>
        </div>
      </Form>
    );
  }
}
