import React, { PureComponent } from 'react';
import {
  Row,
  Col,Form,
  Select,
} from 'snk-web';
import { getSelectChild,getDataSource } from '../../../utils/utils';
import {formItemLayout} from './printManangeconfig';

const FormItem = Form.Item;

export default class ExtEnterAndPro extends PureComponent {
  state = {
    code:undefined,// 因为使用和渠道名称 同一个key会导致onChange事件不调用
    code1:undefined
  };
 
  getRiskOption(){
    const curRiskCode = this.props.form.getFieldValue(this.props.channelFieldKey||'extEnterpNameList')||[];
    if(this.props.oldEnterpCodeMultiple){
      var re = [];
      for(var i=0,j=curRiskCode.length;i<j;i+=1){
        re.push(
          getSelectChild('prodDesc',null,(index,source)=>{
            if(source[index].group!==curRiskCode[i]){
              return false;
            }
            if(this.props.productOnSale){
              if(source[index].state!=='在售'){
                return false;
              }
            }
            return true;
          },true)
        );
      }
      return re;
    }
    return getSelectChild('prodDesc',null,(index,source)=>{
      if(source[index].group!==curRiskCode){
        return false;
      }
      if(this.props.productOnSale){
        if(source[index].state!=='在售'){
          return false;
        }
      }
      return true;
    },true);
  }

  extEnterChange(value){
    const re = {};
    re[this.props.productionFieldKey||'prodNameList'] = undefined;
    this.props.form.setFieldsValue(re);
    if(this.props.onChannelChange){
      this.props.onChannelChange(value);
    }
  }

  productionChange(value){
    // this.setState({
    //     code1:value,
    // });
  }

  
  render() {
      
    const { getFieldDecorator } = this.props.form;
    let commonProps = {};
    if(this.props.allowClear){
      commonProps.allowClear = true;
    }
    const channelData = getSelectChild('extEnterpDesc',null,(index,record)=>{
      if(this.props.productOnSale){
        const proData = getDataSource("selectSource")["prodDesc"];
        let re = false;
        for(let i=0,j=proData.length;i<j;i+=1){
          if(proData[i].group===record[index].code){
            if(proData[i].state==='在售'){
              re = true;
              break;
            }
          }
        }
        return re;
      }
      return true;
    },true);

    let oldEnterpCodeProps = {};
    let prodNoProps = {};
    if(this.props.oldEnterpCodeMultiple){
      oldEnterpCodeProps.mode = 'multiple';
    }
    
    if(this.props.prodNoMultiple){
      prodNoProps.mode = 'multiple';
    }

    return (
      <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
        <Col md={10} sm={24}>
          <FormItem label="业务渠道" {...formItemLayout}>
            {getFieldDecorator(this.props.channelFieldKey||'extEnterpNameList',this.props.ChannelRule||{})(
              <Select
                onChange={this.extEnterChange.bind(this)}
                placeholder="请选择" 
                {...commonProps} {...oldEnterpCodeProps}
                style={{ width: '100%' }}
              >
                {channelData}
              </Select>
                )}
          </FormItem>
        </Col>
        <Col md={10} sm={24}>
          <FormItem label="产品名称" {...formItemLayout}>
            {getFieldDecorator(this.props.productionFieldKey||'prodNameList',this.props.productionRule||{})(
              <Select
                onChange={this.productionChange.bind(this)}
                {...commonProps} {...prodNoProps} placeholder="请选择" style={{ width: '100%' }} >
                {this.getRiskOption()}
              </Select>
                )}
          </FormItem>
        </Col>
      </Row>
    );
  }
}
