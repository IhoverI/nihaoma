import {Popconfirm, Divider,} from 'snk-web';
import {getSelectChild} from '../../../utils/utils'
//表单布局
export const formItemLayout = {
  labelCol: {
    xs: { span: 12 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 12 },
    sm: { span: 16 },
  },
};
// 保单年号选项
export const getPyearListOptions = () => {
  const curYear = new Date().getFullYear();
  const result = [];
  for(let i= curYear;i>=2005; i--){
    result.push(i);
  }
  return result;
}
// 渠道名称选项
export const extEnterpCodeOptions =[
{name: '招商银行',code: '95555'},
{name: '民生银行', code: '95568'},
{name: '建设银行', code: '95533'},
{name: '盛大', code: '80001'},
{name: '江苏邮政', code: '95580'},
{name: '深圳发展银行', code: '95501'},
{name: '深圳陌call', code: '10010'},
{name: '华安保险分公司推广', code: '95556'},
{name: '微信出单', code: '11002'},
{name: '汇金保联', code: '11001'},
{name: '中信银行', code: '95558'}, 
{name: '转银联', code: '95516'},
{name: '转快钱', code: '90001'},
{name: '南昌电话中心',code:'11004'},
{name: '成都自建', code: '11003'},
];

const getExtEnterpName = (extEnterNo) => {
  let extEnterName;
  if (getSelectChild('payChannel1')) {
    for (const i in getSelectChild('payChannel1')) {
      if (getSelectChild('payChannel1')[i].props.value === extEnterNo) {
        extEnterName = getSelectChild('payChannel1')[i].props.children;
      }
    }
    return extEnterName;
  }
}

const formaUnit = (val) => {
  let text = '';
  switch (val) {
    case 'E1003': text = '顺丰'; break;
    case 'E1010': text = '德邦'; break;
    case 'E1008': text = 'EMS'; break;
    case 'E1000': text = '地址不全'; break;
    default: break;
  }
    return text;
}
// 表单头
export const getPrintReportColumns=(onConfirm)=>{
  return(
    [
      {
        title: '保单号',
        dataIndex: 'cPlyNo',
        align: 'center',
        render: (text,record) => <a href={`/#/query-manage/policy-list?cPlyNo=${record.cPlyNo}&prePath=/policy-print/print-manage`}>{text}</a>
      },
      {
        title: '渠道',
        dataIndex: 'extEnterpCode',
        align: 'center',
         render:(val)=>{
          return (
            <span>{getExtEnterpName(val)}</span>
          )
        },
      },
      {
        title: '产品名称',
        dataIndex: 'prodName',
        align: 'center',
      },
      {
        title: '投保人姓名',
        dataIndex: 'cAppNme',
        align: 'center',
      },
      {
        title: '被保人姓名',
        dataIndex: 'cInsrntCnm',
        align: 'center',
      },
      {
        title: '保险起期',
        dataIndex: 'insrncBeginDate',
        align: 'center',
      },
      {
        title: '联系电话', 
        dataIndex: 'cMobileNo',
        align: 'center',
      },
       {
        title: '快递公司',
        dataIndex: 'expressCode',
        align: 'center',
         render: (text) => {
          return (<span>{formaUnit(text)}</span>);
        }
      },
      {
        title: '投递地址',
        dataIndex: 'insrncAddress',
        align: 'center',
      },
       {
        title: '操作',
        dataIndex: 'operation',
        align: 'center',
        width: 200,
        render:(text,record,index) => {
            return (
              <span>
                  <Popconfirm  
                    title={"请确认是否转EMS快递寄送？"} 
                    onConfirm={e=>onConfirm(e,record,'E1008')} 
                    okText="确定" 
                    cancelText="取消"
                  >
                    <a href='#'
                    style= {{display : record.expressCode==='E1008' ? 'none' : ''}}>
                    转EMS
                    </a>
                </Popconfirm>
                <Divider
                 type="vertical"
                 style= {{display : record.expressCode==='E1008' || record.expressCode==='E1000'  ? 'none' : ''}}
                />
                <Popconfirm 
                  title={"请是否标记为地址不全？"} 
                  onConfirm={e=>onConfirm(e,record,'E1000')}
                  okText="确定" 
                  cancelText="取消"
                >
                  <a href='#' 
                  style= {{display : record.expressCode==='E1000' ? 'none' : ''}}>
                  地址不全
                  </a>
                </Popconfirm>
              </span>
            );
        }
      },
    ]
  )
}