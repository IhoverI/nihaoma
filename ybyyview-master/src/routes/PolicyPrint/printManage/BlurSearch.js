import React from 'react';
import { connect } from 'dva';
import {
  Form,
  Row,
  Col,
  Button,
	Input,
	Select,
	DatePicker,
	message
} from 'snk-web';
import ExtEnterAndPro from './ExtEnterAndPro';
import { formItemLayout, getPyearListOptions, extEnterpCodeOptions } from './printManangeconfig';
const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const InputGroup = Input.Group;

@connect(({ loading }) => ({
  loading: loading.effects,
}))
@Form.create()
export default class PreciseSearch extends React.Component { 
		//重置表单
    handleReset = () => {
			this.props.form.resetFields();
		}
		handleSearch = e => {
				//防止链接打开 URL
				if(e)e.preventDefault();
				const { dispatch, form } = this.props;
				//表单校验
				form.validateFields((err, fieldsValue) => {
						if (err) return;
						console.log('fieldsValue',fieldsValue);
						const { InsrncDate,applyDate, ...restParams } = fieldsValue;
						// 保险起期
						let startInsrncBeginDate;
						let endInsrncBeginDate;
						if(InsrncDate){
							startInsrncBeginDate = InsrncDate[0].format("YYYY-MM-DD 00:00:00");
							endInsrncBeginDate = InsrncDate[1].format("YYYY-MM-DD 23:59:59");
						}
						// 投保日期
						let startTime;
						let endTime;
						if(applyDate){
							startTime = applyDate[0].format("YYYY-MM-DD 00:00:00");
							endTime = applyDate[1].format("YYYY-MM-DD 23:59:59");
						}
						const params = {...restParams,startInsrncBeginDate,endInsrncBeginDate,startTime,endTime}
						if(this.props.onSearch) this.props.onSearch(params);
				});
		}
		pyearChange = (value) => {
			if(value.length>=4){
				value.shift();
				message.warning("查询条件保单年份最多支持跨三年查询。");
			}
		}
    render() {
				const { getFieldDecorator, getFieldsValue } = this.props.form;
				const pyear = new Date().getFullYear();
				const { extEnterpCode } = getFieldsValue(['extEnterpCode']) || {};

        return (
          <Form onSubmit={this.handleSearch}>
            <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
							<Col md={10} sm={24}>
								<FormItem
									label="保单年份"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('pyear',{
												initialValue: pyear
											})(
												<Select
													placeholder="请选择"
												>
													{
														getPyearListOptions().map((item)=>(
															<Select.Option key={item} value={item}>{item}</Select.Option>
														))
													}
												</Select>
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="快递公司"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('expressCode',{
												initialValue: 'E1003'
											})(
												<Select
													placeholder="请选择"
												>
												 	<Select.Option value="E1003">顺丰</Select.Option>
                  				<Select.Option value="E1010">德邦</Select.Option>
                  				<Select.Option value="E1008">EMS</Select.Option>
                  				<Select.Option value="E1000">地址不全</Select.Option>
												</Select>
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="保险起期"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('InsrncDate')(
												<RangePicker style={{width: '100%'}}/>
											)
										}
									</FormItem>
							</Col>
							<Col md={10} sm={24}>
								<FormItem
									label="投保日期"
									{...formItemLayout}
									style={{marginBottom:'0'}}
									>
										{
											getFieldDecorator('applyDate')(
												<RangePicker style={{width: '100%'}}/>
											)
										}
									</FormItem>
							</Col>
            </Row>
						<ExtEnterAndPro 
								channelFieldKey="extEnterpCode"
								productionFieldKey="prodNo"
								formItemLayout={formItemLayout}
								{...this.props}
						/>
						<Row gutter={{ md: 6, lg: 24, xl: 48 }}>
							<Col md={10} sm={24}>
								<FormItem
										label="保单号区间"
										{...formItemLayout}
										style={{marginBottom:'0'}}
								>
									<Col span={11}>
												<FormItem >
														{
															getFieldDecorator('startCPlyNo',{})
															(
																<Input placeholder="最小值" />
															)
														} 
												</FormItem>
										</Col>
										<Col span={2}>
												<span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
												-
												</span>
										</Col>
										<Col span={11}>
												<FormItem >
														{
															getFieldDecorator('endCPlyNo',{})
														(
															<Input placeholder="最大值" />
														)}
												</FormItem >
										</Col>
								</FormItem>
							</Col>
							{
								extEnterpCode === '95555' &&
								<Col md={10} sm={24}>
									<FormItem
										label="合规件标记"
										{...formItemLayout}
										style={{marginBottom:'0'}}
										>
											{
												getFieldDecorator('cChannelFlag',{
													rules: [{required: true , message: '请选择合规标记!'}]
												})(
													<Select
														placeholder="请选择"
														disabled={!extEnterpCode}
													>
														<Select.Option value="1">合规</Select.Option>
														<Select.Option value="2">不合规转合规</Select.Option>
													</Select>
												)
											}
										</FormItem>
								</Col>
							}
							
						</Row>
						<Row>
							<Col style={{textAlign: 'center'}}>
								<Button
									 type="primary"
									 htmlType="submit"
									 loading={this.props.loading['printManage/searchPrintQuery']}
									 disabled={this.props.loading['printManage/searchPrintQuery']}
								>
										搜索
								</Button>
								<Button style={{ marginLeft: 10 }} onClick={this.handleReset}>
										重置
								</Button>
							</Col>
					</Row>
        </Form>
        );
    }
}