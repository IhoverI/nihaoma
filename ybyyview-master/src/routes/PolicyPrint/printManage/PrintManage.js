import React, { Fragment } from 'react';
import { connect } from 'dva';
import { Form, Card, Tabs, Table, Button, Popconfirm } from 'snk-web';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import PreciseSearch from './PreciseSearch';
import BlurSearch from './BlurSearch';
import { getPrintReportColumns, } from './printManangeconfig';

const { TabPane } = Tabs;
@connect(({printManage,loading}) => ({
	printManage,
	loading: loading.effects,
}))
@Form.create()
export default class PrintManage extends React.Component {
	state={
    isHadExportAddress: false,
	}

	//发送请求--列表
	 onFetchFinanceList=(params)=>{
      this.fieldsValue = params;
      this.props.dispatch({
        type:'printManage/searchPrintQuery',
        payload:{...params},
      });
      // 如果地址不全已经状态为true
      if(this.state.isHadExportAddress){
        this.setState({isHadExportAddress: false})
      }
   }
   // 批量打印
   onHandlePolicyPrint=(printSource)=>{
    const { PrintQueryData = [], } = this.props.printManage || {};
    const {pyear,}= this.fieldsValue || {};
    let cPlyNos = [];
    for (const i in PrintQueryData) {
      if (i) {
        cPlyNos.push(PrintQueryData[i].cPlyNo)
      } 
    }
    this.props.dispatch({
      type: 'printManage/toMakeGuarantee',
      payload: {cPlyNos, pyear, printSource,} ,
    })
   }
   // 导出地址不全
   onHandleExportAddress=()=>{
      const { PrintQueryData = [], } = this.props.printManage || {};
      this.props.dispatch({
        type: 'printManage/expressAddress',
        payload: {
          mainExpressDtos: PrintQueryData,
        },
      }).then(()=>{
          this.setState({isHadExportAddress: true})
      }).catch((e)=>{
        message.warning(e.message);
      });
   }
   // 快递公司转换
   turnExpressCode=(expressCode)=>{
    const { PrintQueryData = [], } = this.props.printManage || {};
    let cPlyNos = [];
    for (const i in PrintQueryData) {
      if (i) {
        cPlyNos.push(PrintQueryData[i].cPlyNo)
      } 
    }
    this.props.dispatch({
      type: 'printManage/turnExpressCode',
      payload: {
        cPlyNos,
        expressCode,
      }
    })
   }
   // 导出清单
   onHandleExportInfoList=()=>{
    const params = this.fieldsValue || {};
    this.props.dispatch({
      type: 'printManage/exportPrintExpressInfo',
      payload: params,
    })
   }

   // 操作
   onConfirm = (e,record,code) => {
    const params = this.fieldsValue || {};
    this.props.dispatch({
    type: 'printManage/updateEXpressCompany',
    payload: {
      expressCode: code,
      cPlyNo: record.cPlyNo
    }
  }).then(()=>{
    setTimeout(()=>{
      this.props.dispatch({
        type: 'printManage/searchPrintQuery',
        payload: params,
      })
    },200);
  });
}

   // 表单头部导航
   getTableColumns=()=>{
     return getPrintReportColumns(this.onConfirm);
   }
	 // search Form
  renderAdvancedForm = ()=>{
        return (
					<Tabs style={{marginBottom: '20px'}}>
						<TabPane tab="模糊查询" key="1">
								<BlurSearch onSearch={this.onFetchFinanceList}/>
						</TabPane>
						<TabPane tab="精确查询" key="2">
								<PreciseSearch onSearch={this.onFetchFinanceList}/>
						</TabPane>
					</Tabs>
          );
		}
		renderTableTitle=()=>{
      const { PrintQueryData = [], } = this.props.printManage || {};
      const {expressCode, cPlyNo}= this.fieldsValue || {};
      if(!PrintQueryData.length) return null;
      if(cPlyNo) return null;
			return(
				<div style={{margin: "10px 0"}}>
          {
            (expressCode !== 'E1000' || this.state.isHadExportAddress) &&
            <Fragment>
              <Button
                type='primary'
                style={{marginRight:10}} 
                onClick={this.onHandlePolicyPrint.bind(this, 'PA')}
                loading={this.props.loading['printManage/toMakeGuarantee']}
                disabled={this.props.loading['printManage/toMakeGuarantee']}
              >
                AC2060打印
              </Button>
                <Button
                type='primary'
                style={{marginRight:10}} 
                onClick={this.onHandlePolicyPrint.bind(this, 'PB')}
                loading={this.props.loading['printManage/toMakeGuarantee']}
                disabled={this.props.loading['printManage/toMakeGuarantee']}
              >
                备用机打印
              </Button>
            </Fragment>
          }
					
          {
            expressCode === 'E1000' &&
            <Button
              icon='download' 
              style={{marginRight:10}} 
              onClick={this.onHandleExportAddress}
              loading={this.props.loading['printManage/expressAddress']}
              disabled={this.props.loading['printManage/expressAddress']}
            >
              导出地址不全
            </Button>
          }
          {
            expressCode === 'E1003' &&
            <Popconfirm 
              title={"请是否确定要转德邦？"} 
              onConfirm={e=>this.turnExpressCode('E1010')}
              okText="确定" 
              cancelText="取消"
            > 
              <Button 
                type="primary"
                style={{marginRight:10}} 
                loading={this.props.loading['printManage/turnExpressCode']}
						    disabled={this.props.loading['printManage/turnExpressCode']}
              >
                转德邦
              </Button> 
            </Popconfirm>
          }
				  {
            expressCode === 'E1010' &&
            <Popconfirm 
              title={"请是否确定要转顺丰？"} 
              onConfirm={e=>this.turnExpressCode('E1003')}
              okText="确定" 
              cancelText="取消"
            > 
              <Button 
                type="primary"
                style={{marginRight:10}}
                loading={this.props.loading['printManage/turnExpressCode']}
						    disabled={this.props.loading['printManage/turnExpressCode']}
              >
                转顺丰
              </Button> 
            </Popconfirm>
          }
					<Button
						 icon='download' 
						 style={{marginRight:10}} 
             onClick={this.onHandleExportInfoList}
             loading={this.props.loading['printManage/exportPrintExpressInfo']}
             disabled={this.props.loading['printManage/exportPrintExpressInfo']}
					>
             导出清单
          </Button>
				</div>
			)
		}
    render() {
				const { PrintQueryData = [], } = this.props.printManage || {};
        return (
          <PageHeaderLayout>
              <Card bordered={false}>
                  {this.renderAdvancedForm()}
                  {this.renderTableTitle()}
                  <Table
                        bordered
                        columns={this.getTableColumns()}
                        loading={this.props.loading['printManage/searchPrintQuery']}
                        dataSource={PrintQueryData}
                        size="small"
                        rowKey={(record,index) => index}
                        scroll={{x:1500}}
                        pagination={{
                            pageSizeOptions: ['10','20','30','40','50'],
                            showQuickJumper: true,
                            showSizeChanger: true,
                            showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                        }}
                    />
              </Card>
          </PageHeaderLayout>
        );
    }
}