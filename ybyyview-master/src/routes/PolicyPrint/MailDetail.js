import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Card,
  message,
  Tabs,
  Table,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import SearchFormMail from './SearchFormMail';
import SearchExactFormMail from './SearchExactFormMail';

const columns = [
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
    render: (text, record) => <a href={`/#/query-manage/policy-list?cPlyNo=${record.cPlyNo}&prePath=/policy-print/mail-detail`}>{text}</a>
  },
  {
    title: '渠道',
    dataIndex: 'oldEnterpCode',
    align: 'center',
  },
  {
    title: '产品',
    dataIndex: 'prodNo',
    align: 'center',
  },
  {
    title: '投保人姓名',
    dataIndex: 'cAppName',
    align: 'center',
  },
  {
    title: '被保人姓名',
    dataIndex: 'cInsrntCnm',
    align: 'center',
  },
  {
    title: '电子邮箱',
    dataIndex: 'cEmail',
    align: 'center',
  },
  {
    title: '保单状态',
    dataIndex: 'flag',
    align: 'center',
  },
  {
    title: '保单类型',
    dataIndex: 'cElcFlag',
    align: 'center',
  },
  {
    title: '发送类型',
    dataIndex: 'sendType',
    align: 'center',
  },
  {
    title: '最新发送状态',
    dataIndex: 'sendStatus',
    align: 'center',
  }
];

@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
export default class MailDetail extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    formValues: {},
  };

  componentWillMount(){
    if(this.props.history.location.state){
      this.props.history.push({state: undefined});
    } else{
      Object.keys(this.props.rule).forEach(key => delete this.props.rule[key]);
    }
  }

  handleFormReset = (form) => {
    const { dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
  };

  handleSearch = ( e, form ) => {
    this.form = form;
    e.preventDefault();
    this.searchData(true,form);
   };

  searchData = ( isClickSearch, form ) => {
    if(!form){
      message.warning('非最新数据，请重新搜索查询。');
      return;
    }
    if (isClickSearch) {
      this.setState({
        pageNum: 1,
        pageSize: 10
      })
    }
    const { dispatch } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(!fieldsValue.appDate||fieldsValue.appDate.length===0){
        fieldsValue.appDateStart = null;
        fieldsValue.appDateEnd = null;
      }else{
        fieldsValue.appDateStart = fieldsValue.appDate[0].format("YYYY-MM-DD");
        fieldsValue.appDateEnd = fieldsValue.appDate[1].format("YYYY-MM-DD");
      }
      if(!fieldsValue.insrncBeginDate||fieldsValue.insrncBeginDate.length===0){
        fieldsValue.insrncBeginDateStart = null;
        fieldsValue.insrncBeginDateEnd = null;
      }else{
        fieldsValue.insrncBeginDateStart = fieldsValue.insrncBeginDate[0].format("YYYY-MM-DD");
        fieldsValue.insrncBeginDateEnd = fieldsValue.insrncBeginDate[1].format("YYYY-MM-DD");
      }
      if (isClickSearch) {
        fieldsValue.pageNum = 1;
        fieldsValue.pageSize = 10;
      }
      fieldsValue.pageNum = this.state.pageNum;
      fieldsValue.pageSize = this.state.pageSize;
      this.setState({
        formValues: fieldsValue
      })
      dispatch({
        type: 'rule/queryEmailSendDetail',
        payload: fieldsValue,
      }).catch(() => {
        message.error('网络异常，请稍后重试')
      });
    });
  };

  handleTableChange = (pagination, filtersArg, sorter) => {
    this.setState({
      pageSize: pagination.pageSize,
      pageNum: pagination.current,
    },()=>{
      this.searchData(false, this.form);
      }
    );
  };

  renderAdvancedForm() {
    return (
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab={<span>模糊查询</span>} key="1">
          <SearchFormMail {...this.props} root={this}/>
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span>精确查询</span>} key="2">
        <SearchExactFormMail {...this.props} root={this}/>
        </Tabs.TabPane>
      </Tabs>
    );
  }

  render() {
    const { rule: { sendEmailDetailResp }, loading } = this.props;
    const { selectedRows, modalVisible } = this.state;

    let list = [];
    let total = 1;
    if (sendEmailDetailResp) {
      if (sendEmailDetailResp.code == 0) {
        list = sendEmailDetailResp.data.list;
        total = sendEmailDetailResp.data.total;
      }
    }

    return (
      
      <PageHeaderLayout title="">
        <Card bordered={false}>
        <div>{this.renderAdvancedForm()}</div>
        <Table
              loading={loading}
              scroll={{ x: 1300 }}
              rowKey={(record, index) => index}
              dataSource={list}
              bordered={true}
              size='small'
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                current: this.state.pageNum,
                pageSize: this.state.pageSize,
                total:total,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                onChange: (page, pageSize) => this.handleTableChange(page, pageSize),
                showSizeChanger: true,
                pageSizeOptions:['10','20','30','40','50'],
              }}
              columns= {columns}
              onChange={this.handleTableChange}
            />
        </Card>
      </PageHeaderLayout>
    );
  }
}
