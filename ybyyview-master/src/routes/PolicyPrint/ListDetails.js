import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Table,
  Tabs
} from 'snk-web';
import ReprintManageTable from './components/ReprintManageTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './ReprintManage.less';
const FormItem = Form.Item;
const { Option } = Select;

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const InputGroup = Input.Group;

const _formatUnit = (val,code) => {
  let text = '';
  if (code === 'printStatus') {
    switch (val) {
      case '0': text = '待打印'; break;
      case '1': text = '打印成功'; break;
      case '2': text = '打印失败'; break;
      default: break; 
    }
  } else if (code === 'printForm') {
    switch (val) {
      case '1': text = '新单打印'; break;
      case '2': text = '补寄打印'; break;
      case '3': text = '补充打印'; break;
      default: break; 
    }
  }
    return text;
}

const columns = [
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '保单号年份', 
    dataIndex: 'pyear',
    align: 'center',
   },
  {
    title: '打印时间',
    dataIndex: 'printDate',
    align: 'center',
   },
  {
    title: '操作人员',
    dataIndex: 'operCode',
    align: 'center',
  },
   {
    title: '保单打印', 
    dataIndex: 'cGuarPrint',
    align: 'center',
  },
   {
    title: '快递面单打印',
    dataIndex: 'cExpressPrint',
    align: 'center',
  },
  {
    title: '回执单打印',
    dataIndex: 'cReceiptPrint',
    align: 'center',
  },
  {
    title: '打印批次号',
    dataIndex: 'printBatchNo',
    align: 'center',
  },
  {
    title: '打印状态',
    dataIndex: 'printStatus',
    align: 'center',
    render: (text, record, index) => {
      return <span>{_formatUnit(text,'printStatus')}</span>
    }
  },
   {
    title: '打印类型',
    dataIndex: 'printForm',
    align: 'center',
    render: (text, record, index) => {
      return <span>{_formatUnit(text,'printForm')}</span>
    }
  },
  {
    title: '已打印资料',
    dataIndex: 'printInfoData',
    align: 'center',
    width: 100
  },
  {
    title: '备注',
    dataIndex: 'remark',
    align: 'center',
    width: 100
  }
];

@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
export default class ListDetails extends PureComponent {
  state = {
    pageSize: 10,
    pageNum: 1,
  };

  componentWillMount(){
      Object.keys(this.props.rule).forEach(key => delete this.props.rule[key]);
  }
   handleFormReset = () => {
    const { dispatch, form } = this.props;
    form.resetFields();
  };


  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(1,10);  
  };

   perpareValues(values){
    if(!values.cRegisterDate||values.cRegisterDate.length===0){
      values.startcRegisterDate = '';
      values.endcRegisterDate = '';
    }else{
      values.startcRegisterDate = values.cRegisterDate[0].format("YYYY-MM-DD 00:00:00");
      values.endcRegisterDate = values.cRegisterDate[1].format("YYYY-MM-DD 23:59:59");
    }

    delete values.cRegisterDate;
    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = '';
      }
    }
    return values;
  }

  exportPrintInfos =  () => {
     const { dispatch, form } = this.props;
      let selType = {};
      let startPrintDate,endPrintDate;
      form.validateFields((err, fieldsValue) => {
        if (fieldsValue.printDate.length > 1) {
          startPrintDate = fieldsValue.printDate[0].format("YYYY-MM-DD 00:00:00");
          endPrintDate = fieldsValue.printDate[1].format("YYYY-MM-DD 23:59:59");
        } else {
          startPrintDate = null;
          endPrintDate = null;
        }
        dispatch({
          type: 'rule/exportPrintInfos',
          payload: {
            pageNum: 1,
            rowNum: 10,
            startPrintDate,
            endPrintDate,
            cPlyNo: fieldsValue.cPlyNo,
            printBatchNo: fieldsValue.printBatchNo,
            operCode: fieldsValue.operCode,
            cGuarPrint: fieldsValue.cGuarPrint,
            cExpressPrint: fieldsValue.cExpressPrint,
          },
        }).then(()=>{});
      });
  }

  renderAdvancedForm() {
    const { getFieldDecorator,getFieldValue } = this.props.form;
    return (
      <Form className='custom-form-wrapper'  onSubmit={this.handleSearch.bind(this)} layout="inline">
        <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
          <Col md={24} lg={8} sm={24}>
            <FormItem label="保单号">
              {getFieldDecorator('cPlyNo',{initialValue: ''})(
                <Input  placeholder='请输入'/>
                )}
            </FormItem>
          </Col>
           <Col md={24} lg={8} sm={24}>
            <FormItem label="打印日期">
              {getFieldDecorator('printDate',{initialValue: ''})(
                <RangePicker />
                )}
            </FormItem>
          </Col> 
          <Col md={24} lg={8} sm={24}>
            <FormItem label="保单状态">
              {getFieldDecorator('cGuarPrint')(
                <Select placeholder="请选择">
                  <Option value='0'>待打印</Option>
                  <Option value='1'>打印成功</Option>
                  <Option value='2'>打印失败</Option>
                </Select>
                )}
            </FormItem>
          </Col> 
        </Row>
         <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
          <Col md={24} lg={8} sm={24}>
            <FormItem label="打印批次号">
              {getFieldDecorator('printBatchNo',{initialValue: ''})(
                <Input  placeholder='请输入'/>
                )}
            </FormItem>
          </Col>
          <Col md={24} lg={8} sm={24}>
            <FormItem label="操作人">
              {getFieldDecorator('operCode',{initialValue: ''})(
                <Input  placeholder=''/>
                )}
            </FormItem>
          </Col>
          <Col md={24} lg={8} sm={24}>
            <FormItem label="快递面单状态">
              {getFieldDecorator('cExpressPrint')(
                <Select placeholder="请选择">
                  <Option value='0'>待打印</Option>
                  <Option value='1'>打印成功</Option>
                  <Option value='2'>打印失败</Option>
                </Select>
                )}
            </FormItem>
          </Col> 
        </Row>
        <div className={styles.reprintManageOperator} style={{ overflow: 'hidden',textAlign:'center' }}>
          <span style={{ marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
                  查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
            </Button>
          </span>
        </div>
      </Form>
    );
  }

  searchData = (pageNum,pageSize) => {
      const { dispatch, form } = this.props;
      let selType = {};
      let startPrintDate,endPrintDate;
      form.validateFields((err, fieldsValue) => {
        if (fieldsValue.printDate.length > 1) {
          startPrintDate = fieldsValue.printDate[0].format("YYYY-MM-DD 00:00:00");
          endPrintDate = fieldsValue.printDate[1].format("YYYY-MM-DD 23:59:59");
        } else {
          startPrintDate = null;
          endPrintDate = null;
        }
        dispatch({
          type: 'rule/queryPrintInfos',
          payload: {
            pageNum,
            rowNum: pageSize,
            startPrintDate,
            endPrintDate,
            cPlyNo: fieldsValue.cPlyNo,
            printBatchNo: fieldsValue.printBatchNo,
            operCode: fieldsValue.operCode,
            cGuarPrint: fieldsValue.cGuarPrint,
            cExpressPrint: fieldsValue.cExpressPrint,
          },
        }).then(()=>{});
      });
  }

  handleTablePage =(pagination, filters, sorter) => {
    this.setState({
      rowNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }

  render() {
    const { rule: { queryPrintInfosData }, loading } = this.props;
    const { getFieldDecorator } = this.props.form;
    let total,data;
    if (queryPrintInfosData) {
      if (queryPrintInfosData.code == 0) {
        data = queryPrintInfosData.data.list;
        total = queryPrintInfosData.data.total;
      } else {
        data = [];
        total = 0;
      }
    }
    
    const rowKey = (record,index) => {
      return index
    }

    return (
      <div>
      <PageHeaderLayout title="">
        <Card bordered={false}>
            <div style={{marginBottom: 8}}>{this.renderAdvancedForm()}</div>
            <Button onClick={this.exportPrintInfos} type="primary">导出清单</Button>
            <Table
              loading={loading}
              scroll={{x:1600}}
              rowKey={rowKey}
              dataSource={data}
              bordered
              size="small"
              columns={columns}
              onChange={this.handleTablePage}
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                total:total,
                pageSizeOptions:['10','20','30','40','50'],
              }}
            />
        </Card>
      </PageHeaderLayout>
      
      </div>
    );
  }
}