import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
} from 'snk-web';
import styles from './PrintManage.less';

const FormItem = Form.Item;

@Form.create()
export default class SearchExactFormReprint extends PureComponent {
  handleExactSearch(e){
    this.props.root.handleSearch(e,this.props.form);
  }
  render() {
    const { form,root } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Form className='custom-form-wrapper'  onSubmit={this.handleExactSearch.bind(this)} layout="inline">
        <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="保单号">
              {getFieldDecorator('cPlyNo',
              {rules: [{ max: 30, message: '请输入正确的保单号' }]},
              {initialValue: '', })
                  (<Input placeholder="" />)}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="投保单号">
              {getFieldDecorator('cPlyAppNo',
              {rules: [{ max: 30, message: '请输入正确的投保单号' }]},
              {initialValue: ''})
                  (<Input placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <div className={styles.reprintManageOperator} style={{ overflow: 'hidden',textAlign:'center' }}>
          <span style={{ marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
                  查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
                  重置
            </Button>
          </span>
        </div>
      </Form>
    );
  }
}
