import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  Input,
  DatePicker,
} from 'snk-web';
import { getSelectChild } from '../../utils/utils'
import styles from './ReprintManage.less';


const FormItem = Form.Item;
const { Option } = Select;
const InputGroup = Input.Group;



@Form.create()
export default class SearchFormReprint extends PureComponent {
  
  constructor(props){
    super(props);
    this.state = {
      
    };
    props.root.nomalForm = props.form;
  }

  handleSearch(e){
    this.props.root.handleSearch(e,this.props.form);
  }
 
  render() {
    const { form,root } = this.props;
    const { getFieldDecorator,getFieldValue } = form;
    const { RangePicker } = DatePicker;

    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper'  onSubmit={this.handleSearch.bind(this)} layout="inline">
        <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="保单年份">
              {getFieldDecorator('pyear',{initialValue: pyear})(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild('pyear')}
                </Select>
                )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="补打类型">
              {getFieldDecorator('reprintType',{initialValue: ''})(
                <Select  placeholder="请选择" style={{ width: '100%' }} onChange={this.selRepintType}>
                  {getSelectChild('reprintTypeDesc')}
                </Select> 
                )}               
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="渠道名称">
              {getFieldDecorator('extEnterpCodeList',{initialValue: []})(
                <Select
                  mode='multiple' 
                  placeholder="请选择"
                  style={{ width: '100%' }}
                >
                  {getSelectChild('extEnterpDesc',null,null,true)}
                </Select>
                )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="保单号区间">
              <InputGroup compact>
                {getFieldDecorator('startCPlyNo',{initialValue: '', rules: [{ max: 30, message: '请输入正确的投保单号' }]})(
                  <Input style={{ width :'42%', textAlign: 'center' }} placeholder="最小值" />
              )}
                <Input style={{ width :'16%', borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="~" disabled />
                {getFieldDecorator('endCPlyNo',{initialValue: '', rules: [{ max: 30, message: '请输入正确的投保单号' }]})(
                  <Input style={{ width :'42%', textAlign: 'center', borderLeft: 0 }} placeholder="最大值" />
              )}
              </InputGroup>
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="投保日期">
              {getFieldDecorator('applyDate',{initialValue: ''})(
                <RangePicker style={{ width: '100%' }} onChange={this.selApplyDate} />
                  )}
            </FormItem>
          </Col>
        </Row>
        <div className={styles.reprintManageOperator} style={{ overflow: 'hidden',textAlign:'center' }}>
          <span style={{ marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
                  查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
                  重置
            </Button>
          </span>
        </div>
      </Form>
    );
  }
}
