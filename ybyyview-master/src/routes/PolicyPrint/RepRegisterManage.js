import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
	Form,
	Input,
	Row,
	Col,
	DatePicker,
	Card,
	Button,
	Table,
  Modal,
	Select,
	Popconfirm,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import moment from 'moment';
import {getChannelNameByValue,getProductNameByValue,getProductNameByExtEnterpCodeAndProdNo } from '../../utils/utils'

const { Option } = Select;
const FormItem =Form.Item;
const { RangePicker } = DatePicker;
const _formatUnit = (val,code) => {
  let text = '';
  if (code === 'flag') {
    switch (val) {
      case '1': text = '有效'; break;
      case '2': text = '注销'; break;
      case '3': text = '退保'; break;
      case '4': text = '过期'; break;
      default: break; 
    }
  } else if (code === 'reprintType') {
    switch (val) {
      case '1': text = '补打'; break;
      case '2': text = '补寄'; break;
      case '3': text = '电子邮件'; break;
      default: break; 
    }
  } else if (code === 'expressCode') {
    switch (val) {
      case 'E1003': text = '顺丰'; break;
      case 'E1010': text = '德邦'; break;
      case 'E1008': text = 'EMS'; break;
      default: break; 
    }
  } else {
    switch (val) {
      case '1': text = '待补打'; break;
      case '2': text = '已补打'; break;
      case '3': text = '补打失败'; break;
      default: break; 
    }
  }
    return text;
}

@connect(({umssouserinfo, rule, loading }) => ({
  currentUserName: umssouserinfo.currentUser.principal.name,
  rule,
  loading: loading.models.rule,
}))
@Form.create()

export default class RepRegisterManage extends PureComponent {
	state = {
		modalVisible: false,
		pageSize: 10,
		pageNum: 1,
		cPlyNoId: '',
	}
	handleSearch = ( e ) => {
		if (e) {
			e.preventDefault();
		}
		const { form } = this.props;
		form.validateFields((err, fieldsValue) => {
			if (err) {
				return false;
			}
			let searchForm = {
				cPlyNo: fieldsValue.cPlyNo,
				registerNum: fieldsValue.registerNum,
      };
			if(fieldsValue.cRegisterDate && fieldsValue.cRegisterDate.length) {
				searchForm = {
					...searchForm,
					startcRegisterDate: moment(fieldsValue.cRegisterDate[0]).format('YYYY-MM-DD 00:00:01'),
					endcRegisterDate: moment(fieldsValue.cRegisterDate[1]).format('YYYY-MM-DD 23:59:59'),
				}
      }
      this.props.dispatch({
        type: 'rule/queryGuaranteeReprint',
        payload: {
          ...searchForm,
        }
      })
		})
	 };

	handleFormReset = () => {
    this.props.form.resetFields();
	};
	recordEdit = (record)=> {
		this.setState({
			modalVisible: true,
			cPlyNoId: record.id,
    })
    let repInfoArr = [];  // 补寄资料
    if (record.cGuarPrint === '1') {
      repInfoArr = [...repInfoArr, 'cGuarPrint'];
    } 
    if (record.cReceiptPrint === '1') {
      repInfoArr = [...repInfoArr, 'cReceiptPrint'];
    }
    if (record.cRiderPrint === '1') {
      repInfoArr = [...repInfoArr, 'cRiderPrint'];
    }
    if (record.cInvoicePrint === '1') {
      repInfoArr = [...repInfoArr, 'cInvoicePrint'];
    }
		this.props.form.setFieldsValue({
      cAppNme: record.cAppNme,
      cPlyNo: record.cPlyNo,
			cMobileNo: record.cMobileNo,
			sendAddressD: record.sendAddress,
      repInfo: repInfoArr,
      cRemark: record.cRemark,
    })
	}
	recordDelete = (id) => {
		this.props.dispatch({
      type: 'rule/deleteGuaranteeReprint',
      payload: {
        id,
      },
    }).then(() => {
			this.handleSearch();
		});
	}
	handleModalVisible = () => {
		this.setState({
			modalVisible: false,
		})
	}
	handleTableChange = (pagination) => {
    this.setState({
      pageSize: pagination.pageSize,
      pageNum: pagination.current,
    });
	};
	okHandle = () => {
		const { form } = this.props;
		form.validateFields((err, fieldsValue) => {
			if (err) {
				return false;
      }
      let repInfoObj = {
        cGuarPrint: '0',
        cReceiptPrint: '0',
        cRiderPrint: '0',
        cInvoicePrint: '0',
      };
      if (fieldsValue.repInfo.length) {
        fieldsValue.repInfo.map((item) => {
          repInfoObj[item] = '1';
        });
			}
			let addressStr = '';
			if (fieldsValue.sendAddressA) {
				addressStr += `${fieldsValue.sendAddressA}省`;
			}
			if (fieldsValue.sendAddressB) {
				addressStr += `${fieldsValue.sendAddressB}市`;
			}
			if (fieldsValue.sendAddressC) {
				addressStr += `${fieldsValue.sendAddressC}区`;
			}
			if (fieldsValue.sendAddressD) {
				addressStr += `${fieldsValue.sendAddressD}`;
			}
			const modifyForm = {
				id: this.state.cPlyNoId,
        cAppNme: fieldsValue.cAppNme,
        cPlyNo: fieldsValue.cPlyNo,
        cMobileNo: fieldsValue.cMobileNo,
        sendAddress: addressStr,
				cRemark: fieldsValue.cRemark,
        ...repInfoObj,
			}
      this.props.dispatch({
        type: 'rule/updateGuaranteeReprint',
        payload: {
          ...modifyForm,
        },
      }).then((res) => {
        this.setState({
          modalVisible: false,
				});
				this.handleSearch();
      });
		})
	}
	render() {
		const { form, currentUserName, rule={} } = this.props;
		const { repRegisterData=[] } = rule;
		const { modalVisible } = this.state;
		const { getFieldDecorator } = form; 
		let count = 0;
		if (repRegisterData) {
			count = repRegisterData.length
		} else {
			count = 0;
		}
		const columns = [
			{
				title: '保单号',
				dataIndex: 'cPlyNo',
				align: 'center',
			},
			{
				title: '渠道', 
				dataIndex: 'extEnterpCode',
				align: 'center',
				render:(val)=>{
					return getChannelNameByValue(val);
				},
			},
			{
				title: '打印时间',
				dataIndex: 'printDate',
				align: 'center',
				render: (text) => {
					let date = '';
					if (text) {
						date = moment(text).format('YYYY-MM-DD hh:mm:ss');
					} else {
						date = '';
					}
					return date;
				}
			},
			{
				title: '快递运单号',
				dataIndex: 'postNo',
				align: 'center',
			},
			{
				title: '产品名称',
				dataIndex: 'prodNo',
				align: 'center',
				 render:(text, record)=>{
					return getProductNameByExtEnterpCodeAndProdNo(record.extEnterpCode, text);
				},
			 },
			{
				title: '投保人姓名',
				dataIndex: 'cAppNme',
				align: 'center',
			},
			 {
				title: '被投保人姓名', 
				dataIndex: 'cInsrntCnm',
				align: 'center',
			},
			 {
				title: '保险起期',
				dataIndex: 'insrncBeginDate',
				align: 'center',
			},
			{
				title: '联系电话',
				dataIndex: 'cMobileNo',
				align: 'center',
			},
			{
				title: '快递公司',
				dataIndex: 'expressCode',
				align: 'center',
				render: (text,record) => {
					return <span>{_formatUnit(text,'expressCode')}</span>
				}
			},
			{
				title: '收件人姓名',
				dataIndex: 'cAppNme1',
				align: 'center',
				render: (text,record) => {
					return (
						<span>{record.cAppNme}</span>
					)
				}
			},
			 {
				title: '投递地址',
				dataIndex: 'sendAddress',
				align: 'center',
			},
			 {
				title: '补寄类型', 
				dataIndex: 'repairSendType',
				align: 'center',
			},
			 {
				title: '补寄资料',
				dataIndex: 'repairSendData',
				align: 'center',
			},
			 {
				title: '补发登记日期',
				dataIndex: 'cRegisterDate',
				align: 'center',
			},
			 {
				title: '补发登记人员',
				dataIndex: 'cRegisterPerson',
				align: 'center',
			},
			{
				title: '备注',
				dataIndex: 'cRemark',
				align: 'center',
			},
			{
				title: '操作',
				dataIndex: 'operate',
				render: 'align',
				width: 120,
				render: (text, record) => {
					let isDisable = false;
					if (record.printStatus !== '1') {
						isDisable = true;
					} else {
						if (currentUserName !== record.cRegisterPerson) {
							isDisable = true;
						}
					}
					return (
						<Fragment>
							<Button onClick={this.recordEdit.bind(this, record)} disabled={isDisable} type="primary" size="small">编辑</Button>
							<Popconfirm title={'确定提交'} onConfirm={this.recordDelete.bind(this, record.id)}>
								<Button disabled={isDisable} type="primary" size="small" style={{ marginLeft: 5 }}>删除</Button>
							</Popconfirm>
						</Fragment>
					)
				}
			}
		];
		return (
			<PageHeaderLayout title="">
				<Card>
					<Form className="custom-form-wrapper custom-search-form-wrapper" layout="inline">
						<Row gutter={{ md: 24, lg: 24, xl: 24 }}>
							<Col md={12} sm={24}>
								<FormItem label="保单号">
                  {getFieldDecorator('cPlyNo')(
                    <Input style={{ width: '100%' }} />
                  )}
								</FormItem>
							</Col>
							<Col md={12} sm={24}>
								<FormItem label="登记人账号">
									{getFieldDecorator('registerNum')(
										<Input style={{ width: '100%' }} />
									)}
								</FormItem>
							</Col>
							<Col md={12} sm={24}>
								<FormItem label="补发登记日期">
									{
										getFieldDecorator('cRegisterDate')(
											<RangePicker style={{ width: '100%' }} />
										)
									}
								</FormItem>
							</Col>
						</Row>						
					</Form>
					<div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
            <span style={{ marginBottom: 24 }}>
              <Button onClick={this.handleSearch} type="primary" >
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </div>
					<Table
              scroll={{x:1800}}
              dataSource={repRegisterData}
							bordered
							rowKey={record => record.id}
              size="small"
              columns={columns}
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                ...{
                  current:this.state.pageNum,
                  pageSize:this.state.pageSize,
                  showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                  total:count,
                  pageSizeOptions:['10','20','30','40','50'],
                },
              }}
              onChange={this.handleTableChange}
            />
				</Card>
				<Modal
          title=""
          visible={modalVisible}
          onOk={this.okHandle}
					onCancel={this.handleModalVisible}
					okText="提交修改"
					maskClosable={false}
					width="730px"
        >
					<Form>
						<Row>
							<Col>
								<Form.Item label="投保人姓名">
									{getFieldDecorator('cAppNme', {
										rule: [{ message: '投保人姓名'}]
									})(
										<Input readOnly />
									)}
								</Form.Item>
							</Col>
              <Col>
								<Form.Item label="保单号">
									{getFieldDecorator('cPlyNo', {
										rule: [{ message: '保单号'}]
									})(
										<Input readOnly />
									)}
								</Form.Item>
							</Col>
              <Col>
								<Form.Item label="手机号">
									{getFieldDecorator('cMobileNo', {
										rule: [{ message: '手机号'}]
									})(
										<Input />
									)}
								</Form.Item>
							</Col>
							<Col>
								<FormItem label="投递地址">
									<Input.Group 
										compact
										style={{ width: '100%' }}
									>
										{getFieldDecorator('sendAddress')(
											<Input style={{ display: 'none' }}/>
										)}
										{getFieldDecorator('sendAddressA')(
											<Input style={{ width: '7%' }} />
										)}
										<Input style={{ width: '3%', borderLeft: 0, padding: '4px 2px', pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="省" disabled />
										{getFieldDecorator('sendAddressB')(
											<Input style={{ width: '7%' }} />
										)}
										<Input style={{ width: '3%', borderLeft: 0, padding: '4px 2px', pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="市" disabled />
										{getFieldDecorator('sendAddressC')(
											<Input style={{ width: '7%' }} />
										)}
										<Input style={{ width: '3%', borderLeft: 0, padding: '4px 2px', pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="区" disabled />
										{getFieldDecorator('sendAddressD')(
											<Input style={{ width: '70%' }} placeholder="详细地址：如道路、门牌号、小区、楼栋号、单元室等"/>
										)}
									</Input.Group>
								</FormItem>
							</Col>
              <Col>
                <FormItem label="补寄资料"> 
                  {getFieldDecorator('repInfo',
                  { 
                    rules: [{ required: this.state.typeRulesData==='1', message: '必填！' }],
                  })(
                    <Select  placeholder="请选择" 
                      style={{ width: '100%' }} 
                      mode="multiple"
                      >
                      <Option value="cGuarPrint">保单</Option>
                      <Option value="cReceiptPrint">回执</Option>
                      <Option value='cRiderPrint'>批单</Option>
                      <Option value='cInvoicePrint'>发票</Option>
                    </Select>
                  )}
                </FormItem> 
							</Col>
              <Col>
								<Form.Item label="备注">
									{getFieldDecorator('cRemark', {
										rule: [{ message: '联系电话'}]
									})(
										<Input />
									)}
								</Form.Item>
							</Col>
						</Row>
					</Form>
				</Modal>
			</PageHeaderLayout>
		);
	}
}
