import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  Dropdown,
  InputNumber,
  DatePicker,
} from 'snk-web';
import Styles from '../index.less';
import { getDataSource,getSelectChild } from '../../../utils/utils'

const FormItem = Form.Item;
const { Option } = Select;


const FieldsMap = {
    "applyClntNmeA":"thirdAppName",
    "applyCertfCls":"thirdcertType",
    "applyCertfCde":"thirdcertNo",
};

export default class KouKuanRenXinxi extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showWriteBank:false,
      thirdCardTypeValue: props.policyMapInfo.thirdCardType || null, // 账户类型
    }
  }
  componentDidMount() {
    const { dispatch } = this.props;
  
  }

  BranchBankChange(value){
     this.setState({
         showWriteBank:value==='手动录入',
     });
  }

  handleSubmit(){

  }

  SameWithTouBaoRen(){
      this.props.parent.SameWithTouBaoRen(FieldsMap);
  }

  thirdcertTypeChange(value){
    this.props.parent.cardTypeChange(value,"thirdcertType");
  }

  thirdCardTypeChange(value){
    this.setState({thirdCardTypeValue:value});
    // 0203 贷记卡 ，0201 借记卡
    // 根据账户类型 决定 信用卡有效期 的状态 
    if(value=='0201'){
        this.props.form.setFieldsValue({thirdCardDate:""});
        // $("#CARD_END_DATE").val("");
        // $("#CARD_END_DATE").attr('readonly', true);
        // $("#CARD_END_DATE").removeClass("required1");
        // $("#PAY_BANK_CITY").addClass("required1");
    }else{
        // $("#CARD_END_DATE").attr('readonly', false);
        // $("#INSTALLMENT_NUM").attr('readonly', false);
        // $("#CARD_END_DATE").addClass("required1");
        // $("#PAY_BANK_CITY").removeClass("required1");
        // $("#PAY_BANK_CITY").attr('readonly', true);
        // $("#PAY_BANK_CITY").val("");
        this.props.form.setFieldsValue({thirdPayCity:""});
    }
  }
    

  render() {
    const { policyMapInfo = {}, form } = this.props;
		const { getFieldDecorator } = form;
    let thirdPayCity_Props = {}; // 第三方扣款银行所属城市 状态
    let thirdPayCityRule = {}; // 第三方扣款银行所属城市 验证规则
    let thirdCardDate_Props = {}; // 信用卡有效期 状态
    let thirdCardDateRule = {initialValue: 12};// 信用卡有效期 验证规则
    const INSTALLMENT_NUM_Props = {}; // 分期付款次数 状态
    if(this.state.thirdCardTypeValue=='0201'){ // 借记卡
        thirdPayCityRule = {
            rules:[
                {
                required:true,message:'必填',
            }],
        };
        thirdPayCity_Props = {
            placeholder:'请输入',
        };
        thirdCardDate_Props = {
            disabled:true,
            placeholder:'',
        };
    }else{
        thirdPayCity_Props = {
            disabled:true,
        };
        thirdCardDateRule = {
            initialValue: 12,
            rules:[
                {pattern:/^\d*$/,'message':"必须为数字"},
                {
                required:true,message:'必填',
            }],
        };
        thirdCardDate_Props = {
            placeholder:'请输入',
        };
    }
    return (
      <div className={Styles.warpper}>
        <div className={Styles.sectionTitle}>扣款人信息 <Button onClick={this.SameWithTouBaoRen.bind(this)} size='small' type='primary'>同投保人</Button></div>
        <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="姓名">
              {getFieldDecorator('thirdAppName', {
                initialValue: policyMapInfo.thirdAppName,
                rules: [{required: true, message: '必填!'}],
              })(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="证件类型">
              {getFieldDecorator('thirdcertType', {
                initialValue: policyMapInfo.thirdcertType,
              })(
                <Select onChange={this.thirdcertTypeChange.bind(this)}  placeholder="请选择" style={{ width: '100%' }}>
                    {getSelectChild("certiDesc")}
                  </Select>
                    )}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="证件号码">                    
              {getFieldDecorator('thirdcertNo',{
                initialValue: policyMapInfo.thirdcertNo,
                rules: [{
                  validator: (rule,value,callback)=>{this.props.parent.validatorIDCard(rule,value,callback,"thirdcertNo")},
                }],
              })(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="账户类型">
              {getFieldDecorator('thirdCardType',{
                initialValue: policyMapInfo.thirdCardType,
                rules: [{ required: true, message: '必填!' }],
                }
              )(<Select onChange={this.thirdCardTypeChange.bind(this)} placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild("bankCardDesc")}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="卡号">
              {getFieldDecorator('thirdcardId',{
                initialValue: policyMapInfo.thirdcardId,
                rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="发卡银行">
              {getFieldDecorator('thirdBranchBank',{
                initialValue: policyMapInfo.thirdBranchBank,
                rules: [{ required: true, message: '必填!' }],
                        })(
                          <Select onChange={this.BranchBankChange.bind(this)}  placeholder="请选择" style={{ width: '100%' }}>
                            {getSelectChild("bank")}
                          </Select>
                    )}
            </FormItem>
            {this.state.showWriteBank?
                        (
                          <FormItem label="">
                            {getFieldDecorator('thirdBranchBankWrite',{
                initialValue: policyMapInfo.thirdBranchBankWrite,
                rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入发卡银行" />)}
                          </FormItem>
                    ):null}
          </Col>
        </Row>
        <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="分期付款次数">
              {getFieldDecorator('tms',{
                            initialValue: 12,
                        })(<Input placeholder="请输入" disabled />)}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="信用卡有效期">
              {getFieldDecorator('thirdCardDate', {
                initialValue: policyMapInfo.thirdCardDate,
              })(
                <Input placeholder="请输入" />
              )}
            </FormItem>
          </Col>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="扣款渠道">
              {getFieldDecorator('payChannel',{initialValue: '银联'},{
                            rules: [{ required: true, message: '必填!' }],
                        })(
                          <Input placeholder="请输入" />
                    )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
          <Col md={8} lg={8} sm={16}>
            <FormItem label="第三方扣款银行所属城市">
              {getFieldDecorator('thirdPayCity', {initialValue: policyMapInfo.thirdPayCity},thirdPayCityRule)(<Input {...thirdPayCity_Props} />)}
            </FormItem>
          </Col>
        </Row>
      </div>
    );
  }
}
