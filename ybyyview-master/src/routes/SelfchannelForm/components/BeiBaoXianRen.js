import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  DatePicker,
  Checkbox,
} from 'snk-web';
import Styles from '../index.less';
import { getDataSource,getSelectChild } from '../../../utils/utils'

const FormItem = Form.Item;
const { Option } = Select;

const FieldsMap = {
    "applyClntNmeA":"insrncName",
    "applySex":"insrncSex",
    "applyClntAddr":"insrncAddress",
    "applyCertfCls":"insrncCardType",
    "applyCertfCde":"insrncCardId",
    "appBirth":"insrncBirth",
    "applyMobile":"insrncMobile",
    "applyTel":"insrncDayTel",
    "applyJobType":"insrncJobType",
    "applyCompanyName":"insrncAddr",
    "applyZipCde":"insrncPostCode",
    "identifyLongValidA": "identifyLongValidI",
    "countryA": "countryI",
    "identifyExpiryA": "identifyExpiryI",
    "occupationCodeA": "occupationCodeI",
};

export default class BeiBaoXianRenXiang extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isILongLess: props.policyMapInfo && props.policyMapInfo.identifyLongValidI === '1' ? true : false,
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
  
  }

  SameWithTouBaoRen(){
    this.props.parent.SameWithTouBaoRen(FieldsMap);
    const value = this.props.form.getFieldValue('identifyLongValidA');
    this.setState({
      isILongLess: value,
    });
    this.props.form.setFieldsValue({ 'identifyLongValidI': value });
  }

  handleSubmit(){

  }
  insrncBirthChange(){
    this.props.parent.insrncBirthChange();
  }
  InsrncCardTypeChange(value){
      this.props.parent.cardTypeChange(value,"insrncCardType");
  }

  renderSelectOption = (data) => {
    return data.map(item => (
      <Option key={item.code} value={item.code}>{item.codeName}</Option>
    ));
  }

  render() {
    const { policyMapInfo = {}, form, selfchannelForm } = this.props;
    const { getFieldDecorator } = form;
    const {
      CountryCode,
      ItemAccOccupationCode,
    } = selfchannelForm;
    return (
        <div className={Styles.warpper}>
            <div className={Styles.sectionTitle}>被保险人信息 <Button onClick={this.SameWithTouBaoRen.bind(this)} size='small' type='primary'>同投保人</Button></div>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="姓名">
                     {getFieldDecorator('insrncName',{
                          initialValue: policyMapInfo.insrncName,
                          rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="证件类型">
                    {getFieldDecorator('insrncCardType',{
                         initialValue: policyMapInfo.insrncCardType,
                         rules: [{ required: true, message: '必填!' }],
                        })(
                        <Select onChange={this.InsrncCardTypeChange.bind(this)}  placeholder="请选择" style={{ width: '100%' }}>
                            {getSelectChild("certiDesc")}
                        </Select>
                    )}
                    </FormItem>
                </Col>  
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="证件号码">
                      {getFieldDecorator('insrncCardId',{
                         initialValue: policyMapInfo.insrncCardId,
                         rules: [{ required: true, message: '必填!' },
                            {
                              validator: (rule,value,callback)=>{this.props.parent.validatorIDCard(rule,value,callback,"insrncCardId")}
                            }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="性别">
                    {getFieldDecorator('insrncSex',{
                         initialValue: policyMapInfo.insrncSex,
                         rules: [{ required: true, message: '必填!' }],
                        })(
                        <Select  placeholder="请选择" style={{ width: '100%' }}>
                            {getSelectChild('sex')}
                        </Select>
                    )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="地址">
                     {getFieldDecorator('insrncAddress',{
                         initialValue: policyMapInfo.insrncAddress,
                         rules: [{ required: true, message: '必填!' },{pattern:/^(?=([\u4e00-\u9fa5].*){5})/,'message':"具体地址字符不少于5个汉字"},],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="出生日期">
                    {getFieldDecorator('insrncBirth',{
                      initialValue: policyMapInfo.insrncBirth ? moment(moment(policyMapInfo.insrncBirth).format('YYYY-MM-DD'), 'YYYY-MM-DD') : '',
                      rules: [{ required: true, message: '必填!' }],
                    })(
                        <DatePicker onChange={this.insrncBirthChange.bind(this)}  style={{ width: '100%' }} placeholder="出生日期" />
                    )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="联系电话">
                      {getFieldDecorator('insrncMobile',{
                         initialValue: policyMapInfo.insrncMobile,
												 rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"},{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="日间电话">
                      {getFieldDecorator('insrncDayTel',{
                         initialValue: policyMapInfo.insrncDayTel,
												 rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"}],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="夜间电话">
                      {getFieldDecorator('insrncNightPhone',{
                         initialValue: policyMapInfo.insrncNightPhone,
												 rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"}],})(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="职业/工种">
                    {getFieldDecorator('occupationCodeI',{
                       initialValue: policyMapInfo.occupationCodeI,
											 rules: [{ required: true, message: '必填!' }],
                      })(
                        <Select
                          showSearch
                          optionFilterProp="children"
                          placeholder="请选择"
                        >
                          {this.renderSelectOption(ItemAccOccupationCode)}
                        </Select>
                      )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="公司名称">
                    {getFieldDecorator('insrncAddr', {
                         initialValue: policyMapInfo.insrncAddr,
										})(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="邮编">
                      {getFieldDecorator('insrncPostCode',{
                         initialValue: policyMapInfo.insrncPostCode,
												 rules: [{pattern:/^\d{6}$/,  message: '邮编为6位数字!' }],
											})(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={8}>
                  <FormItem label="证件有效期">
                    {getFieldDecorator('identifyLongValidI', {
                      valuePropName: 'checked',
                      initialValue: policyMapInfo.identifyLongValidI === '1' ? true : false,
                    })(<Checkbox onChange={() => this.setState((state) => ({
                      isILongLess: !state.isILongLess,
                    }))}>长期</Checkbox>)}
                  </FormItem>
                </Col>
                <Col md={8} lg={8} sm={8}>
                  <FormItem label="证件有效止期">
                    {getFieldDecorator('identifyExpiryI', {
                      initialValue: policyMapInfo.identifyExpiryI ? moment(moment(policyMapInfo.identifyExpiryI).format('YYYY-MM-DD'), 'YYYY-MM-DD') : '',
                    })(
                      <DatePicker disabled={this.state.isILongLess} allowClear={true} style={{ width: '100%' }} />
                    )}
                  </FormItem>
                </Col>
                <Col md={8} lg={8} sm={8}>
                    <FormItem label="国籍">
                        {getFieldDecorator('countryI',{
                          initialValue: policyMapInfo.countryI || '1010090156',
                          rules: [{ required: true, message: '必填!' }],
                        })(
                          <Select optionFilterProp="children" showSearch placeholder="请选择" style={{ width: '100%' }}>
                            {this.renderSelectOption(CountryCode)}
                          </Select>
                        )}
                    </FormItem>
                </Col>
            </Row>
        </div>
    );
  }
}
