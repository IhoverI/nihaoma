import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  Dropdown,
  InputNumber,
  DatePicker,
} from 'snk-web';
import Styles from '../index.less';
import moment from 'moment';
import { getSelectChild } from '../../../utils/utils';

const FormItem = Form.Item;
const { Option } = Select;

export default class ChanpinXiaoShou extends PureComponent {
  state = {
   peeType: 'peeType0636',
  };
  componentDidMount() {
    const { dispatch } = this.props;
  
  }
  peeTypeChange(value){
    this.props.parent.calculatePRM();
  }

  onSignDateTimeChange(){
    this.props.parent.calculatePRM();
  }

  prodNoChange(value){
    if (value === '0636') {
      this.setState({
        peeType: 'peeType0636',
      });
    }
    if (value === '1212') {
      this.setState({
        peeType: 'peeType1212',
      });
    }
    this.props.parent.calculatePRM();
  }

  disabledSignDate = (current) => {
      return current && current > moment().endOf('day');
  }


  render() {
    const { policyMapInfo = {}, form } = this.props;
		const { getFieldDecorator } = form;
    return (
        <div className={Styles.warpper}>
            <div className={Styles.sectionTitle}>产品销售信息</div>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
               <Col md={8} lg={8} sm={16}>
                    <FormItem label="产品代码">
                    {getFieldDecorator('prodNo',{
														initialValue: policyMapInfo.prodNo,
                            rules: [{ required: true, message: '必填!' }],
                        })(
                        <Select onChange={this.prodNoChange.bind(this)} placeholder="请选择" style={{ width: '100%' }}>
                           <Option value={'0636'}>尊尚人生</Option>
                           <Option value={'1212'}>祥泰安康</Option>
                        </Select>
                    )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="款别">
                    {getFieldDecorator('peeType',{
														initialValue: policyMapInfo.peeType,
                            rules: [{ required: true, message: '必填!' }],
                        })(
                        <Select onChange={this.peeTypeChange.bind(this)}  placeholder="请选择" style={{ width: '100%' }}>
                            {getSelectChild(this.state.peeType)}
                        </Select>
                    )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="所交保费">
                    {getFieldDecorator('prm',{
														initialValue: policyMapInfo.prm,
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input readOnly placeholder="自动计算" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                <FormItem label="签单日期">
									{getFieldDecorator('signDateTime',{
										initialValue: moment(moment(policyMapInfo.signDateTime).format('YYYY-MM-DD'), 'YYYY-MM-DD'),
										rules: [{ required: true, message: '必填!' }],
									})(
											<DatePicker
											onChange={this.onSignDateTimeChange.bind(this)}
											style={{ width: '100%' }}
											placeholder="签单日期"
											disabledDate={this.disabledSignDate}
											/>
									)}
                </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <FormItem label="电话销售代表姓名">
                    {getFieldDecorator('teleLeadName',{
                      initialValue: policyMapInfo.teleLeadName,
                      rules: [{ required: true, message: '必填!' }],
                    })(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="销售录音电话号码">
                      {getFieldDecorator('saleManTel',{
												initialValue: policyMapInfo.saleManTel,
                        rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"},
                        { required: true, message: '必填!' }],
                      })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
            <Col md={8} lg={8} sm={16}>
                    <FormItem label="电话销售代表编号">
                    {getFieldDecorator('teleLeadCode',{
														initialValue: policyMapInfo.teleLeadCode,
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
        </div>
    );
  }
}
