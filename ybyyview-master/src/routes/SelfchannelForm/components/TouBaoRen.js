import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  Dropdown,
  InputNumber,
  DatePicker,
  Checkbox
} from 'snk-web';
import Styles from '../index.less';
import { getDataSource,getSelectChild } from '../../../utils/utils'

const FormItem = Form.Item;
const { Option } = Select;

export default class TouBaoRen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      cElcState:"",
      isTLongLess: props.policyMapInfo && props.policyMapInfo.identifyLongValidA === '1' ? true : false,
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
  
  }

  appBirthChange(){
    // this.props.parent.appBirthChange();
  }

  ApplyCertfClsChange(value){
    // this.setState({
    //     showIDCardInput:(value!=='101')
    // });
    this.props.parent.cardTypeChange(value,"applyCertfCls");
  }

  handleSubmit(){

  }

  ApplyElecFlagChange(value){
    this.setState({cElcState:value});
  }

  renderSelectOption = (data) => {
    return data.map(item => (
      <Option key={item.code} value={item.code}>{item.codeName}</Option>
    ));
  }

  render() {
    const { policyMapInfo = {}, form, selfchannelForm } = this.props;
    const {
      CountryCode,
      ItemAccOccupationCode,
      relationBetweenCode,
    } = selfchannelForm;
		const { getFieldDecorator } = form;
    let thirdCardDate_Props = {};
    let emailRule = {}; 
		let postAddressRule = {};
    switch (this.state.cElcState) {
        case '0': 
        emailRule = {
            rules:[
                {pattern:/^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/,'message':"确认邮箱格式！"},
                ]
        };
        postAddressRule = {
					rules:[
                {pattern:/^(?=([\u4e00-\u9fa5].*){5})/,'message':"具体地址字符不少于5个汉字"},
                {required:true,message:'必填！'}
            ]
        };
        break;
        case '1':
        emailRule = {
            rules:[
                {pattern:/^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/,'message':"确认邮箱格式！"},
                {
                required:true,message:'必填！'
            }]
        };   
        break;
        case '2':
        postAddressRule = {
          rules:[
             {pattern:/^(?=([\u4e00-\u9fa5].*){5})/,'message':"具体地址字符不少于5个汉字"},
              {required:true,message:'必填！'}
          ]
        };
        emailRule = {
            rules:[
                {pattern:/^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/,'message':"确认邮箱格式！"},
                {
                required:true,message:'必填！'
            }]
        };
        break;  
        default: break;
    } 
    // const relation1 = getDataSource('selectSource').relation1;
    return (
        <div className={Styles.warpper}>
            <div className={Styles.sectionTitle}>投保人信息项</div>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="姓名">
                     {getFieldDecorator('applyClntNmeA',{
											 	initialValue: policyMapInfo.applyClntNmeA,
                        rules: [{ required: true, message: '必填!' }],
                      })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="证件类型">
                    {getFieldDecorator('applyCertfCls',{
											initialValue: policyMapInfo.applyCertfCls || '01',
                      rules: [{ required: true, message: '必填!' }],
                    })(
                        <Select onChange={this.ApplyCertfClsChange.bind(this)}  placeholder="请选择" style={{ width: '100%' }}>
                         {getSelectChild("certiDesc")}
                        </Select>
                    )}
                    </FormItem>
                </Col>  
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="证件号码">
                      {getFieldDecorator('applyCertfCde',{
												initialValue: policyMapInfo.applyCertfCde,
                        rules: [{ required: true, message: '必填!' },{
                          validator: (rule,value,callback)=>{this.props.parent.validatorIDCard(rule,value,callback,"applyCertfCde")}
                      	}],
                      })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                {/* {false?(<Col md={8} lg={8} sm={16}>
                    <FormItem label="身份证号">
                      {getFieldDecorator('applyCertfNo',{
                            rules: [{ required: true, message: '必填!' },
                            {
                                validator: (rule,value,callback)=>{this.props.parent.validatorIDCard(rule,value,callback,"applyCertfNo")}
                          }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>):null} */}
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="性别">
                    {getFieldDecorator('applySex', {
											initialValue: policyMapInfo.applySex,
										})(
                    <Select placeholder="请选择" style={{ width: '100%' }}>
                        {getSelectChild('sex')}
                    </Select>
                    )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="出生日期">
                      {getFieldDecorator('appBirth', {
                        initialValue: policyMapInfo.appBirth ? moment(moment(policyMapInfo.appBirth).format('YYYY-MM-DD'), 'YYYY-MM-DD') : '',
                      })(
                          <DatePicker onChange={this.appBirthChange.bind(this)} style={{ width: '100%' }} placeholder="出生日期" />
                      )}
                    </FormItem>
                </Col>
               <Col md={8} lg={8} sm={16}>
                    <FormItem label="地址">
                     {getFieldDecorator('applyClntAddr',{
														initialValue: policyMapInfo.applyClntAddr,
                            rules: [{ required: true, message: '必填!' },{pattern:/^(?=([\u4e00-\u9fa5].*){5})/,'message':"地址不少于5个汉字"},],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="联系电话">
                      {getFieldDecorator('applyMobile',{
														initialValue: policyMapInfo.applyMobile,
                            rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"},{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="日间电话">
                      {getFieldDecorator('applyTel',{
														initialValue: policyMapInfo.applyTel,
														rules:[{pattern:/^\d{8,}$/,'message':"号码格式有误"}]
											})(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="职业/工种">
                      {getFieldDecorator('occupationCodeA',{
													initialValue: policyMapInfo.occupationCodeA,
                          rules: [{ required: true, message: '必填!' }],
                      })(
                        <Select optionFilterProp="children" showSearch placeholder="请选择" >
                          {this.renderSelectOption(ItemAccOccupationCode)}
                        </Select>
                      )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="公司名称">
                      {getFieldDecorator('applyCompanyName', {
												initialValue: policyMapInfo.applyCompanyName,
											})(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="邮编">
                      {getFieldDecorator('applyZipCde',{
												initialValue: policyMapInfo.applyZipCde,
												rules: [{pattern:/^\d{6}$/,  message: '邮编为6位数字!' }]
											})(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="快递地址">
                      {getFieldDecorator('postAddress', {initialValue: policyMapInfo.postAddress},postAddressRule)(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="与被保人关系">
                        {getFieldDecorator('cRelationI',{
													initialValue: policyMapInfo.cRelationI,
													rules: [{ required: true, message: '必填!' }],
                        })(
                          <Select optionFilterProp="children" showSearch placeholder="请选择" style={{ width: '100%' }} onChange={this.relationChange}>
                            {this.renderSelectOption(relationBetweenCode)}
                          </Select>
                        )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                     <FormItem label="保单发送方式">
                        {getFieldDecorator('cElcFlag',{
                            initialValue: policyMapInfo.cElcFlag,
                            rules: [{ required: true, message: '必填!' }],
                        })(
                          <Select onChange={this.ApplyElecFlagChange.bind(this)} placeholder="请选择" style={{ width: '100%' }}>
                            <Option value="0">纸质保单</Option>
                            <Option value="1">电子保单</Option>
                            <Option value="2">电子和纸质</Option>
                            <Option value="4">纸质转电子</Option>
                          </Select>
                        )}
                    </FormItem>
                </Col>
                <Col md={8} lg={8} sm={16}>
                     <FormItem label="电子邮件">
                        {getFieldDecorator('cEmail', {
                          initialValue: policyMapInfo.cEmail
                        },emailRule)(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
								<Col md={8} lg={8} sm={8}>
                  <FormItem label="证件有效期">
                    {getFieldDecorator('identifyLongValidA', {
                      rules: [{ required: true, message: '证件有效期必填!' }],
                      valuePropName: 'checked',
                      initialValue: policyMapInfo.identifyLongValidA === '1' ? true : false,
                    })(
                      <Checkbox onChange={() => this.setState((state) => ({
                        isTLongLess: !state.isTLongLess,
                      }))}>长期</Checkbox>
                    )}
                  </FormItem>
                </Col>
                <Col md={8} lg={8} sm={8}>
                	<FormItem label="证件有效止期">  
                    {getFieldDecorator('identifyExpiryA', {
                      initialValue: policyMapInfo.identifyExpiryA ? moment(moment(policyMapInfo.identifyExpiryA).format('YYYY-MM-DD'), 'YYYY-MM-DD') : '',
                    })(
                      <DatePicker disabled={this.state.isTLongLess} allowClear={true} style={{ width: '100%' }} />
                    )}
                  </FormItem>
                </Col>
                <Col md={8} lg={8} sm={8}>
                    <FormItem label="国籍">
                    {getFieldDecorator('countryA',{
                      initialValue: policyMapInfo.countryA || '1010090156',
                      rules: [{ required: true, message: '国籍必填!' }],
                    })(
                      <Select optionFilterProp="children" showSearch placeholder="请选择" style={{ width: '100%' }}>
                        {this.renderSelectOption(CountryCode)}
                      </Select>
                    )}
                    </FormItem>
                </Col>
            </Row>
        </div>
    );
  }
}
