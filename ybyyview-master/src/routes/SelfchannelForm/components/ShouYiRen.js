import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  message,
  Divider,
} from 'snk-web';
import SingleShouYiRen from '../../../components/SingleShouYiRen/SingleShouYiRen';
import Styles from '../index.less';
import { getSelectChild, getDataSource } from '../../../utils/utils';

const FormItem = Form.Item;
const { Option } = Select;

export default class ShouYiRen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      relationIsSelf: true,
      SingleShouYiRenList: props.policyMapInfo.beneficiaryList || [],
    };
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'selfchannelForm/checkCodeType',
    });
  }

  handleSubmit(){

  }

  BenfTypeChange(value){
    if (value === '1') {
      this.props.form.setFieldsValue({ insrncRelation: [] });
      this.setState({
        relationIsSelf: false,
      });
      this.addBeneficiary();
    } else {
      this.setState({
        relationIsSelf: true,
        // SingleShouYiRenList: [],
      });
      this.props.form.setFieldsValue({ insrncRelation: undefined });
    }
  }
  //multiple

  addBeneficiary = () => {
    // 受益人最多为5人
    if (this.state.SingleShouYiRenList.length < 5) {
      this.setState((state) => ({
        SingleShouYiRenList: [...state.SingleShouYiRenList, {}],
      }));
    } else {
      message.warning('受益人最多为5人', 5);
    }
  }

  //  确认添加
  confirmAddBeneficiary = (data, index) => {
    const oldArr = this.state.SingleShouYiRenList;
    oldArr.pop();
    this.setState({
      SingleShouYiRenList: [...oldArr, data],
    });
  }
// 删除受益人
  DeleteBeneificiary = (index) => {
    const newBeneificiary = this.state.SingleShouYiRenList.filter((item, i) => i + 1 !== index );
    this.setState({
      SingleShouYiRenList: [...newBeneificiary],
    });
    this.props.dispatch({
      type: 'selfchannelForm/deletebeneficiaryList',
      payload: {
        index,
      }
    })
  }

  render() {
    const { policyMapInfo = {}, form, parent } = this.props;
    const { getFieldDecorator } = form;
    return (
        <div className={Styles.warpper}>
            <div className={Styles.sectionTitle}>受益人信息项</div>
            <Row gutter={{ md: 24, lg: 24}}>
                <Col md={8} lg={8} sm={16}>
                    <FormItem label="受益方式">
                        {getFieldDecorator('benfType',{
                            initialValue: policyMapInfo.benfType || '0',
                            rules: [{ required: true, message: '必填!' }],
                        })(
                        <Select onChange={this.BenfTypeChange.bind(this)} placeholder="请选择" style={{ width: '100%' }}>
                            {getSelectChild('benfType')}
                        </Select>
                        )}
                    </FormItem>
                </Col>
                {/* 当受益人只有投保人时，受益人直接是法定 */}
                {
                  policyMapInfo.benfType !== '1' && this.state.relationIsSelf ? (
                    <Fragment>
                      <Col md={8} lg={8} sm={16}>
                        <FormItem label="受益人姓名">
                        {getFieldDecorator('benfClntNmeB', {
                            initialValue: policyMapInfo.benfClntNmeB || '法定',
                        })(<Input placeholder="请输入" readOnly />)}
                        </FormItem>
                      </Col>
                      <Col md={8} lg={8} sm={16}>
                          <FormItem label="受益人与被保人关系">
                              {getFieldDecorator('insrncRelation', {
                                  initialValue: policyMapInfo.insrncRelation || '法定',
                              })(<Input placeholder="请输入" readOnly />)}
                          </FormItem>
                      </Col>
                    </Fragment>
                  ) : null
                }
            </Row>
            {
              this.props.form.getFieldValue('benfType') === '1' ? (
                this.state.SingleShouYiRenList.map((item, index) =>{
                  const { beneficiaryCertfCde } = item;
                  return (
                    <SingleShouYiRen
                      policyMapInfo={item}
                      parentForm={form}
                      confirmAddBeneficiary={this.confirmAddBeneficiary}
                      DeleteBeneificiary={this.DeleteBeneificiary}
                      key={beneficiaryCertfCde}
                      index={index+1}
                    />
                  );
                })
               ) : null
            }
            {
              policyMapInfo.benfType !== '1' && this.state.relationIsSelf ? null : (
                <Fragment>
                  <Divider/>
                  <Button type="primary" size="small" onClick={this.addBeneficiary}>新增受益人</Button>
                </Fragment>
              )
            }
        </div>
    );
  }
}
