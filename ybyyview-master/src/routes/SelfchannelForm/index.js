import React, { PureComponent, Fragment } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import moment from 'moment';
import TouBaoRen from './components/TouBaoRen';
import BeiBaoXianRen from './components/BeiBaoXianRen';
import KoukuanRenXinxi from './components/KoukuanRenXinxi';
import ChanPinXiaoShouXinxi from './components/ChanPinXiaoShouXinxi';
import ShouYiRen from './components/ShouYiRen';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './index.less';
import { CheckIdCard } from '../../utils/utils'


const FormItem = Form.Item;
const { Option } = Select;

@connect(({selfchannelForm,loading, searchlist }) => ({
  policyMapInfo: searchlist.policyMapInfo,
  selfChannelData: searchlist.selfChannelData,
  selfchannelForm,
  loading: loading.models.rule,
}))
@Form.create()
export default class SelfChannelForm extends PureComponent {
  state = {
   isLoading:false
  };

  componentWillMount() {
    const { dispatch,form } = this.props;
    this.props.dispatch({
      type: 'selfchannelForm/checkCodeType',
    });
    dispatch({
      type: 'selfchannelForm/checkCodeType',
    });
    dispatch({
      type:'selfchannelForm/getUserInfo',
      payload:{},
    }).then(()=>{
      let { selfchannelForm: { getUserInfo } } = this.props;
      if (getUserInfo) {
        if (getUserInfo.code == 0) {
          let userCode = getUserInfo.data.userCode;
          let userEmail = getUserInfo.data.userEmail;
          let userName = getUserInfo.data.userName;
          let userPhone = getUserInfo.data.userPhone;

          form.setFieldsValue({
            teleLeadCode:userCode,
            teleLeadName:userName,
          })
        }
      }
    }).catch((e)=>{
      console.log(e);
      message.error('获取登录用户信息失败，请尝试重新加载');
    });
  }

  componentDidMount = () => {
    this.props.dispatch({
      type: 'selfchannelForm/checkCodeType',
    });
  }

  componentWillUnmount() {
    this.props.dispatch({
      type: 'searchlist/save',
      payload: {
        isSelfchannelAudit: false,
      }
    });
  }
  prepare(values){
    // 处理时间格式 appBirth insrncBirth signDateTime
    const timeArr = ["appBirth","insrncBirth","signDateTime"];
    for(let i=0,j=timeArr.length;i<j;i++){
      const itemValue = values[timeArr[i]];
      if(itemValue){
        values[timeArr[i]] = itemValue.format("YYYY-MM-DD");
      }
    }
    // 当投保人的证件类型为 身份证的时候 身份证字段等于证件号码
    if(values["applyCertfCls"] ==="01"){
      values["applyCertfNo"] = values["applyCertfCde"];
    }
    // 当受益人为指定时候 与被保人关系 的值以逗号隔开
    let insrncRelation = values["insrncRelation"];
    if(insrncRelation&&insrncRelation instanceof Array){
        values["insrncRelation"] = insrncRelation.join(",");
    }
    // 当发卡银行为手写的时候，字段thirdBranchBank 取 thirdBranchBankWrite
    let thirdBranchBank = values["thirdBranchBank"];
    if(thirdBranchBank==='手动录入'){
      values["thirdBranchBank"] = values["thirdBranchBankWrite"];
      delete values["thirdBranchBankWrite"];
    }

    let thirdcardId = values["thirdcardId"];
    values["thirdcardId"] = thirdcardId.replace(/\s+/g, "");

    values['beneficiaryList'] = [];
    if (this.props.form.getFieldValue('benfType') === '0') {
      values['beneficiaryList'] = [...values['beneficiaryList'], { insrncRelation: '法定', benfClntNmeB: '法定' }];
    }
    return values;
  }

  validateAge = () =>{
    var now = new Date();
    var prodNo = this.props.form.getFieldValue("prodNo");
    var insrncBirth = this.props.form.getFieldValue("insrncBirth").toDate();
    var signDateTime =  this.props.form.getFieldValue("signDateTime");//签单日期
    var s1=this.getAge(insrncBirth,new Date(signDateTime.toDate().getTime()+24*60*60*1000));
    if(prodNo === '1212'){//祥泰安康
      if(s1<18){
        return '[祥泰安康]被投保人不能小于18岁';
      }
      if(s1>60){
        return '[祥泰安康]被投保人不能大于60岁';
      }
    }else if(prodNo === '0636'){//尊尚人生
      if(s1<18){
        return '[尊尚人生]被投保人不能小于18岁';
      }
      if(s1>65){
        return '[尊尚人生]被投保人不能大于65岁';
      }
    }
    return null;
  }
  handleSubmit = (e) => {
    const { dispatch } = this.props;
    e.preventDefault();
    this.props.form.validateFields();// 有时候第一次点击无效
    this.props.form.validateFields((err, values) => {
      const date = new Date();
      const today = date.getFullYear() +'-'+ (date.getMonth() + 1) +'-'+ date.getDate();

      /**
       * 暂时删除
       * 心情好的时候统一修改
       */
      // const selectDate = new Date(values.thirdCardDate.format("YYYY-MM-DD"))
      // const formatToday = new Date(today);
      // if (selectDate < formatToday) {
      //   message.warn('[信用卡有效期]必须大于当前日期');
      // }

      // 投保人证件日期规则，1表示长期， 0表示有止期
      if (!values.identifyExpiryA && !values.identifyLongValidA) {
        message.warning('请选择投保人证件有效期');
        return false;
      }
      if (!values.identifyLongValidA) {
        values.identifyLongValidA = '0'; // 证件是否长期， 1表示长期， 0表示有止期
        values.identifyExpiryA = values.identifyExpiryA.format("YYYY-MM-DD"); // 证件是否长期， 1表示长期， 0表示有止期
      } else {
        values.identifyLongValidA = '1'; // 证件是否长期， 0表示长期， 1表示有止期
        values.identifyExpiryA = '9999-12-30';
      };
      // 被保人证件日期规则，1表示长期， 0表示有止期
      if (!values.identifyExpiryI && !values.identifyLongValidI) {
        message.warning('请选择被保人证件有效期');
        return false;
      }
      if (!values.identifyLongValidI) {
        values.identifyLongValidI = '0'; // 证件是否长期， 1表示长期， 0表示有止期
        values.identifyExpiryI = values.identifyExpiryI.format("YYYY-MM-DD"); // 证件是否长期， 1表示长期， 0表示有止期
      } else {
        // 证件无止期 identifyLongValidB为0,；
        values.identifyLongValidI = '1'; // 证件是否长期， 0表示长期， 1表示有止期
        values.identifyExpiryI = '9999-12-30';
      };
      if (!err) {
        var ageInfo = this.validateAge();
        if (ageInfo) {
          Modal.error({
            title: '操作失败',
            content: ageInfo,
          });
          return;
        }
        this.setState({
          isLoading:true
        });
        dispatch({
          type:'selfchannelForm/submit',
          payload:this.prepare(values),
        }).then((result)=>{
          console.log(result);
          const { code, message: msg } = result;
          this.setState({
            isLoading:false
          });
          if (code === 0) {
            // this.props.form.resetFields();
          }
        }).catch((e)=>{
          this.setState({
            isLoading:false
          });
          Modal.error({
            title: '操作失败',
            content: '提交失败,请稍后再试...',
          });
        });
      } else {
        console.log(err);
      }
    });
  }

  SameWithTouBaoRen(FieldsMap){
    const values = this.props.form.getFieldsValue();
    for(const key in FieldsMap){
      const CurKey = FieldsMap[key];
      const re = {};
      re[CurKey] = values[key];
      this.props.form.setFieldsValue(re);
    }
  }

  cardTypeChange(value,fieldKey){
    let validateKey = null;
    if(fieldKey==='applyCertfCls'){
      // 投保人证件类型
      validateKey = "applyCertfCde";
    }else if(fieldKey==='insrncCardType'){
      // 被保人证件类型
      validateKey = "insrncCardId";
    }else if(fieldKey==='thirdcertType'){
       // 扣款人证件号码
       validateKey = "thirdcertNo";
    }
    setTimeout(()=>{
      // 延迟 不然验证的时候 获取的值不正确
      this.props.form.validateFields([validateKey], { force: true });
    },300);
  }

  validatorIDCard(rule,value,callback,fieldKey){
    let isIDCard = false;
    if(fieldKey==='insrncCardId'){
      //被保人证件
      isIDCard = this.props.form.getFieldValue("insrncCardType")==='01';
    }else if(fieldKey==='applyCertfNo'){
      // 投保人身份证
      isIDCard = true;
    }else if(fieldKey==='applyCertfCde'){
      // 投保人证件号码
      isIDCard = this.props.form.getFieldValue("applyCertfCls")==='01'; 
    }else if(fieldKey==='thirdcertNo'){
      // 扣款人证件号码
      isIDCard = this.props.form.getFieldValue("thirdcertType")==='01'; 
    }
    var re = CheckIdCard(value);
    if(re!=='success'){
      if(isIDCard){
          callback(re);
      }
    }else{
      if(this.props.form.getFieldValue('applyCertfCls')==='01'){
        this.calculateSexAndBirth(value,fieldKey);
      }
      if(fieldKey==='insrncCardId'){
        // 按照被保人的生日计算
        this.calculatePRM();
      }
    }
    callback();
  }

  calculateSexAndBirth(value,fieldKey){
    // 根据身份证推出性别
    const sexMap = {
      "applyCertfNo":"applySex",
      "applyCertfCde":"applySex",
      "insrncCardId":"insrncSex",
    };
    const target = sexMap[fieldKey];
    if(target){
      const cardValue = this.props.form.getFieldValue(fieldKey);
      var re = {};
      // 偶数为女
      if(cardValue.substr(16, 1) % 2 == 0){
        re[target] = "02";
      }else{
        re[target] = "01";
      }
      this.props.form.setFieldsValue(re);
    }
    // 根据身份证推断出 生日
    const BrithMap = {
      "applyCertfNo":"appBirth",
      "applyCertfCde":"appBirth",
      "insrncCardId":"insrncBirth",
    };
    const birthTarget = BrithMap[fieldKey];
    if(birthTarget){
      const curCardValue = this.props.form.getFieldValue(fieldKey)
      const birthStr = curCardValue.substr(6, 4)+"-"+curCardValue.substr(10, 2)+"-"+curCardValue.substr(12, 2);
      const brithRe = {};
      brithRe[birthTarget] = moment(birthStr,'YYYY-MM-DD')
      this.props.form.setFieldsValue(brithRe);
    }
  }

  /*
    计算出所交保费
    1. 在验证完 被投保人 身份证的时候 调用
    2. 在选择完 产品代码 时候调用
    3. 在选择完 款别的时候调用
    4. 在选完被投保人生日的时候调用
    5. 在签单日期 选择完调用
  */
  calculatePRM(){
    setTimeout(()=>{
      this._calculatePRM();
    },300);
  }

  insrncBirthChange(){
    this.calculatePRM();
  }


  getAge(bDay,nDay) {
    var nbDay = new Date(nDay.getFullYear(),bDay.getMonth(),bDay.getDate()),
      age = nDay.getFullYear() - bDay.getFullYear();
    return nbDay.getTime()<=nDay.getTime()?age:--age;
  }


  _calculatePRM(){
    var prod_type= this.props.form.getFieldValue("peeType");//款别,0/1/2对应第二列
    const prod_type_map = {'A':0,'B':1,'C':2, 'D':3,'E':4};
    
    var prod_no= this.props.form.getFieldValue("prodNo");//商品代码
    var cert_no="";
    let PRM_Value = "";
    var cert_no=this.props.form.getFieldValue("insrncCardId");//被保险人证件号码
    var insrncBirth = this.props.form.getFieldValue("insrncBirth")//被保险人出生
    var i = -1;
    var TheArray = [["62","93","124"],["79","119","158"],["89","134","178"],
    ["110","165","220"],["131","197","262"],["131","197","262"]];
    var type=[65,108,134,134,145];
    var birthday;
    if(cert_no){
      var re = CheckIdCard(cert_no);
      if(re==='success'){
         birthday=new Date(cert_no.substr(6, 4)+"/"+cert_no.substr(10, 2)+"/"+cert_no.substr(12, 2));
      }
    }
    if(insrncBirth&&!birthday){
       birthday = insrncBirth.toDate();
    }
    var now = new Date();
    if(prod_no === '1212'){//祥泰安康
      if(prod_type != null  && prod_type != '' && birthday){
        prod_type = prod_type_map[prod_type];

        var signDateTime =  this.props.form.getFieldValue("signDateTime");//签单日期
        if(signDateTime){
          var s1=this.getAge(birthday,new Date(signDateTime.toDate().getTime()+24*60*60*1000));
          if(s1 >=59 ){
            i=5;
          }else if(s1 >=56){
            i=4;
          }else if(s1 >= 46){
            i=3;
          }else if(s1 >= 36){
            i=2;
          }else if(s1 >= 26){
            i=1;
          }else if(s1 >= 18){
            i=0;
          }else{
            i = -1;
          }
          if(i===-1){
            PRM_Value = undefined;
          }else{
            PRM_Value = ((TheArray[i][prod_type])*12);
          }
        }
      }
    }else if(prod_no === '0636'){//尊尚人生
      if(prod_type != null  && prod_type != ''){
        prod_type = prod_type_map[prod_type];
        PRM_Value = (type[prod_type]*12);
      }
    }

    this.props.form.setFieldsValue({"prm":PRM_Value});
  }
  backToSelfchannelAudit = () => {
    this.props.dispatch(routerRedux.push('/apply-manage/selfchannel-audit'));
  }
  render() {
    const { isSelfchannelAudit } = this.props.selfChannelData;
    return (
      <PageHeaderLayout title="">
        <Form className='custom-form-wrapper self-channel-form' onSubmit={this.handleSubmit.bind(this)} layout="inline">
            <Card bordered={false}>
                <TouBaoRen parent={this} {...this.props}/>
                <Divider/>
                <BeiBaoXianRen parent={this} {...this.props}/>
                <Divider/>
                <ShouYiRen parent={this} {...this.props}/>
                <Divider/>
                <ChanPinXiaoShouXinxi parent={this} {...this.props}/>
                <Divider/>
                <KoukuanRenXinxi parent={this} {...this.props}/>
                <Divider/>
                <div style={{textAlign:'center'}}>
                  <Button loading={this.state.isLoading} type="primary" htmlType="submit">
                    提交
                  </Button>
                  {isSelfchannelAudit ? (<Button onClick={this.backToSelfchannelAudit} style={{ marginLeft: '5px' }} type="primary" htmlType="submit">
                    返回
                  </Button>) : null}
                </div>
            </Card>
        </Form>
      </PageHeaderLayout>
    );
  }
}
