import React from 'react';
import moment from 'moment';
import {
  Button, Select, Popconfirm, Spin
} from 'snk-web';

import {
  getChannelNameByValue,
  getProductNameByExtEnterpCodeAndProdNo,
  getDataSource,
} from '../../utils/utils';
const { Option } = Select;

export function getPayColumns (){
  return [
    {
      title: '保单号',
      dataIndex: 'cPlyNo',
      align: 'center'
    },
    {
      title: '投保单号',
      dataIndex: 'cPlyAppNo',
      align: 'center'
    },
    // {
    //   title: '业务渠道',
    //   dataIndex: 'oldEnterpCode',
    //   align: 'center'
    // },
    {
      title: '期次',
      dataIndex: 'period',
      align: 'center'
    },
    {
      title: '扣款渠道',
      dataIndex: 'extEnterpCode',
      align: 'center'
    },
    {
      title: '请缴金额',
      dataIndex: 'prm',
      align: 'center'
    },
    {
      title: '请款类型',
      dataIndex: 'payType',
      align: 'center'
    },
    {
      title: '初次请款日期',
      dataIndex: 'payDateFirst',
      align: 'center',
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>
    },
    {
      title: '最后请款日期',
      dataIndex: 'payDateLast',
      align: 'center',
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>
    },
    {
      title: '序列号',
      dataIndex: 'serialNo',
      align: 'center'
    },
    {
      title: '请款次数',
      dataIndex: 'payTime',
      align: 'center'
    },
    {
      title: '扣款回盘时间',
      dataIndex: 'chargeDate',
      align: 'center',
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>
    },
    {
      title: '扣款状态',
      dataIndex: 'chargeFlag',
      align: 'center'
    },
    {
      title: '扣款失败原因',
      dataIndex: 'failReason',
      align: 'center'
    },
    {
      title: '是否继续请款',
      dataIndex: 'payAgainFlag',
      align: 'center'
    },
    {
      title: '实收状态',
      dataIndex: 'prmPayInFlag',
      align: 'center'
    },
    {
      title: '实付状态',
      dataIndex: 'cmmPayOutFlag',
      align: 'center'
    },
  ]
};

export function getEndorOldColumns() {
  return [
    {
      title: '批单号',
      dataIndex: 'cEdrNo',
      align: 'center',
      width: 180,
    },
    {
      title: '批改申请人',
      dataIndex: 'cOperCde',
      align: 'center',
      width: 200,
    },
    {
      title: '批改时间',
      dataIndex: 'tUdrDate',
      align: 'center',
      width: 160,
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>
    },
    {
      title: '批改类型',
      dataIndex: 'cEdrType',
      align: 'center',
      width: 100,
    },
    {
      title: '批文',
      dataIndex: 'cEdrCtnt',
      align: 'center',
    },
  ]
};

export function getEndorColumns() {
  return [
    {
      title: '批单号',
      dataIndex: 'policyEndorNo',
      align: 'center',
      width: 180
    },
    {
      title: '类型',
      dataIndex: 'endorType',
      align: 'center',
      width: 100
    },
    {
      title: '核保状态',
      dataIndex: 'endorStatus',
      align: 'center',
      width: 100
    },
    {
      title: '提交人',
      dataIndex: 'userName',
      align: 'center',
      width: 200
    },
    {
      title: '审核人',
      dataIndex: 'underwriteName',
      align: 'center',
      width: 200
    },
    {
      title: '批改提交时间',
      dataIndex: 'submitTime',
      align: 'center',
      width: 180,
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>
    },
    {
      title: '核保时间',
      dataIndex: 'underwriteTime',
      align: 'center',
      width: 180,
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>
    },
    {
      title: '批文',
      dataIndex: 'endorText',
      align: 'center',
      width: 440
    }
  ]
};

export function getEndorCusColumns() {
  return [
    {
      title: '投保人批单号',
      dataIndex: 'appPolicyEndorNo',
      align: 'center',
      width: 180
    },
    {
      title: '被保人批单号',
      dataIndex: 'recPolicyEndorNo',
      align: 'center',
      width: 180
    },
    // {
    //   title: '批改项',
    //   dataIndex: 'endorItems',
    //   align: 'center',
    //   width: 250
    // },
    {
      title: '批改前内容',
      dataIndex: 'oldContent',
      align: 'center',
      width: 400
    },
    {
      title: '批改后内容',
      dataIndex: 'newContent',
      align: 'center',
      width: 400
    },
    {
      title: '核保状态',
      dataIndex: 'status',
      align: 'center',
      width: 100
    },
    {
      title: '提交人',
      dataIndex: 'crtUser',
      align: 'center',
      width: 180,
    },
    {
      title: '审核人',
      dataIndex: 'appUser',
      align: 'center',
      width: 180,
    },
    {
      title: '批改提交时间',
      dataIndex: 'crtTm',
      align: 'center',
      width: 180,
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>

    },
    {
      title: '核保通过时间',
      dataIndex: 'appTm',
      align: 'center',
      width: 180,
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>

    },
    {
      title: '批文',
      dataIndex: 'endorText',
      align: 'center',
      width: 200
    },
    {
      title: '附件',
      dataIndex: 'downloadUrl',
      align: 'center',
      width: 100,
      render: (text, record) => {
        if (text === null) {
          return '暂无附件'
        } else {
          const buttonArr = [];
          const fileIdArr = text.split(",");
          for (const i in fileIdArr) {
            const suffix = fileIdArr[i].substring(fileIdArr[i].lastIndexOf("\.") + 1 , fileIdArr[i].length);
            let buttonItem;
            if (suffix === 'png' || suffix === 'pdf' || suffix === 'jpg' || suffix === 'jpeg') {
                buttonItem = (
                  <Button href={fileIdArr[i]} style={{marginRight:6}} target="_blank">
                    <span style={{margin: '0 4px'}}>
                      {fileIdArr[i].substring(fileIdArr[i].lastIndexOf("\.") + 1 , fileIdArr[i].length)}
                    </span>
                    文件预览
                  </Button>
                );
              } else {
                buttonItem = (
                  <Button 
                    value={fileIdArr[i]} 
                    icon = 'download' 
                    style={{marginRight:6}}
                    onClick={e=>download(fileIdArr[i])}>
                      下载
                        <span style={{margin: '0 4px'}}>
                          {fileIdArr[i].substring(fileIdArr[i].lastIndexOf("\.") + 1 , fileIdArr[i].length)}
                        </span>
                      文件
                  </Button>
                );
              }
            buttonArr.push(buttonItem);
          }
          // return <Button onClick={() => download(text)}>下载</Button>
          return <div>{buttonArr.map((item)=>{return item})}</div>
        }
      }
    },
  ]
};

export function download(fileId) {

  var url =  `${SERVERLOCAL}ybyy-policy/policy/correct/download`;

  var TargetFrame = document.createElement("iframe");
  TargetFrame.setAttribute("name",'download_frame');
  TargetFrame.setAttribute("style","display:none");
  document.body.appendChild(TargetFrame);

  var form=document.createElement("form");
  form.setAttribute("style","display:none");
  form.setAttribute("target","download_frame");
  form.setAttribute("method","post");
  form.setAttribute("action",url);

  var input1=document.createElement("input");
  input1.setAttribute("type","hidden");
  input1.setAttribute("name","path");
  input1.setAttribute("value",fileId);
  form.appendChild(input1);
  document.body.appendChild(form);
 
  form.submit();
  setTimeout(()=>{
    document.body.removeChild(form);
    document.body.removeChild(TargetFrame);
  },100);
}

export function getEndorCancelColumns() {
  return [
    {
      title: '类型',
      dataIndex: 'operFlag',
      align: 'center',
    },
    {
      title: '操作时间',
      dataIndex: 'operTime',
      align: 'center',
    },
    {
      title: '短信发送时间',
      dataIndex: 'validStartTime',
      align: 'center',
    },
    {
      title: '操作人',
      dataIndex: 'operCode',
      align: 'center',
    },
    {
      title: '备注（取消续保原因）',
      dataIndex: 'operRemark',
      align: 'center',
    },
  ]
};

export function getThirdMessageColumns() {
  return [
    {
      title: '操作时间',
      dataIndex: 'createTime',
      align: 'center',
      render: (text, record) => <div>{text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : ''}</div>
    },
    {
      title: '原扣款渠道',
      dataIndex: 'oldEnterpCode',
      align: 'center',
    },
    {
      title: '新扣款渠道',
      dataIndex: 'newEnterpCode',
      align: 'center',
    },
    {
      title: '原卡号',
      dataIndex: 'oldCardNo',
      align: 'center',
    },
    {
      title: '新卡号',
      dataIndex: 'newCardNo',
      align: 'center',
    },
    {
      title: '操作人',
      dataIndex: 'submitUser',
      align: 'center',
    },
  ]
};

export function formaUnit(val,code) {
  let text = '';
  if (code === 'expressCode') {
    switch (val) {
      case 'E1002': text = '圆通'; break;
      case 'E1003': text = '顺丰'; break;
      case 'E1004': text = '宅急送'; break;
      case 'E1005': text = '中通'; break;
      case 'E1006': text = '银雁'; break;
      case 'E1007': text = '天天'; break;
      case 'E1008': text = 'EMS'; break;
      case 'E1009': text = '满逸'; break;
      case 'E1010': text = '德邦'; break;
      default: break;
    }
  } else if (code === 'paperReceiptFlag') {
    switch (val) {
      case '1': text = '已入库'; break;
      case '2': text = '待入库'; break;
      case '3': text = '遗失'; break;
      default: break;
    }
  } else if (code === 'postStatus'){
    switch (val) {
      case '0': text = '在途'; break;
      case '1': text = '签收'; break;
      case '3': text = '问题件'; break;
      case '5': text = '待派送'; break;
      default: break;
    }
  } else if (code === 'expressType') {
    switch (val) {
      case '1': text = '寄送出去'; break;
      case '2': text = '回寄快递'; break;
      case '3': text = '补寄寄送出去'; break;
      case '4': text = '补寄回寄快递'; break;
      default: break;
    }
  } else if (code === 'orderStatus') {
    if (val === '0') {
      text = '签收'
    } else if (val === '1') {
      text = '中转'
    } else if (val === '2') {
      text = '入库'
    } else if (val === '3') {
      text = '派送中(在途)'
    } else if (val === '4') {
      text = '未发'
    } else if (val === '5') {
      text = '客户拒签(拒收)'
    } else if (val === '6') {
      text = '联系不上(无人接收)'
    } else if (val === '7') {
      text = 'EMS物流中'
    } else if (val === '8') {
      text = '问题件'
    } else {
      text = val;
    }
}
  return text;
}
export function getExpressColumns(){
  var _this = this;
  return [
    {
      title: '保单号',
      dataIndex: 'cPlyNo',
      align: 'center',
    },
    {
      title: '投保人姓名', // ----
      dataIndex: 'postName',
      align: 'center',
    },
     {
      title: '被保人姓名',
      dataIndex: 'cInsrntCnm',
      align: 'center',
    },
    {
      title: '渠道', // ----
      dataIndex: 'extEnterpCode',
      align: 'center',
      render:(val)=>{
        return getChannelNameByValue(val);
      },
    },
    {
      title: '产品', // ----
      dataIndex: 'prodNo',
      align: 'center',
      render:(text, record)=>{
        return getProductNameByExtEnterpCodeAndProdNo(record.extEnterpCode, text);
      },
    },
    {
      title: '寄送类型', // ----
      dataIndex: 'expressType',
      align: 'center',
      render: (text) => {
        return (<span>{formaUnit(text,'expressType')}</span>)
      }
    },
    {
      title: '运单号',
      dataIndex: 'postNo',
      align: 'center',
    },
    {
      title: '快递公司名称',
      dataIndex: 'expressCode',
      align: 'center',
      render: (text) => {
        return (<span>{formaUnit(text,'expressCode')}</span>)
      }
    },
    {
      title: '快递状态',
      dataIndex: 'postStatus',
      align: 'center',
        render: (text) => {
          return (<span>{formaUnit(text,'postStatus')}</span>)
        }
    },
    {
      title: '最新快递路由信息', // ---
      dataIndex: 'orderStatus',
      align: 'center',
      render: (text) => {
          return (<span>{formaUnit(text,'orderStatus')}</span>)
        }
    },
    {
      title: '快递签收时间', // ---
      dataIndex: 'contractTime',
      align: 'center',
    },
    {
      title: '收件人姓名', // ---
      dataIndex: 'postName1',
      align: 'center',
      render: (text, record, index) => {
          return (<span>{record.postName}</span>)
        }
    },
    {
      title: '快递地址',
      dataIndex: 'postAddress',
      align: 'center',
    },
    {
      title: '联系电话', // ---
      dataIndex: 'cMobileNo',
      align: 'center',
    },
    {
      title: '快递下单时间', // ---
      dataIndex: 'expressPlaceOrderDate',
      align: 'center',
    },
    {
      title: '纸质回执状态',
      dataIndex: 'paperReceiptFlag',
      align: 'center',
        render: (text) => {
          return (<span>{formaUnit(text,'paperReceiptFlag')}</span>)
        }
    },
    {
      title: '纸质回执入库日期',
      dataIndex: 'scanCodeDate',
      align: 'center',
    },
    {
        title: '备注',
        dataIndex: 'remark',
        align: 'center',
    },
  ];
}

export function getImageColumns({ emailSend }) {
  return [
    {
      title: '保单号',
      dataIndex: 'cPlyNo',
      align: 'center'
    },
    {
      title: '影像类型',
      dataIndex: 'uploadType',
      align: 'center'
    },
    {
      title: '打印单证流水号',
      dataIndex: 'vocherNo',
      align: 'center'
    },
    {
      title: '上传日期',
      dataIndex: 'uploadDate',
      align: 'center',
      render: (code) => {
        if (code) {
          return (<span>{moment(code).format('YYYY-MM-DD')}</span>)
        } else {
          return (<div></div>)
        }
      }
    },
    {
      title: '影像文件编号',
      dataIndex: 'imageId',
      align: 'center'
    },
    {
      title: '档案编号',
      dataIndex: 'serialNo',
      align: 'center'
    },
    {
      title: '档案柜编号',
      dataIndex: 'recordNo',
      align: 'center'
    },
    {
      title: '保单类型',
      dataIndex: 'cElcFlag',
      align: 'center',
    },
    {
      title: '首次发送邮件时间',
      dataIndex: 'fristSendDate',
      align: 'center',
      render: (text) => {
        return text ? moment(text).format('YYYY-MM-DD') : '';
      }
    },
    {
      title: '同意转换时间',
      dataIndex: 'agreeElcDate',
      align: 'center',
      render: (text) => {
        return text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : '';
      }
    },
    {
      title: '红包提现时间',
      dataIndex: 'accountDate',
      align: 'center',
      render: (text) => {
        return text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : '';
      }
    },
    {
      title: '首次查阅时间',
      dataIndex: 'firstViewDate',
      align: 'center',
      render: (text) => {
        return text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : '';
      }
    },
    {
      title: '最新查阅时间',
      dataIndex: 'lastViewDate',
      align: 'center',
      render: (text) => {
        return text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : '';
      }
    },
    {
      title: '查阅次数',
      dataIndex: 'viewNum',
      align: 'center',
    },
    {
      title: '文件',
      dataIndex: 'downloadUrl',
      align: 'center',
      render: (text, record) => {
        if (record.migrateFlag === '1') {
          return <div>
            <Button onClick={() => showPicture(text)}>查看</Button>
          </div>
        } else if (record.migrateFlag === '0') {
          return '未迁移'
        } else if (record.migrateFlag === '2') {
          return '生成pdf失败'
        } else {
          return '迁移失败'
        }
      }
    },
    {
      title: '邮件发送',
      dataIndex: 'emailSend',
      align: 'center',
      render: (text, record) => {
        if (record.uploadType === '回执影像') {
          return null;
        }
        return (
            <Popconfirm 
            title={`是否发送邮件至${record.cEmail}`} onConfirm={() => emailSend({ cEmail: record.cEmail, cPlyNo: record.cPlyNo })} okText="是" cancelText="否">
                  <Button disabled={record.status === '0'}>发送</Button>
            </Popconfirm>
        )
      }
    }
  ]
}

export function showPicture(text) {
  window.open(text)
}

export function getPrintColumns() {
  return [
    {
      title: '保单号',
      dataIndex: 'cPlyNo',
      align: 'center'
    },
    {
      title: '打印类型',
      dataIndex: 'printType',
      align: 'center'
    },
    {
      title: '打印发票期次',
      dataIndex: 'period',
      align: 'center'
    },
    {
      title: '打印时间',
      dataIndex: 'printDate',
      align: 'center',
      render: (code) => {
        if (code) {
          return (<span>{moment(code).format('YYYY-MM-DD HH:mm:ss')}</span>)
        } else {
          return (<div></div>)
        }
      }
    },
    {
      title: '单证号',
      dataIndex: 'printNo',
      align: 'center'
    },
    {
      title: '操作人员',
      dataIndex: 'operCode',
      align: 'center'
    }
  ]
}

export function getSmsColumns() {
  return [
    {
      title: '业务系统编号',
      dataIndex: 'sysId',
      align: 'center',
      width: 100
    },
    {
      title: '保单号',
      dataIndex: 'sysSmId',
      align: 'center',
      width: 170
    },
    {
      title: '机构码',
      dataIndex: 'depCode',
      align: 'center',
      width: 80
    },
    {
      title: '产品代码',
      dataIndex: 'classCode',
      align: 'center',
      width: 80
    },
    {
      title: '业务代码',
      dataIndex: 'businessCode',
      align: 'center',
      width: 80
    },
    {
      title: '短信创建时间',
      dataIndex: 'createTime',
      align: 'center',
      width: 150,
      render: (code) => {
        if (code) {
          return (<span>{moment(code).format('YYYY-MM-DD HH:mm:ss')}</span>)
        } else {
          return (<div></div>)
        }
      }
    },
    {
      title: '短信内容',
      dataIndex: 'smContent',
      align: 'center',
      width: 600
    }
  ]
}
export function getRecallColumns() {
  return [
    {
      title: '取消续保原因',
      dataIndex: 'cancelRenewalFlag',
      align: 'center'
    },
    {
      title: '数据类型',
      dataIndex: 'dataType',
      align: 'center'
    },
    {
      title: '优惠措施',
      dataIndex: 'favorableFlag',
      align: 'center'
    },
    {
      title: '讯息单的导入类型',
      dataIndex: 'impCaseType',
      align: 'center'
    },
    {
      title: '导入人员代码',
      dataIndex: 'impTsr',
      align: 'center'
    },
    {
      title: '导入日期',
      dataIndex: 'importDate',
      align: 'center'
    },
    {
      title: '保单号',
      dataIndex: 'policyNo',
      align: 'center'
    },
    {
      title: '挽留成功保费备注',
      dataIndex: 'premiumcharge',
      align: 'center'
    },
    {
      title: '是否保全',
      dataIndex: 'preservationFlag',
      align: 'center'
    },
    {
      title: '保全成功月',
      dataIndex: 'preservationMonth',
      align: 'center'
    },
    {
      title: '处理日期',
      dataIndex: 'processDate',
      align: 'center'
    },
    {
      title: '保单到期时间',
      dataIndex: 'processDateEnd',
      align: 'center'
    },
    {
      title: '处理人员签名',
      dataIndex: 'processName',
      align: 'center'
    },
    {
      title: '备注',
      dataIndex: 'remark',
      align: 'center'
    },
    {
      title: '留用备注',
      dataIndex: 'remark1',
      align: 'center'
    },
    {
      title: '任务状态',
      dataIndex: 'status',
      align: 'center'
    },
    {
      title: '任务号',
      dataIndex: 'taskID',
      align: 'center'
    },
    {
      title: '处理人员代码',
      dataIndex: 'tsr',
      align: 'center'
    }
  ]
}

export function getSurrenderColumns() {
  return [
    {
      title: '回盘标志',
      dataIndex: 'backFlag',
      align: 'center'
    },
    {
      title: '是否接通',
      dataIndex: 'callFlag',
      align: 'center'
    },
    {
      title: '导入人员',
      dataIndex: 'impTsr',
      align: 'center'
    },
    {
      title: '导入日期',
      dataIndex: 'importDate',
      align: 'center'
    },
    {
      title: '投保单号',
      dataIndex: 'orderNo',
      align: 'center'
    },
    {
      title: '处理日期',
      dataIndex: 'processDate',
      align: 'center'
    },
    {
      title: '处理结果',
      dataIndex: 'processResult',
      align: 'center'
    },
    {
      title: '回访结果',
      dataIndex: 'recallResult',
      align: 'center'
    },
    {
      title: '是否退款',
      dataIndex: 'refundCharge',
      align: 'center'
    },
    {
      title: '处理状态',
      dataIndex: 'refundStatus',
      align: 'center'
    },
    {
      title: '任务处理状态',
      dataIndex: 'status',
      align: 'center'
    },
    {
      title: '任务号',
      dataIndex: 'taskID',
      align: 'center'
    },
    {
      title: 'TSR',
      dataIndex: 'tsr',
      align: 'center'
    },
    {
      title: '合同回收情况',
      dataIndex: 'voucherFlag',
      align: 'center'
    }
  ]
}

export function getDockingColumns() {
  return [
    {
      title: '取消续保原因',
      dataIndex: 'cancelRenawal',
      align: 'center'
    },
    {
      title: '结案结果',
      dataIndex: 'closeCaseDesc',
      align: 'center'
    },
    {
      title: '数据类型',
      dataIndex: 'dataType95556',
      align: 'center'
    },
    {
      title: '是否邮递续保通知函',
      dataIndex: 'deliveryRenewal',
      align: 'center'
    },
    {
      title: '批改处理标志',
      dataIndex: 'endorFlag',
      align: 'center'
    },
    {
      title: '优惠措施',
      dataIndex: 'favorableFlag',
      align: 'center'
    },
    {
      title: '登记人员代码',
      dataIndex: 'impTsr',
      align: 'center'
    },
    {
      title: '录入日期',
      dataIndex: 'importDate',
      align: 'center'
    },
    {
      title: '是否已与招行联系',
      dataIndex: 'linkMCBK',
      align: 'center'
    },
    {
      title: '保单号',
      dataIndex: 'policyNo',
      align: 'center'
    },
    {
      title: '挽留成功保费备注',
      dataIndex: 'preservationCharge',
      align: 'center'
    },
    {
      title: '是否保全成功',
      dataIndex: 'preservationFlag',
      align: 'center'
    },
    {
      title: '保全成功月',
      dataIndex: 'preservationMonth',
      align: 'center'
    },
    {
      title: '处理日期',
      dataIndex: 'processDate',
      align: 'center'
    },
    {
      title: '处理人名字',
      dataIndex: 'processName',
      align: 'center'
    },
    {
      title: '紧急程度',
      dataIndex: 'rapidFlag',
      align: 'center'
    },
    {
      title: '备注',
      dataIndex: 'remark',
      align: 'center'
    },
    {
      title: '处理状态',
      dataIndex: 'status',
      align: 'center'
    },
    {
      title: '任务号',
      dataIndex: 'taskID',
      align: 'center'
    },
    {
      title: 'TSR',
      dataIndex: 'tsr',
      align: 'center'
    }
  ]
}

export function getOperColumns() {
  return [
    {
      title: '保单号',
      dataIndex: 'cPlyNo',
      align: 'center'
    },
    {
      title: '操作人',
      dataIndex: 'operName',
      align: 'center'
    },
    {
      title: '操作时间',
      dataIndex: 'operTime',
      align: 'center'
    },
    {
      title: '操作动作',
      dataIndex: 'operFlag',
      align: 'center'
    },
    {
      title: '登录系统时间',
      dataIndex: 'operEnterTime',
      align: 'center'
    },
    {
      title: '处理结果',
      dataIndex: 'operResultFlag',
      align: 'center'
    },
    {
      title: '操作记录',
      dataIndex: 'operRemark',
      align: 'center'
    }
  ]
}
 