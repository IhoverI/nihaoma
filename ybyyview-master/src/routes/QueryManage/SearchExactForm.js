import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
} from 'snk-web';
import styles from './PolicyQuery.less';

const FormItem = Form.Item;

@connect(({ searchlist, loading }) => ({
  policyQuerySearchFormData: searchlist.policyQuerySearchFormData,
  loading: loading.effects,
}))
@Form.create()
export default class SearchExactForm extends PureComponent {
  
  handleSearch = e => {
    //防止链接打开 URL
    if(e)e.preventDefault();
    const { dispatch, form } = this.props;
    //表单校验
    form.validateFields((err, fieldsValue) => {
        if (err) return;
        const params = {...fieldsValue}
        if(this.props.onSearch) this.props.onSearch(params);
    });
  }
  onHandleReset=()=>{
    console.log(this.props.form);
    this.props.form.setFieldsValue({
      cPlyNo: '',
      cPlyAppNo: '',
    });
  }
  render() {
    const { policyQuerySearchFormData, form } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 16 },
      },
   };
    return (
      <Form onSubmit={this.handleSearch}>
            <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
              <Col md={10} sm={24}>
                <FormItem
                  {...formItemLayout}
                  style={{marginBottom:'0'}}
                  label="保单号">
                  {
                    getFieldDecorator('cPlyNo', {
                      initialValue: policyQuerySearchFormData.cPlyNo,
                      rules: [{ max: 30, message: '请输入正确的保单号' }],
                    })
                    (<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
              <Col md={10} sm={24}>
                <FormItem 
                  label="投保单号"
                  {...formItemLayout}
                  style={{marginBottom:'0'}}
                > 
                    {getFieldDecorator('cPlyAppNo', {
                      initialValue: policyQuerySearchFormData.cPlyAppNo,
                      rules: [{ max: 30, message: '请输入正确的投保单号' }],
                    })(<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
            </Row>
            <div className={styles.policyQueryOperator} style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
              <span style={{ marginBottom: 24 }}>
                <Button
                  loading={this.props.loading['searchlist/SearchPolicyQuery']}
                  htmlType="submit"
                  type="primary"
                >
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.onHandleReset}>
                  重置
                </Button>
              </span>
            </div>
          </Form>
    );
  }
}
