import React, { Fragment } from 'react';
import { connect } from 'dva';
import {
	Table,
	Card,
	Button,
	Form,
	Modal,
  Popconfirm,
  message,
} from 'snk-web';
import moment from 'moment';

import './PolicyList.less';
import { routerRedux } from 'dva/router';
class ValueAddItem extends React.PureComponent {
  constructor(props) {
    super(props);
		const { cPlyNo='', cCertfCde='', cCertfCls='' } = this.props;
		this.state = {
			cCertfCde,
			cCertfCls,
      cPlyNo,
      isExpressModal: false,
      givenCount: 0,
      isShowRechargeModal: false,
      currentRechargeRecordDetail: [],
      isPreserve: false,
      retryIndex: null,
      giveGiftsIndex: null,
		}
	}
  // 保全人
  columns1 = [
    {
      title: '保全状态',
      dataIndex: 'saveFlag',
      align: 'center',
      render: (text, record) => {
        const { saveFlag } = record;
        return saveFlag === '1' ? '未保全' : '已保全';
      }
    },
    {
      title: '保全等级',
      dataIndex: 'rankName',
      align: 'center',
    },
    {
      title: '赠送服务',
      dataIndex: 'serviceName',
      align: 'center',
    },
    {
      title: '保全日期',
      dataIndex: 'saveDate',
      align: 'center',
    },
    {
      title: '操作人',
      dataIndex: 'createUser',
      align: 'center',
    },
    {
      title: '操作',
      dataIndex: '',
      align: 'center',
      render: (text, record, index) => {
        const { saveFlag, flag } = record;
        const isDisable = (flag === '2' || flag === '3') || this.state.isPreserve;
        return (
          saveFlag === '1' ? 
          <Fragment>
            <Popconfirm title="确定保全成功并赠送服务?" onConfirm={this.confirmPreserve.bind(this, record, index)} okText="是" cancelText="否">
              <Button loading={this.state.giveGiftsIndex === index ? this.props.loading['valueAdd/giveGifts'] : false} size="small" type="primary" disabled={isDisable} style={{color: isDisable ? '#333' : ''}}>保全成功</Button>
            </Popconfirm>
          </Fragment> : ''
        )
      }
    }
  ]
  // 已赠送服务
  columns = [
    {
      title: '序号',
      dataIndex: 'index',
      align: 'center',
      render: (text, record, index) => index + 1,
    },
    {
      title: '服务类别',
      dataIndex: 'type',
      align: 'center',
    },
    {
      title: '服务名称',
      dataIndex: 'serviceName',
      align: 'center',
    },
    {
      title: '服务状态',
      dataIndex: 'status',
      align: 'center',
    },
    {
      title: '服务次数',
      dataIndex: 'serviceCount',
      align: 'center',
      render: (text, record) => {
        const { serviceType, pkgServiceType } = record;
        if (parseInt(serviceType, 10) === 7 || parseInt(serviceType, 10) === 6 ) {
          return text;
        } else {
          return '-';
        }
      },
    },
    {
      title: '保全日期',
      dataIndex: 'saveDate',
      align: 'center',
    },
    {
      title: '首次赠送日期',
      dataIndex: 'sendDate',
      align: 'center',
    },
    {
      title: '领用日期',
      dataIndex: 'receiveDate',
      align: 'center',
    },
    {
      title: '失效日期',
      dataIndex: 'invalidDate',
      align: 'center',
    },
    {
      title: '失效原因',
      dataIndex: 'invalidDesc',
      align: 'center',
    },
		{ 
			title: '操作', 
			dataIndex: 'serviceType', 
			key: 'serviceType', 
			align: 'center', 
			render: (text, record,index) => {
        const { serviceNo, serviceName, status } = record; 
        const isExpressage = parseInt(record.serviceType, 10) === 4 || parseInt(record.serviceType, 10) === 5 || parseInt(record.pkgServiceType, 10) === 4 || parseInt(record.pkgServiceType, 10) === 5;
        const isShowRechargRecord = parseInt(record.serviceType, 10) === 7 && parseInt(record.pkgServiceType, 10) === 3;
				return ( 
					<Fragment> 
						<Popconfirm title="确定重发消息？" onText="重发" cancelText="取消" onConfirm={this.retryNote.bind(this, serviceNo, serviceName, index)}> 
							<Button size="small" loading={this.state.retryIndex === index ? this.props.loading['valueAdd/retryNote'] : false} type="primary" disabled={status === '已使用' || status === '可使用' ? false : true}>重发消息</Button> 
						</Popconfirm>
            {
              isExpressage ? (
                <Button size="small" loading={this.props.loading['valueAdd/expressList']} style={{ marginLeft: '5px' }} type="primary" onClick={this.checkExpress.bind(this, serviceNo)}>查看物流</Button>
              ) : null
            }
            {
              isShowRechargRecord ? <Button size="small" style={{ marginLeft: '5px' }} type="primary" onClick={this.showRechargeRecord.bind(this, record)}>充值记录</Button> : null
            }
            
					</Fragment> 
				)
			}, 
		}, 
  ];
  // 物流信息
  columns2 = [
    {
      title: '快递类型',
      dataIndex: 'postType',
      align: 'center',
      render: (text) => {
        let prod = '';
        switch (text) {
          case '1':
            prod = '寄出';
            break;
          case '4':
            prod = '送检';
            break;
          case '2':
          case '3':
            prod = '回寄';
            break;
          default:
            break;
        }
        return prod;
      }
    },
    {
      title: '快递公司',
      dataIndex: 'expressName',
      align: 'center',
    },
    {
      title: '快递单号',
      dataIndex: 'expressNo',
      align: 'center',
    },
    {
      title: '快递状态',
      dataIndex: 'status',
      align: 'center',
    },
    {
      title: '最新路由状态',
      dataIndex: 'routerInfo',
      align: 'center',
    },
    {
      title: '收件地址',
      dataIndex: 'detailAddress',
      align: 'center',
    },
    {
      title: '收件电话',
      dataIndex: 'receiverPhone',
      align: 'center',
      render: (text, record) => {
        const { postType } = record;
        return postType === '1' ? text : '-';
      }
    },
    {
      title: '下单时间',
      dataIndex: 'orderDate',
      align: 'center',
      render: (text) => text ? moment(text).format('YYYY-MM-DD hh:mm:ss') : '',
    },
    {
      title: '签收时间',
      dataIndex: 'receivedDate',
      align: 'center',
      render: (text) => text ? moment(text).format('YYYY-MM-DD hh:mm:ss') : '',
    }
  ];
  // 充值记录
  columns3 = [
    {
      title: '序号',
      dataIndex: 'index',
      align: 'center',
      render: (text, record, index) => index + 1
    },
    {
      title: '充值账号',
      dataIndex: 'exchangePhone',
      align: 'center',
    },
    {
      title: '充值时间',
      align: 'center',
      render: (text, record) => {
        return moment(record.rechargeDate).format('YYYY-MM-DD')
      }
    },
  ]
  confirmPreserve = (record, index) => {
    const { cPlyNo, cCertfCde, cCertfCls, transactionCode } = this.props;
    const { serviceType } = record;
    this.setState({
      isPreserve: true,
      giveGiftsIndex: index,
    });
    this.props.dispatch({
      type: 'valueAdd/giveGifts',
      payload: {
        cPlyNo,
        cCertfCde,
        cCertfCls,
        serviceType,
        transactionCode,
      }
    }).then((result) => {
      this.setState({
        isPreserve: false,
      });
      const { code, message: msg } = result;
      if (code === 0) {
        message.success(msg || '重发成功');
      }
      if (code === 1) {
				message.error(msg);
      }
    })
  }

  checkExpress = (serviceNo) => {
    this.props.dispatch({
      type: 'valueAdd/expressList',
      payload: {
        serviceNo,
      }
    })
    this.setState({
      isExpressModal: true
    });
  }

  retryNote = (serviceNo, serviceName, index) => {
    const { cCertfCde, cCertfCls, cPlyNo } = this.state;
    this.setState({
      retryIndex: index,
    })
		this.props.dispatch({ 
			type: 'valueAdd/retryNote', 
			payload: {
				cCertfCde,
				cCertfCls,
				serviceNo,
				cPlyNo,
				serviceName,
			} 
		}).then((result) => {
      const { code, message: msg } = result;
      if (code === 0) {
        message.success(msg || '重发成功');
      }
      if (code === 1) {
        message.error(msg || '重发失败');
      }
    }); 
  };
  
  toIntegralPage = () => {
    this.props.dispatch(routerRedux.push(`/query-manage/search-integral?cCertfCde=${this.props.cCertfCde}`));
  }

  showRechargeRecord = (record) => {
    this.setState(() => ({
      isShowRechargeModal: true,
      currentRechargeRecordDetail: record.orderList,
    }));
  }

  render() {
    const {
      valueAdd: { serviceDetailsQueryList: { serviceList = [], saveList = [] }, detailExpressInfo },
    } = this.props;
    let flag;
    if(saveList.length) {
      flag = saveList[0].saveFlag;
    }
    return (
      <Fragment>
        <Card>
          <Table
            columns={this.columns}
            dataSource={serviceList}
            bordered
            rowKey={item => item.saveDate}
            pagination={false}
            scroll={{x: 1400}}
            size='small'
            title={() => {
              return (
                <Fragment>
                  <span>已赠送服务</span>
                  <Button style={{ marginLeft: '10px' }} onClick={this.toIntegralPage} size="small" type="primary">查看客户所有增值服务</Button>
                </Fragment>
              );
            }}
          />
        </Card>
        <Card>
          <Table
            columns={this.columns1}
            dataSource={saveList}
            bordered
            rowKey={item => item.saveDate}
            pagination={false}
            size='small'
            title={() => `保全状态：${flag === '0' ? '已保全' : '未保全' }` }
          />
        </Card>
        <Modal
          maskClosable={false}
          title='物流信息'
          visible={this.state.isExpressModal}
          onOk={() => this.setState({ isExpressModal: false })}
          onCancel={() => this.setState({ isExpressModal: false })}
          width="80%"
        >
          <Table
            columns={this.columns2}
            dataSource={detailExpressInfo}
            bordered
            rowKey={item => item.orderNo}
            pagination={false}
            scroll={{x: 1000}}
            size='small'
          />
        </Modal>
        <Modal
          maskClosable={false}
          title="充值记录"
          visible={this.state.isShowRechargeModal}
          onOk={() => this.setState({ isShowRechargeModal: false })}
          onCancel={() => this.setState({ isShowRechargeModal: false })}
        >
          <Table
            columns={this.columns3}
            dataSource={this.state.currentRechargeRecordDetail}
            bordered
            rowKey={item => item.rechargeDate}
            pagination={false}
            size='small'
            title={() => '充值记录' }
          />
        </Modal>
      </Fragment>
    );
  }
}

export default connect(({ valueAdd, loading }) => ({
	valueAdd,
  loading: loading.effects,
}))(Form.create()(ValueAddItem));
