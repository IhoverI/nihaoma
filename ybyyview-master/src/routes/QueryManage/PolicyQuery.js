import React, { PureComponent } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Card,
  Button,
  message,
  Table,
  Tabs,
  Select,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import SearchForm from './SearchForm';
import SearchExactForm from './SearchExactForm';
import styles from './PolicyQuery.less';

const columns = [
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
    render: (text, record) => <a href={`/#/query-manage/policy-list?cPlyNo=${record.cPlyNo}&prePath=/query-manage/policy-query`}>{text}</a>
  },
  {
    title: '原保单号',
    dataIndex: 'cPlyNoOld',
    align: 'center',
  },
  {
    title: '产品名称',
    dataIndex: 'prodName',
    align: 'center',
  },
  {
    title: '保单状态',
    dataIndex: 'flag',
    align: 'center',
  },
  {
    title: '最新扣款状态',
    dataIndex: 'lastChargeFlag',
    align: 'center',
  },
  {
    title: '是否继续请款',
    dataIndex: 'payAgainFlag',
    align: 'center',
  },
  {
    title: '成功扣款期次',
    dataIndex: 'successTime',
    align: 'center',
  },
  {
    title: '保险起期',
    dataIndex: 'insrncBeginDate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
  },
  {
    title: '被保人姓名',
    dataIndex: 'cInsrntCnm',
    align: 'center',
  },
  {
    title: '投保人姓名',
    dataIndex: 'cAppNme',
    align: 'center',
  },
  {
    title: '续保次数',
    dataIndex: 'renewalTime',
    align: 'center',
  },
  {
    title: '续保状态',
    dataIndex: 'renewalFlag',
    align: 'center',
  },
  {
    title: '保单类型',
    dataIndex: 'cElcFlag',
    align: 'center',
  },
  {
    title: '投保单号',
    dataIndex: 'cPlyAppNo',
    align: 'center',
  },
];

@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.effects,
}))
export default class PolicyQuery extends PureComponent {
  //发送请求--列表
  onFetchPolicyData=(params)=>{
    this.fieldsValue = params;
    this.handleTablePage();
 }
  handleTablePage=(pageSizeOptions = {})=>{
    const { current:pageNum = 1, pageSize = 10 } = pageSizeOptions;
    const formParams = this.fieldsValue;
    let policyQuerySearchFormData = {};
    if (this.fieldsValue) {
      if (!formParams.insrncCardNo) {
        for( let key in formParams) {
          if (!formParams[key]) {
            delete formParams[key];
          }
        }
      }
      policyQuerySearchFormData = { pageNum, pageSize, ...formParams };
    } else {
      policyQuerySearchFormData = { ...this.props.searchlist.policyQuerySearchFormData, pageNum, pageSize, isPageChange: true };
    }
    this.props.dispatch({
      type:'searchlist/SearchPolicyQuery',
      payload:policyQuerySearchFormData,
    });
  }

  // 导出excel
  onExportExcel=()=>{
    const { PolicyQueryTotal} = this.props.searchlist;
    if (PolicyQueryTotal <= 0) {
      message.warning('请重新查询后，再进行导出操作！');
      return;
    }
    if (PolicyQueryTotal > 10000) {
      message.warning('数据超过1万条，不允许导出！');
      return;
    }
    this.props.dispatch({
      type: 'searchlist/policyExcel',
      payload: this.fieldsValue,
    }).catch(()=>{
      message.error('网络异常请稍后再试');
    });

  }
  changeTabs = (activeKey) => {
    this.props.dispatch({
      type: 'searchlist/save',
      payload: {
        policyQueryDefaultTab: activeKey,
      }
    })
  }
  renderAdvancedForm() {
    const { policyQueryDefaultTab } = this.props.searchlist;
    return (
      <Tabs defaultActiveKey={policyQueryDefaultTab} onChange={this.changeTabs}>
        <Tabs.TabPane tab={<span>精确查询</span>} key="1">
          <SearchExactForm onSearch={this.onFetchPolicyData}/>
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span>模糊查询</span>} key="2">
          <SearchForm onSearch={this.onFetchPolicyData}/>
        </Tabs.TabPane>
      </Tabs>
    );
  }

  render() {
    const { PolicyQueryData = [], PolicyQueryTotal, policyQuerySearchFormData={} } = this.props.searchlist;
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
        <div>
            <div>{this.renderAdvancedForm()}</div>
            <div className={styles.policyQueryOperator}>
              <Table
                loading={this.props.loading['searchlist/SearchPolicyQuery']}
                rowKey={record => record.cPlyNo}
                dataSource={PolicyQueryData}
                scroll={{x: 1400}}
                bordered
                title={()=>{
                  return (
                  <Button 
                    type='primary'
                    onClick={this.onExportExcel}
                    loading={this.props.loading['searchlist/policyExcel']}
                  >
                    导出excel
                  </Button>)
                }}
                size="small"
                pagination={{
                  current: policyQuerySearchFormData.pageNum || 1,
                  pageSize: policyQuerySearchFormData.pageSize || 10,
                  pageSizeOptions: ['10','20','30','40','50'],
                  showQuickJumper: true,
                  showSizeChanger: true,
                  showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                  total:PolicyQueryTotal,
                }}
                columns={columns}
                onChange={this.handleTablePage}
              />
            </div>
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
