import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  Input,
  DatePicker,
} from 'snk-web';
import moment from 'moment';
import { getSelectChild } from '../../utils/utils'
import ChannelAndProduction from '../../components/ChannelAndProduction';
import styles from './PolicyQuery.less';


const FormItem = Form.Item;
const { Option } = Select;
@connect(({ searchlist, loading }) => ({
  policyQuerySearchFormData: searchlist.policyQuerySearchFormData,
  loading: loading.effects,
}))
@Form.create()
export default class SearchForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      ChannelRule: {
        initialValue: props.policyQuerySearchFormData.oldEnterpCodeList || []
      },
      productionRule: {
        initialValue: props.policyQuerySearchFormData.prodNoList || []
      }
    }
  }
  onHandleReset=()=>{
    this.props.form.setFieldsValue({
      pyear: undefined,
      tInsrncTm: undefined,
      cBizCertNo: undefined,
      insrncCardNo: undefined,
      flag: undefined,
      isNewPly: undefined,
      cAppName: undefined,
      cInsrntCnm: undefined,
    });
    this.setState({
      ChannelRule: {
        initialValue: [],
      },
      productionRule: {
        initialValue: [],
      }
    })
  }
    handleSearch = e => {
    //防止链接打开 URL
    if(e)e.preventDefault();
    const { dispatch, form } = this.props;
    //表单校验
    form.validateFields((err, fieldsValue) => {
        if (err) return;
        const {tInsrncTm, insrncCardNo, ...restValues} = fieldsValue;
        let tInsrncBgnTmStart;
        let tInsrncBgnTmEnd;
        if(tInsrncTm && tInsrncTm.length){
          tInsrncBgnTmStart = tInsrncTm[0].format("YYYY-MM-DD");
          tInsrncBgnTmEnd = tInsrncTm[1].format("YYYY-MM-DD");
          delete fieldsValue.tInsrncTm;
        }
        const params = {...fieldsValue, tInsrncBgnTmStart, tInsrncBgnTmEnd}
        if(this.props.onSearch) this.props.onSearch(params);
    });
  }
 
  render() {
    const { policyQuerySearchFormData, form } = this.props;
    const { getFieldDecorator } = form;
    const { RangePicker } = DatePicker;
    return (
        <Form className='custom-form-wrapper' onSubmit={this.handleSearch} layout="inline">
            <ChannelAndProduction
              oldEnterpCodeMultiple = {true}
              prodNoMultiple = {true}
              ChannelRule={this.state.ChannelRule}
              productionRule={this.state.productionRule}
              channelFieldKey="oldEnterpCodeList"
              productionFieldKey="prodNoList"
              {...this.props} allowClear
            />
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <FormItem label="保单年份">
                  {getFieldDecorator('pyear',{
                    initialValue: policyQuerySearchFormData.pyear,
                  })(
                    <Select allowClear placeholder="请选择">
                      {getSelectChild('pyear')}
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label="保险起期">
                  {getFieldDecorator('tInsrncTm',{
                    initialValue: policyQuerySearchFormData.tInsrncTm,
                  })(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <FormItem label="投保人证件号码">
                  {getFieldDecorator('cBizCertNo',{
                    initialValue: policyQuerySearchFormData.cBizCertNo,
                  })(<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label="被保人证件号码">
                  {getFieldDecorator('insrncCardNo',{
                    initialValue: policyQuerySearchFormData.insrncCardNo,
                  })(<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
            </Row>
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={6} sm={24}>
                <FormItem label="保单状态">
                  {getFieldDecorator('flag',{
                    initialValue: policyQuerySearchFormData.flag,
                  })
                    (<Select placeholder='请选择'>
                      {getSelectChild('plyFlag')}
                    </Select>)}
                </FormItem>
              </Col>
              <Col md={6} sm={24}>
                <FormItem label="是否新单">
                  {getFieldDecorator('isNewPly',{
                    initialValue: policyQuerySearchFormData.isNewPly,
                  })
                    (<Select placeholder='请选择'>
                      <Option value='0'>是</Option>
                      <Option value='1'>否</Option>
                    </Select>)}
                </FormItem>
              </Col>
              <Col md={6} sm={24}>
                <FormItem label="投保人姓名">
                  {getFieldDecorator('cAppName',{
                    initialValue: policyQuerySearchFormData.cAppName,
                  })(<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
              <Col md={6} sm={24}>
                <FormItem label="被保人姓名">
                  {getFieldDecorator('cInsrntCnm',{
                    initialValue: policyQuerySearchFormData.cInsrntCnm,
                  })(<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
            </Row>
            <div className={styles.policyQueryOperator} style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
              <span style={{ marginBottom: 24 }}>
                <Button
                   loading={this.props.loading['searchlist/SearchPolicyQuery']}
                   htmlType="submit" type="primary" >
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.onHandleReset}>
                  重置
                </Button>
              </span>
            </div>
          </Form>
    );
  }
}
