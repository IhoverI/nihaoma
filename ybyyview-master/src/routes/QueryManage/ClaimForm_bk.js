import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import ClaimFormTable from './components/ClaimFormTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild } from '../../utils/utils'

import styles from './ClaimForm.less';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const statusMap = ['default', 'processing', 'success', 'error'];
const status = ['关闭', '运行中', '已上线', '异常'];
const columns = [
  {
    title: '保单号',
    dataIndex: 'code1',
  },
  {
    title: '投保单号',
    dataIndex: 'code2',
  },
  {
    title: '产品名称',
    dataIndex: 'code3',
  },
  {
    title: '被保人姓名',
    dataIndex: 'code4',
  },
  {
    title: '投保人姓名',
    dataIndex: 'code5',
  },
  {
    title: '保单状态',
    dataIndex: 'code6',
  },
  {
    title: '是否续保',
    dataIndex: 'code7',
  },
  {
    title: '续保次数',
    dataIndex: 'code8',
  },
  {
    title: '保险起期',
    dataIndex: 'code9',
  },
  {
    title: '是否电子保单',
    dataIndex: 'code10',
  },
  {
    title: '扣款成功次数',
    dataIndex: 'code11',
  },
  {
    title: '原保单号',
    dataIndex: 'code12',
  },
];



@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
export default class ClaimForm extends PureComponent {
  state = {
    expandForm: false,
    selectedRows: [],
    formValues: {},
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'rule/fetch',
    });
  }

  handleClaimFormTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'rule/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  };

//  toggleForm = () => {
  //  this.setState({
    //  expandForm: !this.state.expandForm,
    //});
  //};

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            no: selectedRows.map(row => row.no).join(','),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'rule/fetch',
        payload: values,
      });
    });
  };



  render() {
    const { rule: { data }, loading } = this.props;
    const { selectedRows } = this.state;
    const { getFieldDecorator } = this.props.form;

    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

  

    return (
      <PageHeaderLayout title="理赔信息查询">
        <Card bordered={false}>
            <Form className='custom-form-wrapper'  onSubmit={this.handleSearch} layout="inline">
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <FormItem label="投保人证件类型">
                <Select  placeholder="请选择" style={{ width: '100%' }} >
                  {getSelectChild('certiDesc')}
                </Select>
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label="投保人证件号码">
                  <Input placeholder="请输入" />
                </FormItem>
              </Col>
            </Row>
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <FormItem label="被保人证件类型">
                <Select  placeholder="请选择" style={{ width: '100%' }} >
                  {getSelectChild('certiDesc')}
                </Select>                
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label="被保人证件号码">
                  <Input placeholder="请输入" />
                </FormItem>
              </Col>
            </Row>
            <div className={styles.claimFormOperator} style={{ overflow: 'hidden' }}>
              <span style={{ float: 'right', marginBottom: 24 }}>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </span>
            </div>
            </Form>
            <div className={styles.claimFormOperator}>
              <Button icon="file-excel" type="primary" onClick={() => this.handleModalVisible(true)}>
                导出excel
              </Button>
            <ClaimFormTable
              selectedRows={selectedRows}
              loading={loading}
              data={data}
              columns={columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleClaimFormTableChange}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
