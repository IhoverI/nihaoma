import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  DatePicker,
  Input,
  Select,
  Popover,
  Collapse,
  Spin,
  Table,
  Tag,
  Checkbox,
  message,
} from 'snk-web';
import {  PermissionWrapper } from 'snk-sso-um';
import { connect } from 'dva';
import {Link} from 'dva/router';
import FooterToolbar from 'components/FooterToolbar';
import SingleShouYiRen from '../../components/SingleShouYiRen/SingleShouYiRen';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ValueAddItem from './ValueAddItem';
// import CustomRank from './CustomRank';
import {
  getPayColumns, getEndorColumns, getExpressColumns, getImageColumns, getOperColumns,
  getPrintColumns, getSmsColumns, getRecallColumns, getSurrenderColumns, getDockingColumns,
  getEndorCusColumns, getEndorCancelColumns, getThirdMessageColumns, getEndorOldColumns,
  getValueAddColumns,
} from './PolicyListColumn';
// import { valueAddData } from './valueAddData';

import styles from './PolicyList.less';
import { getDataSource, timestampToString, getUrlParam, datetimeFormat } from '../../utils/utils'


const { Option } = Select;
const { RangePicker } = DatePicker;
const  Panel  = Collapse.Panel;

const labelSource = getDataSource('labelSource');

class PolicyList extends PureComponent {
  constructor(props){
    super(props);
    let cPlyNo = getUrlParam(this.props.location.search).cPlyNo;
    let prePath = getUrlParam(this.props.location.search).prePath;
    this.state = {
      width: '100%',
      cPlyNo: cPlyNo || '',
      prePath: prePath || '',
      hasPlyNo: cPlyNo || false,
    }
  }

  viewBack = () => {
    this.props.history.push({
      pathname:  this.state.prePath,
      state: this.state.cPlyNo,
    });
  }


  componentDidMount() {
    const searchTerm = {
      cPlyNo: this.state.cPlyNo
    };
    const { dispatch } = this.props;
    dispatch({
      type: 'fastMoney/fetchPolicyList',
      payload: searchTerm,
    }).then(() => {
      // 增值服务
      const { fastMoney: { policyData } } = this.props;
      const baseProfile = policyData.data  ? policyData.data.baseDto : {}; // 基础信息
      this.setState({
        transactionCode: baseProfile ? baseProfile.caseDescribe : '',
      });
      dispatch({
        type: 'valueAdd/policyServiceQuery',
        payload: {
          transactionCode: baseProfile ? baseProfile.caseDescribe : '',
          ...searchTerm,
        },
      });
    });
    window.addEventListener('resize', this.resizeFooterToolbar);
  }


  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeFooterToolbar);
  }
  resizeFooterToolbar = () => {
    const sider = document.querySelectorAll('.ant-layout-sider')[0];
    const width = `calc(100% - ${sider.style.width})`;
    if (this.state.width !== width) {
      this.setState({ width });
    }
  };

  renderSelectOption = (data) => {
    return data.map(item => (
        <Option key={item.code} value={item.code}>{item.codeName || item.name}</Option>
    ));
  }

  emailSend = (param) => {
    this.props.dispatch({
      type: 'valueAdd/sendEmail',
      payload: {
        to: param.cEmail,
        cPlyNo: param.cPlyNo,
      }
    }).then((res) => {
      const { 
        code,
        data 
      } = res;
      if (code === 0) {
        message.success(data.message);
      } else {
        message.error(data.message);
      }
    });
  }
 
  render() {
    const { fastMoney, loading, selfchannelForm } = this.props;
    const {
      CountryCode,
      ItemAccOccupationCode,
      relationBetweenCode,
    } = selfchannelForm;

    const { fastMoney: { policyData }, valueAdd: { serviceDetailsQueryList, isSecPlyDate }, form, dispatch } = this.props;
    const { serviceList=[] } = serviceDetailsQueryList;

    const baseProfile = null == policyData.data ? undefined : policyData.data.baseDto; // 基础信息
    const payProfile = null == policyData.data ? undefined : policyData.data.payMap; // 请款信息
    const appProfile = null == policyData.data ? undefined : policyData.data.appDto; // 投保人信息
    const recProfile = null == policyData.data ? undefined : policyData.data.recDto; // 被保人信息
    const bnfProfile = null == policyData.data ? undefined : policyData.data.bnfDto; // 受益人信息
    const thirdProfile = null == policyData.data ? undefined : policyData.data.thirdDto; // 第三方卡号信息
    const imageDtoList = null == policyData.data ? undefined : policyData.data.imageDtoList; // 影像信息

    const endorOldDtoList = null == policyData.data ? undefined : policyData.data.endorOldDtoList; // 批发历史
    const endorDtoList = null == policyData.data ? undefined : policyData.data.endorDtoList; // 批改信息
    const endorCusDtoList = null == policyData.data ? undefined : policyData.data.endorCusDtoList; // 客户信息批改
    const endorCancelDtoList = null == policyData.data ? undefined : policyData.data.endorCancelDtoList; // 取消续保批改
    const thirdMessageDtoList = null == policyData.data ? undefined : policyData.data.thirdMessageDtoList; // 扣款渠道批改
    const valueAddDtoList = null == policyData.data ? undefined : policyData.data.valueAddDtoList; // 扣款渠道批改

    const expressList = null == policyData.data ? undefined : policyData.data.expressList; // 快递信息

    // 回访信息
    const recallDtoList = null == policyData.data ? undefined : policyData.data.recallDtoList;
    const surrenderDtoList = null == policyData.data ? undefined : policyData.data.surrenderDtoList;
    const dockingDtoList = null == policyData.data ? undefined : policyData.data.dockingDtoList;printDtoList

    const printDtoList = null == policyData.data ? undefined : policyData.data.printDtoList; // 打印信息
    const smsDtoList = null == policyData.data ? undefined : policyData.data.smsDtoList; // 短信信息
    const operDtoList = null == policyData.data ? undefined : policyData.data.operDtoList; // 保单操作日志信息

    const background = '#eee';

    return (
      <PageHeaderLayout 
      title="保单详情" 
      wrapperClassName={styles.policyList} 
      >
      <Spin spinning={loading} size='large'>
      { !loading && this.state.hasPlyNo ? 
        <Form hideRequiredMark layout="inline" className={styles.inlineForm}>
          <Collapse defaultActiveKey={['1']} onChange={this.callback}>
            {/*保单基本信息*/}
            <Panel header={labelSource.policyInfo} key="1" style={{background: background}} className={styles.policyInfo}>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='保单号'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile && undefined === baseProfile  ? '' : baseProfile.cPlyNo}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='保单状态'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.flag}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item  label='续保次数'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.renewalTime}
                  /> 
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='业务渠道'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.oldEnterpCode}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='产品名称'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.projectName}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='产品计划'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.cTgtFld1}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='每期保费'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.prm}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='扣款卡号'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.cardId}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='保险期间'>
                    <RangePicker disabled defaultValue=
                    {[null===baseProfile || undefined === baseProfile || baseProfile.insrncBeginDate === null ? '' : moment(baseProfile.insrncBeginDate), 
                      null===baseProfile || undefined === baseProfile || baseProfile.insrncBeginDate === null ? '' : moment(baseProfile.insrncEndDate)]} 
                    style={{ width: '100%' }} 
                    readOnly="true"
                    />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='投保日期'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : moment(baseProfile.appDate).format('YYYY-MM-DD')}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='保单类型'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.cElcFlag}
                  />                
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='缴别'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.payKind}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='订单号'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.caseDescribe}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='投保单号'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.cPlyAppNo}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='原保单号'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.cPlyNoOld}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label='续保新保单号'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.renewalPolicyNo}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={24} className={`${styles.address} ${styles.policyAddress}`}>
                <Form.Item label='客户要求投递地址'>
                  <Input
                    readOnly="true"
                    value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.insrncAddress}
                  />
                </Form.Item>
              </Col>
            </Row>
            </Panel>
            
            {/* appProfile 投保人基本信息 */}
            <Panel header={labelSource.applyInfo} key="2" style={{background: background}} className={styles.applyInfo}>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.applyClntNmeA}>
                  <Input
                    readOnly="true"
                    value={ null===appProfile || undefined === appProfile ? '' : appProfile.cClntNme }
                  />                
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
              <Form.Item label={labelSource.applySex}>
                <Input
                  readOnly="true"
                  value={null===appProfile || undefined === appProfile ? '' : appProfile.cSex}
                />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.appBirth}>
                  <Input
                    readOnly="true"
                    value={null===appProfile || undefined === appProfile || appProfile.tBornAge === null? '' : moment(appProfile.tBornAge).format('YYYY-MM-DD')}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.applyMobile}>
                  <Input
                    readOnly="true"
                    value={null===appProfile || undefined === appProfile ? '' : appProfile.cMobileNo}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.applyCertfCls}>
                  <Input
                    readOnly="true"
                    value={null===appProfile || undefined === appProfile ? '' : appProfile.cCertfCls}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.applyCertfCde}>
                  <Input
                    readOnly="true"
                    value={appProfile ? appProfile.cCertfCde : ''}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.identifyLongValid}>
                  <Checkbox checked={appProfile && appProfile.identifyLongValid === '1' ? true : false}>长期</Checkbox>
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.identifyExpiry}>
                  <Input
                    readOnly="true"
                    value={ appProfile && appProfile.identifyExpiry ? moment(appProfile.identifyExpiry).format('YYYY-MM-DD') : ''}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.country}>
                  <Select
                    optionFilterProp="children"
                    showSearch placeholder="请选择"
                    style={{ width: '100%' }}
                    value={ appProfile && appProfile.country ? appProfile.country : ''}
                    disabled
                  >
                    {this.renderSelectOption(CountryCode)}
                  </Select>
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.applyMail}>
                  <Input
                    readOnly="true"
                    value={null===appProfile || undefined === appProfile ? '' : appProfile.cMail}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.applyJobType}>
                  <Select
                    optionFilterProp="children"
                    showSearch placeholder="请选择"
                    style={{ width: '100%' }}
                    value={ appProfile && appProfile.occupationCode ? appProfile.occupationCode : ''}
                    disabled
                  >
                    {this.renderSelectOption(ItemAccOccupationCode)}
                  </Select>          
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.applyTel}>
                  <Input
                    readOnly="true"
                    value={null===appProfile || undefined === appProfile ? '' : appProfile.cTel}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.applyZipCde}>
                  <Input
                    readOnly="true"
                    value={null===appProfile || undefined === appProfile ? '' : appProfile.cZipCde}
                  />                
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={24} className={styles.address}>
                <Form.Item label={labelSource.applyClntAddr}>
                  <Input
                    readOnly="true"
                    value={null===appProfile || undefined === appProfile ? '' : appProfile.cClntAddr}
                  />
                </Form.Item>
              </Col>
            </Row>
            </Panel>

            {/* recProfile 被保人基本信息*/}
            <Panel header={labelSource.insrncInfo} key="3" style={{background: background}} className={styles.insrncInfo}>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.insrncName}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cClntNme}
                  />                
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.insrncSex}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cSex}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.insrncBirth}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile || recProfile.tBornAge === null ? '' : moment(recProfile.tBornAge).format('YYYY-MM-DD')}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.cMobileNoI}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cMobileNo}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.insrncCardType}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cCertfCls}
                  />                
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.insrncCardId}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cCertfCde}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.identifyLongValid}>
                  <Checkbox checked={recProfile && recProfile.identifyLongValid === '1' ? true : false}>长期</Checkbox>
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.identifyExpiry}>
                  <Input
                    readOnly="true"
                    value={ appProfile && recProfile.identifyExpiry ? moment(recProfile.identifyExpiry).format('YYYY-MM-DD') : ''}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.country}>
                  <Select
                    optionFilterProp="children"
                    showSearch placeholder="请选择"
                    style={{ width: '100%' }}
                    value={ recProfile && recProfile.country ? recProfile.country : ''}
                    disabled
                  >
                    {this.renderSelectOption(CountryCode)}
                  </Select>
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.cMailI}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cMail}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.cAppprofType}>
                  <Select
                    optionFilterProp="children"
                    showSearch placeholder="请选择"
                    style={{ width: '100%' }}
                    value={ recProfile && recProfile.occupationCode ? recProfile.occupationCode : '' }
                    disabled
                  >
                    {this.renderSelectOption(ItemAccOccupationCode)}
                  </Select>
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.cTelI}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cTel}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12} lg={12} xl={6}>
                <Form.Item label={labelSource.insrncPostCode}>
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cZipCde}
                  />                
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={24} className={styles.address}>
                <Form.Item label={labelSource.insrncAddress} >
                  <Input
                    readOnly="true"
                    value={null===recProfile || undefined === recProfile ? '' : recProfile.cClntAddr}
                  />
                </Form.Item>
              </Col>
            </Row>
            </Panel>

            {/*受益人与TSR基本信息*/}
            <Panel header={labelSource.beneTsrInfo} key="4" style={{background: background}} className={styles.beneTsrInfo}>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              <Col sm={24} md={8}>
                <Form.Item label={labelSource.tsrCode}>
                <Input
                  readOnly="true"
                  value={null===bnfProfile || undefined === bnfProfile ? '' : bnfProfile.tsrCode}
                />                
                </Form.Item>
              </Col>
              <Col sm={24} md={8}>
              <Form.Item label={labelSource.tsrName}>
                <Input
                  readOnly="true"
                  value={null===bnfProfile || undefined === bnfProfile ? '' : bnfProfile.tsrName}
                />
                </Form.Item>
              </Col>
              <Col sm={24} md={8}>
                <Form.Item label={labelSource.tsrTeam}>
                  <Input
                    readOnly="true"
                    value={null===bnfProfile || undefined === bnfProfile ? '' : bnfProfile.tsrTeam}
                  />
                </Form.Item>
              </Col>
            </Row>
            {/* 受益人列表 */}
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 40 }}>
              {/* 反洗钱业务需求修改页面 */}
              <Col sm={24} md={8}>
                <Form.Item label="受益方式">
                  <Input
                    readOnly="true"
                    style={{ width: '100%' }}
                    value={ bnfProfile && bnfProfile.benfType === '1' ? '指定' : '法定'}
                  />
                </Form.Item>
              </Col>
              {/* 当受益人只有投保人时，受益人直接是法定 */}
              {
                bnfProfile && bnfProfile.benfType === '0' ? (
                  <Fragment>
                    <Col sm={24} md={8}>
                      <Form.Item label="受益人姓名">
                        <Input
                          style={{ width: '100%' }}
                          readOnly="true"
                          value='法定'
                        />
                      </Form.Item>
                    </Col>
                    <Col sm={24} md={8}>
                      <Form.Item label="受益人与被保人关系">
                        <Input
                          style={{ width: '100%' }}
                          readOnly="true"
                          value='法定'
                        />
                      </Form.Item>
                    </Col>
                  </Fragment>
                ) : null
              }
              {
                bnfProfile && bnfProfile.benfType === '1' ? bnfProfile.bnfDtoList.map((item) =>{
                  return (
                    <div style={{ padding: '0 10px'}}>
                      <SingleShouYiRen
                        // parentForm={form}
                        // DeleteBeneificiary={this.DeleteBeneificiary}
                        policyMapInfo={item}
                        isSameWithTouBaoRen={true}
                        isDeleteBeneificiary={true}
                        key={item}
                        index={item}
                      />
                    </div>
                  );
                }) : null
              }
            </Row>
            </Panel>

            {/*请扣款信息*/}
            <Panel header={labelSource.payInfo} key="5" style={{background: background}}>
              <div>
                | 请款总数 {payProfile === undefined ? 0 : payProfile.paySum} 条 |
                请款成功数 {payProfile === undefined ? 0 : payProfile.paySuccess} 条 |
                请款失败数 {payProfile === undefined ? 0 : payProfile.payFail} 条 |
                退费总数 {payProfile === undefined ? 0 : payProfile.refundSum} 条 |
                退费成功数 {payProfile === undefined ? 0 : payProfile.refundSuccess} 条 |
                未回盘数 {payProfile === undefined ? 0 : payProfile.notCharge} 条 |
              </div>
              { payProfile === undefined || !payProfile.payDtoList ? null :
                <div style={{marginTop: 15}}>
                  <Table
                    columns={getPayColumns()}
                    dataSource={payProfile.payDtoList}
                    bordered={true}
                    pagination={false}
                    rowKey={record => record.serialNo}
                    scroll={{x: 1800}}
                    size='small'
                    />
                </div> }
            </Panel>

            {/*快递信息*/}
            <Panel header={labelSource.expressInfo} key="6" style={{background: background}}>
              { expressList === undefined || expressList.length === 0 ? '<暂无快递信息>' :
                <Table
                  columns={getExpressColumns()}
                  dataSource={expressList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  scroll={{x: 1800}}
                  size='small'
                  /> }
            </Panel>

            {/*影像信息*/}
            <Panel header='影像信息' key="7" style={{background: background}}>
              { imageDtoList === undefined || imageDtoList.length === 0 ? '<暂无影像信息>' :
                <Table
                  columns={getImageColumns({ emailSend: this.emailSend })}
                  dataSource={imageDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  size='small'
                  /> }
            </Panel>

            {/*回访信息*/}
            <Panel header={labelSource.revisitInfo} key="9" style={{background: background}}>
              { recallDtoList === undefined || recallDtoList.length === 0 ? '<暂无讯息单回访信息>' :
                <Table
                  columns={getRecallColumns()}
                  dataSource={recallDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  scroll={{x: 2800}}
                  size='small'
                  title={() => '讯息单回访信息'}
                  /> }
              <br />
              { surrenderDtoList === undefined || surrenderDtoList.length === 0 ? '<暂无退保回访信息>' :
                <Table
                  columns={getSurrenderColumns()}
                  dataSource={surrenderDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  scroll={{x: 2800}}
                  size='small'
                  title={() => '退保回访信息'}
                  /> }
              <br />
              { dockingDtoList === undefined || dockingDtoList.length === 0 ? '<暂无对接回访信息>' :
                <Table
                  columns={getDockingColumns()}
                  dataSource={dockingDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  scroll={{x: 2800}}
                  size='small'
                  title={() => '对接回访信息'}
                  /> }
            </Panel>

            {/*赠送积分*/}
            <Panel header={labelSource.integralInfo} key="11" style={{background: background}}>
              暂无赠送积分信息
            </Panel>

            {/*打印信息*/}
            <Panel header={labelSource.printInfo} key="12" style={{background: background}}>
              { printDtoList === undefined || printDtoList.length === 0 ? '<暂无打印信息>' :
                <Table
                  columns={getPrintColumns()}
                  dataSource={printDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  size='small'
                  /> }
            </Panel>

            {/*短信信息*/}
            <Panel header={labelSource.shortMessage} key="13" style={{background: background}}>
              { smsDtoList === undefined || smsDtoList.length === 0 ? '<暂无短信信息>' :
                <Table
                  columns={getSmsColumns()}
                  dataSource={smsDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  size='small'
                  scroll={{x: 1300}}
                  /> }
            </Panel>

            {/*保单操作日志*/}
            <Panel header={labelSource.policyLog} key="14" style={{background: background}}>
              { operDtoList === undefined || operDtoList.length === 0 ? '<暂无保单操作日志>' :
                <Table
                  columns={getOperColumns()}
                  dataSource={operDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  size='small'
                  /> }
            </Panel>

            {/*保单批改信息*/}
            <Panel header={labelSource.correctionInfo} key="15" style={{background: background}}>
              { endorOldDtoList === undefined || endorOldDtoList.length === 0 ? '<暂无历史批改信息>' :
                <Table
                  columns={getEndorOldColumns()}
                  dataSource={endorOldDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record) => record.tUdrDate}
                  size='small'
                  title={() => '历史批改信息'}
                />}
              <br />
              { endorDtoList === undefined || endorDtoList.length === 0 ? '<暂无保单注退批改信息>' :
                <Table
                  columns={getEndorColumns()}
                  dataSource={endorDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record) => record.endorTime}
                  scroll={{x: 1580}}
                  size='small'
                  title={() => '保单注退批改信息'}
                />}
              <br />
              { endorCusDtoList === undefined || endorCusDtoList.length === 0 ? '<暂无客户信息批改信息>' :
                <Table
                  columns={getEndorCusColumns()}
                  dataSource={endorCusDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  scroll={{x: 2280}}
                  size='small'
                  title={() => '客户信息批改信息'}
                /> }
              <br />
              { endorCancelDtoList === undefined || endorCancelDtoList.length === 0 ? '<暂无取消续保批改信息>' :
                <Table
                  columns={getEndorCancelColumns()}
                  dataSource={endorCancelDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  size='small'
                  title={() => '取消续保批改信息'}
                /> }
              <br />
              { thirdMessageDtoList === undefined || thirdMessageDtoList.length === 0 ? '<暂无扣款渠道批改信息>' :
                <Table
                  columns={getThirdMessageColumns()}
                  dataSource={thirdMessageDtoList}
                  bordered={true}
                  pagination={false}
                  rowKey={(record, index) => index}
                  size='small'
                  title={() => '扣款渠道批改信息'}
                /> }
            </Panel>

            {/*第三方卡号信息*/}
            <Panel header={labelSource.cardInfoInfo} key="16" style={{background: background}}>
              { thirdProfile !== null ?
              <div>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label='第三方扣款所属城市'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdPayCity}
                  />                
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                <Form.Item label='第三方扣款银行名称'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdBank}
                  />
                  </Form.Item>
                </Col>
                <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label='第三方扣款开户行名称'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdBranchBank}
                  />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label='第三方扣款账号'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdCardId}
                  />                
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                <Form.Item label='第三方扣款账号有效期'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdCardDate}
                  />
                  </Form.Item>
                </Col>
                <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label='第三方扣款账户姓名'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdAppName}
                  />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label='第三方扣款账户类型'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdCardType}
                  />                
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                <Form.Item label='第三方扣款客户证件类型'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdCertType}
                  />
                  </Form.Item>
                </Col>
                <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label='第三方扣款客户证件号码'>
                  <Input
                    readOnly="true"
                    value={null===thirdProfile || undefined === thirdProfile ? '' : thirdProfile.thirdCertNo}
                  />
                  </Form.Item>
                </Col>
              </Row></div> : '<暂无第三方卡号信息>' }
            </Panel>

            {/*增值服务*/}
            <Panel header={labelSource.valueAdd} key="17" style={{background: background}}>
              <ValueAddItem
                cPlyNo={this.state.cPlyNo}
                cCertfCde={appProfile ? appProfile.cCertfCde : ''}
                cCertfCls={appProfile ? (appProfile.cCertfCls === '身份证' ? '01' : '99') : ''}
                transactionCode={this.state.transactionCode}
              />
            </Panel>
          </Collapse>
          <Card>
            <PermissionWrapper permissionKey='扣款渠道批改'>
              <Link style={{marginLeft: 10}} to={{pathname: '/correction/fastmoney-correction', cPlyNo: this.state.cPlyNo}} >
                <Button type='primary'>扣款渠道批改</Button>
              </Link>
            </PermissionWrapper>
            <PermissionWrapper permissionKey='保单注退'>
              <Link style={{marginLeft: 30}} to={{pathname: '/correction/cancel-correction', cPlyNo: this.state.cPlyNo}} >
                <Button type='primary'>保单注退批改</Button>
              </Link>
            </PermissionWrapper>
            <PermissionWrapper permissionKey='客户信息批改'>
              <Link style={{marginLeft: 30}} to={{pathname: '/correction/basic-correction', cPlyNo: this.state.cPlyNo}} >
                <Button type='primary'>客户信息批改</Button>
              </Link>
            </PermissionWrapper>
            <PermissionWrapper permissionKey='取消续保'>
              <Link style={{marginLeft: 30}} to={{pathname: '/correction/review-correction', cPlyNo: this.state.cPlyNo}} >
               <Button type='primary'>取消续保批改</Button>
              </Link>
            </PermissionWrapper>
            {/* <PermissionWrapper permissionKey='增值服务'>
              <Link style={{marginLeft: 30}} to={{pathname: '/valueadd/value-added-info', transactionCode: this.state.transactionCode, cPlyNo: this.state.cPlyNo, cCertfCde: appProfile ? appProfile.cCertfCde : '', cCertfCls: appProfile ? (appProfile.cCertfCls === '身份证' ? '01' : '99') : '' }} >
               <Button type='primary'>赠送增值服务</Button>
              </Link>
            </PermissionWrapper> */}
            <Button style={{marginLeft: 30}} onClick={this.viewBack}>返回</Button>
          </Card>
          </Form> : null
        }
        </Spin>
      </PageHeaderLayout>
    );
  }
}

export default connect(({ fastMoney, valueAdd, loading, selfchannelForm }) => ({
  valueAdd,
  fastMoney,
  loading: loading.models.fastMoney,
  selfchannelForm,
}))(Form.create()(PolicyList));
