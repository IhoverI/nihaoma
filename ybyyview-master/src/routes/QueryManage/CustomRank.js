import React, { Fragment, PureComponent } from 'react';
import {
	Table,
	Card,
	Button,
	Tag,
	InputNumber,
	Form,
	Popconfirm,
	message,
	Spin,
} from 'snk-web';
import { connect } from 'dva';
class CustomRank extends PureComponent {
	constructor(props) {
		super(props);
		const { cPlyNo='', cCertfCde='', cCertfCls='', transactionCode='' } = this.props;
		this.state = {
			cCertfCde,
			cCertfCls,
			cPlyNo,
			snList: [],
			givenCount: 0,
			transactionCode,
			selectedRowKeys: [],
		}
	}

	componentWillMount() {
		const { valueAdd: { serviceDetailsQueryVipRank: { ranKName = '' } } } = this.props;
		if (ranKName) {
			let givenCount = 0;
			switch (ranKName) {
				case '黄金VIP': givenCount = 1; break;
				case '钻石VIP': givenCount = 1; break;
				case '铂金VIP': givenCount = 2; break;
				default: givenCount = 0; break;
			}
			this.setState({
				givenCount,
			})
		}
	}
  // 可赠送 
	columns2 = [ 
		{ 
			title: '可赠送的服务', 
			dataIndex: 'serviceName', 
			key:'serviceName', 
			align: 'center', 
		},
		{
			title: '数量',
			dataIndex: 'id',
			key: 'id',
			align: 'center',
			render: (text, record) => {
				return (
					<InputNumber
						disabled={!this.state.snList.some(item => item === record.serviceType) }
						min={1}
						max={this.state.givenCount}
						value={this.getCount(record.serviceType, this.state.snList)}
						onChange={this.numberChange.bind(this, record.serviceType)}
					/>
				);
			},
		},
		{ 
			title: '操作', 
			dataIndex: 'serviceType',
			key: 'serviceType',
			align: 'center',
			render: (text, record, index) => {
				const { serviceName, serviceType } = record;
				const { selectedRowKeys } = this.state;
				const isDisable = selectedRowKeys.some(item => item === serviceType);
				const sgGift = this.state.snList.filter(item => item === serviceType );
				return ( 
					<Popconfirm
						title={`确定赠送<${serviceName}>服务？`}
						onText="赠送"
						cancelText="取消"
						onConfirm={ () => {
							this.setState({ snList: [...sgGift] }, () => {
								this.giveGifts();
							});
						}}
					> 
						<Button size="small" type="primary" disabled={!isDisable}>赠送服务</Button> 
					</Popconfirm> 
				); 
			} 
		}, 
	];

	getCount = (num, total) => {
		let i = 0;
		total.map(item => {
			if(item === num) {
				i += 1;
			}
		})
		return i;
	}

	numberChange = (serviceType, value) => {
		let isPlus = true;	//判断是加号操作还是减号操作
		let arr = this.state.snList;

		if (value < this.getCount(serviceType, arr)) {
			isPlus = false;
		}
		if (isPlus) {
			if (this.state.snList.length === this.state.givenCount) {
				message.warning(`该等级用户只能拥有${this.state.givenCount}项赠送服务`);
				return false;
			};
			let count = [];
			for(let i = 0; i < value; i += 1) {
				count = [...count, serviceType];
			}
			arr = [...arr, serviceType];
		} else {
			const index = arr.indexOf(serviceType);
			arr.splice(index, 1);
		}
		this.setState({
			snList: [...arr],
		});
	}

	// 批量赠送增值服务
	giveGifts = () => {
		const { dispatch } = this.props;
		const { cPlyNo, cCertfCde, cCertfCls, snList, transactionCode } = this.state;
		if (snList.length) {
			dispatch({
				type: 'valueAdd/giveGifts',
				payload: {
					cPlyNo,
					cCertfCde,
					cCertfCls,
					serviceTypeList: snList.map( item => item.toString()),
					transactionCode,
				},
			}).then((data) => {
				if (data.code === 0) {
					message.success('赠送成功');
				} else {
					message.warning(data.message);
				}
				this.setState({
					snList: [],
					selectedRowKeys: [],
				});
			});
		} else {
			message.info('请至少选择一个赠送服务');
		}
	}

	
	render() {
		const { selectedRowKeys } = this.state;
		const rowSelection = {
			onChange: (selectedRowKeys, selectedRows) => {
				this.setState({
					snList: selectedRows.map(item => item.serviceType),
					selectedRowKeys,
				});
				// console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
			},
			selectedRowKeys,
			getCheckboxProps: record => {
				return {
					disabled: (!this.state.snList.some(item => item === record.serviceType) && this.state.snList.length === this.state.givenCount),
					serviceType: record.serviceType,
				}
			},
		};
		const { loading, valueAdd: { serviceDetailsQueryVipRank: { ranKName, vipRankList=[] } } } = this.props;
		let rankClass = 'blue';
		switch (ranKName) {
			case '普通客户': rankClass = 'blue'; break;
			case '铂金VIP': rankClass = 'orange'; break;
			case '黄金VIP': rankClass = 'gold'; break;
			case '钻石VIP': rankClass = 'purple'; break;
			default: rankClass = 'blue'; break;
		}
    return (
				<Card>
					<Tag color={rankClass}>客户等级：{ranKName}</Tag>
					<Button type="primary" size="small" loading={loading['valueAdd/giveGifts']} onClick={this.giveGifts}>批量赠送</Button>
					<Table
						dataSource={vipRankList}
						loading={loading['valueAdd/ServiceDetails']}
						bordered
						showHeader={false}
						rowSelection={rowSelection}
						size="small"
						pagination={false}
						columns={this.columns2}
						rowKey= { item => item.serviceType }
						title={() => '可赠送的增值服务'}
						style={{
							marginTop: '10px',
						}}
					/>
				</Card>
    )
  }
}
export default connect(({ valueAdd, loading }) => ({
	valueAdd,
  loading: loading.effects,
}))(Form.create()(CustomRank))