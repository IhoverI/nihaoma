import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import { getDataSource,getSelectChild } from '../../utils/utils'

const { RangePicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;

const columns = [
  {
    title: '保单号',
    dataIndex: 'policyNo',
  },
  {
    title: '赔案号',
    dataIndex: 'claimNo',
  },
  {
    title: '被保险人',
    dataIndex: 'insuredName',
  },  {
    title: '出险人',
    dataIndex: 'reportorName',
  },{
    title: '出险地点',
    dataIndex: 'damageAddress',
  },{
    title: '出险原因',
    dataIndex: 'damageName',
  }, {
    title: '出险时间',
    dataIndex: 'damageStartdate',
  },{
    title: '立案金额',
    dataIndex: 'sumEstloss',
  },{
    title: '结案时间',
    dataIndex: 'endCaseDate',
  },{
    title: '赔付次数',
    dataIndex: 'paidTimes',
  },{
    title: '赔案状态',
    dataIndex: 'nodeName',
  },
]
  

@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.models.searchlist,
}))
@Form.create()
export default class ClaimForm extends PureComponent {
  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
  };

  handleSearch = e => {
    e.preventDefault();
    this.searchData();
  };

  isEmpty(val){
    return val===undefined||val===null||val==='';
  }

  searchData = () => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(this.isEmpty(fieldsValue.certfCode)&&this.isEmpty(fieldsValue.cPlyNo)){
        message.warn("投保单号 和 客户证件号码 必须填写其中一项");
        return;
      }
      if(fieldsValue.certfCode===''){
        delete fieldsValue.certfCode;
      }
      if(fieldsValue.cPlyNo===''){
        delete fieldsValue.cPlyNo;
      }
      dispatch({
        type: 'searchlist/queryClaimInfo',
        payload: fieldsValue,
      }).then(()=>{});
    });
  }

  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 16 },
      },
    };
    return (
      <div>
        <Form className='' onSubmit={this.handleSearch}>
           <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
             <Col md={8} sm={24}>
                <FormItem {...formItemLayout} label="保单号">
                  {
                    getFieldDecorator('cPlyNo',{rules: [{ max: 30, message: '请输入正确的保单号' }]})(
                    <Input placeholder="请输入" style={{ width: '100%' }}/>)
                  }
                 </FormItem>
              </Col>
              <Col md={8} sm={24}>
                <FormItem {...formItemLayout} label="客户证件类型">
                  {getFieldDecorator('certfType')(
                     <Select allowClear placeholder="请选择" style={{ width: '100%' }}>
                       {getSelectChild("certiDesc")}
                     </Select>
                    )}
                </FormItem>
              </Col>
               <Col md={8} sm={24}>
                <FormItem {...formItemLayout} label="客户证件号码">
                  {
                    getFieldDecorator('certfCode')(
                    <Input placeholder="请输入" style={{ width: '100%' }}/>)
                  }
                 </FormItem>
              </Col>
          </Row> 

          <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
          </div>
        </Form>
      </div>
    );
  }

  render() {
    const { searchlist:{ClaimInfo}, loading } = this.props;
    const data =ClaimInfo|| [];
    const rowKey = (record, index) => {
      return index
    }
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
            <div>{this.renderAdvancedForm()}</div>
            <Table
              loading={loading}
              scroll={{x: 1900}}
              rowKey={rowKey}
              columns={columns}
              dataSource={data}
              bordered
              size="small"
              pagination={null}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
