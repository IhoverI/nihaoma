import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Input,
  Select,
  Button,
  message,
  Table,
  Form,
  DatePicker,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ChannelAndProduction from '../../components/ChannelAndProduction';
import { getDataSource,getSelectChild } from '../../utils/utils'

const { Option } = Select;

const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
    render: (text, record, index) => index + 1,
  },
  {
    title: '服务名称',
    dataIndex: 'serviceName',
    align: 'center',
  },
  {
    title: '服务状态',
    dataIndex: 'status',
    align: 'center',
  },
  {
    title: '当前是否可用',
    dataIndex: 'canUseFlag',
    align: 'center',
  },
  {
    title: '首次赠送日期',
    dataIndex: 'sendDate',
    align: 'center',
  },
  {
    title: '领取日期',
    dataIndex: 'receiveDate',
    align: 'center',
    render: val => <span>{ val ? moment(val).format('YYYY-MM-DD') : '' }</span>,
  },
  {
    title: '服务次数', // 基因检测-空/在线医生-客户提问次数/视频会员-原始订单号对应的该服务已成功充值次数
    dataIndex: 'serviceCount',
    align: 'center',
    // render: (text, record) => {
    //   const { serviceType, type } = record;
    //   if (parseInt(serviceType, 10) === 3 || parseInt(serviceType, 10) === 6) {
    //     return text;
    //   } else {
    //     return '-';
    //   }
    // }
  },
  {
    title: '客户姓名',
    dataIndex: 'clientName',
    align: 'center',
  },
  {
    title: '客户证件号',
    dataIndex: 'certfCode',
    align: 'center',
  },
  {
    title: '业务渠道代码',
    dataIndex: 'oldEnterpCode',
    align: 'center',
  },
  {
    title: '产品名称',
    dataIndex: 'prodName',
    align: 'center',
  },
  {
    title: '产品代码',
    dataIndex: 'prpdCode',
    align: 'center',
  },
  {
    title: '款别',
    dataIndex: 'cTgtFld1',
    align: 'center',
  },
  {
    title: '原始订单号',
    dataIndex: 'transactionCode',
    align: 'center',
  },
  {
    title: '新单保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '已续保次数', // 原始订单号对应的最新保单的续保次数
    dataIndex: 'renewalTime',
    align: 'center'
  },
  {
    title: '最新保单状态', // 原始订单号对应的最新保单的保单状态
    dataIndex: 'flag',
    align: 'center',
  },
  {
    title: '投保单导入日期',
    dataIndex: 'inputDate',
    align: 'center',
    render: val => <span>{ val ? moment(val).format('YYYY-MM-DD') : '' }</span>,
  },
  {
    title: '保险起期',
    dataIndex: 'insrncBeginDate',
    align: 'center',
    render: val => <span>{ val ? moment(val).format('YYYY-MM-DD') : '' }</span>,
  },
  {
    title: '首期扣款日期',
    dataIndex: 'chargeDate',
    align: 'center',
    render: val => <span>{ val ? moment(val).format('YYYY-MM-DD') : '' }</span>,
  },
  {
    title: '新单年化保费',
    dataIndex: 'prm',
    align: 'center',
  },
  {
    title: '订单已实缴保额',
    dataIndex: 'paySumPrm',
    align: 'center',
  },
  {
    title: '服务评分',
    dataIndex: 'serviceScore',
    align: 'center',
    render: text => text || '暂无评分',
  },
  {
    title: '服务评价',
    dataIndex: 'serviceComment',
    align: 'center',
    render: text => text || '暂无评价',
  },
];

@connect(({ valueAdd, loading }) => ({
  valueAdd,
  loading: loading.effects,
}))
@Form.create()
export default class newOrderData extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    searchValues: {},
    total: 0,
    servicesItem: [],
  };

  renderSelectOption = (data) => {
    return data.map(item => (
        <Option key={item.code} value={item.code}>{item.item}</Option>
    ));
  }

  handleFormReset = () => {
    this.props.form.resetFields();
  };
   
  handleSearch = e => {
    e.preventDefault();
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(1,10);
  };

  searchData = (pageNum,pageSize) => {
    const { dispatch } = this.props;
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { inputDate, insrncDate, receiveDate, sendDate } = fieldsValue;
      if((!inputDate || !inputDate.length)&&(!sendDate || !sendDate.length)&&(!insrncDate || !insrncDate.length)&&(!receiveDate || !receiveDate.length)){
        message.error('领取日期,赠送日期,投保单导入日期,保险起期四个条件至少选一', 5);
        return
      }
      fieldsValue = this.prepareValues(fieldsValue);    
      this.setState({
        searchValues: fieldsValue
      })
      dispatch({
        type: 'valueAdd/newValueAddData',
        payload: {
          ...fieldsValue,
          pageNum,
          pageSize,
        },
      }).then((result) => {
        const { total } = result;
        this.setState({
          total, 
        });
      });
    });
  }

  prepareValues(values){

    if(!values.insrncDate||values.insrncDate.length===0){
      values.insrncBeginDate = null;
      values.insrncEndDate = null;
    }else{
      values.insrncBeginDate = values.insrncDate[0].format("YYYY-MM-DD");
      values.insrncEndDate = values.insrncDate[1].format("YYYY-MM-DD");
    }
    delete values.insrncDate;

    if(!values.receiveDate||values.receiveDate.length===0){
      values.receiveBeginDate = null;
      values.receiveEndDate = null;
    }else{
      values.receiveBeginDate = values.receiveDate[0].format("YYYY-MM-DD");
      values.receiveEndDate = values.receiveDate[1].format("YYYY-MM-DD");
    }
    delete values.receiveDate;

    if(!values.inputDate||values.inputDate.length===0){
      values.inputBeginDate = null;
      values.inputEndDate = null;
    }else{
      values.inputBeginDate = values.inputDate[0].format("YYYY-MM-DD");
      values.inputEndDate = values.inputDate[1].format("YYYY-MM-DD");
    }
    delete values.inputDate;

    if(!values.sendDate||values.sendDate.length===0){
      values.sendBeginDate = null;
      values.sendEndDate = null;
    }else{
      values.sendBeginDate = values.sendDate[0].format("YYYY-MM-DD");
      values.sendEndDate = values.sendDate[1].format("YYYY-MM-DD");
    }
    delete values.sendDate;
    
    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  exportExcel = () => {
    let total = this.state.total;
    if (total === 0) {
      message.error('请搜索出结果后，再进行导出操作！');
      return;
    }
    let { dispatch } = this.props;
    let searchValues = this.state.searchValues;
    dispatch({
      type: 'valueAdd/exportNewValueAddData',
      payload: searchValues,
    }).then(() => {
      //
    }).catch(()=>{
      message.error('网络异常请稍后再试');
    });
  }
  
  // 列表状态发生改变
  handleTableChange =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }

  render() {
    const { loading, valueAdd: { newValueAddData }, form } = this.props;
    const { getFieldDecorator } = form;
    const { total } = this.state;
    let datalist = [];
    if(newValueAddData){
      datalist = newValueAddData.list;
    }
    const { RangePicker } = DatePicker;

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <Form className='custom-form-wrapper' layout="inline">
            <ChannelAndProduction
              oldEnterpCodeMultiple = {false}
              prodNoMultiple = {true}
              channelFieldKey="oldEnterpCode"
              productionFieldKey="prpdNoList"
              newOrderDataChannelData={true}
              ChannelRule={{rules: [{required: true, message: '业务渠道必填'}]}}
              {...this.props} allowClear
            />
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <Form.Item label="服务名称">
                    {getFieldDecorator('serviceTypeList', {
                      rules: [
                        { required: true, message: '服务名称必选' }
                      ]
                    })(
                      <Select mode="multiple" placeholder="请选择">
                        {this.renderSelectOption(getDataSource('valueAdd').newPolicy)}
                      </Select>
                    )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="服务状态">
                  {getFieldDecorator('status')(
                    <Select placeholder="请选择">
                      {this.renderSelectOption(getDataSource('valueAdd').ServiceStatus)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="当前是否可用">
                  {getFieldDecorator('canUseFlag')(
                    <Select placeholder="请选择状态">
                      <Option value="0">不可用</Option>
                      <Option value="1">可用</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="赠送日期">
                  {getFieldDecorator('sendDate')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="领用日期">
                  {getFieldDecorator('receiveDate')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="保险起期">
                  {getFieldDecorator('insrncDate')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="投保单导入日期">
                  {getFieldDecorator('inputDate')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
            <span style={{ marginBottom: 24 }}>
              <Button loading={loading['valueAdd/newValueAddData']} onClick={this.handleSearch} type="primary" >
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </div>
          <Table
            loading={loading['valueAdd/newValueAddData']}
            rowKey={record => record.serviceNo}
            dataSource={datalist}
            scroll={{x: 1400}}
            bordered
            title={()=>{
              return <Button loading={this.props.loading['valueAdd/exportNewValueAddData']} type='primary' onClick={this.exportExcel}>导出excel</Button>
            }}
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            columns={columns}
            onChange={this.handleTableChange}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}
