import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import request from '../../utils/request';
import {
  Row,
  Col,
  Card,
  Form,
  Select,
  Table,
  Button,
  DatePicker,
  Input,
  Upload,
  Icon,
  message,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from  './LogisticsManage.less'

const { RangePicker } = DatePicker;
const FormItem = Form.Item;

const columns = [
  {
    title: '序号',
    align: 'center',
    render: (text, record, index) => index + 1,
  },
  {
    title: '服务类别', // ----
    dataIndex: 'type',
    align: 'center',
  },
   {
    title: '服务名称',
    dataIndex: 'serviceName',
    align: 'center',
  },
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },

  {
    title: '业务渠道', 
    dataIndex: 'extEnterpName',
    align: 'center',
  },
  {
    title: '快递单号',
    dataIndex: 'expressNo',
    align: 'center',
  },
  {
    title: '快递公司',
    dataIndex: 'expressName',
    align: 'center',
  },
  {
    title: '下单时间', 
    dataIndex: 'orderDate',
    align: 'center',
  },
  {
    title: '签收时间', 
    dataIndex: 'receivedDate',
    align: 'center',
  },
  {
    title: '快递状态',
    dataIndex: 'sendStatus',
    align: 'center',
  },
  {
    title: '最新路由信息', 
    dataIndex: 'routerInfo',
    align: 'center',
  },
  {
    title: '收件人', 
    dataIndex: 'receivedName',
    align: 'center',
  },
  {
    title: '联系方式', // ---
    dataIndex: 'receivedPhone',
    align: 'center',
  },
  {
    title: '投递地址',
    dataIndex: 'receiverAddress',
    align: 'center',
  },
  {
    title: '检测盒编码',
    dataIndex: 'boxId',
    align: 'center',
  },
  {
    title: '寄送类型',
    dataIndex: 'postType',
    align: 'center',
  },
];

@connect(({ valueAdd, loading }) => ({
  valueAdd,
  loading: loading.effects,
}))
@Form.create()
export default class LogisticsManage extends PureComponent {
  state={
        packageLoading:false,
        offlineLoading:false,
  }
  
  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
  };

  handleDownload = () => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(!fieldsValue.cPlyNo&&!fieldsValue.orderDate){
        message.error('下单时间和保单号必选其一')
        return;
      }
      fieldsValue = this.perpareValues(fieldsValue);
        dispatch({
          type: 'valueAdd/exportExpressReport',
          payload:{
            ...fieldsValue,
          },
        })
    });
  }

  downloadModelPackage =()=>{
    let {dispatch} = this.props;
    dispatch({
      type: 'valueAdd/downLoadPackageTemplate',
    })
  }
  downloadModelOffline =()=>{
    let {dispatch} = this.props;
    dispatch({
      type: 'valueAdd/downLoadLineTemplate',
    })
  }
  downloadModelGene =()=>{
    let {dispatch} = this.props;
    dispatch({
      type: 'valueAdd/downLoadGeneBoxTemplate',
    })
  }
  handleImport = () => {
    this.props.dispatch({
        type: 'searchlist/FixedReportDownload',
        payload:{
          ...this.state.formValues,
        },
      }).then(()=>{
      });
  }

  handleSearch = e => {
    e.preventDefault();
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(1,10);
  };

  // 列表状态发生改变
  handleTableChange =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }


  searchData = (pageNum,pageSize) => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(!fieldsValue.cPlyNo&&!fieldsValue.orderDate){
        message.error('下单时间和保单号必选其一')
        return;
      }
      fieldsValue = this.perpareValues(fieldsValue);
        dispatch({
          type: 'valueAdd/queryExpressReport',
          payload:{
            ...fieldsValue,
            pageNum,
            pageSize
          },
        });
    });
  }

  perpareValues(values){
    if(values.orderDate&&values.orderDate.length>0){
      values.beginOrderDate=values.orderDate[0].format('YYYY-MM-DD');
      values.endOrderDate =values.orderDate[1].format('YYYY-MM-DD');
      delete values.orderDate;
    }
    return values
  }

  renderAdvancedForm() {
    const { loading, form } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="下单时间">
              {getFieldDecorator('orderDate')(
                <RangePicker style={{ width: '100%' }}  />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="保单号">
              {getFieldDecorator('cPlyNo')(
                <Input/>
              )}
            </FormItem>
          </Col>
        </Row>        
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
            <Button icon='download' loading={loading['valueAdd/exportExpressReport']} style={{marginLeft: 8}} onClick={this.handleDownload}>导出清单</Button>
        </div>
      </Form>
    );
  }

  render() {
    const { loading,  valueAdd: { expressReportData } } = this.props;
    const _this = this;
    const props1 = {
      name: 'file',
      showUploadList:false,
      action:`${SERVERLOCAL}ybyy-valueadded/valueAdded/importPackageInfo`,
      withCredentials: true,
      className:styles.uploadButton,
      disabled:this.state.packageLoading,
      onChange(info) {
        if (info.file.status == 'uploading') {
          console.log(info)
          _this.setState({packageLoading:true})
        }else{
          _this.setState({packageLoading:false})
          if (info.file.response == '') {
            message.success(`${info.file.name} 导入成功`);
          } else  {
            message.error(`${info.file.name}  导入失败`);
          }
        }
      },
    };
    const  props2 = {
      name: 'file',
      showUploadList:false,
      headers:{'Method':'POST'},
      className:styles.uploadButton,
      action: `${SERVERLOCAL}/ybyy-valueadded/valueAdded/importLineInfo`,
      withCredentials: true,
      disabled:this.state.offlineLoading,
      onChange(info) {
        console.log(info)
        if (info.file.status == 'uploading') {
          _this.setState({offlineLoading:true})
        }else{
          _this.setState({offlineLoading:false})
          if (info.file.response == '') {
            message.success(`${info.file.name} 导入成功`);
          } else  {
            message.error(`${info.file.name}  导入失败`);
          }
        }
      },
    };
    const  props3 = {
      name: 'file',
      showUploadList:false,
      headers:{'Method':'POST'},
      className:styles.uploadButton,
      action: `${SERVERLOCAL}/ybyy-valueadded/valueAdded/importGeneExpressInfo`,
      withCredentials: true,
      disabled:this.state.offlineLoading,
      onChange(info) {
        if (info.file.status == 'uploading') {
          _this.setState({offlineLoading:true})
        }else{
          _this.setState({offlineLoading:false})
          if (info.file.response.code == 0) {
            message.success(`${info.file.name} 导入成功`);
          } else  {
            message.error(`${info.file.name} 导入失败`);
          }
        }
      },
    };

    let datalist;
    let total;
    if(expressReportData){
      datalist = expressReportData.list || [];
      total = expressReportData.total || 0;
    }
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
            <div>{this.renderAdvancedForm()}</div>
            <Table
              loading={loading['valueAdd/queryExpressReport']}
              rowKey={record => record.expressNo}
              dataSource={datalist}
              scroll={{x:1500}}
              bordered
              size="small"
              pagination={{
                showQuickJumper: true,
                showSizeChanger: true,
                ...{
                    current:this.state.pageNum,
                    pageSize:this.state.pageSize,
                    showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                    total: total,
                    pageSizeOptions:['10','20','30','40','50'],
                },
              }}
              onChange={this.handleTableChange}
              title={() => <Fragment>
              <Button  icon='download'  loading={loading['valueAdd/downLoadPackageTemplate']} type='primary' 
              style={{marginLeft: 8}} onClick={this.downloadModelPackage}>打包寄送模板下载</Button>
              <Upload {...props1} >
                 <Button loading={this.state.packageLoading}>
                     <Icon type="upload" /> 打包寄送导入
                </Button>
              </Upload>
              <Button icon='download' loading={loading['valueAdd/downLoadLineTemplate']} type='primary' 
              style={{marginLeft: 8}} onClick={this.downloadModelOffline}>线下单导入模板下载</Button>
              <Upload {...props2} >
                 <Button loading={this.state.offlineLoading}>
                     <Icon type="upload" /> 线下单导入
                </Button>
              </Upload>
              <Button  icon='download'  loading={loading['valueAdd/downLoadGeneBoxTemplate']} type='primary' 
              style={{marginLeft: 8}} onClick={this.downloadModelGene}>检测盒编码模板下载</Button>
              <Upload {...props3} >
                 <Button loading={this.state.GeneLoading}>
                     <Icon type="upload" /> 检测盒编码导入
                </Button>
              </Upload>
              </Fragment>
            }
            columns={columns}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
