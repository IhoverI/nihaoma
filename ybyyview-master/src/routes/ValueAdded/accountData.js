import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Input,
  Select,
  Button,
  message,
  Table,
  Form,
  DatePicker,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ChannelAndProduction from '../../components/ChannelAndProduction';
import { getDataSource,getSelectChild } from '../../utils/utils'

const { Option } = Select;
const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
    render: (text, record, index) => index + 1,
  },
  {
    title: '服务类别',
    dataIndex: 'type',
    align: 'center',
  },
  {
    title: '服务名称',
    dataIndex: 'serviceName',
    align: 'center',
  },
  {
    title: '首次赠送日期',
    dataIndex: 'sendDate',
    align: 'center',
    render: text => text ? moment(text).format('YYYY-MM-DD hh:mm:ss') : '',
  },
  {
    title: '消费日期',
    dataIndex: 'rechargeDate',
    align: 'center',
    render: text => text ? moment(text).format('YYYY-MM-DD hh:mm:ss') : '',
  },
  {
    title: '业务渠道代码',
    dataIndex: 'oldEnterpCode',
    align: 'center',
  },
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '客户姓名',
    dataIndex: 'clientNo',
    align: 'center',
  },
  {
    title: '客户证件号',
    dataIndex: 'certfCode',
    align: 'center',
  },
  {
    title: '报告生成状态',
    dataIndex: 'reportStatus',
    align: 'center',
  },
];

@connect(({ valueAdd, loading }) => ({
  valueAdd,
  loading: loading.effects,
}))
@Form.create()
export default class accountData extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    searchValues: {},
    total: 0,
    servicesItem: [],
  };

  renderSelectOption = (data) => {
    return data.map(item => (
        <Option key={item.code} value={item.code}>{item.item}</Option>
    ));
  }

  handleFormReset = () => {
    this.props.form.resetFields();
  };

  handleSearch = e => {
    e.preventDefault();
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(1, 10);
  };
   
  searchData = (pageNum, pageSize) => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue = this.prepareValues(fieldsValue);
      this.setState({
        searchValues: fieldsValue
      })
      dispatch({
        type: 'valueAdd/getOrderMsg',
        payload: {
          ...fieldsValue,
          serviceType: parseInt(fieldsValue.serviceType),
          pageNum,
          pageSize,
        },
      }).then((result) => {
        let { total } = result;
        if (total) message.success('查询成功');
        this.setState({
          total,
        });
      }).catch(()=>{
        message.error('网络异常请稍后再试');
      });
    });
  }

  prepareValues(values){

    if(!values.rechargeDate||values.rechargeDate.length===0){
      values.rechargeDateStart = null;
      values.rechargeDateEnd = null;
    }else{
      values.rechargeDateStart = values.rechargeDate[0].format("YYYY-MM-DD");
      values.rechargeDateEnd = values.rechargeDate[1].format("YYYY-MM-DD");
    }
    delete values.rechargeDate;

    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  exportExcel = () => {
    let total = this.state.total;
    if (total === 0) {
      message.error('请搜索出结果后，再进行导出操作！');
      return;
    }
    let { dispatch } = this.props;
    let searchValues = this.state.searchValues;
    // console.log(searchValues)
    dispatch({
      type: 'valueAdd/exportOrderMsg',
      payload: searchValues,
    }).then(() => {
      //
    }).catch(()=>{
      message.error('网络异常请稍后再试');
    });
  }
  
  // 列表状态发生改变
  handleTableChange =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }
  render() {
    const { loading, valueAdd: { OrderMsgDataList }, form } = this.props;
    const { getFieldDecorator } = form;
    let datalist = [];
    let total;
    if(OrderMsgDataList){
      if(OrderMsgDataList){
        datalist = OrderMsgDataList.list;
        total = OrderMsgDataList.total
      }
    }
    const { RangePicker } = DatePicker;

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <Form className='custom-form-wrapper' layout="inline">
            <ChannelAndProduction
                oldEnterpCodeMultiple = {true}
                prodNoMultiple = {true}
                channelFieldKey="oldEnterpCodes"
                productionFieldKey="prodNo"
                // ChannelRule={{rules: [{required: true, message: '业务渠道必填'}]}}
                {...this.props} allowClear
              />
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <Form.Item label="服务类别">
                  {getFieldDecorator('type')(
                    <Select placeholder="请选择">
                      {this.renderSelectOption(getDataSource('valueAdd').serviceType)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="服务名称">
                    {getFieldDecorator('serviceType', {
                      rules: [
                        { required: true, message: '请选择消费日期' }
                      ],
                    })(
                      <Select placeholder="请选择">
                        {this.renderSelectOption(getDataSource('valueAdd').valueServiceItem)}
                      </Select>
                    )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="消费日期">
                  {getFieldDecorator('rechargeDate', {
                    rules: [
                      { required: true, message: '请选择消费日期' }
                    ]
                  })(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
            <span style={{ marginBottom: 24 }}>
              <Button loading={loading['valueAdd/valueAddedData']} onClick={this.handleSearch} type="primary" >
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </div>
          <Table
            loading={loading['valueAdd/getOrderMsg']}
            rowKey={record => record.sporderId}
            dataSource={datalist}
            // scroll={{x: 1000}}
            bordered
            title={()=>{
              return <Button loading={this.props.loading['valueAdd/exportValueAddedService']} type='primary' onClick={this.exportExcel}>导出excel</Button>
            }}
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            columns={columns}
            onChange={this.handleTableChange}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}
