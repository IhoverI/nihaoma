import React, { PureComponent, Fragment}  from 'react';
import { connect } from 'dva';
import {
  Form,
  Table,
  Row,
  Col,
  Button,
  Select,
  DatePicker,
  Card,
} from 'snk-web';
import moment from 'moment';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getSelectChild, getDataSource  } from '../../utils/utils';

const { MonthPicker } = DatePicker;

const datalist = [
  {
    cPlyNo: '278143322',
    batchNumber: '12',
    valueAddedItem: 'geneticTest',
    cdKey: '1233',
    perPrice: '199',
    perchaseDate: 1551766488333,
    expirationDate: 1551966488333,
    getDate: 1551866488333,
    customer: 'fannie',
    relationTel: 15273180809,
    usedTime: 1551946488333,
    status: 'offline',
  },
  {
    cPlyNo: '278122',
    batchNumber: '432',
    valueAddedItem: 'geneticTest',
    cdKey: '1233',
    perPrice: '199',
    perchaseDate: 1551766488333,
    expirationDate: 1551966488333,
    getDate: 1551866488333,
    customer: 'fannie',
    relationTel: 15273180809,
    usedTime: 1551946488333,
    status: 'offline',
  },
]
@connect(({ reportModels, loading }) => ({
  loading: loading.models.reportModels,
}))
@Form.create()
export default class InventoryDetail extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    searchValues: {},
    total: 0,
  };

  Columns = [
    {
      title: '序号',
      dataIndex: 'index',
      align: 'center',
      render: (text, record, index) => index + 1,
    },
    {
      title: '批次号',
      dataIndex: 'batchNumber',
      align: 'center',
    },
    {
      title: '增值服务名称',
      dataIndex: 'valueAddedItem',
      align: 'center',
      render: text => getDataSource('valueAdd').valueAddItem[text],
    },
    {
      title: '兑换码',
      dataIndex: 'cdKey',
      align: 'center',
    },
    {
      title: '单价',
      dataIndex: 'perPrice',
      align: 'center',
    },
    {
      title: '生效日期',
      dataIndex: 'perchaseDate',
      align: 'center',
      render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
    },
    {
      title: '失效日期',
      dataIndex: 'expirationDate',
      align: 'center',
      render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
    },
    {
      title: '领取日期',
      dataIndex: 'getDate',
      align: 'center',
      render: val => <span>{moment(val).format('YYYY-MM-DD')}</span>,
    },
    {
      title: '领取客户',
      dataIndex: 'customer',
      align: 'center',
    },
    {
      title: '客户联系电话',
      dataIndex: 'relationTel',
      align: 'center',
    },
    {
      title: '使用时间',
      dataIndex: 'usedTime',
      align: 'center',
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      render: text => getDataSource('valueAdd').status[text],
    },
  ]

  render() {
    const { form, loading } = this.props;
    const { getFieldDecorator } = form;
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
        <Form className='custom-form-wrapper' layout="inline">
            <Row gutter={16}>
              <Col md={12} sm={24}>
                <Form.Item label="保单年份">
                  {getFieldDecorator('pyear')(
                    <Select allowClear placeholder="请选择">
                      {getSelectChild('pyear')}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item  label="考核月份: ">
                  {getFieldDecorator(
                    'calcMonth',{
                    rules: [{required: true, message: `请输入考核月份`,}]
                    })
                    (<MonthPicker  format={"YYYYMM"} />)
                  }
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
            <span style={{ marginBottom: 24 }}>
              <Button loading={loading} onClick={this.handleSearch} type="primary" >
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </div>
          <Table
            loading={loading}
            rowKey={record => record.cPlyNo}
            dataSource={datalist}
            scroll={{x: 1400}}
            bordered
            title={()=>{
              return <Button loading={loading} type='primary' onClick={this.exportExcel}>导出excel</Button>
            }}
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:this.state.total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            columns={this.Columns}
            onChange={this.handleTableChange}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}