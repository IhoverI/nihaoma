import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Input,
  Select,
  Button,
  message,
  Table,
  Form,
  DatePicker,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ChannelAndProduction from '../../components/ChannelAndProduction';
import { getDataSource,getSelectChild } from '../../utils/utils'

const { Option } = Select;

const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    key: 'index',
    align: 'center',
    render: (text, record, index) => index + 1,
  },
  {
    title: '保全等级',
    dataIndex: 'rankName',
    key: 'rankName',
    align: 'center',
  },
  {
    title: '服务名称',
    dataIndex: 'serviceName',
    key: 'serviceName',
    align: 'center',
  },
  {
    title: '服务状态',
    dataIndex: 'status',
    key: 'status',
    align: 'center',
  },
  {
    title: '当前是否可用',
    dataIndex: 'canUseFlag',
    key: 'canUseFlag',
    align: 'center',
  },
  {
    title: '首次赠送日期',
    dataIndex: 'sendDate',
    align: 'center',
  },
  {
    title: '领取日期',
    dataIndex: 'receiveDate',
    key: 'receiveDate',
    align: 'center',
    render: val => <span>{ val ? moment(val).format('YYYY-MM-DD') : '' }</span>,
  },
  {
    title: '保全日期',
    dataIndex: 'saveDate',
    key: 'saveDate',
    align: 'center',
    render: val => <span>{ val ? moment(val).format('YYYY-MM-DD') : '' }</span>,
  },
  {
    title: '客户姓名',
    dataIndex: 'clientName',
    align: 'center',
  },
  {
    title: '客户证件号',
    dataIndex: 'certfCode',
    align: 'center',
  },
  // {
  //   title: '服务次数', // 基因检测-空/在线医生-客户提问次数/视频会员-原始订单号对应的该服务已成功充值次数
  //   dataIndex: 'serviceCount',
  //   key: 'serviceCount',
  //   align: 'center',
  //   render: (text, record) => {
  //     const { serviceType, type } = record;
  //     if (parseInt(serviceType, 10) === 3 || parseInt(serviceType, 10) === 6) {
  //       return text;
  //     } else {
  //       return '-';
  //     }
  //   }
  // },
  {
    title: '业务渠道代码',
    dataIndex: 'oldEnterpCode',
    key: 'oldEnterpCode',
    align: 'center',
  },
  {
    title: '产品名称',
    dataIndex: 'prodName',
    key: 'prodName',
    align: 'center',
  },
  {
    title: '产品代码',
    dataIndex: 'prodNo',
    key: 'prodNo',
    key: 'prodNo',
    align: 'center',
  },
  {
    title: '款别',
    dataIndex: 'cTgtFld1',
    key: 'cTgtFld1',
    align: 'center',
  },
  {
    title: '原始订单号',
    dataIndex: 'transactionCode',
    key: 'transactionCode',
    align: 'center',
  },
  {
    title: '保全保单号',
    dataIndex: 'cPlyNo',
    key: 'cPlyNo',
    align: 'center',
  },
  {
    title: '续保次数', // 客服点击赠送的保单的续保次数
    dataIndex: 'renewalTime',
    key: 'renewalTime',
    align: 'center'
  },
  {
    title: '保单状态', // 客服点击赠送的保单的续保次数
    dataIndex: 'flag',
    key: 'flag',
    align: 'center',
  },
  {
    title: '年化保费',
    dataIndex: 'prm',
    key: 'prm',
    align: 'center',
  },
  {
    title: '成功扣款期次',
    dataIndex: 'payCount',
    key: 'payCount',
    align: 'center',
  },
  {
    title: '服务评分',
    dataIndex: 'serviceScore',
    key: 'serviceScore',
    align: 'center',
    render: text => text || '暂无评分',
  },
  {
    title: '服务评价',
    dataIndex: 'serviceComment',
    key: 'serviceComment',
    align: 'center',
    render: text => text || '无评价',
  },
];

@connect(({ valueAdd, loading }) => ({
  valueAdd,
  loading: loading.effects,
}))
@Form.create()
export default class securityPolicyData extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    searchValues: {},
    total: 0,
    servicesItem: [],
  };

  renderSelectOption = (data) => {
    return data.map(item => (
        <Option key={item.code} value={item.code}>{item.item}</Option>
    ));
  }

  handleFormReset = () => {
    this.props.form.resetFields();
    this.setState({
      showArea: false,
    });
  };

  handleSearch = e => {
    e.preventDefault();
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(1,10);
  };
   
  searchData = (pageNum, pageSize) => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { saveData, receiveDate } = fieldsValue;
      // if((!saveData || !saveData.length)&&(!receiveDate || !receiveDate.length)){
      //   message.error('领取日期,投保单导入日期,保险起期三个条件至少选一')
      //   return
      // }
      fieldsValue = this.prepareValues(fieldsValue);
      this.setState({
        searchValues: fieldsValue,
      });
      dispatch({
        type: 'valueAdd/securityPolicy',
        payload: {
          ...fieldsValue,
          pageNum,
          pageSize,
        },
      }).then((result) => {
        const { total } = result;
        this.setState({
          total, 
        });
      });
    });
  }

  prepareValues(values){

    if(!values.receiveDate	||values.receiveDate.length===0){
      values.receiveBeginDate	 = null;
      values.receiveEndDate = null;
    }else{
      values.receiveBeginDate	 = values.receiveDate[0].format("YYYY-MM-DD");
      values.receiveEndDate = values.receiveDate[1].format("YYYY-MM-DD");
    }
    delete values.receiveDate;

    if(!values.saveData||values.saveData.length===0){
      values.saveBeginDate = null;
      values.saveEndDate = null;
    }else{
      values.saveBeginDate = values.saveData[0].format("YYYY-MM-DD");
      values.saveEndDate = values.saveData[1].format("YYYY-MM-DD");
    }
    delete values.saveData;

    if(!values.sendDate||values.sendDate.length===0){
      values.sendBeginDate = null;
      values.sendEndDate = null;
    }else{
      values.sendBeginDate = values.sendDate[0].format("YYYY-MM-DD");
      values.sendEndDate = values.sendDate[1].format("YYYY-MM-DD");
    }
    delete values.sendDate;

    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  exportExcel = () => {
    let total = this.state.total;
    if (total === 0) {
      message.error('请搜索出结果后，再进行导出操作！');
      return;
    }
    let { dispatch } = this.props;
    let searchValues = this.state.searchValues;
    dispatch({
      type: 'valueAdd/exportSecurityPolicy',
      payload: searchValues,
    }).then(() => {
      //
    }).catch(()=>{
      message.error('网络异常请稍后再试');
    });
  }
  
  // 列表状态发生改变
  handleTableChange =(pagination, filters, sorter) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }

  render() {
    const { loading, valueAdd: { securityPolicyData }, form } = this.props;
    const { getFieldDecorator } = form;
    let datalist = [];
    let total;
    if(securityPolicyData){
      datalist = securityPolicyData.list;
      total = securityPolicyData.total;
    }
    const { RangePicker } = DatePicker;

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <Form className='custom-form-wrapper' layout="inline">
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              {/* <Col md={12} sm={24}>
                <Form.Item label="服务名称">
                    {getFieldDecorator('serviceTypeList')(
                      <Select mode="multiple" placeholder="请选择">
                        {this.renderSelectOption(getDataSource('valueAdd').securityPolicy)}
                      </Select>
                    )}
                </Form.Item>
              </Col> */}
              <Col md={12} sm={24}>
                <Form.Item label="服务状态">
                  {getFieldDecorator('status')(
                    <Select placeholder="请选择">
                      {this.renderSelectOption(getDataSource('valueAdd').ServiceStatus)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="当前是否可用">
                  {getFieldDecorator('canUseFlag')(
                    <Select placeholder="请选择状态">
                      <Option value="0">不可用</Option>
                      <Option value="1">可用</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="领取日期">
                  {getFieldDecorator('receiveDate')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="首次赠送日期">
                  {getFieldDecorator('sendDate')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="保全等级">
                  {getFieldDecorator('rankNameList', {
                    rules: [
                      { required: true, message: '请选择保全等级' }
                    ]
                  })(
                    <Select mode="multiple" placeholder="请选择">
                      {
                        getDataSource('valueAdd').securityPlyState.map(item => (
                          <Option key={item.code} value={item.item}>{item.item}</Option>
                        ))
                      }
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="保全日期">
                  {getFieldDecorator('saveData', {
                    rules: [
                      { required: true, message: '请选择保全日期' }
                    ]
                  })(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <ChannelAndProduction
              oldEnterpCodeMultiple = {true}
              prodNoMultiple = {true}
              channelFieldKey="oldEnterpCodeList"
              productionFieldKey="prodNo"
              // ChannelRule={{rules: [{required: true, message: '业务渠道必填'}]}}
              {...this.props} allowClear
            />
          </Form>
          <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
            <span style={{ marginBottom: 24 }}>
              <Button loading={loading['valueAdd/securityPolicy']} onClick={this.handleSearch} type="primary" >
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </div>
          <Table
            loading={loading['valueAdd/securityPolicy']}
            rowKey={record => record.serviceNo}
            dataSource={datalist}
            scroll={{x: 1400}}
            bordered
            title={()=>{
              return <Button loading={this.props.loading['valueAdd/exportSecurityPolicy']} type='primary' onClick={this.exportExcel}>导出excel</Button>
            }}
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            columns={columns}
            onChange={this.handleTableChange}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}
