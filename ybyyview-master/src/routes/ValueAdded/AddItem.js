import React from 'react';
import {
  Input, Form, Select, Modal, Col, Row,message, InputNumber
} from 'snk-web';
import {
	getDataSource,
} from '../../utils/utils';
import styles from './addInventory.less';

const { Option } = Select;
@Form.create()
export default class AddItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			servicesItem: [],
			title: props.title,
			visible: props.visible,
			defaultFormData: props.defaultFormData,
			isUpdata: Object.keys(props.defaultFormData).length ? true : false, // 为true时是更新，false是新增
		}
	}

	componentDidMount(){
	}

	handleOk = (e) => {
		this.props.form.validateFields((err, fieldsValue) => {
			const { dispatch, defaultFormData: { serviceType } } = this.props;
      const { isUpdata } = this.state;
      if (!err) {
        const payload = {
          ...fieldsValue,
					// informType: fieldsValue.informType.join(','),
					attribute: parseInt(fieldsValue.attribute),
					flag: parseInt(fieldsValue.flag),
          serviceType,
        };
				if (isUpdata) {
					dispatch({
						type: 'valueAdd/updateConf',
						payload,
          }).then(response=> {
            if(response.code==0){
              message.success('success')
              dispatch({
                type: 'valueAdd/confQuery',
              })
            }else{
              message.error(response.message)
            }
          })
				} else {
					dispatch({
						type: 'valueAdd/addConf',
						payload: {
							...fieldsValue,
							attribute: parseInt(fieldsValue.attribute),
              value: parseInt(fieldsValue.value),
              serviceType,
							// informType: fieldsValue.informType.join(','),
						},
					}).then(response=> {
            if(response.code==0){
              message.success('success')
              dispatch({
                type: 'valueAdd/confQuery',
              })
            }else{
              message.error(response.message)
            }
          })
        }
        this.props.onChange(false);
			}
    });
	
  }

  handleCancel = (e) => {
		this.props.onChange(false);
	}

  renderSelectOption = (data) => {
    return data.map(item => (
       <Option key={item.code} value={item.code}>{item.item}</Option>
    ));
  }

	render() {
		const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
		};
		const { getFieldDecorator } = this.props.form;
		const { defaultFormData } = this.props;
		return (
			<Modal
				maskClosable={false}
        title={this.state.title}
        visible={this.state.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
      	<Form className={styles.customaddwrapper2} layout="inline">
					<Row>
						<Col span={12}>
							<Form.Item label="增值服务名称" {...formItemLayout}>
								{getFieldDecorator('serviceName', {
									rules: [{
										type: 'string', message: '增值服务名称不能为空', required: true,
									}],
									initialValue: defaultFormData.serviceName,
								})(
                  <Input/>
									// <Select>
									// 	{this.renderSelectOption(this.state.servicesItem)}
									// </Select>
								)}
							</Form.Item>
						</Col>
						<Col span={12}>
							<Form.Item label="厂商名称" {...formItemLayout}>
								{getFieldDecorator('provider', {
									rules: [{
										type: 'string', required: true, message: '请输入厂商名称'
									}],
									initialValue: defaultFormData.provider,
								})(
									<Input />
								)}
							</Form.Item>
						</Col>
            </Row>
            <Row>
						<Col span={12}>
              <Form.Item label="价值/份" {...formItemLayout}>
                {getFieldDecorator('value', {
                  rules: [{
                     required: true, message: '请输入增值服务项目价值'
                  }],
                  initialValue: defaultFormData.value,
                })(
                  <InputNumber min={0.01}  precision={2} />
                )}
              </Form.Item>
						</Col>
						<Col span={12}>
              <Form.Item
                {...formItemLayout}
                label="商品属性"
              >
                {getFieldDecorator('attribute', {
                  		rules: [{
                        required: true, message: '商品属性不能为空'
                      }],
                  initialValue: defaultFormData.attribute
                })(
                  <Select style={{width:'100%'}}>
                    {this.renderSelectOption(getDataSource('valueAdd').attribute)}
                  </Select>
                )}
              </Form.Item>
						</Col>
            </Row>
            <Row>
						<Col span={12}>
              <Form.Item
                {...formItemLayout}
                label="状态"
              >
                {getFieldDecorator('flag', {
                    rules: [{
                       required: true, message: '请输入增值服务状态'
                    }],
                  initialValue: defaultFormData.flag
                })(
                  <Select>
                    {this.renderSelectOption(getDataSource('valueAdd').status)}
                  </Select>
                )}
              </Form.Item>
						</Col>
            {/* <Col span={12} >
              <Form.Item label="服务平均分" {...formItemLayout}>
                {getFieldDecorator('average', {
                  rules: [{type: 'number', required: true, message: '请写入服务平均分'}],
                  initialValue: defaultFormData.average,
                })(
                  <InputNumber style={{width:'100%'}} />
                )}
              </Form.Item>
            </Col> */}
						{/* <Col span={24}>
              <Form.Item
                {...formItemLayout}
                label="通知方式"
              >
                {getFieldDecorator('informType', {
                    rules: [{
                      required: true, message: '请输入增值服务状态'
                   }],
                  initialValue: defaultFormData.informType ? defaultFormData.informType : [],
                })(
                  <Select	mode="multiple">
                    {this.renderSelectOption(getDataSource('valueAdd').informType)}
                  </Select>
                )}
              </Form.Item>
            </Col> */}
             </Row>
            <Row>
            <Col span={24} className="textAreaEdit">
              <Form.Item label="领取规则" {...formItemLayout}>
                {getFieldDecorator('getRule', {
                  rules: [{ required: true, message: '请写入领取规则'}],
                  initialValue: defaultFormData.getRule,
                })(
                  <Input.TextArea  style={{marginBottom: '5px'}}/>
                )}
              </Form.Item>
            </Col>
            </Row>
            <Row>
            <Col span={24} >
              <Form.Item label="详情描述" {...formItemLayout}  >
                {getFieldDecorator('describe', {
                  rules: [{type: 'string', required: true, message: '请写入增值服务详情描述'}],
                  initialValue: defaultFormData.describe,
                })(
                  <Input.TextArea />
                )}
              </Form.Item>
            </Col>
            </Row> 
				</Form>
      </Modal>  
		);
	}
}