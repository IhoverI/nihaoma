import React, { PureComponent, Fragment}  from 'react';
import { connect } from 'dva';
import {
  Select,
  Table,
  Form,
  Card,
  Row,
  Col,
  DatePicker,
  Button,
  Popconfirm,
  message,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource } from '../../utils/utils';
import styles from './valueAdd.less';

const { RangePicker } = DatePicker;
const { Option } = Select;
@connect(({ loading, valueAdd }) => ({
  valueAdd,
  loading: loading.effects,
}))
@Form.create()
export default class ValueAddedPrint extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    searchValues: {},
    total: 0,
    applyIdArr: [],
    printDisabled:true,
  };

  componentDidMount=()=>{
    this.props.dispatch({
      type: 'valueAdd/confQuery',
    })
  }
  exportExcel = () => {
    // 批量打印
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldValue) => {
      if (err) return;
      const payload = this.prepareValue(fieldValue);
      dispatch({
        type: 'valueAdd/placeExpressOrder',
        payload,
      }).then((response) => {
        if(response.code === 0) {
          message.success(response.message);
          this.handleSearch()
        }else{
          message.error(response.message)
        }
      })
    })
  }

  // 列表状态发生改变
  handleTableChange =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }

  handleSearch = e => {
    e.preventDefault();
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(1,10);
  };

  searchData = (pageNum,pageSize) => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const payload = this.prepareValue(fieldsValue);
      dispatch({
        type: 'valueAdd/queryPendingExpress',
        payload: {
          ...payload,
          pageNum,
          pageSize,
        },
      });
    })
  }

  prepareValue = (values) => {
    if (values.ctime) {
      values.ctimeBegin = values.ctime[0].format('YYYY-MM-DD');
      values.ctimeEnd = values.ctime[1].format('YYYY-MM-DD');
    }
    delete values.ctime;

    for( let key in values) {
      if (!values[key]) {
        delete values[key];
      }
    }
    // if(values.expressCode&&values.expressCode=='NULL')  delete values.X;
    return values;
  }

  handleFormReset = () => {
    this.props.form.resetFields();
  }

  // 导出所有查询结果
  exportList = () => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldValue) => {
      if (err) return;
      const payload = this.prepareValue(fieldValue);
      dispatch({
        type: 'valueAdd/exportExpressOrder',
        payload,
      })
    })
   
  }

  Columns = [
    {
      title: '序号',
      dataIndex: 'index',
      align: 'center',
      render: (text, record, index) => index + 1,
    },
    {
      title: '保单号',
      dataIndex: 'cPlyNo',
      align: 'center',
    },
    {
      title: '快递公司',
      dataIndex: 'expressName',
      align: 'center',
    },
    {
      title: '服务名称',
      dataIndex: 'serviceName',
      align: 'center',
    },
    {
      title: '收件人姓名',
      dataIndex: 'clientName',
      align: 'center',
    },
    {
      title: '联系电话',
      dataIndex: 'clientPhone',
      align: 'center',
    },
    {
      title: '寄送地址',
      dataIndex: 'postAddress',
      align: 'center',
      render: (text, record) => {
        const { clientProvince, clientCity, clientArea, clientAddress } = record;
        return `${clientProvince}${clientCity}${clientArea}${clientAddress}`;
      }
    },
    {
      title: '打印状态',
      dataIndex: 'printStatus',
      align: 'center',
      className: `${styles.printStatusTd}`,
      render: text => {
        let classN = 'printStatusIsOver';
        if (text === '0') {
          classN = 'printStatusWaiting';
        }
        if (text === '2') {
          classN = 'printStatusFail';
        }
        return (
          <span className={styles[classN]}>{getDataSource('valueAdd').printStatus[text].item}</span>
        )
      },
    },
    {
      title: '快递单号',
      dataIndex: 'expressNo',
      align: 'center',
    },
    // {
    //   title: '操作',
    //   dataIndex: 'operate',
    //   align: 'center',
    //   render: (text, record) => {
    //     return (
    //       <Popconfirm title="确认删除" onConfirm={this.exportList.bind(this, record.applyId)} okText="确认" cancelText="取消">
    //         <Button disabled={record.printStatus !== '0'} type='primary' size="small">删除</Button>
    //       </Popconfirm>
    //     )
    //   }
    // },
  ]

  // rowSelection =  {
  //   onChange: (selectedRowKeys, selectedRows) => {
  //     console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  //     this.setState({
  //       applyIdArr: selectedRowKeys,
  //     });
  //   },
  //   getCheckboxProps: record => ({
  //     disabled: record.printStatus !== '0', // Column configuration not to be checked
  //     cPlyNo: record.cPlyNo,
  //   }),
  // };

  // renderSelectOption = (data) => {
  //   return data.map(item => (
  //       <Option key={item.code} value={item.code}>{item.item}</Option>
  //   ));
  // }
  renderSelectOption = (data) => {
    return data.filter(e=>e.code?true:e.attribute==0).map(item => (
      item.serviceType?<Select.Option key={item.serviceType} value={item.serviceType}>{item.serviceName}</Select.Option>:
      <Select.Option key={item.code} value={item.code}>{item.item}</Select.Option>
    ));
  }
  onChangePrintStatus=(val)=>{
   let items =  getDataSource('valueAdd').printStatus.filter(e=>e.item=='未打印');
  let  printDisabled=val!==items[0].code;
   this.setState({printDisabled})
  }
  
  render() {
    const { form, loading, valueAdd: { expressPrintData }  } = this.props;
    let datalist;
    let total;
    if(expressPrintData){
      datalist = expressPrintData.list || [];
      total = expressPrintData.total || 0;
    }
    const { getFieldDecorator } = form;
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
        <Form className='custom-form-wrapper' layout="inline" onSubmit={this.handleSearch}>
            <Row gutter={16}>
              <Col md={12} sm={24}>
                <Form.Item label='服务名称'>
                  {getFieldDecorator('serviceType', {
                    rules: [
                      { required: true, message: '请选择服务名称'}
                    ]
                  })(
                    <Select>
                        {this.renderSelectOption(this.props.valueAdd.valueAddConfigList)}
                      {/* {this.renderSelectOption(getDataSource('valueAdd').expressType)} */}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label='领取时间'>
                  {getFieldDecorator('ctime')(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label='打印状态'>
                  {getFieldDecorator('printStatus', {
                      rules: [
                        { required: true, message: '请选择打印状态'}
                      ]
                    })(
                    <Select onChange={this.onChangePrintStatus}>
                      {this.renderSelectOption(getDataSource('valueAdd').printStatus)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              {/* <Col md={12} sm={24}>
                <Form.Item label='快递公司'>
                  {getFieldDecorator('expressCode', {
                      rules: [
                        { required: true, message: '请选择快递公司'}
                      ]
                    })(
                    <Select>
                      {this.renderSelectOption(getDataSource('valueAdd').expressCode)}
                    </Select>
                  )}
                </Form.Item>
              </Col> */}
            </Row>
            <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
              <span style={{ marginBottom: 24 }}>
                <Button htmlType="submit" type="primary" >
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </span>
            </div>
          </Form>
          <Table
            loading={loading['valueAdd/queryPendingExpress', 'valueAdd/confQuery']}
            rowKey={record => record.expressNo}
            dataSource={datalist}
            scroll={{x: 1400}}
            bordered
            title={()=>{
              return (
                <Fragment>
                  <Button disabled={this.state.printDisabled} loading={loading['valueAdd/placeExpressOrder']} type='primary' onClick={this.exportExcel}>批量打印</Button>
                  <Button loading={loading['valueAdd/exportExpressOrder']} type='primary' style={{ marginLeft: '5px' }} onClick={this.exportList.bind(this, '')}>导出清单</Button>
                </Fragment>
              )
            }}
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            columns={this.Columns}
            onChange={this.handleTableChange}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}