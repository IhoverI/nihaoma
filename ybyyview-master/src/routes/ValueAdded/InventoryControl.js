import React, { PureComponent, Fragment}  from 'react';
import { connect } from 'dva';
import {
  Form,
  Table,
  Row,
  Col,
  Select,
  DatePicker,
  Button,
  Card,
  message,
} from 'snk-web';
import moment from 'moment';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import AddInventory from './AddInventory';
import { getDataSource } from '../../utils/utils';

const { RangePicker } = DatePicker;


@connect(({ loading, valueAdd }) => ({
  valueAdd,
  loading: loading.effects,
}))
@Form.create()
export default class InvertoryControl extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    searchValues: {},
    total: 0,
    visible: false,
    defaultFormData: {},
    servicesItem:[],
  };
  componentDidMount=()=>{
    this.props.dispatch({
      type: 'valueAdd/confQuery',
    }).then(response=>{
      if (response.code === 0) {
				this.setState({
					servicesItem: response.data,
				})
			}
    })

  }
  
  Columns = [
    {
      title: '序号',
      dataIndex: 'index',
      align: 'center',
      render: (text, record, index) => index + 1,
    },
    // {
    //   title: '批次号',
    //   dataIndex: 'purchaseId',
    //   align: 'center',
    // },
    {
      title: '增值服务名称',
      dataIndex: 'serviceName',
      align: 'center',
    },
    {
      title: '厂商名称',
      dataIndex: 'provider',
      align: 'center',
    },
    {
      title: '采购日期',
      dataIndex: 'purchaseDate',
      align: 'center',
      render: val => <span>{val?moment(val).format('YYYY-MM-DD'):null}</span>,
    },
    // {
    //   title: '生效日期',
    //   dataIndex: 'validDate',
    //   align: 'center',
    //   render: val => <span>{val?moment(val).format('YYYY-MM-DD'):null}</span>,
    // },
    {
      title: '失效日期',
      dataIndex: 'invalidDate',
      align: 'center',
      render: val => <span>{val?moment(val).format('YYYY-MM-DD'):null}</span>,
    },
    {
      title: '可领取',
      dataIndex: 'remainNumber',
      align: 'center',
    },
    {
      title: '已领取',
      dataIndex: 'receivedNumber',
      align: 'center',
    },
   
    {
      title: '已失效',
      dataIndex: 'invalidNumber',
      align: 'center',
    },
    {
      title: '总数量',
      dataIndex: 'totalNumber',
      align: 'center',
    },
    {
      title: '单价',
      dataIndex: 'unitPrice',
      align: 'center',
    },
    {
      title: '小计',
      dataIndex: 'totalPrice',
      align: 'center',
    },
    // {
    //   title: '状态',
    //   dataIndex: 'status',
    //   align: 'center',
    // },
    {
      title: '操作',
      dataIndex: 'operate',
      align: 'center',
      render: (text, record) => {
        return (
          <Button size="small" onClick={this.toEdit.bind(this, record)} type='primary'>编辑</Button>
        )
      }
    }
  ]

  toEdit = (record) => {
    this.setState({
      modalTitle: '修改库存配置',
      defaultFormData: record,
      visible: true,
    })
  }

  // 用于承接子组件传来的值
  handleChange =(e,code) => {
    this.setState({
      visible: e,
    });
    if(code==0)this.handleSearch()
  }

  checkInventoryDetail = (data) => {
    // this.props.dispatch(routerRedux.push(`/valueadded/inventory-detail?serviceName=${getDataSource('valueAdd').valueAddItem[data]}`));
  }

  exportExcel = () => {
    // 导出数据
    let { dispatch, form } = this.props;
    form.validateFields((err, fieldValue) => {
      if (err) return;
      fieldValue = this.prepareValues(fieldValue);

      dispatch({
        type: 'valueAdd/exportStockInfo',
        payload: {
          ...fieldValue,
        },
      }).then(response=>{
        if(response.code!==0){
          message.error(response.message)
        }
      });
    })

  }

  handleSearch = e => {
    e.preventDefault();
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(1, 10);
  };

  searchData = (pageNum, pageSize) => {
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue = this.prepareValues(fieldsValue);
      dispatch({
        type: 'valueAdd/queryStockInfo',
        payload: {
          ...fieldsValue,
          pageNum,
          pageSize,
        },
      }).then((result) => {
        const { total, list } = result;
        this.setState({
          total,
        });
      });
    })
  }

  prepareValues(values){

    if(!values.stockDate||values.stockDate.length===0){
      values.bgnDate = null;
      values.endDate = null;
    }else{
      values.bgnDate = values.stockDate[0].format("YYYY-MM-DD");
      values.endDate = values.stockDate[1].format("YYYY-MM-DD");
    }
    delete values.stockDate;

    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  // 列表状态发生改变
  handleTableChange =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize);
    });
  }
  
  handleFormReset = () => {
    this.props.form.resetFields();
  }

  showModal = () => {
    this.setState({
      modalTitle: '添加库存配置',
      visible: true,
      defaultFormData: {},
    });
  }
  showTotal=(total, range) => {
    const { valueAdd: { stockList } } = this.props;
    let totoalNum = range[1]-range[0]+1;
     let  totalMoney = stockList.map(e=>e.totalPrice||0).reduce((sum,e)=> sum+e).toFixed(2)
    return ` 共${totoalNum}条，合计${totalMoney}元  `;
  }

  renderSelectOption = (data) => {
    return data.filter(e=>e.code?true:e.attribute==0).map(item => (
      item.serviceType?<Select.Option key={item.serviceType} value={item.serviceType}>{item.serviceName}</Select.Option>:
      <Select.Option key={item.code} value={item.code}>{item.item}</Select.Option>
    ));
  }

  render() {
    const { form, loading, dispatch, valueAdd: { stockList } } = this.props;
    const { getFieldDecorator } = form;
    const { defaultFormData, visible, modalTitle } = this.state;
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <Form className='custom-form-wrapper' layout="inline">
            <Row gutter={16}>
              <Col md={12} sm={24}>
                <Form.Item  label="采购日期">
                  {getFieldDecorator('stockDate', {
                    rules: [{
                      required: true, message: '请填入日期',
                    }],
                  })(
                    <RangePicker style={{ width: '100%' }}  />
                  )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item  label="服务名称">
                  {getFieldDecorator('serviceType',)
                  (
                    <Select allowClear={true}>
                    {this.renderSelectOption(this.props.valueAdd.valueAddConfigList)}
                       </Select>
                    )}
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item  label="状态">
                  {getFieldDecorator('status',)
                    (  <Select allowClear={true}>
                      {this.renderSelectOption(getDataSource('valueAdd').status)}
                         </Select>)
                  }
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
            <span style={{ marginBottom: 24 }}>
              <Button loading={loading['valueAdd/queryStockInfo']} onClick={this.handleSearch} type="primary" >
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button loading={loading['valueAdd/exportStockInfo']} style={{ marginLeft: 8 }} icon="download" onClick={this.exportExcel}>
                导出清单
              </Button>
            </span>
          </div>
          <Table
            loading={loading['valueAdd/queryStockInfo']}
            rowKey={record => record.purchaseId}
            dataSource={stockList}
            scroll={{x: 1400}}
            bordered
            title={()=>{
              return (
                <Fragment>
                 <Button  type="primary"  style={{marginTop: '10px'}} onClick={this.showModal.bind(this)}>添加库存配置</Button>
                </Fragment>
              )
            }}
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: this.showTotal,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:this.state.total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            columns={this.Columns}
            onChange={this.handleTableChange}
          />
        </Card>
        {
          visible ? (
            <AddInventory servicesItem={this.state.servicesItem} title={modalTitle} dispatch={dispatch} defaultFormData={defaultFormData} visible={this.state.visible} onChange={this.handleChange.bind(this)} />
          ) : null
        }
      </PageHeaderLayout>
    );
  }
}