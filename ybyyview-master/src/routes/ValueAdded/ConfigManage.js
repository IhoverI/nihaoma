import React, { PureComponent,Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Table, Input, Form, Select, Button, Card,Tooltip
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import AddItem from './AddItem';
import {
  getDataSource,
} from '../../utils/utils';

@connect(({ valueAdd, loading }) => ({
  valueAdd,
  loading: loading.models.valueAdd,
}))
@Form.create()
export default class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, defaultFormData: {} };
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'valueAdd/confQuery',
    });
  }

  columns = [
    {
      title: '序号',
      dataIndex: 'order',
      align: 'center',
      render: (text, record, index) => index + 1,
      width:50,
    },
    {
      title: '增值服务名称',
      dataIndex: 'serviceName',
      align: 'center', 
      width:100,       
    },
    {
      title: '厂商名称',
      dataIndex: 'provider',
      align:'center',
      width:150,
    },
    {
      title: '价值/份',
      dataIndex: 'value',
      align: 'center',   
      width:80,         
    },
    {
      title: '商品属性',
      dataIndex: 'attribute',
      align: 'center',        
      render: (text) => text === 0 ? '实物' : '虚拟' ,
      width:80,    
    },
    {
      title: '状态',
      dataIndex: 'flag',
      align: 'center',        
      render: (text) => text == 0 ? '已上架' : '已下架',
      width:80,    
    },
    {
      title: '通知方式',
      dataIndex: 'informType',
      align: 'center',
      render: (text) => {
        return text?getDataSource('valueAdd').informType.filter(e=>text.split(',').indexOf(e.code)>-1).map(e=>e.item).join():null;
      },
      width:80,    
    },
    {
      title: '领取规则',
      dataIndex: 'getRule',
      align: 'center',
      width: 100, 
      render: (text, record, index) => text && text.length > 15 ? (
        <Tooltip title={text}>
          <span>{text.substr(0,15)+'...'}</span>
        </Tooltip>
      ) : text,
    },
    {
      title: '增值服务详情描述',
      dataIndex: 'describe',
      align: 'center',  
      width:200,
      render: (text, record, index) => text && text.length > 15 ? (
        <Tooltip title={text}>
          <span>{text.substr(0,15)+'...'}</span>
        </Tooltip>
      ) : text,
    },
    {
      title: '服务平均分',
      dataIndex: 'average',
      align: 'center',    
      width:80,        
    },
    {
      title: '操作',
      dataIndex: 'operation',
      align: 'center',
      width:80,    
      render: (text, record) => {
        return (
          <div>
            <Button size="small" type="primary" onClick={this.upDataModal.bind(this, record)}>修改</Button>
          </div>
        );
      },
    },
  ];

  upDataModal = (record) => {
    const { informType } = record;
    // const textArr = informType.includes(',') ? informType.split(',') : [informType];
    this.setState({
      modalTitle: '配置修改',
      visible: true,
      defaultFormData: {...record,  },
    })
  }
  showModal = () => {
    this.setState({
      modalTitle: '添加增值服务',
      visible: true,
      defaultFormData: {},
    });
  }
  // 用于承接子组件传来的值
  handleChange =(e) => {
    this.setState({
      visible: e,
    });
  }

  renderSelectOption = (data) => {
    return data.map(item => (
        <Option key={item.code} value={item.code}>{item.item}</Option>
    ));
	}

  render() {
    const { valueAdd: { valueAddConfigList }, dispatch, loading } = this.props;
    const { visible, modalTitle, defaultFormData } = this.state;
    return (
      <PageHeaderLayout>
        <Card title="增值服务配置管理" bordered={false}>
          <Table
            bordered
            scroll={{x: 1400,y:450}}
            dataSource={valueAddConfigList}
            columns={this.columns}
            rowKey={item => item.serviceType}
            title={()=>{
              return (
                <Fragment>
                <Button  type="primary"  style={{marginTop: '10px'}} onClick={this.showModal}>添加增值服务</Button>
                </Fragment>
              )
            }}
            size="small"
						loading={loading}
            pagination={false}
          />
          {
            visible ? (<AddItem title={modalTitle} dispatch={dispatch} defaultFormData={defaultFormData} visible={visible} onChange={this.handleChange.bind(this)} />) : null
          }
        </Card>
      </PageHeaderLayout>
    );
  }
}