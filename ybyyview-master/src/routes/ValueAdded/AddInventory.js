import React from 'react';
import moment from 'moment';
import {
  Row, Col, Input, Form, Select, DatePicker, Modal,message,InputNumber
} from 'snk-web';
import {
  getDataSource,
} from '../../utils/utils';
import styles from './addInventory.less'

const { Option } = Select;
const dateFormat = 'YYYY-MM-DD';
@Form.create()
export default class AddInventory extends React.Component {
	constructor(props) {
    super(props);
		this.state = {
      // servicesItem: [],
      title: props.title,
			visible: props.visible,
			defaultFormData: props.defaultFormData,
			isUpdata: Object.keys(props.defaultFormData).length ? true : false, // 为true时是更新，false是新增
		}
  }
  
  // componentDidMount(){
  //   this.props.dispatch({
  //     type: 'valueAdd/confQuery',
  //   }).then((response) => {
  //     if (response.code === 0) {
  //       this.setState({
  //         servicesItem: response.data,
  //       });
  //     }
  //   });
  // }

	handleOk = (e) => {
    let code= 1;
		this.props.form.validateFields((err, fieldsValue) => {
			const { dispatch } = this.props;
      const { isUpdata, defaultFormData } = this.state;
      if (!err) {
        const { serviceType, status, unitPrice, totalNumber, purchaseDate, invalidDate, validDate,totalPrice } = fieldsValue;
				let payload = {
          serviceType,
          status,
          unitPrice: Number(unitPrice).toFixed(2),
          totalNumber: parseInt(totalNumber),
          purchaseDate: moment(purchaseDate).format(dateFormat),
          invalidDate: moment(invalidDate).format(dateFormat),
          validDate: moment(validDate).format(dateFormat),
          totalPrice:Number(totalPrice).toFixed(2),
        };
				if (isUpdata) {
					dispatch({
						type: 'valueAdd/editStockInfo',
						payload: {
              ...payload,
              purchaseId: defaultFormData.purchaseId,
            },
					}).then((response) => {
            response.code==0?message.success(response.message):message.error(response.message);
            code=response.code
          }).then(()=>{
            this.props.onChange(false,code);
          })
				} else {
          payload.remainNumber=payload.totalNumber
					dispatch({
						type: 'valueAdd/addStockInfo',
						payload,
					}).then((response) => {
            response.code==0?message.success(response.message):message.error(response.message);
            code=response.code
          }).then(()=>{
            this.props.onChange(false,code);
          })
				}
			}
    });
		
  }

  handleCancel = (e) => {
		this.props.onChange(false);
  }
  
  renderSelectOption = (data) => {
    return data.filter(e=>e.code?true:e.attribute==0).map(item => (
      item.serviceType?<Select.Option key={item.serviceType} value={item.serviceType}>{item.serviceName}</Select.Option>:
      <Select.Option key={item.code} value={item.code}>{item.item}</Select.Option>
    ));
  }

  onChangeTotalPrice1=(e)=>{
     let {form} =this.props;
     let totalPrice= (form.getFieldValue('unitPrice')||0)*e
     form.setFieldsValue({totalPrice})
  }
  onChangeTotalPrice2=(e)=>{
     let {form} =this.props;
     let totalPrice= e*form.getFieldValue('totalNumber')||0
     form.setFieldsValue({totalPrice})
  }

  serviceTypeOnchange=(e)=>{
    let items =  this.props.servicesItem.filter(item=>item.serviceType==e)
    let provider = items[0].provider
    let status=items[0].flag==0?'上架':'下架'
    this.props.form.setFieldsValue({provider,status})
  }

	render() {
		const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
		};
		const { getFieldDecorator } = this.props.form;
		const { defaultFormData } = this.props;
		return (
      <Modal
          maskClosable={false}
          title={this.state.title}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form layout="inline" className={styles.customaddwrapper}>
            <Row>
              <Col span={12}>
                <Form.Item label="服务名称">
                  {getFieldDecorator('serviceType', {
                    rules: [{
                      type: 'number', message: '增值服务名称不能为空', required: true,
                    }],
                    initialValue: Number(defaultFormData.serviceType)||null,
                  })(
                    <Select onChange={this.serviceTypeOnchange}>
                        {this.renderSelectOption(this.props.servicesItem)}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="厂商">
                {getFieldDecorator('provider', {
                  initialValue: defaultFormData.provider,
                })(
                  <Input disabled />
                )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="状态">
                {getFieldDecorator('status', {
                  initialValue: defaultFormData.status,
                })(
                  <Input disabled />
                )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="采购日期">
                  {getFieldDecorator('purchaseDate', {
                    rules: [{
                      required: true, message: '请输入采购日期',
                    }],
                    initialValue: defaultFormData.purchaseDate ? moment(moment(defaultFormData.purchaseDate).format(dateFormat), dateFormat) : null,
                  })(
                    <DatePicker />
                  )}
                </Form.Item>
              </Col>
              {/* <Col span={12}>
                <Form.Item label="生效日期">
                  {getFieldDecorator('validDate', {
                    rules: [{
                      required: true, message: '请输入生效日期',
                    }],
                    initialValue: defaultFormData.validDate ? moment(moment(defaultFormData.validDate).format(dateFormat), dateFormat) : null,
                  })(
                    <DatePicker />
                  )}
                </Form.Item>
              </Col> */}
              <Col span={12}>
                <Form.Item label="失效日期">
                  {getFieldDecorator('invalidDate', {
                    rules: [{
                      required: true, message: '请输入失效日期',
                    }],
                    initialValue: defaultFormData.invalidDate ? moment(moment(defaultFormData.invalidDate).format(dateFormat), dateFormat) : null,
                  })(
                    <DatePicker />
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="购买总量">
                  {getFieldDecorator('totalNumber', {
                     rules: [{
                      required: true, message: '请输入购买总量',
                    }],
                    initialValue: defaultFormData.totalNumber,
                  })(
                    <InputNumber  min={0} precision={0}  onChange={this.onChangeTotalPrice1} style={{width:'100%'}}/>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="单价">
                  {getFieldDecorator('unitPrice', {
                    rules: [{
                      required: true, message: '请输入服务单价',
                    }],
                    initialValue: defaultFormData.unitPrice,
                  })(
                    <InputNumber min={0.01} precision={2}   onChange={this.onChangeTotalPrice2}  style={{width:'100%'}}/>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="总价">
                  {getFieldDecorator('totalPrice', {
                    initialValue: defaultFormData.totalPrice,
                  })(
                    <InputNumber  min={0.01} precision={2}  style={{width:'100%'}} />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Modal>
		);
	}
}