import React, { PureComponent } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Card,
  Table,
  Tabs,
  Button,
  message,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './ApplyList.less';
import SearchForm from './SearchForm';
import SearchExactForm from './SearchExactForm';

const columns = [ 
  {
    title: '渠道代码',
    dataIndex: 'formExtEnterpCode',
    align: 'center',
  },
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '投保单号',
    dataIndex: 'cPlyAppNo',
    align: 'center',
  },
  {
    title: '年化保费',
    dataIndex: 'prm',
    align: 'center',
  },
  {
    title: 'Tsr姓名',
    dataIndex: 'tsrName',
    align: 'center',
  },
  {
    title: '产品代码',
    dataIndex: 'prodNo',
    align: 'center',
  },
  {
    title: '款别',
    dataIndex: 'kuanbie',
    align: 'center',
  },
  {
    title: '保险起期',
    dataIndex: 'insrncBeginDate',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
  },
  {
    title: '名单类型',
    dataIndex: 'menuType',
    align: 'center',
  },
  {
    title: '投保人姓名',
    dataIndex: 'toName',
    align: 'center',
  },
  {
    title: '被保人姓名',
    dataIndex: 'beName',
    align: 'center',
  },
  {
    title: '被保人姓别',
    dataIndex: 'sex',
    align: 'center',
    render: val =>{
      if(val==='01'){
        return '男'
      }else if(val==='02'){
        return '女'
      }
      return val;
    },
  },
  {
    title: '被保人出生日期',
    dataIndex: 'birth',
    align: 'center',
    render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
  },
  {
    title: '保单类型',
    dataIndex: 'policyType',
    align: 'center',
  },
  {
    title: '投递地址',
    dataIndex: 'area',
    align: 'center',
  },
  {
    title: '备注',
    width:220,
    dataIndex: 'remark',
    align: 'center',
  },
];

@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.models.searchlist,
}))
export default class ApplyList extends PureComponent {
  state = {
    pageSize:10,
    pageNum:1,
    channelName:'-',
    searchType:'',//'nomal||exact'
  };


  componentWillMount(){
    Object.keys(this.props.searchlist).forEach(key => {
      if(key !== 'allPolicyQueryData' && key!== 'policyQuerySearchFormData' && key !== 'selfChannelData' && key!== 'policyQueryDefaultTab' && key!== 'policyMapInfo') { delete this.props.searchlist[key] }
    });
  }
  
  handleFormReset = (form) => {
    form.resetFields();
  };

  handleSearch = (e,form) => {
    e.preventDefault();
    this.form = form;
    this.setState({pageNum:1,searchType:'nomal'},()=>{
      this.searchData(form);
    });
  };

  searchData(form){
    const { dispatch} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'searchlist/SearchApplyList',
        payload: this.perpareValues(fieldsValue),
      }).then(()=>{
      }).catch(e=>{
        message.error('网络异常请稍后再试');
      });
      
    });
  }

  downLoad(form){
    const { dispatch} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'searchlist/DownLoadApplyList',
        payload: this.perpareValues(fieldsValue),
      }).then(()=>{
      }).catch(e=>{
        console.log(e);
        message.error('网络异常请稍后再试');
      });
    });
  }

  handleExactSearch = (e,form) => {
    e.preventDefault();
    this.form = form;
    this.setState({pageNum:1,searchType:'exact'},()=>{
      this.searchExactData(form);
    });
  }

  searchExactData(form){
    const { dispatch} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'searchlist/SearchExactApplyList',
        payload: fieldsValue,
      }).then(()=>{
      }).catch(e=>{
        message.error('网络异常请稍后再试');
      });
      
    });
  }

 

  perpareValues(values){
    if(!values.insrncBeginDate||values.insrncBeginDate.length===0){
      values.insrncBeginDateStart = '';
      values.insrncBeginDateEnd = '';
    } else {
      values.insrncBeginDateStart = values.insrncBeginDate[0].format("YYYY-MM-DD");
      values.insrncBeginDateEnd = values.insrncBeginDate[1].format("YYYY-MM-DD");
    }
    if(!values.appDate||values.appDate.length===0){
      values.appDateStart = '';
      values.appDateEnd = '';
    } else {
      values.appDateStart = values.appDate[0].format("YYYY-MM-DD");
      values.appDateEnd = values.appDate[1].format("YYYY-MM-DD");
    }

    delete values.insrncBeginDate;
    delete values.insrncBeginDate;
    delete values.appDate;
    values.pageNum = this.state.pageNum;
    values.pageSize = this.state.pageSize;
    values.type = parseInt(values.type);
    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  renderAdvancedForm() {
    return (
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab={<span>模糊查询</span>} key="1">
          <SearchForm {...this.props} root={this}/>
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span>精确查询</span>} key="2">
         <SearchExactForm {...this.props} root={this}/>
        </Tabs.TabPane>
      </Tabs>
      
    );
  }


  handleTableChange = (pagination) => {
    this.setState({
      pageSize: pagination.pageSize,
      pageNum: pagination.current,
    },()=>{
      if(this.state.searchType === 'nomal'){
        this.searchData(this.form);
      }else{
        this.searchExactData(this.form);
      }
    });
    
  };

  render() {
    const { searchlist, loading } = this.props;
    const data = searchlist.applylist||[];
    const p = {};
    let datalist = [];
    let allCount = 0;
    let sumMoney = 0;
    let sumCount = 0;
    if(data.cPlyNo){
      //精确查找
      datalist = [data];
    }else{
      if(data[0]){
        datalist = data[0].list;
        allCount = data[0].total;
      }
      if(data[1]){
        sumMoney = data[1].totalMoney;
        sumCount = data[1].count;
      }
     
    }
    if(this.state.searchType==='nomal'){
      p.title=() => {
        return <div className={styles.TableTitle}>
          <Button style={{ marginLeft: 8 }} type="primary" onClick={()=>{this.downLoad(this.nomalForm)}}>
              下载
            </Button>
        </div>
      }
    }
   
  

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
            <div>{this.renderAdvancedForm()}</div>
            <Table
              loading={loading}
              rowKey={record => record.cPlyNo}
              dataSource={datalist}
              bordered
              scroll={{ x: 2700 }}
              size="small"
              {...p}
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                ...{
                  current:this.state.pageNum,
                  pageSize:this.state.pageSize,
                  showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                  total:allCount,
                  pageSizeOptions:['10','20','30','40','50'],
                },
              }}
              columns={columns}
              onChange={this.handleTableChange}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
