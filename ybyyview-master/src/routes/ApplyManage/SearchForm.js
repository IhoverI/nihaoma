import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  DatePicker,
} from 'snk-web';
import ChannelAndProduction from 'components/ChannelAndProduction';
import { getSelectChild } from '../../utils/utils'



const FormItem = Form.Item;
const { Option } = Select;

@Form.create()
export default class SearchForm extends PureComponent {
  
  constructor(props){
    super(props);
    this.state = {
      disabledArea:true,
      disabledAppDate:false,
    };
    props.root.nomalForm = props.form;
  }
  handleSearch(e){
      this.props.root.handleSearch(e,this.props.form);
  }

  onTypeChange(value){
    const { form} = this.props;
    const { getFieldValue,setFieldsValue } = form;
    const extEnterpCode = getFieldValue('extEnterpCode');
    this.setAreaState(value,extEnterpCode);
  }

  // （2）在查询“1:成功清单”的时候，常规情况下遮住地区字段。如果渠道名称选择“自建渠道”，则露出地区字段。
  // 在查询“2:质检不通过（自建渠道）；3:待质检（自建渠道）”的时候，遮住投保日期，地区和保险起期。
  setAreaState = (type,extEnterpCode) => {
    const { form} = this.props;
    const { setFieldsValue } = form;
    let disabledArea = false;
    let disabledAppDate = false;
    if(type==="1"){ // 成功
       if (extEnterpCode) {
        // 招商银行
        if (extEnterpCode === '95555') {
          disabledArea = false;
        }else{
          disabledArea = true;
          setFieldsValue({area:undefined});
        }
      }else{
        disabledArea = true;
        setFieldsValue({area:undefined});
      }
    }else{
      // 在查询“2:质检不通过（自建渠道）；3:待质检（自建渠道）”的时候，遮住投保日期，地区和保险起期。
      disabledArea = true;
      disabledAppDate = true;
      setFieldsValue({area:undefined,insrncBeginDate:undefined,appDate:undefined});
    }
    this.setState({disabledArea,disabledAppDate});
  }

  onChannelChange(value){
    const { form } = this.props;
    const { getFieldValue } = form;
    const type = getFieldValue('type');
    this.setAreaState(type,value);
  }
 
  render() {
    const { form , root } = this.props;
    const { getFieldDecorator,getFieldValue } = form;
    const disabledArea = false;
  
    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch.bind(this)} layout="inline">
        <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="保单年份">
              {getFieldDecorator('pYear',{initialValue: pyear})(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild('pyear')}
                </Select>
                )}
            </FormItem>
          </Col>
        </Row>
        <ChannelAndProduction 
          productOnSale = {true}
          onChannelChange={this.onChannelChange.bind(this)}
          channelFieldKey="extEnterpCode"
          productionFieldKey="prodNo"
          {...this.props}
          allowClear
        />
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="清单类型">
              {getFieldDecorator('type',{
                initialValue: "1",
              })(
                <Select 
                  onChange={this.onTypeChange.bind(this)}
                  placeholder="请选择"
                  style={{ width: '100%' }}
                >
                  {/* {getSelectChild("applyTypeList")} */}
                  <Select.Option key="1">成功清单</Select.Option>
                  <Select.Option key="2">质检不通过</Select.Option>
                  <Select.Option key="3">待质检</Select.Option>
                </Select>)}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="投保日期">
              {getFieldDecorator('appDate')( 
                <DatePicker.RangePicker disabled={this.state.disabledAppDate} allowClear style={{ width: '100%' }}  />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="地区">
              {getFieldDecorator('area')(
                <Select placeholder="请选择" style={{ width: '100%' }} allowClear disabled={this.state.disabledArea}>
                  {/* <Option value="0">广东</Option>
                <Option value="1">深圳</Option>
                <Option value="2">成都</Option>
                <Option value="3">南昌</Option>
                <Option value="4">广西</Option> */}
                  <Option value={2}>上海招行</Option>
                  <Option value={3}>成都招行</Option>
                </Select>)}
            </FormItem>
          </Col>
          <Col md={12} sm={30}>
            <FormItem label="保险起期">
              {getFieldDecorator('insrncBeginDate')(
                <DatePicker.RangePicker  disabled={this.state.disabledAppDate} style={{ width: '100%' }} />
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
          <Button type="primary" htmlType="submit">
              查询
          </Button>
          <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
              重置
          </Button>
        </div>
      </Form>
    );
  }
}
