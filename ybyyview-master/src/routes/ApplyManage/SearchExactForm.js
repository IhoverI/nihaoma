import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
} from 'snk-web';

const FormItem = Form.Item;

@Form.create()
export default class SearchExactForm extends PureComponent {
  handleExactSearch(e){
    this.props.root.handleExactSearch(e,this.props.form);
  }
  render() {
    const { form,root } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleExactSearch.bind(this)} layout="inline">
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="投保单号">
              {getFieldDecorator('cPlyAppNo',
              {rules: [{ max: 30, message: '请输入正确的投保单号' }]}
              )(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
              重置
            </Button>
        </div>
      </Form>
    );
  }
}
