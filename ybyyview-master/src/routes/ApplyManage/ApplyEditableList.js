import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
	Select,
  Table,
  Card,
  Button,
  DatePicker,
  Form,
	Col,
	Row,
} from 'snk-web';
import { getDataSource,getSelectChild } from '../../utils/utils'
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

const FormItem = Form.Item;
@connect(({  loading, searchlist }) => ({
  loading: loading.models.searchlist,
  searchlist
}))
@Form.create()
export default class ApplyEditableList extends PureComponent {
  state = {
   current:0,
   dataSource: [],
   pageNum: 1,
   pageSize: 10,
  };
  componentDidMount() {
    this.searchData();
  }

  handleTablePage = (pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData();
    });
  }

  searchData = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
				values = this.prepareValue(values);
        this.props.dispatch({
          type: 'searchlist/queryChannelPolicy',
          payload: {
            pageNum: this.state.pageNum,
            pageSize: this.state.pageSize,
						postValue:values,
						queryType: '1',
          }
        });
      }
    });
    
  }

  handleFormReset=()=>{
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({pageNum:1,pageSize:10});
  }

  handleSearch=(e)=>{
    e.preventDefault();
    this.searchData();
  }

  prepareValue(values){
    if(!values.cPlyAppNo) {
      values.cPlyAppNo = null;
    }else{
      values.cPlyAppNo = values.cPlyAppNo.replace(/^\s+|\s+$/gm,'');
    }
    
    if(!values.appDate||values.appDate.length===0) {
      values.appDate = null;
    }else{
      const re = [values.appDate[0].format("YYYY-MM-DD"),values.appDate[1].format("YYYY-MM-DD")];
      values.appDate = JSON.stringify(re);
    }

    for(var key in values){
      if(key!=='appDate'&&key!=='cPlyAppNo'&&key!=='status'){
        delete values[key];
      }
    }
  
    return values;
  }
	applyEdit = (policyMapInfo, cPlyAppNo) => {
		this.props.dispatch({
			type: 'searchlist/goToEdit',
			payload: {
				...policyMapInfo,
				cPlyAppNoEditable: cPlyAppNo,
			}
		});
	}
  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className='custom-form-wrapper' onSubmit={this.handleSearch} layout="inline">
        <Row>
					<Col span={15}>
						<FormItem label="投保单录入时间">  
							{getFieldDecorator('appDate', this.state.mustInputRule)(
								<DatePicker.RangePicker allowClear={true} style={{ width: '100%' }} onChange={this.onChange}/>
							)}
						</FormItem>
					</Col>
				</Row>
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
        </div>
      </Form>
    );
  }

  render() {
    const { searchlist: { queryChannelData }, loading } = this.props;
    let total;
    let {dataSource} = this.state;
    if (queryChannelData) {
      dataSource = queryChannelData.list;
      total = queryChannelData.total || 0;
    }
    this.state.dataSource = dataSource;
    const rowKey = (record,index) => {
      return index
    }
		const format = (val, code) => {
			let text;
			switch (val) {
				case '3': text = '待审核'; break;
				case '4': text = '已打回'; break;
				default: break;
			}
			return text;
		}
		const columns = [
			{
				title: '投保单号',
				dataIndex: 'cPlyAppNo',
				align: 'center'
			},
			{
				title: '销售录音电话',
				dataIndex: 'saleManTel',
				align: 'center'
			},
			{
				title: '销售人员姓名',
				dataIndex: 'teleLeadName',
				align: 'center'
			},
			{
				title: '销售人员编号',
				dataIndex: 'teleLeadCode',
				align: 'center'
			},
			{
				title: '产品名称',
				dataIndex: 'prodCName',
				align: 'center',
			}, 
			{
				title: '款别',
				dataIndex: 'feeType',
				align: 'center'
			}, 
			{
				title: '被保人姓名',
				dataIndex: 'insrncName',
				align: 'center'
			}, 
			{
				title: '投保日期',
				dataIndex: 'appDate',
				align: 'center'
			}, 
			{
				title: '审核状态',
				dataIndex: 'status',
				align: 'center',
				render: (text) => {
					return (
						<span>{format(text,'status')}</span>
					);
				}
			},
			{
				title: '备注',
				dataIndex: 'remark',
				align: 'center',
			},
			{
				title: '操作',
				dataIndex: 'action',
				align: 'center',
				render: (text, record) => (<Button type="primary" size="small" onClick={this.applyEdit.bind(this, record.policyMapInfo, record.cPlyAppNo)}>编辑</Button>),
			}
		];
		return (
			<PageHeaderLayout title="">
				<Card bordered={false}>
					<div>{this.renderAdvancedForm()}</div>
					<Table 
						rowKey={rowKey}
						pagination={false} 
						size='small' 
						bordered
						pagination={{
							showQuickJumper: true,
							showSizeChanger: true,
							...{
									current:this.state.pageNum,
									pageSize:this.state.pageSize,
									total:total,
							},
						}} 
						onChange={this.handleTablePage}
						columns={columns} 
						loading={loading}
						dataSource={dataSource} />
				</Card>
			</PageHeaderLayout>
		);
  }
}

