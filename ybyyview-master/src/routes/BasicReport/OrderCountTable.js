import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { getSelectChild } from '../../utils/utils'

import {
  Row,
  Col,
  Card,
  Form,
  Table,
} from 'snk-web';


const formaUnit = (text) => {
    let extEnterpDesc;
    if (getSelectChild('extEnterpDesc')){
      for (let i in getSelectChild('extEnterpDesc')) {
        if (getSelectChild('extEnterpDesc')[i].props.value === text) {

          extEnterpDesc = getSelectChild('extEnterpDesc')[i].props.children
        }
      }
    }
    return extEnterpDesc
  }

const realDayColumns = [
  {
    title: '排名',
    dataIndex: 'order',
    align: 'center',
    width: "20%",
  },
  {
    title: '项目组',
    dataIndex: 'orderExtenterpCode',
    align: 'center',
    width: "40%",
    render: (text) => {
      return (<span>{formaUnit(text)}</span>)
    }
  },
  {
    title: '占比',
    dataIndex: 'realDayEachRate',
    align: 'center',
    width: "30%",
  }, 
];
const realMonthColumns = [
  {
    title: '排名',
    dataIndex: 'order',
    align: 'center',
    width: "20%",
  },
  {
    title: '项目组',
    dataIndex: 'orderExtenterpCode',
    align: 'center',
    width: "40%",
    render: (text) => {
      return (<span>{formaUnit(text)}</span>)
    }
  },
  {
    title: '占比',
    dataIndex: 'realMonthEachRate',
    align: 'center',
    width: "30%",
  }, 
];


@connect(({ searchlist, loading }) => ({
  // searchlist,
  // loading: loading.models.searchlist,
}))
export default class OrderCountTable extends PureComponent {

  getColumns(){
    if (this.props.dateFlag === '2') {
      return realMonthColumns;
    } else {
      return realDayColumns;
    }

  }

  render() {
    const {orderCountData,loading} = this.props;
    let data;
    if (orderCountData) {
      data = orderCountData[0] || [];
    }
    const rowKey = (record, index) => { 
        return index;
    }

    return (
      <Table
        loading={loading}
        rowKey={rowKey}
        dataSource={data}
        size="small"
        scroll={{ y: 300 }}
        columns={this.getColumns()}
      />
    );
  }
}
