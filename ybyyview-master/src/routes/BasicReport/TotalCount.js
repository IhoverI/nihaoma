import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild } from '../../utils/utils';
import styles from './BasicReport.less';
const dayIcon = require('../../assets/u8840.png');
const monthIcon = require('../../assets/u8969.png');
const yearIcon = require('../../assets/u8993.png');

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const { Option } = Select;

const dayIconStyle = {
    background: `url(${dayIcon})`,
    backgroundRepeat: 'no-repeat',
    backgroundPositionX: 15,
    backgroundPositionY: 8,
    backgroundColor: "rgba(0, 0, 255, 0.61)"
};
const monthIconStyle = {
  background: `url(${monthIcon})`,
  backgroundRepeat: 'no-repeat',
  backgroundPositionX: 15,
  backgroundPositionY: 9,
  backgroundColor: "rgba(130, 221, 47, 0.74)",
}
const yearIconStyle = {
  background: `url(${yearIcon})`,
  backgroundRepeat: 'no-repeat',
  backgroundPositionX: 16,
  backgroundPositionY: 9,
  backgroundColor: "rgba(255, 126, 0, 0.61)",
}

@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.models.rule,
}))
@Form.create()
export default class ApplyList extends PureComponent {
  state = {
  };

  componentWillReceiveProps(nextProps) {
  }
  
 
  render() {
    const {totalCountData} = this.props;
    let totalCountThisYear,rateYear,totalCountThisMonth,rateMonth,totalCountThisDay,
        rateDay,rateDayCount,rateMonthCount,rateYearCount;
    if (totalCountData) {
      totalCountThisYear = totalCountData.当年总业绩 || 0;
      rateYear = totalCountData.年比率 || 0;
      totalCountThisMonth = totalCountData.当月总业绩 || 0;
      rateMonth = totalCountData.月比率 || 0;
      totalCountThisDay = totalCountData.当日总业绩 || 0;
      rateDay = totalCountData.日比率 || 0;
      if (rateDay !== '' && rateDay !== 0) {
        rateDayCount = rateDay.substring(0,rateDay.length - 1);
      }
      if (rateMonth !== '' && rateMonth !== 0) {
        rateMonthCount = rateMonth.substring(0,rateMonth.length - 1);
      }
      if (rateYear !== '' && rateYear !== 0) {
        rateYearCount = rateYear.substring(0,rateYear.length - 1);
      } 
    }

    return (
        <Card bordered={false}>
          <div className={styles.titleDiv}>
            <div className={styles.titleItem}>
              <div className={styles.rateDesc}>
                <img src={ rateDayCount >= 0 ? require('../../assets/up.png') : require('../../assets/down.png')} />
                <span style={{marginRight:6, color: rateDayCount >= 0 ? '#1afa29' : 'red'}}>{rateDay}</span>
                <span>同比当日</span>
              </div>
              <div className={styles.itemIcon} style={dayIconStyle}></div>
              <div style={{marginLeft: 16, fontSize: 16}}>
                 <div>当日总业绩</div>
                 <div>{totalCountThisDay}</div>
               </div>
            </div>
            <div className={styles.titleItem}>
              <div className={styles.rateDesc}>
                <img src={ rateMonthCount >= 0 ? require('../../assets/up.png') : require('../../assets/down.png')} />
                <span style={{marginRight:6, color: rateMonthCount >= 0 ? '#1afa29' : 'red'}}>{rateMonth}</span>
                <span>同比上月</span>
              </div>
              <div className={styles.itemIcon} style={monthIconStyle}></div>
              <div style={{marginLeft: 16, fontSize: 16}}>
                 <div>当月总业绩</div>
                 <div>{totalCountThisMonth}</div>
               </div>
            </div>
            <div className={styles.titleItem}>
              <div className={styles.rateDesc}>
                <img src={ rateYearCount >= 0 ? require('../../assets/up.png') : require('../../assets/down.png')} />
                <span style={{marginRight:6, color: rateYearCount >= 0 ? '#1afa29' : 'red'}}>{rateYear}</span>
                <span>同比去年</span>
              </div>
               <div className={styles.itemIcon} style={yearIconStyle}></div>
               <div style={{marginLeft: 16, fontSize: 16}}>
                 <div>当年总业绩</div>
                 <div>{totalCountThisYear}</div>
               </div>
            </div>
          </div>
        </Card>
    );
  }
}
