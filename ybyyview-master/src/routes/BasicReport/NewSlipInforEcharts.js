import React, { PureComponent, Fragment } from 'react';
import echarts from 'echarts/lib/echarts'; // 必须
import Line from './Line'

export default class NewSlipInforEcharts extends PureComponent {

  componentWillReceiveProps(nextProps) {
    const {newSlipInforData} = nextProps;
    const xList = [];
    const legend = [];
    const values = [];

    const format =(newSlipInforData) => {
      if (newSlipInforData) {
        legend.push(newSlipInforData[0])
        const data = []
        const listArr = [];
        for (const i in newSlipInforData) {
          if (newSlipInforData[i]) {
            listArr.push(newSlipInforData[i])
          }
        }
        listArr.shift();
        for (const i in listArr) {
          if (listArr[i]) {
            data.push(listArr[i][0])
          }
        }
        return values.push(data);
      } 
    }
    
    if (newSlipInforData) {
      const xListData = [];
        if (newSlipInforData[1]) {
          for (const i in newSlipInforData[1]) {
            if (newSlipInforData[1][i]) {
               xListData.push(newSlipInforData[1][i])
            }
          }
          xListData.shift();
          for (const i in xListData) {
            if (xListData[i]) {
              xList.push(xListData[i][0].slice(5,7))
            }
          }
        }

        format(newSlipInforData[2]);
        format(newSlipInforData[3]);
        format(newSlipInforData[4]);
        format(newSlipInforData[5]);
    }

    const newXlist = Array.from(new Set(xList)) 
    const Setting = {
      xList: newXlist,
      data: values,
      legend,
      colors: ['#aed4c2','#dda490','#8db9be','#fbe289'],
      danwei: '万',
      areaStyle: {normal: {}},
      width: 600,
    };
    const LineOption = new Line(Setting);
    const lineEcharts = echarts.init(window.document.getElementById('newSlipInforEcharts'));
    lineEcharts.setOption(LineOption);
  }

  render() {
    return (
      <div id='newSlipInforEcharts' style={{width: '76%',minHeight: '280px'}} />
    );
  }
}
