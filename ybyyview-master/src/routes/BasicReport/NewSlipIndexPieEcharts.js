import React, { PureComponent } from 'react';
import echarts from 'echarts/lib/echarts'; // 必须
import Pie from './Pie'

export default class NewSlipIndexPieEcharts extends PureComponent {

  componentWillReceiveProps(nextProps) {
    const {newSlipIndexPieData} = nextProps;

    let legend;
    let data;
    if (newSlipIndexPieData) {
        if (newSlipIndexPieData[2]) {
            legend = newSlipIndexPieData[2] || {};
        }
        if (newSlipIndexPieData[1]) {
            data = newSlipIndexPieData[1] || {};
        }
    }
    const Setting = {
      data,
      legend,
    };
    const LineOption = new Pie(Setting);
    const lineEcharts = echarts.init(window.document.getElementById('NewSlipIndexPieEcharts'));
    lineEcharts.setOption(LineOption);
  }

  render() {
    return (
      <div id='NewSlipIndexPieEcharts' style={{width: '100%' ,minHeight: '320px'}} />
    );
  }
}
