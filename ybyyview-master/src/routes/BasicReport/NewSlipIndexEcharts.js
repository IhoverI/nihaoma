import React, { PureComponent } from 'react';
import echarts from 'echarts/lib/echarts'; // 必须
import Line from './Line'

export default class NewSlipIndexEcharts extends PureComponent {

  componentWillReceiveProps(nextProps) {
    const {newSlipIndexData} = nextProps
    let xList;
    const legend = [];
    const values = [];
    if (newSlipIndexData) {
        xList = newSlipIndexData.xList;
        if (newSlipIndexData.yList) {
            for (const key in newSlipIndexData.yList) {
                legend.push(key);
                values.push(newSlipIndexData.yList[key]);
            }
        }
    }
    
    const Setting = {
      xList,
      data: values,
      legend,
      colors: ['#8618ffc4'],
      danwei: '%',
      width: 540,
    };
    const LineOption = new Line(Setting);
    const lineEcharts = echarts.init(window.document.getElementById('NewSlipIndexEcharts'));
    lineEcharts.setOption(LineOption);
  }

  render() {
    return (
      <div id='NewSlipIndexEcharts' style={{width: '100%',minHeight: '320px'}} />
    );
  }
}
