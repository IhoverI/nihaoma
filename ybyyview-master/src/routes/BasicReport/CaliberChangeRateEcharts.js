import React, { PureComponent, Fragment } from 'react';
import echarts from 'echarts/lib/echarts'; // 必须
import Line from './Line'

export default class CaliberChangeRateEcharts extends PureComponent {

  componentWillReceiveProps(nextProps) {
    const {caliberChangeRateData} = nextProps
    const xList = [];
    const legend = [];
    const values = [];
    const yinHangValue = [];
    const huaAnValue = [];
    if (caliberChangeRateData) {
        if (caliberChangeRateData.银行口径) {
            legend.push('银行口径')
            const data = caliberChangeRateData.银行口径
            for (const i in data) {
                if (data[i]) {
                    yinHangValue.push(data[i])
                }
            }
            values.push(yinHangValue);
        }
        if (caliberChangeRateData.华安口径) {
            legend.push('华安口径')
            const data = caliberChangeRateData.华安口径
            for (const i in data) {
                if (data[i]) {
                    xList.push(`${i}月`)
                    huaAnValue.push(data[i])
                }
            }
            values.push(huaAnValue)
        }
    }

    const Setting = {
      xList,
      data: values,
      legend,
      colors: ['#91e5e7','#e58dc2'],
      danwei: '%',
      areaStyle: {normal: {}},
    };
    const LineOption = new Line(Setting);
    const lineEcharts = echarts.init(window.document.getElementById('CaliberChangeRateEcharts'));
    lineEcharts.setOption(LineOption,true);
  }

  render() {
    return (
      <div id='CaliberChangeRateEcharts' style={{width:'100%',minHeight: '280px'}} />
    );
  }
}
