// import echarts from 'echarts/lib/echarts'; // 必须
import 'echarts/lib/chart/pie';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markLine';

import { getSelectChild } from '../../utils/utils'

export default function Pie(setting) {
  const {
    data, legend,
  } = setting;
  const desc = [];
  const seriesData = [];
  if (legend) {
     if (getSelectChild('payChannel1') || getSelectChild('prodDesc')) {
      for (const j in legend) {
          if (legend[j]) {
            for (const i in getSelectChild('payChannel1')) {
                if (getSelectChild('payChannel1')[i].props.value === legend[j]) {
                    desc.push(getSelectChild('payChannel1')[i].props.children);
                }
            }
          }
        for (const i in getSelectChild('prodDesc')) {
          if (getSelectChild('prodDesc')[i].props.value === legend[j]) {
            desc.push(getSelectChild('prodDesc')[i].props.children);
          }
        }
      }
    }
  } 

  if (data) {
    for (const i in desc) {
        if (desc[i]) {
            seriesData.push({
                value:data[i],
                name: desc[i],
            })
        } 
    }
  }

  const option = {
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c}(万件) ({d}%)",
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: desc,
    },
    
    series : [
        {
            name: '',
            type: 'pie',
            radius : '50%',
            data: seriesData,
            center: ['58%', '50%'],
            itemStyle: {
                // normal: {label:{
            	// 	show:true,
                //     formatter:'{b} : {c} ({d}%)',
                // },
            	// },
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)',
                },
            },
        },
    ],
};
  return option;
}

