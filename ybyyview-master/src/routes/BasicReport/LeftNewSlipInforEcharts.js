import React, { PureComponent, Fragment } from 'react';
import styles from './BasicReport.less';

export default class LeftNewSlipInforEcharts extends PureComponent {

  state = {
    onlineInsurance : 0,
    onlineInsuranceRate: 0,
    onlineInsuranceRateCount: 0,
    tuibaoFee: 0,
    tuibaoRate: 0,
    tuibaoRateCount: 0,
  };

  componentWillReceiveProps(nextProps) {
    const {newSlipInforData} = nextProps
    if (newSlipInforData) {
      if (newSlipInforData[0]) {
        this.setState({
          onlineInsurance: newSlipInforData[0][0],
          tuibaoFee: newSlipInforData[0][1],
          onlineInsuranceRate: newSlipInforData[0][2],
          tuibaoRate: newSlipInforData[0][3],
        });
        if (newSlipInforData[0][2]) {
          const onlineInsuranceRateCount = newSlipInforData[0][2].substring(0,newSlipInforData[0][2].length - 1);
          this.setState({
            onlineInsuranceRateCount,
          });
        }
        if (newSlipInforData[0][3]) {
          const tuibaoRateCount = newSlipInforData[0][3].substring(0,newSlipInforData[0][3].length - 1);
          this.setState({
            tuibaoRateCount,
          });
        }
      } 
    }
  }

  render() {
    return (
      <div className={styles.LeftNewSlipDiv}>
        <div className={styles.LeftNewSlipItem}>
          <div>累计本年新单线上保费收入</div>
          <div>
            <span style={{fontSize:22}}>{this.state.onlineInsurance}</span>
            <span>（万）</span>
          </div>
          <div style={{position: 'relative'}}>
            <img src={this.state.onlineInsuranceRateCount >= 0 ? require('../../assets/up.png') : require('../../assets/down.png')} alt='' />
            {this.state.onlineInsuranceRate}
            <span style={{marginLeft: 4}}>同比上年</span>
          </div>
        </div>
        <div className={styles.LeftNewSlipItem}>
          <div>累计本年新单退保保费</div>
          <div>
            <span style={{fontSize:22}}>{this.state.tuibaoFee}</span>
            <span>（万）</span>
          </div>
          <div style={{position: 'relative'}}>
            <img src={this.state.tuibaoRateCount >= 0 ? require('../../assets/up.png') : require('../../assets/down.png')} alt='' />
            {this.state.tuibaoRate}
            <span style={{marginLeft: 4}}>同比上年</span>
          </div>
        </div>
      </div>
    );
  }
}
