import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Radio,
  Spin,
} from 'snk-web';

import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild } from '../../utils/utils';
import TotalCount from './TotalCount';
import NewSlipInforEcharts from './NewSlipInforEcharts';
import NewSlipIndexEcharts from './NewSlipIndexEcharts';
import LeftNewSlipInforEcharts from './LeftNewSlipInforEcharts';
import CaliberChangeRateEcharts from './CaliberChangeRateEcharts';
import DayMonthTotalCountEcharts from './DayMonthTotalCountEcharts';
import NewSlipIndexPieEcharts from './NewSlipIndexPieEcharts';
import OrderCountTable from './OrderCountTable';
import styles from './BasicReport.less';

const { MonthPicker, RangePicker } = DatePicker;
const FormItem = Form.Item;
const { Option } = Select;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;


@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.models.searchlist,
}))
@Form.create()
export default class BasicReport extends PureComponent {
  state = {
    selFlag: '1',
    newExtEnterpCode: '',
    checkType02: '1', // 1--日, 2--月
    dayMonthTotal: [],
    dateFlag: '1',
    caliberCode: '',
    newSlipInfor: '',
    radioValue: 1,
    slipRadioValue: 3,
    checkType03: 31,// 32--月, 31--年
    orderType: 1,
    orderDate: '',
    orderOneYear: new Date().getFullYear(),
    dayMonthTotalFlag: '1',
    loading: false,
    contentNo : 0,
    totalCountLoading: true,
    containLoading: true,
  };

  spinToggle = (key,value) => {
    this.setState({
      contentNo: key,
      loading: value,
    });
  }

  componentDidMount() {

    // 年月日总业绩 -- ok
    this.totalCount();
    
    // 业绩情况
    this.dayMonthTotal(this.state.dayMonthTotal,this.state.checkType02);

    // 业绩情况--项目业绩组排行榜 -- ok
    this.orderCount(this.state.checkType03,new Date().getFullYear()+'-'+'01'+'-'+'01');

    // 新单情况 -- ok
    this.newSlipInfor(this.state.newSlipInfor);
    
    // 新单运营指标 -- ok
    this.newSlipIndex(this.state.newExtEnterpCode);

    // 新单运营指标 -- 排行榜 -- ok
    this.orderTwoData(new Date().getFullYear()+'-'+'01'+'-'+'01',this.state.orderType);

    // 业绩转换率 -- ok
    this.caliberCode(this.state.caliberCode)

  }

  totalCount = () => {
    this.props.dispatch({
      type: 'searchlist/totalCount',
    }).then(()=>{
      this.setState({totalCountLoading : false});
    }).catch((e) => {
      message.error(e.message);
      this.setState({totalCountLoading : false});
    });
  }

  newSlipIndex = (newExtEnterpCode) => {
    this.props.dispatch({
      type: 'searchlist/newSlipIndex',
      payload: {
        newCountExtEnterpCode: newExtEnterpCode
      }
    }).then(()=>{
      this.setState({loading : false,containLoading: false});
    }).catch((e) => {
      message.error(e.message);
      this.setState({loading : false});
    });
  }

  dayMonthTotal = (extEnterpCodeList,checkType02) => {
    this.props.dispatch({
      type: 'searchlist/dayMonthTotalCount',
      payload: {
        checkType02: checkType02, // 日--1，月--2
        extEnterpCodeList: extEnterpCodeList,
      }
    }).then(()=>{
      this.setState({loading : false});
    }).catch((e) => {
      message.error(e.message);
      this.setState({loading : false});
    });
  }

  orderCount = (checkType03,date) => {
    if (this.state.checkType03 === 32) {
      this.props.dispatch({
        type: 'searchlist/orderCount',
        payload: {
          checkType03: checkType03,
          orderMonthDate: date
        }
      }).then(()=>{
      this.setState({loading : false});
      }).catch((e) => {
        message.error(e.message);
        this.setState({loading : false});
      });
    } else {
      this.props.dispatch({
        type: 'searchlist/orderCount',
        payload: {
          checkType03: checkType03,
          orderYearDate: date
        }
      }).then(()=>{
        this.setState({loading : false});
      }).catch((e) => {
        message.error(e.message);
        this.setState({loading : false});
      });
    }
  }

  caliberCode = (caliberCode) => {
     this.props.dispatch({
      type: 'searchlist/caliberChangeRate',
      payload: {
        caliberCode,
      }
    }).then(()=>{
      this.setState({loading : false });
    }).catch((e) => {
      message.error(e.message);
      this.setState({loading : false});
    });
  }

  newSlipInfor = (newSlipInfor) => {
    this.props.dispatch({
      type: 'searchlist/newSlipInfor',
      payload: {
        newExtEnterpCode: newSlipInfor
      }
    }).then(()=>{
      this.setState({loading : false});
    }).catch((e) => {
        message.error(e.message);
        this.setState({loading : false});
      });
  }

  orderTwoData = (date,orderType) => {
     if (this.state.slipRadioValue === 3) {
       this.props.dispatch({
        type: 'searchlist/newOrderTwo',
        payload: {
          newOrderTwoType: orderType,
          newOrderTwoYear: date
        }
      }).then(()=>{
        this.setState({loading : false});
      }).catch((e) => {
        message.error(e.message);
        this.setState({loading : false});
      });
    } else if (this.state.slipRadioValue === 4) {
      this.props.dispatch({
        type: 'searchlist/newOrderTwo',
        payload: {
          newOrderTwoType: orderType,
          newOrderTwoMonth: date
        }
      }).then(()=>{
        this.setState({loading : false});
      }).catch((e) => {
        message.error(e.message);
        this.setState({loading : false});
      });
    }
  }

  dayMonthOnChange = e => {
    this.props.form.setFieldsValue({dayMonthTotal:[]});
    // if (e.target.value === '1') {
    //   this.setState({checkType02: e.target.value,dayMonthTotalFlag:'1'});
    // } else {
    //   this.setState({checkType02: e.target.value,dayMonthTotalFlag:'2'});
    // }
    this.spinToggle(2,true);
    this.dayMonthTotal(this.state.dayMonthTotal,e.target.value);
  }

  selChange = e => {
      this.setState({selFlag: e.target.value});
  }

  // onChangeDayMonthTotal = (val) => {
  //   this.spinToggle(2,true);
  //   this.dayMonthTotal(val,this.state.checkType02);
  // }

  onChangeMonthOrDay = (val) => {
    this.spinToggle(3,true);
    if (this.state.radioValue === 1) {
      this.orderCount(this.state.checkType03,val+'-'+'01'+'-'+'01')
    } else {
      this.orderCount(this.state.checkType03,val.format("YYYY-MM-DD"))
    } 
  }

  onChangeNewSlipInfor = (val) => {
    this.spinToggle(4,true);
    this.newSlipInfor(val)
  }

  onChangeNewSlipIndex = (val) => {
     this.spinToggle(5,true);
    this.newSlipIndex(val);
  }

  onChangeOrderType = (val) => {
    this.spinToggle(6,true);
    this.orderTwoData(this.state.orderDate,val)
  }

  onChangeCaliberCode = (val) => {
    this.spinToggle(7,true);
    this.caliberCode(val);
  }

   onChangeRadio = (e) => {
    if (e.target.value === 1) {
      this.setState({
        checkType03: 31,
        radioValue: e.target.value,
        dateFlag: '1',
        orderOneYear: '',
      });
    } else {
      this.setState({
        checkType03: 32,
        radioValue: e.target.value,
        dateFlag: '2'
      });
    }
  }

  onChangenewSlipRadio = (e) => {
    this.setState({slipRadioValue: e.target.value});
    this.props.form.setFieldsValue({orderType:''})
  }

  onChangeYearSlip = (val) => {
    this.setState({orderDate:val+'-'+'01'+'-'+'01'});
    this.props.form.setFieldsValue({orderType:''})
  }

  onChangeMonthSlip = (val) => {
    this.setState({orderDate: val.format("YYYY-MM-DD")});
    this.props.form.setFieldsValue({orderType:''})
  }
 
  render() {
    const { getFieldDecorator } = this.props.form;
    const {loading, searchlist:
        {
        totalCountData,
        dayMonthtotalCountData,
        orderCountData,
        newSlipInforData,
        newSlipIndexData,
        caliberChangeRateData,
        newSlipIndexPieData,
      }
    } = this.props;

    return (
      <PageHeaderLayout title="">
        <Spin spinning={this.state.containLoading}>
        <Card bordered={false}>
          {/* 总业绩---年月日 */}
          <TotalCount totalCountData={totalCountData}/>
          <Row style={{marginTop: 10}}>
            {/* 业绩情况---月日 */}
            <div className={styles.NewSlipIndexLeftDiv}>
              <div className={styles.newSlipDiv} style={{width: "60%"}}>
                  <div className={styles.newSlipTitleDiv}>
                    <div className={styles.newSlipTitleDes}><span>业绩情况</span></div>
                  </div>
                  <Spin spinning={this.state.contentNo==2 ? this.state.loading : false}>
                  <div style={{margin: 12}}>
                    <RadioGroup defaultValue="1" onChange={this.dayMonthOnChange}>
                      <RadioButton value="1">每日</RadioButton>
                      <RadioButton value="2">每月</RadioButton>
                    </RadioGroup>
                  </div>
                  <div className={styles.newSlipEchartsDiv}>
                    <DayMonthTotalCountEcharts 
                      dayMonthTotalCount={dayMonthtotalCountData} 
                      dayMonthTotalFlag={this.state.dayMonthTotalFlag}/>
                  </div>
                  </Spin>
              </div>
              {/* 业绩情况---项目组业绩排行榜*/}
               <div className={styles.NewSlipIndexRightDiv}>
                  <div className={styles.newSlipTitleDiv}>
                    <div style={{margin: "10px 12px"}}>
                      <span style={{fontSize: 16,color:'rgba(0, 0, 0, 0.85)'}}>项目组业绩排行榜</span>
                       <div>
                        <RadioGroup onChange={this.onChangeRadio} value={this.state.radioValue}>
                          <Radio value={1}>年份选择</Radio>
                          <Radio value={2}>月份选择</Radio>
                        </RadioGroup>
                        {
                          this.state.radioValue === 1 
                            ?  <span>{getFieldDecorator('orderOneYear',{initialValue: this.state.orderOneYear})(
                                  <Select placeholder="请选择" style={{ width: '50%' }} onChange={this.onChangeMonthOrDay}>
                                    {getSelectChild('pyear')}
                                  </Select>
                                )}</span>
                            : <MonthPicker style={{ width: '50%' }} 
                                placeholder='请选择月份' 
                                onChange={this.onChangeMonthOrDay}/>
                        }
                      </div>
                    </div>
                  </div>
                  <Spin spinning={this.state.contentNo==3 ? this.state.loading : false}>
                    <div className={styles.newSlipEchartsDiv} style={{marginTop: 6}}>
                      <OrderCountTable orderCountData={orderCountData} dateFlag={this.state.dateFlag} />
                    </div>
                  </Spin>
              </div> 
              </div>
          </Row>
          {/* 新单情况*/}
          <Row style={{marginTop: 10}}>
            <div className={styles.newSlipDiv}>
              <div className={styles.newSlipTitleDiv}>
                <div className={styles.newSlipTitleDes}><span>新单情况</span></div>
                <div className={styles.newSlipTitleSel}>
                  <span>项目组名称：</span>
                  <Select  placeholder="请选择" style={{ width: '60%' }} onChange={this.onChangeNewSlipInfor}>
                       {getSelectChild('extEnterpDesc')}
                  </Select>
                </div>
              </div>
              <Spin spinning={this.state.contentNo==4 ? this.state.loading : false}>
                <div className={styles.newSlipEchartsDiv}>
                    <LeftNewSlipInforEcharts newSlipInforData={newSlipInforData} />
                    <NewSlipInforEcharts newSlipInforData={newSlipInforData} />
                </div>
              </Spin>
            </div>
          </Row>
          <Row style={{marginTop: 10}}>
            {/* 新单运营指标*/}
            <div className={styles.NewSlipIndexLeftDiv}>
              <div className={styles.newSlipDiv} style={{width: "60%"}}>
                  <div className={styles.newSlipTitleDiv}>
                    <div className={styles.newSlipTitleDes}><span>新单运营指标</span></div>
                    <div className={styles.newSlipTitleSel}>
                      <span>项目组名称：</span>
                      <Select  placeholder="请选择" style={{ width: '60%' }} onChange={this.onChangeNewSlipIndex}>
                          {getSelectChild('extEnterpDesc')}
                      </Select>
                    </div>
                  </div>
                  <Spin spinning={this.state.contentNo==5 ? this.state.loading : false}>
                    <div className={styles.newSlipEchartsDiv}>
                      <NewSlipIndexEcharts newSlipIndexData={newSlipIndexData}/>
                    </div>
                  </Spin>
              </div>
              {/* 新单运营指标---项目组业绩排行榜*/}
              <div style={{width: '38%', border: '1px solid #eee'}}>
                <div className={styles.newSlipTitleDiv}>
                  <div style={{margin: "10px 12px"}}>
                    <span style={{fontSize: 16,color:'rgba(0, 0, 0, 0.85)'}}>项目组业绩排行榜</span>
                    <div>
                      <RadioGroup onChange={this.onChangenewSlipRadio} value={this.state.slipRadioValue}>
                        <Radio value={3}>年份选择</Radio>
                        <Radio value={4}>月份选择</Radio>
                      </RadioGroup>
                      {
                        this.state.slipRadioValue === 3 
                          ?  <Select placeholder="请选择" style={{ width: '50%' }} onChange={this.onChangeYearSlip} defaultValue={new Date().getFullYear()}>
                              {getSelectChild('pyear')}
                            </Select>
                          : <MonthPicker style={{ width: '50%' }} 
                              placeholder='请选择月份' 
                              onChange={this.onChangeMonthSlip}/>
                      }
                    </div> 
                    <div style={{textAlign:'right',marginTop: 6}}>
                      <span style={{marginRight: "3%"}}>新单指标:</span>
                      {getFieldDecorator('orderType',{initialValue: this.state.orderType})(
                      <Select  placeholder="请选择" style={{ width: '50%',marginRight: "4%" }} onChange={this.onChangeOrderType}>
                        <Option value={1}>每日线上成交件数</Option>
                        <Option value={2}>撤件件数</Option>
                        <Option value={3}>犹退件数</Option>
                        <Option value={4}>首期扣款成功件数</Option>
                      </Select>
                      )}
                    </div> 
                  </div>
                  <Spin spinning={this.state.contentNo==6 ? this.state.loading : false}>
                    <div style={{borderTop: '1px solid #eee'}}>
                      <NewSlipIndexPieEcharts newSlipIndexPieData={newSlipIndexPieData} />
                    </div>
                  </Spin>  
                </div>
              </div>
            </div>
          </Row>
          {/*业绩转换率*/}
          <Row style={{marginTop: 10}}>
            <div className={styles.NewSlipIndexLeftDiv}>
              <div className={styles.newSlipDiv} style={{width: "60%"}}>
                   <div className={styles.newSlipTitleDiv}>
                    <div className={styles.newSlipTitleDes}><span>业绩转换率</span></div>
                    <div className={styles.newSlipTitleSel}>
                      <span>项目组名称：</span>
                      {getFieldDecorator('caliberCode',{initialValue:''})(
                      <Select  placeholder="请选择" style={{ width: '60%' }} onChange={this.onChangeCaliberCode}>
                          {getSelectChild('extEnterpDesc')}
                      </Select>
                      )}
                    </div>
                  </div> 
                  <Spin spinning={this.state.contentNo==7 ? this.state.loading : false}>
                    <div className={styles.newSlipEchartsDiv}>
                      <CaliberChangeRateEcharts caliberChangeRateData={caliberChangeRateData}/>
                    </div>
                  </Spin>
              </div>
            </div>
          </Row>
        </Card>
        </Spin>
      </PageHeaderLayout>
    );
  }
}
