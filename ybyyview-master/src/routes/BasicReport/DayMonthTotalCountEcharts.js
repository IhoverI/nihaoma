import React, { PureComponent, Fragment } from 'react';
import echarts from 'echarts/lib/echarts'; // 必须
import Line from './Line'

export default class DayMonthTotalCountEcharts extends PureComponent {

  componentWillReceiveProps(nextProps) {
    const {dayMonthTotalCount} = nextProps
    const xList = [];
    const extEnterpCodeTotalData = [];
    const legend = [];
    const values = [];
      if (dayMonthTotalCount) {
        const extEnterpCode = dayMonthTotalCount[0];
        const extEnterpCodeTotal = dayMonthTotalCount[1];
        if (extEnterpCodeTotal) {
          for (const i in extEnterpCodeTotal) {
           if (extEnterpCodeTotal[i][0]) {
             xList.push(extEnterpCodeTotal[i][0])
             extEnterpCodeTotalData.push(extEnterpCodeTotal[i][2])
           }
          }
          legend.push(extEnterpCodeTotal[0][1])
        }
        
        values.push(extEnterpCodeTotalData);

        if (extEnterpCode) {
          for (const i in extEnterpCode) {
           if (extEnterpCode[i]) {
             legend.push(extEnterpCode[i][0][1])
             const dataArr = [];
             for (const j in extEnterpCode[i]) {
               if (extEnterpCode[i][j]) {
                  dataArr.push(extEnterpCode[i][j][2])
               }
             }
              values.push(dataArr)
           }
          }
        }
      }
    const Setting = {
      xList,
      data: values,
      // legend,
      colors: ['#8618ffc4'],
      danwei: '万',
      lable: legend,
      width: 540,
    };
    const LineOption = new Line(Setting);
    const lineEcharts = echarts.init(window.document.getElementById('DayMonthTotalCountEcharts'));
    lineEcharts.setOption(LineOption);
  }

  render() {
    return (
      <div id='DayMonthTotalCountEcharts' style={{width: '100%',minHeight: '320px'}} />
    );
  }
}
