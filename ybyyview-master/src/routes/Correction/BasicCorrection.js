import React, { PureComponent, Fragment } from 'react';
import {
  Checkbox,
  Button,
  Form,
  Icon,
  Col,
  Row,
  DatePicker,
  Divider,
  Input,
  Select,
  Popover,
  Collapse,
  Badge,
  Avatar,
  message,
  Modal,
  Upload,
  Radio,
  Popconfirm,
  Tooltip,
} from 'snk-web';

import { connect } from 'dva';
import FooterToolbar from 'components/FooterToolbar';
import SingleShouYiRen from '../../components/SingleShouYiRen/SingleShouYiRen';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './BasicCorrection.less';
import { getDataSource, timestampToString, getUrlParam, getSelectChild,CheckIdCard } from '../../utils/utils'
import { withRouter } from 'dva/router';
import moment from 'moment';

const { Option } = Select;
const { RangePicker } = DatePicker;
const  Panel  = Collapse.Panel;
const { TextArea } = Input;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

const labelSource = getDataSource('labelSource');

 @connect(({ profile, loading, selfchannelForm }) => ({
    profile,
    selfchannelForm,
    loading: loading.models.profile,
  }))
  @Form.create()
export default class BasicCorrection extends PureComponent {
  constructor(props){
    super(props);
    const { profile: { queryData = {} } } = props;
    this.state = {
      width: '100%',
      cAppNo: '',  
      dataSource: {},  
      onlyRead: '1',
      signTm: '',
      insrncBorth: '',
      cAppBorth: '',
      uploadFlag: '0',
      fileList: [],
      uploading: false,
      cPlyNo: props.location.cPlyNo || getUrlParam(props.location.search).cPlyNo,
      editType: '1',
      sureCommit:false,
      cAppSexFlag: '',
      SingleShouYiRenList: [],
      isBenfUpFlag: false, // 受益人信息是否要更改，默认不更改；
    };
  }

  componentWillMount(){
    if(this.props.history.location.state){
      this.props.history.push({state: undefined});
    } else{
      Object.keys(this.props.profile).forEach(key => delete this.props.profile[key]);
    }
  }
  
  componentDidMount() {
    if (this.state.cPlyNo) {
      this.props.dispatch({
        type: 'selfchannelForm/checkCodeType',
      });
       this.props.dispatch({
        type: 'profile/query',
        payload: {
          cPlyNo: this.state.cPlyNo
        },
      }).then((response) => { 
        const { data: { bnfDto = {}, identifyLongValidA, identifyLongValidI } } = response; 
        const { bnfDtoList, benfType } = bnfDto; 
        this.setState({ 
          relationIsSelf: benfType !== '1' ? true: false, // 受益人是法定0,true， 是指定1 false;
          SingleShouYiRenList: bnfDtoList,
          isTLongLess: identifyLongValidA === '1' ? true : false,
          isILongLess: identifyLongValidI === '1' ? true : false,
        });
      });
    } else {
     
    }
  }

  resetForm = () => {
    this.props.form.resetFields();
    this.setState({
      cAppNo: '',  
      dataSource: {},  
      onlyRead: '1',
      signTm: '',
      insrncBorth: '',
      cAppBorth: '',
      uploadFlag: '0',
      fileList: [],
      uploading: false,
      editType: '1',
      sureCommit:false,
      SingleShouYiRenList: [],
      }
    );
  }

  onChangeEditType = (e) => {
    if (e.target.value === '0') {
      this.setState({
        onlyRead: '0',
        editType: "0",
      });
    } else if (e.target.value === '1'){
      this.setState({
        onlyRead: '1',
        editType: "1",
      });
    } 
  }

  onCancel = () => {
    this.setState({sureCommit: false});
  }

  onConfirm = () => {
    const {form } = this.props;
    const { fileList } = this.state;
    form.validateFields((err, fieldsValue) => { 
      // if (fieldsValue.postAddress !== '') {
      //   if (/^(?=([\u4e00-\u9fa5].*){5})/.test(fieldsValue.postAddress) === false) {
      //     message.warn(`[客户要求投递地址]格式有误，不少于5个汉字`);
      //   }
      // }
      // if (fieldsValue.cAppAddr !== '') {
      //   if (/^(?=([\u4e00-\u9fa5].*){5})/.test(fieldsValue.cAppAddr) === false) {
      //     message.warn(`[投保人家庭地址]格式有误，不少于5个汉字`);
      //   }
      // }
      // if (fieldsValue.insrncAddress !== '') {
      //   if (/^(?=([\u4e00-\u9fa5].*){5})/.test(fieldsValue.insrncAddress) === false) {
      //     message.warn(`[被保人家庭地址]格式有误，不少于5个汉字`);
      //   }
      // }
      // if (fieldsValue.insrncComp !== '') {
      //   if (/^(?=([\u4e00-\u9fa5].*){5})/.test(fieldsValue.insrncComp) === false) {
      //     message.warn(`[被保人工作单位地址]格式有误，不少于5个汉字`);
      //   }
      // }

      let cAppSex,insrncSex,cElcFlag;
      if (fieldsValue.cAppSex  === '男') {
        cAppSex = '01'
      } else if (fieldsValue.cAppSex  === '女') {
        cAppSex = '02'
      } else {
         cAppSex = fieldsValue.cAppSex;
      }
      
      if (fieldsValue.insrncSex  === '男') {
        insrncSex = '01'
      } else if (fieldsValue.insrncSex  === '女') {
        insrncSex = '02'
      } else {
         insrncSex = fieldsValue.insrncSex;
      }

      // 投保人证件日期规则，1表示长期， 0表示有止期
      if (!fieldsValue.identifyExpiryA && !fieldsValue.identifyLongValidA) {
        message.warning('请选择投保人证件有效期');
        return false;
      }
      if (!fieldsValue.identifyLongValidA) {
        fieldsValue.identifyLongValidA = '0'; // 证件是否长期， 1表示长期， 0表示有止期
        fieldsValue.identifyExpiryA = fieldsValue.identifyExpiryA.format("YYYY-MM-DD"); // 证件是否长期， 1表示长期， 0表示有止期
      } else {
        fieldsValue.identifyLongValidA = '1'; // 证件是否长期， 0表示长期， 1表示有止期
        fieldsValue.identifyExpiryA = '9999-12-30';
      };
      // 被保人证件日期规则，1表示长期， 0表示有止期
      if (!fieldsValue.identifyExpiryI && !fieldsValue.identifyLongValidI) {
        message.warning('请选择被保人证件有效期');
        return false;
      }
      if (!fieldsValue.identifyLongValidI) {
        fieldsValue.identifyLongValidI = '0'; // 证件是否长期， 1表示长期， 0表示有止期
        fieldsValue.identifyExpiryI = fieldsValue.identifyExpiryI.format("YYYY-MM-DD"); // 证件是否长期， 1表示长期， 0表示有止期
      } else {
        // 证件无止期 identifyLongValidB为0,；
        fieldsValue.identifyLongValidI = '1'; // 证件是否长期， 0表示长期， 1表示有止期
        fieldsValue.identifyExpiryI = '9999-12-30';
      };

      /**
       * 别跟我说什么逻辑严密
       * 别跟我谈什么底层架构
       * 老夫敲代码就是一把梭
       * 复制粘贴拿起键盘就干
       */
      if(fieldsValue.cElcFlag.indexOf('纸')>=0 && fieldsValue.cElcFlag.indexOf('+')<0){
        cElcFlag = '0'
      } else if(fieldsValue.cElcFlag.indexOf('电')>=0 && fieldsValue.cElcFlag.indexOf('纸')<0){
        cElcFlag = '1'
      } else if(fieldsValue.cElcFlag.indexOf('+')>=0){
        cElcFlag = '2'
      } else{
        cElcFlag = '3'
      }

      let signTm;
      let insrncBorth;
      let cAppBorth;
      let cardId;
      if (fieldsValue) {
        insrncBorth = fieldsValue.insrncBorth === '' ? '' : fieldsValue.insrncBorth.format("YYYY-MM-DD");
        cAppBorth = fieldsValue.cAppBorth === '' ? '' : fieldsValue.cAppBorth.format("YYYY-MM-DD");
        cardId = fieldsValue.cardId === '' ? '' : fieldsValue.cardId.replace(/\s+/g, "");
      }

      const selInsrncBorth = this.state.insrncBorth === '' ? insrncBorth : this.state.insrncBorth;
      const selCAppBorth = this.state.cAppBorth === '' ? cAppBorth : this.state.cAppBorth;

      if (this.state.onlyRead === '0') {
         const p = {
            cPlyNo: fieldsValue.cPlyNo,
            flag: 0,
            cardId: cardId,
            endorText: fieldsValue.endorText,
            ...this.state.fileList
        };
        this.props.dispatch({
          type: 'profile/updateCardId',
          payload: p,
        }).then(()=>{
        }).catch((e)=>{
          message.error(e.message || '网络异常');
        });
        this.setState({ fileList: [], sureCommit: false });
      } else {
        let beneficiaryList = [];
        if (this.props.form.getFieldValue('benfType') === '0') {
          beneficiaryList = [{ insrncRelation: '法定', benfClntNmeB: '法定' }];
        }
        const p = {
          ...fieldsValue,
          insrncBorth: selInsrncBorth,
          cAppBorth: selCAppBorth,
          cAppSex,
          insrncSex,
          cElcFlag,
          beneficiaryList,
          benfUpFlag: this.state.isBenfUpFlag ? '1' : '0',
          ...this.state.fileList
        };
        this.props.dispatch({
          type: 'profile/apply',
          payload: p,
        }).then(()=>{
        }).catch((e)=>{
          message.error(e.message || '网络异常');
        });
        this.setState({fileList: [],sureCommit:false});
      }
    })
  }

  makeSure = e => {
    this.setState({sureCommit:true});
  }

  format = (value,code) => {
    let text = '';
    if (code === 'insrncSex' || code === 'cAppSex') {
      switch (value) {
        case "01": return "男"; break;
        case "02": return "女"; break;
        default:break;
      }
    } else if (code === 'relation') {
      switch (value) {
        case "601004": return "配偶"; break;
        case "601002": return "子女"; break;
        case "601003": return "父母"; break;
        case "601012": return "兄弟姐妹"; break;
        case "601006": return "本人"; break;
        default: return value; break;
      }
    }
  }

  handleSearch = () => {
    const cPlyNo = this.props.form.getFieldValue('cPlyNo');
    const regMessage = /^[A-Za-z0-9]{4,30}$/;
    regMessage.test(cPlyNo) ? '' : message.warning('请输入正确保单号');

    const searchTerm = {
      cPlyNo,
    };
    this.props.dispatch({
      type: 'profile/query',
      payload: searchTerm,
    }).then((response) => {
      const { data: { bnfDto = {}, identifyLongValidA, identifyLongValidI } } = response; 
      const { bnfDtoList, benfType } = bnfDto; 
      this.setState({ 
        relationIsSelf: benfType !== '1' ? true: false, // 受益人是法定0,true， 是指定1 false;
        SingleShouYiRenList: bnfDtoList,
        isTLongLess: identifyLongValidA === '1' ? true : false,
        isILongLess: identifyLongValidI === '1' ? true : false,
      });
    });
    this.setState({
      cAppNo: cPlyNo,
      uploadFlag: '1',
    });
    this.props.form.resetFields();
    this.props.form.setFieldsValue({cPlyNo:cPlyNo});

  };

  selSignTm = (val) => {
    this.setState({
      signTm: val.format("YYYY-MM-DD"),  
    });
  }


  selInsrncBorth = (val) => {
    this.setState({
      insrncBorth: val.format("YYYY-MM-DD"),
    });
  }

  selCAppBorth  = (val) => {
     this.setState({
      cAppBorth: val.format("YYYY-MM-DD"),
    });
  }

  cAppCertfType = (val) => {
    setTimeout(()=>{
      this.props.form.validateFields(['cAppCertfNo'], { force: true });
    },300);
  }
  insrncCardTypeChange = (val)=>{
    setTimeout(()=>{
      this.props.form.validateFields(['insrncCardId'], { force: true });
    },300);
  }

  validatorIDCard(rule,value,callback,fieldKey){
    if(value===''){
      callback();
      return;
    }
    var re = CheckIdCard(value);
    if(fieldKey==='cAppCertfNo'){
      const cAppCertfType = this.props.form.getFieldValue("cAppCertfType");
      if(re!=='success'&&cAppCertfType==='01'){
        callback(re);
      }
    }
    if(fieldKey==='insrncCardId'){
      const insrncCardType = this.props.form.getFieldValue("insrncCardType");
      if(re!=='success'&&insrncCardType==='01'){
        callback(re);
      }
    }
    callback();
  }

  formaUnit = (val,code) => {
    if (code === 'cElcFlag') {
      let text = '';
      switch (val) {
        case '0': text = '纸质保单'; break;
        case '1': text = '电子保单'; break;
        case '2': text = '电子+纸质'; break;
        case '3': text = '纸质保单'; break;
        case '4': text = '纸质转电子'; break;
        default: break;
      }
        return text;
    }
  }

  onChangecAppSexFlag = (val) => {}

  // 受益方式发生改变
  BenfTypeChange(value){
    if (value === '1') {
      this.props.form.setFieldsValue({ insrncRelation: [] });
      this.setState({
        relationIsSelf: false,
      });
      if (!this.state.SingleShouYiRenList.length) {
        this.addBeneficiary();
      }
    } else {
      this.setState({
        relationIsSelf: true,
        // SingleShouYiRenList: [],
      });
      this.props.form.setFieldsValue({ insrncRelation: undefined });
    }
  }
  //multiple

  addBeneficiary = () => {
    // 受益人最多为5人
    if (this.state.SingleShouYiRenList.length < 5) {
      this.setState((state) => ({
        SingleShouYiRenList: [...state.SingleShouYiRenList, {}],
      }));
    } else {
      message.warning('受益人最多为5人', 5);
    }
  }

  //  确认添加
  confirmAddBeneficiary = (data, index) => {
    const oldArr = this.state.SingleShouYiRenList;
    oldArr.pop();
    this.setState({
      SingleShouYiRenList: [...oldArr, data],
    });
  }
  // 删除受益人
  DeleteBeneificiary = (index) => {
    const newBeneificiary = this.state.SingleShouYiRenList.filter((item, i) => i + 1 !== index );
    this.setState({
      SingleShouYiRenList: [...newBeneificiary],
    });
    this.props.dispatch({
      type: 'selfchannelForm/deletebeneficiaryList',
      payload: {
        index,
      }
    })
  }

  // 确认批改受益人信息
  confirmReCorrect = () => {
    this.setState({
      // relationIsSelf: true,
      isBenfUpFlag: true,
      // SingleShouYiRenList: [],
    });
    // this.props.dispatch({
    //   type: 'selfchannelForm/save',
    //   payload: {
    //     beneficiaryList: {}
    //   }
    // })
  }

  renderSelectOption = (data) => {
    return data.map(item => (
        <Option key={item.code} value={item.code}>{item.codeName || item.name}</Option>
    ));
  }

  render() {
    let cPlyNo = this.state.cPlyNo;

    const dateFormat = 'YYYY-MM-DD';
    const { profile: { queryData  }, form, loading, selfchannelForm } = this.props;
    const { getFieldDecorator } = form;
    const baseProfile = queryData == null ? {} : queryData;
    const queryDatas = queryData ||{};
    let isChange = {};
    if(queryData){
      if (queryDatas.isChange !== undefined || queryDatas.isChange !== null) {
        isChange = queryDatas.isChange || {};
      }
    }
    const {
      CountryCode,
      ItemAccOccupationCode,
      relationBetweenCode,
    } = selfchannelForm;
    const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    };

    const prams = {
      showUploadList:true,
      action: '',
      multiple:true,
      name: 'file',
      onRemove: (file) => {
        this.setState(({ fileList }) => {
          const index = fileList.indexOf(file);
          const newFileList = fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: (file) => {
        const isXls = file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        if (isXls) {
          message.error('excel上传无法支持类型xlsx，支持xls。');
          return;
        } else{
          this.setState(({ fileList }) => ({
            fileList: fileList.concat(file),
          }));
          return false;
        }
        
      },
      fileList: this.state.fileList,
    }
    let proName;
    if (baseProfile !== undefined) {
      if (getSelectChild('prodDesc')) {
        for (const i in getSelectChild('prodDesc')) {
          if (getSelectChild('prodDesc')[i].props.value === baseProfile.prodNo) {
            proName = getSelectChild('prodDesc')[i].props.children || '';
          }
        }
      }
    }
    
     let extEnterName;
     if (baseProfile !== undefined) {
      if (getSelectChild('extEnterpDesc')) {
        for (const i in getSelectChild('extEnterpDesc')) {
          if (getSelectChild('extEnterpDesc')[i].props.value === baseProfile.extEnterpCode) {
            extEnterName = getSelectChild('extEnterpDesc')[i].props.children || '';
          }
        }
      }
    }
    return (
      <PageHeaderLayout 
        title="" 
        wrapperClassName={styles.basicCorrection} 
      >
      <div>
        <div style={{margin: "14px 0", textAlign: "center"}}>
          {getFieldDecorator('selType', {initialValue: this.state.editType})(
          <RadioGroup onChange={this.onChangeEditType}>
            <Radio value='0'>只修改扣款卡号</Radio>
            <Radio value='1'>修改其他信息</Radio>
          </RadioGroup>
          )}
            {getFieldDecorator('cPlyNo', {initialValue: cPlyNo ? cPlyNo : ''})(<Input style={{ width: '30%' }} placeholder="请输入保单号" />)}
            <Button type="primary" style={{ marginLeft: 8 }} onClick={this.handleSearch} loading={loading}>
              查询
            </Button> 
        </div>
        </div>
        <Form 
          layout="inline" 
          className='custom-form-wrapper'
        >  
          <Collapse defaultActiveKey={['1','2','3','4','5']}>
            {/* 保单信息列表 */}
            <Panel header={labelSource.policyInfo} key="1">
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='保单号'>
                     {getFieldDecorator('cPlyNo',{initialValue: baseProfile ? (baseProfile.cPlyNo ? baseProfile.cPlyNo : '') :''})(
                    <Input
                      value={undefined === baseProfile ? '' : baseProfile.cPlyNo}
                      disabled
                    /> 
                     )}                 
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='原始保单号'>
                    {getFieldDecorator('cPlyNoOld',{initialValue: baseProfile ? (baseProfile.cPlyNoOld ? baseProfile.cPlyNoOld : '') :''})(
                      <Input disabled />
                )}  
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.transationCode}>
                    {getFieldDecorator('transationCode',{initialValue: baseProfile ? (baseProfile.transationCode ? baseProfile.transationCode : '') : ''})(
                      <Input disabled />
                  )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.prodCName}>
                    {getFieldDecorator('prodNo',{initialValue: baseProfile ? (baseProfile.prodNo !== undefined ? baseProfile.prodNo : '') : ''})(
                      <Input disabled />
                  )}
                  </Form.Item>
                </Col>
                  <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.renewalTime}>
                    {getFieldDecorator('renwalNumber',{initialValue: baseProfile ? (baseProfile.renwalNumber ? baseProfile.renwalNumber : '') : ''})(
                      <Input placeholder='' disabled/>
                  )}
                  </Form.Item>
                </Col> 
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.productPlan}>
                    {getFieldDecorator('feeType',{initialValue: baseProfile ? (baseProfile.feeType ? baseProfile.feeType : '') : ''})(
                    <Input disabled />
                     )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.payKind}>
                    {getFieldDecorator('payWay',{initialValue: baseProfile ? (baseProfile.payWay ? baseProfile.payWay : '') : ''})(
                      <Input disabled />
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.nGetPrm}>
                    {getFieldDecorator('prm',{initialValue: baseProfile ? (baseProfile.prm ? baseProfile.prm : '') : ''})(
                      <Input disabled />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.tInsrncTm}>
                    {getFieldDecorator('insrncDate',{initialValue: baseProfile ? (baseProfile.insrncDate ? baseProfile.insrncDate : '') : ''})(
                      <Input disabled />)}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.cardId}>
                    {getFieldDecorator('cardId',{initialValue: baseProfile ? (baseProfile.cardId ? baseProfile.cardId : '') : ''})(
                      <Input disabled={!(this.state.onlyRead === '0')} 
                      style={{color: this.props.form.getFieldValue('cardId') === (baseProfile ? baseProfile.cardId : '') && (isChange.cardId ? (isChange.cardId === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='保单类型'>
                    {getFieldDecorator('cElcFlag',{initialValue: baseProfile ?　(baseProfile.cElcFlag ? this.formaUnit(baseProfile.cElcFlag,'cElcFlag') : '') : ''})(
                      <Input disabled />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='业务渠道'>
                    {getFieldDecorator('extEnterpCode',{initialValue: baseProfile ? (baseProfile.extEnterpCode !== undefined ? baseProfile.extEnterpCode : '') : ''})(
                      <Input disabled />
                  )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                  <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保单号'>
                    {getFieldDecorator('cPlyAppNo',{initialValue: baseProfile ? (baseProfile.cPlyAppNo　? baseProfile.cPlyAppNo : '') : ''})(
                      <Input disabled />
                  )}
                  </Form.Item>
                </Col> 
                <Col md={16} lg={16} sm={16}>
                  <Form.Item label={labelSource.postAddress}>
                    {getFieldDecorator('postAddress',{initialValue: baseProfile ? (baseProfile.postAddress ? baseProfile.postAddress : '') : ''})(
                      <Input disabled={!(this.state.onlyRead === '1')} 
                        style={{color: this.props.form.getFieldValue('postAddress') === (baseProfile ? baseProfile.postAddress : '') && (isChange.postAddress ? (isChange.postAddress === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
                  )}
                  </Form.Item>
                </Col>
              </Row>
            </Panel>
            {/* 投保人信息列表 */}
            <Panel header={labelSource.applyInfo} key="2">
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.applyClntNmeA}>
                    {getFieldDecorator('cAppName',{initialValue: baseProfile ? (baseProfile.cAppName ? baseProfile.cAppName : '') : ''})(
                      <Input  disabled={!(this.state.onlyRead === '1')} 
                        style={{color: this.props.form.getFieldValue('cAppName') === (baseProfile ? baseProfile.cAppName : '') && (isChange.cAppName ? (isChange.cAppName === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.applySex}>
                    {getFieldDecorator('cAppSex',{initialValue: baseProfile ? (baseProfile.cAppSex ? this.format(baseProfile.cAppSex,"cAppSex") : '') : ''})(
                       <Select placeholder="请选择" 
                       style={{ width: '100%'}}
                       disabled={!(this.state.onlyRead === '1')}
                       onChange={this.onChangecAppSexFlag}
                       >
                        <Option value='01'>男</Option>
                        <Option value='02'>女</Option>
                      </Select>
                  )} 
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.appBirth}>
                    {getFieldDecorator('cAppBorth',{
                        initialValue: baseProfile.cAppBorth ? moment(baseProfile.cAppBorth, dateFormat) : '',
                    })(
                        <DatePicker
                          disabled={!(this.state.onlyRead === '1')}
                          format={dateFormat} 
                          onChange={this.selCAppBorth}
                          style={{ width: '100%' }}
                        />
                    )} 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.applyCertfCls}>
                    {getFieldDecorator('cAppCertfType',{initialValue: baseProfile ? (baseProfile.cAppCertfType ? baseProfile.cAppCertfType : '') : ''})(
                      <Select placeholder="请选择" style={{ width: '100%' }} disabled={!(this.state.onlyRead === '1')} onChange={this.cAppCertfType.bind(this)}>
                        {getSelectChild('certiDesc')}
                      </Select>
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item  label={labelSource.applyCertfCde}>
                    {getFieldDecorator('cAppCertfNo',{
                      // rules:[{ validator: (rule,value,callback)=>{this.validatorIDCard(rule,value,callback,'cAppCertfNo')}}],
                      initialValue: baseProfile ? (baseProfile.cAppCertfNo ? baseProfile.cAppCertfNo : '') : ''})(
                      <Input  disabled={!(this.state.onlyRead === '1')} 
                        style={{color: this.props.form.getFieldValue('cAppCertfNo') === (baseProfile ? baseProfile.cAppCertfNo : '') && (isChange.cAppCertfNo ? (isChange.cAppCertfNo === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
                )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.applyMobile}>
                    {getFieldDecorator('cAppPhone',{
                      // rules: [{
                      //   pattern: /^[1][3,4,5,7,8][0-9]{9}$/, message: '号码格式有误',
                      // }],
                      initialValue: baseProfile ? (baseProfile.cAppPhone ? baseProfile.cAppPhone : '') : ''})(
                      <Input disabled={!(this.state.onlyRead === '1')} 
                          style={{color: this.props.form.getFieldValue('cAppPhone') === (baseProfile ? baseProfile.cAppPhone : '') && (isChange.cAppPhone ? (isChange.cAppPhone === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={8}>
                  <Form.Item label="证件有效期">
                    {getFieldDecorator('identifyLongValidA', {
                      rules: [{ required: true, message: '证件有效期必填!' }],
                      valuePropName: 'checked',
                      initialValue:  baseProfile.identifyLongValidA === '0' ? false : true,
                    })(
                      <Checkbox onChange={() => this.setState((state) => ({
                        isTLongLess: !state.isTLongLess,
                      }))}>长期</Checkbox>
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={8}>
                	<Form.Item label="证件有效止期">  
                    {getFieldDecorator('identifyExpiryA', {
                        initialValue: baseProfile.identifyExpiryA ? moment(baseProfile.identifyExpiryA, dateFormat) : '',
                    })(
                      <DatePicker disabled={this.state.isTLongLess} allowClear={true} style={{ width: '100%' }} />
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={8}>
                    <Form.Item label="国籍">
                    {getFieldDecorator('countryA',{
                      initialValue: baseProfile.countryA || '1010090156',
                      rules: [{ required: true, message: '国籍必填!' }],
                    })(
                      <Select optionFilterProp="children" showSearch placeholder="请选择" style={{ width: '100%' }}>
                        {this.renderSelectOption(CountryCode)}
                      </Select>
                    )}
                    </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.applyZipCde}>
                    {getFieldDecorator('cAppZip',{initialValue: baseProfile ? (baseProfile.cAppZip ? baseProfile.cAppZip : '') : ''})(
                      <Input disabled />
                )}                
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.applyTel}>
                    {getFieldDecorator('applyTel',{
                      initialValue: baseProfile ? (baseProfile.applyTel ? baseProfile.applyTel : '') : ''}
                    )(<Input disabled />)}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.applyMail}>
                    {getFieldDecorator('cEmail',{
                      // rules: [{
                      //   type: this.props.form.getFieldValue('cEmail') === (baseProfile ? baseProfile.cEmail : '') ? '' : 'email', message: '请输入正确的格式',
                      // }],
                      initialValue: baseProfile ? (baseProfile.cEmail ? baseProfile.cEmail : '') : ''},
                      )(
                        <Input disabled={!(this.state.onlyRead === '1')} 
                          style={{color: this.props.form.getFieldValue('cEmail') === (baseProfile ? baseProfile.cEmail : '') && (isChange.cEmail ? (isChange.cEmail === 0 ? true : false): true) ? '' : '#ff0000'}}
                        />
                )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.occupationCodeA}>
                    {getFieldDecorator('occupationCodeA',{
												initialValue: baseProfile.occupationCodeA,
                        rules: [{ required: true, message: '必填!' }],
                    })(
                      <Select optionFilterProp="children" showSearch placeholder="请选择" >
                        {this.renderSelectOption(ItemAccOccupationCode)}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col md={16} lg={16} sm={16}>
                  <Form.Item label={labelSource.applyClntAddr}>
                    {getFieldDecorator('cAppAddr',{initialValue: baseProfile ? (baseProfile.cAppAddr ? baseProfile.cAppAddr : '') : ''})(
                      <Input disabled={!(this.state.onlyRead === '1')} 
                      style={{color: this.props.form.getFieldValue('cAppAddr') === (baseProfile ? baseProfile.cAppAddr : '') && (isChange.cAppAddr ? (isChange.cAppAddr === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Panel>
            {/* 被保人列表 */}
            <Panel header={labelSource.insrncInfo} key="3">
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.insrncName}>
                    {getFieldDecorator('insrncName',{initialValue: baseProfile ? (baseProfile.insrncName ? baseProfile.insrncName : '') : ''})(
                      <Input disabled={!(this.state.onlyRead === '1')} 
                      style={{color: this.props.form.getFieldValue('insrncName') === (baseProfile ? baseProfile.insrncName : '') && (isChange.insrncName ? (isChange.insrncName === 0 ? true : false): true) ? '' : '#ff0000'}}
                    />
                )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.insrncSex}>
                    {getFieldDecorator('insrncSex',{initialValue: baseProfile ? (baseProfile.insrncSex ? this.format(baseProfile.insrncSex,"insrncSex") : '') : ''})(
                       <Select placeholder="请选择" style={{ width: '100%' }} disabled={!(this.state.onlyRead === '1')}>
                        <Option value='01'>男</Option>
                        <Option value='02'>女</Option>
                      </Select>
              )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.dealDate}>
                    {getFieldDecorator('insrncBorth',{
                        initialValue: baseProfile.insrncBorth ? moment(baseProfile.insrncBorth,dateFormat) : ''})(
                          <DatePicker
                            disabled={!(this.state.onlyRead === '1')}
                            format={dateFormat} 
                            onChange={this.selInsrncBorth}
                            style={{ width: '100%' }}
                          />
                      )} 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.insrncCardType}>
                    {getFieldDecorator('insrncCardType',{initialValue: baseProfile ? (baseProfile.insrncCardType ? baseProfile.insrncCardType : '') : ''})(
                      <Select onChange={this.insrncCardTypeChange.bind(this)} placeholder="请选择" style={{ width: '100%' }} disabled={!(this.state.onlyRead === '1')}>
                        {getSelectChild('certiDesc')}
                      </Select>
                )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.insrncCardId}>
                    {getFieldDecorator('insrncCardId',{
                      // rules:[{ validator: (rule,value,callback)=>{this.validatorIDCard(rule,value,callback,'insrncCardId')}}],
                      initialValue: baseProfile ? (baseProfile.insrncCardId ? baseProfile.insrncCardId : '') : ''})(
                      <Input placeholder='' disabled={!(this.state.onlyRead === '1')} 
                      style={{color: this.props.form.getFieldValue('insrncCardId') === (baseProfile ? baseProfile.insrncCardId : '') && (isChange.insrncCardId ? (isChange.insrncCardId === 0 ? true : false): true) ? '' : '#ff0000'}}
                    />
                )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.cMobileNoI}>
                    {getFieldDecorator('insrncMobile',{
                      //  rules: [{
                      //   pattern: /^[1][3,4,5,7,8][0-9]{9}$/, message: '格式有误，不允许空格等特殊符号',
                      // }],
                      initialValue: baseProfile ? (baseProfile.insrncMobile ? baseProfile.insrncMobile : '') : ''}
                      )(
                        <Input disabled={!(this.state.onlyRead === '1')} 
                        style={{color: this.props.form.getFieldValue('insrncMobile') === (baseProfile ? baseProfile.insrncMobile : '') && (isChange.insrncMobile ? (isChange.insrncMobile === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
              )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label="证件有效期">
                    {getFieldDecorator('identifyLongValidI', {
                      valuePropName: 'checked',
                      initialValue:  baseProfile.identifyLongValidI === '0' ? false : true,
                    })(<Checkbox onChange={() => this.setState((state) => ({
                      isILongLess: !state.isILongLess,
                    }))}>长期</Checkbox>)}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label="证件有效止期">
                    {getFieldDecorator('identifyExpiryI', {
                        initialValue: baseProfile.identifyExpiryI ? moment(baseProfile.identifyExpiryI, dateFormat) : '',
                      })(
                      <DatePicker disabled={this.state.isILongLess} allowClear={true} style={{ width: '100%' }} />
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                    <Form.Item label="国籍">
                        {getFieldDecorator('countryI',{
                          initialValue: baseProfile.countryI ? baseProfile.countryI : '1010090156',
                          rules: [{ required: true, message: '必填!' }],
                        })(
                          <Select optionFilterProp="children" showSearch placeholder="请选择" style={{ width: '100%' }}>
                            {this.renderSelectOption(CountryCode)}
                          </Select>
                        )}
                    </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.insrncPostCode}>
                    {getFieldDecorator('insrncZip',{initialValue: baseProfile ? (baseProfile.insrncZip ? baseProfile.insrncZip: '') : ''})(
                      <Input disabled />
                  )}                
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.cTelI}>
                    {getFieldDecorator('insrncHomePhone',{
                      // rules: [{pattern:/^\d{8,}$/,'message':"号码格式有误"}],
                      initialValue: baseProfile ?  (baseProfile.insrncHomePhone ? baseProfile.insrncHomePhone : '') : '',
                    })(
                      <Input disabled={!(this.state.onlyRead === '1')} 
                      style={{color: this.props.form.getFieldValue('insrncHomePhone') === (baseProfile ? baseProfile.insrncHomePhone : '') && (isChange.insrncHomePhone ? (isChange.insrncHomePhone === 0 ? true : false): true) ? '' : '#ff0000'}}
                    />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.cMailI}>
                    {getFieldDecorator('insrncMail',{
                      // rules: [{
                      //   pattern: this.props.form.getFieldValue('insrncMail') === (baseProfile ? baseProfile.insrncMail : '') ? '' : /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/, message: '请输入正确的格式',
                      // }],
                      initialValue: baseProfile ? (baseProfile.insrncMail ? baseProfile.insrncMail : '') : ''}
                      )(
                        <Input disabled={!(this.state.onlyRead === '1')} 
                        style={{color: this.props.form.getFieldValue('insrncMail') === (baseProfile ? baseProfile.insrncMail : '') && (isChange.insrncMail ? (isChange.insrncMail === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
                  )} 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.cAppprofType}>
                    {getFieldDecorator('occupationCodeI',{initialValue: baseProfile ? (baseProfile.occupationCodeI ? baseProfile.occupationCodeI : '') : ''})(
                      <Select optionFilterProp="children" showSearch placeholder="请选择" >
                        {this.renderSelectOption(ItemAccOccupationCode)}
                      </Select>
                  )}
                  </Form.Item>
                </Col>
                <Col md={16} lg={16} sm={16}>
                  <Form.Item label={labelSource.insrncAddress}>
                    {getFieldDecorator('insrncAddress',{initialValue: baseProfile? (baseProfile.insrncAddress ? baseProfile.insrncAddress : '') : ''})(
                      <Input  disabled={!(this.state.onlyRead === '1')} 
                      style={{color: this.props.form.getFieldValue('insrncAddress') === (baseProfile ? baseProfile.insrncAddress : '') && (isChange.insrncAddress ? (isChange.insrncAddress === 0 ? true : false): true) ? '' : '#ff0000'}}
                      />
                  )}
                  </Form.Item>
                </Col>
              </Row>
            </Panel>
            {/* 受益人列表 */}
            <Panel header={labelSource.beneTsrInfo} key="4">
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.tsrCode}>
                    {getFieldDecorator('tsrCode',{initialValue: baseProfile ? (baseProfile.tsrCode ? baseProfile.tsrCode : '') : ''})(
                      <Input disabled/>
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.tsrName}>
                    {getFieldDecorator('tsrName',{initialValue: baseProfile  ? (baseProfile.tsrName ? baseProfile.tsrName : '') : ''})(
                      <Input disabled/>
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.tsrTeam}>
                    {getFieldDecorator('tsrTeam',{initialValue: baseProfile ? (baseProfile.tsrTeam ? baseProfile.tsrTeam : '') : ''})(
                      <Input disabled/>
                  )}
                  </Form.Item>
                </Col>
                {/* 原有受益人列表删除，替换成新的 */}
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label="受益方式">
                    {getFieldDecorator('benfType',{
                      initialValue: baseProfile.bnfDto ? baseProfile.bnfDto.benfType : '0',
                      rules: [{ required: true, message: '必填!' }],
                    })(
                    <Select disabled={!this.state.isBenfUpFlag} onChange={this.BenfTypeChange.bind(this)} placeholder="请选择" style={{ width: '100%' }}>
                      {getSelectChild('benfType')}
                    </Select>
                    )}
                  </Form.Item>
                </Col>
                {/* 当受益人只有投保人时，受益人直接是法定 */}
                {
                  this.state.relationIsSelf ? (
                    <Fragment>
                      <Col md={8} lg={8} sm={16}>
                        <Form.Item  label="受益人姓名">
                        {getFieldDecorator('benfClntNmeB', {
                            initialValue: '法定',
                        })(<Input disabled={!this.state.isBenfUpFlag} placeholder="请输入" readOnly />)}
                        </Form.Item >
                      </Col>
                      <Col md={8} lg={8} sm={16}>
                          <Form.Item  label="受益人与被保人关系">
                              {getFieldDecorator('insrncRelation', {
                                  initialValue: '法定',
                              })(<Input disabled={!this.state.isBenfUpFlag} placeholder="请输入" readOnly />)}
                          </Form.Item >
                      </Col>
                    </Fragment>
                  ) : null
                }
              </Row>
              {
                !this.state.relationIsSelf ? (
                  this.state.SingleShouYiRenList.map((item, index) =>{
                    const { beneficiaryCertfCde } = item;
                    return (
                      <SingleShouYiRen
                        isSameWithTouBaoRen={!this.state.isBenfUpFlag}
                        policyMapInfo={item}
                        parentForm={form}
                        confirmAddBeneficiary={this.confirmAddBeneficiary}
                        DeleteBeneificiary={this.DeleteBeneificiary}
                        key={beneficiaryCertfCde}
                        index={index + 1}
                      />
                    );
                  })
                ) : null
              }
              <Divider/>
              <Button disabled={!this.state.isBenfUpFlag} type="primary" size="small" onClick={this.addBeneficiary}>新增受益人</Button>
              <Button
                size='small'
                type="primary"
                style={{
                  marginLeft: '10px',
                }}
                onClick={this.confirmReCorrect}
              >批改受益人</Button>
            </Panel>
            {/* 批文及相关资料 */}
            <Panel header="批文与相关资料上传" key="5">
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={16} lg={16} sm={16}>
                  <Form.Item label={
                    <span>
                      批文
                    </span>}>
                    {getFieldDecorator('endorText',{initialValue: ''})(
                    <TextArea placeholder=""  style={{ width: '100%', height: '100px' }}  disabled={!(this.state.onlyRead === '1')}/>
                      )}
                  </Form.Item>
                  </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={16} lg={16} sm={16}>
                  <div style={{textAlign: 'left', marginTop: 30, width: '100%',}}>
                    <Form.Item label="相关资料上传">
                      <Upload {...prams}>
                        <Button disabled={!(this.state.onlyRead === '1')}>
                          <Icon type="upload" />
                        </Button>
                      </Upload>
                    </Form.Item>
                </div>
                </Col>
              </Row>
            </Panel>
          </Collapse>
          <div>
            <div style={{width: '100%', textAlign: 'center', marginTop: 60 }}>
              <Popconfirm title='确认提交' visible={this.state.sureCommit} onCancel={this.onCancel} onConfirm={this.onConfirm}>
                  <Button type="primary" style={{marginRight: 40}} loading={loading} onClick={this.makeSure}>
                    确认批改
                  </Button>
                </Popconfirm>
              {
                this.props.location.cPlyNo !== undefined 
                ?
                <Button style={{marginLeft: 40, width: 88 }}>
                  <a href={`/#/query-manage/policy-list?cPlyNo=${this.props.location.cPlyNo}&prePath=/correction/basic-correction`}>取消</a>
                </Button>
              :
                <Button style={{marginLeft: 40, width: 88 }} onClick={this.resetForm.bind(this)}>
                取消
                </Button>
              }
            </div>
          </div>
        </Form>
      </PageHeaderLayout>
    )
      
}
}

