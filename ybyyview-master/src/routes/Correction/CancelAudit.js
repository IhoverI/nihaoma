import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { DatePicker } from 'snk-web';
import {
  Row,
  Col,
  Steps,
  Select,
  Table,
  Card,
  Button,
  Dropdown,
  Badge,
  Divider,
  Modal,
  Form,
  Input,
  message,
  notification
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './CancelAudit.less';
import { datetimeFormat } from '../../utils/utils';
import FormItem from '../../../node_modules/antd/lib/form/FormItem';

const { Option } = Select;
const Step = Steps.Step;

const steps = [{
    title: '注退上报',
    content: '注退上报',
  }, {
    title: '注退审核',
    content: '注退审核',
  }, {
    title: '完成',
    content: '完成',
  }];

@Form.create()
@connect(({  loading,profile }) => ({
  loading: loading.models.profile,
  profile,
}))

export default class CancelAudit extends PureComponent {
  state = {
    pageNum: 1,
    pageSize: 10,
    remark: '',
    fieldsValue: {},
    selectedRows: [],
    selectedRowKeys: [],
  };
  componentDidMount() {
    this.search();
  }


  _formatFlag = (text) => {
      let val = '';
      switch(text) {
        case '1': val = '有效'; break;
        case '2': val = '注销'; break;
        case '3': val = '退保'; break;
        case '4': val = '过期'; break;
      }
      return val;
    }

  _formatexecuteType = (text) => {
    let val = '';
    switch(text) {
      case '01': val = '注销'; break;
      case '02': val = '犹豫期退保'; break;
      case '03': val = '无退费退保'; break;
      case '04': val = '全额退费退保'; break;
      case '05': val = '部分退费退保'; break;
    }
    return val;
  }

  getColumns(){
    var _this = this;
    return [{
          title: '保单号',
          dataIndex: 'cPlyNo',
          align: 'center',
          width: 180,
          render: (text, record) => <a href={`/#/query-manage/policy-list?cPlyNo=${record.cPlyNo}&prePath=/correction/cancel-audit`}>{text}</a>,
        }, {
          title: '产品名称',
          dataIndex: 'prodName',
          align: 'center',
          width: 100,
        }, {
          title: '批改提交账号',
          dataIndex: 'userName',
          align: 'center',
          width: 180,
        }, {
          title: '款别',
          dataIndex: 'cTgtFld1',
          align: 'center',
          width: 100,
        }, {
          title: '被保险人姓名',
          dataIndex: 'cInsrntCnm',
          align: 'center',
          width: 110,
        }, {
          title: '保险起期',
          dataIndex: 'insrncBeginDate',
          align: 'center',
          width: 145,
          render: (text) => {
            return (<div>{datetimeFormat(text)}</div>)
          }
        }, {
          title: '保险止期',
          dataIndex: 'insrncEndDate',
          align: 'center',
          width: 145,
          render: (text) => {
            return (<div>{datetimeFormat(text)}</div>)
          }
        },{
          title: '保单状态',
          dataIndex: 'flag',
          align: 'center',
          width: 100,
          render: (text) => {
            return (
              <span>{this._formatFlag(text)}</span>
            );
          }
        },{
          title: '成功扣款期次',
          dataIndex: 'successPeriod',
          align: 'center',
          width: 110,
        },{
          title: '注退类型',
          dataIndex: 'executeType',
          align: 'center',
          width: 100,
          render: (text) => {
            return (
              <span>{this._formatexecuteType(text)}</span>
            )
          }
        },{
          title: '批文',
          dataIndex: 'endorText',
          align: 'center',
          width: 460,
        },{
          title: '退保期次',
          dataIndex: 'surrenderPeriod',
          align: 'center',
          width: 100,
        },
        {
          title: '备注',
          dataIndex: 'remark',
          align: 'center',
          // fixed: 'right',
          width: 200,
          render: (text,record) => {
            return (
              <Input.TextArea id={`${record.cPlyNo}`} />
            );
          }
        },
      ];
  }

  passConfirm = () => {
    Modal.confirm({
      title: '确定通过?',
      content: '逐条同步到核心，请耐心等候',
      onOk: () => this.pass(),
    });
  }

  pass = () => {
    let selectedRows = this.state.selectedRows;
    let payload = [];
    let arrItem = {};
    let remarkArr = [];
    for (var index = 0; index < selectedRows.length; index++) {
      const remarkItem = document.getElementById(`${selectedRows[index].cPlyNo}`).value;
      remarkArr.push(remarkItem);
      arrItem = {remark: remarkArr[index], cPlyNo: selectedRows[index].cPlyNo}
      payload.push(arrItem);
    }
    this.props.dispatch({
      type: 'profile/pass',
      payload: {
        reqBody: payload
      },
    }).then(() => {
      let {profile:{passResp}} = this.props;
      let successNum = passResp.data.success.length || 0;
      let failNum = passResp.data.fail.length || 0;
      let title = '成功' + successNum + '条,失败' + failNum + '条';
      let jsonResp = '';
      if (failNum !== 0) {
        jsonResp = JSON.stringify(passResp.data.fail, undefined, 2);
        jsonResp = jsonResp.replace(new RegExp('cPlyNo',"gm"), '保单号');
        jsonResp = jsonResp.replace(new RegExp('message',"gm"), '失败原因');
      }
      const args = {
        message: title,
        description: jsonResp,
        duration: 0,
        onClose: this.handleClose
      };
      notification.open(args);
    })
  }

  backConfirm = () => {
    Modal.confirm({
      title: '确定打回?',
      content: '站内信通知申请人',
      onOk: () => this.back(),
    });
  }

  back = () => {
    let selectedRows = this.state.selectedRows;
    let payload = [];
    let arrItem = {};
    let remarkArr = [];
    for (var index = 0; index < selectedRows.length; index++) {
      const remarkItem = document.getElementById(`${selectedRows[index].cPlyNo}`).value;
      if (remarkItem === '') {
        message.warning('请填写备注');
        return;
      } else {
        remarkArr.push(remarkItem);
        arrItem = {remark: remarkArr[index], cPlyNo: selectedRows[index].cPlyNo}
        payload.push(arrItem);
      }
    }
    
    this.props.dispatch({
      type: 'profile/back',
      payload: {
        reqBody: payload
      },
    }).then(() => {
      let {profile:{backResp}} = this.props;
      let successNum = backResp.data.success.length || 0;
      let failNum = backResp.data.fail.length || 0;
      let title = '成功' + successNum + '条,失败' + failNum + '条';
      let jsonResp = '';
      if (failNum !== 0) {
        jsonResp = JSON.stringify(backResp.data.fail, undefined, 2);
        jsonResp = jsonResp.replace(new RegExp('cPlyNo',"gm"), '保单号');
        jsonResp = jsonResp.replace(new RegExp('message',"gm"), '失败原因');
      }
      const args = {
        message: title,
        description: jsonResp,
        duration: 0,
        onClose: this.handleClose
      };
      notification.open(args);
    }).catch(() => {
      message.error('网络异常，请稍后重试')
    })
  }

  handleClose = () => {
    this.search();
  }
  
  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({selectedRowKeys, selectedRows});
  }

  handleTableChange = (page, pageSize) => {
    this.setState({
      pageNum: page,
      pageSize: pageSize,
      selectedRowKeys: [],
      selectedRows: [],
    })
    let { dispatch } = this.props;
    let searchValues = this.state.searchValues;
    searchValues.pageNum = page;
    searchValues.pageSize = pageSize;
    dispatch({
      type: 'profile/endorList',
      payload: searchValues,
    }).catch(() => {
      message.error('网络异常，请稍后重试')
    })
  }

  resetForm = () => {
    this.props.form.resetFields();
  }

  search = () => {
    this.setState({
      selectedRowKeys: [],
      selectedRows: [],
    })
    let { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(!fieldsValue.submitTime||fieldsValue.submitTime.length===0){
        fieldsValue.submitTimeStart = null;
        fieldsValue.submitTimeEnd = null;
      }else{
        fieldsValue.submitTimeStart = fieldsValue.submitTime[0].format("YYYY-MM-DD");
        fieldsValue.submitTimeEnd = fieldsValue.submitTime[1].format("YYYY-MM-DD");
      }
      fieldsValue.pageNum = 1;
      fieldsValue.pageSize = 10;
      delete fieldsValue.submitTime;
      this.setState({
        pageNum: 1,
        pageSize: 10,
        searchValues: fieldsValue,
      });
      dispatch({
        type: 'profile/endorList',
        payload: fieldsValue,
      }).catch(() => {
        message.error('网络异常，请稍后重试')
      })
    });
  }

  render() {
    const { loading, profile: { endorListData }} = this.props;
    const { getFieldDecorator } = this.props.form;
    let data = [];
    let total = 0;
    if (endorListData){
      data = endorListData.list
      total = endorListData.total
    }

    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };

    const { MonthPicker, RangePicker } = DatePicker;

    const formItemLayout = {
      labelCol: {
        sm: { span: 4 },
      },
      wrapperCol: {
        sm: { span: 20 },
      },
    };
    
    const hasSelected = this.state.selectedRows.length > 0;
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <Steps style={{width:'70%',margin:'0px auto 20px auto'}} current={1}>
              {steps.map(item => <Step key={item.title} title={item.title} />)}
          </Steps>
          <Form>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
              <Col sm={12} xs={24}>
                <Form.Item {...formItemLayout} label='保单号'>
                  {getFieldDecorator('cPlyNO',
                  {rules: [{ max: 30, message: '请输入正确的投保单号' }]})(
                    <Input placeholder='请输入'>
                    </Input>
                  )}
                </Form.Item>
              </Col>
              <Col sm={12} xs={24}>
                <Form.Item {...formItemLayout} label='批改申请日期'>
                  {getFieldDecorator('submitTime')(
                    <RangePicker allowClear style={{ width: '100%' }}>
                    </RangePicker>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
              <span style={{ marginBottom: 24 }}>
                <Button loading={loading} onClick={this.search} type="primary" >
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.resetForm}>
                  重置
                </Button>
              </span>
            </div>
          </Form>
          <Table 
            title={() => {
              return <div style={{ display:'flex'}}>
                    <Button disabled={!hasSelected} loading={loading} style={{marginRight:10}}  type='primary' onClick={this.passConfirm}>审核通过</Button>
                    <Button disabled={!hasSelected} loading={loading} style={{marginRight:10}} onClick={this.backConfirm}>打回</Button>
                    {hasSelected ? <Button disabled style={{marginRight:10}}>Selected {this.state.selectedRows.length} itmes</Button> : ''}
              </div>;
            }}
            loading={loading}
            scroll={{x: 1950}}
            size='small'
            rowKey={record => record.cPlyNo}
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current:this.state.pageNum,
              pageSize:this.state.pageSize,
              total:total,
              onChange: (page, pageSize) => this.handleTableChange(page, pageSize),
              showSizeChanger: true,
              onShowSizeChange: (page, pageSize) => this.handleTableChange(page, pageSize),
              pageSizeOptions:['10','20','30','40','50'],
            }}
            bordered={true}
            rowSelection={rowSelection} 
            columns={this.getColumns()} 
            dataSource={data} />
        </Card>
      </PageHeaderLayout>
    );
  }
}
