import React, { PureComponent } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Form,
  Row,
  Col,
  Input,
  DatePicker,
  Select,
  Button,
  Card,
  InputNumber,
  Radio,
  Icon,
  Tooltip,
  message,
  Popconfirm,
  Modal,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './CancelCorrection.less';
import { STATUS_CODES } from 'http';
import { getRegularCheck, getSelectChild, getHost, getUrlParam, timestampToString, getTimeDistance } from '../../utils/utils';


const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const VIEWLOCAL = SERVERLOCAL;

@connect(({ policyCancel, loading }) => ({
  policyCancel,
  loading: loading.models.policyCancel
}))
@Form.create()
export default class CancelCorrection extends PureComponent {

  state = {
    isSelected: true,
    newEndorText: '',
    successPeriod: 0,
    surrenderPeriod: 0
  }

  componentDidMount = () => {
    let cPlyNo = this.props.location.cPlyNo || getUrlParam(this.props.location.search).cPlyNo;
    if (cPlyNo) {
      let searchTerm = {
        cPlyNo: cPlyNo
      };
      this.props.dispatch({
        type: 'policyCancel/fetchPolicyCancel',
        payload: searchTerm
      });
    }
  }

  handleSearch = () => {

    const { form, dispatch } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      let cPlyNo = fieldsValue.cPlyNo1;
      if (cPlyNo === '') {
        message.error('请输入保单号');
        return;
      }
      let searchTerm = {
        cPlyNo: cPlyNo
      };
      dispatch({
        type: 'policyCancel/fetchPolicyCancel',
        payload: searchTerm
      }).then(() => {
        this.props.form.resetFields(['executeType']);
        this.props.form.resetFields(['surrenderPeriod']);
        this.props.form.resetFields(['endorText']);
      }).catch(() => {
        message.error('网络异常，请稍后重试');
      })
    })
  }

  handleFormReset = () => {
    const {form} = this.props;
    form.resetFields();
  }
  
  handleChange = (value) => {

    const { form, dispatch } = this.props;

    let newPeriod = 0;
    let successPeriod = form.getFieldValue('successPeriod');
    if (successPeriod === undefined) {
      successPeriod = 0;
    }
    switch (value) {
      case '01': newPeriod = 12; break;
      case '02': newPeriod = 12; break;
      case '03': newPeriod = 12 - successPeriod; break;
      case '04': newPeriod = 12; break;
      case '05': newPeriod = 12; break;
    }
    form.setFieldsValue({surrenderPeriod: newPeriod})
    if ('05' !== value) {
      this.setState({
        isSelected: true
      })
    } else {
      this.setState({
        isSelected: false
      })
    }
    this.surrenderPeriodChange(newPeriod, value);
  }

  surrenderPeriodChange = (value, executeType) => {
    const { form, dispatch } = this.props;
    let cPlyNo = form.getFieldValue('cPlyNo');
    let successPeriod = form.getFieldValue('successPeriod');
    let searchTerm = {
      cPlyNo: cPlyNo,
      executeType: executeType,
      successPeriod: successPeriod,
      surrenderPeriod: value
    };
    this.setState({
      successPeriod: successPeriod,
      surrenderPeriod: value
    });
    dispatch({
      type: 'policyCancel/queryBackPrm',
      payload: searchTerm
    }).then( () => {
        this.createEndorText()
      }
    )
  }

  createEndorText = () => {
    const { policyCancel, form } = this.props;
    let executeType = form.getFieldValue('executeType');
    let successPeriod = this.state.successPeriod;
    let surrenderPeriod = this.state.surrenderPeriod;
    let backPrm = policyCancel.queryBackPrm.data;
    let realPeroid = successPeriod + surrenderPeriod - 12;
  
    let endorText = '';
    switch (executeType) {
      case '01':
        endorText = '经投保人申请保险人同意，本保单注销，保险合同解除。';
        break;
      case '02':
        endorText = `经投保人申请保险人同意，本保单终止保险责任，首年保单客户同意自动销毁保单（以电话录音为确认凭证），因犹豫期退保，退还保险费${backPrm}元。`;
        break;
      case '03':
        endorText = '经投保人申请保险人同意，本保单退保，保险合同解除。';
        break;
      case '04':
        endorText = `经投保人申请保险人同意，本保单终止保险责任，客户同意自动销毁保单（以电话录音为确认凭证），因客户抱怨等原因，退还保险费${backPrm}元。`;
        break;
      case '05':
        endorText = `经投保人申请保险人同意，本保单退保，保险合同解除。因客户不认可最后${realPeroid}期扣款、退还保险费用${backPrm}元。`;
        break;
    }
    if (realPeroid <= 0 && executeType === '05') {
      endorText = '';
    }

    form.setFieldsValue({endorText: endorText});
  }

  success = (message) => {
    Modal.success({
      title: '提交结果',
      content: message,
      onOk: this.handleSearch,
    });
  }
  
  error = (message) => {
    Modal.error({
      title: '提交结果',
      content: message,
      onOk: this.handleSearch,
    });
  }

  showConfirm = () => {
    Modal.confirm({
      title: '确定提交?',
      content: '自动审核的批改，将实时同步到核心；人工审核的批改，将提交至人工审核',
      onOk: () => this.handleSubmit(),
    });
  }

  handleSubmit = () => {
    const { form, dispatch } = this.props;
    form.validateFieldsAndScroll((err, fieldsValue) => {
      dispatch({
        type: 'policyCancel/submitPolicyCancel',
        payload: fieldsValue
      }).then(() => {
        let { executeResp } = this.props.policyCancel;
        if (executeResp.code === 0) {
          this.success(executeResp.message);
        } else {
          this.error(executeResp.message);
        }
      }).catch(() => {
        message.error('网络异常，请稍后重试')
      })
    })
  }

  cancelSubmitConfirm = () => {
    let cPlyNo = this.props.form.getFieldValue('cPlyNo');
    if (cPlyNo === '') {
      message.error('请搜索后，再点击撤消');
      return;
    }
    Modal.confirm({
      title: `确定提交:${cPlyNo}?`,
      content: '撤消申请后，保单将恢复正常请款',
      onOk: () => this.cancelSubmit(cPlyNo),
    });
  }

  cancelSubmit = (cPlyNo) => {
    let { dispatch } = this.props;
    let searchTerm = {
      cPlyNo: cPlyNo
    };
    dispatch({
      type: 'policyCancel/cancelSubmit',
      payload: searchTerm
    }).then(() => {
      let { cancelSubmitResp } = this.props.policyCancel;
      if (cancelSubmitResp.code === 0) {
        this.success(cancelSubmitResp.message);
      } else {
        this.error(cancelSubmitResp.message);
      }
    }).catch(() => {
      message.error('网络异常，请稍后重试')
    })
  }

  render() {
    const { loading, policyCancel, form } = this.props;
    const { search: { data, code } } = policyCancel;
    const isSuccess = code === 0 ? true : false;

    let cPlyNo = this.props.location.cPlyNo || getUrlParam(this.props.location.search).cPlyNo;

    const { getFieldDecorator, getFieldValue } = this.props.form;
    const policyProfile = null == data ? undefined : data;
    let one = true;
    let two = true;
    let three = true;
    let four = true;
    let fire = true;
    if (policyProfile !== undefined) {
      one = policyProfile.surrenderTypeMap.one === '1' ? false : true;
      two = policyProfile.surrenderTypeMap.two === '1' ? false : true;
      three = policyProfile.surrenderTypeMap.three === '1' ? false : true;
      four = policyProfile.surrenderTypeMap.four === '1' ? false : true;
      fire = policyProfile.surrenderTypeMap.fire === '1' ? false : true;
    }
    const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    };

    const submitFormLayout = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 10, offset: 7 },
      },
    };

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <Form  style={{ marginTop: 8 }}>
          <div className={styles.information} >
              <FormItem {...formItemLayout} label="保单号">
                {getFieldDecorator('cPlyNo1',
                {initialValue: cPlyNo ? cPlyNo : ''},
                )(
                  <Input style={{ width: '50%' }} placeholder="请输入" />
                )}
                <Button type="primary" loading={loading} style={{ marginLeft: 8 }} onClick={this.handleSearch}>
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </FormItem>
            </div>
            <div className={styles.searchListItem}>
              <FormItem
                {...formItemLayout}
                label={
                  <span>
                    保单号
                    { isSuccess ? 
                      <em className={styles.optional}>
                        <Tooltip title="点击跳转至保单详情页">
                          <a  href={`/#/query-manage/policy-list?cPlyNo=${policyProfile.cPlyNo}&prePath=/correction/cancel-correction`} >
                            （详情）
                            <Icon type="info-circle-o" style={{ marginRight: 4 }} />
                          </a>
                        </Tooltip>
                      </em> : null
                    }
                  </span>
                }
              >
                {getFieldDecorator('cPlyNo', {initialValue: null===policyProfile || undefined === policyProfile ? '' : policyProfile.cPlyNo})(
                  <Input
                    style={{ width: '100%' }}
                    disabled
                    placeholder=""
                  />
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="被保人姓名">
                <Input
                  style={{ width: '100%' }}
                  disabled
                  placeholder=""
                  value={undefined === policyProfile ? '' : policyProfile.cInsrntCnm}
                />
              </FormItem>
              <FormItem {...formItemLayout} label="产品名称">
                <Input
                  style={{ width: '100%' }}
                  disabled
                  placeholder=""
                  value={null===policyProfile || undefined === policyProfile ? '' : policyProfile.prodName}
                />
              </FormItem>
              <FormItem {...formItemLayout} label="产品代码">
                <Input
                  style={{ width: '100%' }}
                  disabled
                  placeholder=""
                  value={null===policyProfile || undefined === policyProfile ? '' : policyProfile.prodNo}
                />
              </FormItem>
              <FormItem {...formItemLayout} label="款别">
                <Input
                  style={{ width: '100%' }}
                  disabled
                  placeholder=""
                  value={null===policyProfile || undefined === policyProfile ? '' : policyProfile.cTgtFld1}
                />
              </FormItem>
              <FormItem {...formItemLayout} label="保险期间">
              <RangePicker value= 
                    {[null===policyProfile || undefined === policyProfile ? '' : moment(policyProfile.insrncBeginDate),
                    null===policyProfile || undefined === policyProfile ? '' : moment(policyProfile.insrncEndDate)]}
                    style={{ width: '100%' }}
                    disabled
                    />
              </FormItem>
              <FormItem {...formItemLayout} label="成功扣款期次">
                {getFieldDecorator('successPeriod', {initialValue: null===policyProfile || undefined === policyProfile ? '' : policyProfile.successPeriod})(
                  <Input
                    style={{ width: '100%' }}
                    disabled
                    placeholder="成功扣了多少期"
                  />
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="保单状态">
                {getFieldDecorator('falg', {initialValue: null===policyProfile || undefined === policyProfile ? '' : policyProfile.flag})(
                  <Select disabled>
                    <Option value='1'>有效</Option>
                    <Option value='2'>注销</Option>
                    <Option value='3'>退保</Option>
                    <Option value='4'>过期</Option>
                  </Select>
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="批退状态">
                {getFieldDecorator('endorStatus', {initialValue: null===policyProfile || undefined === policyProfile ? '' : policyProfile.endorStatus})(
                  <Select disabled >
                    <Option value='01'>自动审核</Option>
                    <Option value='02'>待审核</Option>
                    <Option value='03'>审核通过</Option>
                    <Option value='04'>审核不通过</Option>
                    <Option value='05'>已退费</Option>
                    <Option value='06'>撤消申请</Option>
                  </Select>
                )}
              </FormItem>
            </div>
            <div className={styles.searchListItem}>
              <FormItem {...formItemLayout} label="注退类型">
                {getFieldDecorator('executeType')(
                  <Select placeholder="请选择" style={{ width: '100%' }}
                    onSelect={this.handleChange} >
                    <Option disabled={one} value='01'>注销</Option>
                    <Option disabled={two} value='02'>犹豫期内退保</Option>
                    <Option disabled={three} value='03'>无退费退保</Option>
                    <Option disabled={four} value='04'>全额退费退保</Option>
                    <Option disabled={fire} value='05'>部分退费退保</Option>
                  </Select>
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="退保期次">
                {getFieldDecorator('surrenderPeriod')(
                  <Select placeholder="请选择" style={{ width: '100%' }}
                    disabled={this.state.isSelected}
                    onSelect={(value) => this.surrenderPeriodChange(value, form.getFieldValue('executeType'))} >
                    {getSelectChild('period')}
                  </Select>
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="批文">
                {getFieldDecorator('endorText')(
                <Input.TextArea
                    style={{ width: '100%' }}
                    placeholder="批文自动生成，不可更改"
                    disabled
                    autosize={true}
                  />
                )}
              </FormItem>
            </div>
            <FormItem {...submitFormLayout} style={{ marginTop: 32 }}>
              <Button type="primary" onClick={this.showConfirm} loading={loading}>
                提交注退申请
              </Button>
              <Button style={{ marginLeft: 32}} type="primary" onClick={this.cancelSubmitConfirm} loading={loading}>
                撤消申请
              </Button>
            </FormItem>
          </Form>
        </Card>
      </PageHeaderLayout>
    );
  }
}
