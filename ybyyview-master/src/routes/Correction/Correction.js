import React, { PureComponent, Fragment } from 'react';
import {
  Button,
  Form,
  Icon,
  Col,
  Row,
  DatePicker,
  Input,
  Select,
  Checkbox,
  Collapse,
  Modal,
  message,
  Tooltip,
} from 'snk-web';
import moment from 'moment';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import SingleShouYiRen from '../../components/SingleShouYiRen/SingleShouYiRen';
import { Link } from 'dva/router';
import styles from './BasicCorrection.less';
import { getDataSource,getSelectChild } from '../../utils/utils'

const { Option } = Select;
const { RangePicker } = DatePicker;
const  Panel  = Collapse.Panel;
const { TextArea } = Input;

const labelSource = getDataSource('labelSource');

@connect(({ profile, loading, selfchannelForm }) => ({
  selfchannelForm,
  profile,
  loading: loading.models.profile,
}))
@Form.create()
export default class Correction extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      width: '100%',
      visible: false,
      confirmLoading: false,
      path: this.props.location.pathRouter,
      id: this.props.location.id,
      cAppNo: '',
      remarkRule: {},
      liked: true,
      SingleShouYiRenList: [],
    };
  }

  componentDidMount() {
    if (this.state.id !== undefined) {
      this.props.dispatch({
        type: 'profile/userInforDetail',
        payload: {
          id: this.state.id,
        },
      }).then((response) => {
        this.props.dispatch({
          type: 'selfchannelForm/checkCodeType',
        });
        const { data: { beneficiaryList = '' } } = response;
        const symbol = '#partition#';
        const arr = beneficiaryList.split(symbol);
        const bnfList = arr.map((item) => JSON.parse(item));
        this.setState({
          SingleShouYiRenList: [...bnfList],
        });
      });
    }
  }

  download = (fileId) => {

    var url =  `${SERVERLOCAL}ybyy-policy/policy/correct/download`;

    var TargetFrame = document.createElement("iframe");
    TargetFrame.setAttribute("name",'download_frame');
    TargetFrame.setAttribute("style","display:none");
    document.body.appendChild(TargetFrame);

    var form=document.createElement("form");
    form.setAttribute("style","display:none");
    form.setAttribute("target","download_frame");
    form.setAttribute("method","post");
    form.setAttribute("action",url);

    var input1=document.createElement("input");
    input1.setAttribute("type","hidden");
    input1.setAttribute("name","path");
    input1.setAttribute("value",fileId);
    form.appendChild(input1);
    document.body.appendChild(form);
   
    form.submit();
    setTimeout(()=>{
      document.body.removeChild(form);
      document.body.removeChild(TargetFrame);
    },100);
  }


  makeSure = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      this.props.dispatch({
        type: 'profile/apply',
        payload: fieldsValue,
      });
    })
  }

  addCancel = () => {
    this.setState({
      visible: false,
    });
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  correctPass = () => {
      this.props.dispatch({
          type: 'profile/passOrNot',
          payload: {
            statu: 1,
            cAppNo: this.state.id,
          },
      }).then(()=>{
        const { profile: { passOrNotResp  } } = this.props;
        const { code, message } = passOrNotResp;
        if (code === 1) {
          Modal.warning({
            title: '提示',
            content: message,
            okText: '知道了',
          })
        }
        if (code === 0) {
          Modal.success({
            title: '审核通过提交成功',
            content: message,
            onOk: () => {
              this.props.history.push({
                pathname:'/correction/basic-audit',
              });
            }
          });
        }
      }).catch(e=>{
      message.error(e.message||'网络异常请稍后再试');
    });
  }

  addOk = () => {
    let value =this.props.form.getFieldValue("remark");
    value = value.replace(/\s+/g, ""); 
    value = value.replace(/<\/?.+?>/g,""); 
    this.props.form.setFieldsValue({remark:value});
    this.props.form.validateFields((err, values) => {
      if(err) return;
      this.setState({
        visible: false,
      });
      this.props.dispatch({
        type:'profile/passOrNot',
        payload: {
          statu: 2,
          remark: values.remark,
          cAppNo: this.state.id,
        },
      }).then(()=>{
        let { profile: { passOrNotResp  } } = this.props;
        let resCode = passOrNotResp.code;
        let resMes = passOrNotResp.message;
        if (resCode === 1) {
          Modal.warning({
            title: '提示',
            content: resMes,
            okText: '知道了',
          })
        }
        if (resCode === 0) {
          Modal.success({
            title: '打回',
            content: resMes || '提交成功',
            onOk: () => {
              this.props.history.push({
                pathname:'/correction/basic-audit',
              });
            }
          });
        }
      }).catch(e => {
        message.error(e.message ||'网络异常请稍后再试', 5);
      });
      this.setState({
        confirmLoading: true,
      });
    })
  }

    formaUnit = (val,code) => {
      if (code === 'cElcFlag') {
        let text = '';
        switch (val) {
          case '0': text = '纸质保单'; break;
          case '1': text = '电子保单'; break;
          case '2': text = '电子+纸质'; break;
          case '3': text = '纸质保单'; break;
          case '4': text = '纸质转电子'; break;
          default: break;
        }
          return text;
      }
    }
        
    format = (value,code) => {
      let text = '';
      if (code === 'insrncSex' || code === 'cAppSex') {
        switch (value) {
          case "106001": return "男"; break;
          case "106002": return "女"; break;
          case "01": return "男"; break;
          case "02": return "女"; break;
          default:break;
        }
      } else if (code === 'relation') {
        switch (value) {
          case "601004": return "配偶"; break;
          case "601002": return "子女"; break;
          case "601003": return "父母"; break;
          case "601012": return "兄弟姐妹"; break;
          case "601006": return "本人"; break;
          default:break;
        }
      } else if (code === 'insrncCardType' || code === 'cAppCertfType') {
        switch(value) {
          case "01": return "身份证"; break;
          case "02": return "护照"; break;
          case "03": return "军官证"; break;
          case "05": return "台胞证"; break;
          case "06": return "港澳返乡证"; break;
          case "07": return "出生证"; break;
          case "99": return "其他"; break;
          default:break;
        }
      } else if (code === 'benfType') {
        switch(value) {
          case "1": return "指定"; break;
          case "2": return "法定"; break;
          default:break;
        }
    }
  }

  renderSelectOption = (data) => {
    return data.map(item => (
        <Option key={item.code} value={item.code}>{item.codeName || item.name}</Option>
    ));
  }

  render() {
    const { profile: { queryInfoData }, form, loading, selfchannelForm } = this.props;
    const baseProfile = queryInfoData ||{};
    const {
      CountryCode,
      ItemAccOccupationCode,
      relationBetweenCode,
    } = selfchannelForm;
    let isChange = {};
    if(queryInfoData){
      if (queryInfoData.isChange !== undefined || queryInfoData.isChange !== null) {
        isChange = queryInfoData.isChange || {};
      }
    }
    
    const { getFieldDecorator } = form;
    const buttonArr = [];
    if (getSelectChild('prodDesc') && baseProfile.prodNo !== '' ) {
      for (const i in getSelectChild('prodDesc')) {
        if (getSelectChild('prodDesc')[i].props.value === baseProfile.prodNo) {
          proName = getSelectChild('prodDesc')[i].props.children || '';
        }
      }
    }
    if (queryInfoData !== {} && queryInfoData) {
      const fileIdArr = queryInfoData.file || '';
      for (const i in fileIdArr) {
        const suffix = fileIdArr[i].substring(fileIdArr[i].lastIndexOf("\.") + 1 , fileIdArr[i].length);
        let buttonItem;
        if (suffix === 'png' || suffix === 'pdf' || suffix === 'jpg' || suffix === 'jpeg') {
            buttonItem = (
              <Button href={fileIdArr[i]} style={{marginRight:6}} target="_blank">
                <span style={{margin: '0 4px'}}>
                  {fileIdArr[i].substring(fileIdArr[i].lastIndexOf("\.") + 1 , fileIdArr[i].length)}
                </span>
                 文件预览
              </Button>
            );
          } else {
            buttonItem = (
              <Button 
                value={fileIdArr[i]} 
                icon = 'download' 
                style={{marginRight:6}}
                onClick={e=>this.download(fileIdArr[i])}>
                  下载
                    <span style={{margin: '0 4px'}}>
                      {fileIdArr[i].substring(fileIdArr[i].lastIndexOf("\.") + 1 , fileIdArr[i].length)}
                    </span>
                  文件
              </Button>
            );
          }
         buttonArr.push(buttonItem);
      }
    }

    return (
      <PageHeaderLayout 
        title="保单详情" 
        wrapperClassName={styles.basicCorrection} 
      >
        <Form layout="inline" className='custom-form-wrapper' onSubmit={this.makeSure}>
          <Collapse defaultActiveKey={['1','2','3','4','5']}>
            <Panel header={labelSource.policyInfo} key="1">
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='保单号'>
                    {getFieldDecorator('cPlyNo',{initialValue: baseProfile ? baseProfile.cPlyNo :''})(
                    <Input readOnly style={{color: isChange.cPlyNo === 0 ? '' : '#ff0000'}}/>   
                    )}              
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='原始保单号'>
                    {getFieldDecorator('cPlyNoOld',{initialValue: baseProfile ? baseProfile.cPlyNoOld :''})(
                      <Input readOnly style={{color: isChange.cPlyNoOld === 0 ? '' : '#ff0000'}} />
                )}  
                  </Form.Item>
                </Col>
                 <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.transationCode}>
                    {getFieldDecorator('transationCode',{initialValue: baseProfile ? (baseProfile.transationCode ? baseProfile.transationCode : '') : ''})(
                      <Input readOnly  style={{color: isChange.transationCode === 0 ? '' : '#ff0000'}}/>
                  )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.prodCName}>
                    {getFieldDecorator('prodNo',{initialValue: baseProfile ? (baseProfile.prodNo ? baseProfile.prodNo : '') : ''})(
                      <Input readOnly style={{color: isChange.prodNo === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col lg={8} md={8} sm={16}>
                  <Form.Item label={labelSource.renewalTime}>
                    {getFieldDecorator('renwalNumber',{initialValue: baseProfile ? baseProfile.renwalNumber : ''})(
                      <Input readOnly style={{color: isChange.renwalNumber === 0 ? '' : '#ff0000'}}/>
                  )}
                  </Form.Item>
                </Col> 
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.productPlan}>
                    {getFieldDecorator('feeType',{initialValue: baseProfile ? baseProfile.feeType : ''})(
                    <Input readOnly style={{color: isChange.feeType === 0 ? '' : '#ff0000'}} />
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.payKind}>
                    {getFieldDecorator('payWay',{initialValue: baseProfile ? baseProfile.payWay : ''})(
                      <Input readOnly style={{color: isChange.payWay === 0 ? '' : '#ff0000'}} />
                    )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.nGetPrm}>
                    {getFieldDecorator('prm',{initialValue: baseProfile ? baseProfile.prm : ''})(
                      <Input readOnly style={{color: isChange.prm === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.tInsrncTm}>
                    {getFieldDecorator('insrncDate',{initialValue: baseProfile ? baseProfile.insrncDate : ''})(
                      <Input readOnly style={{color: isChange.insrncDate === 0 ? '' : '#ff0000'}} />)}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.cardId}>
                    {getFieldDecorator('cardId',{initialValue: baseProfile ? baseProfile.cardId : ''})(
                      <Input readOnly style={{color: isChange.cardId === 0 ? '' : '#ff0000'}}/>
                    )}
                  </Form.Item>
                </Col>
                 <Col md={8} lg={8} sm={16}>
                  <Form.Item label='保单类型'>
                    {getFieldDecorator('cElcFlag',{initialValue: baseProfile ?　this.formaUnit(baseProfile.cElcFlag,'cElcFlag') : ''})(
                      <Input readOnly style={{color: isChange.cElcFlag === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='业务渠道'>
                    {getFieldDecorator('extEnterpCode',{initialValue: baseProfile ? baseProfile.extEnterpCode : ''})(
                      <Input readOnly style={{color: isChange.extEnterpCode === 0 ? '' : '#ff0000'}}/>
                  )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label='投保单号'>
                    {getFieldDecorator('cPlyAppNo',{initialValue: baseProfile ? baseProfile.cPlyAppNo : ''})(
                      <Input readOnly style={{color: isChange.cPlyAppNo === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col md={16} lg={16} sm={16}>
                  <Form.Item label={labelSource.postAddress}>
                    {getFieldDecorator('postAddress',{initialValue: baseProfile ? baseProfile.postAddress : ''})(
                      <Input readOnly style={{color: isChange.postAddress === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
              </Row>
            </Panel>

            <Panel header={labelSource.applyInfo} key="2">
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label={labelSource.applyClntNmeA}>
                    {getFieldDecorator('cAppName',{initialValue: baseProfile ? baseProfile.cAppName : ''})(
                      <Input readOnly style={{color: isChange.cAppName === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={labelSource.applySex}>
                    {getFieldDecorator('cAppSex',{initialValue: baseProfile ? this.format(baseProfile.cAppSex,"cAppSex") : ''})(
                      <Input readOnly style={{color: isChange.cAppSex === 0 ? '' : '#ff0000'}} />
                  )} 
                  </Form.Item>
                </Col>
                <Col xl={{span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label={labelSource.appBirth}>
                    {getFieldDecorator('cAppBorth',{initialValue: baseProfile ? baseProfile.cAppBorth : ''})(
                      <Input readOnly style={{color: isChange.cAppBorth === 0 ? '' : '#ff0000'}}/>
                  )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label={labelSource.applyCertfCls}>
                    {getFieldDecorator('cAppCertfType',{initialValue: baseProfile ? this.format(baseProfile.cAppCertfType,"cAppCertfType") : ''})(
                      <Input readOnly style={{color: isChange.cAppCertfType === 0 ? '' : '#ff0000'}}/>
                  )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={labelSource.applyCertfCde}>
                    {getFieldDecorator('cAppCertfNo',{initialValue: baseProfile ? baseProfile.cAppCertfNo : ''})(
                      <Input readOnly style={{color: isChange.cAppCertfNo === 0 ? '' : '#ff0000'}} />
                )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label={labelSource.applyMobile}>
                    {getFieldDecorator('cAppPhone',{initialValue: baseProfile ? baseProfile.cAppPhone : ''})(
                      <Input readOnly style={{color: isChange.cAppPhone === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.identifyLongValid}>
                    <Checkbox checked={baseProfile && baseProfile.identifyLongValidA === '1' ? true : false}>长期</Checkbox>
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.identifyExpiry}>
                    <Input
                      readOnly="true"
                      value={ baseProfile && baseProfile.identifyExpiryA ? moment(baseProfile.identifyExpiryA).format('YYYY-MM-DD') : ''}
                    />
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.country}>
                    <Select
                      style={{ width: '100%' }}
                      value={ baseProfile && baseProfile.countryA ? baseProfile.countryA : ''}
                      disabled
                    >
                      {this.renderSelectOption(CountryCode)}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label={labelSource.applyZipCde}>
                    {getFieldDecorator('cAppZip',{initialValue: baseProfile ? baseProfile.cAppZip : ''})(
                      <Input readOnly style={{color: isChange.cAppZip === 0 ? '' : '#ff0000'}} />
                )}                
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={labelSource.applyTel}>
                    {getFieldDecorator('applyTel',{initialValue: baseProfile ? baseProfile.applyTel : ''})(
                      <Input readOnly style={{color: isChange.applyTel === 0 ? '' : '#ff0000'}} />
                )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label={labelSource.applyMail}>
                    {getFieldDecorator('cEmail',{initialValue: baseProfile ? baseProfile.cEmail : ''})(
                      <Input readOnly style={{color: isChange.cEmail === 0 ? '' : '#ff0000'}} />
                )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label={labelSource.applyJobType}>
                    <Select
                      style={{ width: '100%' }}
                      value={ baseProfile && baseProfile.occupationCodeA ? baseProfile.occupationCodeA : ''}
                      disabled
                    >
                      {this.renderSelectOption(ItemAccOccupationCode)}
                    </Select>       
                  </Form.Item>
                </Col>
                <Col xl={{ span: 16, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label={labelSource.applyClntAddr}>
                    {getFieldDecorator('cAppAddr',{initialValue: baseProfile ? baseProfile.cAppAddr : ''})(
                      <Input readOnly style={{color: isChange.cAppAddr === 0 ? '' : '#ff0000'}} />
                )}
                  </Form.Item>
                </Col>
              </Row>
            </Panel>

            <Panel header={labelSource.insrncInfo} key="3">
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label={labelSource.insrncName}>
                    {getFieldDecorator('insrncName',{initialValue: baseProfile ? baseProfile.insrncName : ''})(
                      <Input readOnly style={{color: isChange.insrncName === 0 ? '' : '#ff0000'}} />
                )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={labelSource.insrncSex}>
                    {getFieldDecorator('insrncSex',{initialValue: baseProfile ? this.format(baseProfile.insrncSex,"insrncSex") : ''})(
                      <Input readOnly style={{color: isChange.insrncSex === 0 ? '' : '#ff0000'}} />
              )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label={labelSource.insrncBirth}>
                    {getFieldDecorator('insrncBorth',{initialValue: baseProfile ? baseProfile.insrncBorth : ''})(
                      <Input readOnly style={{color: isChange.insrncBorth === 0 ? '' : '#ff0000'}} />
                )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label={labelSource.insrncCardType}>
                    {getFieldDecorator('insrncCardType',{initialValue: baseProfile ? this.format(baseProfile.insrncCardType,"insrncCardType") : ''})(
                      <Input readOnly style={{color: isChange.insrncCardType === 0 ? '' : '#ff0000'}} />
                )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={labelSource.insrncCardId}>
                    {getFieldDecorator('insrncCardId',{initialValue: baseProfile ? baseProfile.insrncCardId : ''})(
                      <Input readOnly style={{color: isChange.insrncCardId === 0 ? '' : '#ff0000'}} />
                  )}
                </Form.Item>
                    </Col>
                    <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                      <Form.Item label={labelSource.cMobileNoI}>
                        {getFieldDecorator('insrncMobile',{initialValue: baseProfile ? baseProfile.insrncMobile : ''})(
                          <Input readOnly style={{color: isChange.insrncMobile === 0 ? '' : '#ff0000'}} />
                  )}
                </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.identifyLongValid}>
                    <Checkbox checked={baseProfile && baseProfile.identifyLongValidI === '1' ? true : false}>长期</Checkbox>
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.identifyExpiry}>
                    <Input
                      readOnly="true"
                      value={ baseProfile && baseProfile.identifyExpiryI ? moment(baseProfile.identifyExpiryI).format('YYYY-MM-DD') : ''}
                    />
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.country}>
                    <Select
                      style={{ width: '100%' }}
                      value={ baseProfile && baseProfile.countryI ? baseProfile.countryI : ''}
                      disabled
                    >
                      {this.renderSelectOption(CountryCode)}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label={labelSource.insrncPostCode}>
                    {getFieldDecorator('insrncZip',{initialValue: baseProfile ? baseProfile.insrncZip : ''})(
                      <Input readOnly style={{color: isChange.insrncZip === 0 ? '' : '#ff0000'}} />
                  )}                
                  </Form.Item>
                </Col>
                <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
                  <Form.Item label={labelSource.cTelI}>
                    {getFieldDecorator('insrncHomePhone',{initialValue:baseProfile ?  baseProfile.insrncHomePhone : ''})(
                      <Input readOnly style={{color: isChange.insrncHomePhone === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label={labelSource.cMailI}>
                    {getFieldDecorator('insrncMail',{initialValue: undefined === baseProfile ? '' : baseProfile.insrncMail})(
                      <Input readOnly  style={{color: isChange.insrncMail === 0 ? '' : '#ff0000'}} />
                  )} 
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={6} md={12} sm={24}>
                  <Form.Item label={labelSource.cAppprofType}>
                    <Select
                      style={{ width: '100%' }}
                      value={ baseProfile && baseProfile.occupationCodeI ? baseProfile.occupationCodeI : ''}
                      disabled
                    >
                      {this.renderSelectOption(ItemAccOccupationCode)}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xl={{ span: 16, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
                  <Form.Item label={labelSource.insrncAddress}>
                    {getFieldDecorator('insrncAddr',{initialValue: baseProfile　? baseProfile.insrncAddr : ''})(
                      <Input readOnly style={{color: isChange.insrncAddr === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
              </Row>
            </Panel>
            <Panel header={labelSource.beneTsrInfo} key="4">
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.tsrCode}>
                    {getFieldDecorator('tsrCode',{initialValue: baseProfile ?baseProfile.tsrCode : ''})(
                      <Input readOnly style={{color: isChange.tsrCode === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.tsrName}>
                    {getFieldDecorator('tsrName',{initialValue: baseProfile  ? baseProfile.tsrName : ''})(
                      <Input readOnly style={{color: isChange.tsrName === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label={labelSource.tsrTeam}>
                    {getFieldDecorator('tsrTeam',{initialValue: baseProfile ? baseProfile.tsrTeam : ''})(
                      <Input readOnly style={{color: isChange.tsrTeam === 0 ? '' : '#ff0000'}} />
                  )}
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                {/* 反洗钱业务需求修改页面 */}
                <Col md={8} lg={8} sm={16}>
                  <Form.Item label="受益方式">
                    <Input
                      readOnly="true"
                      value={ baseProfile && baseProfile.benfType === '1' ? '指定' : '法定'}
                    />
                  </Form.Item>
                </Col>
                {/* 当受益人只有投保人时，受益人直接是法定 */}
                {
                  baseProfile && baseProfile.benfType === '0' ? (
                    <Fragment>
                      <Col md={8} lg={8} sm={16}>
                        <Form.Item label="受益人姓名">
                          <Input
                            readOnly="true"
                            value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.benfClntNmeB}
                          />
                        </Form.Item>
                      </Col>
                      <Col md={8} lg={8} sm={16}>
                          <Form.Item label="受益人与被保人关系">
                            <Input
                              readOnly="true"
                              value={null===baseProfile || undefined === baseProfile ? '' : baseProfile.insrncRelation}
                            />
                          </Form.Item>
                      </Col>
                    </Fragment>
                  ) : null
                }
                {
                  baseProfile.benfType === '1' ? this.state.SingleShouYiRenList.map((item) =>{
                    return (
                      <div className={baseProfile.benfUpFlag === '1' ? 'changeBaseProfile' : '' } style={{ padding: '0 10px'}}>
                        <SingleShouYiRen
                          policyMapInfo={item}
                          isSameWithTouBaoRen={true}
                          isDeleteBeneificiary={true}
                          key={item}
                          index={item}
                        />
                      </div>
                    );
                  }) : null
                }
              </Row>
            </Panel>
            <Panel header="批文与相关资料下载" key="5">
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
            <Col md={16} lg={16} sm={16}>
              <Form.Item label={
                <span>
                 批文
                 <em>
                   <Tooltip title="">
                       <Icon type="info-circle-o" style={{ marginLeft : 2, marginRight: 4 }} />
                   </Tooltip>
                 </em>
                </span>}>
                {getFieldDecorator('endorText',{initialValue: baseProfile ? (baseProfile.endorText ? baseProfile.endorText : '') : ''})(
                <TextArea style={{ width: '100%', height: '100px' }} readOnly/>
                 )}
              </Form.Item>
            </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }} style={{marginTop: 6}}>
              <Col md={16} lg={16} sm={16}>
              <Form.Item label="相关资料下载或预览">
              <div>{buttonArr.map((item)=>{return item})}</div>
              </Form.Item>
              </Col>
              </Row>
            </Panel>
          </Collapse>
          <div style={{width: '100%', textAlign: 'center', marginTop: 60 }}>
            <Button type="primary" loading={loading}  style={{marginRight: 40}} onClick={this.correctPass}>
              审核通过
            </Button>
            <Button loading={loading} style={{marginLeft: 40, width: 88 }} onClick={this.showModal}>
              打回
            </Button>
          </div>
          <Modal
            visible={this.state.visible}
            onOk={this.addOk}
            confirmLoading={this.state.confirmLoading}
            onCancel={this.addCancel}
          >
            <Form>
              <Form.Item
                label="打回原因"
              >
                {getFieldDecorator('remark', {
                        initialValue: '',
                        rules: [
                          { required:  true, message: '输入不能为空' },
                        ],
                    })(
                      <TextArea onChange = {this.remarkChange} placeholder="请填写打回原因" />
                        )}
              </Form.Item>
            </Form>
          </Modal>
        </Form>
      </PageHeaderLayout>
    );
  }
}
