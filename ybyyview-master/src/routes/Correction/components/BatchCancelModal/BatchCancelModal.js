import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Button, Modal, Form, Select, Row, Col, Upload, Icon, message, Divider,
} from 'snk-web';

@connect(({ loading, correctionManage}) => ({
  correctionManage,
  loading: loading.effects,
}))
@Form.create()

export default class BatchCancelModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fileList: [],
      isVisible: false,
      modalContent: '',
    }
  }

  // 文件上传
  importFile = () => {
    this.props.form.validateFields((err, formValues) => {
      if (err) return;
      formValues.uploadFile = this.state.fileList[0];
      this.props.dispatch({
        type: 'correctionManage/importPendingCancelExcl',
        payload: formValues,
      }).then(result => {
        const { code, message: msg, data } = result || {};
        if (code) {
         this.props.uploadModalCallback(msg);
        } else {
          this.props.uploadModalCallback(data);
        }
      });
    })
  }

  onCancel = () => {
    this.props.uploadModalCallback();
  }

  render() {
    const { form: { getFieldDecorator } } = this.props;
    const { fileList } = this.state;
    const props = {
      onRemove: file => {
        this.setState(state => {
          const index = state.fileList.indexOf(file);
          const newFileList = state.fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: file => {
        this.setState(state => ({
          fileList: [...state.fileList, file],
        }));
        return false;
      },
      fileList,
    }
    return (
      <div>
      <Modal
        title='导入文件'
        visible={this.props.isUploadModalVisible}
        centered
        okText='上传'
        okType='primary'
        onCancel={this.onCancel}
        onOk={this.importFile}
        confirmLoading={this.props.loading['correctionManage/importPendingCancelExcl']}
      >
        <Form className='custom-form-wrapper' layout="inline">
          <Row>
            <Col md={24} sm={24} style={{ textAlign: 'center' }}>
              <Form.Item label='文件类型'>
                {
                  getFieldDecorator('datatType', {
                    rules: [{ required: true, message: '文件类型为必录项' }],
                  })(
                    <Select allowClear style={{ width: '100%' }}>
                      <Select.Option value='1'>不合规件</Select.Option>
                      <Select.Option value='2'>请款失败100天</Select.Option>
                      <Select.Option value='3'>续单首期无请款</Select.Option>
                    </Select>
                  )
                }
              </Form.Item>
            </Col>
            <Col md={24} sm={24}>
              <Form.Item label='文件上传'>
                <Upload {...props}>
                  <Button>
                    <Icon type="upload" />选取文件
                  </Button>
                </Upload>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
      </div>
    );
  }
}