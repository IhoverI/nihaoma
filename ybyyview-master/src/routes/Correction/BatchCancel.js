import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Button, Form, Select, Row, Col, Tag, Card, DatePicker, Input, Table, message, Modal,
} from 'snk-web';
const { RangePicker } = DatePicker;
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ChannelAndProduction from '../../components/ChannelAndProduction';
import BatchCancelModal from './components/BatchCancelModal/BatchCancelModal';
import {extEnterpOPtsFn, extEnterpDescFunc} from './BatchCancelconfig';


@connect(({ correction, loading}) => ({
  correction,
  loading: loading.effects,
}))
@Form.create()
export default class BatchCancel extends PureComponent{
  state = {
    pageNum: 1,
    pageSize: 50,
    total: 0,
    isUpload: false,
    uploading: false,
    batchCancelData: [],
    selectedRowKeys : [],
    totalprm: 0,
    isBatchCancelResultModalVisible: false,
    batchCancelResultContent: '',
    isUploadModalVisible:false

  }

  // 重置表单
  handleFormReset = () => {
    this.props.form.resetFields();
  }

  // 取消modal蒙板
  uploadModalCallback = (data) => {
    console.log(data)
    if(!data){
      this.setState({
        isUploadModalVisible: false,
      });
      return;
    }
    this.setState({
      isUploadModalVisible: false,
      isBatchCancelResultModalVisible: true,
      batchCancelResultContent: data,
    });
  }

  handleSearch = e => {
    e.preventDefault();
    this.setState({
      pageNum: 1,
      pageSize: 50,
    });
    this.queryBatchCancel(1, 50);
  };

  queryBatchCancel = (pageNum, pageSize) => {
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) return;
      // console.log(fieldsValue)
      fieldsValue = this.prepare(fieldsValue);
      this.setState({
        cancelFlag: fieldsValue.cancelFlag,
        selectedRowKeys: [],
      })
      this.props.dispatch({
        type: 'correctionManage/queryCancelData',
        payload: {
          ...fieldsValue,
          pageNum,
          pageSize,
        },
      }).then(result => {
        const { data: { list, total }, code, message: msg } = result;
        if (code === 0) {
          message.success(msg || '查询成功');
            this.setState({
              batchCancelData: list,
              total,
            });
        } else {
          message.error(msg || '查询失败');
        }
      })
    })
  }

  prepare = (values) => {
    if (values.importDate && values.importDate.length) {
      values.importBeginDate = values.importDate[0].format('YYYY-MM-DD');
      values.importEndDate = values.importDate[1].format('YYYY-MM-DD');
    }
    delete values.importDate;

    if (values.cancelDate && values.cancelDate.length) {
      values.cancelBeginDate = values.cancelDate[0].format('YYYY-MM-DD');
      values.cancelEndDate = values.cancelDate[1].format('YYYY-MM-DD');
    }
    delete values.cancelDate;

    for( let key in values) {
      if (!values[key]) {
        delete values[key];
      }
    }
    if(values.extEnterpCode && !values.extEnterpCode.length)  values.extEnterpCode = null;
    return values;
  }

  // 选中表格行
  onSelectChange = (selectedRowKeys, selectedRows) => {
    const totalprm = selectedRows.reduce((prev, curr) => prev + curr.prm, 0);
    this.setState({ selectedRowKeys, selectedRows, totalprm });
  };

  // 批量注销
  batchCancel = () => {
    if (!this.state.selectedRowKeys.length) return;
    this.props.dispatch({
      type: 'correctionManage/batchCancel',
      payload: {
        list: [...this.state.selectedRowKeys],
      }
    }).then(result => {
      const { code, message: msg } = result;
      if (code === 0) {
       this.setState({
        isBatchCancelResultModalVisible: true,
        batchCancelResultContent: msg,
       })
        this.queryBatchCancel(this.state.pageNum, this.state.pageSize);
      } else {
        message.error(msg || '批量注销失败');
      }
    })
  }

  // 列表状态发生改变
  handleTableChange =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.queryBatchCancel(pagination.current,pagination.pageSize);
    });
  }

  exportExcel = () => {
    // 导出数据
    let { dispatch, form } = this.props;
    const { pageNum, pageSize } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue = this.prepare(fieldsValue);
      dispatch({
        type: 'correctionManage/exportPendingCancelExcl',
        payload: {
          ...fieldsValue,
          pageNum,
          pageSize,
        },
      }).then(response=>{
        if(response.code!==0){
          message.error(response.message)
        }
      });
    })

  }

  // 表格数据
  Columns = [
    {
      title: '业务渠道',
      dataIndex: 'extEnterpCode',
      render: (text, record, index) => extEnterpDescFunc(text),
      align: 'center',
    },
    {
      title: '订单号',
      dataIndex: 'caseDescribe',
      align: 'center',
    },
    {
      title: '保单号',
      dataIndex: 'cPlyNo',
      align: 'center',
    },
    {
      title: '原保单号',
      dataIndex: 'cPlyNoOld',
      align: 'center',
    },
    {
      title: '保单状态',
      dataIndex: 'flag',
      // 1-有效，2-注销，3-退保，4-过期
      render: (text, record, index) => ['','有效','注销', '退保', '过期'][text],
      align: 'center',
    },
    {
      title: '投保人姓名',
      dataIndex: 'clntName',
      align: 'center',
    },
    {
      title: '年化保费',
      dataIndex: 'prm',
      align: 'center',
    },
    {
      title: '投保日期',
      dataIndex: 'inputDate',
      align: 'center',
    },
    {
      title: '成功扣款期次',
      dataIndex: 'succeedPeriod',
      align: 'center',
    },
    {
      title: '最近请款日期',
      dataIndex: 'recentlyPayDate',
      align: 'center',
    },
    {
      title: '最近扣款状态',
      dataIndex: 'chargeFlag',
      align: 'center',
    },
    {
      title: '最近扣款描述',
      dataIndex: 'chargeDescribe',
      align: 'center',
    },
    {
      title: '导入日期',
      dataIndex: 'importDate',
      align: 'center',
    },
    {
      title: '注销日期',
      dataIndex: 'cancelDate',
      align: 'center',
    },
    {
      title: '导入操作账号',
      dataIndex: 'importOperator',
      align: 'center',
    },
    {
      title: '注销操作账号',
      dataIndex: 'cancelOperator',
      align: 'center',
    },
  ]
  render() {
    const { form, loading } = this.props;
    const { getFieldDecorator } = form;
    const { total, pageNum, pageSize, isUpload, selectedRowKeys, totalprm } = this.state;
    // const {  } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      getCheckboxProps: record => ({
        disabled: record.flag === '2',
      }),
    };
    const hasSelected = selectedRowKeys.length > 0;
    const { datatType, extEnterpCode } = form.getFieldsValue(['datatType', 'extEnterpCode']) || {};
    return (
      <PageHeaderLayout>
        <Card bordered={false}>
          <Form className='custom-form-wrapper' layout="inline" onSubmit={this.handleSearch}>
            <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <Form.Item label='数据类型'>
                  {
                    getFieldDecorator('datatType', {
                      rules: [{ required: true, message: '文件类型为必录项' }],
                    })(
                      <Select allowClear style={{ width: '100%' }}>
                        <Select.Option value='1'>不合规件</Select.Option>
                        <Select.Option value='2'>请款失败100天</Select.Option>
                        <Select.Option value='3'>续单首期无请款</Select.Option>
                      </Select>
                    )
                  }
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label='渠道名称'>
                  {
                    getFieldDecorator('extEnterpCode', {
                    })(
                      <Select allowClear mode="multiple" style={{ width: '100%' }}>
                        {
                          extEnterpOPtsFn(datatType).map((item)=>
                          <Select.Option key={item.code} value={item.code}>{item.name}</Select.Option>
                          )
                        }
                      </Select>
                    )
                  }
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label="保单号">
                    {
                      getFieldDecorator('cPlyNo',{
                        rules: [{ max: 30, message: '请输入正确的格式' }],
                      })(<Input placeholder="请输入" />)
                    }
                </Form.Item>
              </Col>
   
              <Col md={12} sm={24}>
                <Form.Item label='清单导入日期'>
                  {
                    getFieldDecorator('importDate')(
                      <RangePicker style={{ width: '100%' }}  />
                    )
                  }
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label='批量注销日期'>
                  {
                    getFieldDecorator('cancelDate')(
                      <RangePicker style={{ width: '100%' }}  />
                    )
                  }
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label='注销状态'>
                  {
                    getFieldDecorator('cancelFlag')(
                      <Select allowClear style={{ width: '100%' }}>
                        <Select.Option value='1'>未注销</Select.Option>
                        <Select.Option value='2'>已注销</Select.Option>
                      </Select>
                    )
                  }
                </Form.Item>
              </Col>
              {extEnterpCode && extEnterpCode.indexOf('95555') > -1 && 
                <Col md={12} sm={24}>
                  <Form.Item label='地区'>
                    {
                      getFieldDecorator('cDptCde')(
                        <Select allowClear placeholder='请选择'>
                          <Select.Option value='0106880101'>上海</Select.Option>
                          <Select.Option value='0106880104'>成都</Select.Option>
                        </Select>
                      )
                    }
                  </Form.Item>
              </Col>
              }
            </Row>
            <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
              <span style={{ marginBottom: 24 }}>
                <Button htmlType="submit" type="primary" >
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </span>
            </div>
          </Form>
          <Table
            rowSelection={rowSelection}
            dataSource={this.state.batchCancelData}
            loading={loading['correctionManage/queryCancelData']}
            rowKey={record => record.cPlyNo}
            scroll={{x: 1400}}
            bordered
            title={()=>{
              return (
                <Fragment>
                  <Button disabled={this.state.printDisabled} loading={loading['correctionManage/batchCancel']} type='primary' onClick={this.batchCancel}>批量注销</Button>
                  <Button type='primary'  style={{ marginLeft: '10px'}} onClick={() => { this.setState({ isUploadModalVisible: true }) }}>文件导入</Button>
                  <Button loading={loading['correctionManage/queryCancelData']} type='primary' style={{ marginLeft: '10px' }} onClick={this.exportExcel}>导出清单</Button>
                  {
                    hasSelected ? (
                      <Tag color="magenta">{`已选 ${selectedRowKeys.length} 条，合计保费${totalprm}`}</Tag>
                    ) : null
                  }
                </Fragment>
              )
            }}
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current: pageNum,
              pageSize: pageSize,
              total:total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            columns={this.Columns}
            onChange={this.handleTableChange}
          />
        </Card>
        <BatchCancelModal isUploadModalVisible={this.state.isUploadModalVisible} uploadModalCallback={this.uploadModalCallback} />
        <Modal
          width="240px"
          closable={false}
          footer={null}
          visible={this.state.isBatchCancelResultModalVisible}         
        >
          <p>{this.state.batchCancelResultContent}</p>
           <div style={{textAlign: 'center'}}>
            <Button type="primary" onClick={()=>{this.setState({isBatchCancelResultModalVisible: false})}}>确定</Button>
           </div>
          
        </Modal>
      </PageHeaderLayout>
    );
  }
}