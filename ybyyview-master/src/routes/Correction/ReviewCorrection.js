import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Table
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource, getSelectChild, timestampToString } from '../../utils/utils'
import styles from './ReviewCorrection.less';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;

@connect(({ correctionManage, loading }) => ({
  correctionManage,
  loading: loading.models.correctionManage,
}))
@Form.create()
export default class ReviewCorrection extends PureComponent {
  state = {
    pageNum: 1,
    rowNum: 10,
    isRenewal: true,
    formValues: {}
  };

  getColumns = () => {
    let { loading } = this.props;
    return [
      {
        title: '保单号',
        dataIndex: 'cPlyNo',
        align: 'center',
        render: (text, record) => <a href={`/#/query-manage/policy-list?cPlyNo=${record.cPlyNo}&prePath=/correction/review-correction`}>{text}</a>,
      },
      {
        title: '产品名称',
        dataIndex: 'prodName',
        align: 'center'
      },
      {
        title: '款别',
        dataIndex: 'feeType',
        align: 'center'
      },
      {
        title: '保单状态',
        dataIndex: 'flag',
        align: 'center'
      },
      {
        title: '保险起期',
        dataIndex: 'tStartTm',
        align: 'center'
      },
      {
        title: '保险止期',
        dataIndex: 'tEndTm',
        align: 'center'
      },
      {
        title: '被保人姓名',
        dataIndex: 'cClientName',
        align: 'center'
      },
      {
        title: '投保人姓名',
        dataIndex: 'cAppName',
        align: 'center'
      },
      {
        title: '续保次数',
        dataIndex: 'renwalNumber',
        align: 'center'
      },
      {
        title: '续保状态',
        dataIndex: 'renwalFlag',
        align: 'center',
        render: (text, record) => {
          if (record.renwalFlag === '1') {
            return '取消续保'
          } else if (record.renwalFlag === '0') {
            return '待续保'
          } else {
            return '已续保'
          }
        },
      },
      {
        title: '成功扣款期次',
        dataIndex: 'times',
        align: 'center'
      },
      {
        title: '取消续保短信发送日期',
        dataIndex: 'renwalTime',
        align: 'center',
        render: (text, record) => {
          return (
            <DatePicker disabled={record.renwalFlag !== '0'} disabledDate={this.disabledDate} allowClear={false} defaultValue={text ? moment(text, 'YYYY-MM-DD') : text}
              onChange={(date, dateString) => {
                date ? record.renwalTime=dateString : undefined;
              }} />
          )
        }
      },
      {
        title: '备注',
        dataIndex: 'remark',
        align: 'center',
        width: 140,
        render: (text, record) => {
          return (
            <Select disabled={record.renwalFlag !== '0'} id={`${record.cPlyNo}`} placeholder='取消续保时选择' defaultValue={text} style={{ width: '100%' }}
              onSelect={(value) => {record.remark=value}}>
              <Option value='2'>客户不续保</Option>
              <Option value='3'>有理赔</Option>
              <Option value='A'>没有扣款记录</Option>
              <Option value='1'>其它</Option>
            </Select>
          )
        }
      },
      {
        title: '操作',
        align: 'center',
        render: (text, record) => {
          if (record.renwalFlag === '1') {
            return <Button type='primary' loading={loading} onClick={() => this.showConfirmRenew(record)}>恢复续保</Button>
          } else if (record.renwalFlag === '0') {
            return <Button type='primary' loading={loading} onClick={() => this.showConfirmCancel(record)}>取消续保</Button>
          } else {
            return <Button disabled loading={loading} onClick={() => this.cancelRenewal(record)}>已续保</Button>
          }
        },
      }
    ]
  }

  componentWillMount(){
    if(this.props.history.location.state){
      this.props.history.push({state: undefined});
    } else{
      Object.keys(this.props.correctionManage).forEach(key => delete this.props.correctionManage[key]);
    }
  }


  disabledDate(current) {
    return current && current < moment().add(-1, 'days').endOf('day');
  }

  showConfirmRenew = (record) => {
    Modal.confirm({
      title: '确定提交?',
      content: '恢复续保，短信即时发送',
      onOk: () => this.renewRenewal(record),
    });
  }

  renewRenewal = (record) => {
    let { dispatch } = this.props;
    // 恢复续保，不用选择日期 2018.08.24
    // let inputRenwalTime = record.renwalTime;
    // if (inputRenwalTime === undefined) {message.error('请选择日期'); return}
    let values = {
      cPlyNo: record.cPlyNo,
      renwalFlag: '0',
    }
    dispatch({
      type: 'correctionManage/executeRenwal',
      payload: values
    }).then(() => {
      const { correctionManage: { CancelOrRenewalResp } } = this.props;
      if (CancelOrRenewalResp.code === 0) {
        this.success(CancelOrRenewalResp.message);
      } else {
        this.error(CancelOrRenewalResp.message);
      }
    })
  }

  showConfirmCancel = (record) => {
    Modal.confirm({
      title: '确定提交?',
      content: '取消续保，短信根据所选时间发送',
      onOk: () => this.cancelRenewal(record),
    });
  }

  cancelRenewal = (record) => {
    let { dispatch } = this.props;
    let inputRemark = record.remark;
    let inputRenwalTime = record.renwalTime;
    if (inputRenwalTime === undefined) {message.error('请选择日期'); return}
    if (inputRemark === '' || inputRemark === undefined) {message.error('请填写备注'); return};
    let values = {
      cPlyNo: record.cPlyNo,
      remark: record.remark,
      renwalFlag: '1',
      renwalTime: inputRenwalTime
    }
    dispatch({
      type: 'correctionManage/executeRenwal',
      payload: values
    }).then(() => {
      const { correctionManage: { CancelOrRenewalResp } } = this.props;
      if (CancelOrRenewalResp.code === 0) {
        this.success(CancelOrRenewalResp.message);
      } else {
        this.error(CancelOrRenewalResp.message);
      }
    })
  }

  success = (message) => {
    Modal.success({
      title: '提交成功',
      content: message,
      onOk: this.handleSearch,
    });
  }
  
  error = (message) => {
    Modal.error({
      title: '提交失败',
      content: message,
      onOk: this.handleSearch,
    });
  }

  componentDidMount = () => {
    let cPlyNo = this.props.location.cPlyNo;
    if (cPlyNo) {
      this.props.form.setFieldsValue({cPlyNo, cPlyNo})
      let searchTerm = {
        cPlyNo: cPlyNo
      };
      this.props.dispatch({
        type: 'correctionManage/cancelReviewQuery',
        payload: searchTerm,
      });
    }
  }

  handleSearch = () => {
    this.setState({
      pageNum: 1,
      rowNum: 10
    })
    this.searchData(1,10);
  };

  searchData = (pageNum,rowNum) => {
    const { dispatch, form } = this.props;
    if(!form){
      message.warning('非最新数据，请重新搜索查询。');
      return;
    }
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        cClientName: fieldsValue.cClientName,
        cPlyNo: fieldsValue.cPlyNo,
        cardId: fieldsValue.cardId,
        appCardId: fieldsValue.appCardId,
        pageNum: pageNum,
        rowNum: rowNum,
      };
      this.setState({
        formValues: values
      })
      dispatch({
        type: 'correctionManage/cancelReviewQuery',
        payload: values,
      })
    })
  }

  handleFormReset = () => {
    const { form } = this.props;
    this.setState({pageNum:1,rowNum:10});
    form.resetFields();
  };

  handleTableChange = (page, pageSize) => {
    let { dispatch } = this.props;
    this.setState({
      pageNum: page,
      rowNum: pageSize
    })
    this.searchData(page,pageSize);
  }

  render() {
    const { correctionManage: { ReviewQueryData }, loading } = this.props;
    const { selectedRows, modalVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    let cPlyNo = this.props.location.cPlyNo;
    let list = [];
    let total = 1;

    if (ReviewQueryData) {
      if (ReviewQueryData.code == 0) {
        list = ReviewQueryData.data.list;
        total = ReviewQueryData.data.count;
      }
    }

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
            <Form className='custom-form-wrapper custom-form-wrapper-1' layout="inline">
            <Row gutter={10}>
              <Col md={12} sm={24}>
                <FormItem label="保单号">
                  {getFieldDecorator('cPlyNo',{initialValue: cPlyNo ? cPlyNo : '',
                 rules: [{
                    max: 30, message: '请输入正确的格式',
                  }],
              })
                  (<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label="被保人姓名">
                  {getFieldDecorator('cClientName')(<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
            </Row>
            <Row gutter={10}>
              <Col md={12} sm={24}>
                <FormItem label="投保人证件号码">
                  {getFieldDecorator('appCardId')(<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label="被保人证件号码">
                  {getFieldDecorator('cardId')(<Input placeholder="请输入" />)}
                </FormItem>
              </Col>
            </Row>
            <div style={{textAlign:'center',padding:12}}>
               <Button loading={loading} type="primary" onClick={this.handleSearch}>
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
            </div>
            </Form>
            <Table
              loading={loading}
              scroll={{x: 1500}}
              rowKey={record => record.cPlyNo}
              dataSource={list}
              bordered
              size="small"
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                current:this.state.pageNum,
                pageSize:this.state.pageSize,
                total:total,
                onChange: (page, pageSize) => this.handleTableChange(page, pageSize),
                pageSizeOptions:['10','20','30','40','50'],
              }}
              columns={this.getColumns()}
            />
        </Card>
      </PageHeaderLayout>
    );
  }
}
