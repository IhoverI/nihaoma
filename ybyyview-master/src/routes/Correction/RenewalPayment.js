import { 
    Table, 
    Row,
    Col,
    Card,
    Form,
    Input,
    Select,
    Icon,
    Button,
    DatePicker,
    Popconfirm,
    Modal,
    Tooltip,
    message} from 'snk-web';
import React from 'react';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getSelectChild,getRegularCheck,getProductNameByExtEnterpCodeAndProdNo } from '../../utils/utils'
import styles from './FastmoneyCorrection.less';

const FormItem = Form.Item;
const formItemStyle = {
    labelCol: {
        xs: { span: 12 },
        sm: { span: 8 },
    },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 16 },
    },
  };

const CreateForm = Form.create()(props => {
    const { modalVisible,fastData,isDebitCard,isJiejiCard,cPlyNoOld,form,
        handleModalVisible,thirdCardTypeChange,handleSubmit } = props;

    //表单值
    const policyProfile = null == fastData.data ? undefined : fastData.data;
    const isSuccess = fastData.code === 0 ? true : false;
    
    const okHandle = () => {
        form.validateFields((err, fieldsValue) => {
            if (err) return;
            form.resetFields();
            handleSubmit(fieldsValue);         //真正提交数据
        });
    };

    const cancelHandle = () => {    
        form.resetFields();                     //重置
        handleModalVisible(false,'');
    }

    const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
    };


    return (
        <Modal
            title="转快钱继续请款"
            visible={modalVisible}
            style={{width:'800px'}}
            onOk={okHandle}
            onCancel={cancelHandle}
            // onCancel={() => handleModalVisible(false,'')}
        >
            <div className={styles.searchListItem}>
            <div className={styles.sectionTitle}>保单信息：</div>
              <FormItem
                {...formItemLayout}
                label={
                  <span>
                    保单号
                    { isSuccess ? 
                      <em className={styles.optional}>
                        <Tooltip title="点击跳转至保单详情页">
                          <a  href={`/#/query-manage/policy-list?cPlyNo=${policyProfile.cPlyNo}&prePath=/correction/fastmoney-correction`} >
                            （详情）
                            <Icon type="info-circle-o" style={{ marginRight: 4 }} />
                          </a>
                        </Tooltip>
                      </em> : null
                    }
                  </span>
                }
              >
               {form.getFieldDecorator('cPlyNo',
                {initialValue : undefined === policyProfile ? '' : policyProfile.cPlyNo })
                (
                    <Input
                        style={{ width: '100%' }}
                        readOnly="true"
                        placeholder=""
                    />
                )}
              </FormItem>

              <FormItem {...formItemLayout} >
                {form.getFieldDecorator('cPlyNoOld', 
                {initialValue: cPlyNoOld })
                (<Input style={{ display: 'none' }} placeholder="" />)}
              </FormItem>

              <FormItem {...formItemLayout} label="被保人姓名">
                {form.getFieldDecorator('cInsrntCnm',{initialValue : undefined === policyProfile ? '' : policyProfile.cInsrntCnm})
                    (<Input
                    style={{ width: '100%' }}
                    readOnly="true"
                    placeholder=""
                    />) }
              </FormItem>
              <FormItem {...formItemLayout} label="银行渠道">
                 {form.getFieldDecorator('oldEnterpCode',
                  {initialValue: undefined === policyProfile ? '' : policyProfile.oldEnterpCode})
                  (<Select placeholder="请选择" disabled>
                    {getSelectChild('extEnterpDesc')}
                    </Select>
                  )}
              </FormItem>
            </div>
            <div className={styles.sectionTitle}>第三方扣款信息：</div>
            <div className={styles.searchListItem}>
                <FormItem {...formItemLayout}  label="第三方扣款客户证件类型">
                    {form.getFieldDecorator('thirdCertType',
                    {initialValue: undefined === policyProfile ? '' : policyProfile.thirdCertType},{
                    rules: [{ required: true, message: '必录项！' }],
                    })(
                        <Select disabled placeholder="请选择" style={{ width: '100%' }}>
                        {getSelectChild('certiDesc')}
                        </Select>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="扣款客户证件号码">
                    {form.getFieldDecorator('thirdCertNo',
                    {initialValue: undefined === policyProfile ? '' : policyProfile.thirdCertNo},getRegularCheck('certNo'))(
                    <Input disabled style={{ width: '100%' }} placeholder="" />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="第三方扣款银行名称">
                    {form.getFieldDecorator('thirdBank',
                    {
                        rules: [{
                            required: true, message: '请选择扣款银行!',
                        }],
                        initialValue: undefined === policyProfile ? '' : policyProfile.thirdBank
                    })
                    (
                        <Select placeholder="请选择" style={{ width: '100%' }}>
                        {getSelectChild('bank')}
                        </Select>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="第三方扣款开户行名称">
                    {form.getFieldDecorator('thirdBranchBank',
                    {
                        rules: [{
                            required: true, message: '请输入第三方扣款开户行名称!',
                        }],
                        initialValue: undefined === policyProfile ? '' : policyProfile.thirdBranchBank
                    })
                    (
                    <Input placeholder='需要手动输入'></Input>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="第三方扣款账户类型">
                    {form.getFieldDecorator('thirdCardType',
                    {
                        rules: [{
                            required: true, message: '请选择账户类型!',
                        }],
                        initialValue: undefined === policyProfile ? '' : policyProfile.thirdCardType
                    },
                    {rules: [{ required: true, message: '必录项！' }]})
                    (
                    <Select placeholder="请选择" style={{ width: '100%' }}
                        onChange={(value) => thirdCardTypeChange(value)} >
                        <Select.Option value='0201'>借记卡</Select.Option>
                        <Select.Option value='0204'>存折</Select.Option>
                    </Select>
                    )}
                </FormItem>
                { isDebitCard ?
                <FormItem {...formItemLayout} label="第三方扣款所属城市">
                    {form.getFieldDecorator('thirdPayCity',
                    {
                        rules: [{
                            required: true, message: '请输入第三方扣款所属城市!',
                        }],
                        initialValue: undefined === policyProfile ? '' : policyProfile.thirdPayCity
                    })
                    (
                    <Input placeholder='借记卡时必填'/>
                    )}
                </FormItem> : null
              }
                <FormItem {...formItemLayout} label="第三方扣款账号姓名">
                    {form.getFieldDecorator('thirdAppName',
                    {
                        rules: [{
                            required: true, message: '请输入账户姓名!',
                        }],
                        initialValue: undefined === policyProfile ? '' : policyProfile.thirdAppName
                    })
                    (
                    <Input style={{ width: '100%' }} placeholder="" />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="第三方扣款账号">
                    {form.getFieldDecorator('thirdCardId',
                    {
                        rules: [
                            { required: true, message: '请输入扣款账号' },
                            { pattern: /^(\d+)((?:\.\d+)?)$/, message: '请输入合法卡号' },
                        ],
                        initialValue: undefined === policyProfile ? '' : policyProfile.thirdCardId
                    })(
                    <Input style={{ width: '100%' }} placeholder="" />
                    )}
                </FormItem>
                {form.getFieldDecorator('cPlyAppNo', 
                {initialValue: undefined === policyProfile ? '' : policyProfile.cPlyAppNo})
                (<Input style={{ display: 'none' }} placeholder="" />)}
            </div>
        </Modal>
    );
});


//装饰器
@connect(({ renewalPaymentMdl,fastMoney, loading }) => ({
    renewalPaymentMdl,
    fastMoney,
    loading: loading.models.renewalPaymentMdl,
}))

@Form.create()
export default class RenewalPayment extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            dataSource:[], 
            pageNum: 1,
            pageSize: 10,
            total:1,
            modalVisible: false,    //弹出框属性
            fastData: {},           //保单号带出的相关数据
            cPlyNoOld:"",           //原保单号
            isJiejiCard: true,
            isDebitCard:true,
        };
        this.columns = [
            {
                title: '操作',
                align: 'center',
                width:'300px',
                render: (record) => {
                    return <div>
                        <Popconfirm title="是否挽回成功并继续请款？"  onConfirm={() => this.goOnRenewal(record.cPlyNoOld)}
                          onCancel={() => this.cancel()} okText="是" cancelText="否">
                            <Button type='primary' >原渠道继续请款</Button>
                        </Popconfirm>
                        &nbsp;&nbsp;
                        <Button type='primary' onClick={() => this.quickRenewal(record)} >转快钱继续请款</Button>
                    </div>
                },
            },
            {
                title: '保单号',
                dataIndex: 'cPlyNo',
                align: 'center',
                width:'150px',
                render: (text, record) => <a href={`/#/query-manage/policy-list?cPlyNo=${record.cPlyNo}&prePath=/correction/review-correction`}>{text}</a>,
            },
            {
                title: '保单状态',
                dataIndex: 'flag',
                width:'120px',
                align: 'center',
                render: (text, record) => {
                    if (record.flag === '1') {
                        return '有效'
                    } else if (record.flag === '2') {
                        return '注销'
                    } else if (record.flag === '3'){
                        return '退保'
                    }else {
                        return '过期'
                    }
                },
            },
            {
                title: '投保人姓名',
                dataIndex: 'cAppNme',
                width:'120px',
                align: 'center'
            },
            {
                title: '投保人证件号',
                dataIndex: 'insrncCardNo',
                width:'150px',
                align: 'center'
            },
            {
                title: '被保人姓名',
                dataIndex: 'cInsrntCnm',
                width:'120px',
                align: 'center'
            },
            {
                title: '产品名称',
                dataIndex: 'prodNo',
                width:'120px',
                align: 'center',
                render:(text, record)=>{
                    return getProductNameByExtEnterpCodeAndProdNo(record.extEnterpCode, text);
                },
            },
            {
                title: '产品款别',
                dataIndex: 'cTgtFld1',
                width:'120px',
                align: 'center'
            },
            {
                title: '年化保费',
                dataIndex: 'prm',
                width:'120px',
                align: 'center'
            },
            {
                title: '每期保费',
                dataIndex: 'eachPrm',
                width:'120px',
                align: 'center'
            },
            {
                title: '续保次数',
                dataIndex: 'renewalTime',
                width:'120px',
                align: 'center'
            },  
            {
                title: '原保单成功扣款期次',
                dataIndex: 'periodLast',
                width:'120px',
                align: 'center'
            },
            {
                title: '原保单已缴保费',
                dataIndex: 'paid',
                width:'120px',
                align: 'center'
            },
            {
                title: '扣款渠道',
                dataIndex: 'extEnterpCode',
                width:'120px',
                align: 'center'
            },
            {
                title: '扣款卡号',
                dataIndex: 'cardId',
                width:'150px',
                align: 'center'
            },
            {
                title: '原保单',
                dataIndex: 'cPlyNoOld',
                width:'150px',
                align: 'center',
            }
            
        ];
    }

    componentDidMount() {
        // this.setState({fastData:this.props.fastMoney.fastData});
    }

    cancel = (e) =>{
        console.log(e);
        message.error('Click on No');
    }

    //原渠道继续请款
    goOnRenewal = (cPlyNoOld) => {
        const { dispatch } = this.props;
        let params = {
            cPlyNoOld : cPlyNoOld,
            type : "1",
						operationType: '0',
        };
        dispatch({
            type: 'renewalPaymentMdl/updateRenewalThirdFlag',
            payload:params
        }).then(()=>{  
            setInterval(message.success("提交成功"),3000);       
            const newData = [...this.state.dataSource];
            const index = newData.findIndex(item => item.cPlyNoOld === cPlyNoOld );           
            if (index > -1) {
                this.setState({ dataSource: newData.filter(item => item.cPlyNoOld !== cPlyNoOld) });  
            }
        }).catch((e)=>{
            console.log(e);
        });
    }

    
    quickRenewal = (record) => {
        this.handleModalVisible(true,record);      //调用弹出模态框
    }

    //转快钱继续请款 弹出框
    handleModalVisible = (flag,record) => {
        // this.props.form.resetFields();
        this.setState({
            modalVisible: !!flag,
        });
     
        //存在保单号才传参请求数据
        if(record.cPlyNo !== undefined && record.cPlyNo !== null ){
            const {dispatch} = this.props;
            //拼接参数
            let params = {
                cPlyNo: record.cPlyNo,
                type: '01',
								operationType: '1',
							};
            //传入保单号，查询弹出框需要的相关信息
            dispatch({
                type: 'fastMoney/fetchFastMoney',
                payload: params,
            }).then(()=>{    
                this.setState({
                    cPlyNoOld:record.cPlyNoOld,
                    fastData: this.props.fastMoney.fastData
                });
            }).catch((e)=>{
                console.log(e);
            });  
        }
    
    };
    
    //第三方扣款类型
    thirdCardTypeChange = (value) => {
        if(value==='0201'){
          this.setState({
            isDebitCard: true
          })
        }else{
          this.setState({
            isDebitCard: false
          })
        }
        if(value==='0203') {
          this.setState({
            isJiejiCard: true
          })
        } else {
          this.setState({
            isJiejiCard: false
          })
        }
    };

    // 提交
    handleSubmit = fields => {
        fields.type = '01',
        this.props.dispatch({
            type: 'fastMoney/submitFastMoney',
            payload: fields,
        }).then(()=>{ 
            this.setState({
                modalVisible: false
            });
            console.log('原保单:'+ fields.cPlyNoOld + ',新保单：' + fields.cPlyNo + ',投保人:'+fields.cInsrntCnm);
            //提交成功后，再对原保单停止后续请款
            setInterval(this.goOnRenewal(fields.cPlyNoOld),5000)
        }).catch((e)=>{
            console.log(e);
        });

    };

    //查询续保首期无请款
    handleSearchRenewal = e => {
        //防止链接打开 URL
        if(e){
          e.preventDefault();
        }
        const { dispatch, form } = this.props;
  
        //表单校验
        form.validateFields((err, fieldsValue) => {
            if (err) return;

            // if (fieldsValue.insrncDate.length > 0) {
            //     if(fieldsValue.insrncDate[1].diff(fieldsValue.insrncDate[0],'day')>31){
            //     message.warn("[保险起期生成日期]间隔需要小于31天！");
            //     return;
            //     }
            // }
            //时间控件格式转换
            // fieldsValue = this.perpareValues(fieldsValue);
  
            const values = {
                ...fieldsValue,         //表单所有组件的值
                pageNum: this.state.pageNum,
                pageSize: this.state.pageSize,
            };
        
            //查询请求
            dispatch({
                type: 'renewalPaymentMdl/queryRenewalPayment',
                payload: values,
            }).then(()=>{
                if(this.props.renewalPaymentMdl.total === 0){
                    message.warn('该保单不符合规则，请联系续期室或系统管理员');
                }
                this.setState({dataSource: this.props.renewalPaymentMdl.renewalPayData,
                    total:this.props.renewalPaymentMdl.total });
            }).catch((e)=>{
                console.log(e);
            });
        });
    }

    //重置表单
    handleReset = () => {
        this.props.form.resetFields();
    }

    //格式化表单时间
    perpareValues(values){
        let { insrncDate } = values;
        let newValues = {};
        if(!insrncDate||insrncDate.length===0){
          newValues = {
            insrncBeginDateStart: '',
            insrncBeginDateEnd: '',
            ...values,
          };
        }else{
          newValues = {
            insrncBeginDateStart: insrncDate[0].format("YYYY-MM-DD"),
            insrncBeginDateEnd: insrncDate[1].format("YYYY-MM-DD"),
            ...values,
          };
        }
    
        delete newValues.insrncDate;
    
        return newValues;
    }

    renderAdvancedForm() {
        const { getFieldDecorator } = this.props.form;
    
        return (
          <div>
            <Form className='' onSubmit={this.handleSearchRenewal} >
                <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                    <Col md={12} sm={24}>
                        <FormItem {...formItemStyle} label="保单号">
                        {getFieldDecorator('cPlyNo',{
                        rules: [{
                            max: 30, message: '请输入正确的格式',
                        }],
                    })
                        (<Input placeholder="请输入" />)}
                        </FormItem>
                    </Col>
                    <Col md={12} sm={24}>
                        <FormItem  {...formItemStyle} label="被保人姓名">
                        {getFieldDecorator('cInsrntCnm')(<Input placeholder="请输入" />)}
                        </FormItem>
                    </Col>
                </Row>
                <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
                    <Col md={12} sm={24}>
                        <FormItem {...formItemStyle} label="投保人证件号码">
                        {getFieldDecorator('appCardId')(<Input placeholder="请输入" />)}
                        </FormItem>
                    </Col>
                    <Col md={12} sm={24}>
                        <FormItem {...formItemStyle} label="被保人证件号码">
                        {getFieldDecorator('cardId')(<Input placeholder="请输入" />)}
                        </FormItem>
                    </Col>
                </Row>
    
                <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
                    <Button type="primary" htmlType="submit">
                        查询
                    </Button>
                    <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
                        重置
                    </Button>
                </div>
            </Form>
          </div>
        );
    }

    //翻页查询
    handleTablePage = (pagination, filtersArg, sorter) => {
        this.setState({
          pageNum: pagination.current,
          pageSize: pagination.pageSize,
        },()=>{
          this.searchData();
        });
    };

    searchData(){
        const { dispatch,form } = this.props;
        form.validateFields((err, fieldsValue) => {
            if (err) return;
            dispatch({
                type: 'renewalPaymentMdl/queryRenewalPayment',
                payload: this.perpareValues(fieldsValue),
            }).then(()=>{
            }).catch(e=>{
        
            })
        });
    }

    render() {
        let {dataSource,modalVisible,total} = this.state;
        //传递给模态框的函数
        const parentMethods = {
            handleModalVisible : this.handleModalVisible ,
            handleSubmit : this.handleSubmit,
            thirdCardTypeChange : this.thirdCardTypeChange,
            fastData : this.state.fastData,
            isDebitCard : this.state.isDebitCard,
            isJiejiCard : this.state.isJiejiCard,
            cPlyNoOld : this.state.cPlyNoOld,
            
        };
        
        const rowKey = (record, index) => {
            return index
        }

        //获取model数据源对象
        const {loading,renewalPaymentMdl} = this.props;
        
        // let total = 1;
        // if(renewalPaymentMdl.renewalPayData){   //模型 renewalPaymentMdl 的参数
        //     total = renewalPaymentMdl.total;    
        // }

        const columns = this.columns;

        return (
            <PageHeaderLayout title="续保挽回继续请款">
                <Card bordered={false}>
                <div>
                    <div>{this.renderAdvancedForm()}</div>
                    <Table
                    loading={loading}
                    scroll={{x:2400,y:460}}
                    columns={columns}
                    dataSource={dataSource}
                    rowKey={rowKey}
                    bordered
                    size="small"
                    pagination={{
                        showQuickJumper: true,
                        showSizeChanger: true,
                        showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                        total:total,
                        ...{
                            current:this.state.pageNum,
                            pageSize:this.state.pageSize,
                            pageSizeOptions:['10','20','30','40','50'],
                        }
                    }}
                    onChange={this.handleTablePage}
                    />
                </div>
                </Card>
                <CreateForm {...parentMethods} modalVisible={modalVisible} />
            </PageHeaderLayout>
        );
    }

}