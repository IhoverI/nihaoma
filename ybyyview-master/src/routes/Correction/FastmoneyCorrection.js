import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form,
  Row,
  Col,
  Input,
  DatePicker,
  Select,
  Button,
  Card,
  InputNumber,
  Radio,
  Icon,
  Tooltip,
  message,
  Popconfirm
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './FastmoneyCorrection.less';
import rule from '../../models/rule';
import { STATUS_CODES } from 'http';
import { getRegularCheck, getSelectChild, getHost, getUrlParam } from '../../utils/utils';


const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const VIEWLOCAL = SERVERLOCAL;

@connect(({ fastMoney, loading }) => ({
  fastMoney,
  loading: loading.models.fastMoney
}))
@Form.create()
export default class FastmoneyCorrection extends PureComponent {
  state = {
    thirdCardTypeValue:null, //账户类型
    isFastMoney: true,
    isDebitCard: true,
    isDaijiCard: true,
  };

  componentDidMount = () => {
    let cPlyNo = this.props.location.cPlyNo;
    if (cPlyNo) {
      let searchTerm = {
        cPlyNo: cPlyNo,
        type: '01'
      };
      this.props.dispatch({
        type: 'fastMoney/fetchFastMoney',
        payload: searchTerm,
      });
    }
  }

  handleSearch = e => {
    let { form } = this.props;
    this.setState({
      isDebitCard: true,
      isDaijiCard: true,
    },()=>{
      form.validateFieldsAndScroll((err, values) => {
        if (err) return;
        let searchTerm = {
          cPlyNo: values.cPlyNo,
          type: '01'
        };
        form.resetFields();
        form.setFieldsValue(
          searchTerm
        );
        this.props.dispatch({
          type: 'fastMoney/fetchFastMoney',
          payload: searchTerm,
        });
      })
    });
    
  };

  // 提交
  handleSubmit = e => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values.type = '01';
        this.props.dispatch({
          type: 'fastMoney/submitFastMoney',
          payload: values,
        });
      }
    });
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
  };

  // 暂存
  handleSave = e => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.dispatch({
          type: 'fastMoney/saveFastMoney',
          payload: values,
        });
      }
    });
  };

  thirdCardTypeChange(value){
    if(value==='0201'){
      this.setState({
        isDebitCard: true
      })
    }else{
      this.setState({
        isDebitCard: false
      })
    }
    if(value==='0203') {
      this.setState({
        isDaijiCard: true
      })
    } else {
      this.setState({
        isDaijiCard: false
      })
    }
  }

  render() {
    const { fastMoney: { fastData, blobURL  }, fastMoney, loading } = this.props;
    let cPlyNo = this.props.location.cPlyNo;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const policyProfile = null == fastData.data ? undefined : fastData.data;
    const isSuccess = fastData.code === 0 ? true : false;

    const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    };

    const submitFormLayout = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 10, offset: 7 },
      },
    };
    
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <Form  style={{ marginTop: 8 }}>
            <div className={styles.information} >
              <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
                  <Col md={24} sm={24}>
                    <FormItem {...formItemLayout} label="保单号">
                      {getFieldDecorator('cPlyNo',
                      {initialValue: cPlyNo ? cPlyNo : '', rules: [{required: true, message: 'please input!'}]})
                      
                      (<Input style={{ width: '100%' }} placeholder='请输入' />)}
                    </FormItem>
                  </Col>
                </Row>
                <div className={styles.mailDetailOperator} style={{ textAlign: 'center' }}>
              <span>
                <Button loading={loading} type="primary" onClick={this.handleSearch}>
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </span>
            </div>
            </div>
            <div className={styles.searchListItem}>
            <div className={styles.sectionTitle}>保单信息：</div>
              <FormItem
                {...formItemLayout}
                label={
                  <span>
                    保单号
                    { isSuccess ? 
                      <em className={styles.optional}>
                        <Tooltip title="点击跳转至保单详情页">
                          <a  href={`/#/query-manage/policy-list?cPlyNo=${policyProfile.cPlyNo}&prePath=/correction/fastmoney-correction`} >
                            （详情）
                            <Icon type="info-circle-o" style={{ marginRight: 4 }} />
                          </a>
                        </Tooltip>
                      </em> : null
                    }
                  </span>
                }
              >
                <Input
                  style={{ width: '100%' }}
                  readOnly="true"
                  placeholder=""
                  value={undefined === policyProfile ? '' : policyProfile.cPlyNo}
                />
              </FormItem>
              <FormItem {...formItemLayout} label="被保人姓名">
                <Input
                  style={{ width: '100%' }}
                  readOnly="true"
                  placeholder=""
                  value={undefined === policyProfile ? '' : policyProfile.cInsrntCnm}
                />
              </FormItem>
              <FormItem {...formItemLayout} label="银行渠道">
                 {getFieldDecorator('oldEnterpCode',
                  {initialValue: undefined === policyProfile ? '' : policyProfile.oldEnterpCode})
                  (<Select placeholder="请选择" disabled>
                    {getSelectChild('extEnterpDesc')}
                    </Select>
                  )}
              </FormItem>
            </div>
            <div className={styles.sectionTitle}>第三方扣款信息：</div>
            <div className={styles.searchListItem}>
            <FormItem {...formItemLayout}  label="第三方扣款客户证件类型">
                {getFieldDecorator('thirdCertType',
                  {initialValue: undefined === policyProfile ? '' : policyProfile.thirdCertType},{
                  rules: [{ required: true, message: '必录项！' }],
                })(
                    <Select placeholder="请选择" style={{ width: '100%' }}>
                    {getSelectChild('certiDesc')}
                    </Select>
                  )}
              </FormItem>
              <FormItem {...formItemLayout} label="扣款客户证件号码">
                {getFieldDecorator('thirdCertNo',
                  {initialValue: undefined === policyProfile ? '' : policyProfile.thirdCertNo},getRegularCheck('certNo'))(
                  <Input style={{ width: '100%' }} placeholder="" />
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="第三方扣款银行名称">
                {getFieldDecorator('thirdBank',
                  {initialValue: undefined === policyProfile ? '' : policyProfile.thirdBank},
                  {rules: [{ required: true, message: '请选择扣款银行' }]})
                  (
                    <Select placeholder="请选择" style={{ width: '100%' }}>
                      {getSelectChild('thirdBank')}
                    </Select>
                  )}
              </FormItem>
              <FormItem {...formItemLayout} label="第三方扣款开户行名称">
                {getFieldDecorator('thirdBranchBank',
                  {initialValue: undefined === policyProfile ? '' : policyProfile.thirdBranchBank})
                (
                  <Input placeholder='需要手动输入'></Input>
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="第三方扣款账户类型">
                {getFieldDecorator('thirdCardType',
                {initialValue: undefined === policyProfile ? '' : policyProfile.thirdCardType},
                {rules: [{ required: true, message: '必录项！' }]})
                (
                  <Select placeholder="请选择" style={{ width: '100%' }}
                    onChange={(value) => this.thirdCardTypeChange(value)} >
                    <Option value='0201'>借记卡</Option>
                    <Option value='0204'>存折</Option>
                  </Select>
                )}
              </FormItem>
              { this.state.isDaijiCard ?
                <FormItem {...formItemLayout} label={
                    <span>
                      第三方扣款账号有效期
                      <em className={styles.optional}>
                        <Tooltip title="年份后两位+月份，例：0112">
                            <Icon type="info-circle-o" style={{ marginRight: 4 }} />
                        </Tooltip>
                      </em>
                    </span>
                  }>
                  {getFieldDecorator('thirdCardDate',
                    {initialValue: undefined === policyProfile ? '' : policyProfile.thirdCardDate})
                  (
                    <Input placeholder='贷记卡时必填'/>
                  )}
                </FormItem> : null
              }
              { this.state.isDebitCard ? 
              <FormItem {...formItemLayout} label="第三方扣款所属城市">
                {getFieldDecorator('thirdPayCity',
                {initialValue: undefined === policyProfile ? '' : policyProfile.thirdPayCity},)
                (
                  <Input placeholder='借记卡时必填'/>
                )}
              </FormItem> : null
              }
              <FormItem {...formItemLayout} label="第三方扣款账号姓名">
                {getFieldDecorator('thirdAppName',
                {initialValue: undefined === policyProfile ? '' : policyProfile.thirdAppName},
                {rules: [{ required: true, message: '必录项！' }]})
                (
                  <Input style={{ width: '100%' }} placeholder="" />
                )}
              </FormItem>
              <FormItem {...formItemLayout} label="第三方扣款账号">
                {getFieldDecorator('thirdCardId',
                {initialValue: undefined === policyProfile ? '' : policyProfile.thirdCardId},
                getRegularCheck('cardId'))(
                <Input style={{ width: '100%' }} placeholder="" />
                )}
              </FormItem>
                {getFieldDecorator('cPlyAppNo', 
                {initialValue: undefined === policyProfile ? '' : policyProfile.cPlyAppNo})
                (<Input style={{ display: 'none' }} placeholder="" />)}
            </div>
            <FormItem {...submitFormLayout} style={{ marginTop: 32 }}>
              <Button loading={loading} type="primary" onClick={this.handleSubmit}>
                提交
              </Button>
            </FormItem>
          </Form>
        </Card>
      </PageHeaderLayout>
    );
  }
}
