
// 业务渠道
export const extEnterpDesc = [
  {name: '招商银行', code: '95555'},
  {name: '民生银行', code: '95568'},
  {name: '建设银行', code: '95533'},
  {name: '盛大', code: '80001'},
  {name: '江苏邮政', code: '95580'},
  {name: '深发展', code: '95501'},
  {name: '深圳陌call', code: '10010'},
  {name: '南昌电话中心',code:'11004'},
  {name: '成都自建', code: '11003'},
  {name: '分公司推广', code: '95556'},
  {name: '微信出单', code: '11002'},
  {name: '汇金保联', code: '11001'},
  {name: '中信银行', code: '95558'},
];
// 业务渠道匹配
export const extEnterpDescFunc =(extEnterpCode)=>{
  const result = extEnterpDesc.find((item)=>item.code === extEnterpCode);
  return result && result.name;
}

export const extEnterpOPtsFn = (datatType='3')=>{
  if(datatType=== '1'){
    return [
      {name: '招商银行', code: '95555'},
    ]
  }
  if(datatType=== '2'){
    return [
      {name: '招商银行', code: '95555'},
      {name: '民生银行', code: '95568'},
      {name: '中信银行', code: '95558'},
    ]
  }
  if(datatType=== '3'){
    return extEnterpDesc;
  }

}


