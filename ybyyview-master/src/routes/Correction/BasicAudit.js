import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import {
  Row,
  Col,
  Steps,
  Select,
  Table,
  Card,
  Button,
  Dropdown,
  Badge,
  Form,
  Input,
  DatePicker,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './BasicAudit.less';

const { Option } = Select;
const Step = Steps.Step;
const FormItem = Form.Item;
const { RangePicker } = DatePicker;

const steps = [{
  title: '用户信息批改',
  content: '用户信息批改',
}, {
  title: '待审核',
  content: '待审核',
}, {
  title: '审核完成',
  content: '审核完成',
}];


  const statusMap = ['success', 'default'];
  const status = ['待审核', '审核完成'];
  
@connect(({  loading, profile }) => ({
  loading: loading.models.profile,
  profile,
}))
@Form.create()
export default class BasicAudit extends PureComponent {
  state = {
    current:0,
    pageNum: 1,
    rowNum: 10,
    cPlyNo: '',
    startTime: '',
    endTime: '',
  };


  componentDidMount() {
    this.searchData(1,10,this.state.cPlyNo,this.state.appTime,this.state.endTime)
  }

  searchData = (pageNum,rowNum,cPlyNo,startTime,endTime) => {
     this.props.dispatch({
      type: 'profile/userCorrect',
       payload: {
         pageNum,
         rowNum,
         cPlyNo,
         startTime,
         endTime,
       }
    });
  }

    _formatUnit = (val) => {
      let text = '';
      switch(val) {
        case '0':
          text = '待审核';
          break;
        case '1':
          text = '审核通过';
          break;
        case '2':
          text = '审核拒绝';
          break
        default:
          break;
      }
        return text;
    }

 getColumns(){
    var _this = this;
    return [{
          title: '保单号',
          dataIndex: 'cPlyNo',
          align: 'center',
        }, 
        {
          title: '批改提交账号',
          dataIndex: 'endorUserId',
          align: 'center',
        }, 
        {
          title: '被保人姓名',
          dataIndex: 'cInsrntCnm',
          align: 'center',
        }, 
        {
          title: '操作',
          dataIndex: 'action',
          align: 'center',
          render: (text, record) => {
            return (
              <Link to={{pathname: '/message-manage/correction',id: record.cPlyNo, toReload: true }}>
                查看详情
              </Link>
            );
          }
        },{
          title: '备注',
          dataIndex: 'remark',
          align: 'center',
        }];
  }

  serachData = () => {
    const { dispatch, form } = this.props;
    let startTime,endTime;
    form.validateFields((err, fieldsValue) => {
      if (fieldsValue.appTime === undefined){
        this.setState({
          startTime: '',
          endTime: ''
        })
      } else if (fieldsValue.appTime.length > 0) {
        this.setState({
          startTime: fieldsValue.appTime[0].format("YYYY-MM-DD"),
          endTime: fieldsValue.appTime[1].format("YYYY-MM-DD")
        })
        startTime = fieldsValue.appTime[0].format("YYYY-MM-DD");
        endTime = fieldsValue.appTime[1].format("YYYY-MM-DD");
      } else {
        this.setState({
          startTime: '',
          endTime: ''
        })
      }
      this.searchData(1,10,fieldsValue.cPlyNo,startTime,endTime);
    })
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      pageNum:0, 
      rowNum:10,
    });
  }

    handleTablePage =(pagination, filters, sorter) => {
    this.setState({
      pageNum: pagination.current,
      rowNum: pagination.pageSize,
    },()=>{
      this.searchData(pagination.current,pagination.pageSize,this.state.cPlyNo,this.state.startTime, this.state.endTime);
    });
  }

  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    return (
        <Form className='custom-form-wrapper custom-search-form-wrapper' layout="inline" style={{marginTop:10}}>
          <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
            <Col md={8} sm={24}>
              <FormItem label="保单号">
                {getFieldDecorator('cPlyNo',{
                 rules: [{
                    max: 30, message: '请输入正确的格式',
                  }],
              },{initialValue: ''})(
                  <Input placeholder="" />
                )}
              </FormItem>
            </Col>
            <Col md={12} sm={24}>
              <FormItem label="批改申请日期">
                {getFieldDecorator('appTime')(
                  <RangePicker  style={{ width: '100%' }} />
                )}
              </FormItem>
            </Col>
          </Row>
          <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
            <Button type="primary" onClick={this.serachData}>
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
        </div>
        </Form>
    )
  }



  render() {
    const { profile:{userCorrectData},loading } = this.props;
    
    let data,total;
    if (userCorrectData) {
      data = userCorrectData.list || [];
      total = userCorrectData.total || 0;
    }
    const { current } = this.state;
    const rowKey = (record) => { 
        return record.id;
    }
    return (
      <PageHeaderLayout title="">
            <Card bordered={false}>
              <div>{this.renderAdvancedForm()}</div>
                <Steps style={{width:'70%',margin:'0px auto 20px auto'}} current={current}>
                    {steps.map(item => <Step key={item.title} title={item.title} />)}
                </Steps>
                <Table 
                  loading={loading}
                  rowKey={rowKey}
                  dataSource={data} 
                  bordered 
                  size='small'
                  pagination={{
                    showQuickJumper: true,
                    showSizeChanger: true,
                    ...{
                        current:this.state.pageNum,
                        pageSize:this.state.rowNum,
                        showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                        total:total,
                        pageSizeOptions:['10','20','30','40','50'],
                    },
                  }}
                  onChange={this.handleTablePage}
                  columns={this.getColumns()} 
                  />
            </Card>
      </PageHeaderLayout>
    );
  }
}
