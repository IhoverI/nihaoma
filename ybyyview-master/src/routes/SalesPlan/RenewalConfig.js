import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Table
} from 'snk-web';
import RenewalConfigTable from './components/RenewalConfigTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild } from '../../utils/utils'

import styles from './RenewalConfig.less';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;

const EditableCell2 = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} placeholder='请填写打回原因'/>
      : value
    }
  </div>
);

const getExtEnterpCode = (val) => {
   let proName;
    if (getSelectChild('extEnterpDesc')) {
      for (const i in getSelectChild('extEnterpDesc')) {
        if (getSelectChild('extEnterpDesc')[i].props.value === val) {
          proName = getSelectChild('extEnterpDesc')[i].props.children;
        }
      }
      return proName;
    }
}

const _formatUnit = (val,code) => {
  let text;
  if (code === '') {
    switch(val) {
      case '1': text = '是'; break;
      case '0': text = '否'; break;
      default: break; 
    }
  } else {
    switch(val) {
      case '1': text = '是'; break;
      case '0': text = '否'; break;
      default: break;
    }
  }
  return text;
}

@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
export default class RenewalConfig extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    dataSource: {},
    isSurfacemailEdit: false,
    isSurfacemail: '',
    isNoteEdit: false,
    isNote: '',
    isMailEdit: false,
    isMail: '',
  };

  componentDidMount() {}

  renderColumns(text, record, column) {
    return (
      <EditableCell2
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record, column)}
      />
    );
  }

  handleChange(value, record, column) {
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => item === record)[0];
    if (target) {
      target[column] = value;
      this.setState({ dataSource: newData });
    }
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  };

  save(record) {
    const { dispatch, form } = this.props;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => item === record)[0];
    if (target) {
      delete record.editable;
      this.setState({ data: newData });
    }

    let data = {
      ...record,
      isMail: this.state.isMail === '' ? record.isMail : this.state.isMail,
      isNote: this.state.isNote === '' ? record.isNote : this.state.isNote,
      isSurfacemail: this.state.isSurfacemail === '' ? record.isSurfacemail : this.state.isSurfacemail,
    }
    this.props.dispatch({
        type: 'rule/renwalInfo',
        payload: data
    })
  }

   edit(record,dataSource) { 
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => item === record)[0];
    if (target) {
      record.editable = true;
      this.setState({ data: newData });
    }
  }

  cancel(record) {
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => item === record)[0];
    if (target) {
      delete record.editable;
      this.setState({ data: newData });
    }
  }

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'rule/renwalPlanQuery',
        payload: {
          extEnterpCode:fieldsValue.extEnterp
        },
      });
    });
  };

  isSurfacemail = (val) => {
    if (val === '0') {
      this.setState({isSurfacemailEdit: false,isSurfacemail: val});
    } else {
      this.setState({isSurfacemailEdit: true,isSurfacemail:val});
    }
  }

  isNote = (val) => {
    if (val === '0') {
      this.setState({isNoteEdit:false,isNote:val});
    } else {
      this.setState({isNoteEdit:true,isNote:val});
    }
  }

  isMail = (val) => {
    if (val === '0') {
      this.setState({isMailEdit: false,isMail:val});
    } else {
      this.setState({isMailEdit: true,isMail:val});
    }
  }

  render() {
    const { rule: { renwalPlanQueryData }, loading } = this.props;
    let {dataSource} = this.state;
    if (renwalPlanQueryData) {
      dataSource = renwalPlanQueryData|| [];
    }
    this.state.dataSource = dataSource;
    const { selectedRows, modalVisible } = this.state;
    const { getFieldDecorator } = this.props.form;

    const columns = [
    {
      title: '渠道名称',
      dataIndex: 'extEnterpCode',
      align: 'center',
      width: 100,
      render: (text) => {
        return (<span>{getExtEnterpCode(text)}</span>);
      }
    },
    {
      title: '产品名称',
      dataIndex: 'prodName',
      align: 'center',
      width: 100,
    },
    {
      title: '平邮',
      dataIndex: 'isSurfacemail',
      align: 'center',
      width: 50,
      render:(text,index,record) => {
          return <FormItem>
              {getFieldDecorator(`${'isSurfacemail'+''+index.id}`,{initialValue: `${_formatUnit(text,'isSurfacemail')}`})(
                <Select onChange={this.isSurfacemail}>
                  <Option value="1">是</Option>
                  <Option value="0">否</Option>
                </Select>
              )} 
          </FormItem>
      }
    },
    {
      title: '打印时间',
      dataIndex: 'faceDay',
      align: 'center',
      width: 200,
      render: (text, record) => {
        return (
          <span>
            <span>保险止期提前</span>{this.renderColumns(text, record, 'faceDay')}天
          </span>
        );
      }
    },
    {
      title: '短信',
      dataIndex: 'isNote',
      align: 'center',
      width: 50,
      render:(text,index,record) => {
            return <FormItem>
                {getFieldDecorator(`${'isNote'+''+index.id}`,{initialValue: `${_formatUnit(text,'isNote')}`})(
                  <Select onChange={this.isNote}>
                    <Option value="1">是</Option>
                    <Option value="0">否</Option>
                  </Select>
                )} 
            </FormItem>
        }
    },
    {
      title: '短信发送时间',
      dataIndex: 'noteDay',
      align: 'center',
      width: 200,
      render: (text, record) => {
        return (
          <span>
            <span>保险止期提前</span>{this.renderColumns(text, record, 'noteDay')}天
          </span>
        );
      }
    },
    {
      title: '邮件',
      dataIndex: 'isMail',
      align: 'center',
      width: 50,
       render:(text,index,record) => {
            return <FormItem>
                {getFieldDecorator(`${'isMail'+''+index.id}`,{initialValue: `${_formatUnit(text,'isMail')}`})(
                  <Select onChange={this.isMail}>
                    <Option value="1">是</Option>
                    <Option value="0">否</Option>
                  </Select>
                )} 
            </FormItem>
        }
    },
    {
      title: '邮件发送时间',
      dataIndex: 'mailDay',
      align: 'center',
      width: 200,
      render: (text, record) => {
        return (
          <span>
            <span>保险止期提前</span>{this.renderColumns(text, record, 'mailDay')}天
          </span>
        );
      }
    },
    {
      title: '操作',
      dataIndex: 'action',
      align: 'center',
      width: 140,
      render: (text, record) => {
        const { editable } = record;
        return (
          <Fragment>
              {
                editable ?
                  <span>
                    <a onClick={() => this.save(record)}>保存</a>
                    <a onClick={() => this.cancel(record)}> 取消</a>
                  </span>
                  : <a onClick={() => this.edit(record)}> 编辑</a>
              }
          </Fragment>
        );
      }
    }
  ];

    const rowKey = (record,index) => {
      return index;
    }

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
            <Form className='custom-form-wrapper'  onSubmit={this.handleSearch} layout="inline">
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <FormItem label="渠道名称">
                {getFieldDecorator('extEnterp')(
                <Select
                placeholder="请选择"
                style={{ width: '30%' }}
                >
                {getSelectChild('extEnterpDesc',null,null,true)}
                </Select>
                )}
                <Button type="primary" htmlType="submit" style={{marginLeft:20}}>
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
                </FormItem>
            </Row>
            </Form>
            <div className={styles.renewalConfigOperator}>
            <Table
              loading={loading}
              rowKey={rowKey}
              dataSource={dataSource}
              scroll={{x:1500}}
              bordered
              size="small"
              columns={columns}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
