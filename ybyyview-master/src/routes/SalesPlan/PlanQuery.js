import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Table
} from 'snk-web';
import PlanQueryTable from './components/PlanQueryTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild } from '../../utils/utils'

import styles from './PlanQuery.less';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const statusMap = ['default', 'processing', 'success', 'error'];
const status = ['关闭', '运行中', '已上线', '异常'];
const columns = [
  {
    title: '险种代码',
    dataIndex: 'insrncCode',
    align: 'center',
  },
  {
    title: '行业企业代码',
    dataIndex: 'entCode',
    align: 'center',
  },
  {
    title: '险种名称',
    dataIndex: 'insrncName',
    align: 'center',
  },
  {
    title: '款别',
    dataIndex: 'feeType',
    align: 'center',
  },
  {
    title: '险别名称（旧）',
    dataIndex: 'oldRiskName',
    align: 'center',
  },
  {
    title: '险别代码（旧）',
    dataIndex: 'oldRiskCode',
    align: 'center',
  },
  {
    title: '险别名称（新）',
    dataIndex: 'newRiskName',
    align: 'center',
  },
  {
    title: '险别代码（新）',
    dataIndex: 'newRiskCode',
    align: 'center',
  },
  {
    title: '性别',
    dataIndex: 'sex',
    align: 'center',
  },
  {
    title: '年龄',
    dataIndex: 'age',
    align: 'center',
  },
  {
    title: '新单保额',
    dataIndex: 'newAmt',
    align: 'center',
  },
  {
    title: '第一年及以后续保保额',
    dataIndex: 'renwalAmt1',
    align: 'center',
  },
  {
    title: '新单费率',
    dataIndex: 'newRate',
    align: 'center',
  },
  {
    title: '续单费率',
    dataIndex: 'renwalRate1',
    align: 'center',
  },
  {
    title: '每月保费',
    dataIndex: 'prm',
    align: 'center',
  },
  {
    title: '年化保费',
    dataIndex: 'pmtPrm',
    align: 'center',
  },
];

@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
export default class PlanQuery extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'rule/fetch',
    });
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            no: selectedRows.map(row => row.no).join(','),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      let extEnterpCode,prodNo;
      if (fieldsValue.riskCode.length > 0) {
        extEnterpCode = fieldsValue.riskCode.slice(0,5);
        prodNo = fieldsValue.riskCode.substring(fieldsValue.riskCode.length-4)
      } else {
        extEnterpCode = '';
        prodNo = '';
      }
      if (err) return;
      dispatch({
        type: 'rule/planQuery',
        payload: {
          extEnterpCode,
          prodNo,
        },
      });
    });
  };
  handleAdd = fields => {
    this.props.dispatch({
      type: 'rule/add',
      payload: {
        description: fields.desc,
      },
    });

    message.success('添加成功');
    this.setState({
      modalVisible: false,
    });
  };

   

  render() {
    const { rule: { planQueryData }, loading } = this.props;
    let data;
    let optionArr = [];
    let total;
    if (planQueryData) {
      data = planQueryData.list || [];
    }

    if (getDataSource('prodDesc')) {
      for (const i in getDataSource('prodDesc')) {
        const item = (
          <Option key={getDataSource('prodDesc')[i].group+""+getDataSource('prodDesc')[i].name+''+getDataSource('prodDesc')[i].code}>
            {getDataSource('prodDesc')[i].group+'-'+getDataSource('prodDesc')[i].code+'-'+getDataSource('prodDesc')[i].name}
          </Option>
        );
        optionArr.push(item);
      }
    }


    const { selectedRows, modalVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
   
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
            <Form className='custom-form-wrapper'  onSubmit={this.handleSearch} layout="inline">
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
               <Col md={12} sm={24}>
                <FormItem label="产品名称">
                  {getFieldDecorator('riskCode',{initialValue: [],rules:[{required: true, message:'必选！'}]})(
                     <Select
                      placeholder="请选择"
                      style={{ width: '100%' }}
                    >
                      {optionArr}
                    </Select> 
                    )}
                </FormItem>
              </Col>
            </Row>
            <div className={styles.planQueryOperator} style={{ overflow: 'hidden',textAlign:'center',padding:10  }}>
              <span style={{ marginBottom: 24 }}>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </span>
            </div>
            </Form>
            <div className={styles.planQueryOperator}>
            <Table
              loading={loading}
              dataSource={data}
              scroll={{x:2000}}
              bordered
              size="small"
              columns={columns}
            />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
