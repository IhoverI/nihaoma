import React, { PureComponent, Fragment } from 'react';
import echarts from 'echarts/lib/echarts'; // 必须
import 'echarts/lib/chart/gauge';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markLine';
import { getSelectChild } from '../../utils/utils'
import styles from './RightChart.less';


export default class RightChart extends PureComponent {
  
  componentWillReceiveProps(nextProps) {
    const {sucessRenewalTotalData} = nextProps
    let seriesVlaueData;
    if (sucessRenewalTotalData) {
        if (sucessRenewalTotalData[1]) {
          const data = sucessRenewalTotalData[1][0] || 0;
          if (data !== undefined && data !== 0) {
            seriesVlaueData = data.substring(0,data.length - 1);
          }
        }
    }
    const option = {
        tooltip : {
              formatter: "{a} <br/>{b} : {c}%",
          },
          toolbox: {
              feature: {
                  restore: {},
                  saveAsImage: {},
              },
          },
          series: [
              {
                  name: '本年续保成功率',
                  type: 'gauge',
                  detail: {
                    formatter:'{value}%',
                    fontSize: 18,
                  },
                  data: [{value: seriesVlaueData}],
                  axisLine: {
                    lineStyle: {
                      width: 10,
                    },
                  },
                  radius: '60%' ,
                  splitLine: {
                    length: 10,
                    lineStyle: {
                      color: 'auto',
                    },
                  },
                  pointer: {
                    width: 5,
                  },
                  itemStyle: {
                    color: '#3696ea',
                  }, 
              },
                
          ],
    };

    const lineEcharts = echarts.init(window.document.getElementById('sucessRenewalTotal'));
    lineEcharts.setOption(option);
  }
  
    desFunc = (val) => {
      if (getSelectChild('extEnterpDesc')) {
        for (const i in getSelectChild('extEnterpDesc')) {
          if (getSelectChild('extEnterpDesc')[i].props.value === val) {
            return getSelectChild('extEnterpDesc')[i].props.children
          }
        }
        for (const i in getSelectChild('prodDesc')) {
          if (getSelectChild('prodDesc')[i].props.value === val) {
            return getSelectChild('prodDesc')[i].props.children;
          }
        }
      }
    }

  render() {
    const {sucessRenewalTotalData,orderRenewalTotalData} = this.props;
    let success;
    if (sucessRenewalTotalData) {
      if (sucessRenewalTotalData[1]) {
        success = sucessRenewalTotalData[1][1] || 0;
      }
    }
    const orderList = [];
    
    if (orderRenewalTotalData) {
      for (const key in orderRenewalTotalData[0]) {
        if (orderRenewalTotalData[0][key]) {
          const item = (
            <div key={key} className={styles.orderItem}>
              <div>{orderRenewalTotalData[1][key]}</div>
              <div>{this.desFunc(orderRenewalTotalData[0][key])}</div>
            </div>
        );
          orderList.push(item);
        }
      }
    }
    return (
      <div className={styles.chartDiv}>
        <div id='sucessRenewalTotal'  style={{width:'100%',minHeight: '300px'}} />
        <span className={styles.des}><span style={{marginRight: 6}}>比去年同期高</span>{success}</span>
        <div className={styles.orderList}>
          {
            orderList.map((item)=> { return item })
          }
        </div>
      </div>
    );
  }
}
