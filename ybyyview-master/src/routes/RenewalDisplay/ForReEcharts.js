import React, { PureComponent, Fragment } from 'react';
import { getSelectChild } from '../../utils/utils'
import echarts from 'echarts/lib/echarts'; // 必须
import Line from './Line'

export default class ForReEcharts extends PureComponent {

  componentWillReceiveProps(nextProps) {
    const {forRenewalData} = nextProps
    const keys = [];
    const values = [];
    let desc = [];
    for (const i in forRenewalData) {
      if (i) {
        keys.push(i)
        values.push(forRenewalData[i] || 0)
      }
    }
    const Setting = {
      lable: keys,
      data: [values],
      legend: ['待续保'],
      colors: ['#40a9ff'],
      // showLegend: false,
    };
    const LineOption = new Line(Setting);
    const lineEcharts = echarts.init(window.document.getElementById('forRenewalTotal'));
    lineEcharts.setOption(LineOption);
  }

  render() {
    return (
      <div id='forRenewalTotal' style={{width:'100%',minHeight: '280px'}}/>
    );
  }
}
