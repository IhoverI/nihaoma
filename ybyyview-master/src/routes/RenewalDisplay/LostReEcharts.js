import React, { PureComponent, Fragment } from 'react';
import echarts from 'echarts/lib/echarts'; // 必须
import Line from './Line'

export default class LostReEcharts extends PureComponent {

  componentWillReceiveProps(nextProps) {
    const {lostRenewalTotalData} = nextProps
    const keys = [];
    const values = [];
    for (const i in lostRenewalTotalData) {
      if (i) {
         keys.push(i)
        values.push(lostRenewalTotalData[i] || 0)
      }
    }
    const Setting = {
      lable: keys,
      data: [values],
      legend: ['续保流失'],
      colors: ['#8618ffc4'],
      // showLegend: false,
    };
    const LineOption = new Line(Setting);
    const lineEcharts = echarts.init(window.document.getElementById('lostRenewalTotal'));
    lineEcharts.setOption(LineOption);
  }

  render() {
    return (
      <div id='lostRenewalTotal' style={{width:'100%',minHeight: '280px'}}/>
    );
  }
}
