// import echarts from 'echarts/lib/echarts'; // 必须
import 'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markLine';

import { getSelectChild } from '../../utils/utils'

export default function Line(setting) {
  const {
    lable, data, legend, colors,
  } = setting;
  const series = [];
  const desc = []
  if (lable) {
     if (getSelectChild('payChannel1') || getSelectChild('prodDesc')) {
      for (const j in lable) {
        for (const i in getSelectChild('payChannel1')) {
          if (getSelectChild('payChannel1')[i].props.value == lable[j]) {
            desc.push(getSelectChild('payChannel1')[i].props.children);
          }
        }
        for (const i in getSelectChild('prodDesc')) {
          if (getSelectChild('prodDesc')[i].props.value == lable[j]) {
            desc.push(getSelectChild('prodDesc')[i].props.children);
          }
        }
      }
    }
  }
  if (data && data.length > 0) {
    data.forEach((i, index) => {
      const TEMP = {
        name: legend[index],
        type: 'line',
        areaStyle: {normal: {}},
        // showAllSymbol: true,
        data: i,
        itemStyle: {
        normal: {
            color: colors[index],
            lineStyle: {
                color: colors[index],
              },
          },
      },
      };
      series.push(TEMP);
    });
  }

  const option = {
    tooltip: {
        trigger: 'axis',
    },
    legend: {
        data:[],
    },
    xAxis:  {
        type: 'category',
        // boundaryGap: false,
        splitLine:{show: false},
        data: desc,
        axisTick:{show:false},
    },
    yAxis: {
        type: 'value',
        splitLine:{show: false},
        axisLabel: {
            // formatter: '{value} 万'
        },
    },
      series,
};
  return option;
}

