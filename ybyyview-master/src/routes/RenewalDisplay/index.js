import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Radio,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Spin
} from 'snk-web';
import DataSet from '@antv/data-set';
import ForReEcharts from './ForReEcharts';
import AlreadyReEcharts from './AlreadyReEcharts';
import LostReEcharts from './LostReEcharts';
import RightChart from './RightChart';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './index.less';
import { getDataSource,getSelectChild } from '../../utils/utils'

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const { Option } = Select;


@connect(({ searchlist, loading }) => ({
  searchlist,
  loading: loading.models.searchlist,
}))
@Form.create()
export default class ApplyList extends PureComponent {
  constructor(props){
    super(props)
    const date = new Date();
    date.setTime(date.getTime());
    let today = date.getFullYear()+"-" + (date.getMonth()+1) + "-" + date.getDate() + " " + '00' + ":"+'00' + ":" + '00';
    const insrncToday =  date.getFullYear()+"-" + (date.getMonth()+1) + "-" + date.getDate() 
    const lastDay = date.getFullYear()+"-" + 12 + "-" + 31 + " " + '00' + ":"+'00' + ":" + '00';
    const firstDay = date.getFullYear()+"-" + 1 + "-" + 1;
    this.state = {
      modalVisible: false,
      expandForm: false,
      selectedRows: [],
      formValues: {},
      insrncEndDateStart: today,
      insrncEndDateEnd: lastDay,
      lostReDateStart: firstDay,
      lostReDateEnd: insrncToday,
      alReDateStart: firstDay,
      alReDateEnd: insrncToday,
      extEnterpCode: '',
      loading: true,
    };
  }
  

  componentDidMount() {
  
    // 待续保
    this.forReData(this.state.insrncEndDateStart,this.state.insrncEndDateEnd,this.state.extEnterpCode);
    // 已续保
    this.alreadyReData(this.state.alReDateStart,this.state.alReDateEnd,this.state.extEnterpCode);
    // 续保流失
    this.lostReData(this.state.lostReDateStart,this.state.lostReDateEnd,this.state.extEnterpCode);
    // 情况汇总—排名前3
    this.orderReData(this.state.extEnterpCode);
    // 情况汇总—本年续保成功率
    this.successReData(this.state.extEnterpCode);
  }

  onChangeForReData = (value) => {
    if (value) {
      const start = value[0].format("YYYY-MM-DD 00:00:00");
      const end = value[1].format("YYYY-MM-DD 00:00:00");
      this.setState({
        insrncEndDateStart: start,
        insrncEndDateEnd: end,
      });
    }
  }

  onChangeAlReData = (value) => {
    if (value) {
      const start = value[0].format("YYYY-MM-DD");
      const end = value[1].format("YYYY-MM-DD");
      this.setState({
        alReDateStart: start,
        alReDateEnd: end,
      });
    }
  }
  
  onChangeLostReData = (value) => {
    if (value) {
      const start = value[0].format("YYYY-MM-DD");
      const end = value[1].format("YYYY-MM-DD");
      this.setState({
        lostReDateStart: start,
        lostReDateEnd: end,
      });
    }
  }

  handleSearch = e => {
    this.setState({loading: true});
    e.preventDefault();
    const { dispatch, form } = this.props;
    let extEnterpCode;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      // todo...
      if (fieldsValue.extEnterp == undefined) {
        extEnterpCode = '';
      } else {
        extEnterpCode = fieldsValue.extEnterp;
      }
      // 待续保
      this.forReData(this.state.insrncEndDateStart,this.state.insrncEndDateEnd,extEnterpCode);
      // 已续保
      this.alreadyReData(this.state.alReDateStart,this.state.alReDateEnd,extEnterpCode);
      // 续保流失
      this.lostReData(this.state.lostReDateStart,this.state.lostReDateEnd,extEnterpCode);
      // 情况汇总—排名前3
      this.orderReData(extEnterpCode);
      // 情况汇总—本年续保成功率
      this.successReData(extEnterpCode);
    });
  };

  // 待续保
  forReData = (start,end,extEnterpCode) => {
    this.props.dispatch({
      type: 'searchlist/forRenewalTotal',
      payload: {
        insrncEndDateStart: start,
        insrncEndDateEnd: end,
        extEnterpCode: extEnterpCode,
      }
    }).then(()=>{
      this.setState({loading: false});
    });
  }

  // 已续保
  alreadyReData = (start,end,extEnterpCode) => {
    this.props.dispatch({
      type: 'searchlist/alreadyRenewalTotal',
      payload: {
        insrncBeginDateStart: start,
        insrncBeginDateEnd: end,
        extEnterpCode: extEnterpCode,
      }
    }).then(()=>{
      // this.setState({loading: false});
    });
  }

  // 续保流失
  lostReData = (start,end,extEnterpCode) => {
    this.props.dispatch({
      type: 'searchlist/lostRenewalTotal',
      payload: {
        insrncBeginDateStart: start,
        insrncBeginDateEnd: end,
        extEnterpCode: extEnterpCode,
      }
    });
  }

  // 情况汇总—排名前3
  orderReData = (extEnterpCode) => {
    this.props.dispatch({
      type: 'searchlist/orderRenewalTotal',
      payload:{
        extEnterpCode: extEnterpCode
      }
    });
  }

  // 情况汇总—本年续保成功率
  successReData =(extEnterpCode) => {
    this.props.dispatch({
      type: 'searchlist/sucessRenewalTotal',
      payload: {
        extEnterpCode: extEnterpCode
      }
    });
  }
  
  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={12}>
            <FormItem label="渠道名称">
              {getFieldDecorator('extEnterp')(
            <Select
              placeholder="请选择"
              style={{ width: '100%' }}
              >
              {getSelectChild('extEnterpDesc',null,null,true)}
            </Select>
            )}
            </FormItem>
          </Col>
          <Col md={4} sm={30}>
            <Button type="primary" htmlType="submit">
                查询
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }
  
  render() {
    const { loading, searchlist: 
      {forRenewalTotal,alreadyRenewal,sucessRenewalTotalData,lostRenewalTotal,orderRenewalTotalData}
    } = this.props;
    let forRenewalTotalData,alreadyRenewalTotalData,lostRenewalTotalData;
    if (forRenewalTotal) {
      forRenewalTotalData = forRenewalTotal.待续保 || {}
    }
    if (alreadyRenewal) {
      alreadyRenewalTotalData = alreadyRenewal.已续保 || {}
    }
    if (lostRenewalTotal) {
      lostRenewalTotalData = lostRenewalTotal.续保流失 || {}
    }
    const dateFormat = 'YYYY-MM-DD';
    const date = new Date();
    date.setTime(date.getTime());
    const today = date.getFullYear()+"-" + (date.getMonth()+1) + "-" + date.getDate();

    return (
      <PageHeaderLayout title="">
        <div>
            <div style={{padding:10,marginBottom:30,backgroundColor:'#fff',borderRadius:'4px'}}>{this.renderAdvancedForm()}</div>
            <Spin spinning={this.state.loading}>
            <Row gutter={{ md: 12, lg: 12, xl:12 }}>
                <Col md={17} sm={24}>
                    <Card className={styles.cardItem}>
                      <div className={styles.cardDiv}>
                        <span>待续保保单保费整体情况</span>
                        <span className={styles.cardItemTitle}>续保单的保险起期：</span>
                        <RangePicker 
                          defaultValue={[moment(`${today}`, dateFormat), moment(`${date.getFullYear()}-12-31`, dateFormat)]}
                          format={dateFormat}
                          onChange={this.onChangeForReData}
                          style={{width: 250}}/>
                      </div>
                      <div style={{marginTop: 10}}>单位:(万)</div>
                      <ForReEcharts forRenewalData={forRenewalTotalData || {}}/>
                    </Card>
                     <Card className={styles.cardItem}> 
                       <div className={styles.cardDiv}>
                        <span>已续保保单保费整体情况</span>
                        <span className={styles.cardItemTitle}>续保单的保险起期：</span>
                        <RangePicker 
                          defaultValue={[moment(`${date.getFullYear()}-1-1`, dateFormat), moment(`${today}`, dateFormat)]}
                          format={dateFormat}
                          onChange={this.onChangeAlReData}
                          style={{width: 250}}/>
                      </div>
                      <div style={{marginTop: 10}}>单位:(万)</div>
                      <AlreadyReEcharts alreadyRenewalData={alreadyRenewalTotalData || {}}/> 
                    </Card>
                     <Card className={styles.cardItem}>
                       <div className={styles.cardDiv}>
                        <span>续保流失整体情况</span>
                        <span className={styles.cardItemTitle}>续保单的保险起期：</span>
                        <RangePicker 
                          defaultValue={[moment(`${date.getFullYear()}-1-1`, dateFormat), moment(`${today}`, dateFormat)]}
                          format={dateFormat}
                          onChange={this.onChangeLostReData}
                          style={{width: 250}}/>
                      </div>
                      <div style={{marginTop: 10}}>单位:(件数)</div>
                      <LostReEcharts lostRenewalTotalData={lostRenewalTotalData || {}}/> 
                    </Card>
                </Col>
                <Col md={7} sm={24}>
                    <Card title="本年续保成功率">
                        <RightChart sucessRenewalTotalData={sucessRenewalTotalData} orderRenewalTotalData={orderRenewalTotalData}/>
                    </Card>
                </Col>
            </Row>
            </Spin>
        </div>
      </PageHeaderLayout>
    );
  }
}
