import React, { PureComponent, Fragment } from 'react';
import echarts from 'echarts/lib/echarts'; // 必须
import Line from './Line'

export default class AlreadyReEcharts extends PureComponent {

  componentDidMount() {
    const echartsDiv = window.document.getElementById('alreadyRenewalTotal');
    window.onreset = () => {
      echartsDiv.resize();
    }
  }

  componentWillReceiveProps(nextProps) {
    const {alreadyRenewalData} = nextProps
    const keys = [];
    const values = [];
    for (const i in alreadyRenewalData) {
      keys.push(i)
      values.push(alreadyRenewalData[i] || 0)
    }
    const Setting = {
      lable: keys,
      data: [values],
      legend: ['已续保'],
      colors: ['#1cff1878'],
      // showLegend: false,
    };
    const LineOption = new Line(Setting);
    const lineEcharts = echarts.init(window.document.getElementById('alreadyRenewalTotal'));
    lineEcharts.setOption(LineOption);
  }

  render() {
    return (
      <div id='alreadyRenewalTotal' style={{width:'100%',minHeight: '280px'}}/>
    );
  }
}
