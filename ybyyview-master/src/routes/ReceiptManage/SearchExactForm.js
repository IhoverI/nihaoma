import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Input,
  Button,
} from 'snk-web';

const FormItem = Form.Item;

@Form.create()
export default class SearchExactForm extends PureComponent {
  handleExactSearch(e){
    this.props.root.searchData(e,this.props.form);
  }
  render() {
    const { form,root } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Form className='custom-form-wrapper' onSubmit={this.handleExactSearch.bind(this)} layout="inline">
          <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
            <Col md={10} sm={24}>
              <FormItem label="保单号">
                {getFieldDecorator('cPlyNo',{
                 rules: [{
                    max: 30, message: '请输入正确的格式',
                  }],
              })(<Input />)}
              </FormItem>
            </Col>
          </Row>
              <div style={{ textAlign: 'center',margin: "15px 0px" }}>
              <span>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
                  重置
                </Button>
              </span>
            </div>
          </Form>
    );
  }
}
