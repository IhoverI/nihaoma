import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  Input,
  DatePicker,
} from 'snk-web';
import { getSelectChild } from '../../utils/utils'
import ChannelAndProduction from '../../components/ChannelAndProduction';

const FormItem = Form.Item;
const { Option } = Select;

@Form.create()
export default class SearchForm extends PureComponent {
  
  constructor(props){
    super(props);
    this.state = {
      
    };
    props.root.nomalForm = props.form;//?
  }

  handleSearch(e){
    this.props.root.searchData(e,this.props.form);
  }
 
  render() {
    const { form,root,loading } = this.props;
    const { getFieldDecorator,getFieldValue } = form;
    const { RangePicker } = DatePicker;

    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper' onSubmit={this.handleSearch.bind(this)} layout="inline">
        <ChannelAndProduction
          oldEnterpCodeMultiple = {true}
          prodNoMultiple = {true}
          channelFieldKey="oldEnterpCodeList"
          productionFieldKey="prodNoList"
          {...this.props} allowClear
        />
        <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="保单年份">
              {getFieldDecorator('pyear', {initialValue: new Date().getFullYear()},
                {rules: [{required: true, message: 'Please input pyear!'}]})(
                <Select allowClear placeholder="请选择" style={{ width: '100%' }}>
                {getSelectChild('pyear')}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="纸质回执状态">
              {getFieldDecorator('paperReceiptFlag', {initialValue: '2'})(
                <Select allowClear placeholder="请选择" style={{ width: '100%' }}>
                {getSelectChild('paperReceiptFlag')}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="快递公司">
              {getFieldDecorator('expressCode')(
                <Select allowClear placeholder="请选择" style={{ width: '100%' }}>
                {getSelectChild('expressCode')}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="投保日期">
              {getFieldDecorator('appDate')(
                <RangePicker style={{ width: '100%' }}  />
              )}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="回执入库日期">
              {getFieldDecorator('scanPigeonholeDate')(
                <RangePicker style={{ width: '100%' }}  />
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
          <span style={{ marginBottom: 24 }}>
            <Button loading={loading} htmlType="submit" type="primary" >
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
              重置
            </Button>
          </span>
        </div>
      </Form>
    );
  }
}
