import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import ChangeModal from './ChangeModal';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Table,
  Upload,
  Tabs,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getDataSource,getSelectChild,datetimeFormat } from '../../utils/utils';
import styles from './ReceiptManage.less';
import SearchForm from './SearchForm';
import SearchExactForm from './SearchExactForm';

const FormItem = Form.Item;

@connect(({ receiptManage, loading }) => ({
  receiptManage,
  loading: loading.models.receiptManage,
}))
export default class ReceiptManage extends PureComponent {

  state = {
    formValues: {},
    pageNum: 1,
    pageSize: 10,
    visible: false,
    record: {}
  };

  componentWillMount(){
    // Object.keys(this.props.receiptManage).forEach(key => delete this.props.receiptManage[key]);
  }

  getColumns = () => {
    return [
      {
        title: '保单号',
        dataIndex: 'cPlyNo',
        align: 'center',
        render: (text, record) => <a href={`/#/query-manage/policy-list?from=receiptmanage&cPlyNo=${record.cPlyNo}`}>{text}</a>
      },
      {
        title: '业务渠道',
        dataIndex: 'oldEnterpCode',
        align: 'center',
      },
      {
        title: '产品名称',
        dataIndex: 'prodNo',
        align: 'center',
      },
      {
        title: '保险起期',
        dataIndex: 'insrncBeginDate',
        align: 'center',
        render: (code) => {
          if (code) {
            return (<span>{moment(code).format('YYYY-MM-DD')}</span>)
          } else {
            return (<div></div>)
          }
        }
      },
      {
        title: '纸质回执状态',
        dataIndex: 'paperReceiptFlag',
        align: 'center',
      },
      {
        title: '回执入库日期',
        dataIndex: 'scanPigeonholeDate',
        align: 'center',
        render: (code) => {
          if (code) {
            return (<span>{moment(code).format('YYYY-MM-DD')}</span>)
          } else {
            return (<div></div>)
          }
        }
      },
      {
        title: '归档编码',
        dataIndex: 'serialNo',
        align: 'center',
      },
      {
        title: '回执影像编码',
        dataIndex: 'eleImageName',
        align: 'center',
      },
      
      {
        title: '操作',
        align: 'center',
        render: (text, record) => <a onClick={(record) => this.showModal(text, record)}>编辑</a>
      }
    ]
  }

  showModal = (record) => {
    this.setState({
      visible: true,
      record: record
    });
  }

  searchData = (e,form) => {
    e.preventDefault();
    this.setState({
        pageNum: 1,
        pageSize: 10
      });
    const { dispatch } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(!fieldsValue.appDate||fieldsValue.appDate.length===0){
        fieldsValue.appDateStart = null;
        fieldsValue.appDateEnd = null;
      }else{
        fieldsValue.appDateStart = fieldsValue.appDate[0].format("YYYY-MM-DD");
        fieldsValue.appDateEnd = fieldsValue.appDate[1].format("YYYY-MM-DD");
      }
      if(!fieldsValue.scanPigeonholeDate||fieldsValue.scanPigeonholeDate.length===0){
        fieldsValue.scanPigeonholeDateStart = null;
        fieldsValue.scanPigeonholeDateEnd = null;
      }else{
        fieldsValue.scanPigeonholeDateStart = fieldsValue.scanPigeonholeDate[0].format("YYYY-MM-DD");
        fieldsValue.scanPigeonholeDateEnd = fieldsValue.scanPigeonholeDate[1].format("YYYY-MM-DD");
      }
      fieldsValue.pageNum = 1;
      fieldsValue.pageSize = 10;
      this.setState({
        formValues: fieldsValue
      })
      dispatch({
        type: 'receiptManage/receiptQuery',
        payload: fieldsValue,
      }).catch(() => {
        message.error('网络异常，请稍后重试')
      });
    });
  }

  onUpDateReceiptSuccess(){
    this.setState({
      visible: false,
    });
    let editPlyNo = {
      cPlyNo: this.state.record.cPlyNo,
    }
    this.props.dispatch({
      type: 'receiptManage/receiptQuery',
      payload: editPlyNo,
    }).catch(() => {
      message.error('网络异常，请稍后重试')
    });
  }

  onCancel() {
    this.setState({
      visible: false,
    })
  }

  handleFormReset = (form) => {
    form.resetFields();
  }


  handleTableChange = (page, pageSize) => {
    let { dispatch } = this.props;
    this.setState({
      pageNum: page,
      pageSize: pageSize
    })
    let values = this.state.formValues;
    values.pageNum = page;
    values.pageSize = pageSize;
    dispatch({
      type: 'receiptManage/receiptQuery',
      payload: values,
    })
  }

  renderAdvancedForm() {
    return (
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab={<span>模糊查询</span>} key="1">
          <SearchForm {...this.props} root={this}/>
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span>精确查询</span>} key="2">
        <SearchExactForm {...this.props} root={this}/>
        </Tabs.TabPane>
      </Tabs>
    );
  }


  render() {
    const { loading,receiptManage: {ReceiptQueryData} } = this.props;
    let list = [];
    let total = 1;
    if (ReceiptQueryData) {
      if (ReceiptQueryData.code == 0) {
        list = ReceiptQueryData.data.list;
        total = ReceiptQueryData.data.total;
      }
    }

    const param = {
      showUploadList:false,
      name: 'file',
      action: `${SERVERLOCAL}ybyy-policy/policy/ExpressManage/importExcel`,
      withCredentials: true,
      onChange(info) {
        if (info.file.status === 'done') {
          if (info.file.response.code === 0) {
            message.success(`《${info.file.name}》: ${info.file.response.message}`,2);
          } else {
            message.error(`《${info.file.name}》上传失败: ${info.file.response.message}`, 4);
          }
        } else if (info.file.status === 'error') {
          message.error(`《${info.file.name}》: file upload failed.`);
        }
        
      }
    };

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
        <div>
            <div>{this.renderAdvancedForm()}</div>
            <Table
              loading={loading}
              scroll={{ x: 1540 }}
              rowKey={record => record.cPlyNo}
              dataSource={list}
              bordered={true}
              size='small'
              title={() => {
                return  <Upload {...param}>
                          <Button>
                            <Icon type="upload" /> 导入Excel清单
                          </Button>
                        </Upload>
              }}
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                current: this.state.pageNum,
                pageSize: this.state.pageSize,
                total:total,
                showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                onChange: (page, pageSize) => this.handleTableChange(page, pageSize),
                showSizeChanger: true,
                pageSizeOptions:['10','20','30','40','50'],
                onShowSizeChange: (page, pageSize) => this.handleTableChange(page, pageSize),
              }}
              columns= {this.getColumns()}
            />
            </div> 
        </Card>
        <Modal
          title="更新回执"
          visible={this.state.visible}
          footer={null}
          onCancel={() => this.onCancel()}
          destroyOnClose={true}
          >
          <ChangeModal
            record={this.state.record}
            onUpDateReceiptSuccess={() => this.onUpDateReceiptSuccess()}
            onCancel={() => this.onCancel()}
            >
          </ChangeModal>
        </Modal>
      </PageHeaderLayout>
    );
  }
}
