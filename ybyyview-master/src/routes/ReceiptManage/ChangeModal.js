import React, { PureComponent } from 'react';
import { Form, Input, Row, Col, Select, Button, message, DatePicker } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';

const FormItem = Form.Item;
const Option = Select.Option

@connect(({ receiptManage, loading }) => ({
  receiptManage,
  loading: loading.models.receiptManage,
}))
@Form.create()
export default class ChangeModal extends PureComponent {

  handleSubmit = () => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.dispatch({
          type: 'receiptManage/updateReceipt',
          payload: values,
        }).then(()=>{
          const { receiptManage } = this.props;
          if (!receiptManage.UpdateResp.code) {
            this.props.onUpDateReceiptSuccess();
          } else {
            message.error(receiptManage.UpdateResp.message);
          }
        }).catch(e=>{
          message.error(e.message||'网络异常请稍后再试');
        });
      }
    });
  };

  onCancel = () => {
    this.props.onCancel();
  }

  render() {

    const { loading } = this.props;

    const { getFieldDecorator } = this.props.form;
    
    const { cPlyNo, serialNo, eleImageName, scanPigeonholeDate } = this.props.record;
    let aa = scanPigeonholeDate !== null ? {initialValue: moment(scanPigeonholeDate)} : '';

    const formItemLayout = {
      labelCol: {
        sm: { span: 7 },
      },
      wrapperCol: {
        sm: { span: 14 },
      },
    };

    return (
      <Form>
        <Row>
          <Col>
            <FormItem {...formItemLayout} label="保单号">
              {getFieldDecorator('cPlyNo',
              {initialValue: cPlyNo})(<Input disabled/>)}
            </FormItem>
            <FormItem {...formItemLayout} label="归档日期">
              {getFieldDecorator('scanPigeonholeDate', {...aa, rules: [{required: true, message: '归档编码必填'}]})
              (<DatePicker/>)}
            </FormItem>
            <FormItem {...formItemLayout} label="归档编码">
              {getFieldDecorator('serialNo', {initialValue: serialNo})
              (<Input placeholder='遗失时可为空'/>)}
            </FormItem>
            <FormItem {...formItemLayout} label="回执电子影像文件名称">
              {getFieldDecorator('eleImageName', {initialValue: eleImageName})
              (<Input placeholder='遗失时可为空'/>)}
            </FormItem>
            <FormItem {...formItemLayout} label="纸质回执状态">
              {getFieldDecorator('paperReceiptFlag')(
                <Select placeholder='若选择已遗失，归档编码和影像名称可为空'>
                  <Option value='1'>已入库</Option>
                  <Option value='3'>已遗失</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ margin: 12 }}>
          <Button loading={loading}  type="primary" onClick={this.handleSubmit}>
            保存
          </Button>
          <Button style={{ margin: 12 }} onClick={this.onCancel}>
            取消
          </Button>
        </div>
      </Form>
    )
  }
}