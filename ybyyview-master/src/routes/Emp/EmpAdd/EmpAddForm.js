import React, { PureComponent, Fragment } from 'react';
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  Dropdown,
  InputNumber,
  DatePicker,
} from 'snk-web';
import Styles from './index.less';
import { getDataSource,getSelectChild } from '../../../utils/utils'

const FormItem = Form.Item;
const { Option } = Select;


const FieldsMap = {
    "applyClntNmeA":"thirdAppName",
    "applyCertfCls":"thirdcertType",
    "applyCertfCde":"thirdcertNo",
};

export default class EmpAddForm extends PureComponent {
  state = {
   showWriteBank:false,
   thirdCardTypeValue:null, //账户类型
  };
  
  componentDidMount() {
    const { dispatch } = this.props;
  
  }

  BranchBankChange(value){
     this.setState({
         showWriteBank:value==='手动录入'
     });
  }

  handleSubmit(){

  }

  SameWithTouBaoRen(){
      this.props.parent.SameWithTouBaoRen(FieldsMap);
  }

  thirdcertTypeChange(value){
    this.props.parent.cardTypeChange(value,"thirdcertType");
  }

  thirdCardTypeChange(value){
    this.setState({thirdCardTypeValue:value});
    // 0203 贷记卡 ，0201 借记卡
    // 根据账户类型 决定 信用卡有效期 的状态 
    if(value=='0201'){
        this.props.form.setFieldsValue({thirdCardDate:""});
        // $("#CARD_END_DATE").val("");
        // $("#CARD_END_DATE").attr('readonly', true);
        // $("#CARD_END_DATE").removeClass("required1");
        // $("#PAY_BANK_CITY").addClass("required1");
    }else{
        // $("#CARD_END_DATE").attr('readonly', false);
        // $("#INSTALLMENT_NUM").attr('readonly', false);
        // $("#CARD_END_DATE").addClass("required1");
        // $("#PAY_BANK_CITY").removeClass("required1");
        // $("#PAY_BANK_CITY").attr('readonly', true);
        // $("#PAY_BANK_CITY").val("");
        this.props.form.setFieldsValue({thirdPayCity:""});
    }
  }
    

  render() {
    const { getFieldDecorator } = this.props.form;
    let thirdPayCity_Props = {}; // 第三方扣款银行所属城市 状态
    let thirdPayCityRule = {}; // 第三方扣款银行所属城市 验证规则
    let thirdCardDate_Props = {}; // 信用卡有效期 状态
    let thirdCardDateRule = {initialValue: 12};// 信用卡有效期 验证规则
    let INSTALLMENT_NUM_Props = {}; // 分期付款次数 状态
    if(this.state.thirdCardTypeValue=='0201'){ //借记卡
        thirdPayCityRule = {
            rules:[
                {
                required:true,message:'必填'
            }]
        };
        thirdPayCity_Props = {
            placeholder:'请输入'
        };
        thirdCardDate_Props = {
            disabled:true,
            placeholder:''
        };
    }else{
        thirdPayCity_Props = {
            disabled:true,
        };
        thirdCardDateRule = {
            initialValue: 12,
            rules:[
                {pattern:/^\d*$/,'message':"必须为数字"},
                {
                required:true,message:'必填'
            }]
        };
        thirdCardDate_Props = {
            placeholder:'请输入'
        };
    }
    return (
        <div className={Styles.warpper}>
            <div className={Styles.sectionTitle}>基础信息 </div>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={12} sm={16}>
                    <FormItem label="姓名">
                    {getFieldDecorator('empName',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="身份证号码">
                    {getFieldDecorator('identityCode',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="出生年月">
                     {getFieldDecorator('birthday',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={16}>
                    <FormItem label="性别">
                    {getFieldDecorator('sex',{
                            nitialValue: '男',
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="年龄">
                     {getFieldDecorator('thirdcertNo',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="入职日期">
                    {getFieldDecorator('thirdcardId',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="电话号码">
                     {getFieldDecorator('mobile',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="邮箱地址">
                    {getFieldDecorator('email',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="社保账户">
                     {getFieldDecorator('socialAccount',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="公积金账户">
                    {getFieldDecorator('accumulationAccount',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="派遣公司名称">
                     {getFieldDecorator('outsourceId',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="培训岗位">
                    {getFieldDecorator('positionName',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="是否为内荐">
                     {getFieldDecorator('isRecommended',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="内荐人">
                    {getFieldDecorator('recommendedPerson',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="紧急联系人">
                     {getFieldDecorator('emergencyPerson',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="紧急联系人电话">
                    {getFieldDecorator('emergencyTel',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>     
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="家庭住址">
                     {getFieldDecorator('homeAddress',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="在职状态">
                    {getFieldDecorator('status',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>         
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="离职日期">
                     {getFieldDecorator('leaveDate',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="培训期">
                    {getFieldDecorator('status',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>     
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="上线日期">
                     {getFieldDecorator('onlineDate',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="转正时间">
                    {getFieldDecorator('status',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>            
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                 <Col md={8} lg={12} sm={20}>
                    <FormItem label="工号">
                     {getFieldDecorator('employeeCode',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>            

            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="分期付款次数">
                      {getFieldDecorator('tms',{
                            initialValue: 12,
                            rules: [{pattern:/^\d*$/,'message':"必须为数字"},{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="信用卡有效期">
                        {getFieldDecorator('thirdCardDate',thirdCardDateRule)(<Input  {...thirdCardDate_Props} />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="扣款金额">
                    {getFieldDecorator('amount',{
                            rules: [{ pattern: /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/, message: '请输入正确的金额' }],
                        })(<Input placeholder="请输入" />)}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="扣款流水号">
                      {getFieldDecorator('payNo')(<Input disabled placeholder="" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="扣款状态">
                    {getFieldDecorator('status')(<Input disabled placeholder="" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="扣款渠道">
                    {getFieldDecorator('payChannel',{
                            rules: [{ required: true, message: '必填!' }],
                        })(
                        <Select  placeholder="请选择" style={{ width: '100%' }}>
                            {getSelectChild('payChannel')}
                        </Select>
                    )}
                    </FormItem>
                </Col>
            </Row>
            <Row gutter={{ md: 24, lg: 24, xl: 48 }}>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="发卡网点">
                      {getFieldDecorator('siteCode')(<Input disabled placeholder="" />)}
                    </FormItem>
                </Col>
                <Col md={8} lg={12} sm={20}>
                    <FormItem label="第三方扣款银行所属城市">
                        {getFieldDecorator('thirdPayCity',thirdPayCityRule)(<Input {...thirdPayCity_Props} />)}
                    </FormItem>
                </Col>
            </Row>
        </div>
    );
  }
}
