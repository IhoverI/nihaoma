import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import moment from 'moment';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import EmpAddForm from '../../Forms/EmpForm/EmpAddForm';
import styles from './index.less';
import { CheckIdCard } from '../../../utils/utils'


const FormItem = Form.Item;
const { Option } = Select;

@connect(({  loading }) => ({
  loading: loading.models.rule,
}))

@Form.create()
export default class SelfChannelForm extends PureComponent {
  state = {
   isLoading:false
  };
  componentDidMount() {
    const { dispatch } = this.props;
  }

  prepare(values){
    // 处理时间格式 appBirth insrncBirth signDateTime
    const timeArr = ["appBirth","insrncBirth","signDateTime"];
    for(let i=0,j=timeArr.length;i<j;i++){
      const itemValue = values[timeArr[i]];
      if(itemValue){
        values[timeArr[i]] = itemValue.format("YYYY-MM-DD");
      }
    }
    // 当投保人的证件类型为 身份证的时候 身份证字段等于证件号码
    if(values["applyCertfCls"] ==="101"){
      values["applyCertfNo"] = values["applyCertfCde"];
    }
    // 当受益人为指定时候 与被保人关系 的值以逗号隔开
    let insrncRelation = values["insrncRelation"];
    if(insrncRelation&&insrncRelation instanceof Array){
        values["insrncRelation"] = insrncRelation.join(",");
    }
    // 当发卡银行为手写的时候，字段thirdBranchBank 取 thirdBranchBankWrite
    let thirdBranchBank = values["thirdBranchBank"];
    if(thirdBranchBank==='手动录入'){
      values["thirdBranchBank"] = values["thirdBranchBankWrite"];
      delete values["thirdBranchBankWrite"];
    }
    console.log(values);
    return values;
  }
  handleSubmit = (e) => {
    const { dispatch } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({
          isLoading:true
        });
        dispatch({
          type:'selfchannelForm/submit',
          payload:this.prepare(values),
        }).then(()=>{
          this.setState({
            isLoading:false
          });
          Modal.success({
            title: '',
            content: '提交成功！',
          });
        }).catch((e)=>{
          this.setState({
            isLoading:false
          });
          Modal.error({
            title: '操作失败',
            content: '提交失败,请稍后再试...',
          });
        });
      }
    });
  }

  SameWithTouBaoRen(FieldsMap){
    const values = this.props.form.getFieldsValue();
    for(const key in FieldsMap){
      const CurKey = FieldsMap[key];
      const re = {};
      re[CurKey] = values[key];
      this.props.form.setFieldsValue(re);
    }
  }

  cardTypeChange(value,fieldKey){
    let validateKey = null;
    if(fieldKey==='applyCertfCls'){
      // 投保人证件类型
      validateKey = "applyCertfCde";
    }else if(fieldKey==='insrncCardType'){
      // 被保人证件类型
      validateKey = "insrncCardId";
    }else if(fieldKey==='thirdcertType'){
       // 扣款人证件号码
       validateKey = "thirdcertNo";
    }
    setTimeout(()=>{
      // 延迟 不然验证的时候 获取的值不正确
      this.props.form.validateFields([validateKey], { force: true });
    },300);
  }

  validatorIDCard(rule,value,callback,fieldKey){
    let isIDCard = false;
    if(fieldKey==='insrncCardId'){
      //被保人证件
      isIDCard = this.props.form.getFieldValue("insrncCardType")==='101';
    }else if(fieldKey==='applyCertfNo'){
      // 投保人身份证
      isIDCard = true;
    }else if(fieldKey==='applyCertfCde'){
      // 投保人证件号码
      isIDCard = this.props.form.getFieldValue("applyCertfCls")==='101'; 
    }else if(fieldKey==='thirdcertNo'){
      // 扣款人证件号码
      isIDCard = this.props.form.getFieldValue("thirdcertType")==='101'; 
    }
    callback();
  }

  calculateSexAndBirth(value,fieldKey){
    // 根据身份证推出性别
    const sexMap = {
      "applyCertfNo":"applySex",
      "applyCertfCde":"applySex",
      "insrncCardId":"insrncSex",
    };
    const target = sexMap[fieldKey];
    if(target){
      const cardValue = this.props.form.getFieldValue(fieldKey);
      var re = {};
      // 偶数为女
      if(cardValue.substr(16, 1) % 2 == 0){
        re[target] = "2";
      }else{
        re[target] = "1";
      }
      this.props.form.setFieldsValue(re);
    }
    // 根据身份证推断出 生日
    const BrithMap = {
      "applyCertfNo":"appBirth",
      "applyCertfCde":"appBirth",
      "insrncCardId":"insrncBirth",
    };
    const birthTarget = BrithMap[fieldKey];
    if(birthTarget){
      const curCardValue = this.props.form.getFieldValue(fieldKey)
      const birthStr = curCardValue.substr(6, 4)+"-"+curCardValue.substr(10, 2)+"-"+curCardValue.substr(12, 2);
      const brithRe = {};
      brithRe[birthTarget] = moment(birthStr,'YYYY-MM-DD')
      this.props.form.setFieldsValue(brithRe);
    }
  }

  /*
    计算出所交保费
    1. 在验证完 投保人 身份证的时候 调用
    2. 在选择完 产品代码 时候调用
    3. 在选择完 款别的时候调用
  */
  calculatePRM(){
    setTimeout(()=>{
      this._calculatePRM();
    },300);
  }

  _calculatePRM(){
    var prod_type= this.props.form.getFieldValue("peeType");//款别,0/1/2对应第二列
    var prod_no= this.props.form.getFieldValue("prodNo");//商品代码
    var cert_no="";
    let PRM_Value = "";
    var APP_CERT_NO=this.props.form.getFieldValue("applyCertfNo");//身份证号
    var APP_CERT_NUM=this.props.form.getFieldValue("applyCertfCde");//证件号码
    var APP_CERT_TYPE=this.props.form.getFieldValue("applyCertfCls")//证件类别
    if(APP_CERT_TYPE == '101'){
      cert_no=APP_CERT_NUM;
    }else{
      cert_no=APP_CERT_NO;
    }
    var i = -1;
    var TheArray = [["62","93","124"],["79","119","158"],["89","134","178"],["110","165","220"],["131","197","262"],["131","197","262"]];
    var type=[65,108,134];
    var birthday=new Date(cert_no.substr(6, 4)+"/"+cert_no.substr(10, 2)+"/"+cert_no.substr(12, 2));
    var now = new Date();
    if(prod_type!==undefined && cert_no ){
      if(prod_no === '1223'){//祥泰安康
        var s1=(now.getTime()-birthday.getTime())/(60*60*24*365*1000);
        if(s1 >=59 ){
          i=5;
        }else if(s1 >=56){
          i=4;
        }else if(s1 >= 46){
          i=3;
        }else if(s1 >= 36){
          i=2;
        }else if(s1 >= 26){
          i=1;
        }else if(s1 >= 18){
          i=0;
        }else{
         
        }	
        if(i>=0){
          PRM_Value = ((TheArray[i][prod_type])*12);
        }
        
      }else if(prod_no === '06A8'){//尊尚人生
        PRM_Value = (type[prod_type]*12);
      }
    }

    this.props.form.setFieldsValue({"prm":PRM_Value});
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <PageHeaderLayout title="人员基本信息">
        <Form className='custom-form-wrapper' onSubmit={this.handleSubmit.bind(this)} layout="inline">
            <Card bordered={false}>
            <EmpAddForm parent={this} {...this.props}/>
                <Divider/>
                <div style={{textAlign:'center'}}>
                  <Button loading={this.state.isLoading} type="primary" htmlType="submit">
                    提交
                  </Button>
                </div>
            </Card>
        </Form>
      </PageHeaderLayout>
    );
  }
}
