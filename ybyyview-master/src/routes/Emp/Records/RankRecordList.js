import React, { PureComponent, Fragment } from 'react';
import { Table,Popconfirm } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';

import EmpTable from '../../../components/EmpTable';
import EditableCell from '../../../components/EmpTable/EditableCell';
import StandardTable from '../../../components/StandardTable';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';

import styles from './RankRecordList.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const firstLineFlagMap = ['一线', '非一线'];
const statusMap = ['生效', '待生效'];

/*
 * 新增职级弹出框
*/
const CreateRankForm = Form.create()(props => {
  const { rankVisible, form, handleAdd, handleRankVisible} = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  
  return (
    <Modal
      title="新建职级"
      visible={rankVisible}
      onOk={okHandle}
      onCancel={() => handleRankVisible(false)}
    >
     <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="岗位" >
        {form.getFieldDecorator('positionName', {
          rules: [{ required: true, message: '岗位名称不能为空' }],
        })
        (<Input placeholder="请输入岗位" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="职级" >
        {form.getFieldDecorator('rankName', {
          rules: [{ required: true, message: '职级名称不能为空' }],
        })
        (<Input placeholder="请输入岗位" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="序号">
        {form.getFieldDecorator('order', {
          rules: [{ required: true, message: '序号不能为空' }],
        })(<Input placeholder="请输入职级" />)}
      </FormItem>
    </Modal>
  );
});

/*
 * 新增岗位弹出框
*/
const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      title="新建岗位"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible(false)}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="岗位">
        {form.getFieldDecorator('positionName', {
          rules: [{ required: true, message: '岗位名称不能为空' }],
        })(<Input placeholder="请输入" />)}
      </FormItem>
    </Modal>
  );
});

const EditableCell2 = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
      : value
    }
  </div>
);


@connect(({positions,loading}) => ({
  positions,
  loading: loading.models.positions,
}))

@Form.create()
export default class RankRecordList extends PureComponent {
  state = {
    dataSource: {},
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
  };
  

  componentDidMount() {
    console.log('componentDidMount called..');
    const { dispatch,positions,dataSource} = this.props;
    dispatch({
      type: 'positions/fetchList',
    }).then(()=>{
      this.setState({dataSource:positions.data});
      console.log(this.state);
    }).catch((e)=>{
      console.log(e);
    });
    
  }


  renderColumns(text, record, column) {
    return (
      <EditableCell2
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  edit(record,dataSource) { 
    console.log('edit called...');
    console.log(this.state);
    const {key} = record;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    console.log(target);
    if (target) {
      record.editable = true;
      this.setState({ data: newData });
    }
  }

  save(record) {
    const {key} = record;
    console.log('save called...');
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    const { dispatch } = this.props;
    if (target) {
      delete record.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
      console.log('record is..');
      console.log(record);
      let updatePosition = {
        id: record.id,
        positionName: record.positionName,
      };
      dispatch({
        type: 'positions/editPosition',
        data: updatePosition,
      }).catch((e)=>{
        console.log(e);
      });
    }
  }

  cancel(record) {
    console.log('cancel called...');
    const {key} = record;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    if (target) {
      console.log(this.cacheData);
      //Object.assign(target, this.cacheData.filter(item => key === record.key)[0]);
      delete record.editable;
      this.setState({ data: newData });
    }
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource = this.state.dataSource;
      // const target = dataSource.find(item => item.key === key);
      // if (target) {
      //   target[dataIndex] = value;
      //   this.setState({ dataSource });
      //   alert(this.state.dataSource[0].name);
      // }
    };
  }

  handleChange(value, key, column) {
    const newData = [...this.state.dataSource];
    console.log('handleChange called');
    console.log(newData);
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ dataSource: newData });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'rule/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  };


  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            no: selectedRows.map(row => row.no).join(','),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'rule/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleRankVisible = flag => {
    console.log("list handleRankVisible called");
    this.setState({
      rankVisible: !!flag,
    });
    console.log(flag);
    console.log(!!flag);
    console.log(this.state.rankVisible);
  };


  handleAdd = fields => {
    console.log('handleAdd called...');
    console.log(fields);
    this.props.dispatch({
      type: 'positions/addPosition',
      data: {
        positionName: fields.positionName,
      },
    });

    message.success('添加成功');
    this.setState({
      modalVisible: false,
    });
  };


  expandedRowRender = () => {
  return (<p>展示</p>);
  }

  handleExpandChange = (enable) => {
    this.setState({ expandedRowRender: enable ? expandedRowRender : undefined });
  }

  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 24 }}>
          <Col md={16} sm={24}>
            <FormItem label="关键字查询">
              {getFieldDecorator('no')(<Input placeholder="可根据岗位名称、职级进行搜索" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="关键字查询">
              {getFieldDecorator('no')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <span style={{ float: 'right', marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              收起 <Icon type="up" />
            </a>
          </span>
        </div>
      </Form>
    );
  }

  renderForm() {
    return this.state.expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  render() {
    console.log('positionList render called..');
    const {loading, positions} = this.props;
    let { selectedRows, modalVisible, rankVisible, dataSource} = this.state;
    dataSource = positions.data;
    //this.setState({dataSource:dataSource});
    //this.cacheData = dataSource.map(item => ({ ...item }));
    this.state.dataSource = dataSource;
    console.log(this.state.dataSource);
    const columns = [
      {
        title: '岗位名称',
        dataIndex: 'positionName',
        editable:false,
        width: '20%',
        render: (text, record) => this.renderColumns(text, record, 'positionName'),
      },
      {
        title: '职级',
        dataIndex: 'rank',
        width: '25%',
        editable:false,
      },
      {
        title: '更新日期',
        dataIndex: 'updatedTime',
        width: '20%',
        editable:false,
        render(val){
          return <div> {moment(val).format("YYYY-MM-DD HH:mm:ss")} </div>
        } 
      },
      {
        title: '类别',
        dataIndex: 'firstLineFlag',
        width: '10%',
        filters: [
          {
            text: firstLineFlagMap[0],
            value: 0,
          },
          {
            text: firstLineFlagMap[1],
            value: 1,
          },
        ],
        onFilter: (value, record) => record.status.toString() === value,
        render(val) {
          return <Badge status={firstLineFlagMap[val]} text={firstLineFlagMap[val]} />;
        },
      },
      {
        title: '状态',
        dataIndex: 'status',
        width: '10%',
        filters: [
          {
            text: statusMap[0],
            value: 0,
          },
          {
            text: statusMap[1],
            value: 1,
          },
        ],
        onFilter: (value, record) => record.status.toString() === value,
        render(val) {
          return <Badge status={statusMap[val]} text={statusMap[val]} />;
        },
      },
      {
        title: '操作',
        width: '25%',
        render: (text, record) => {
          const { editable } = record;
          return (
            <Fragment>
                {
                  editable ?
                    <span>
                      <a onClick={() => this.save(record)}>保存</a>
                      <Popconfirm title="确定要放弃编辑吗?" onConfirm={() => this.cancel(record)}>
                        <a> 取消</a>
                      </Popconfirm>
                    </span>
                    : <a onClick={() => this.edit(record)}> 编辑</a>
                }
            </Fragment>
          );
        },
      },
    ];

    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleRankVisible: this.handleRankVisible,
      handleModalVisible: this.handleModalVisible,
    };

    return (
      <PageHeaderLayout title="查询与编辑岗位职级">
        <Card bordered={false}>
          <div className={styles.positionList}>
            <div className={styles.positionListForm}>{this.renderForm()}</div>
            <div className={styles.positionListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新建岗位
              </Button>
            </div>
            <EmpTable
             selectedRows={selectedRows}
             loading={loading}
             dataSource={dataSource}
             state={dataSource}
             columns={columns}
             onSelectRow={this.handleSelectRows}
             rankVisible={this.rankVisible}
             onChange={this.handleStandardTableChange}
            />
           
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <CreateRankForm {...parentMethods} rankVisible={rankVisible} />
      </PageHeaderLayout>
    );
  }
}
