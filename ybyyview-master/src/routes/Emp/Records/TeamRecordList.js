import React, { PureComponent, Fragment } from 'react';
import { Table,Popconfirm } from 'snk-web';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';

import EditableCell from '../../../components/EmpTable/EditableCell';
import TeamTable from '../../../components/TeamRecordTable';
import StandardTable from '../../../components/StandardTable';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';

import styles from './TeamRecordList.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const firstLineFlagMap = ['一线', '非一线'];
const statusMap = ['生效', '待生效'];

/*
 * 新增职级弹出框
*/
const CreateRankForm = Form.create()(props => {
  const { rankVisible, form, handleAdd, handleRankVisible} = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  
  return (
    <Modal
      title="新建职级"
      visible={rankVisible}
      onOk={okHandle}
      onCancel={() => handleRankVisible(false)}
    >
     <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="岗位" >
        {form.getFieldDecorator('positionName', {
          rules: [{ required: true, message: '岗位名称不能为空' }],
        })
        (<Input placeholder="请输入岗位" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="职级" >
        {form.getFieldDecorator('rankName', {
          rules: [{ required: true, message: '职级名称不能为空' }],
        })
        (<Input placeholder="请输入岗位" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="序号">
        {form.getFieldDecorator('order', {
          rules: [{ required: true, message: '序号不能为空' }],
        })(<Input placeholder="请输入职级" />)}
      </FormItem>
    </Modal>
  );
});



const EditableCell2 = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
      : value
    }
  </div>
);


@connect(({empRecord,loading,positions}) => ({
  empRecord,
  positions,
  loading: loading.models.empRecord,
}))

@Form.create()
export default class TeamRecordList extends PureComponent {
   state = {
      dataSource: {},
      modalVisible: false,
      expandForm: false,
      selectedRows: [],
      formValues: {},
      positions:{},
      empRecord:{},
    };

  componentDidMount() {
    console.log('componentDidMount called..');
    const {dispatch} = this.props;
    const {dataSource} = this.state;
    dispatch({
      type: 'empRecord/search_team_record',
    }).then(()=>{
      this.setState({dataSource:this.props.empRecord.data});

      this.setState({dataSource:this.props.empRecord.data}, function () {
        console.log("*******+++");
        console.log(this.state.dataSource);
      });
      //this.state.dataSource = this.props.empRecord;
      //this.setState({empRecord:this.props.empRecord});
      console.log("*******");
      console.log(this.state);
      console.log("*******--");
      console.log(this.props.empRecord);
      console.log("*******--");
      console.log(this.state.dataSource);
    }).catch((e)=>{
      console.log(e);
    });
  }

  renderColumns(text, record, column) {
    return (
      <EditableCell2
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  edit(record,dataSource) { 
    console.log('edit called...');
    console.log(this.state);
    const {key} = record;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    console.log(target);
    if (target) {
      record.editable = true;
      this.setState({ data: newData });
    }
  }

  save(record) {
    const {key} = record;
    console.log('save called...');
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    const { dispatch } = this.props;
    if (target) {
      delete record.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
      console.log('record is..');
      console.log(record);
      let updatePosition = {
        id: record.id,
        positionName: record.positionName,
      };
      dispatch({
        type: 'positions/editPosition',
        data: updatePosition,
      }).catch((e)=>{
        console.log(e);
      });
    }
  }

  cancel(record) {
    console.log('cancel called...');
    const {key} = record;
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    if (target) {
      console.log(this.cacheData);
      //Object.assign(target, this.cacheData.filter(item => key === record.key)[0]);
      delete record.editable;
      this.setState({ data: newData });
    }
  }

  onCellChange = (key, dataIndex) => {
    return (value) => {
      const dataSource = this.state.dataSource;
      // const target = dataSource.find(item => item.key === key);
      // if (target) {
      //   target[dataIndex] = value;
      //   this.setState({ dataSource });
      //   alert(this.state.dataSource[0].name);
      // }
    };
  }

  handleChange(value, key, column) {
    const newData = [...this.state.dataSource];
    console.log('handleChange called');
    console.log(newData);
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ dataSource: newData });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'rule/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  };


  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            no: selectedRows.map(row => row.no).join(','),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'rule/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleRankVisible = flag => {
    console.log("list handleRankVisible called");
    this.setState({
      rankVisible: !!flag,
    });
    console.log(flag);
    console.log(!!flag);
    console.log(this.state.rankVisible);
  };


  handleAdd = fields => {
    console.log('handleAdd called...');
    console.log(fields);
    this.props.dispatch({
      type: 'positions/addPosition',
      data: {
        positionName: fields.positionName,
      },
    });

    message.success('添加成功');
    this.setState({
      modalVisible: false,
    });
  };


  expandedRowRender = () => {
  return (<p>展示</p>);
  }

  handleExpandChange = (enable) => {
    this.setState({ expandedRowRender: enable ? expandedRowRender : undefined });
  }



  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="姓名">
              {getFieldDecorator('empName')(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="所在团队">
              {getFieldDecorator('groupName')(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="所在组别">
              {getFieldDecorator('teamName')(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="岗位">
              {getFieldDecorator('positionName')(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="职级">
              {getFieldDecorator('rankName')(<Input disabled placeholder="" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderAdvancedForm();
  }

  render() {
    console.log('teamRecord render called..');
    const {loading, position} = this.props;
    let { selectedRows, modalVisible, rankVisible, empRecord, dataSource} = this.state;
    console.log(dataSource.recordList);
    dataSource = dataSource.recordList;
/*
    dataSource = [
      {
      teamName: '高铁组',
      bgnTime: '1514736000',
      endTime: '1546272000',
      key:'27',
    }, {
      teamName: '飞机组',
      bgnTime: '1514736000',
      endTime: '1546272000',
      key:'28',
    },],*/


    //this.setState({dataSource:dataSource});
    //this.cacheData = dataSource.map(item => ({ ...item }));
    //this.state.dataSource = dataSource;
    console.log(">>>>>>>>>>>==");
    console.log(this.state.dataSource);
    const columns = [
      {
        title: '组别编码',
        dataIndex: 'key',
        editable:false,
        width: '15%',
        render: (text, record) => this.renderColumns(text, record, 'positionName'),
      },
      {
        title: '组别名称',
        dataIndex: 'teamName',
        width: '20%',
        editable:false,
        render(val){
           return 
       
            <Input.Group compact>
                <Select defaultValue="alipay" style={{ width: 100 }}>
                  <Option value="alipay">支付宝</Option>
                  <Option value="bank">银行账户</Option>
                </Select>
                {getFieldDecorator('receiverAccount', {
                  initialValue: data.receiverAccount,
                  rules: [
                    { required: true, message: '请输入收款人账户' },
                    { type: 'email', message: '账户名应为邮箱格式' },
                  ],
                })(<Input style={{ width: 'calc(100% - 100px)' }} placeholder="test@example.com" />)}
             </Input.Group>
       
        }
      },
      {
        title: '挂靠开始时间',
        dataIndex: 'bgnTime',
        width: '20%',
        editable:true,
        render(val){
          return <div> {moment(val).format("YYYY-MM-DD HH:mm:ss")} </div>
        } 
      },
      {
        title: '挂靠结束时间',
        dataIndex: 'endTime',
        width: '20%',
        editable:true,
        render(val){
          return <div> {moment(val).format("YYYY-MM-DD HH:mm:ss")} </div>
        } 
      },
      {
        title: '操作',
        width: '25%',
        render: (text, record) => {
          const { editable } = record;
          return (
            <Fragment>
                {
                  editable ?
                    <span>
                      <a onClick={() => this.save(record)}>保存</a>
                      <Popconfirm title="确定要放弃编辑吗?" onConfirm={() => this.cancel(record)}>
                        <a> 取消</a>
                      </Popconfirm>
                    </span>
                    : <a onClick={() => this.edit(record)}> 编辑</a>
                }
            </Fragment>
          );
        },
      },
    ];

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleRankVisible: this.handleRankVisible,
      handleModalVisible: this.handleModalVisible,
    };

    return (
      <PageHeaderLayout title="查询与编辑组别轨迹">
        <Card bordered={false}>
          <div className={styles.positionList}>
            <div className={styles.positionListForm}>{this.renderForm()}</div>
            <div className={styles.positionListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增轨迹
              </Button>
            </div>
            <TeamTable
             selectedRows={selectedRows}
             loading={loading}
             dataSource={dataSource}
             state={dataSource}
             columns={columns}
             onSelectRow={this.handleSelectRows}
             rankVisible={this.rankVisible}
             onChange={this.handleStandardTableChange}
            />
           
          </div>
        </Card>
        <CreateRankForm {...parentMethods} rankVisible={rankVisible} />
      </PageHeaderLayout>
    );
  }
}
