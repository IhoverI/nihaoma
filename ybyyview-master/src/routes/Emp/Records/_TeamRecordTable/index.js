import { Table, Input, Icon, Button, Popconfirm, Divider, Badge, Form, Modal} from 'snk-web';
import  React,{ Fragment } from 'react';
import styles from './index.less';
import { stat } from 'fs';
import { connect } from 'dva';
import moment from 'moment';

const firstLineFlagMap = ['一线', '非一线'];
const statusMap = ['生效', '待生效'];
const FormItem = Form.Item;

/*
 * 新增职级弹出框
*/
  const CreateRankForm = Form.create()(props => {
  console.log('CreateRankForm called..');
  const {rankVisible, form, handleAdd, handleRankVisible} = props;
  console.log(rankVisible);
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      this.handleAdd(fieldsValue);
    });
  };
 
  return (
    <Modal
      title="新建职级"
      visible={rankVisible}
      onOk={okHandle}
      onCancel={() => handleRankVisible(false)}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="职级" >
        {form.getFieldDecorator('rankName', {
          rules: [{ required: true, message: '职级名称不能为空' }],
        })
        (<Input placeholder="请输入职级" type='text' />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="序号">
        {form.getFieldDecorator('order', {
          rules: [{ required: true, message: '序号不能为空' }],
        })(<Input placeholder="请输入序号" />)}
      </FormItem>
    </Modal>
  );
});

@connect(({positions,loading}) => ({
  positions,
  loading: loading.models.positions,
}))

class EditableTable extends React.Component {
  constructor(props) {
    console.log('constructor called...');
    super(props);
    const { dataSource, rankVisible } = this.props;
    this.columns = this.props.columns;
    this.state = this.props;
  }

  // componentWillReceiveProps(nextprops,nextstate){
  //   if(JSON.stringify(nextprops.dataSource)!==JSON.stringify(this.state.dataSource)){
  //     this.setState({
  //       dataSource:nextprops.dataSource
  //     });
  //   }
  // }

  handleAdd = fields => {
    console.log('handleAdd called...');
    console.log(fields);
    this.props.dispatch({
      type: 'positions/addRank',
      data: {
        positionName: fields.positionName,
      },
    }).then(()=>{
      message.success('添加成功');
      this.setState({
        rankVisible: false,
      });
    }).catch((e)=>{
      console.log(e);
    });
  };

  handleRankVisible = flag => {
    console.log("handleRankVisible called..."+(!!flag));
    this.setState({
      rankVisible: !!flag,
    });
    console.log(this.state.rankVisible);
  };

  edit(record,dataSource) { 
    console.log('edit called...');
    const {key} = record;
    const newData = [...this.props.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    console.log(this.props);
    if (target) {
      record.editable = true;
      this.setState({ dataSource: newData });
    }
  }

  save(record,dataSource) {
    const {key} = record;
    console.log('save called...');
    const newData = [...this.props.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    const { dispatch } = this.props;
    if (target) {
      delete record.editable;
      this.setState({ dataSource: newData });
      this.cacheData = newData.map(item => ({ ...item }));
      console.log('record is..');
      console.log(record);
      let updateRank = {
        id: record.id,
        rankName: record.rankName,
      };
      dispatch({
        type: 'positions/editRank',
        data: updateRank,
      }).catch((e)=>{
        console.log(e);
      });
    }
    /*
    const {key} = record;
    const newData = [...this.props.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    if (target) {
      delete record.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(record => ({ ...record }));
    }*/
  }


  cancel(record,dataSource) {
    const {key} = record;
    const newData = [...this.props.dataSource];
    const target = newData.filter(item => key === record.key)[0];
    if (target) {
      //Object.assign(target, this.cacheData.filter(item => key === record.key)[0]);
      delete record.editable;
      this.setState({ dataSource: newData });
    }
  }

  handleChange(value, record, column) {
    const newData = [...this.state.dataSource];
    const key = record.key;
    console.log('handleChange called..');
    const target = newData.filter(item => key === record.key)[0];
    
    if (target) {
      record[column] = value;
      this.setState({ dataSource: newData });
    }
  }


  onCellChange = (key, dataIndex) => {
    console.log('expandedRowRender called..');
    return (value) => {
      const dataSource = this.state.dataSource;
      // const target = dataSource.find(item => item.key === key);
      // if (target) {
      //   target[dataIndex] = value;
      //   this.setState({ dataSource });
      //   alert(this.state.dataSource[0].name);
      // }
    };
  }


  renderColumns(text, record, column) {
    const EditableCell = ({ editable, value, onChange }) => (
      <div>
        {editable
          ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
          : value
        }
      </div>
    );

    return (
      <EditableCell
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record, column)}
      />
    );
  }

  render() {
    console.log('index render called...');
    const { dataSource, rankVisible } = this.props;
    const columns = this.columns;
    //this.state.dataSource = dataSource;
    this.cacheData = dataSource.map(item => ({ ...item }));
    console.log(dataSource);
    return (
      <div>
        <Table bordered dataSource={dataSource} columns={columns} indentSize={20} />
      </div>
    );
  }
}

  export default EditableTable;
  