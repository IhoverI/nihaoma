import { 
  Table, 
  Popconfirm,
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider} from 'snk-web';
import React, { PureComponent, Fragment } from 'react';
import { routerRedux } from 'dva/router';
import styles from './OutSource.less';
import {PermissionWrapper,PermissinSelectOption} from 'snk-sso-um';
import moment from 'moment';
import { connect } from 'dva';
//import ClmForm from '../../components/ClmForm';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { isArray } from 'util';


const FormItem = Form.Item;

//table 行编辑的属性定义
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

//table 单元格的定义
class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (      
      <EditableContext.Consumer>        
        {(form) => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [{
                      // required: true,
                      message: `请输入 ${title}!`,
                    }],
                    initialValue: record[dataIndex],
                  })(this.getInput())}
                </FormItem>
              ) : restProps.children}
            </td>             
          );
        }}
      </EditableContext.Consumer>
    );
  }

}



//装饰器
@connect(({umssouserinfo, outSource,loading}) => ({
  umssouserinfo,
  outSource,
  loading:loading.models.outSource
}))


@Form.create()
export default class OutSource extends React.Component {
    state = {
      dataSource: [],
      modalVisible: false,   
      selectedRows: [],
      data: [],
      value: '',
      recommend: {},
      pageNum: 1,
      pageSize: 10,
      editingKey: '',
      groupData:[],
      editingRecord: {},
    };

    //构造table结构
    constructor(props){
      super(props);
      this.columns = [
        {
          title: '操作',
          dataIndex: 'operation',
          width: '150px',
          render: (text, record) => {
            const editable = this.isEditing(record);
            return (
              <div>
                {editable ? (
                  <span>
                    <EditableContext.Consumer>
                      {form => (
                        <a
                          href="javascript:;"
                          onClick={() => this.save(form, record.key)}
                          style={{ marginRight: 8 }}
                        >
                          保存
                        </a>
                      )}
                    </EditableContext.Consumer>
                      <Popconfirm
                        title="确定放弃编辑?"
                        onConfirm={() => {                     
                          this.cancel(record.key)
                        }}
                      >
                        <a style={{ marginRight: 8 }}>取消</a>
                      </Popconfirm>
  
                      {(record.key + '').indexOf('new') == -1 ?
                        (<Popconfirm
                          title="确定删除?"
                          onConfirm={() => {
                            this.deleteRecord(record.key);
                          }}
                        >
                          <a style={{ marginRight: 8 }}>删除</a>
                        </Popconfirm>):(<div></div>)}
                  </span>
                ) : (
                  <span>
                      <a onClick={() => this.edit(record)}>编辑</a>
                  </span>
                )}
              </div>
            );
          },
        },
        {
          title: '派遣公司名称',
          dataIndex: 'outsourceName',
          width: '130px',
          editable:true, 
        },
        {
          title: '团队',
          dataIndex: 'groupId',
          width: '130px',
          editable:false,
          render: (text, record) => {    
            const {groupId} = record;
            //定义options
            const gId = record.groupId + '';
            let groupOptions = [];
            groupOptions = this.props.outSource.groupData.map(
              group => <Select.Option key={group.id}>{group.groupName}</Select.Option>
            );
              const val = this.props.outSource.groupData.filter(e=>e.id==gId)
               
              const name = val.length>0?val[0].groupName:''
            return ( <div>
              {(this.state.editingKey == record.key) ? (
                 <Select style={{ width: 120 }} 
                   onChange={(recordSelected) => this.handleGroupChange(recordSelected)} value = {gId} >
                   {groupOptions}
                     </Select>
               ): <span>{name}</span>
               }
             </div>)
          },
        
        },  
        {
          title: '负责人',
          dataIndex: 'contactPerson',
          width: '110px',
          editable:true,
        },
        {
          title: '联系电话',
          dataIndex: 'contactTel',
          width: '120px',
          editable:true,
        },
        {
          title: '邮箱地址',
          dataIndex: 'contactEmail',
          width: '130px',
          editable:true,
        },
        {
          title: '公司地址',
          dataIndex: 'address',
          width: '140px',
          editable:true,
        },
        {
          title: '开户机构',
          dataIndex: 'bankAccountName',
          width: '130px',
          editable:true,
        },
        {
          title: '银行账户',
          dataIndex: 'bankAccount',
          width: '130px',
          editable:true,
        },
        {
          title: '备注',
          dataIndex: 'remark',
          width: '130px',
          editable:true,
        }
      ];
    }

    componentDidMount() {

      console.log('componentDidMount called..');
      const { dispatch, umssouserinfo:{currentUser} } = this.props;


      //获取团队名称的数据
      dispatch({
        type: 'outSource/searchGroup',
        data: "",
      }).then(()=>{
        console.log(this.props.outSource.groupData);
        this.setState({groupData: this.props.outSource.groupData});
      }).catch((e)=>{
        console.log(e);
      });

      dispatch({
        type: 'outSource/selectOutSourceInfo',
        data:{}
      }).then(()=>{
        this.setState({dataSource: this.props.outSource.list});
      }).catch((e)=>{
        console.log(e);
      });
  
      
    }

    //根据团队名称查询派遣公司
    handleSearch = e => {
      //防止链接打开 URL
      if(e){
        e.preventDefault();
      }
      const { dispatch, form } = this.props;

      //表单校验
      form.validateFields((err, fieldsValue) => {
        if (err) return;

        const values = {
          ...fieldsValue,         //表单所有组件的值
          updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
          pageNum: this.state.pageNum,
          pageSize: this.state.pageSize,
        };
    
        //查询请求
        dispatch({
          type: 'outSource/selectOutSourceInfo',
          data: values,
        }).then(()=>{
          this.setState({dataSource: this.props.outSource.list});
        }).catch((e)=>{
          console.log(e);
        });
      });
    }

    //重置
    handleFormReset = () => {
      const { form, dispatch } = this.props;
      form.resetFields();
      this.setState({
        formValues: {},
      });
    };

    //团队名称改变事件
    handleGroupChange = (recordSelected) => { 
      const newData = [...this.state.dataSource];
      const {key} = this.state.editingRecord;
      const index = newData.findIndex(item => key === item.key);
console.log('index是行位置索引：'+ index);
console.log('key是派遣公司主键：'+ key);
console.log('recordSelected是团队主键：'+ recordSelected);

    if (index > -1) {
      let item = newData[index];
      item.groupId = recordSelected;
      newData.splice(index, 1, {
        ...this.state.editingRecord,
        ...item,
      });
      this.setState({ dataSource: newData });
    }

      // let item = newData[index];
      // item.groupId = recordSelected;
      // newData.splice(index, 1, {
      //   ...this.state.editingRecord,
      //   ...item,
      // });
      // this.setState({ dataSource: newData });
    }


    //分页 
    handleStandardTableChange = (pagination, filtersArg, sorter) => {
        console.log("handleStandardTableChange called..");
        console.log(pagination);
        this.setState({
          pageNum: pagination.current,
          pageSize: pagination.pageSize,
        }, ()=> {
          this.handleSearch('');
        });
    }

    //手动选中
    handleSelectRows = rows => {
      this.setState({
        selectedRows: rows,
      });
    }

    //新建派遣公司
    handleAdd = () => {

      const {dataSource, editingKey} = this.state;

      if(editingKey != ''){
        message.error("你有规则在编辑状态，请先处理后再新增！");
      }else{
        const key = 'new' + Math.random() * 1000;
        const newData = {
          key: key,
          groupId:''
        };
        if(dataSource){
          this.setState({
            dataSource: [...dataSource, newData],
          });
        }   
        this.edit(newData); // 状态更新为"编辑中",规则编码默认new
      }
    }
   

    //table 行编辑事件
    isEditing = (record) => {
      return record.key === this.state.editingKey;   
    };

    edit(record){
      const {editingKey} = this.state;
      if(editingKey != ''){
        message.error("你有规则正在编辑状态，请先处理后再编辑！")      
      }else{
        this.setState({ editingKey: record.key, editingRecord: record});
      }
    }

    save(form, key) {
      form.validateFields((error, row) => {
        if (error) {
          return;
        }
        //获取数据源

        const newData = [...this.state.dataSource];
        //校验派遣公司
        if(this.outSourceIsNotNull(key, row)){
          return;
        }
        if(this.groupIdNotNull(key, newData)){
          return;
        }

        const index = newData.findIndex(item => key === item.key);  
        if (index > -1) {
          const item = newData[index];
          
          newData.splice(index, 1, {
            ...item,
            ...row,
          });
          
          //获取本行 index 的 jsonObj
          let outData = newData[index];          
          //拼接参数
          let toSaveData = {
              id:outData.id,
              groupId: outData.groupId,
              outsourceName: outData.outsourceName,
              contactPerson: outData.contactPerson,
              contactTel: outData.contactTel,
              contactEmail: outData.contactEmail,
              address: outData.address,
              bankAccountName: outData.bankAccountName,
              bankAccount: outData.bankAccount,
              remark: outData.remark
          }

          if(outData.outsourceName){
            this.setState({ dataSource: newData, editingKey: ''});
          }else{
            console.log('--------'+ outData.outsourceName + '000');
            message.error('请输入劳务派遣公司!');
            return;
          }
        
          //定义path
          const {dispatch} = this.props;
          if(this.state.editingKey && ((this.state.editingKey + '').indexOf('new') != -1)){
            dispatch({
              type: 'outSource/insertOutSource',
              data: toSaveData,
            }).then(()=>{
              // 保存后，把ruleId更新到data对应的record中
              const newData = [...this.state.dataSource];
              
              const index = newData.findIndex(item => key === item.key);
              if (index > -1) {
                const item = newData[index];
                
                // item.key = this.props.outSource.;
                newData.splice(index, 1, {
                  ...this.state.editingRecord,
                  ...item,
                });                
              }
              this.setState({ dataSource: newData });
              message.success("新增公司信息成功");
              
            }).catch((e)=>{
              message.error(e.message);
              
            });
          }else{
            dispatch({
              type: 'outSource/upadateOutSource',
              data: toSaveData,
            }).then(()=>{             
              message.success("保存公司信息成功");
              // this.setState({ dataSource: newData, editingKey: '' });
            }).catch((e)=>{
              message.error(e.message);
            });
          }
        } else {
          newData.push(Row);
          this.setState({ dataSource: newData, editingKey: '' });
        }
      });

    };

    //取消保存
    cancel = (key) => {
      this.setState({ editingKey: '' });
   
      // 如果是新增的规则未保存就放弃编辑，删除该行
      if((key + '').indexOf('new') != -1){
        this.onDelete(key);
      }
    };

    onDelete = (key) => {
      const data = [...this.state.dataSource];
      this.setState({ dataSource: data.filter(item => item.key !== key) });
    }

    //删除
    deleteRecord = (key) => {
      const {dispatch} = this.props;
      const deleteData = {
        id: key,
      }
      dispatch({
        type: 'outSource/deleteOutSource',
        data: deleteData,
      }).then(()=>{
        const data = [...this.state.dataSource];
        this.setState({ dataSource: data.filter(item => item.key !== key),
                        editingKey: '' ,
                      });
        message.success('删除成功');
      }).catch((e)=>{
        message.error(e.message);
      });
    }

    outSourceIsNotNull(key, data){     
      if(!data.outsourceName) {
         message.error("请输入派遣公司");
         return true;
      }
      return false;
      // const index = data.findIndex(item => key === item.key);
      // if (index > -1) {
      //   let item = data[index];      
      //   if(!item.outsourceName){
      //     message.error("请输入派遣公司");
      //     return true;
      //   }
      // }
      // return false;
    }

    groupIdNotNull(key, data){
      // if(!data.groupId) {
      //    message.error("请输入团队");
      //    return true;
      // }
      // return false;
      const index = data.findIndex(item => key === item.key);
      if (index > -1) {
        let item = data[index];
        if(!item.groupId){
          message.error("请输入团队");
          return true;
        }
      }
      return false;
    }

    //加载组件
    render() {
      //合并组件
      const components = {
        body: {
          row: EditableFormRow,
          cell: EditableCell,
        },
      };

      //引入table列和编辑
      const columns = this.columns.map((col) => {
        if (!col.editable) {
          return col;
        }
        return {
          ...col,
          onCell: record => ({
            record,
            inputType: col.dataIndex,
            dataIndex: col.dataIndex,
            title: col.title,
            editing: this.isEditing(record),
          }),
        };
      });

      const {loading,form, outSource} = this.props;
      let { selectedRows, modalVisible, addVisible, dataSource} = this.state;


      const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
          this.setState({
            selectedRows: selectedRows,
          });
        },
        getCheckboxProps: record => ({
          disabled: record.name === 'Disabled User', // Column configuration not to be checked
          name: record.name,
        }),
      };    

      let total = 0;
      if(outSource.data){   //模型 initOutSourceInfo 的参数
        total = outSource.data.total;    
      }

      //定义团队名称Options值
      let groupOptions = [];
      groupOptions = this.props.outSource.groupData.map(group => <Select.Option key={group.id}>{group.groupName}</Select.Option>);

      return (
        <PageHeaderLayout title="公司信息查询">
              <Card bordered={false}>
        <Form onSubmit={this.handleSearch} layout="inline">
          <Row gutter={{ md: 12, lg: 12, xl: 24 }} style={{paddingRight:50}}>
            <Col md={10} sm={10}>
              <FormItem label="团队名称">
                  {form.getFieldDecorator('groupId')
                    (<Select style={{ width: 220 }} >
                      {groupOptions}
                    </Select>)
                  }
              </FormItem>
            </Col>
            <Col md={12} sm={24}>
                <span className={styles.submitButtons}>
                  <Button type="primary" htmlType="submit">
                    搜索
                  </Button>
                  <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                    重置
                  </Button>
                </span>
                
            </Col>
          </Row>
        </Form>
        
              <div className={styles.positionList}>
                <div className={styles.positionListOperator}>
                    <Button icon="plus" type="primary" onClick={this.handleAdd}>
                      新建派遣公司
                    </Button>
                </div>
                <br/>
                <Table
                  components={components}
                  // selectedRows={selectedRows}
                  // rowSelection={rowSelection}
                  loading={loading}
                  dataSource={dataSource}
                  state={dataSource}
                  columns={columns}
                  // rowClassName="editable-row"
                  // onSelectRow={this.handleSelectRows}
                  addVisible={this.addVisible}
                  onChange={this.handleStandardTableChange}
                  scroll={{ x: 2000,y:490 }}
                  pagination={{
                    showSizeChanger: true,
                    showQuickJumper: true,
                    ...{
                    current: this.state.pageNum,
                    pageSize: this.state.pageSize,
                    total: total,
                    }
                  }}
                  />
              </div>
            </Card>
        </PageHeaderLayout>
      );
    }
}



