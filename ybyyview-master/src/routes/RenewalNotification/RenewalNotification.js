import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card, Form, Table, Row, Col, Input, Button, message,
} from 'snk-web';
import moment from 'moment';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

const columns = [
  {
    title: '保单号',
    dataIndex: 'cPlyNo',
    align: 'center',
  },
  {
    title: '投保人姓名',
    dataIndex: 'cAppNme',
    align: 'center',
  },
  {
    title: '投保人证件号码',
    dataIndex: 'cCertfCde',
    align: 'center',
  },
  {
    title: '业务渠道',
    dataIndex: 'extEnterpCode',
    align: 'center',
  },
  {
    title: '产品名称',
    dataIndex: 'prodName',
    align: 'center',
  },
  {
    title: '续保次数',
    dataIndex: 'renewalTime',
    align: 'center',
  },
  {
    title: '寄送印刷厂日期',
    dataIndex: 'sendDate',
    align: 'center',
    render: text => <span>{ text ? moment(text).format('YYYY-MM-DD hh:mm:ss') : '' }</span>,
  },
  {
    title: '寄送地址',
    dataIndex: 'insrncAddress',
    align: 'center',
  }
]

@connect(({ searchlist, loading}) => ({
  searchlist,
  loading: loading.effects,
}))
@Form.create()
export default class RenewalNotification extends PureComponent {
  state = {
    RenewalNotiDate: [],
    pageNum: 1,
    pageSize: 10,
    total: 0,
  }

  handleSearch = () => {
    this.queryData(1, 10);
  }

  queryData = (pageNum, pageSize) => {
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(!fieldsValue.cPlyNo&&!fieldsValue.cCertfCde) {
        message.warning('证件号、保单号必填一项', 5);
        return;
      }
      this.props.dispatch({
        type: 'searchlist/noticeSendQuery',
        payload: {
          ...fieldsValue,
          pageNum,
          pageSize,
        },
      }).then(result => {
        const { code, data: { list, total }, message: msg } = result;
        if (code === 0) {
          this.setState({
            total,
            RenewalNotiDate: list,
          });
          message.success(msg || '查询成功');
        } else {
          message.error(msg || '查询失败');
        }
      });
    });
  }

  // 重置表单
  handleFormReset = () => {
    this.props.form.resetFields();
  }

  // 列表状态发生改变
  handleTableChange =(pagination) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.queryData(pagination.current,pagination.pageSize);
    });
  }

  render() {
    const { form: { getFieldDecorator } } = this.props;
    const { total, pageNum, pageSize } = this.state;
    return (
      <PageHeaderLayout>
        <Card bordered={false}>
          <Form className='custom-form-wrapper' layout="inline" >
            <Row gutter={{ md: 4, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <Form.Item label='投保人证件号'>
                  {
                    getFieldDecorator('cCertfCde', {
                      // rules: [{ required: true }],
                    })(<Input placeholder="请输入" />)
                  }
                </Form.Item>
              </Col>
              <Col md={12} sm={24}>
                <Form.Item label='保单号'>
                  {
                    getFieldDecorator('cPlyNo', {
                      // rules: [{ required: true, max: 30, message: '请输入正确的保单号' }],
                    })
                    (<Input placeholder="请输入" />)
                  }
                </Form.Item>
              </Col>
              <Col>
              </Col>
            </Row>
            <div style={{ padding:'12px',overflow: 'hidden',textAlign:'center' }}>
              <span style={{ marginBottom: 24 }}>
                <Button  type="primary"  onClick={this.handleSearch}>
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </span>
            </div>
          </Form>
          <Table
            columns={columns}
            dataSource={this.state.RenewalNotiDate}
            loading={this.props.loading['searchlist/noticeSendQuery']}
            rowKey={record => record.cPlyNo}
            scroll={{x: 1400}}
            bordered
            size="small"
            pagination={{
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
              current: pageNum,
              pageSize: pageSize,
              total:total,
              pageSizeOptions:['10','20','30','40','50'],
            }}
            onChange={this.handleTableChange}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}