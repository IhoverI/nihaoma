import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form,
  Row,
  Col,
  Input,
  DatePicker,
  Select,
  Button,
  Card,
  InputNumber,
  Radio,
  Icon,
  Tooltip,
  message,
  Collapse,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './JobManageForm.less';
import { getRegularCheck, getSelectChild, getHost, getUrlParam } from '../../utils/utils';


const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;


@connect(({ quartzmanage,loading }) => ({
  quartzmanage,
  loading: loading.models.quartzmanage,
}))
@Form.create()
export default class QuartzAssign extends PureComponent {
  constructor(props){
    super(props);
    this.state={
      record:props.record
    };
  }
  componentDidMount(){
    this.initForm(this.props,false);
    console.log('props', this.props);
  }
  initForm(props,isWillReceive){
    this.props.form.resetFields();
    const record = props.record;
    if(isWillReceive){
      this.setState({
        record
      });
    }
    if(record){
      // 赋值操作
      this.props.form.setFieldsValue({
        jobId:record.jobId,
        dependency:record.dependency,
        execPlan:record.execPlan,
        isValid:record.isValid,
        jobCode:record.jobCode,
        jobConf:record.jobConf,
        jobDesc:record.jobDesc,
        jobName:record.jobName,
        url:record.url,
        urlType:record.urlType,
        workdayOnly:record.workdayOnly,
        flag:record.flag,
        mailCc:record.mailCc,
        mailFlag:record.mailFlag,
        mailTemplate:record.mailTemplate,
        mailTo:record.mailTo,
        phone:record.phone,
        smsFlag:record.smsFlag,
        smsTemplate:record.smsTemplate,
        lockFlag:record.lockFlag,
        sysCode: record.sysCode,
      });
    }
    
  }
  componentWillReceiveProps(nextProps){
    if(JSON.stringify(nextProps.record)!==JSON.stringify(this.state.record)){
      this.initForm(nextProps,true);
    }
  }
  handleSubmit = e => {
    const { getFieldValue } = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
      // console.log('value', values);
      if (!err) {
        values = this.state.record?this.prepareModifyValue(values):this.prepareAddValue(values);
        // console.log('处理的value', values);
        this.props.dispatch({
          type: this.state.record?'quartzmanage/UpdateJob':'quartzmanage/AddJob',
          payload: values,
        }).then(()=>{
          this.props.onAddJobSuccess({
            isModify:!!this.state.record,
            record:this.state.record,
          });
        }).catch(e=>{
          message.error(e.message||'网络异常请稍后再试');
        });
      }
    });
  };

  prepareModifyValue(values){
    var Re = {
      whereMap:{
        jobId:"",
      },
      lockFlag:"",
      updateMap:{
        dependency:"",
        execPlan:"",
        isValid:"",
        sysCode:"",
        jobConf:"",
        jobDesc:"",
        jobName:"",
        url:"",
        urlType:"",
        workdayOnly:""
      },
      jobNoticeConf:{
        flag:"",
        mailCc:"",
        mailFlag:"",
        mailTemplate:"",
        mailTo:"",
        phone:"",
        smsFlag:"",
        smsTemplate:""
      }
    };
    for(var key in Re){
      for(var innerKey in Re[key]){
        Re[key][innerKey] = values[innerKey];
      }
    }
    Re.lockFlag = values.lockFlag;
    return Re;
  }

  prepareAddValue(values){
    // 新增的时候 改造成后台需要的格式
    var Re = {
      job:{
        dependency:"",
        execPlan:"",
        isValid:"",
        jobCode:"",
        jobConf:"",
        jobDesc:"",
        jobName:"",
        url:"",
        urlType:"",
        workdayOnly:"",
        sysCode: "",
      },
      jobNoticeConf:{
        flag:"",
        mailCc:"",
        mailFlag:"",
        mailTemplate:"",
        mailTo:"",
        phone:"",
        smsFlag:"",
        smsTemplate:""
      }
    };
    for(var key in Re){
      for(var innerKey in Re[key]){
        Re[key][innerKey] = values[innerKey];
      }
    }
    return Re;
  }

  onCancel = e => {
    this.props.onCancel();
  };

  render() {
    const { loading,form } = this.props;
    const { sysCodeData } = this.props;
    const { getFieldDecorator, getFieldValue } = form;
    const Panel = Collapse.Panel;
    const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 7 },
        md: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 12 },
        md: { span: 16 },
      },
    };
    const formItemLayout1 = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 7 },
        md: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 12 },
        md: { span: 20 },
      },
    };
    const submitFormLayout = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 10, offset: 7 },
      },
    };

    return (
        <Form className={styles.form} style={{ marginTop: 8 }}>
            <div>JOB基本配置：</div>
              <Row>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="JOB编号">
                    {getFieldDecorator('jobId')(<Input disabled placeholder="" />)}
                  </FormItem>
                </Col>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="JOB名称">
                     {getFieldDecorator('jobName',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="执行计划">
                     {getFieldDecorator('execPlan')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="URL">
                    {getFieldDecorator('url',{
                            rules: [{ required: true, message: '必填!' }],
                        })(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="URL类型">
                    {getFieldDecorator('urlType',{
                      initialValue:'POST'
                    })(
                      <Select placeholder="请选择" style={{ width: '100%' }}>
                        <Option value="POST">POST</Option>
                        <Option value="GET">GET</Option>
                      </Select>)}
                  </FormItem>
                </Col>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="任务配置项">
                    {getFieldDecorator('jobConf')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="系统模块">
                    {getFieldDecorator('sysCode', {
                      initialValue: sysCodeData.length > 0 ? sysCodeData[0].code : ''
                    })(
                      <Select placeholder="请选择" style={{ width: '100%' }}>
                        {
                          sysCodeData.map((item) => {
                            return <Option value={item.code} key={item.id}>{item.codeName}</Option>
                          })
                        }
                      </Select>)}
                  </FormItem>
                </Col>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="依赖关系">
                    {getFieldDecorator('dependency')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={24} sm={24}>
                  <FormItem {...formItemLayout1} label="任务描述">
                    {getFieldDecorator('jobDesc')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={12} sm={24}>
                    <FormItem {...formItemLayout} label="是否工作日">
                      {getFieldDecorator('workdayOnly',{
                          initialValue: "1",
                        })(
                        <Select placeholder="请选择" style={{ width: '100%' }}>
                          <Option value="1">是</Option>
                          <Option value="0">否</Option>
                        </Select>)}
                    </FormItem>
                  </Col>
                  <Col md={12} sm={24}>
                    <FormItem {...formItemLayout} label="是否有效">
                      {getFieldDecorator('isValid',{
                          initialValue: "1",
                        })(
                        <Select placeholder="请选择" style={{ width: '100%' }}>
                          <Option value="1">有效</Option>
                          <Option value="0">无效</Option>
                        </Select>)}
                    </FormItem>
                  </Col>
              </Row>
              {
                this.state.record?( <Row>
                    <Col md={12} sm={24}>
                      <FormItem {...formItemLayout} label="锁标识">
                        {getFieldDecorator('lockFlag',{
                            initialValue: "0",
                          })(
                          <Select placeholder="请选择" style={{ width: '100%' }}>
                            <Option value="0">未锁</Option>
                            <Option value="1">已锁</Option>
                          </Select>)}
                      </FormItem>
                    </Col>
                </Row>):null
              }
            <div>执行结果通知：</div>
              <Row>
                <Col md={24} sm={24}>
                  <FormItem {...formItemLayout1} label="邮件接收人">
                    {getFieldDecorator('mailTo')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={24} sm={24}>
                  <FormItem {...formItemLayout1} label="邮件抄送人">
                    {getFieldDecorator('mailCc')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={24} sm={24}>
                  <FormItem {...formItemLayout1} label="邮件模板">
                     {getFieldDecorator('mailTemplate')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={24} sm={24}>
                  <FormItem {...formItemLayout1} label="是否发送邮件">
                   {getFieldDecorator('mailFlag',{
                          initialValue: "1",
                        })(
                        <Select placeholder="请选择" style={{ width: '100%' }}>
                          <Option value="1">发送</Option>
                          <Option value="0">不发送</Option>
                        </Select>)}
                  </FormItem>
                </Col>
              </Row>
             
              <Row>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="手机号码">
                      {getFieldDecorator('phone')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="短信模板">
                     {getFieldDecorator('smsTemplate')(<Input placeholder="请输入" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="是否发送短信">
                     {getFieldDecorator('smsFlag',{
                          initialValue: "1",
                        })(
                        <Select placeholder="请选择" style={{ width: '100%' }}>
                          <Option value="1">发送</Option>
                          <Option value="0">不发送</Option>
                        </Select>)}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col md={12} sm={24}>
                  <FormItem {...formItemLayout} label="是否有效">
                      {getFieldDecorator('flag',{
                          initialValue: "1",
                        })(
                        <Select placeholder="请选择" style={{ width: '100%' }}>
                          <Option value="1">有效</Option>
                          <Option value="0">无效</Option>
                        </Select>)}
                  </FormItem>
                </Col>
              </Row>
            <div style={{ margin: 12 }}>
              <Button loading={loading}  type="primary" onClick={this.handleSubmit}>
                保存
              </Button>
              <Button style={{ margin: 12 }} onClick={this.onCancel}>
                取消
              </Button>
            </div>
        </Form>
    );
  }
}
