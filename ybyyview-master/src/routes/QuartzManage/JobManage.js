import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  Table,
  DatePicker,
  Popconfirm,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import JobManageForm from './JobManageForm';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './JobManage.less';

const FormItem = Form.Item;
const { Option } = Select;


@connect(({ quartzmanage,loading }) => ({
  quartzmanage,
  loading: loading.models.quartzmanage,
}))
@Form.create()
export default class JobManage extends PureComponent {
  state = {
    modalVisible: false,
    formValues: {},
    pageSize:10,
    pageNum:1,
    record:null,
  };
  
  constructor(props){
    super(props);
    props.dispatch({
      type: 'quartzmanage/reset',
    });
    this.searchSysCode();
  }


  handleJobManageTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;
    this.setState({
      pageSize: pagination.pageSize,
      pageNum: pagination.current,
    },()=>{
      this.searchData();
    });
  };

  searchSysCode = () => {
    this.props.dispatch({
      type: 'quartzmanage/SearchSysCode',
      payload: {
        codeType: 'sysCode'
      }
    }).then(() => {
      this.searchData();
    })
  }

  getColumns(){
    const columns = [
      {
        title: '任务ID',
        dataIndex: 'jobId',
        align: 'center',
      },
      {
        title: '系统编码',
        dataIndex: 'sysCode',
        align: 'center',
      },
      {
        title: '任务名称',
        dataIndex: 'jobName',
        align: 'center',
      },
      {
        title: '执行计划',
        dataIndex: 'execPlan',
        align: 'center',
      },
      {
        title: '任务描述',
        dataIndex: 'jobDesc',
        align: 'center',
      },
      {
        title: '是否被锁',
        dataIndex: 'lockFlag',
        align: 'center',
        render:(val)=>{
          return val==="1"?"是":"否";
        }
      },
      {
        title: '是否有效',
        dataIndex: 'isValid',
        align: 'center',
        render:(val)=>{
          if(val==="0"){
            return <span className={styles.novalidate}><span className={styles.novalidatemark}></span>无效</span>
          }
          return <span className={styles.validate}><span className={styles.validatemark}></span>有效</span>
        }
      },
    
      {
        title: '创建时间',
        dataIndex: 'createDate',
        align: 'center',
      },
      {
        title: '操作',
        dataIndex:'action',
        align: 'center',
        render: (value,record) => (
          <Fragment>
            <a onClick={this.modifyRow.bind(this,record)}>修改</a>
            {record.isValid==="0"?null:<Divider type="vertical" />}
            {record.isValid==="0"?null:(<Popconfirm title="确定删除该项吗？" onConfirm={this.deleteRow.bind(this,record)} okText="删除" cancelText="取消">
              <a href="#">删除</a>
            </Popconfirm>)}
          </Fragment>
        ),
      },
    ];
    return columns;
  }

  modifyRow(record){
    this.setState({
      modalVisible:true,
      record:record
    });
  }

  deleteRow(record){
    this.props.dispatch({
      type: 'quartzmanage/DeleteJob',
      payload: {jobId:record.jobId},
    }).then(()=>{
      message.success('删除成功');
    }).catch(e=>{
      message.error(e.message||'网络异常请稍后再试');
    });
  }
  searchData(){
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if(fieldsValue["isValid"]==="2"){
        delete fieldsValue["isValid"];
      }
      dispatch({
        type: 'quartzmanage/SearchJobList',
        payload: fieldsValue,
      }).then(()=>{
      }).catch(e=>{
        message.error(e.message||'网络异常请稍后再试');
      });
    });
  }

  handleSearch = e => {
    e.preventDefault();
    this.setState({pageNum:1},()=>{
      this.searchData();
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  onAddJobSuccess(params){
    this.handleModalVisible(false);
    message.success(params.isModify?"更新成功！":"新增成功！");
    if(params.isModify){
      //重新查询更新UI record
      this.props.dispatch({
        type: 'quartzmanage/UpdateRow',
        payload: {jobId:params.record.jobId},
      }).then(()=>{

      }).catch(()=>{
        this.searchData();
      });
    }
  }


  render() {
    const { loading,quartzmanage } = this.props;
    const JobListData = quartzmanage.JobListData;
    const { modalVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const { SysCodeData } = this.props.quartzmanage;
    let total = JobListData.total || 0;
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
            <div>
               <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch} layout="inline">
                  <Row >
                    <Col md={6} sm={24}>
                      <FormItem label="任务名称">
                        {getFieldDecorator('jobName')(<Input placeholder="模糊搜索" />)}
                      </FormItem>
                    </Col>
                    <Col md={4} sm={24}>
                      <FormItem label="是否有效">
                        {getFieldDecorator('isValid',{
                            initialValue: "2",
                          })(
                          <Select placeholder="请选择" style={{ width: '100px' }}>
                            <Option value="2">全部</Option>
                            <Option value="1">有效</Option>
                            <Option value="0">无效</Option>
                          </Select>)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24}>
                      <FormItem label="系统模块">
                        {getFieldDecorator('sysCode', {
                          initialValue: ''
                        })(
                          <Select allowClear placeholder="请选择" style={{ width: '100%' }}>
                            {
                              SysCodeData.map((item) => {
                                return <Option value={item.code} key={item.id}>{item.codeName}</Option>
                              })
                            }
                          </Select>)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24}>
                      <Button style={{margin:'0 30px'}} type="primary" htmlType="submit">
                       查询
                     </Button>
                    </Col>
                  </Row>
                </Form>
            </div>
            <Table
              loading={loading}
              bordered
              dataSource={JobListData.list}
              size="small"
              title={() => {
                 return  <Button icon="plus" type="primary" onClick={() =>{
                          this.setState({
                            record:null,
                            modalVisible:true
                          });
                        }}>
                          新建任务
                        </Button>
                ; 
              }}
              columns={this.getColumns()}
              size="small"
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                ...{
                  current:this.state.pageNum,
                  pageSize:this.state.pageSize,
                  showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                  total:total,
                  pageSizeOptions:['10','20','30','40','50'],
                },
              }}
              onChange={this.handleJobManageTableChange.bind(this)}
            />
          </div>
        </Card>
        
        <Modal
          title={this.state.record?"修改任务":"新建任务"}
          width={700}
          footer={null}
          style={{ top: 20 }}
          onCancel={() => this.handleModalVisible(false)}
          visible={modalVisible}
        >
          <JobManageForm
            record={this.state.record}
            sysCodeData={SysCodeData}
            onAddJobSuccess={this.onAddJobSuccess.bind(this)}
            onCancel={() => this.handleModalVisible(false)}/>
        </Modal>
      </PageHeaderLayout>
    );
  }
}
