import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Table,
  Modal,
  message,
  Badge,
  Divider,
} from 'snk-web';
import QuartzListForm from './QuartzListForm';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { bubbleSort } from '../../utils/utils'

import styles from './QuartzList.less';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;

const resultStatusMap = ['success', 'default'];
const resultStatus = ['成功', '失败'];
const nowStatusMap = ['success', 'default'];
const nowStatus = ['已执行', '已取消'];




@connect(({ quartzmanage,loading }) => ({
  quartzmanage,
  loading: loading.models.quartzmanage,
}))
@Form.create()
export default class QuartzList extends PureComponent {
  state = {
    modalVisible: false,
    formValues: {},
    pageSize:50,
    pageNum:1,
    modifyPlanTime:null,
    editingBufferId:null
  };

  constructor(props){
    super(props);
    props.dispatch({
      type: 'quartzmanage/reset',
    });
  }

  componentDidMount =()=>{
    this.searchSysCode();
  }

  searchSysCode = () => {
    this.props.dispatch({
      type: 'quartzmanage/SearchSysCode',
      payload: {
        codeType: 'sysCode'
      }
    }).then(() => {
      this.searchData();
    })
  }

  handleQuartzListTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;
    this.setState({
      pageSize: pagination.pageSize,
      pageNum: pagination.current,
    },()=>{
      this.searchData();
    });
  };

  handleSearch = e => {
    e.preventDefault();
    this.setState({pageNum:1},()=>{
      this.searchData();
    });
  };

  searchData(){
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue = this.perpareValues(fieldsValue);
      dispatch({
        type: 'quartzmanage/SearchJobBufferList',
        payload: fieldsValue,
      }).then(()=>{
      }).catch(e=>{
        message.error('网络异常请稍后再试');
      });
      
    });
  }

  perpareValues(values){
    if(!values.planTime||values.planTime.length===0){
      values.planTimeFrom = null;
      values.planTimeTo = null;
    }else{
      values.planTimeFrom = values.planTime[0].format("YYYY-MM-DD");
      values.planTimeTo = values.planTime[1].format("YYYY-MM-DD");
    }
    delete values.planTime;
    values.pageNum = this.state.pageNum;
    values.pageSize = this.state.pageSize;
    if(values.runFlags){
      values.runFlags = [values.runFlags];
    }
    for(var key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = null;
      }
    }
    return values;
  }

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  onAddJobSuccess(){
    this.handleModalVisible(false);
    message.success("新增成功！");
  }

  getColumns(){
    const columns = [
      {
        title: '任务ID',
        dataIndex: 'jobId',
        align: 'center',
      },
      {
        title: '任务名称',
        dataIndex: 'jobName',
        align: 'center',
      },
      {
        title: '计划开始时间',
        dataIndex: 'planTime',
        align: 'center',
        render: (val,record) => {
          if(record.bufferId === this.state.editingBufferId){
            return (<DatePicker
              onChange={this.modifyPlanTimeChange.bind(this)}
              format="YYYY-MM-DD HH:mm:ss"
              showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
            />)
          }
          return  <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
        },
      },
      {
        title: '开始时间',
        dataIndex: 'beginDate',
        align: 'center',
        render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
      },
      {
        title: '结束时间',
        dataIndex: 'endDate',
        align: 'center',
        render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
      },
      {
        title: '执行时间',
        dataIndex: 'runTime',
        align: 'center',
      },
      {
        title: '执行结果',
        dataIndex: 'status',
        align: 'center',
        render: val => {
          let re = '';
          if(val==="1"){
            re ='成功';
          }else if(val==='0'){
            re ='未知';
          }else{
            re ='失败';
          }
          return <span>{re}</span>;
        },
      },
      {
        title: '执行结果描述',
        dataIndex: 'resultDesc',
        align: 'center',
        width:200,
        render:(value) => {
          return <div style={{height:50,overflow:'auto'}}>{value}</div>
        }
      },
      {
        title: '状态',
        dataIndex: 'runFlag',
        align: 'center',
        render: val => {
          let re = '';
          if(val==="1"){
            re ='已执行';
          }else if(val==='0'){
            re ='未执行';
          }else if(val==='2'){
            re ='已取消';
          }
          return <span>{re}</span>;
        },
      },
      {
        title: '操作',
        dataIndex: 'action',
        align: 'center',
        render: (val,record) => {
          if(record.bufferId === this.state.editingBufferId){
            return (<Fragment>
              <a onClick={this.SavePlanTime.bind(this,record)}>保存</a>
              <Divider type="vertical" />
              <a onClick={()=>{
                 this.resetModify();
                }}>取消</a>
            </Fragment>);
          }
          if(record.runFlag==='0'){
            //未执行
            return (
              <Fragment>
                <a onClick={()=>{
                  this.setState({
                    editingBufferId:record.bufferId,
                    modifyPlanTime:null,
                  });
                }}>修改时间</a>
                <Divider type="vertical" />
                <a onClick={this.stop.bind(this,record)} >停止</a>
              </Fragment>
            );
          }else if(record.status!=="0"&&record.status!=="1"&&record.runFlag==="1"){
            // 失败并且已执行
            return (
                <a onClick={this.restart.bind(this,record)}>重新执行</a>
            );
          }
          return null;
        },
      },
    ];
    return columns;
  }

  resetModify(){
    this.setState({
      editingBufferId:null,
      modifyPlanTime:null
    });
  }
  SavePlanTime(record){
    if(!this.state.modifyPlanTime){
      message.info("请输入时间");
      return;
    }
    this.updateRow({
      planTime:moment(this.state.modifyPlanTime).format('YYYY-MM-DD HH:mm:ss')
    },record,()=>{
      this.resetModify();
    });
  }
  modifyPlanTimeChange(val){
    this.setState({
      modifyPlanTime:val
    });
  }

  stop(record){
    this.updateRow({
      runFlag:"2"
    },record);
  }
  restart(record){
    this.updateRow({
      runFlag:"0"
    },record);
  }

  updateRow(updateMap,record,success,error){
    const { dispatch, form } = this.props;
    dispatch({
      type: 'quartzmanage/UpdateJobBuffer',
      payload: {
        updateMap:updateMap,
        whereMap:{
          bufferId:record.bufferId
        }
      },
    }).then(()=>{
      message.success("更新成功！");
      success&&success();
    }).catch(e=>{
      error&&error();
      message.error(e.message||'网络异常请稍后再试');
    });
  }

  render() {
    const { loading,quartzmanage } = this.props;
    const { selectedRows, modalVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const {JobBufferListData, SysCodeData} = quartzmanage;
    /**
     * 冒泡排序显示最新时间数据
     * 后期表格添加切换按钮排序
     */
    let dataSource = JobBufferListData.list ||[];
    dataSource = bubbleSort(dataSource,"planTime");
    
    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
               <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch} layout="inline">
                  <Row>
                    <Col md={5} sm={24}>
                      <FormItem label="任务名称">
                          {getFieldDecorator('jobName')(<Input placeholder="模糊搜索" />)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24}>
                      <FormItem label="执行状态">
                       {getFieldDecorator('status',  {
                          initialValue: ''
                        })(<Select allowClear placeholder="请选择" style={{ width: '200px' }}>
                          <Option value="0">未执行</Option>
                          <Option value="1">成功</Option>
                          <Option value="2">执行失败</Option>
                          <Option value="3">超时</Option>
                          <Option value="4">前置任务未执行</Option>
                      </Select> )}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24}>
                      <FormItem label="系统模块">
                        {getFieldDecorator('sysCode', {
                          initialValue: ''
                        })(
                          <Select allowClear placeholder="请选择" style={{ width: '100%' }}>
                            {
                              SysCodeData.map((item) => {
                                return <Option value={item.code} key={item.id}>{item.codeName}</Option>
                              })
                            }
                          </Select>)}
                      </FormItem>
                    </Col>
                    <Col md={6} sm={24} offset={1}>
                      <FormItem label="计划开始时间">
                         {getFieldDecorator('planTime',{
                           initialValue:[moment(new Date(), 'YYYY-MM-DD'), moment(new Date(), 'YYYY-MM-DD')]
                         })(<RangePicker />)}
                      </FormItem>
                    </Col>
                  </Row>
                  <div style={{textAlign:'center',margin:'10px 0'}}>
                    <Button type="primary" htmlType="submit">
                        查询
                      </Button>
                  </div>
                </Form>
            <Table
              loading={loading}
              dataSource={dataSource}
              bordered
              rowKey={(record,index) => index}
              size="small"
               title={() => {
                 return <Button icon="plus" 
                        type="primary" 
                        onClick={() => this.handleModalVisible(true)}>
                          新建任务
                      </Button>
                ; 
              }}
              pagination={{
                showSizeChanger: true,
                showQuickJumper: true,
                ...{
                  current:this.state.pageNum,
                  pageSize:this.state.pageSize,
                  showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                  total:JobBufferListData.total,
                  pageSizeOptions:['10','20','30','40','50','100'],
                },
              }}
              columns={this.getColumns()}
              onChange={this.handleQuartzListTableChange}
            />
          </div>
        </Card>
        <Modal
          title="新建任务"
          width={450}
          footer={null}
          style={{ top: 20 }}
          onCancel={() => this.handleModalVisible(false)}
          visible={modalVisible}
        >
          <QuartzListForm 
            onAddJobSuccess={this.onAddJobSuccess.bind(this)}
            onCancel={() => this.handleModalVisible(false)}/>
        </Modal>
      </PageHeaderLayout>
    );
  }
}
