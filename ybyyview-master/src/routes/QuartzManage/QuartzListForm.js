import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form,
  Row,
  Col,
  Input,
  DatePicker,
  Select,
  Button,
  Card,
  InputNumber,
  Radio,
  Icon,
  Tooltip,
  message,
  Collapse,
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { getRegularCheck, getSelectChild, getHost, getUrlParam } from '../../utils/utils';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const serverHost = SERVERLOCAL;


@connect(({ quartzmanage,loading }) => ({
  quartzmanage,
  loading: loading.models.quartzmanage,
}))
@Form.create()
export default class QuartzAssign extends PureComponent {

  componentDidMount(){
    this.searchJobListCondition();
  }

  handleSubmit = e => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values = this.prepareValue(values);
        this.props.dispatch({
          type: 'quartzmanage/AddJobBuffer',
          payload: values,
        }).then(()=>{
          this.props.onAddJobSuccess();
        }).catch(e=>{
          message.error(e.message||'网络异常请稍后再试');
        });
      }
    });
  };


  prepareValue(values){
    values.jobId = values.job.key;
    values.jobName = values.job.label;
    delete values.job;
    return values;
  }

  searchJobListCondition(){
    this.props.dispatch({
      type: 'quartzmanage/SearchJobList',
      payload: {
        pageSize:1000,
        pageNum:1,
        isValid:'1'
      },
    }).then(()=>{

    }).catch(e=>{
      message.error('网络异常请稍后再试');
    });
  }

  onCancel = e => {
    this.props.onCancel();
  };

  getOption(data){
    if(!data){
      return null;
    }
    const options = [];
    for(let i=0,j=data.length;i<j;i++){
      const item = data[i];
      options.push(<Option key={i} value={item.jobId}>{item.jobName}</Option>);
    }
    return options;
  }

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { loading,quartzmanage } = this.props;
    const JobListData = quartzmanage.JobListData;
    const formItemLayout = {
      labelCol: {
        xs: { span: 12 },
        sm: { span: 7 },
        md: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 12 },
        sm: { span: 12 },
        md: { span: 16 },
      },
    };
    return (
        <Form style={{ marginTop: 8 }}>
            <Row>
                <Col md={24} sm={24}>
                  <FormItem {...formItemLayout} label="任务名称">
                      {
                        getFieldDecorator('job', 
                          {
                              rules: [{ required: true, message: '必填！' }],
                          }
                        )(
                        <Select
                           labelInValue
                           showSearch
                           optionFilterProp="children"
                           placeholder="请选择"
                        >
                          {this.getOption(JobListData.list)}
                        </Select>
                        )
                      }
                  </FormItem>
                </Col>
            </Row>
            <Row>
                <Col md={24} sm={24}>
                <FormItem {...formItemLayout} label="计划开始时间">
                    {getFieldDecorator('planTime', {
                        rules: [{ required: true, message: '必填！' }],
                    })(<DatePicker showTime format="YYYY-MM-DD HH:mm:ss"/>)}
                    </FormItem>
                </Col>
            </Row>
            <Row>
                <Col md={24} sm={24}>
                    <FormItem {...formItemLayout} label="任务入参">
                    {getFieldDecorator('param', {})(<Input placeholder="根据实际需要选填" />)}
                    </FormItem>
                </Col>
            </Row>
            <div style={{ margin: 12 }}>
              <Button loading={loading} type="primary" onClick={this.handleSubmit}>
                保存
              </Button>
              <Button style={{ margin: 12 }} onClick={this.onCancel}>
                取消
              </Button>
            </div>
        </Form>
    );
  }
}
