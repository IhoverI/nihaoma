import React, { PureComponent, Fragment, Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Table,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Popconfirm,
  Tabs,
  Upload
} from 'snk-web';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './index.less';
import {getChannelNameByValue,getProductNameByValue,getProductNameByExtEnterpCodeAndProdNo} from '../../utils/utils'
import SearchForm from './SearchForm';
import SearchExactForm from './SearchExactForm';


const { RangePicker } = DatePicker;

const FormItem = Form.Item;
const { Option } = Select;


function handleChange(value) {
}


const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');


@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))

export default class ExpressRoute extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    pageNum: 1,
    pageSize: 10,
    formatVal: {},
  };

  handleFormReset = (form) => {
    const { dispatch } = this.props;
    this.setState({pageNum:1,pageSize:10});
    form.resetFields();
  };

  formaUnit = (val,code) => {
    let text = '';
    if (code === 'expressCode') {
      switch (val) {
        case 'E1002': text = '圆通'; break;
        case 'E1003': text = '顺风'; break;
        case 'E1004': text = '宅急送'; break;
        case 'E1005': text = '中通'; break;
        case 'E1006': text = '银雁'; break;
        case 'E1007': text = '天天'; break;
        case 'E1008': text = 'EMS'; break;
        case 'E1009': text = '满逸'; break;
        case 'E1010': text = '德邦'; break;
        default: break;
      }
    } else if (code === 'paperReceiptFlag') {
      switch (val) {
        case '1': text = '已入库'; break;
        case '2': text = '待入库'; break;
        case '3': text = '遗失'; break;
        default: break;
      }
    } else if (code === 'postStatus'){
      switch (val) {
        case '0': text = '在途'; break;
        case '1': text = '签收'; break;
        case '3': text = '问题件'; break;
        case '5': text = '初始化'; break;
        default: break;
      }
    } else if (code === 'expressType') {
      switch (val) {
        case '1': text = '寄送出去'; break;
        case '2': text = '回寄快递'; break;
        case '3': text = '补寄寄送出去'; break;
        case '4': text = '补寄回寄快递'; break;
        default: break;
      }
    } else if (code === 'orderStatus') {
        if (val === '0') {
          text = '签收'
        } else if (val === '1') {
          text = '中转'
        } else if (val === '2') {
          text = '入库'
        } else if (val === '3') {
          text = '派送中(在途)'
        } else if (val === '4') {
          text = '未发'
        } else if (val === '5') {
          text = '客户拒签(拒收)'
        } else if (val === '6') {
          text = '联系不上(无人接收)'
        } else if (val === '7') {
          text = 'EMS物流中'
        } else if (val === '8') {
          text = '问题件'
        } else {
          text = val;
        }
    }
    return text;
  }
  getColumns(){
    var _this = this;
    return [
      {
        title: '保单号',
        dataIndex: 'cPlyNo',
        align: 'center',
      },
      {
        title: '投保人姓名', // ----
        dataIndex: 'postName',
        align: 'center',
      },
       {
        title: '被保人姓名',
        dataIndex: 'cInsrntCnm',
        align: 'center',
      },
      {
        title: '渠道', // ----
        dataIndex: 'extEnterpCode',
        align: 'center',
        render:(val)=>{
          return getChannelNameByValue(val);
        },
      },
      {
        title: '产品', // ----
        dataIndex: 'prodNo',
        align: 'center',
        render:(text, record)=>{
          return getProductNameByExtEnterpCodeAndProdNo(record.extEnterpCode, text);
        },
      },
      {
        title: '寄送类型', // ----
        dataIndex: 'expressType',
        align: 'center',
        render: (text) => {
          return (<span>{this.formaUnit(text,'expressType')}</span>)
        }
      },
      {
        title: '运单号',
        dataIndex: 'postNo',
        align: 'center',
      },
      {
        title: '快递公司名称',
        dataIndex: 'expressCode',
        align: 'center',
        render: (text) => {
          return (<span>{this.formaUnit(text,'expressCode')}</span>)
        }
      },
      {
        title: '快递状态',
        dataIndex: 'postStatus',
        align: 'center',
          render: (text) => {
            return (<span>{this.formaUnit(text,'postStatus')}</span>)
          }
      },
      {
        title: '最新快递路由信息', // ---
        dataIndex: 'orderStatus',
        align: 'center',
        render: (text) => {
            return (<span>{this.formaUnit(text,'orderStatus')}</span>)
          }
      },
      {
        title: '快递签收时间', // ---
        dataIndex: 'contractTime',
        align: 'center',
      },
      {
        title: '收件人姓名', // ---
        dataIndex: 'postName1',
        align: 'center',
        render: (text, record, index) => {
            return (<span>{record.postName}</span>)
          }
      },
      {
        title: '快递地址',
        dataIndex: 'postAddress',
        align: 'center',
      },
      {
        title: '联系电话', // ---
        dataIndex: 'cMobileNo',
        align: 'center',
      },
      {
        title: '快递下单时间', // ---
        dataIndex: 'expressPlaceOrderDate',
        align: 'center',
      },
      {
        title: '纸质回执状态',
        dataIndex: 'paperReceiptFlag',
        align: 'center',
          render: (text) => {
            return (<span>{this.formaUnit(text,'paperReceiptFlag')}</span>)
          }
      },
      {
        title: '纸质回执入库日期',
        dataIndex: 'scanCodeDate',
        align: 'center',
      },
      {
        title: '保单状态',
        dataIndex: 'flag',
        align: 'center',
      },
      {
          title: '备注',
          dataIndex: 'remark',
          align: 'center',
      },
    ];
  }

  componentWillMount(){
      Object.keys(this.props.rule).forEach(key => delete this.props.rule[key]);
  }

  handleSearch = (e,form) => {
    e.preventDefault();
    const { dispatch } = this.props;
    this.form = form;
    this.setState({
      pageNum: 1,
      pageSize: 10,
    });
    this.searchData(form,1,10);
  };

  handleExactSearch = (e,form) => {
    e.preventDefault();
    this.form = form;
    this.setState({pageNum:1,searchType:'exact'},()=>{
      this.searchData(form,1,10);
    });
  }

    handleTableChange = (pagination, filters, sorter) => {
    this.setState({
      pageNum: pagination.current,
      pageSize: pagination.pageSize,
    },()=>{
      this.searchData(this.form,pagination.current,pagination.pageSize);
    });
  }

  searchData =(form,pageNum,pageSize) => {
    const { dispatch } = this.props;
    form.validateFields((err, fieldsValue) => {
      let expressTypes;
      if (fieldsValue.expressTypes === '新单') {
        expressTypes = ['1','2'];
      } else if (fieldsValue.expressTypes === '补寄') {
        expressTypes = ['3','4'];
      } else {
        expressTypes = null;
      }
      let pyear;
      if (fieldsValue.pyear) {
        pyear = parseInt(fieldsValue.pyear);
      }
      if (err) return;
      fieldsValue = this.perpareValues(fieldsValue);

      this.setState({
        formatVal: {
          ...fieldsValue,
          expressTypes,
          pyear,
          pageNum,
          rowNum: pageSize,
        },
      });
      dispatch({
        type: 'rule/expressReceiptQuery',
        payload: {
          ...fieldsValue,
          expressTypes,
          pyear,
          pageNum,
          rowNum: pageSize,
        },
      });
    });
  }

  perpareValues(values){
    if(!values.insuredDate||values.insuredDate.length===0){
      values.startTime = '';
      values.endTime = '';
    } else {
      values.startTime = values.insuredDate[0].format("YYYY-MM-DD 00-00-00");
      values.endTime = values.insuredDate[1].format("YYYY-MM-DD 23-59-59");
    }
    if(!values.scanCodeDate||values.scanCodeDate.length===0){
      values.startScanCodeDate = '';
      values.endScanCodeDate = '';
    } else {
      values.startScanCodeDate = values.scanCodeDate[0].format("YYYY-MM-DD 00-00-00");
      values.endScanCodeDate = values.scanCodeDate[1].format("YYYY-MM-DD 23-59-59");
    }
    if(!values.expressPlace||values.expressPlace.length===0){
      values.staExpressPlaceOrderDate = '';
      values.endExpressPlaceOrderDate = '';
    } else {
      values.staExpressPlaceOrderDate = values.expressPlace[0].format("YYYY-MM-DD 00-00-00");
      values.endExpressPlaceOrderDate = values.expressPlace[1].format("YYYY-MM-DD 23-59-59");
    }
    if(!values.contractDate||values.contractDate.length===0){
      values.staContractTime = '';
      values.endContractTime = '';
    } else {
      values.staContractTime = values.contractDate[0].format("YYYY-MM-DD 00-00-00");
      values.endContractTime = values.contractDate[1].format("YYYY-MM-DD 23-59-59");
    }
    if(!values.insrncBeginDate||values.insrncBeginDate.length===0){
      values.startInsrncBeginDate = '';
      values.endInsrncBeginDate = '';
    } else {
      values.startInsrncBeginDate = values.insrncBeginDate[0].format("YYYY-MM-DD");
      values.endInsrncBeginDate = values.insrncBeginDate[1].format("YYYY-MM-DD");
    }
    if (values.extEnterpNameList !== undefined) {
      values.extEnterpCode = values.extEnterpNameList
    } else {
      values.extEnterpCode = ''
    }
    if (values.prodNameList !== undefined) {
      values.prodNo = values.prodNameList || '';
    } else {
      values.prodNo = '';
    }

    delete values.insuredDate;
    delete values.scanCodeDate;
    delete values.expressPlace;
    delete values.contractDate;
    delete values.insrncBeginDate;
    delete values.extEnterpNameList;
    delete values.prodNameList;

    for(const key in values){
      if(values[key]===undefined||values[key]===""){
        values[key] = '';
      }
    }
    return values;
  }

  renderAdvancedForm() {
    return (
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab={<span>模糊查询</span>} key="1">
          <SearchForm {...this.props} root={this}/>
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span>精确查询</span>} key="2">
        <SearchExactForm {...this.props} root={this}/>
        </Tabs.TabPane>
      </Tabs>
    );
  }

  printExcel = () => {
    this.props.dispatch({
      type: 'rule/exportExpressReceiptExcel',
      payload: this.state.formatVal,
    }).then().catch(e=>{
      message.error(e.message || '网络异常请稍后再试');
    });
  }
  // 导出问题清单
  onExportExpressQuestionInfo=() => {
    this.props.dispatch({
      type: 'rule/exportExpressQuestionInfo',
      payload: {},
    }).catch(e=>{
      message.error(e.message || '网络异常请稍后再试');
    });
  }
  render() {
    const { loading, rule: {receiptData} } = this.props;
    const { selectedRows, modalVisible } = this.state;
    let data;
    let total;
    if (receiptData) {
      if(receiptData.code == 0){
        data = receiptData.data.list;
        total = receiptData.data.total;
      }
    }

    const rowSelection = {
       onChange: (selectedRowKeys, selectedRows) => {
         
        },
    };

    const rowKey = (record, index) => { 
        return index;
    }

    const param = {
      showUploadList:false,
      name: 'file',
      beforeUpload: (file) => {
        this.props.dispatch({
          type: 'rule/uploadExpressBackFile',
          payload: {file}
        }).then().catch((e)=>{
          message.error(e.message || '网络异常');
        });
        return false;
      },
    };

    return (
      <PageHeaderLayout title="">
        <Card bordered={false}>
          <div>
            <div>{this.renderAdvancedForm()}</div>
            <Table scroll={{ x: 2000 }} 
                loading={loading}
                rowKey={this.rowKey}
                pagination={{
                showQuickJumper: true,
                showSizeChanger: true,
                ...{
                    current:this.state.pageNum,
                    pageSize:this.state.pageSize,
                    showTotal: (total, range) => `第${range[0]}条-第${range[1]}条 共${total}条  `,
                    total:total,
                    pageSizeOptions:['10','20','30','40','50'],
                },
              }}
                size='small'
                title={() => {
                  return (
                  <div>
                     <Upload {...param}>
                        <Button type='primary'>导入快递清单</Button>
                      </Upload>
                      <Button type='primary' onClick={this.printExcel} style={{marginLeft: 8}}>导出Excel清单</Button>
                      <Button type='primary' onClick={this.onExportExpressQuestionInfo} style={{marginLeft: 8}}>导出快递问题件</Button>
                  </div>
                  )
                }}
                bordered
                columns={this.getColumns()}
                onChange={this.handleTableChange}
             dataSource={data} />
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
}
