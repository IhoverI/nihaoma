import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  Input,
  DatePicker,
} from 'snk-web';
import { getSelectChild } from '../../utils/utils';
import ChannelAndProduction from 'components/ChannelAndProduction';

const FormItem = Form.Item;
const { Option } = Select;

@Form.create()
export default class SearchForm extends PureComponent {
  
  constructor(props){
    super(props);
    this.state = {
      
    };
    props.root.nomalForm = props.form;
  }

  handleSearch(e){
    this.props.root.handleSearch(e,this.props.form);
  }
 
  render() {
    const { form,root } = this.props;
    const { getFieldDecorator,getFieldValue } = form;
    const { RangePicker } = DatePicker;

    const date = new Date();
    const pyear = date.getFullYear();

    return (
      <Form className='custom-form-wrapper custom-search-form-wrapper' onSubmit={this.handleSearch.bind(this)} layout="inline">
        <Row gutter={{ md: 6, lg: 18, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="保单年份">
              {getFieldDecorator('pyear',{initialValue: pyear})(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {getSelectChild('pyear')}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="快递状态">
              {getFieldDecorator('postStatus')(
                <Select  
                  placeholder="请选择" 
                  style={{ width: '100%' }}
                >
                  <Option value="0">在途</Option>
                  <Option value="1">签收</Option>
                  <Option value="3">问题件</Option>
                  <Option value="5">待派送</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="快递公司">
              {getFieldDecorator('expressCode')(
                <Select  placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="E1003">顺丰</Option>
                  <Option value="E1010">德邦</Option>
                  <Option value="E1008">EMS</Option> 
                </Select>)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="纸质回执状态">
              {getFieldDecorator('paperReceiptFlag')(
                <Select  placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="1">已入库</Option>
                  <Option value="2">待入库</Option>
                  <Option value="3">遗失</Option>
                </Select>)}
            </FormItem>
          </Col>
        </Row>
        <ChannelAndProduction allowClear ChannelRule={this.state.mustInputRule} {...this.props} />
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="投保日期">  
              {getFieldDecorator('insuredDate')( // 
                <RangePicker  style={{ width: '100%' }} />
                )}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="纸质入库日期">
              {getFieldDecorator('scanCodeDate')(
                <RangePicker  style={{ width: '100%' }} />
                )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="快递下单日期">
              {getFieldDecorator('expressPlace')(
                <RangePicker  style={{ width: '100%' }} />
                )}
            </FormItem>
          </Col>
          <Col md={12} sm={24}>
            <FormItem label="快递签收日期">
              {getFieldDecorator('contractDate')(
                <RangePicker  style={{ width: '100%' }} />
                )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <FormItem label="保险起期">
              {getFieldDecorator('insrncBeginDate')(
                <RangePicker  style={{ width: '100%' }} />
                )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="寄送类型">
              {getFieldDecorator('expressTypes',{initialValue: null})( // 新单 1,2 补寄 3，4
                <Select  placeholder="请选择" style={{ width: '100%' }}>
                  <Option value='新单'>新单</Option>
                  <Option value='补寄'>补寄</Option>
                </Select>)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="保单状态">
              {getFieldDecorator('flag',{initialValue: null})( // 新单 1,2 补寄 3，4
                <Select  placeholder="请选择" style={{ width: '100%' }}>
                  <Option value='1'>有效</Option>
                  <Option value='2'>注销</Option>
                  <Option value='3'>退保</Option>
                  <Option value='4'>过期</Option>
                </Select>)}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden',textAlign:'center',paddingTop:10,paddingBottom:20  }}>
          <Button type="primary" htmlType="submit">
              查询
          </Button>
          <Button style={{ marginLeft: 8 }} onClick={()=>{root.handleFormReset(form)}}>
              重置
          </Button> 
        </div>
      </Form>
    );
  }
}
