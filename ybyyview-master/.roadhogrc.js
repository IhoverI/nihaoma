const path = require('path');
const pxtorem = require('postcss-pxtorem');

const svgSpriteDirs = [
  require.resolve('antd').replace(/warn\.js$/, ''), // antd 内置svg
  path.resolve(__dirname, 'src/assets/svg'),  // 业务代码本地私有 svg 存放目录
];

export default {
  entry: 'src/index.js',
  theme: 'src/theme.js',
  publicPath:"./",
  hash:true,
  svgSpriteLoaderDirs: svgSpriteDirs,
  extraBabelPlugins: [
    'transform-runtime',"transform-decorators-legacy",
    [
        'babel-plugin-module-resolver',
        {
          alias: {
            components: './src/components',
          },
        },
    ],
    ['import', { 'libraryName': 'snk-web', 'libraryDirectory': 'src/lib' }],
  ],
  extraBabelIncludes:[
    "./components","node_modules/snk-sso-um"
  ],
  cssModulesExclude:[
    "./components"
  ],
  env: {
    development: { // 开发环境
      extraBabelPlugins: [
        'dva-hmr',
      ],
      define: {
        SYSTEMNAME:'银保运营系统v2.0(开发环境)',
        SERVER:'',
        SERVERLOCAL: 'http://10.1.109.202:8370/',
        SERVERHR:'http://10.1.109.202:8370/hr-emp/',
        VIEWLOCAL:"http://10.1.109.201:8000/#/",
        UMBUTTONSERVER:'http://10.1.109.202:8370/um/findUserAllButton',// UM 按钮
        SSOSERVER:'http://10.1.109.202:8370/sso/user', // sso 地址
        UMMENUSERVER:'http://10.1.109.202:8370/um/findUserAllMenu', // UM 地址
        SSOLOGOUTSERVER:'http://10.1.109.27:8080/sso/logout',
        CLAIMHREF:'http://10.1.109.101:8006/claim/processRegist.do?actionType=prepareQueryClaimStatues', //理赔查询链接
        ILOGURl: 'http://10.1.109.201:9081/teamserver/faces/login.jsp',
      }
    },
    sit:{
      define:{
        SERVER:'',
        SYSTEMNAME:'银保运营系统v2.0(SIT环境)',
        SERVERLOCAL: 'http://10.1.109.202:8370/',
        SERVERHR:'http://10.1.109.202:8370/hr-emp/',
        VIEWLOCAL:"http://10.1.109.201:8000/#/",
        UMBUTTONSERVER:'http://10.1.109.202:8370/um/findUserAllButton',// UM 按钮
        SSOSERVER:'http://10.1.109.202:8370/sso/user', // sso 地址
        UMMENUSERVER:'http://10.1.109.202:8370/um/findUserAllMenu', // UM 地址
        SSOLOGOUTSERVER:'http://10.1.109.27:8080/sso/logout',
        CLAIMHREF:'http://10.1.109.58:8006/claim/processRegist.do?actionType=prepareQueryClaimStatues', //理赔查询链接
        ILOGURl: 'http://10.1.109.201:9081/teamserver/faces/login.jsp',
      },
    },
    uat:{
      define:{
        SERVER:'',
        SYSTEMNAME:'银保运营系统v2.0(UAT环境)',
        SERVERLOCAL: 'http://10.1.109.203:8370/',
        SERVERHR:'http://10.1.109.203:8370/hr-emp/',
        VIEWLOCAL:"http://10.1.109.203:8000/#/",
        SSOSERVER:'http://10.1.109.203:8370/sso/user', // sso 地址
        UMMENUSERVER:'http://10.1.109.203:8370/um/findUserAllMenu', // UM 地址
        SSOLOGOUTSERVER:'http://10.1.109.27:8080/sso/logout',
        UMBUTTONSERVER:'http://10.1.109.203:8370/um/findUserAllButton',// UM 按钮   
        CLAIMHREF:'http://10.1.23.4:8006/claim/processRegist.do?actionType=prepareQueryClaimStatues', //理赔查询链接
        ILOGURl: 'http://10.1.109.201:9081/teamserver/faces/login.jsp',
      },
    },
    production: { // 生产环境
      define: {
        // SERVERLOCAL: 'http://10.1.109.202:8360/',
        // UMLOCAL:'',
        // viewlocal:"",
        /*SERVER:'',
        SYSTEMNAME:'银保运营系统v2.0',
        SERVERLOCAL: 'http://10.1.4.109:8370/',
        SERVERHR:'http://10.1.109.203:8370/',
        VIEWLOCAL:"http://10.1.4.109:8370/#/",
        SSOSERVER:'http://10.1.4.109:8370/sso/user', // sso 地址
        UMMENUSERVER:'http://10.1.4.109:8370/um/findUserAllMenu', // UM 地址
        SSOLOGOUTSERVER:'http://sso.sinosafe.com.cn:8080/sso/logout',
        UMBUTTONSERVER:'http://10.1.4.109:8370/um/findUserAllButton',// UM 按钮   
        CLAIMHREF:'http://10.1.23.4:8006/claim/processRegist.do?actionType=prepareQueryClaimStatues', //理赔查询链接
        ILOGURl: 'http://10.1.109.201:9081/teamserver/faces/login.jsp',*/
      }
    }
  },
  define: {
    VERSION: require('./package.json').version
  },
  hash: true,
  ignoreMomentLocale: true,
}
