## 配置公司内部镜像

> npm config set registry http://10.1.109.158:8081/nexus/content/groups/npm-all --global

## 组件引用

### 组件底层是antd 

> 原来的方式 import { Button , xxx} from 'antd'

> 现在的方式 import { Button , xxx} from 'snk-web'


## 打包

```
    "start": "cross-env ESLINT=none NODE_ENV=development roadhog server",
    "start-sit": "cross-env ESLINT=none NODE_ENV=sit roadhog server",
    "start-uat": "cross-env ESLINT=none NODE_ENV=uat roadhog server",
    "start-pro": "cross-env ESLINT=none NODE_ENV=production roadhog server",
    "build": "cross-env ESLINT=none NODE_ENV=development roadhog build",
    "build-uat": "cross-env ESLINT=none NODE_ENV=uat roadhog build",
    "build-sit": "cross-env ESLINT=none NODE_ENV=sit roadhog build",
    "build-pro": "cross-env ESLINT=none NODE_ENV=production roadhog build",
```

> 以上是package.json中的命令配置。 分别对应了 四个环境的运行命令和打包命令

> 其中区分不同环境的变量是NODE_ENV={xxxx}

> 不同环境的服务器地址配置在 **.roadhogrc.js** 文件中,其中的配置字段是 env 

> 在roadhogrc中 针对不同的开发环境 把相应的链接配置完善【重点】

> 运行相应环境的命令是  npm run start|start-sit|start-uat|start-pro   （其中选一个）

> 运行验证相应环境正常后，

> 再运行相应的打包命令 npm run build|build-sit|build-uat|build-pro   （其中选一个）

> 在项目dist文件中找到相应的打包文件